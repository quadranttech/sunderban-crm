//     //----------Select the first tab and div by default
    $('.vertical-nav-wrap > ul > li > a').eq(0).addClass( "selected" );
    $('.vertical-tabs-content > .ver-tabs-panel').eq(0).css('display','block');
//     //---------- This assigns an onclick event to each tab link("a" tag) and passes a parameter to the showHideTab() function
 $(".top-menu a").on("click", function(){
      $(".top-menu").find(".active").removeClass("active");
      $(this).parent().addClass("active");
   });
$(".wrapper").css("padding-top",$( "header.header").height());
// PROFILE USER
$(".profile-usermenu").css("top",$( "header.header").height());
$( "header.header").height();
$('.dropdown-button').dropdown('open');
// OPEN CONTACT SCREEN
$(".select-screen").click(function(){
    $(".screens").addClass("screen-selected");
    $("#contact-screen").css('display', 'block');
    $("#select-screen").css('display', 'none');
});
// MATERIAL SELECT
$('select').material_select();
// DRAG AND DROP
$( "#sortable" ).sortable({
    placeholder: "ui-state-highlight"
});
$( "#sortable" ).disableSelection();
// MORE TASKS
var mouse_is_inside = false;
$('.more-task-popup').hover(function(){ 
    mouse_is_inside=true; 
}, function(){ 
    mouse_is_inside=false; 
});
$("body").mouseup(function(){ 
    if(! mouse_is_inside) $('.more-task-popup').hide();
});
// NOTIFICATION
var notification_close=false;
$('.notification').click (function(event){ 
    if(!jQuery(".notification-wrapper").hasClass("notification-open"))
    {
        jQuery(".notification-wrapper").addClass("notification-open");
        setTimeout(function(){ 
            notification_close=true;
        }, 500)
    }
});
jQuery(document).on("click","body > div",function(event) {
    if((jQuery(event.target).closest(".notification-wrapper").length==0) && (jQuery(".notification-wrapper").hasClass("notification-open")) && notification_close==true)
    {
        jQuery(".notification-wrapper").removeClass("notification-open");
        notification_close=false;
    }
    else
    {
        if(jQuery(event.target).closest(".chips").length==1)
        {
            jQuery(".notification-wrapper").removeClass("notification-open");
            notification_close=false;
        }
    }
    if(jQuery(event.target).closest(".search-icon").length==1 && jQuery(".search-open").length==0)
    {
        jQuery(".search").addClass("search-open");
    }
    else if((jQuery(event.target).closest(".search").length==0 && jQuery(".search-open").length==1) || (jQuery(event.target).closest(".search-icon").length==1 && jQuery(".search-open").length==1))
    {
        jQuery(".search").removeClass("search-open");
    }
});

// Partpay

jQuery(".partpay-toggle").hide();

jQuery(".part-pay").click(function(){
    jQuery(".partpay-toggle").slideToggle("slow");
});

// CLONE ROW
$('.im-profile-form-wrapper .add-input').click (function(){ 
    $( $('#clonable-iM').html() ).appendTo('.im-profile-form-wrapper');
});
$('.social-profile-form-wrapper .add-input').click (function(){ 
    $( $('#social-profile').html() ).appendTo('.social-profile-form-wrapper');
});
$('.address-form-wrapper .add-input').click (function(){ 
    $( $('#clone-address').html() ).appendTo('.address-form-wrapper');
});
// OPEN INFORMATIONS SECTION
$(".address-information, .im-profile, .social-profile, .add-company-contact").hide();
$(".open-address").click(function(){
    $(".address-information").slideToggle("slow");
});
$(".open-im").click(function(){
    if(($(document).scrollTop()<=120 || $(".address-information").css("display")=="block" || $(".im-profile").css("display")=="block") && $(".im-profile").css("display")=="none")
    {
        $(".im-profile").slideToggle("slow");
        $('html, body').animate({
            scrollTop: $(".im-profile").offset().top
        }, 1500);
    }
    else
    {
        $(".im-profile").slideToggle("slow");
    }
});
$(".open-social").click(function(){
    if($(document).scrollTop()<=350 && $(".social-profile").css("display")=="none")
    {
        $(".social-profile").slideToggle("slow");
        $('html, body').animate({
            scrollTop: $(".social-profile").offset().top
        }, 1500);
    }
    else
    {
        $(".social-profile").slideToggle("slow");
    }
});
$(".add-contact-company").click(function(){
    $(".add-company-contact").slideToggle("slow");
});
// WRONG PASSWORD OPEN EMAIL
$(".submit-button > button.teal-lighten-one").hide();
$("span.wrong-pass a.need-help").click(function(){
    history.pushState('obj', 'forgot-password', APP_URL+'/welcome#forgot-password');
    $("#signin_email").parent(".input-field").removeClass("input-invalid");
    $(".wrong-password").addClass("hide-pass animated fadeOut");
    $(".submit-button .orange").addClass("animated fadeOut");
    $(".add-email").addClass("email-full");
    $(".submit-button > button").hide();
    $(".submit-button > button.teal-lighten-one").show(700);
    jQuery("#backToSignIn").delay(700).css("display","block");
    // jQuery("#sendResetPasswordEmail").delay(700).css("display","inline-block");
});
//BACK TO LOGIN
jQuery("#backToSignIn").click(function(event){
    event.preventDefault();
    history.pushState('obj', 'backToSignIn', APP_URL);
    jQuery(this).hide();
    jQuery(".wrong-password").removeClass("hide-pass animated fadeOut");
    jQuery(".add-email").removeClass("email-full");
    jQuery(".submit-button > button.teal-lighten-one").hide();
    jQuery(".submit-button > button").show();
    jQuery("#sendResetPasswordEmail").hide();
});
// SIGN UP FORM OPEN
$(".signin-form-wrapper .newuser a").click(function(){
    $(".signin-form-wrapper").animate({ opacity: 0 }, 1000);
    $(".signup-form-wrapper").animate({ opacity: 1 }, 3000);
    $(".signup-form-wrapper").css('z-index', '2');
});
// SIGN IN FORM OPEN
$(".signup-form-wrapper .newuser a").click(function(){
    $(".signin-form-wrapper").animate({ opacity: 1 }, 3000);
    $(".signup-form-wrapper").animate({ opacity: 0 }, 3000);
});
// TAB
$( "#tabs" ).tabs();
// TAB DOCUMENT DRAG NAME
'use strict';
$('.input-file').each(function() {
    var $input = $(this),
        $label = $input.next('.drag-label'),
        labelVal = $label.html();
    $input.on('change', function(element) {
        var fileName = '';
        if (element.target.value) fileName = element.target.value.split('\\').pop();
        fileName ? $label.addClass('has-file').find('.file-name').html(fileName) : $label.removeClass('has-file').html(labelVal);
    });
});
// TAB NOTE
$(".note-close").click(function(){
    $(".note-lists").show();
    $(".notes-form").hide();
})
$(document).on("click",".notes",function(){
    $(".notes-form").animate({ opacity: 1 }, 1500);
    $(".note-lists").hide();
    jQuery(".showbox-wrapper").show();
    var note_id=jQuery(this).parent("li.note-li").attr("note-id");
    jQuery("input[name='add-attachment']").val("");
    if(typeof note_id !== "undefined")
    {
        jQuery.ajax({
            url:APP_URL+'/ajax/getNoteById',
            data:{'_token':jQuery("input[name='_token']").val(),note_id:note_id},
            dataType:'json',
            type:'post',
            success:function(data){
              var count=0;
              var attachment_html='';
              while(count<data.attachment.length)
              {
                attachment_html=attachment_html+'<a href="#!"><span>'+data.attachment[count]+'</span></a>';
                count++;
              }
              if(data.response!= null)
              {
                jQuery(".attachment").html(attachment_html);
                jQuery(".attachment").closest(".form-layout-100").show();
                jQuery("#textarea1").val(data.response.note);
                jQuery("#first_name").val(data.response.note_title);
                jQuery("input[name='current_note_id']").val(data.response.id);
                jQuery(".note-border").attr('class', 'note-border '+data.response.color);
              }
              jQuery(".showbox-wrapper").hide();
              $(".notes-form").show();
            },
        });
    }
    else
    {
        jQuery(".showbox-wrapper").hide();
        $(".notes-form").show();
    }
});
$(document).on("click",".note-edit , .note-expand",function(){
    $(".notes-form").animate({ opacity: 1 }, 1500);
    $(".note-lists").hide();
    jQuery(".showbox-wrapper").show();
    var note_id=jQuery(this).parents("li.note-li").attr("note-id");
    if(typeof note_id !== "undefined")
    {
        jQuery.ajax({
            url:APP_URL+'/ajax/getNoteById',
            data:{'_token':jQuery("input[name='_token']").val(),note_id:note_id},
            dataType:'json',
            type:'post',
            success:function(data){
              var count=0;
              var attachment_html='';
              while(count<data.attachment.length)
              {
                attachment_html=attachment_html+'<a href="#!"><span>'+data.attachment[count]+'</span></a>';
                count++;
              }
              if(data.response!= null)
              {
                jQuery(".attachment").html(attachment_html);
                jQuery(".attachment").closest(".form-layout-100").show();
                jQuery("#textarea1").val(data.response.note);
                jQuery("#first_name").val(data.response.note_title);
                jQuery("input[name='current_note_id']").val(data.response.id);
                jQuery(".note-border").addClass(data.response.color);
              }
              jQuery(".showbox-wrapper").hide();
              $(".notes-form").show();
            },
        });
    }
});
$(".note-close").click(function(){
    $(".notes-form").animate({ opacity: 0 }, 1000);
    $(".note-lists").animate({ opacity: 1 }, 1500);
    $(".note-lists").css('z-index', '2');
});
$(".minimize").click(function(){
    $(this).closest(".ui-state-default").find(".maximize").css('display', 'inline-block');
    $(this).closest(".ui-state-default").find(".minimize").css('display', 'none');
    $(this).closest(".ui-state-default").find(".widgets-collapse").slideUp("slow");
});
$(".maximize").click(function(){
    $(this).closest(".ui-state-default").find(".minimize").css('display', 'inline-block');
    $(this).closest(".ui-state-default").find(".maximize").css('display', 'none');
    $(this).closest(".ui-state-default").find(".widgets-collapse").slideDown("slow");
    // $(".widgets-collapse").slideDown("slow");
});
// MODAL
$('.modal').modal({
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .8, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '5%', // Starting top style attribute
    endingTop: '20%' // Ending top style attribute
});
$(".modal-trigger").click(function(){
    jQuery(".modal-content").find("form")[0].reset();
    jQuery(".modal-content").find("form label").removeClass("active");
});
// DATE PICKER
$( "#datepicker" ).datepicker();
// DRAG AND DROP DEALS
(function ( $, window, undefined ) {
    var pluginName = 'dragDrop',
    document = window.document,
    defaults = {
        draggableSelector: ".draggable",
        droppableSelector: ".droppable",
        appendToSelector: false
    };
    function Plugin( element, options ) {
        this.element = element;
        this.options = $.extend( {}, defaults, options) ;
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }
    Plugin.prototype.init = function () {
        var droppables = $(this.element).find(this.options.droppableSelector);
        var draggables = $(this.element).find(this.options.draggableSelector).attr("draggable", "true");
        if(this.options.appendToSelector){
            var appendables = $(this.options.appendToSelector);
            appendables.on({
                'dragenter': function(ev){
                    ev.preventDefault();
                    return true;
                },
                'drop': function(ev){
                    var data = ev.originalEvent.dataTransfer.getData("Text");
                    var draggedEl = document.getElementById(data);
                    var destinationEl = $(ev.target);
                    destinationEl = destinationEl.closest(appendables.selector).siblings(droppables.selector).append(draggedEl);
                    $('.active').removeClass('active');
                    $('.over').removeClass('over');
                    ev.stopPropagation();
                    return false;
                },
                'dragover': function(ev){
                    ev.preventDefault();
                    $(ev.target).closest(appendables.selector).addClass('over');
                    return true;
                },
                'dragleave': function(ev){
                    ev.preventDefault();
                    $(ev.target).closest(appendables.selector).removeClass('over');
                    return true;
                }
            });
        }
        droppables.on({
            'mouseup': function(ev){
                $('.active').removeClass('active');
                return true;
            },
            'dragenter': function(ev){
                ev.preventDefault();
                return true;
            },
            'drop': function(ev){
                var data = ev.originalEvent.dataTransfer.getData("Text");
                var draggedEl = document.getElementById(data);
                var destinationEl = $(ev.target);
                destinationEl.closest(draggables.selector).before(draggedEl);
                $('.active').removeClass('active');
                $('.over').removeClass('over');
                ev.stopPropagation();
                return false;
            },
            'dragover': function(ev){
                ev.preventDefault();
                $(ev.target).closest(draggables.selector).addClass('over');
                return true;
            },
            'dragleave': function(ev){
                ev.preventDefault();
                $(ev.target).closest(draggables.selector).removeClass('over');
                return true;
            }
        });
        draggables.on({
            'mousedown': function(ev){
                $(ev.target).closest(draggables.selector).addClass('active');
                return true;
            },
            'mouseup': function(ev){
                $('.active').removeClass('active');
                return true;
            },
            'dragstart': function(ev){
                ev.originalEvent.dataTransfer.effectAllowed='move';
                ev.originalEvent.dataTransfer.setData("Text", ev.target.getAttribute('id'));
                ev.originalEvent.dataTransfer.setDragImage(ev.target,100,20);
                return true;
            },
            'dragend': function(ev){
                return true;
            }
        });
    };
    // A really lightweight plugin wrapper around the constructor, 
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
            }
        });
    }
}(jQuery, window));
$(document).ready(function(){
  $("#drag-list").dragDrop({
          draggableSelector: ".task",
          droppableSelector: ".task-list",
      appendToSelector: ".drag-column-header"
    });
});
// DATE AND TIME PICKER
jQuery(document).ready(function(){
    jQuery('#date-format').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm'
    });
    jQuery(".tasks-priority #date-fr").bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm'
    });
    jQuery('#date').bootstrapMaterialDatePicker({ 
        weekStart : 0, 
        time: false 
    });
    jQuery('#date2').bootstrapMaterialDatePicker({ 
        weekStart : 0, 
        time: false 
    });
    jQuery('#date3').bootstrapMaterialDatePicker({ 
        weekStart : 0, 
        time: false 
    });
    jQuery.material.init()
});
// TAGS
if(jQuery("input[name='user_agent']").val()!=undefined && jQuery("input[name='user_agent']").val()=="")
{
    dragulaaaaa();
}
function dragulaaaaa()
{
    dragula([
        document.getElementById('1'),
        document.getElementById('2'),
        document.getElementById('3'),
        document.getElementById('4'),
        document.getElementById('5')
    ])
    .on('drag', function(el) {
        // add 'is-moving' class to element being dragged
        el.classList.add('is-moving');
    })
    .on('dragend', function(el) {
        // remove 'is-moving' class from element after dragging has stopped
        el.classList.remove('is-moving');
        // add the 'is-moved' class for 600ms then remove it
        window.setTimeout(function() {
            el.classList.add('is-moved');
            window.setTimeout(function() {
                el.classList.remove('is-moved');
            }, 600);
        }, 100);
    });
}
var createOptions = (function() {
    var dragOptions = document.querySelectorAll('.drag-options');
    // these strings are used for the checkbox labels
    var options = ['Research', 'Strategy', 'Inspiration', 'Execution'];
    // create the checkbox and labels here, just to keep the html clean. append the <label> to '.drag-options'
    function create() {
        for (var i = 0; i < dragOptions.length; i++) {
            options.forEach(function(item) {
                var checkbox = document.createElement('input');
                var label = document.createElement('label');
                var span = document.createElement('span');
                checkbox.setAttribute('type', 'checkbox');
                span.innerHTML = item;
                label.appendChild(span);
                label.insertBefore(checkbox, label.firstChild);
                label.classList.add('drag-options-label');
                dragOptions[i].appendChild(label);
            });
        }
    }
    return {
        create: create
    }
}());
var showOptions = (function () {
    // the 3 dot icon
    var more = document.querySelectorAll('.drag-header-more');
    function show() {
        // show 'drag-options' div when the more icon is clicked
        var target = this.getAttribute('data-target');
        var options = document.getElementById(target);
        options.classList.toggle('active');
    }
    function init() {
        for (i = 0; i < more.length; i++) {
            more[i].addEventListener('click', show, false);
        }
    }
    return {
        init: init
    }
}());
createOptions.create();
showOptions.init();
// LINE CHART

function getDeal(response)
{
    jQuery.ajax({
        url:APP_URL+'/get-all-deals',
        dataType:'json',
        // async:false,
        data:{},
        type:'get',
        success:function(data){
            var data_v = [];
            if(data.status=="success")
            {
                var count=0;
                var monthNames = getYearArray();
                var dealMonthNames = {};
                var newDealMonthNamesAndValue = [];
                var monthNumbers = getYearNumberArray();
                var previousMonthNumber="";
                var value=0;
                jQuery(data.date_array).each(function(index,element){
                    if(monthNumbers.indexOf(element.split("-")[1])!=-1 && previousMonthNumber!=element.split("-")[1])
                    {
                        if((previousMonthNumber!="") && (element.split("-")[1]-previousMonthNumber==1))
                        {
                            dealMonthNames[previousMonthNumber]=value;
                        }
                        value=0;
                        value=parseInt(value)+parseInt(data.deal_array[index]);
                        previousMonthNumber=element.split("-")[1];
                    }
                    else if(monthNumbers.indexOf(element.split("-")[1])!=-1 && previousMonthNumber==element.split("-")[1])
                    {
                        value=parseInt(value)+parseInt(data.deal_array[index]);
                        previousMonthNumber=element.split("-")[1];
                    }
                });
                dealMonthNames[previousMonthNumber]=value;
                jQuery(monthNumbers).each(function(index,element){
                        var valueInserted=false;
                        jQuery.each(dealMonthNames, function(key,valueObj){
                            if(element==key)
                            {
                                data_v.push(valueObj);
                                valueInserted=true;
                            }
                        });
                        if(valueInserted==false && element<=Object.keys(dealMonthNames)[parseInt(Object.keys(dealMonthNames).length)-1])
                        {
                            data_v.push(0);
                        }
                });
                var result = [data_v,data.deal_value_array];
                response(result);
            }
            else
            {
                var result = [data_v,data.deal_value_array];
                data_v=[];
                response(result);
            }
        },
    });
}

function lineChart(deal)
{
    console.log(deal);
    if(deal[0].length>0)
    {
        var yearArray = getYearArray();
        var lineChartData = {
            labels: yearArray,
            datasets: [{
                label: "Deals created",
                borderColor: 'rgb(44,148,230)',
                backgroundColor: 'rgb(255,255,255)',
                borderWidth: 2,
                fill: false,
                lineTension: 0,
                data: deal[0],
                yAxisID: "y-axis-1",
            }, {
                label: "Deals won",
                borderColor: 'rgb(18,154,127)',
                backgroundColor: 'rgb(255,255,255)',
                borderWidth: 2,
                fill: false,
                lineTension: 0,
                data: deal[1],
                yAxisID: "y-axis-2"
            }]
        };

    
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = Chart.Line(ctx, {
            data: lineChartData,
            options: {
                responsive: true,
                hoverMode: 'index',
                stacked: false,
                legend: {
                    onClick: (e) => e.stopPropagation()
                },
                title:{
                    display: true,
                    text:'Sales Month View'
                },
                scales: {
                    yAxes: [{
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "left",
                        id: "y-axis-1",
                    }, {
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "right",
                        id: "y-axis-2",
                        // grid line settings
                        gridLines: {
                            drawOnChartArea: false, // only want the grid lines for one axis to show up
                        },
                    }],
                }
            }
        });
    }
    else
    {
        jQuery(".sales-graph").html('<h3>Monthly Sales Figures</h3><div class="no-sales-data"><img src="assets/img/graph-default.jpg" ><div class="no-sales"><h4>No data yet, lets start now</h4><a href="'+APP_URL+'/deals" class="modal-trigger waves-effect waves-light btn blue">Create Deal</a></div></div>');
    }
}

//BAR CHART
function getDealNameNumber(response)
{
    jQuery.ajax({
        url:APP_URL+'/get-deal-name-number',
        dataType:'json',
        data:{},
        type:'get',
        success:function(data){
            response(data);
        },
    });
}



function getYearArray()
{
    var d = new Date(),
    n = d.getMonth();
    var year_array = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
    var yearCount = 0;
    var yearArray = [];
    while(yearCount<=n)
    {
        yearArray.push(year_array[yearCount]);
        yearCount++;
    }
    return yearArray;
}

function getYearNumberArray()
{
    var d = new Date(),
    n = d.getMonth();
    var year_number_array = ['01','02','03','04','05','06','07','08','09','10','11','12'];
    var yearNumberCount = 0;
    var yearNumberArray = [];
    while(yearNumberCount<=n)
    {
        yearNumberArray.push(year_number_array[yearNumberCount]);
        yearNumberCount++;
    }
    return yearNumberArray;
}

if(window.location.href==APP_URL+'/dashboard' || window.location.href==APP_URL+'/dashboard#!')
{
    setTimeout(function(){
        getDeal(function(output){
          lineChart(output);
        });

        console.log("sdasdas");
        getDealNameNumber(function(output){
            console.log(output);
            var dealNameAndNumber=[];
            var dealName=output.dealStage;
            if(output.response=="success")
            {
                dealNameAndNumber=output.dealNameAndNumber;
            }
            console.log(dealNameAndNumber.every(checkValue));
            console.log(dealNameAndNumber);
            console.log(dealName);
            funnelChart(dealNameAndNumber,dealName,dealNameAndNumber.every(checkValue));
        });

        getShowTutorial();
    }, 1000);
}

function checkValue(age) {
    return age == 0;
}

function getShowTutorial()
{
    jQuery.ajax({
        url:APP_URL+'/get-show-tutorial',
        dataType:'json',
        // async:false,
        data:{},
        type:'get',
        success:function(data){
            jQuery('.flexslider').flexslider({
                animation: "slide"
            });
            if(data.response=="on")
            {
                jQuery("a[href='#tutorial']").trigger("click");
            }
        },
    });
}

//SKIP TUTORIAL
jQuery(".skip_tutorial").click(function(){
    tutorial('skip_tutorial');
});

//VIEW LATER TUTORIAL
jQuery(".view_later_tutorial").click(function(){
    tutorial('view_later_tutorial');
});

function tutorial(purpose)
{
    jQuery.ajax({
        url:APP_URL+'/skip-tutorial',
        dataType:'json',
        // async:false,
        data:{purpose:purpose},
        type:'get',
        success:function(data){
            console.log(data);
            jQuery("#tutorial").hide();
            jQuery(".modal-overlay").hide();
            jQuery("#tutorial").removeClass("open");
        },
    });
}

var currency_symbol={'USD': '$','EUR': '€','CRC': '₡','GBP': '£','ILS': '₪','INR': '₹','JPY': '¥','KRW': '₩','NGN': '₦','PHP': '₱','PLN': 'zł','PYG': '₲','THB': '฿','UAH': '₴','VND': '₫'};




function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (25+o.scrollHeight)+"px";
}
$(document).ready(function() {
  // get the name of uploaded file
    $('input[type="file"]').change(function(){
        var value = $("input[type='file']").val();
        $('.js-value').text(value);
    });
});
$(document).ready(function(){
    $("a#flip").click(function(){
        $("#panel").slideToggle("slow");
    });
    $("a.call-to-close").click(function(){
        $("#panel").slideUp("slow");
    });
});
$("li.drag-column:eq(0)").addClass("active");
$("ul.drag-list").height($("li.drag-column:eq(0)").outerHeight());
$(".nxt").click(function(){
    var old_index=parseInt($("li.active").index());
    var new_index=parseInt($("li.active").index())+1;
    if(new_index<$("ul.drag-list li.drag-column").length)
    {
        $("ul.drag-list li.drag-column:eq("+old_index+")").removeClass("active");
        $("ul.drag-list li.drag-column:eq("+new_index+")").addClass("active");
        $("ul.drag-list").height($("ul.drag-list li.drag-column:eq("+new_index+")").outerHeight());
    }
});
$(".pre").click(function(){
    var old_index=parseInt($("li.active").index());
    var new_index=parseInt($("li.active").index())-1;
    if(new_index>=0)
    {
        $("ul.drag-list li.drag-column:eq("+old_index+")").removeClass("active");
        $("ul.drag-list li.drag-column:eq("+new_index+")").addClass("active");
        $("ul.drag-list").height($("ul.drag-list li.drag-column:eq("+new_index+")").outerHeight());
    }
});

$(".drag-container").width(jQuery(window).width()-20);

if(window.location.href==APP_URL+'/deals' || window.location.href==APP_URL+'/deals#')
{
    if(jQuery(".drag-list li").length>4)
    {
        var li_count=4;
        var width=0+jQuery(".drag-wrapper").width();
        while(li_count<jQuery(".drag-list li.drag-column").length)
        {
            width=width+330;
            li_count++;
        }
        jQuery(".drag-wrapper").width(width);
    }
}

jQuery("input[name='event_type']").change(function(){
    jQuery("input[name='event_type']").closest(".widget-radio").removeClass("active");
    jQuery("input[name='event_type']").attr("checked",false);
    jQuery(this).closest(".widget-radio").addClass("active");
    jQuery(this).attr("checked",true);
});

//CLOSE MODAL
jQuery(".modal-close").click(function(){
    jQuery(this).closest(".modal").hide();
});

// FUNNEL GRAPH
function funnelChart(dealNameAndNumber,dealName,checkValue)
{
    console.log("funnelChart");
    console.log(checkValue);
    if(checkValue==false)
    {
        console.log("if funnel");
        var config = {
            type: 'funnel',
            data: {
                datasets: [{
                    data: dealNameAndNumber,
                    backgroundColor: [
                        "#98caec",
                        "#00acec",
                        "#fcd91d",
                        "#ff7449"
                    ],
                    hoverBackgroundColor: [
                        "#5998c3",
                        "#03688d",
                        "#d6bc2f",
                        "#ce522b"
                    ]
                }],
                labels: dealName
            },
            options: {
                responsive: true,
                sort: 'desc',
                legend: {
                    position: 'top',
                    onClick: (e) => e.stopPropagation()
                },
                title: {
                    display: false,
                    text: 'Chart.js Funnel Chart'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };

        var ctx = document.getElementById("chart-area").getContext("2d");
        window.myDoughnut = new Chart(ctx, config);
    }
    else
    {
        console.log("else funnel");
        jQuery(".funnel-graph").html('<h3>Lead Figures - Current Month</h3><div class="no-sales-data"><img src="assets/img/funnel-graph-default.jpg" ><div class="no-sales"><h4>No data yet, lets start now</h4><a href="'+APP_URL+'/deals" class="modal-trigger waves-effect waves-light btn blue">Create Deal</a></div></div>');
    }
}


// DASHBOARD CHAGE
jQuery(".select-dashboard a").click(function(){
    jQuery(".select-dashboard li").removeClass("active");
    if(jQuery(this).text()=="Company Dashboard")
    {
        jQuery(this).closest("li").addClass("active");
        jQuery(".personal-dashboard").hide();
        jQuery(".company-dashboard").show();
    }
    else
    {
        jQuery(this).closest("li").addClass("active");
        jQuery(".company-dashboard").hide();
        jQuery(".personal-dashboard").show();
    }
});

//PUSH NOTIFICATION
function pushNotification(title)
{
    Push.create(title+"!", {
        body: title,
        icon: '/icon.png',
        timeout: 8000,
        onClick: function () {
            window.focus();
            this.close();
        }
    });
}
ajaxNotification();
function ajaxNotification()
{
    jQuery.ajax({
        url:APP_URL+'/get-ajax-notification',
        dataType:'json',
        // async:false,
        data:{},
        type:'get',
        success:function(data){
            if(data.response.length>0)
            {
                var count=0;
                while(count<data.response.length)
                {
                    pushNotification(data.response[count].notification);
                    count++;
                }
            }
        },
    });
}




// document.getElementById("automation").onclick = function() {
//   this.select();
//   execCommand('copy');
// }

if(jQuery(document).find("#copyButton").length>0)
{
    document.getElementById("copyButton").addEventListener("click", function() {
        copyToClipboard(document.getElementById("copyTarget"));
    });
}

function copyToClipboard(elem) {
      // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
          succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

// $(function () {
//   var $image = $('#avatarInput');
//   var minAspectRatio = 1;
//   var maxAspectRatio = 2;

//   $image.cropper({
//     cropmove: function (e) {
//       var cropBoxData = $image.cropper('getCropBoxData');
//       var cropBoxWidth = cropBoxData.width;
//       var aspectRatio = cropBoxWidth / cropBoxData.height;

//       if (aspectRatio < minAspectRatio) {
//         $image.cropper('setCropBoxData', {
//           height: cropBoxWidth / minAspectRatio
//         });
//       } else if (aspectRatio > maxAspectRatio) {
//         $image.cropper('setCropBoxData', {
//           height: cropBoxWidth / maxAspectRatio
//         });
//       }
//     }
//   });
// });

//CLOSE OVERLAY ON MODAL CLOSE
setTimeout(function(){
jQuery(".modal-close").click(function(){
    console.log("element");
    jQuery(this).closest("modal").find("select").material_select();
    jQuery(".modal-overlay").hide();
    jQuery("button").each(function(index,element){
        jQuery(this).text(jQuery(element).attr("data-name"));
    });
    if(window.location.href==APP_URL+'/invoice' || window.location.href==APP_URL+'/invoice#!')
    {
        jQuery('<span class="">Please wait while reloading</span>').insertAfter(jQuery(".save-invoice:last"));
        setTimeout(function(){
            window.location.href=APP_URL+'/edit-invoice?inv_no='+jQuery("input[name='invoice_no']").val();
        }, 1000);
    }
    if(window.location.href==APP_URL+'/estimate' || window.location.href==APP_URL+'/estimate#!')
    {
        jQuery('<span class="">Please wait while reloading</span>').insertAfter(jQuery(".save-estimate:last"));
        setTimeout(function(){
            window.location.href=APP_URL+'/edit-estimate?est_no='+jQuery("input[name='invoice_no']").val();
        }, 1000);
    }
});
}, 3000);

//SORTING
jQuery(document).on("click",".sortable",function(){
    console.log("afdsfdsf");
    jQuery(".showbox-wrapper").show();
    var temp_html=[];
    var clone_class=jQuery(this).attr("data-clone");
    var target=jQuery(document).find("."+jQuery(this).attr("data-clone"));
    if(jQuery(this).attr("data-sort")=="id")
    {
        var id_array=[];
        jQuery(target).each(function(index,element){
            if(jQuery(element).find(".inv_no").attr("data-id")!=undefined)
            {
                id_array.push(jQuery(element).find(".inv_no").attr("data-id"));
            }
            else
            {
                id_array.push(jQuery(element).find(".est_no").attr("data-id"));
            }
        });
        if(jQuery(this).attr("data-value")=="desc")
        {
            id_array.sort(function(a, b){return a-b});
            jQuery(this).attr("data-value","asc");
            jQuery(this).removeClass("asc");
            jQuery(this).addClass("desc");
        }
        else
        {
            id_array.sort(function(a, b){return b-a});
            jQuery(this).attr("data-value","desc");
            jQuery(this).removeClass("desc");
            jQuery(this).addClass("asc");
            
        }
        console.log(id_array);
        jQuery(id_array).each(function(index,element){
            if(element!="" && element!=undefined)
            {
                temp_html.push('<div class="'+jQuery(target).find("[data-id='"+element+"']").closest("."+clone_class).attr("class")+'">'+jQuery(target).find("[data-id='"+element+"']").closest("."+clone_class).html()+'</div>');
            }
        });
    }
    else if(jQuery(this).attr("data-sort")=="date")
    {
        var date_array=[];
        if(jQuery(this).attr("data-value")=="desc")
        {
            jQuery(this).attr("data-value","asc");
            jQuery(this).removeClass("desc");
            jQuery(this).addClass("asc");
            jQuery(target.find(".created_at")).sort(function(a,b){
                return new Date(jQuery(a).attr("data-date")) > new Date(jQuery(b).attr("data-date"));
            }).each(function(){
                jQuery(this).each(function(index,element){
                    date_array.push(jQuery(element).attr("data-date"));
                });
            });
        }
        else
        {
            jQuery(this).attr("data-value","desc");
            jQuery(this).removeClass("asc");
            jQuery(this).addClass("desc");
            jQuery(target.find(".created_at")).sort(function(a,b){
                return new Date(jQuery(a).attr("data-date")) < new Date(jQuery(b).attr("data-date"));
            }).each(function(){
                jQuery(this).each(function(index,element){
                    date_array.push(jQuery(element).attr("data-date"));
                });
            });
        }
        console.log(date_array);
        jQuery(date_array).each(function(index,element){
            if(element!="" && element!=undefined)
            {
                temp_html.push('<div class="'+jQuery(target).find("[data-date='"+element+"']").closest("."+clone_class).attr("class")+'">'+jQuery(target).find("[data-date='"+element+"']").closest("."+clone_class).html()+'</div>');
            }
        });
    }
    else
    {
        var amount_array=[];
        jQuery(".content-table-body .content-table-row").each(function(index,element){
            amount_array.push(jQuery(element).find(".value").attr("data-amount"));
        });
        console.log(amount_array);
        if(jQuery(this).attr("data-value")=="desc")
        {
            amount_array.sort(function(a, b){return a-b});
            jQuery(this).attr("data-value","asc");
            jQuery(this).removeClass("desc");
            jQuery(this).addClass("asc");
        }
        else
        {
            amount_array.sort(function(a, b){return b-a});
            jQuery(this).attr("data-value","desc");
            jQuery(this).removeClass("asc");
            jQuery(this).addClass("desc");
        }
        console.log(amount_array);
        jQuery(amount_array).each(function(index,element){
            if(element!="" && element!=undefined)
            {
                temp_html.push('<div class="'+jQuery(target).find("[data-amount='"+element+"']").closest("."+clone_class).attr("class")+'">'+jQuery(target).find("[data-amount='"+element+"']").closest("."+clone_class).html()+'</div>');
                jQuery(target).find("[data-amount='"+element+"']").first().remove();
            }
        });
        console.log(temp_html);
    }
    target=jQuery(document).find("."+jQuery(this).attr("data-clone")+"-sort");
    jQuery(target).each(function(index,element){
        jQuery(element).replaceWith(temp_html[index]);
    });
    jQuery(".showbox-wrapper").hide();
});

//FILTER
jQuery(document).on("change","select[name='filter-by-deal-source'],select[name='filter-by-contact-name'],select[name='filter-by-staff-name'],select[name='filter-by-invoice-status'],select[name='filter-by-estimate-status'],select[name='filter-by-date']",function(){
    // console.log(jQuery("select[name='filter-by-deal-source'] :selected").val());
    // console.log(jQuery("select[name='filter-by-contact-name'] :selected").val());
    // console.log(jQuery("select[name='filter-by-staff-name'] :selected").val());
    // console.log(jQuery("select[name='filter-by-invoice-status'] :selected").val());
    // console.log(jQuery("select[name='filter-by-estimate-status'] :selected").val());
    // console.log(jQuery("select[name='filter-by-date'] :selected").val());
    filter();
});

jQuery(document).on("click",".ui-menu-item-wrapper",function(){
    console.log("comiseo-daterangepicker-triggerbutton");
    console.log(jQuery(document).find(".comiseo-daterangepicker-triggerbutton").text());
});

// var rr = setInterval(cloc, 300);
jQuery(document).on("click",".comiseo-daterangepicker-triggerbutton",function(){
    console.log("comiseo-daterangepicker-triggerbutton");
    // setInterval();
    rr = setInterval(cloc, 300);
    // cloc();
    
});

var str1="Select date range...";

function cloc()
{
    var date_range=jQuery("#drp_autogen0").text();
    var str2=date_range;
    // console.log(str1.indexOf(str2));
    console.log(str2.indexOf(str1));
    console.log("str2 = "+str2);
    console.log("str1 = "+str1);
    // if(str2.indexOf(str1) == -1)
    // {
    //     str1=str2;
    //     console.log("ajax");
    //     clearInterval(rr);
    // }
    // if (str1.indexOf(str2) >= 0 || str2.indexOf(str1) >= 0) //IF BUTTON TEXT IS STR1
    // {
    //     console.log(jQuery("#drp_autogen0").text());
        
    // }
    if(str2.indexOf(str1) == -1)
    {
        console.log("match");
        console.log(jQuery("#drp_autogen0").text());
        var str2=date_range;
        console.log(str1.indexOf(str2));
        console.log(str2.indexOf(str1));
        console.log(date_range);
        str1=str2;
        previousButtonText=date_range;
        filter();
        clearInterval(rr);
    }
    
}

jQuery(".ui-menu-item-wrapper").click(function(){
    console.log("ui-menu-item-wrapper");
});

function filter()
{
    var deal_source=jQuery("select[name='filter-by-deal-source'] :selected").val();
    var contact_name=jQuery("select[name='filter-by-contact-name'] :selected").val();
    var staff_name=jQuery("select[name='filter-by-staff-name'] :selected").val();
    var invoice_status=jQuery("select[name='filter-by-invoice-status'] :selected").val();
    var estimate_status=jQuery("select[name='filter-by-estimate-status'] :selected").val();
    var date_range=jQuery("#drp_autogen0").text();
    var str1="Select date range...";
    var str2=date_range;
    if (str1.indexOf(str2) >= 0 || str2.indexOf(str1) >= 0)
    {
        date_range="";
    }
    else
    {
        date_range=getDateRange(date_range);
    }
    console.log(date_range);
    var filter=[deal_source,contact_name,staff_name,invoice_status,estimate_status,date_range];
    var table=['deals.source','deals.contact','deals.owner','invoices','estimates','created_at'];
    console.log(filter);
    console.log(table);
    jQuery.ajax({
        url:APP_URL+'/generate-reports',
        dataType:'json',
        data:{'_token':jQuery("input[name='_token']").val(),filter:filter,table:table},
        type:'post',
        success:function(data){
            console.log(data);
            console.log(data.owner);
            console.log(findId(1,data.owner));
            console.log("response length = "+data.response.length);
            if(data.response.length>0)
            {
                var deals=data.response;
                jQuery(".clone:not(:first)").remove();
                jQuery(deals).each(function(index,element){
                    jQuery(".clone:first").clone().appendTo(".filter-data");
                    console.log(parseInt(index)+parseInt(1));
                    var count=parseInt(index)+1;
                    var owner=findId(element.owner,data.owner);
                    var status=findId(element.status,data.status);
                    var contact=findId(element.contact,data.contact);
                    var target=jQuery(".clone:eq("+count+")");
                    target.find(".created_at").html(element.created_at);
                    target.find(".deal").html(element.title);
                    target.find(".owner").html(owner.first_name+" "+owner.last_name);
                    target.find(".status").html(status.status);
                    target.find(".value").html(element.value);
                    target.find(".name").html(contact.first_name+" "+contact.last_name);
                });
                jQuery(".content-table-body").removeClass("demo-content");
                jQuery('.content-table-body').find('.content-table-row:lt(4)').remove();
                jQuery(".clone:not(:first)").show();
            }
            else
            {
                jQuery(".clone:not(:first)").remove();
            }
        },
    });
}

function getDateRange(dateRange)
{
    var dateRangeArray=dateRange.split(" - ");
    var startDateArray=dateRangeArray[0].split(" ");
    var endDate="";
    if(dateRangeArray[1]!=undefined)
    {
        var endDateArray=dateRangeArray[1].split(" ");
        endDate=getStructuredDateMonthYear(endDateArray);
    }
    console.log(startDateArray);
    var startDate=getStructuredDateMonthYear(startDateArray);
    var dateRange=[startDate,endDate];
    return dateRange;
}

function getStructuredDateMonthYear(dateArray)
{
    var date=dateArray[1];
    date=date.replace(',','');
    if(date<10)
    {
        date='0'+date;
    }
    var month=dateArray[0];
    var monthNameArray=[{'Jan':'01'},{'Feb':'02'},{'Mar':'03'},{'Apr':'04'},{'May':'05'},{'Jun':'06'},{'Jul':'07'},{'Aug':'08'},{'Sep':'09'},{'Oct':'10'},{'Nov':'11'},{'Dec':'12'}];
    var result = getFields(monthNameArray,month);
    var year=dateArray[2];
    var dateRange=year+"-"+result[0]+"-"+date;
    return dateRange;
}

function getFields(input, field) {
    var output = [];
    for (var i=0; i < input.length ; ++i)
        if(input[i][field]!=undefined)
        {
            output.push(input[i][field]);
        }
    return output;
}

var findId = function(purposeName,purposeObjects) {
    console.log(purposeObjects);
    for (var i = 0, len = purposeObjects.length; i < len; i++) {
        if (purposeObjects[i].id === purposeName)
            return purposeObjects[i]; // Return as soon as the object is found
    }
    return null; // The object was not found
}

jQuery(document).on("change","select[name='filter-by-invoice-status']",function(){
    console.log("disabled");
    jQuery("select[name='filter-by-estimate-status']").attr("disabled","disabled");
    jQuery("select[name='filter-by-estimate-status']").material_select();
});

jQuery(document).on("change","select[name='filter-by-estimate-status']",function(){
    console.log("disabled");
    jQuery("select[name='filter-by-invoice-status']").attr("disabled","disabled");
    jQuery("select[name='filter-by-invoice-status']").material_select();
});

//FILTER EXPORT
jQuery("button[name='filter_export']").click(function(){
    if(jQuery(".filter-data").find(".clone").length>1)
    {
        var exportFilterDataArray=[];
        jQuery(".filter-data .clone").each(function(index,element){
            if(index>0)
            {
                tempArray=[];
                jQuery(element).find(".content-table-data").each(function(index2,element2){
                    tempArray.push(jQuery(element2).text());
                });
                exportFilterDataArray.push(tempArray);
            }
        });
        console.log(exportFilterDataArray);
        exportFilterData(exportFilterDataArray,jQuery(this).attr("data-export"));
    }
});

function exportFilterData(exportFilterDataArray,exportAs)
{
    console.log(exportAs);
    jQuery("#filterTable").attr("action",jQuery("#filterTable").attr("data-action")+'/'+exportAs);
    jQuery.ajax({
        url:APP_URL+'/create-report',
        dataType:'json',
        data:{'_token':jQuery("input[name='_token']").val(),exportFilterDataArray:exportFilterDataArray},
        type:'post',
        success:function(data){
            console.log(data);
            jQuery("#filterTable").find("#filterData").val(exportFilterDataArray);
            jQuery("#filterTable").find("button").trigger("click");
        }
    });
}

jQuery("a[href='#import-contact']").click(function(){
    jQuery("#progress-bar").width("0%");
    jQuery(".import-contact-message").text("");
});

jQuery("#contact").change(function(){
    jQuery('#uploadForm').trigger("submit");
});

$('#uploadForm').submit(function(e) {   
        e.preventDefault();
        $('#loader-icon').show();
        var fileExtension = ['csv', 'xls', 'xlsx'];
        if ($.inArray($("#contact").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            console.log("Only formats are allowed : "+fileExtension.join(', '));
            jQuery(".import-contact-message").text("Only formats are allowed : "+fileExtension.join(', '));
            return false;
        }
        $(this).ajaxSubmit({ 
            target:   '#targetLayer', 
            dataType: 'json',
            beforeSubmit: function() {
              $("#progress-bar").width('0%');
            },
            uploadProgress: function (event, position, total, percentComplete){ 
                $("#progress-bar").width(percentComplete + '%');
                $("#progress-bar").html('<div id="progress-status">' + percentComplete +' %</div>')
            },
            success: function (response){
                console.log(response);
                console.log("fgddfgd");
                jQuery(".import-contact-message").text("File Uploaded");
                jQuery(".verifying").text("Veifying");
                if(response.contact_imported == true)
                {
                    setTimeout(function(){ 
                        jQuery(".verifying").text("Contact Imported");
                        setTimeout(function(){ 
                            location.reload(); 
                        }, 2000); 
                    }, 2000);
                }
                else
                {
                    setTimeout(function(){ 
                        jQuery(".verifying").text("Contact Can Not Be Imported");
                    }, 2000);
                }
                $('#loader-icon').hide();
            },
            error: function(response){
                console.log("failed");
                console.log(response);
            },
            resetForm: true 
        }); 
        // jQuery.ajax({
        //     url:APP_URL+'/import/contact',
        //     data:new FormData($("#uploadForm")[0]),
        //     dataType:'json',
        //     async:true,
        //     type:'post',
        //     processData: false,
        //     contentType: false,
        //     target:   '#targetLayer', 
        //     beforeSubmit: function() {
        //       $("#progress-bar").width('0%');
        //     },
        //     uploadProgress: function (event, position, total, percentComplete){ 
        //         $("#progress-bar").width(percentComplete + '%');
        //         $("#progress-bar").html('<div id="progress-status">' + percentComplete +' %</div>')
        //     },
        //     // success:function (response){
        //     //     console.log(response);
        //     //     console.log("fgddfgd");
        //     //     $('#loader-icon').hide();
        //     // },
        //     // error: function(response){
        //     //     console.log("failed");
        //     //     console.log(response);
        //     // },
        //     success:function(response){
        //         console.log("success");
        //         console.log(response);
        //     },
        //     error: function(response){
        //         console.log("failed");
        //         console.log(response);
        //     },
        // });
        return false; 
});

//CHANGE PRODUCT DETAILS ON TYPE CHANGE
jQuery(document).on("change","select[name='type']",function(){
    if(jQuery(this).find(":selected").attr("description") != undefined)
    {
        jQuery("input[name='description']").val(jQuery(this).find(":selected").attr("description"));
    }
    else
    {
        jQuery("input[name='description']").val("");
    }
    if(jQuery(this).find(":selected").attr("room-fare") != undefined)
    {
        jQuery("input[name='price']").val(jQuery(this).find(":selected").attr("room-fare"));
    }
    else
    {
        jQuery("input[name='price']").val("");
    }
    if(jQuery(this).find(":selected").attr("per-person-price") != undefined)
    {
        jQuery("input[name='per_person_price']").val(jQuery(this).find(":selected").attr("per-person-price"));
    }
    else
    {
        jQuery("input[name='per_person_price']").val("");
    }
});

//SHOW CALL ACTIVITY LOG
jQuery(document).on("click",".show-call-activity-log",function(){
    var phone_id = jQuery(this).attr("data-phone");
    jQuery.ajax({
        url:APP_URL+'/getCallActivityLog',
        data:{phone_id:phone_id},
        dataType:'json',
        type:'get',
        success:function(data){
          console.log(data);
          jQuery(".phone-contact-phone-no").text(data.phone_contact.number);
          jQuery(".phone-contact-phone-name").text(data.phone_contact.contact_name);
          var count = 0;
          var activity_log_html = "<tr><td>Date</td><td>Time</td></tr>";
          while(count < data.response.length)
          {
            activity_log_html = activity_log_html + '<tr><td>' + data.response[count].date.split("at")[0] + '</td><td>' + data.response[count].date.split("at")[1] + '</td></tr>';
            count++;
          }
          jQuery(".phone-contact-call-activity-log").html(activity_log_html);
        }
    });
});