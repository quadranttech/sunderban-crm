
//EMAIL CLONE
jQuery(".addAnotherContactEmail").click(function(){
	jQuery(".contact-email-clone").last().clone().appendTo(".contact-email");
	jQuery(".contact-email-clone").last().find("input").val("");
	jQuery(".contact-email-clone").last().find("label").removeClass("active");
	jQuery(".contact-email-clone:last").find(".addAnotherContactEmail").removeClass("add-input").addClass("remove-input remove-contact-email");
	jQuery(".contact-email-clone:last").find(".remove-input").removeClass("addAnotherContactEmail").html("<span></span>");
	jQuery(".contact-email-clone:last").find("select[name='email_purpose[]']").material_select();
	jQuery(".contact-email-clone:last").find("input.select-dropdown:first").remove();
});


//REMOVE EMAIL CLONE
jQuery('body').on('click','.remove-contact-email',function() {
	jQuery(this).closest(".contact-email-clone").remove();
});

//PHONE CLONE
jQuery(".add-another-contact-phone").click(function(){
	jQuery(".contact-phone-clone").last().clone().appendTo(".contact-phone");
	jQuery(".contact-phone-clone").last().find("input").val("");
	jQuery(".contact-phone-clone").last().find("label").removeClass("active");
	jQuery(".contact-phone-clone:last").find(".add-another-contact-phone").removeClass("add-input").addClass("remove-input remove-contact-phone");
	jQuery(".contact-phone-clone:last").find(".remove-input").removeClass("add-another-contact-phone").html("<span></span>");
	jQuery(".contact-phone-clone:last").find("select[name='phone_purpose[]']").material_select();
	jQuery(".contact-phone-clone:last").find("input.select-dropdown:first").remove();
});

//ADDRESS CLONE
jQuery(".add-anothe-contact-address").click(function(){
	jQuery(".address-clone").last().clone().appendTo(".address-information");
	jQuery(".address-clone").last().find("input").val("");
	jQuery(".address-clone").last().find("label").removeClass("active");
	jQuery(".address-clone:last").find(".add-anothe-contact-address").removeClass("add-input").addClass("remove-input remove-contact-address");
	jQuery(".address-clone:last").find("a.remove-contact-address").removeClass("add-anothe-contact-address").html("<span></span>");
	var header_5=jQuery(".address-clone:last").find("h5").attr("data-address");
	header_5=parseInt(header_5)+1;
	header_5_text="Address "+header_5;
	jQuery(".address-clone:last").find("h5").attr("data-address",header_5).text(header_5_text);
	jQuery('.address-clone:last').find("select").material_select();
	jQuery('.address-clone:last').find("input[value='Country']:first").remove();
	jQuery('.address-clone:last').find("input.select-dropdown:first").remove();
});

//REMOVE ADDRESS CLONE
jQuery('body').on('click','.remove-contact-address',function(){
	jQuery(this).closest(".address-clone").remove();
});

//ADD IM PROFILE CLONE
jQuery(".add-another-contact-im-profile").click(function(){
	jQuery(".im-profile-clone").last().clone().appendTo(".im-profile");
	jQuery(".im-profile-clone").last().find("input").val("");
	jQuery(".im-profile-clone").last().find("label").removeClass("active");
	jQuery(".im-profile-clone:last").find(".add-another-contact-im-profile").removeClass("add-input").addClass("remove-input remove-contact-im-profile");
	jQuery(".im-profile-clone:last").find(".remove-contact-im-profile").removeClass("add-another-contact-im-profile").html("<span></span>");
	jQuery(".im-profile-clone:last").find("select[name='imProfile[]']").material_select();
	jQuery(".im-profile-clone:last").find("input.select-dropdown:first").remove();
});

//REMOVE IM PROFILE CLONE
jQuery('body').on('click','.remove-contact-im-profile',function(){
	jQuery(this).closest(".im-profile-clone").remove();
});

//REMOVE PHONE CLONE
jQuery('body').on('click','.remove-contact-phone',function() {
	jQuery(this).closest(".contact-phone-clone").remove();
});

//ADD SOCIAL PROFILE
jQuery(".add-another-social-profile").click(function(){
	jQuery(".social-profile-clone").last().clone().appendTo(".social-profile");
	jQuery(".social-profile-clone").last().find("input").val("");
	jQuery(".social-profile-clone").last().find("label").removeClass("active");
	jQuery(".social-profile-clone:last").find(".add-another-social-profile").removeClass("add-input").addClass("remove-input remove-social-profile");
	jQuery(".social-profile-clone:last").find(".remove-social-profile").removeClass("add-another-social-profile").html("<span></span>");
	jQuery(".social-profile-clone:last").find("select[name='social_profile[]']").material_select();
	jQuery(".social-profile-clone:last").find("input.select-dropdown:first").remove();
});

//REMOVE SOCIAL PROFILE
jQuery('body').on('click','.remove-social-profile',function(){
	jQuery(this).closest(".social-profile-clone").remove();
});

jQuery("#addContactButton").click(function(){
	jQuery("#contact-form").trigger("submit");
});

//20-02-2017
jQuery("#searchContact").keyup(function(){
	var ajax_url=APP_URL+'/ajax/getContactByName';
	var contact_table_row_html=jQuery(".contactTable .content-table-row:first").html();
	jQuery.ajax({
		url: ajax_url,
	    type: 'get',
	    data: {contact: jQuery(this).val()},
	    dataType: 'JSON',
	    success: function (data) {
	    	if(data.response!="")
	    	{
	    		jQuery(".contactTable").html('<div class="content-table-row">'+contact_table_row_html+'</div>');
	    		jQuery(".contactTable").find(".contactFirstChar").html(data.response.first_name.charAt(0).toUpperCase());
	    		jQuery(".contactTable").find(".contacFirstName").html(data.response.first_name);
	    		jQuery(".contactTable").find(".contactLastName").html(data.response.last_name);
	    		jQuery(".contactTable").find(".contactEmail").html(data.response.email);
	    		jQuery(".contactTable").find(".contactPhone").html(data.response.phone);
	    		jQuery(".contactTable").find(".contactAddressWorkHome").html(data.response.address_work_home);
	    		jQuery(".contactTable").find(".contactStreetAddress").html(data.response.street_address);
	    		jQuery(".contactTable").find(".contactLeadStage").html(data.response.lead_stage);
	    		jQuery(".contactTable").find(".contactLeadSource").html(data.response.lead_source);
	    		jQuery(".contactTable").find(".contactMore span a:eq(0)").attr("href",APP_URL+'/edit-contact/'+data.response.id);
	    		jQuery(".contactTable").find(".contactMore span a:eq(1)").attr("data-task-id",data.response.id);
	    	}
	    }
	});
});

//21-02-2017
//CONTACT SELECT ON CLICK
jQuery(".contactFirstCharParent").click(function(){
	jQuery(this).closest(".content-table-row").toggleClass("selected");
});

//CHECK ALL CONTACT
jQuery("input#filled-in-box").click(function(){
	jQuery(".contactTable .content-table-row").toggleClass("selected");
});

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

//EDIT CONTACT
jQuery(".add-extra-phone-no").click(function(){
	jQuery(".phone-no-clone").last().clone().appendTo(".phone-no-clone-parent");
	jQuery(".phone-no-clone").last().find("input").val("");
	jQuery(".phone-no-clone").last().find("label").removeClass("active");
	jQuery(".phone-no-clone").find("select[name='edit_phone_purpose[]']").material_select();
	jQuery(".phone-no-clone").last().find("span.caret , input:first").remove();
	jQuery(".phone-no-clone").last().find("a.add-extra-phone-no").removeClass("add-more").addClass("remove-extra-phone-no remove-item").removeClass("add-extra-phone-no");
});

jQuery(".add-extra-email-no").click(function(){
	jQuery(".email-no-clone").last().clone().appendTo(".email-no-clone-parent");
	jQuery(".email-no-clone").last().find("input").val("");
	jQuery(".email-no-clone").last().find("label").removeClass("active");
	jQuery(".email-no-clone").find("select[name='edit_email_purpose[]']").material_select();
	jQuery(".email-no-clone").last().find("span.caret , input:first").remove();
	jQuery(".email-no-clone").last().find("a.add-extra-email-no").removeClass("add-more").addClass("remove-extra-email-no remove-item").removeClass("add-extra-email-no");
});

jQuery(".add-extra-address").click(function(){
	jQuery(".edit-address-clone").last().clone().appendTo(".edit-address-clone-parent");
	jQuery(".edit-address-clone").last().find("input").val("");
	jQuery(".edit-address-clone").last().find("label").removeClass("active");
	jQuery(".edit-address-clone:last").find("select").material_select();
	// jQuery(".edit-address-clone").last().find("span.caret , input:first").remove();
	jQuery(".edit-address-clone").last().find("input[name='address_array[]']").val('null');
	jQuery(".edit-address-clone:last").find("span.caret , input.select-dropdown:first , .country input.select-dropdown:first").remove();
	jQuery(".edit-address-clone").last().find(".add-extra-address").removeClass("add-more").addClass("remove-address remove-extra-address remove-item").removeClass("add-extra-address");
});

//24-02-2017
jQuery(".add-extra-im-profile").click(function(){
	jQuery(".add-extra-im-profile-clone").last().clone().appendTo(".add-extra-im-profile-parent");
	jQuery(".add-extra-im-profile-clone").last().find("input").val("");
	jQuery(".add-extra-im-profile-clone").last().find("label").removeClass("active");
	jQuery(".add-extra-im-profile-parent").find("select").material_select();
	jQuery(".add-extra-im-profile-clone").last().find("span.caret , input.select-dropdown:first").remove();
	jQuery(".add-extra-im-profile-clone:last").find("input[name='im_profile_id_array[]']").val('null');
	jQuery(".add-extra-im-profile-clone:last").find(".add-extra-im-profile").removeClass("add-extra-im-profile add-more").addClass("remove-item");
});

jQuery(document).on("click",".add-extra-social-profile",function(){
	jQuery(".add-extra-social-profile-clone").last().clone().appendTo(".add-extra-social-profile-parent");
	jQuery(".add-extra-social-profile-clone").last().find("input").val("");
	jQuery(".add-extra-social-profile-clone").last().find("label").removeClass("active");
	jQuery(".add-extra-social-profile-parent").find("select").material_select();
	jQuery(".add-extra-social-profile-clone:last").find("input[name='social_profile_id_array[]']").val('null');
	jQuery(".add-extra-social-profile-clone").last().find("span.caret , input.select-dropdown:first").remove();
	jQuery(".add-extra-social-profile-clone").last().find(".add-extra-social-profile").removeClass("add-extra-social-profile add-more").addClass("remove-item");
});

jQuery(document).on("click",".remove-item",function(){
	jQuery(this).closest(".parent-div").remove();
});

//CLOSE PROFILE
jQuery(".username").click(function(){
	closeProfile();
});

jQuery(document).on("click","body > div",function(event) {
	if(jQuery(event.target).closest(".username").length==0 && jQuery(event.target).closest(".edit-tasks").length==0)
	{
		// closeProfile();
		jQuery(".profile-usermenu").removeClass("menu-open");
		jQuery(".profile-usermenu").addClass("menu-close");
		jQuery(".profile-usermenu").hide();
	}
});

function closeProfile()
{
	if(jQuery(".profile-usermenu").hasClass("menu-close"))
	{
		jQuery(".profile-usermenu").removeClass("menu-close");
		jQuery(".profile-usermenu").addClass("menu-open");
		jQuery(".profile-usermenu").show();
	}
	else
	{
		jQuery(".profile-usermenu").removeClass("menu-open");
		jQuery(".profile-usermenu").addClass("menu-close");
		jQuery(".profile-usermenu").hide();
	}
}

jQuery("select[name='contact-company']").change(function(){
	if(jQuery(this).val()=="select-company")
	{
		jQuery(".add-contact-company").show();
	}
	else
	{
		jQuery(".add-contact-company").hide();
	}
});

//START LOADER
function startLoader(target)
{
	target.html("Saving <span class='spinner'></span> <span class='checkmark'></span>");
	target.addClass("saving");
}

//STOP LOADER
function stopLoader(target)
{
	target.find(".spinner").remove();
    target.addClass("saved");
    setTimeout(function(){
        target.removeClass("saved");
        target.html("Save");
        target.removeClass("saving");
    }, 3000);
}

jQuery(".back-to-contact-grid").click(function(event){
	event.preventDefault();
	window.location.replace(APP_URL+'/contacts');
});

//CANCEL COMPANY MODAL
jQuery(".cancel-modal").click(function(event){
	event.preventDefault();
	jQuery(".modal-close").parents(".modal").find("form")[0].reset();
	jQuery(".modal-close").trigger("click");
});

//CREATE COMPANY
jQuery(".create-compnay").click(function(){
	var company_name=jQuery("#company-name").val();
	var addresstype=jQuery("select[name='addresstype']").val();
	var streetaddress=jQuery("#streetaddress").val();
	var city=jQuery("#city").val();
	var state=jQuery("#state").val();
	var country=jQuery("select[name='country']").val();
	var postcode=jQuery("#postcode").val();
	if(checkValidation(jQuery("#company-name"))==false || checkValidation(jQuery("#streetaddress"))==false)
	{
		return false;
	}
	startLoader(jQuery(".create-compnay"));
	jQuery.ajax({
      url: APP_URL+'/create-company',
      type: 'get',
      data: {'company_name':company_name,'addresstype':addresstype,'streetaddress':streetaddress,'city':city,'state':state,'country':country,'postcode':postcode},
      dataType: 'json',
      success: function (data) {
        if(data.response)
        {
          stopLoader(jQuery(".create-compnay"));
          jQuery(".modal-close").trigger("click");
        }
      }
    });
});

//INCLUDE TASK JS
jQuery.getScript(APP_URL+"/assets/js/task.js");

//INCLUDE DEAL JS
jQuery.getScript(APP_URL+"/assets/js/deals.js");

//INCLUDE DOCUMENT JS
jQuery.getScript(APP_URL+"/assets/js/document.js");

//INCLUDE HISTORY JS
jQuery.getScript(APP_URL+"/assets/js/history.js");

//INCLUDE NOTE JS
jQuery.getScript(APP_URL+"/assets/js/note.js");

//INCLUDE FEED JS
jQuery.getScript(APP_URL+"/assets/js/feed.js");

//INCLUDE SETTINGS JS
jQuery.getScript(APP_URL+"/assets/js/settings.js");

//INCLUDE VALIDATION JS
jQuery.getScript(APP_URL+"/assets/js/validation.js");

//INCLUDE INVOICE JS
jQuery.getScript(APP_URL+"/assets/js/invoice.js");





