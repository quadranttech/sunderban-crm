jQuery("input[name='add-attachment']").change(function (element){
});

jQuery(document).on("click","#add-note",function(){
	jQuery("#first_name").val("");
	jQuery("#textarea1").val("");
	jQuery(".attachment").closest(".form-layout-100").hide();
	var contact_id=jQuery("input[name='contact_id']").val();
	if(contact_id=="" || contact_id==undefined)
	{
		contact_id=0;
	}
	var deal_id=jQuery("input[name='deal_id']").val();
	if(deal_id=="" || deal_id==undefined)
	{
		deal_id=0;
	}
	if(contact_id==0)
	{
		var feed_type="deal";
	}
	else
	{
		var feed_type="contact";
	}
	var ajax_url=APP_URL+'/ajax/create-note';
	jQuery.ajax({
		url:APP_URL+'/ajax/create-note',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'contact_id':contact_id,'deal_id':deal_id,'feed_type':feed_type},
      	type:'post',
      	success:function(data){
        	if(data.response!=0)
        	{
        		jQuery("input[name='current_note_id']").val(data.response);
        		if(jQuery(".all-notes:last li.note-li").length>=4)
        		{
        			var newNoteLiHtml='<ul class="all-notes">';
        			newNoteLiHtml=newNoteLiHtml+data.content;
        			newNoteLiHtml=newNoteLiHtml+'</ul>';
        			jQuery(newNoteLiHtml).insertAfter(jQuery(".all-notes").last());
        		}
        		else
        		{
        			var newNoteLiHtml=data.content;
        			jQuery(newNoteLiHtml).append(jQuery(".all-notes").last());
        		}
        		jQuery(".note-li:last").find("h4").html('untitled');
        		jQuery(".note-li:last").attr("note_id",data.response);
        	}
      	},
	});
});

jQuery(".save-note").click(function(){
	jQuery(".showbox").show();
	startLoader(jQuery(".save-note"));
	// if(jQuery("input[name='add-attachment']").val()!="")
	// {
	// 	jQuery.ajax({
	//    	  url:APP_URL+'/ajax/attachment_upload',
	//       data:new FormData($("#add-note-att")[0]),
	//       dataType:'json',
	//       async:true,
	//       type:'post',
	//       processData: false,
	//       contentType: false,
	//       success:function(response){
	//       },
	    
	//    });
	// }
	jQuery.ajax({
		url:APP_URL+'/ajax/edit-note',
      	dataType:'json',
      	// async:false,
      	data:{'_token':jQuery("input[name='_token']").val(),'note_title':jQuery("#first_name").val(),'note':jQuery("#textarea1").val(),'note_id':jQuery("input[name='current_note_id']").val(),'contact_id':jQuery("input[name='contact_id']").val()},
      	type:'post',
      	success:function(data){
        	stopLoader(jQuery(".save-note"));
        	setTimeout(function(){ 
        		jQuery(".contexual-note-cancel").trigger("click");; 
        	}, 1000);
        	jQuery("li.note-li[note-id='"+jQuery("input[name='contact_id']").val()+"']").find("h4").html(jQuery("#first_name").val());
      	},
	});
	jQuery(".showbox").hide();
});

jQuery(".contexual-note-cancel").click(function(){
	jQuery("#show-note").trigger("click");
	jQuery("#show-deal-note").trigger("click");
});

jQuery("#show-note").click(function(){
	history.pushState('obj', 'note', APP_URL+'/edit-contact/'+window.location.href.split("edit-contact/").pop().substr(0, window.location.href.split("edit-contact/").pop().indexOf('?'))+'?page='+'note');
	jQuery(".showbox-wrapper").show();
	jQuery(".all-notes").remove();
	jQuery(".note-add").remove();
	jQuery.ajax({
		url:'show-note',
	    data:{'_token':jQuery("input[name='_token']").val(),contact_id:jQuery("input[name='contact_id']").val()},
	    dataType:'json',
	    type:'post',
	    success:function(data){
	      jQuery(".showbox-wrapper").hide();
	      if(data.status=="success")
	      {
	      	var count=0;
		    var innerCount=1;
		    var noteLiHtml='<ul class="all-notes">';
		    while(count<data.response.length)
		    {
		      	noteLiHtml=noteLiHtml+data.content;
		      	if(innerCount%4==0)
		      	{
		      		noteLiHtml=noteLiHtml+'</ul><ul class="all-notes">';
		      	}
		      	count++;
		      	innerCount++;
		    }
		    noteLiHtml=noteLiHtml+'</ul><div class="notes note-add"><span class=""><a class="btn-floating btn-large waves-effect waves-light indigo" id="add-note"><i class="material-icons">add</i></a><p>Create New</p></span></div>';
		    jQuery("#tabs-4").find(".tabs-wrapper .note-lists").html(noteLiHtml);
		    jQuery("body li.note-li").each(function(index,element){
	     	jQuery(element).find("h4").html(data.response[index].note_title);
	      	jQuery(element).attr("note-id",data.response[index].id);
	      	jQuery(element).find(".notes").addClass("note-"+data.response[index].color);
		      	if(data.userImage[data.response[index].id]!="" && data.userImage[data.response[index].id]!=null)
		      	{
		      		jQuery(element).find(".note-author img").attr("src",APP_URL+'/uploads/profile-image/'+data.userImage[data.response[index].id]);
		      	}
		      	else
		      	{
		      		jQuery(element).find(".note-author img").attr("src",APP_URL+'/assets/img/images.jpg');
		      	}
		    });
	      }
	      else
	      {
	      	jQuery("#tabs-4").find(".tabs-wrapper .note-lists").html('<div class="notes note-add"><span class=""><a class="btn-floating btn-large waves-effect waves-light indigo" id="add-note"><i class="material-icons">add</i></a><p>Create New</p></span>');
	      }
	      jQuery(".notes-form").hide();
		  jQuery(".note-lists").show();
	    },
	});
});

jQuery(document).on("click",".note-lists .note-delete",function(){
	var note_id=jQuery(this).parents(".note-li").attr("note-id");
	jQuery.ajax({
		url:'delete-note',
	    data:{'_token':jQuery("input[name='_token']").val(),note_id:jQuery(this).parents(".note-li").attr("note-id")},
	    dataType:'json',
	    type:'post',
	    success:function(data){
	      if(data.response!=0)
	      {
	      	jQuery(".all-notes li[note-id='"+note_id+"']").remove();
	      }
	    },
	});
});

jQuery("#show-deal-note").click(function(){
	jQuery(".all-notes").remove();
	jQuery(".note-add").remove();
	var href=jQuery(this).attr("href");
	jQuery(".showbox-wrapper").show();
	jQuery.ajax({
		url:'show-deal-note',
	    data:{'_token':jQuery("input[name='_token']").val(),deal_id:jQuery("input[name='deal_id']").val()},
	    dataType:'json',
	    type:'post',
	    success:function(data){
	      if(data.status=="success")
	      {
	      	  var count=0;
		      var innerCount=1;
		      var noteLiHtml='<ul class="all-notes">';
		      while(count<data.response.length)
		      {
		      	noteLiHtml=noteLiHtml+data.content;
		      	if(innerCount%4==0)
		      	{
		      		noteLiHtml=noteLiHtml+'</ul><ul class="all-notes">';
		      	}
		      	count++;
		      	innerCount++;
		      }
		      noteLiHtml=noteLiHtml+'</ul><div class="notes note-add"><span class=""><a class="btn-floating btn-large waves-effect waves-light indigo" id="add-note"><i class="material-icons">add</i></a><p>Create New</p></span></div>';
		      jQuery("#tabs-5").find(".tabs-wrapper .note-lists").html(noteLiHtml);
		      jQuery("body li.note-li").each(function(index,element){
		      	jQuery(element).find("h4").html(data.response[index].note_title);
		      	jQuery(element).attr("note-id",data.response[index].id);
		      	jQuery(element).find(".notes").addClass("note-"+data.response[index].color);
		      	if(data.userImage[data.response[index].id]!="" && data.userImage[data.response[index].id]!=null)
		      	{
		      		jQuery(element).find(".note-author img").attr("src",APP_URL+'/uploads/profile-image/'+data.userImage[data.response[index].id]);
		      	}
		      	else
		      	{
		      		jQuery(element).find(".note-author img").attr("src",APP_URL+'/assets/img/images.jpg');
		      	}
		      });
	      }
	      else
	      {
	      	jQuery("#tabs-5").find(".tabs-wrapper .note-lists").html('<div class="notes note-add"><span class=""><a class="btn-floating btn-large waves-effect waves-light indigo" id="add-note"><i class="material-icons">add</i></a><p>Create New</p></span>');
	      }
	      jQuery(".showbox-wrapper").hide();
	    },
	});
});