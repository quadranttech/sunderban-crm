jQuery("input[name='document-upload']").change(function (element){
   var fileName = element.target.value.split('\\').pop();
   var fd = new FormData($("#upload-document")[0]);
   var input=document.getElementsByName("document-upload");
   jQuery(".showbox-wrapper").show();
   jQuery.ajax({
   	url:APP_URL+'/ajax/document_upload',
      data:new FormData($("#upload-document")[0]),
      dataType:'json',
      async:true,
      type:'post',
      processData: false,
      contentType: false,
      success:function(response){
        console.log(response);
        var li_html='';
        li_html=li_html+'<li class="document-li"><a class="example-image-link" href="javascript:void(0);" data-featherlight="image"><div class="document-area"><div class="document-wrapper"><span>';
        if(response.file_type=="jpg" || response.file_type=="png" || response.file_type=="jpeg")
        {
          li_html=li_html+'<img src="'+APP_URL+'/uploads/document/'+response.response+'">';
          li_html=li_html+'</span></div><div class="desc">Tulips-31825.jpg</div></div></a><ul class="document-actions"><li class="document-expand"><a href="'+APP_URL+'/uploads/document/'+response.response+'" data-featherlight="image"></a></li><li class="document-downld"><a href="'+APP_URL+'/uploads/document/'+response.response+'" download></a></li><li class="document-remove" data-id="'+response.deal_id+'"></li></ul></li>';
          jQuery(li_html).insertAfter(".all-documents li:first");
          jQuery(".all-documents:first li.document-li:first .example-image-link").attr("href",APP_URL+'/uploads/document/'+response.response);
        }
        else
        {
          li_html=li_html+'<img src="'+APP_URL+'/assets/img/doc.png">';
          li_html=li_html+'</span></div><div class="desc">Tulips-31825.jpg</div></div></a><ul class="document-actions"><li class="document-expand"><a href="'+APP_URL+'/uploads/document/'+response.response+'" data-featherlight="image"></a></li><li class="document-downld"><a href="'+APP_URL+'/uploads/document/'+response.response+'" download></a></li><li class="document-remove" data-id="'+response.deal_id+'"></li></ul></li>';
          jQuery(li_html).insertAfter(".all-documents li:first");
          jQuery(".all-documents:first li.document-li:first .example-image-link").attr("href",APP_URL+'/assets/img/doc.png');
        }
        jQuery(".all-documents:first li.document-li:first .desc").text(response.response);
        jQuery("input[name='document-upload']").val("")
        jQuery(".showbox-wrapper").hide();
      },
    
   });
});

jQuery("#show-document").click(function(){
  jQuery("#tabs-3").find(".document-li").remove();
  history.pushState('obj', 'document', APP_URL+'/edit-contact/'+window.location.href.split("edit-contact/").pop().substr(0, window.location.href.split("edit-contact/").pop().indexOf('?'))+'?page='+'document');
  jQuery(".showbox-wrapper").show();
	var documentLiHtml=
	jQuery.ajax({
		url:APP_URL+'/ajax/getDocumentByContactId',
      	dataType:'json',
        data:{'_token':jQuery("input[name='_token']").val(),contact_id:jQuery("input[name='contact_id']").val()},
      	type:'get',
      	ontentType: false,
      	success:function(data){
          jQuery(".showbox-wrapper").hide();
        	if(data.status=="success")
        	{
        		var count=0;
            var documentLiHtml='';
            var innerCount=1;
        		while(count<data.response.length)
        		{
              documentLiHtml=documentLiHtml+data.content;
        			count++;
              innerCount++;
        		}
            jQuery("#tabs-3").find(".tabs-wrapper .all-documents").append(documentLiHtml);
            jQuery("body li.document-li").each(function(index,element){
              jQuery(element).find(".desc").html(data.response[index].file_name);
              if(data.response[index].file_type=="jpg" || data.response[index].file_type=="png" || data.response[index].file_type=="jpeg")
              {
                var image=APP_URL+'/'+data.response[index].document_path+data.response[index].file_name;
              }
              else
              {
                var image=APP_URL+'/assets/img/doc.png';
              }
              jQuery(element).find(".example-image-link").attr("href",image);
              jQuery(element).find("img").attr("src",image);
              jQuery(element).find(".document-remove").attr("data-id",data.response[index].id);
              jQuery(element).find(".document-expand").attr("href",APP_URL+'/uploads/document/'+data.response[index].file_name);
            });
        	}
      	},
	});
});

//DELETE DOCUMENT
jQuery(document).on("click","div.document .document-remove",function(){
  var data_id=jQuery(this).attr("data-id");
  jQuery.ajax({
    url:APP_URL+'/delete-document',
      data:{'_token':jQuery("input[name='_token']").val(),'data_id':jQuery(this).attr("data-id")},
      dataType:'json',
      type:'post',
      success:function(response){
        jQuery("li.document-remove[data-id='"+data_id+"']").closest("li.document-li").remove();
      },
    
   });
});

//GET DEAL DOCUMENT
jQuery("#show-deal-document").click(function(){
  jQuery(".showbox-wrapper").show();
  jQuery("#upload-document").hide();
  jQuery("#tabs-4").find(".document-li").remove();
  jQuery.ajax({
    url:'show-deal-document',
      data:{'_token':jQuery("input[name='_token']").val(),'deal_id':jQuery("input[name='deal_id']").val()},
      dataType:'json',
      type:'post',
      success:function(data){
        if(data.status=="success")
          {
            var count=0;
            var documentLiHtml='';
            var innerCount=1;
            while(count<data.response.length)
            {
              documentLiHtml=documentLiHtml+data.content;
              count++;
              innerCount++;
            }
            jQuery("#upload-document").show();
            jQuery("#tabs-4").find(".tabs-wrapper .all-documents").append(documentLiHtml);
            jQuery("body li.document-li").each(function(index,element){
              var image=APP_URL+'/'+data.response[index].document_path+data.response[index].file_name;
              jQuery(element).find(".desc").html(data.response[index].file_name);
              jQuery(element).find(".example-image-link").attr("href",image);
              jQuery(element).find("img").attr("src",image);
              jQuery(element).find(".document-remove").attr("data-id",data.response[index].id);
              jQuery(element).find(".document-downld a").attr("href",APP_URL+'/'+data.response[index].document_path+data.response[index].file_name);
              jQuery(element).find(".document-expand a").attr("href",APP_URL+'/'+data.response[index].document_path+data.response[index].file_name);
            });
            jQuery(".showbox-wrapper").hide();
          }
      },
    
   });
});