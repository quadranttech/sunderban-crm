if(window.location.hash.substr(1)=="document")
{
	jQuery("#show-document").trigger("click");
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

if(getParameterByName('page')=="feed")
{
	setTimeout(function(){ 
		jQuery("#show-feed").trigger("click");
		jQuery("#show-feed").parent("li").addClass("ui-tabs-active");
	}, 1000)
	
}

if(getParameterByName('page')=="task")
{
	setTimeout(function(){
		jQuery("#show-contexual-task").trigger("click");
	}, 100)
}

if(getParameterByName('page')=="document")
{
	setTimeout(function(){
		jQuery("#show-document").trigger("click");
	}, 100)
}

if(getParameterByName('page')=="note")
{
	setTimeout(function(){
		jQuery("#show-note").trigger("click");
	}, 100)
}

if(getParameterByName('page')=="deal")
{
	setTimeout(function(){
		jQuery(".show-deal-card").trigger("click");
	}, 100)
}

//DEAL CONTEXUAL
jQuery("#show-deal-feed").click(function(){
	var deal_id=window.location.href.split("?deal_id=").pop().substr(0, window.location.href.split("?deal_id=").pop().indexOf('&'));
	history.pushState('obj', 'deal-details', APP_URL+'/deal-contexual?deal_id='+deal_id+'&page='+'deal_feed');
});

jQuery("#show-deal-details").click(function(){
	jQuery(".showbox-wrapper").show();
	setTimeout(function(){
		jQuery(".showbox-wrapper").hide();
	}, 500)
	var deal_id=window.location.href.split("?deal_id=").pop().substr(0, window.location.href.split("?deal_id=").pop().indexOf('&'));
	history.pushState('obj', 'deal-details', APP_URL+'/deal-contexual?deal_id='+deal_id+'&page='+'deal_details');
});

jQuery("#show-deal-task").click(function(){
	var deal_id=window.location.href.split("?deal_id=").pop().substr(0, window.location.href.split("?deal_id=").pop().indexOf('&'));
	history.pushState('obj', 'deal-details', APP_URL+'/deal-contexual?deal_id='+deal_id+'&page='+'deal_task');
});

jQuery("#show-deal-document").click(function(){
	var deal_id=window.location.href.split("?deal_id=").pop().substr(0, window.location.href.split("?deal_id=").pop().indexOf('&'));
	history.pushState('obj', 'deal-details', APP_URL+'/deal-contexual?deal_id='+deal_id+'&page='+'deal_document');
});

jQuery("#show-deal-note").click(function(){
	var deal_id=window.location.href.split("?deal_id=").pop().substr(0, window.location.href.split("?deal_id=").pop().indexOf('&'));
	history.pushState('obj', 'deal-details', APP_URL+'/deal-contexual?deal_id='+deal_id+'&page='+'deal_notes');
});

jQuery("#show-deal-product").click(function(){
	var deal_id=window.location.href.split("?deal_id=").pop().substr(0, window.location.href.split("?deal_id=").pop().indexOf('&'));
	history.pushState('obj', 'deal-details', APP_URL+'/deal-contexual?deal_id='+deal_id+'&page='+'deal_product');
})

if(getParameterByName('page')=="deal_feed")
{
	setTimeout(function(){
		jQuery("#show-deal-feed").trigger("click");
		jQuery("#show-deal-feed").parent("li").addClass("ui-tabs-active");
	}, 500)
}

if(getParameterByName('page')=="deal_details")
{
	setTimeout(function(){
		jQuery("#show-deal-details").trigger("click");
	}, 100)
}

if(getParameterByName('page')=="deal_task")
{
	setTimeout(function(){
		jQuery("#show-deal-task").trigger("click");
	}, 100)
}

if(getParameterByName('page')=="deal_document")
{
	setTimeout(function(){
		jQuery("#show-deal-document").trigger("click");
	}, 100)
}

if(getParameterByName('page')=="deal_notes")
{
	setTimeout(function(){
		jQuery("#show-deal-note").trigger("click");
	}, 100)
}

if(getParameterByName('page')=="deal_product")
{
	setTimeout(function(){
		jQuery("#show-deal-product").trigger("click");
	}, 100)
}