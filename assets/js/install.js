jQuery("input[name='database_host']").focusout(function(){
	var database_host=checkValidation(jQuery("input[name='database_host']"));
});

jQuery("input[name='database_name']").focusout(function(){
	var database_name=checkValidation(jQuery("input[name='database_name']"));
});

jQuery("input[name='database_username']").focusout(function(){
	var database_username=checkValidation(jQuery("input[name='database_username']"));
});

jQuery("#check-database-connectivity").click(function(event){
	var database_host=checkValidation(jQuery("input[name='database_host']"));
	var database_name=checkValidation(jQuery("input[name='database_name']"));
	var database_username=checkValidation(jQuery("input[name='database_username']"));
	if(database_host==false || database_name==false || database_username==false || jQuery("select[name='timezone']").val()=="")
	{
		return false;
	}
	else
	{
		jQuery.ajax({
		    url: APP_URL+'/create-database',
		    type: 'post',
		    data: {'_token':jQuery("input[name='_token']").val(),'db_host':jQuery("input[name='database_host']").val(),'db_name':jQuery("input[name='database_name']").val(),'db_username':jQuery("input[name='database_username']").val(),'db_password':jQuery("input[name='database_password']").val(),'timezone':jQuery("select[name='timezone']").val()},
		    dataType: 'JSON',
		    success: function (data) {
		    	console.log(data);
		      if(data.status=="success")
		      {
		        jQuery(".installer-form").submit();
		      }
		      else
		      {
		      	if(data.message=="php_version")
		      	{
		      		jQuery('<div class="connection-error"><span>Please upgrade to php version 5.6</span></div>').insertAfter("#check-database-connectivity");
		      	}
		      	if(data.message=="database_not_empty")
		      	{
		      		jQuery('<div class="connection-error"><span>Please empty database</span></div>').insertAfter("#check-database-connectivity");
		      	}
		      	else
		      	{
		      		jQuery('<div class="connection-error"><span>Conntection Error</span></div>').insertAfter("#check-database-connectivity");
		      	}
		      }
		    }
	  	});
	}
});

function checkValidation(target)
{
	if(target.val()=="")
	{
		target.parent(".input-field").addClass("input-invalid");
		return false;
	}
	else
	{
		target.parent(".input-field").removeClass("input-invalid");
		return true;
	}
}

// MATERIAL SELECT

$('select').material_select();

jQuery(".cancel-image-crop-signin").click(function(){
	jQuery("#image-cropper").hide();
});

jQuery("input[name='company_name']").keyup(function(){
	jQuery(".company-details h2").html(jQuery(this).val());
	jQuery("label[for='portal_name']").addClass("active");
});

jQuery("input[name='street_address']").keyup(function(){
	jQuery(".company-details .address").html(jQuery(this).val()+',');
});

jQuery("input[name='city']").keyup(function(){
	jQuery(".company-details .city").html(jQuery(this).val()+',');
});

jQuery("input[name='state']").keyup(function(){
	jQuery(".company-details .state").html(jQuery(this).val());
});

jQuery(".modal-close").click(function(){
	jQuery("#image-cropper").hide();
});