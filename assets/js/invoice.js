//CREATE INVOICE
jQuery(document).on("click",".add-new-item",function(){
	var add_new_item=true;
	refreshIntervalId = setInterval(calculateInvoiceTotal, 300);
	if(jQuery(".invoice-item-clone:last").next(".content-table-row").length>0)
	{
		if(jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='item_details[]']").val()=="")
		{
			jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='item_details[]']").parent(".input-field").addClass("input-invalid");
			add_new_item=false;
		}
		else
		{
			jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='item_details[]']").parent(".input-field").removeClass("input-invalid");
			add_new_item=true;
		}
		if(jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='invoice_quantity[]']").val()=="")
		{
			jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='invoice_quantity[]']").parent(".input-field").addClass("input-invalid");
			add_new_item=false;
		}
		else
		{
			jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='invoice_quantity[]']").parent(".input-field").removeClass("input-invalid");
			add_new_item=true;
		}
		if(jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='tax[]']").val()=="")
		{
			jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='tax[]']").closest(".input-field").addClass("input-invalid");
			add_new_item=false;
		}
		else
		{
			jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='tax[]']").closest(".input-field").removeClass("input-invalid");
			add_new_item=true;
		}
		if(jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='total[]']").val()=="")
		{
			jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='total[]']").parent(".input-field").addClass("input-invalid");
			add_new_item=false;
		}
		else
		{
			jQuery(".invoice-item-clone:last").next(".content-table-row").find("input[name='total[]']").parent(".input-field").removeClass("input-invalid");
			add_new_item=true;
		}
	}
	if(add_new_item==true)
	{
		if(jQuery(".invoice-item-clone").next(".content-table-row").length==0)
		{
			jQuery(".invoice-item-clone").clone().insertAfter(jQuery(".invoice-item-clone:last"));
			jQuery(".invoice-item-clone:last").removeClass("invoice-item-clone").addClass("line-item").show();
			jQuery(".invoice-item-clone:last").next().find("select[name='currency[]']").material_select();
			jQuery(".invoice-item-clone:last").next().find(".currency span:first").remove();
			jQuery(".invoice-item-clone:last").next().find(".currency input:first").remove();
			var inputAutoCompleteTarget = jQuery(".invoice-item-clone:last").next().find("input[name='item_details[]']");
			inputAutoComplete(inputAutoCompleteTarget);
		}
		else
		{
			jQuery("#sortable").append(jQuery(".invoice-item-clone").clone());
			jQuery("#sortable .content-table-row:last").removeClass("invoice-item-clone").addClass("line-item").show();
			jQuery("#sortable .content-table-row:last").find("select[name='currency[]']").material_select();
			jQuery("#sortable .content-table-row:last").find(".currency span:first").remove();
			jQuery("#sortable .content-table-row:last").find(".currency input:first").remove();
			var inputAutoCompleteTarget = jQuery("#sortable .content-table-row:last").find("input[name='item_details[]']");
			inputAutoComplete(inputAutoCompleteTarget);
		}
		if(jQuery(".line-item").length>=1)
		{
			jQuery(".add-table-item").remove();
		}
		if(jQuery(".line-item").length>=2)
		{
			jQuery(".line-item:not(:first)").find(".add-new-item").removeClass("add-input add-new-item").addClass("remove-input remove-new-item");
		}
	}
	if(add_new_item==false)
	{
		return false;
	}
	
});

var productData = {};
var productDataObject = {};
function inputAutoComplete(inputAutoCompleteTarget)
{
	jQuery(inputAutoCompleteTarget).autocomplete({
	   data: productData,
	   limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
	   onAutocomplete: function(val) {
	    console.log(val);
	    val = val.split(/[ ,]+/).join('_');
	    console.log(val);
	    console.log(productDataObject);
	    console.log(productDataObject[val]);
	    inputAutoCompleteTarget.closest(".content-table-row").find("input[name='tax[]']").val(productDataObject[val]);
	     // Callback function when value is autcompleted.
	   },
	   minLength: 5, // The minimum length of the input for the autocomplete to start. Default: 1.
	});
}

//REINITIALIZE AUTO COMPLETE ON KEY PRESS
jQuery(document).on("keyup","#sortable .content-table-row input,textarea",function(){
	console.log("ds");
    var inputAutoCompleteTarget = jQuery("#sortable .content-table-row:last").find("input[name='item_details[]']");
	inputAutoComplete(inputAutoCompleteTarget);
});

jQuery(".save-invoice").click(function(event){
	event.preventDefault();
	var status=jQuery(this).attr("data-status");
	var purpose="save";
	var id=jQuery("input[name='invoice_id']").val();
	if(invoiceValidation()==false)
	{
		return false;
	}
	createInvoice('',status,purpose,id);
});

function invoiceValidation()
{
	if(jQuery("input[name='item_details[]']").length==1)
	{
		jQuery(".invoice-error").text(jQuery(".list_of_possible_errors .line-item").text());
		return false;
	}
	if(jQuery("input[name='invoice_title']").val()=="")
	{
		jQuery(".invoice-error").text(jQuery(".list_of_possible_errors .invoice-title").text());
		return false;
	}
	if(jQuery("select[name='invoice_contact']").val()=="select_contact" || jQuery("select[name='invoice_contact']").val()=="no_contact")
	{
		jQuery(".invoice-error").text(jQuery(".list_of_possible_errors .invoice-contact").text());
		return false;
	}
	if(jQuery("input[name='invoice_total']").val()<0)
	{
		jQuery(".invoice-error").text(jQuery(".list_of_possible_errors .invoice-total").text());
		return false;
	}
}

function createInvoice(fileName,status,purpose,id)
{
	var invoice_contact=jQuery("select[name='invoice_contact']").val();
	var invoice_no=jQuery("input[name='invoice_no']").val();
	var order_no=jQuery("input[name='order_no']").val();
	var invoice_date=jQuery("input[name='invoice_date']").val();
	var due_date=jQuery("input[name='due_date']").val();
	var sales_person=jQuery("select[name='sales_person']").val();
	var default_payment_term=jQuery("input[name='default_payment_term']").val();
	var contact_type=jQuery("select[name='invoice_contact'] option:selected").attr("data-contact");
	var quantity_type=jQuery("select[name='quantity']").val();
	var deal_id=jQuery("select[name='deal_id']").val();

	var item_details_array=[];
	var quantity_array=[];
	var rate_array=[];
	var total_array=[];
	var currency=[];
	jQuery("input[name='item_details[]']").each(function(index,element){
		if(index>0)
		{
			item_details_array.push(jQuery(this).val());
		}
	});
	jQuery("input[name='invoice_quantity[]']").each(function(index,element){
		if(index>0)
		{
			quantity_array.push(jQuery(this).val());
		}
	});
	jQuery("input[name='tax[]']").each(function(index,element){
		if(index>0)
		{
			rate_array.push(jQuery(this).val());
		}
	});
	jQuery("input[name='total[]']").each(function(index,element){
		if(index>0)
		{
			total_array.push(jQuery(this).val());
		}
	});
	jQuery("select[name='currency[]']").each(function(index,element){
		if(index>0)
		{
			currency.push(jQuery(this).val());
		}
	});

	var customer_note=jQuery("textarea[name='customer_note']").val();
	var admin_note=jQuery("textarea[name='admin_note']").val();
	var terms_and_conditions=jQuery("textarea[name='terms_and_conditions']").val();
	var shipping_cost=jQuery("input[name='shipping-cost']").val();
	if(jQuery("#filled-in-box").is(':checked'))
	{
		var part_pay=true;
		var part_pay_type=jQuery("select[name='part_pay_type']").val();
		var part_pay_value=jQuery("input[name='part_pay_value']").val();
	}
	else
	{
		var part_pay=false;
		var part_pay_type="no_part_pay";
		var part_pay_value=0;
	}
	if(shipping_cost=="")
	{
		shipping_cost=0;
	}
	var adjustment=jQuery("input[name='adjustment']").val();
	var discount=jQuery("select[name='discount']").val();
	var discount_type=jQuery("select[name='discount']").find(':selected').attr("data-discount-type");
	var discount_id=0;
	var tax=jQuery("select[name='tax']").val();
	var tax_type=jQuery("select[name='tax']").find(':selected').attr("data-tax-type");
	var tax_id=0;
	var inv_total=0;
	jQuery(total_array).each(function(index,element){
		inv_total=parseInt(inv_total)+parseInt(element);
	});
	if(tax!="select_tax" && tax!="no_tax")
	{
		tax_id=jQuery("select[name='tax']").find(':selected').attr("data-tax-id");
		if(tax_type!="fixed")
		{
			tax=((tax / 100) * (parseFloat(inv_total)+parseFloat(shipping_cost)-parseFloat(Math.abs(adjustment))));
		}
	}
	else
	{
		tax=0;
	}
	if(discount!="select_discount" && discount!="no_discount")
	{
		discount_id=jQuery("select[name='discount']").find(':selected').attr("data-discount-id");
		if(discount_type!="fixed")
		{
			discount=((discount / 100) * (parseFloat(inv_total)+parseFloat(shipping_cost)-parseFloat(Math.abs(adjustment))));
		}
	}
	else
	{
		discount=0;
	}
	var total=jQuery("input[name='total']").val();
	if(adjustment=="")
	{
		adjustment=0;
	}
	startLoader(jQuery(".save-invoice[data-status="+status+"]"));
	startLoader(jQuery(".edit-invoice[data-status="+status+"]"));
	jQuery.ajax({
		url:'save-invoice',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'invoice_title':jQuery("input[name='invoice_title']").val(),'invoice_contact':invoice_contact,'invoice_no':invoice_no,'order_no':order_no,'invoice_date':invoice_date,'due_date':due_date,'sales_person':sales_person,'contact_type':contact_type,'quantity_type':quantity_type,'deal_id':deal_id,'item_details_array':item_details_array,'quantity_array':quantity_array,'rate_array':rate_array,'total_array':total_array,'currency':currency,'customer_note':customer_note,'admin_note':admin_note,'terms_and_conditions':terms_and_conditions,'shipping_cost':shipping_cost,'part_pay':part_pay,'part_pay_type':part_pay_type,'part_pay_value':part_pay_value,'adjustment':adjustment,'discount':discount,'tax':tax,'total':total,'default_payment_term':default_payment_term,'status':status,'purpose':purpose,'id':id,'tax_id':tax_id,'discount_id':discount_id,'fileName':fileName},
      	type:'post',
      	success:function(data){
      		console.log(data);
        	if(data.response=="failed")
        	{
        		if(data.message=="company_does_not_have_contact")
        		{
        			jQuery(".invoice-error").html("Company does not have any default contact");
        		}
        		else if(data.message=="inv_no_already_exist")
        		{
        			jQuery(".invoice-error").html("Invoice no already exist");
        		}
        		else
        		{
        			jQuery(".invoice-error").html("SMTP settings not found. <a href='"+APP_URL+"/settings?page=email'>Set up now.</a>");
        		}
        	}
        	else
        	{
        		if(status=="send")
        		{
        			jQuery(".modal-overlay").show();
        			jQuery("#show-invoice-template-preview input[name='inv_id']").val(data.inv_id);
	        		jQuery("#show-invoice-template-preview input[name='invoice-subject-title']").val(data.invoice_title);
	        		jQuery("#show-invoice-template-preview label[for='invoice-subject-title']").addClass("active");
	        		jQuery("#show-invoice-template-preview input[name='inv_no']").val(data.inv_no);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").html(data.content);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".download-pdf").text(data.label_name.download_pdf);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".id").text(data.label_name.id);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".due-on").text(data.label_name.due_on);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".item_name").text(data.label_name.item_name);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".item_quantity").text(data.label_name.quantity);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".item_rate").text(data.label_name.rate);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".total").text(data.label_name.total);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".sub-total").text(data.label_name.sub_total);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".adjustment").text(data.label_name.adjustments);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".discount").text(data.label_name.discount);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".tax").text(data.label_name.tax);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".total").text(data.label_name.total);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".total").text(data.label_name.total);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".thank_you").text(data.label_name.thank_you);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".for").text(data.label_name.for);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".doing-business-with").text(data.label_name.doing_business_with);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".this-invoice-is-generated-using").text(data.label_name.this_invoice_is_generated_using);
	        		jQuery("#show-invoice-template-preview .logo a img").attr("src",APP_URL+'/uploads/company-logo/company-logo.png');
	        		var item_array_count=0;
	        		var item_subtotal=0;
	        		while(item_array_count<data.items_array.length-1)
	        		{
	        			jQuery(".item-clone").clone().insertAfter(".item-clone:last");
	        			item_array_count++;
	        		}
	        		jQuery("span.quantity").text(quantity_type);
	        		jQuery(data.items_array).each(function(index,element){
	        			item_subtotal=item_subtotal+element.total;
	        			jQuery(".item-clone:eq("+index+")").find("td:eq(0)").text(element.item_details);
	        			jQuery(".item-clone:eq("+index+")").find("td:eq(0)").text();
	        			jQuery(".item-clone:eq("+index+")").find("td:eq(2)").text(element.rate);
	        			jQuery(".item-clone:eq("+index+")").find("td:eq(3)").text(element.quantity);
	        			jQuery(".item-clone:eq("+index+")").find("td:eq(4)").text(element.total);
	        		});
	        		item_subtotal=item_subtotal.toFixed(2);
	        		jQuery("td.sub_total").text(data.currency_symbol+" "+item_subtotal);
	        		jQuery("span.currency").text(data.currency_symbol);
	        		jQuery("td.discount").text(data.discount);
	        		jQuery("td.tax").text(data.tax);
	        		jQuery("td.adjustment").text(adjustment);
	        		jQuery("span.portal_name").text(data.portalName);
	        		jQuery(".id-due-on").text("ID: "+data.inv_no+" Due on:"+data.due_date);
	        		jQuery("span.company_name").text("from "+data.admin_company.company_name);
	        		jQuery("span.company_name_2").text(data.admin_company.company_name);
	        		if(data.admin_company.address!=null)
	        		{
	        			jQuery("p.company_address").text(data.admin_company.address);
	        		}
	        		var admin_company_address="";
	        		if(data.admin_company.country!=null && data.admin_company.country!="")
	        		{
	        			admin_company_address=data.admin_company.country+", ";
	        		}
	        		if(data.admin_company.state!=null && data.admin_company.state!="")
	        		{
	        			admin_company_address=admin_company_address+data.admin_company.state+"- ";
	        		}
	        		if(data.admin_company.post_code!=null && data.admin_company.post_code!="")
	        		{
	        			admin_company_address=admin_company_address+data.admin_company.post_code;
	        		}
	        		jQuery(".country-state-post-codes").text(admin_company_address);
	        		jQuery(".contact_name").text("to "+data.contact_details.first_name+" "+data.contact_details.last_name);
	        		if(data.contact_address!=null)
	        		{
	        			if(data.contact_address.street_address!=null && data.contact_address.street_address!="")
	        			{
	        				jQuery(".contact_address").text(data.contact_address.street_address);
	        			}
	        			var contact_address="";
	        			if(data.contact_address.street_address!=null && data.contact_address.street_address!="")
	        			{
	        				contact_address=contact_address+data.contact_address.street_address+", ";
	        			}
	        			if(data.contact_address.state!=null && data.contact_address.state!="")
	        			{
	        				contact_address=contact_address+data.contact_address.state+"- ";
	        			}
	        			if(data.contact_address.post_code!=null && data.contact_address.post_code!="")
	        			{
	        				contact_address=contact_address+data.contact_address.post_code;
	        			}
	        			jQuery(".contact-country-state-post-code").text(contact_address);
	        		}
	        		if(data.terms_and_conditions!=null && data.terms_and_conditions!="")
	        		{
	        			jQuery(".terms_and_conditions").text(data.terms_and_conditions);
	        		}
	        		jQuery(".inv_total").text(data.currency_symbol+" "+data.response);
	        		if(data.admin_company.logo=="" && data.admin_company.logo==null)
	        		{
	        			jQuery(".logo img").attr("src",APP_URL+'/assets/img/logo.png');
	        		}
	        		else
	        		{
	        			jQuery(".logo img").attr("src",data.admin_company.logo);
	        		}
	        		jQuery(".modal-overlay").show();
	        		jQuery("#show-invoice-template-preview").css("z-index","1003");
	        		jQuery("#show-invoice-template-preview").show();
        		}
        		else
        		{
        			window.location.href=APP_URL+'/invoices';
        		}
        		stopLoader(jQuery(".save-invoice"));
        		stopLoader(jQuery(".edit-invoice"));
        	}
      	},
	});
}

jQuery(".send-invoice").click(function(){
	jQuery(".send-invoice").html("Sending <span class='spinner'></span> <span class='checkmark'></span>");
	jQuery.ajax({
		url:APP_URL+'/send-invoice',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'invoice_id':jQuery("input[name='inv_id']").val(),'invoice_subject':jQuery("input[name='invoice-subject-title']").val(),'message_to_customer':jQuery("input[name='message-to-customer']").val()},
      	type:'post',
      	success:function(data){
      		console.log(data);
        	if(data.response=="success")
        	{
        		jQuery("#show-invoice-template-preview").hide();
        		window.location.href=APP_URL+'/invoices';
        	}
        	else
        	{
        		jQuery('<span class="">'+data.status+'</span>').insertAfter(jQuery(".send-invoice"));
        	}
        	jQuery(".send-invoice").find(".spinner").remove();
		    jQuery(".send-invoice").addClass("saved");
		    setTimeout(function(){
		        jQuery(".send-invoice").removeClass("saved");
		        jQuery(".send-invoice").html("Save");
		    }, 3000);
      	},
	});
});

setTimeout(function(){
	if(window.location.href==APP_URL+'/invoice')
	{
		if(jQuery("input[name='default_payment_term']").val()!="")
		{
			setDefaultPayment(jQuery("input[name='default_payment_term']").val());
		}
	}
}, 1000);
jQuery(".default-payment-terms").click(function(){
	if(jQuery("input[name='invoice_date']").val()=="")
	{
		jQuery(".default-payment-terms-error").text("Please Fill Invoice Date");
	}
	else
	{
		jQuery(".default-payment-terms-error").text("");
		jQuery("input[name='default_payment_term']").val(jQuery("select[name='default_payment_term_select']").val());
		setDefaultPayment(jQuery("select[name='default_payment_term_select']").val());
		
	}
});

function setDefaultPayment(defaultPaymentDate)
{
	var someDate = new Date(jQuery("input[name='invoice_date']").val());
	var numberOfDaysToAdd = parseInt(defaultPaymentDate.split("net-")[1]);
	someDate.setDate(someDate.getDate() + numberOfDaysToAdd); 
	var dd = someDate.getDate();
	var mm = someDate.getMonth() + 1;
	var y = someDate.getFullYear();
	if(dd<10)
	{
		dd='0'+dd;
	}
	if(mm<10)
	{
		mm='0'+mm;
	}

	var someFormattedDate = y + '-'+ mm + '-'+ dd;
	jQuery("#date2").val(someFormattedDate);
	jQuery(".default-payment-terms-close").trigger("click");
}

//INVOICE TOTAL

var refreshIntervalId = setInterval(calculateInvoiceTotal, 300);

function calculateInvoiceTotal()
{
	jQuery("input[name='tax[]']").each(function(index,element){
		if(jQuery(this).val()!="")
		{
			var rate=jQuery(this).val();
			var rate_index=index;
			jQuery("input[name='invoice_quantity[]']").each(function(index2,element2){
				if(index2==index && jQuery(this).val()!="")
				{
					var total=rate*jQuery(this).val();
					total=total.toFixed(2);
					jQuery(".invoice-item .content-table-row:eq("+rate_index+")").find("input[name='total[]']").val(total);

					var invoice_total=0;
					jQuery("input[name='total[]']").each(function(index,element){
						if(jQuery(this).val()!="")
						{
							invoice_total=parseFloat(invoice_total)+parseFloat(jQuery(this).val());
							invoice_total=invoice_total.toFixed(2);
						}
					});
					if(jQuery("input[name='shipping-cost']").val()!="")
					{
						invoice_total=parseFloat(invoice_total)+parseFloat(Math.abs(jQuery("input[name='shipping-cost']").val()));
					}
					if(jQuery("input[name='adjustment']").val()!="")
					{
						invoice_total=parseFloat(invoice_total)-parseFloat(Math.abs(jQuery("input[name='adjustment']").val()));
					}
					if(jQuery("select[name='discount']").val()!="select_discount" && jQuery("select[name='discount']").val()!="no_discount")
					{
						var discount=jQuery("select[name='discount']").val();
						var discount_type=jQuery("select[name='discount']").find(':selected').attr("data-discount-type");
						if(discount_type!="fixed")
						{
							discount=((discount / 100) * jQuery("input[name='invoice_total']").val());
							discount=discount.toFixed(2);
						}
						invoice_total=parseFloat(invoice_total)-parseFloat(Math.abs(discount));
					}
					if(jQuery("select[name='tax']").val()!="select_tax" && jQuery("select[name='tax']").val()!="no_tax")
					{
						var tax=jQuery("select[name='tax']").val();
						var tax_type=jQuery("select[name='tax']").find(':selected').attr("data-tax-type");
						if(tax_type!="fixed")
						{
							tax=((tax / 100) * invoice_total);
							tax=tax.toFixed(2);
						}
						invoice_total=parseFloat(invoice_total.toFixed(2))+parseFloat(tax);
					}
					invoice_total=invoice_total.toFixed(2);
					jQuery("input[name='invoice_total']").val(invoice_total);
				}
			});
			clearInterval(refreshIntervalId);
		}
	});
}

jQuery(".edit-invoice").click(function(event){
	event.preventDefault();
	if(invoiceValidation()==false)
	{
		return false;
	}
	var status=jQuery(this).attr("data-status");
	var purpose="edit";
	var id=jQuery("input[name='invoice_id']").val();
	createInvoice('',status,purpose,id);
});

jQuery("select[name='discount']").change(function(){
	var refreshIntervalId = setInterval(calculateInvoiceTotal, 300);
});

jQuery("select[name='tax']").change(function(){
	var refreshIntervalId = setInterval(calculateInvoiceTotal, 300);
});

jQuery("input[name='item_details[]']").change(function(){
	var refreshIntervalId = setInterval(calculateInvoiceTotal, 300);
});

jQuery("input[name='invoice_quantity[]']").change(function(){
	var refreshIntervalId = setInterval(calculateInvoiceTotal, 300);
});

jQuery("input[name='tax[]']").change(function(){
	var refreshIntervalId = setInterval(calculateInvoiceTotal, 300);
});

jQuery(document).on("focus","input[name='total[]']",function(){
	var refreshIntervalId = setInterval(calculateInvoiceTotal, 300);
});

jQuery(".quote-mail").click(function(){
	jQuery.ajax({
		url:APP_URL+'/send-invoice',
      	dataType:'json',
      	data:{'invoice_id':jQuery(this).attr("data-invoice-id")},
      	type:'get',
      	success:function(data){
        	stopLoader(jQuery(".save-invoice"));
      	},
	});
});

jQuery(document).on("click",".remove-new-item",function(){
	jQuery(this).parents(".line-item").remove();
});

jQuery(".save-estimate").click(function(event){
	event.preventDefault();
	var status=jQuery(this).attr("data-status");
	var purpose="save";
	var id=jQuery("input[name='invoice_id']").val();
	if(estimateValidation()==false)
	{
		return false;
	}
	createEstimate('',status,purpose,id);
});

function estimateValidation()
{
	if(jQuery("input[name='item_details[]']").length==1)
	{
		jQuery(".invoice-error").text(jQuery(".list_of_possible_errors .line-item").text());
		return false;
	}
	if(jQuery("input[name='invoice_title']").val()=="")
	{
		jQuery(".invoice-error").text(jQuery(".list_of_possible_errors .estimate-title").text());
		return false;
	}
	if(jQuery("input[name='invoice_contact']").val()=="select_contact")
	{
		jQuery(".invoice-error").text(jQuery(".list_of_possible_errors .invoice-contact").text());
		return false;
	}
	if(jQuery("input[name='invoice_total']").val()<0)
	{
		jQuery(".invoice-error").text(jQuery(".list_of_possible_errors .estimate-total").text());
		return false;
	}
}

function createEstimate(fileName,status,purpose,id)
{
	var invoice_contact=jQuery("select[name='invoice_contact']").val();
	var invoice_no=jQuery("input[name='invoice_no']").val();
	var invoice_date=jQuery("input[name='invoice_date']").val();
	var sales_person=jQuery("select[name='sales_person']").val();
	var default_payment_term=jQuery("input[name='default_payment_term']").val();
	var contact_type=jQuery("select[name='invoice_contact'] option:selected").attr("data-contact");
	var quantity_type=jQuery("select[name='quantity']").val();
	var deal_id=jQuery("select[name='deal_id']").val();

	var item_details_array=[];
	var quantity_array=[];
	var rate_array=[];
	var total_array=[];
	var currency=[];
	jQuery("input[name='item_details[]']").each(function(index,element){
		if(index>0)
		{
			item_details_array.push(jQuery(this).val());
		}
	});
	jQuery("input[name='invoice_quantity[]']").each(function(index,element){
		if(index>0)
		{
			quantity_array.push(jQuery(this).val());
		}
	});
	jQuery("input[name='tax[]']").each(function(index,element){
		if(index>0)
		{
			rate_array.push(jQuery(this).val());
		}
	});
	jQuery("input[name='total[]']").each(function(index,element){
		if(index>0)
		{
			total_array.push(jQuery(this).val());
		}
	});
	jQuery("select[name='currency[]']").each(function(index,element){
		if(index>0)
		{
			currency.push(jQuery(this).val());
		}
	});

	var customer_note=jQuery("textarea[name='customer_note']").val();
	var admin_note=jQuery("textarea[name='admin_note']").val();
	var terms_and_conditions=jQuery("textarea[name='terms_and_conditions']").val();
	var shipping_cost=jQuery("input[name='shipping-cost']").val();
	if(jQuery("#filled-in-box").is(':checked'))
	{
		var part_pay=true;
		var part_pay_type=jQuery("select[name='part_pay_type']").val();
		var part_pay_value=jQuery("input[name='part_pay_value']").val();
	}
	else
	{
		var part_pay=false;
		var part_pay_type="no_part_pay";
		var part_pay_value=0;
	}
	if(shipping_cost=="")
	{
		shipping_cost=0;
	}
	var adjustment=jQuery("input[name='adjustment']").val();
	var discount=jQuery("select[name='discount']").val();
	var discount_type=jQuery("select[name='discount']").find(':selected').attr("data-discount-type");
	var discount_id=0;
	var tax=jQuery("select[name='tax']").val();
	var tax_type=jQuery("select[name='tax']").find(':selected').attr("data-tax-type");
	var tax_id=0;
	var inv_total=0;
	jQuery(total_array).each(function(index,element){
		inv_total=parseInt(inv_total)+parseInt(element);
	});
	if(tax!="select_tax" && tax!="no_tax")
	{
		tax_id=jQuery("select[name='tax']").find(':selected').attr("data-tax-id");
		if(tax_type!="fixed")
		{
			tax=((tax / 100) * (parseFloat(inv_total)+parseFloat(shipping_cost)-parseFloat(Math.abs(adjustment))));
		}
	}
	else
	{
		tax=0;
	}
	if(discount!="select_discount" && discount!="no_discount")
	{
		discount_id=jQuery("select[name='discount']").find(':selected').attr("data-discount-id");
		if(discount_type!="fixed")
		{
			discount=((discount / 100) * (parseFloat(inv_total)+parseFloat(shipping_cost)-parseFloat(Math.abs(adjustment))));
		}
	}
	else
	{
		discount=0;
	}
	var total=jQuery("input[name='total']").val();
	if(adjustment=="")
	{
		adjustment=0;
	}
	startLoader(jQuery(".save-estimate[data-status="+status+"]"));
	startLoader(jQuery(".edit-estimate[data-status="+status+"]"));
	jQuery.ajax({
		url:'save-estimate',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'estimate_title':jQuery("input[name='invoice_title']").val(),'invoice_contact':invoice_contact,'invoice_no':invoice_no,'invoice_date':invoice_date,'sales_person':sales_person,'contact_type':contact_type,'quantity_type':quantity_type,'deal_id':deal_id,'item_details_array':item_details_array,'quantity_array':quantity_array,'rate_array':rate_array,'total_array':total_array,'currency':currency,'customer_note':customer_note,'admin_note':admin_note,'terms_and_conditions':terms_and_conditions,'shipping_cost':shipping_cost,'part_pay':part_pay,'part_pay_type':part_pay_type,'part_pay_value':part_pay_value,'adjustment':adjustment,'discount':discount,'tax':tax,'total':total,'default_payment_term':default_payment_term,'status':status,'purpose':purpose,'id':id,'tax_id':tax_id,'discount_id':discount_id,'fileName':fileName},
      	type:'post',
      	success:function(data){
      		console.log(data);
        	if(data.response=="failed")
        	{
        		if(data.message=="company_does_not_have_contact")
        		{
        			jQuery(".invoice-error").html("Company does not have any default contact");
        		}
        		else if(data.message=="est_no_already_exist")
        		{
        			jQuery(".invoice-error").html("Estimate no already exist");
        		}
        		else
        		{
        			jQuery(".invoice-error").html("SMTP settings not found. <a href='"+APP_URL+"/settings?page=email'>Set up now.</a>");
        		}
        	}
        	else
        	{
        		if(status=="send")
        		{
        			jQuery(".modal-overlay").show();
        			jQuery("#show-invoice-template-preview input[name='est_id']").val(data.est_id);
	        		jQuery("#show-invoice-template-preview input[name='invoice-subject-title']").val(data.estimate_title);
	        		jQuery("#show-invoice-template-preview label[for='invoice-subject-title']").addClass("active");
	        		jQuery("#show-invoice-template-preview input[name='est_no']").val(data.est_no);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").html(data.content);
	        		jQuery("#show-invoice-template-preview .invoice-template-preview").find(".download-pdf").text(data.label_name.download_pdf);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".id").text(data.label_name.id);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".due-on").text(data.label_name.due_on);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".item_name").text(data.label_name.item_name);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".item_quantity").text(data.label_name.quantity);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".item_rate").text(data.label_name.rate);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".total").text(data.label_name.total);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".sub-total").text(data.label_name.sub_total);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".adjustment").text(data.label_name.adjustments);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".discount").text(data.label_name.discount);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".tax").text(data.label_name.tax);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".total").text(data.label_name.total);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".total").text(data.label_name.total);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".thank_you").text(data.label_name.thank_you);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".for").text(data.label_name.for);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".doing-business-with").text(data.label_name.doing_business_with);
					jQuery("#show-invoice-template-preview .invoice-template-preview").find(".this-invoice-is-generated-using").text(data.label_name.this_invoice_is_generated_using);
	        		var item_array_count=0;
	        		var item_subtotal=0;
	        		while(item_array_count<data.items_array.length-1)
	        		{
	        			jQuery(".item-clone").clone().insertAfter(".item-clone:last");
	        			item_array_count++;
	        		}
	        		jQuery("span.quantity").text(quantity_type);
	        		jQuery(data.items_array).each(function(index,element){
	        			item_subtotal=item_subtotal+element.total;
	        			jQuery(".item-clone:eq("+index+")").find("td:eq(0)").text(element.item_details);
	        			jQuery(".item-clone:eq("+index+")").find("td:eq(0)").text();
	        			jQuery(".item-clone:eq("+index+")").find("td:eq(2)").text(element.rate);
	        			jQuery(".item-clone:eq("+index+")").find("td:eq(3)").text(element.quantity);
	        			jQuery(".item-clone:eq("+index+")").find("td:eq(4)").text(element.total);
	        		});
	        		item_subtotal=item_subtotal.toFixed(2);
	        		jQuery("td.sub_total").text(data.currency_symbol+" "+item_subtotal);
	        		jQuery("span.currency").text(data.currency_symbol);
	        		jQuery("td.discount").text(data.discount);
	        		jQuery("td.tax").text(data.tax);
	        		jQuery("td.adjustment").text(adjustment);
	        		jQuery("span.portal_name").text(data.portalName);
	        		jQuery(".id-due-on").text("ID: "+data.est_no+" Due on:"+data.due_date);
	        		jQuery("span.company_name").text("from "+data.admin_company.company_name);
	        		jQuery("span.company_name_2").text("from "+data.admin_company.company_name);
	        		if(data.admin_company.address!=null)
	        		{
	        			jQuery("p.company_address").html(data.admin_company.address);
	        		}
	        		var admin_company_address="";
	        		if(data.admin_company.country!=null && data.admin_company.country!="")
	        		{
	        			admin_company_address=data.admin_company.country+", ";
	        		}
	        		if(data.admin_company.state!=null && data.admin_company.state!="")
	        		{
	        			admin_company_address=admin_company_address+data.admin_company.state+"- ";
	        		}
	        		if(data.admin_company.post_code!=null && data.admin_company.post_code!="")
	        		{
	        			admin_company_address=admin_company_address+data.admin_company.post_code;
	        		}
	        		jQuery(".country-state-post-codes").text(admin_company_address);
	        		jQuery(".contact_name").text("to "+data.contact_details.first_name+" "+data.contact_details.last_name);
	        		if(data.contact_address!=null)
	        		{
	        			if(data.contact_address.street_address!=null && data.contact_address.street_address!="")
	        			{
	        				jQuery(".contact_address").text(data.contact_address.street_address);
	        			}
	        			var contact_address="";
	        			if(data.contact_address.street_address!=null && data.contact_address.street_address!="")
	        			{
	        				contact_address=contact_address+data.contact_address.street_address+", ";
	        			}
	        			if(data.contact_address.state!=null && data.contact_address.state!="")
	        			{
	        				contact_address=contact_address+data.contact_address.state+"- ";
	        			}
	        			if(data.contact_address.post_code!=null && data.contact_address.post_code!="")
	        			{
	        				contact_address=contact_address+data.contact_address.post_code;
	        			}
	        			jQuery(".contact-country-state-post-code").text(contact_address);
	        		}
	        		if(data.terms_and_conditions!=null && data.terms_and_conditions!="")
	        		{
	        			jQuery(".terms_and_conditions").text(data.terms_and_conditions);
	        		}
	        		jQuery(".inv_total").text(data.currency_symbol+" "+data.response);
	        		if(data.admin_company.logo=="" || data.admin_company.logo==null)
	        		{
	        			jQuery(".logo img").attr("src",APP_URL+'/assets/img/logo.png');
	        		}
	        		else
	        		{
	        			jQuery(".logo img").attr("src",data.admin_company.logo);
	        		}
	        		jQuery("#show-invoice-template-preview").show();
	        		jQuery(".modal-overlay").show();
	        		jQuery("#show-invoice-template-preview").css("z-index","1003");
	        		jQuery("#show-invoice-template-preview").show();
	        		// window.location.href=APP_URL+'/estimates';
        		}
        		else
        		{
        			window.location.href=APP_URL+'/estimates';
        		}
        		stopLoader(jQuery(".save-estimate"));
        		stopLoader(jQuery(".edit-estimate"));
        	}
      	},
	});
}

jQuery(".edit-estimate").click(function(event){
	event.preventDefault();
	if(estimateValidation()==false)
	{
		return false;
	}
	var status=jQuery(this).attr("data-status");
	var purpose="edit";
	var id=jQuery("input[name='estimate_id']").val();
	createEstimate('',status,purpose,id);
});

jQuery(".send-estimate").click(function(){
	jQuery(".send-estimate").html("Sending <span class='spinner'></span> <span class='checkmark'></span>");
	jQuery.ajax({
		url:APP_URL+'/send-estimate',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'estimate_id':jQuery("input[name='est_id']").val(),'estimate_subject':jQuery("input[name='invoice-subject-title']").val(),'message_to_customer':jQuery("input[name='message-to-customer']").val()},
      	type:'post',
      	success:function(data){
        	if(data.response=="success")
        	{
        		jQuery("#show-invoice-template-preview").hide();
        		window.location.href=APP_URL+'/estimates';
        	}
        	else
        	{
        		jQuery('<span class="">'+data.status+'</span>').insertAfter(".send-estimate");
        	}
        	jQuery(".send-estimate").find(".spinner").remove();
		    jQuery(".send-estimate").addClass("saved");
		    setTimeout(function(){
		        jQuery(".send-estimate").removeClass("saved");
		        jQuery(".send-estimate").html("Save");
		    }, 3000);
      	},
	});
});

//CHECK INVOICE NO
jQuery("input[name='invoice_no']").keyup(function(){
	if(jQuery(this).attr("data-purpose")!=undefined && jQuery(this).attr("data-purpose")!=null)
	{
		checkInvoiceNo(jQuery(this).attr("data-purpose"),jQuery(this).val());
	}
});

function checkInvoiceNo(purpose,inv_no)
{
	jQuery.ajax({
		url:APP_URL+'/check-invoice',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'purpose':purpose,'inv_no':inv_no},
      	type:'post',
      	success:function(data){
      		console.log(data);
        	if(data.response=="success")
        	{
        		jQuery(".invalid-message").hide();
        	}
        	else
        	{
        		jQuery(".invalid-message").show();
        	}
      	},
	});
}

jQuery(".cancel-invoice").click(function(event){
	event.preventDefault();
	window.location.href=APP_URL+'/invoices';
});

jQuery(".cancel-estimate").click(function(event){
	event.preventDefault();
	window.location.href=APP_URL+'/estimates';
});

//REINITIALIZE PRODUCT MODAL
jQuery(document).on("change","select[name='deal_id']",function(){
	jQuery.ajax({
		url:APP_URL+'/get-product-related-deal',
      	dataType:'json',
      	data:{'deal_id':jQuery(this).val()},
      	type:'get',
      	success:function(data){
      		console.log(data);
        	if(data.response.length > 0)
        	{
        		jQuery(data.response).each(function(index,element){
        			if(element != null)
        			{
        				productData[element.name] = null;
	        			var key = element.name.split(/[ ,]+/).join('_');
	        			if(jQuery.isEmptyObject(productDataObject) || !productDataObject.hasOwnProperty(key))
	        			{
	        				console.log("empty");
	        				productDataObject[key] = element.price;
	        			}
        			}
        			console.log(productDataObject);
        		});
        	}
      	},
	});
});

