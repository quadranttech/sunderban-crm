var email_pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

var signin_error=false;
jQuery("#signin_email").focusout(function(){
    var email=jQuery(this).val();
    if(jQuery(this).val()=="" || !email_pattern.test(email))
    {
      jQuery(this).closest(".input-field").addClass("input-invalid");
      jQuery(this).closest(".input-field").removeClass("input-valid");
      signin_error=true;
    }
    else
    {
      jQuery(this).closest(".input-field").removeClass("input-invalid");
        jQuery(this).closest(".input-field").addClass("input-valid");
        signin_error=false;
    }
});

jQuery("#signin_password").focusout(function(){
    var password=jQuery(this).val();
    if(jQuery(this).val()=="")
    {
      jQuery(this).closest(".input-field").addClass("input-invalid");
      jQuery(this).closest(".input-field").removeClass("input-valid");
      signin_error=true;
    }
    else
    {
      jQuery(this).closest(".input-field").removeClass("input-invalid");
        jQuery(this).closest(".input-field").addClass("input-valid");
        signin_error=false;
    }
});

setInterval(function(){ 
  if(!signin_error)
  {
    jQuery("#login-button").prop("disabled",false);
  } 
  else
  {
    jQuery("#login-button").prop("disabled",true);
  }
}, 100)

jQuery("#login-button").click(function(event){
  event.preventDefault();
  jQuery.ajax({
      url: APP_URL+'/check-user-status',
      type: 'get',
      data: {'email':jQuery("#signin_email").val(),'password':jQuery("#signin_password").val()},
      dataType: 'json',
      success: function (data) {
        if(data.response=="Inactive")
        {
          if(jQuery("#login-button").next("span.login-error").length==0)
          {
            jQuery("<span class='login-error'>Your account has been deactivated by admin</span>").insertAfter("#login-button");
          }
        }
        else
        {
          if(jQuery("#login-button").next("span.login-error").length>0)
          {
            jQuery(".login-error").hide();
          }
          jQuery("#login-form").submit();
        }
      }
  });
  
});

jQuery("#open-sign-up-form").click(function(){
  jQuery(".signin-form-wrapper").hide();
  jQuery(".signup-form-wrapper").show();
});

var signup_error=false;
jQuery("#first_name").focusout(function(){
  if(jQuery(this).val()=="")
  {
    jQuery(this).closest(".input-field").addClass("input-invalid");
    jQuery(this).closest(".input-field").removeClass("input-valid");
  }
  else
  {
    jQuery(this).closest(".input-field").removeClass("input-invalid");
    jQuery(this).closest(".input-field").addClass("input-valid");
    jQuery("#last_name").focus();
  }
});

jQuery("#last_name").focusout(function(){
  if(jQuery(this).val()=="")
  {
    jQuery(this).closest(".input-field").addClass("input-invalid");
    jQuery(this).closest(".input-field").removeClass("input-valid");
  }
  else
  {
    jQuery(this).closest(".input-field").removeClass("input-invalid");
    jQuery(this).closest(".input-field").addClass("input-valid");
  }
});

jQuery("#signup-email").focusout(function(){
  if(jQuery(this).val()=="" || !email_pattern.test(jQuery("#signup-email").val()))
  {
    jQuery(this).closest(".input-field").addClass("input-invalid");
    jQuery(this).closest(".input-field").removeClass("input-valid");
  }
  else
  {
    checkEmailExists(jQuery(this).val());
    
  }
});

jQuery("#company_name").focusout(function(){
  if(jQuery(this).val()=="")
  {
    jQuery(this).closest(".input-field").addClass("input-invalid");
    jQuery(this).closest(".input-field").removeClass("input-valid");
  }
  else
  {
    jQuery(this).closest(".input-field").removeClass("input-invalid");
    jQuery(this).closest(".input-field").addClass("input-valid");
  }
});

jQuery("#signup-password").focusout(function(){
  if(jQuery(this).val()=="")
  {
    jQuery(this).closest(".input-field").addClass("input-invalid");
    jQuery(this).closest(".input-field").removeClass("input-valid");
  }
  else
  {
    jQuery(this).closest(".input-field").removeClass("input-invalid");
    jQuery(this).closest(".input-field").addClass("input-valid");
  }
});

jQuery("#password-confirm").focusout(function(){
  if(jQuery(this).val()=="" || jQuery("#signup-password").val()!=jQuery(this).val())
  {
    jQuery(this).closest(".input-field").addClass("input-invalid");
    jQuery(this).closest(".input-field").removeClass("input-valid");
  }
  else
  {
    jQuery(this).closest(".input-field").removeClass("input-invalid");
    jQuery(this).closest(".input-field").addClass("input-valid");
  }
});

setInterval(function(){ 
  if(jQuery("#first_name").val()!="" && jQuery("#last_name").val()!="")
  {
    jQuery("#full_name").val(jQuery("#first_name").val()+" "+jQuery("#last_name").val());
  }
  if(jQuery("#first_name").val()!="" && jQuery("#last_name").val()!="" && jQuery("#signup-email").val()!="" && jQuery("#signup-password").val()!="" && jQuery("#password-confirm").val()!="" && jQuery("#signup-password").val()==jQuery("#password-confirm").val() && signup_error==false)
  {
    jQuery("#signup-button").prop("disabled",false);
  }
  else
  {
    jQuery("#signup-button").prop("disabled",true);
  }
}, 100)

jQuery("#back-to-login").click(function(){
  jQuery(".signin-form-wrapper").show();
  jQuery(".signup-form-wrapper").hide();
});

jQuery("#signup-button").click(function(){
  if(jQuery(this).attr("data-signup")=="install")
  {
    jQuery.ajax({
      url: APP_URL+'/ajax/create-user',
      type: 'post',
      data: {'_token':jQuery("input[name='_token']").val(),'first_name':jQuery("input[name='first_name']").val(),'last_name':jQuery("input[name='last_name']").val(),'email':jQuery("input[name='email']").val(),'password':jQuery("input[name='password']").val()},
      dataType: 'json',
      success: function (data) {
        if(data.response)
        {
          jQuery(".form-two").hide();
          jQuery(".form-three").show();
          jQuery(".step-slider").find("span.step").removeClass("half").addClass("full");
          jQuery(".three").addClass("active");
          jQuery(".two").addClass("activate");
          jQuery(document).prop('title', 'dealpipe CRM Installation Step 3');
        }
      }
    });
  }
  else
  {
    jQuery("#signup-form").submit();
  }
});

function checkEmailExists(email)
{
  var ajax_url=APP_URL+'/ajax/checkEmailExists';
  jQuery.ajax({
    url: ajax_url,
    type: 'get',
    data: {email: email},
    dataType: 'JSON',
    success: function (data) {
      if(data.response=="")
      {
        jQuery("#signup-email").closest(".input-field").removeClass("input-invalid");
        jQuery("#signup-email").closest(".input-field").addClass("input-valid");
        signup_error=false;
      }
      else
      {
        jQuery("#signup-email").closest(".input-field").addClass("input-invalid");
        jQuery("#signup-email").closest(".input-field").removeClass("input-valid");
        signup_error=true;
      }
    }
  });
}

jQuery("#sendResetPasswordEmail").click(function(){
  event.preventDefault();
  var ajax_url=APP_URL+'/ajax/resetPasswordEmail';
  if(jQuery("#signin_email").val()=="")
  {
    jQuery("#signin_email").parent(".input-field").addClass("input-invalid");
    return false;
  }
  else
  {
    jQuery(".showbox-wrapper").show();
    jQuery.ajax({
      url: ajax_url,
      type: 'get',
      data: {email: jQuery("#signin_email").val()},
      dataType: 'JSON',
      success: function (data) {
        if(data.response=="success")
        {
          jQuery(".error-message").text("");
          jQuery(".success-message").show();
        }
        else
        {
          jQuery(".error-message").text(data.response);
        }
        jQuery(".showbox-wrapper").hide();
      }
    });
  }
});

//RESET PASSWORD
setInterval(function(){ 
  if(jQuery("#new-password").val()=="" || jQuery("#confirm-password").val()=="" || jQuery("#new-password").val()!=jQuery("#confirm-password").val())
  {
    jQuery("#reset-password").prop("disabled",true);
  }
  else
  {
    jQuery("#reset-password").prop("disabled",false);
  }
}, 300)

jQuery("#reset-password").click(function(event){
  event.preventDefault();
  jQuery.ajax({
    url: APP_URL+'/reset-password',
    type: 'post',
    data: {'_token':jQuery("input[name='_token']").val(),'token':jQuery("input[name='token']").val(),'email':jQuery("input[name='email']").val(),'password':jQuery("#new-password").val()},
    dataType: 'JSON',
    success: function (data) {
      if(data.response=="success")
      {
        jQuery("#reset-password").hide();
        jQuery(".success-password-reset").show();
      }
    }
  });
});

jQuery("#file").change(function(){
  readURL(this);
  jQuery('#cropimage').Jcrop({
      onSelect: updateCoords
  });
  jQuery(".modal-overlay").show();
  jQuery("#image-cropper").show();
});

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#cropimage').attr('src', e.target.result);
            $('input[name="image"]').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

jQuery('#cropimage').Jcrop({
    onSelect: updateCoords
});

function updateCoords(c) {
    jQuery('#x').val(c.x);
    jQuery('#y').val(c.y);
    jQuery('#w').val(c.w);
    jQuery('#h').val(c.h);
};

jQuery(document).on("click",".image-crop-signin",function(){
  jQuery(".modal-overlay").hide();
  jQuery(".showbox-wrapper").show();
  jQuery.ajax({
    url:APP_URL+'/upload-company-logo-signin',
    dataType:'json',
    async:true,
    data:new FormData($("#company-form")[0]),
    type:'post',
    processData: false,
    contentType: false,
    success:function(data){
      jQuery("input[name='uploaded_image_name']").val(data.fileName);
      jQuery("#image-cropper").hide();
      jQuery("#result").attr("src",APP_URL+'/uploads/company-logo/'+data.fileName);
      jQuery(".showbox-wrapper").hide();
    },
  });
});