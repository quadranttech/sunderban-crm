//ERROR VALIDATION
var createTask=false;
jQuery("input[name='task_title']").focusout(function(){
	if(jQuery(this).val()=="")
	{
		jQuery(this).closest(".input-field").addClass("input-invalid");
      	jQuery(this).closest(".input-field").removeClass("input-valid");
      	createTask=false;
	}
	else
	{
		jQuery(this).closest(".input-field").removeClass("input-invalid");
        jQuery(this).closest(".input-field").addClass("input-valid");
        createTask=true;
	}
});

jQuery("input[name='task_details']").focusout(function(){
	if(jQuery(this).val()=="")
	{
		jQuery(this).closest(".input-field").addClass("input-invalid");
      	jQuery(this).closest(".input-field").removeClass("input-valid");
      	createTask=false;
	}
	else
	{
		jQuery(this).closest(".input-field").removeClass("input-invalid");
        jQuery(this).closest(".input-field").addClass("input-valid");
        createTask=true;
	}
});

setInterval(function(){ 
	disableSubmit();
}, 300)

function disableSubmit()
{
	if(createTask==true)
	{
		jQuery(".createTask").prop("disabled",false);
	}
	else
	{
		jQuery(".createTask").prop("disabled",true);
	}
}

jQuery(".createTask").click(function(event){
	event.preventDefault();
	if(jQuery("input[name='task_title']").val()=="" || jQuery("input[name='task_details']").val()=="")
	{
		
		jQuery(".createTask").prop("disabled",true);
	}
	if(jQuery("select[name='related_deal']").val()=="" || jQuery("select[name='related_deal']").val()==null)
	{
		jQuery('<span class="error-message">Please Select At Least One Deal</span>').insertAfter(".createTask");
		return false;
	}
	else
	{
		startLoader(jQuery(".createTask"));
		var task_title=jQuery("input[name='task_title']").val();
		var task_details=jQuery("input[name='task_details']").val();
		var assigned_to=jQuery("select[name='assigned_to']").val();
		var due_date=jQuery("input[name='due_date']").val();
		var related_deal=jQuery("#related_deal :selected").val();
		var contact_id=jQuery("input[name='contact_id']").val();
		var deal_id=related_deal;
		if(contact_id==0)
		{
			var feed_type="deal";
		}
		else
		{
			var feed_type="contact";
		}
		var task_id=12;
		console.log(contact_id);
		console.log(deal_id);
		console.log(related_deal);
		console.log(feed_type);
		jQuery.ajax({
		    url: APP_URL+'/save-new-task',
		    type: 'post',
		    data: {'_token':jQuery("input[name='_token']").val(),task_title:task_title,task_details:task_details,assigned_to:assigned_to,due_date:due_date,related_deal:related_deal,contact_id:contact_id,deal_id:related_deal,feed_type:feed_type},
		    dataType: 'JSON',
		    success: function (data) {
		      jQuery(".demo-task").remove();
		      jQuery(".tasks-list").removeClass("demo-content");
		      stopLoader(jQuery(".createTask"));
		      setTask();
		    }
	    });
	}
});

jQuery(".contexual-task-cancel").click(function(event){
	event.preventDefault();
	jQuery(document).find(".modal-close").trigger("click");
});

function setTask()
{
	jQuery.ajax({
	    url: 'setTask',
	    type: 'post',
	    data: {'_token':jQuery("input[name='_token']").val()},
	    dataType: 'JSON',
	    success: function (data) {
	      jQuery(".tasks-list").prepend(data.content);
	      jQuery(".tasks:first").find("input[name='markTask']").attr("id","id-"+parseInt(jQuery(".tasks-list .tasks").length)).val(data.tasks[0].id);
	      jQuery(".tasks:first").find("span:first").addClass("border-right "+data.tasks[0].color);
	      jQuery(".tasks:first").find("label").attr("id","id-0").text(data.tasks[0].task_title);
	      jQuery(".tasks:first").find("div.task-assignee").text(data.tasks[0].assigned_to);
	      if(data.tasks[0].tagsNameArray.length>0)
	      {
	      	var count=0;
	      	var tagsNameHtml="";
	      	while(count<data.tasks[0].tagsNameArray.length)
	      	{
	      		tagsNameHtml=tagsNameHtml+'<span class="tags '+data.tasks[0].tagsColorArray[count]+'">'+data.tasks[0].tagsNameArray[count]+'</span>';
	      		count++;
	      	}
	      	jQuery(".tasks:first").find(".tag-container").html(tagsNameHtml);
	      	jQuery(".tasks:first").find(".mobile-tag-container").html(tagsNameHtml);
	      }
	      jQuery(".tasks:first").find(".tasks-priority").html(data.tasks[0].due_date);
	      jQuery(".tasks:first").find(".tasks-priority-view").html(data.tasks[0].due_date);
	      jQuery(".tasks:first").find("a.icons").addClass("more-actions edit edit-task").text("Edit").attr("data-edit-task-id",data.tasks[0].id);
	      jQuery(".tasks:first").find(".tasks-priority").addClass(data.tasks[0].color);
	      jQuery(".tasks:first").find(".tasks-priority-view").addClass(data.tasks[0].color);
	      jQuery(".tasks:first").attr("data-task-id",data.tasks[0].id);
	      jQuery(".tasks").find("input[name='markTask']").each(function(index,element){
	      	jQuery(element).attr("id","id-"+index);
	      });
	      jQuery(".tasks").each(function(index,element){
	      	jQuery(element).find("label").attr("for","id-"+index);
	      });
	      jQuery(".modal-close").trigger("click");
	    }
    });
}

//MARKED AS COMPLETE ON CLICK TASK
jQuery(document).on("change","input[name='markTask']",function(){
	if(jQuery(this).is(":checked"))
	{
		markComplete(jQuery(this).val());
	}
});

jQuery(document).on("click",".complete",function(){
	markComplete(jQuery(this).parents("ul").find("li:first a").attr("data-edit-task-id"));

});

//CHANGE DUE DATE
jQuery(document).on("click",".change-date",function(){
	jQuery(".edit-task").trigger("click");
});

// MORE TASKS
var tasks_index=0;

var mouse_is_inside = false;
$('.more-task-popup').hover(function(){ 
    mouse_is_inside=true; 
}, function(){ 
    mouse_is_inside=false; 
});
$("body").mouseup(function(){ 
    if(! mouse_is_inside) $('.more-task-popup').hide();
});

var edit_task="close";
jQuery(document).on("click",".edit-task",function(event){
	tasks_index=jQuery(this).parents(".tasks").index();
	jQuery(".more-task-popup").hide();
	var task_id=jQuery(this).attr("data-edit-task-id");
	var target=jQuery(this);
	jQuery(".edit-tasks").remove();
	jQuery(".tasks-short-description").show();
	jQuery(".showbox-wrapper").show();
	var ajax_url=APP_URL+'/ajax/getTaskTemplate';
	var appendHtml='';
	jQuery.ajax({
	    url: ajax_url,
	    type: 'get',
	    data: {task_id: task_id},
	    dataType: 'JSON',
	    success: function (data) {
	      edit_task="open";
	      getUser(tasks_index,data.response,task_id);
	      
	    }
    });
});

var taskTagColorArray=['red','pink','purple','indigo','blue','green','light-green','orange','deep-orange','teal','cyan'];

function getUser(index,editTaskContent,task_id)
{
	var ajax_url=APP_URL+'/ajax/getUser';
	var response=[];
	jQuery.ajax({
	    url: ajax_url,
	    type: 'get',
	    dataType: 'JSON',
	    success: function (data) {
	      response=data;
	      
	      var inner_ajax_url=APP_URL+'/ajax/getTaskTagById';
	      jQuery.ajax({
		    url: inner_ajax_url,
		    type: 'get',
		    data: {task_id:task_id},
		    dataType: 'JSON',
		    success: function (data) {
		    	var taskTagArray=data.structuredTaskTagsArray;
		    	
	    		jQuery("div.tasks:eq("+index+")").find(".tasks-short-description").hide();
		      	jQuery("div.tasks:eq("+index+")").append(editTaskContent);
		      	jQuery("#task-description-id").val(data.tasks.task_description);
		      	jQuery("#task-title").val(data.tasks.task_title);
		      	var count=0;
		      	var optionHtml='';
		      	while(count<response.length)
		      	{
			      	if(response[count].id==data.tasks.assigned_to)
			      	{
			      		optionHtml=optionHtml+'<option value="'+response[count].id+'" selected>'+response[count].first_name+'</option>';
			      	}
			      	else
			      	{
			      		optionHtml=optionHtml+'<option value="'+response[count].id+'">'+response[count].first_name+'</option>';
			      	}
			      	count++;
		      	}
		      	jQuery("#mark-complete").val(task_id);
		      	jQuery("select[name='assign_to']").html(optionHtml);
		      	jQuery('select').material_select();
		      	if(taskTagArray.length>0)
		    	{
		    		var taskTagArrayCount=0;
		    		var taskTagArrayHtml='';
		    		while(taskTagArrayCount<taskTagArray.length)
		    		{
		    			taskTagArrayHtml=taskTagArrayHtml+'<span class="tags '+taskTagArray[taskTagArrayCount].color+'">'+taskTagArray[taskTagArrayCount].tag+' <a href="#!" class="remove-tags" data-task-tag-id='+taskTagArray[taskTagArrayCount].id+' data-task-id='+task_id+'></a></span>';
		    			taskTagArrayCount++;
		    		}
		    		jQuery(".tag-container-child").html(taskTagArrayHtml);
		    	}
		    	jQuery(".task-removed").attr("data-task-id",task_id);
		    	var date = new Date();
		    	if(convertDate(data.tasks.due_date.split(" ")[0])!="00-00-0000")
		    	{
		    		date = data.tasks.due_date.split(" ")[0];
		    	}
				var timeAndDate = moment(date);
				console.log(timeAndDate);
		    	jQuery("#date-fr").bootstrapMaterialDatePicker({
	        		format: 'DD-MMMM-YYYY',
	        		weekStart : 0, 
	        		lang : 'en',
	        		cancelText : 'Cancel',
			        time: false ,
			        currentDate: timeAndDate
	      		});
	      		jQuery(".tasks[data-task-id='" +task_id+ "']").addClass("task-edited");
	      		jQuery(".showbox-wrapper").hide();
		    }
		  });
	      
	      
	    }
    });
}

function convertDate(dateString){
var p = dateString.split(/\D/g)
return [p[2],p[1],p[0] ].join("-")
}

jQuery(document).on("change","#date-fr",function(){
	var refreshIntervalId = setInterval(function(){ 
		if(typeof jQuery(".tasks-list .tasks-priority input#date-fr").val()!="undefined")
		{
			if(jQuery(".tasks-list .tasks-priority input#date-fr").val()!="")
			{
				var due_date_array=jQuery(".tasks-list .tasks-priority input#date-fr").val().split("-");
				var monthNumber=getMonthFromString(due_date_array[1]);
				if(monthNumber<10)
				{
					monthNumber='0'+monthNumber;
				}
				var dateNumber=due_date_array[0];
				var yearNumber=due_date_array[2];
				var time=due_date_array[5];
				jQuery.ajax({
				    url: 'set-due-date',
				    type: 'post',
				    data: {'_token':jQuery("input[name='_token']").val(),'monthNumber':monthNumber,'dateNumber':dateNumber,'yearNumber':yearNumber,'task_id':jQuery(document).find(".task-edited").attr("data-task-id")},
				    dataType: 'JSON',
				    success: function (data) {
				      if(data.response==1)
				      {
				      	jQuery(".tasks[data-task-id='"+jQuery(document).find(".task-edited").attr("data-task-id")+"']").find(".tasks-priority").text(data.difference);
				      	jQuery(".tasks-list .tasks-priority input#date-fr").val();
				      }
				    }
			    });
				clearInterval(refreshIntervalId);
			}
			
		} 
	}, 300)
});

function getMonthFromString(mon){
   return new Date(Date.parse(mon +" 1, 2017")).getMonth()+1
}

function setDateTime()
{
	if(jQuery("#date-fr").val()!=null)
	{
		var monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN",
		  "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
		];

		var d = new Date("2017-06-28");
		jQuery(".dtp-actual-month:last").html(monthNames[d.getMonth()]);
		jQuery(".dtp-actual-num").html(d.getDate());
		jQuery(".year-picker-item[data-year='"+d.getDate()+"']").addClass("active");
		jQuery(".dtp-actual-year").html(d.getDate());
		jQuery(".dtp-picker-days").find("td[data-date='"+d.getDate()+"'] a").addClass("selected");
	}
}

function getMonthFromString(mon){
   return new Date(Date.parse(mon +" 1, 2012")).getMonth()+1
}

jQuery(document).on("click","#mark-complete",function(){
	markComplete(jQuery(this).val());
});

function markComplete(task_id)
{
	var ajax_url=APP_URL+'/ajax/markedCompleteTask';
	jQuery(".showbox-wrapper").show();
	jQuery.ajax({
	    url: ajax_url,
	    type: 'get',
	    data: {task_id: task_id},
	    dataType: 'JSON',
	    success: function (data) {
	      jQuery(".showbox-wrapper").hide();
	      if(data.response=="success")
	      {
	      	var target=jQuery(".tasks[data-task-id='"+task_id+"']");
	      	target.addClass("task-complete");
	      	target.find(".edit-tasks").remove();
	      	target.find(".tasks-short-description").show();
	      	target.find(".edit-task").remove();
	      	target.find("input[name='markTask']").attr({disabled:true,checked:true});
	      }
	    }
    });
}

//CHANGE ASSIGNED TO
jQuery(document).on("click",".assign-task",function(){
	jQuery(".edit-task").trigger("click");
});
jQuery(document).on("change","div.tasks #change-assigned-to",function(){
	jQuery(".showbox-wrapper").show();
	var ajax_url=APP_URL+'/ajax/changeAssignedTo';
	jQuery.ajax({
	    url: ajax_url,
	    type: 'get',
	    data: {task_id: jQuery("#mark-complete").val(),assigned_to:jQuery(this).val()},
	    dataType: 'JSON',
	    success: function (data) {
	      jQuery(".showbox-wrapper").hide();
	      if(data.response==1)
	      {
	      	jQuery(".tasks[data-task-id='"+jQuery("#mark-complete").val()+"']").find(".task-assignee").html('Assigned to '+data.assigned_to.first_name+' '+data.assigned_to.last_name);
	      }
	    }
    });
});

//ADD NEW TAG TO TASK
jQuery(document).on("click","div.tasks #add-new-tag",function(){
	var ajax_url=APP_URL+'/ajax/addNewTagToTask';
	var tag=jQuery("input[name='add_new_tag']").val();
	tag=tag.toUpperCase();
	var target=jQuery(".tasks[data-task-id='"+jQuery("#mark-complete").val()+"']");
	var tag_slug=tag.toLowerCase();
	if(tag!="")
	{
		jQuery(".showbox-wrapper").show();
		jQuery.ajax({
		    url: ajax_url,
		    type: 'get',
		    data: {task_id: jQuery("#mark-complete").val(),tag:tag,tag_slug:tag_slug},
		    dataType: 'JSON',
		    success: function (data) {
		      jQuery(".showbox-wrapper").hide();
		      jQuery(target).find(".edit-tasks .tag-container-child").append('<span class="tags '+data.color+'">'+tag+' <a href="#!" class="remove-tags" data-task-tag-id="'+data.tag_id+'" data-task-id="'+jQuery("#mark-complete").val()+'"></a></span>');
		      jQuery("div.tasks input[name='add_new_tag']").val("");
		      if(target.find(".tasks-short-description .tag-container .tags").length==3)
		      {
		      	if(target.find(".tasks-short-description .tag-container .more-tags").length>0)
		      	{
		      		var previous_task=parseInt(target.find(".tasks-short-description .tag-container .more-tags").attr("data-more-task"));
			      	previous_task=previous_task+1;
			      	target.find(".tasks-short-description .tag-container .more-tags").html(previous_task+' more tags');
			      	target.find(".tasks-short-description .tag-container .more-tags").attr("data-more-task",previous_task);
		      	}
		      	else
		      	{
		      		target.find(".tasks-short-description .tag-container").append('<span class="more-tags" data-more-task="1">1 more tag</span>');
		      	}
		      }
		      else
		      {
		      	target.find(".tasks-short-description .tag-container").append('<span class="tags '+data.color+' '+data.tag+'">'+data.tag+'</span>');
		      }
		    }
	    });
	}
});

//CHANGE TASK TITLE
jQuery(document).on('focusout','#task-title',function() {
	var ajax_url=APP_URL+'/ajax/changeTaskTitle';
	jQuery.ajax({
	    url: ajax_url,
	    type: 'get',
	    data: {task_title: jQuery(this).val(),task_id: jQuery("#mark-complete").val()},
	    dataType: 'JSON',
	    success: function (data) {
	    }
    });
});

jQuery(document).on('focusout','#task-description-id',function(){
	var ajax_url=APP_URL+'/ajax/changeTaskDescription';
	jQuery.ajax({
	    url: ajax_url,
	    type: 'get',
	    data: {task_description: jQuery(this).val(),task_id: jQuery("#mark-complete").val()},
	    dataType: 'JSON',
	    success: function (data) {
	    }
    });
});

//EDIT TASK
jQuery(document).on("click",".editTask",function(){
	startLoader(jQuery(".editTask"));
	setTimeout(function(){ 
		stopLoader(jQuery(".editTask")); 
	}, 2000);
});

//REMOVE TAG FROM TASK
jQuery(document).on("click",".remove-tags",function(){
	var task_tag_id=jQuery(this).attr("data-task-tag-id");
	var task_tag=jQuery(this).closest("span").text();
	var ajax_url=APP_URL+'/ajax/';
	jQuery.ajax({
	    url: APP_URL+'/removeTagFromTask',
	    type: 'get',
	    data: {task_tag_id: jQuery(this).attr("data-task-tag-id"),task_id: jQuery("#mark-complete").val()},
	    dataType: 'JSON',
	    success: function (data) {
	      if(data.response==1)
	      {
	      	jQuery("a[data-task-tag-id='"+task_tag_id+"']").parent("span.tags").remove();
	      	if(jQuery(".tasks[data-task-id='"+jQuery("#mark-complete").val()+"'] .tasks-short-description .tag-container").find(".more-tags").length>0)
	      	{
	      		if(jQuery(".tasks[data-task-id='"+jQuery("#mark-complete").val()+"'] .tasks-short-description .tag-container").find(".more-tags").attr("data-more-task")>1)
	      		{
	      			var new_task_tag_no=parseInt(jQuery(".tasks[data-task-id='"+jQuery("#mark-complete").val()+"'] .tasks-short-description .tag-container").find(".more-tags").attr("data-more-task"))-1;
	      			jQuery(".tasks[data-task-id='"+jQuery("#mark-complete").val()+"'] .tasks-short-description .tag-container").find(".more-tags").attr("data-more-task",new_task_tag_no);
	      			jQuery(".tasks[data-task-id='"+jQuery("#mark-complete").val()+"'] .tasks-short-description .tag-container").find(".more-tags").text(new_task_tag_no+" more tags");
	      		}
	      		else
	      		{
	      			jQuery(".tasks[data-task-id='"+jQuery("#mark-complete").val()+"'] .tasks-short-description .tag-container").find(".more-tags").remove();
	      		}
	      	}
	      	jQuery(".tasks[data-task-id='"+jQuery("#mark-complete").val()+"'] .tasks-short-description .tag-container").find("span."+task_tag).remove();
	      }
	    }
    });
});

//REMOVE TASK
jQuery(document).on("click",".task-removed",function(){
	var ajax_url=APP_URL+'/ajax/removeTask';
	jQuery.ajax({
	    url: ajax_url,
	    type: 'get',
	    data: {task_id: jQuery(this).attr("data-task-id")},
	    dataType: 'JSON',
	    success: function (data) {
	      if(data.response==1)
	      {
	      	jQuery(".tasks .edit-tasks").parent(".tasks").remove();
	      }
	    }
    });
});

//CLOSE EDIT TASK ON OUTCLICK
jQuery(document).on("click","body > div",function(event) {
    if(jQuery(event.target).closest(".edit-tasks").length==0)
    {
    	jQuery(".edit-tasks").prev(".tasks-short-description").show();
    	jQuery(".edit-tasks").closest(".tasks").find(".tasks-short-description label:first").text(jQuery(".edit-tasks input[name='task_title']").val());
    	jQuery(".edit-tasks").remove();
    	jQuery(".tasks-short-description").show();
    	// edit_task="close";
    }
    jQuery(".tasks").removeClass("task-edited");
});

//DELTE TASK
jQuery(".deleteContact").click(function(){
	var ajax_url=APP_URL+'/ajax/deleteContact';
	jQuery.ajax({
	    url: ajax_url,
	    type: 'get',
	    data: {contact_id: jQuery(this).attr("data-task-id")},
	    dataType: 'JSON',
	    success: function (data) {
	      if(data.response==1)
	      {
	      	jQuery(this).parents(".content-table-row").remove();
	      }
	    }
    });
	
});

//SHOW TASK IN CONTEXUAL
jQuery("#show-contexual-task").click(function(){
	jQuery(".tasks").remove();
	jQuery(jQuery(this).attr("href")+" a").hide();
	var href=jQuery(this).attr("href");
	// window.location.hash="#task";
	history.pushState('obj', 'task', APP_URL+'/edit-contact/'+window.location.href.split("edit-contact/").pop().substr(0, window.location.href.split("edit-contact/").pop().indexOf('?'))+'?page='+'task');
	jQuery(".showbox-wrapper").show();
	jQuery.ajax({
	    url: 'get-contexual-task',
	    type: 'post',
	    data: {'_token':jQuery("input[name='_token']").val(),contact_id: jQuery("input[name='contact_id']").val()},
	    dataType: 'JSON',
	    success: function (data) {
	      jQuery(".showbox-wrapper").hide();
	      var count=0;
	      var contexual_task_li_html="";
	      while(count<data.response.length)
	      {
	      	contexual_task_li_html=contexual_task_li_html+data.content;
	      	count++;
	      }
	      jQuery(".tasks-list").prepend(contexual_task_li_html);
	      jQuery(href+" a").show();
	      jQuery(".tasks").each(function(index,element){
	      	jQuery(element).find("input[name='markTask']").attr("id","id-"+index).val(data.response[index].id);
	      	jQuery(element).find("label").attr('for',"id-"+index);
	      	jQuery(element).find("a.icons").addClass("more-actions edit edit-task").text("Edit").attr("data-edit-task-id",data.response[index].id);
	      	jQuery(element).find("span:first").addClass("border-right "+data.response[index].color);
	      	jQuery(element).find("label:first").text(data.response[index].task_title);
	      	jQuery(element).find(".task-assignee").text(data.response[index].assigned_to);
	      	if(data.response[index].tagsNameArray.length>0)
	      	{
	      		var tagCount=0;
	      		var tagsNameHtml="";
	      		while(tagCount<data.response[index].tagsNameArray.length)
	      		{
	      			if(tagCount<3)
	      			{
	      				tagsNameHtml=tagsNameHtml+'<span class="tags '+data.response[index].tagsColorArray[tagCount]+'">'+data.response[index].tagsNameArray[tagCount]+'</span>';
	      			}
	      			tagCount++;
	      		}
	      		if(data.response[index].tagsNameArray.length>3)
	      		{
	      			var more_tag=parseInt(data.response[index].tagsNameArray.length)-3;
	      			tagsNameHtml=tagsNameHtml+'<span class="more-tags" data-more-task="'+more_tag+'">'+more_tag+' more tags</span>';
	      		}
	      		jQuery(element).find(".tag-container").html(tagsNameHtml);
	      		if(data.agent=="mobile")
	      		{
	      			jQuery(element).find(".mobile-tag-container").html(tagsNameHtml);
	      		}
	      	}
	      	jQuery(element).find(".tasks-priority").text(data.response[index].due_date).addClass(data.response[index].color);
	      	if(data.agent=="mobile")
	      	{
	      		jQuery(element).find(".tasks-priority-view").text(data.response[index].due_date).addClass(data.response[index].color);
	      	}
	      	jQuery(element).attr("data-task-id",data.response[index].id);
	      	if(data.permission.task.add=="on")
	      	{
	      		jQuery(".add-icon").show();
	      	}
	      	if(data.response[index].marked_as=="done")
	      	{
	      		jQuery(element).addClass("task-complete");
	      		jQuery(element).find(".edit-task").remove();
	      	}
	      });
	    }
    });
});

jQuery(".show-notification").click(function(){
	jQuery.ajax({
	    url: APP_URL+'/read-all-notification',
	    type: 'get',
	    data: {'user_2':jQuery("input[name='user_2']").val()},
	    dataType: 'JSON',
	    success: function (data) {
	    	console.log(data);
	      	jQuery(".show-notification span:first").text("");
	    }
    });
});

jQuery("a[href='#modal3']").click(function(){
	jQuery("#modal3").find("input[name='task_title']").parent(".input-field").removeClass("input-valid input-invalid");
	jQuery("#modal3").find("input[name='task_details']").parent(".input-field").removeClass("input-valid input-invalid");
	jQuery("#modal3").find("label").removeClass("active");
	document.getElementById("add-new-task-form").reset();
});

//SHOW TASK ASSIGNED TO ME
jQuery("select[name='show-task-assigned-to-me']").on("change",function(){
	if(jQuery(this).val()=="assigned_to_me")
	{
		jQuery(".tasks-list .tasks").hide();
		jQuery(".tasks-list .own-task").show();
	}
	else
	{
		jQuery(".tasks-list .tasks").show();
	}
});