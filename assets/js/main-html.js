

 $(".top-menu a").on("click", function(){
      $(".top-menu").find(".active").removeClass("active");
      $(this).parent().addClass("active");
   });


// OPEN SEARCH BOX
$(".search-icon").click(function(){
	$(".serach-wrapper").toggleClass("serach-wrapper-open");
    $(".search-backdrop").toggleClass("backdropshow");
    $("button.notify-icon").toggleClass("notifyhide");
    $("a.search-cross").toggleClass("search-cross-show");
    
    
});

$(".wrapper").css("padding-top",$( "header.header").height());


// PROFILE USER
$(".profile-usermenu").css("top",$( "header.header").height());


// $(".profile").click(function(){
//     $(".profile-usermenu").show();
// });




$( "header.header").height();

$(".search-icon").click(function(){
    $(".contents").toggleClass("contentopcaity");
});

$('.dropdown-button').dropdown('open');


// OPEN CONTACT SCREEN
$(".select-screen").click(function(){
	$(".screens").addClass("screen-selected");
	$("#contact-screen").css('display', 'block');
	$("#select-screen").css('display', 'none');
});

// MATERIAL SELECT
$('select').material_select();

// DRAG AND DROP
$( "#sortable" ).sortable({
  	placeholder: "ui-state-highlight"
});
$( "#sortable" ).disableSelection();


// MORE TASKS
// $('.tasks .icons').on('click', function(e) {
//     e.preventDefault();

//     $('.more-task-popup').toggleClass('more-task-popup-open');
//     setTimeout(function(){ $('.more-task-popup').show(); }, 50)
    
// });

var mouse_is_inside = false;
$('.more-task-popup').hover(function(){ 
    mouse_is_inside=true; 
}, function(){ 
    mouse_is_inside=false; 
});
$("body").mouseup(function(){ 
    if(! mouse_is_inside) $('.more-task-popup').hide();
});

// NOTIFICATION
$('.notification').click (function(){ 
    $('.notification-wrapper').toggleClass('notification-open');
});

// CLONE ROW
$('.im-profile-form-wrapper .add-input').click (function(){ 
	$( $('#clonable-iM').html() ).appendTo('.im-profile-form-wrapper');
});

$('.social-profile-form-wrapper .add-input').click (function(){ 
	$( $('#social-profile').html() ).appendTo('.social-profile-form-wrapper');
});

$('.address-form-wrapper .add-input').click (function(){ 
	$( $('#clone-address').html() ).appendTo('.address-form-wrapper');
});

// OPEN INFORMATIONS SECTION
 $(".address-information, .im-profile, .social-profile, .add-company-contact").hide();

$(".open-address").click(function(){
    $(".address-information").slideToggle("slow");
});

$(".open-im").click(function(){
    $(".im-profile").slideToggle("slow");
});

$(".open-social").click(function(){
    $(".social-profile").slideToggle("slow");
});

$(".add-contact-company").click(function(){
    $(".add-company-contact").slideToggle("slow");
});

// WRONG PASSWORD OPEN EMAIL
$(".submit-button > button.teal-lighten-one").hide();
$("span.wrong-pass a.need-help").click(function(){
    $(".wrong-password").addClass("hide-pass animated fadeOut");
    $(".submit-button .orange").addClass("animated fadeOut");
    $(".add-email").addClass("email-full");
    $(".submit-button > button").hide();
    $(".submit-button > button.teal-lighten-one").show(700);
    // $(".submit-button > button.teal-lighten-one").animate({ opacity: 1 }, 5000);
});


// SIGN UP FORM OPEN
$(".signin-form-wrapper .newuser a").click(function(){
    $(".signin-form-wrapper").animate({ opacity: 0 }, 1000);
    $(".signup-form-wrapper").animate({ opacity: 1 }, 3000);
    $(".signup-form-wrapper").css('z-index', '2');
});

// SIGN IN FORM OPEN
$(".signup-form-wrapper .newuser a").click(function(){
    $(".signin-form-wrapper").animate({ opacity: 1 }, 3000);
    $(".signup-form-wrapper").animate({ opacity: 0 }, 3000);
});


// TAB

$( "#tabs" ).tabs();


// TAB DOCUMENT DRAG NAME

'use strict';

$('.input-file').each(function() {
    var $input = $(this),
        $label = $input.next('.drag-label'),
        labelVal = $label.html();
    
    $input.on('change', function(element) {
        var fileName = '';
        if (element.target.value) fileName = element.target.value.split('\\').pop();
        fileName ? $label.addClass('has-file').find('.file-name').html(fileName) : $label.removeClass('has-file').html(labelVal);
    });
});

// TAB NOTE

// $(".notes-form").hide();

$(document).on("click",".notes",function(){
    $(".notes-form").animate({ opacity: 1 }, 1500);
    $(".note-lists").animate({ opacity: 0 }, 1000);
    $(".note-lists").css('z-index', '0');
    console.log(jQuery(this).parent("li.note-li").attr("note-id"));
    var note_id=jQuery(this).parent("li.note-li").attr("note-id");
    jQuery("input[name='add-attachment']").val("");
    if(typeof note_id !== "undefined")
    {
        jQuery.ajax({
            url:'getNoteById',
            data:{'_token':jQuery("input[name='_token']").val(),note_id:note_id},
            dataType:'json',
            type:'post',
            success:function(data){
              console.log(data);
              var count=0;
              var attachment_html='';
              while(count<data.attachment.length)
              {
                attachment_html=attachment_html+'<a href="#!"><span>'+data.attachment[count]+'</span></a>';
                count++;
              }
              if(data.response!= null)
              {
                jQuery(".attachment").html(attachment_html);
                jQuery("#textarea1").val(data.response.note);
                jQuery("#first_name").val(data.response.note_title);
                jQuery("input[name='current_note_id']").val(data.response.id);
              }
            },
        });
    }
    
    // $(".note-lists").addClass("note-list-hide");
    // $(".note-lists").hide();
});

$(".note-close").click(function(){
    $(".notes-form").animate({ opacity: 0 }, 1000);
    $(".note-lists").animate({ opacity: 1 }, 1500);
    $(".note-lists").css('z-index', '2');
});





$(".minimize").click(function(){
    $(this).closest(".ui-state-default").find(".maximize").css('display', 'inline-block');
    $(this).closest(".ui-state-default").find(".minimize").css('display', 'none');

    $(this).closest(".ui-state-default").find(".widgets-collapse").slideUp("slow");
});

$(".maximize").click(function(){
    $(this).closest(".ui-state-default").find(".minimize").css('display', 'inline-block');
    $(this).closest(".ui-state-default").find(".maximize").css('display', 'none');

    $(this).closest(".ui-state-default").find(".widgets-collapse").slideDown("slow");
    // $(".widgets-collapse").slideDown("slow");
});

// MODAL

$('.modal').modal({
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .8, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '5%', // Starting top style attribute
    endingTop: '20%' // Ending top style attribute
});


// DATE PICKER

$( "#datepicker" ).datepicker();


// DRAG AND DROP DEALS

// (function ( $, window, undefined ) {
//     var pluginName = 'dragDrop',
//     document = window.document,
//     defaults = {
//         draggableSelector: ".draggable",
//         droppableSelector: ".droppable",
//         appendToSelector: false
//     };


//     function Plugin( element, options ) {
//         this.element = element;
//         this.options = $.extend( {}, defaults, options) ;

//         this._defaults = defaults;
//         this._name = pluginName;

//         this.init();
//     }

//     Plugin.prototype.init = function () {
//         var droppables = $(this.element).find(this.options.droppableSelector);
//         var draggables = $(this.element).find(this.options.draggableSelector).attr("draggable", "true");
    
//         if(this.options.appendToSelector){
//             var appendables = $(this.options.appendToSelector);
          
//             appendables.on({
//                 'dragenter': function(ev){
//                     ev.preventDefault();
//                     return true;
//                 },
//                 'drop': function(ev){
//                     var data = ev.originalEvent.dataTransfer.getData("Text");
//                     var draggedEl = document.getElementById(data);
//                     var destinationEl = $(ev.target);

//                     destinationEl = destinationEl.closest(appendables.selector).siblings(droppables.selector).append(draggedEl);
//                     $('.active').removeClass('active');
//                     $('.over').removeClass('over');
//                     ev.stopPropagation();
//                     return false;
//                 },
//                 'dragover': function(ev){
//                     ev.preventDefault();
//                     $(ev.target).closest(appendables.selector).addClass('over');
//                     return true;
//                 },
//                 'dragleave': function(ev){
//                     ev.preventDefault();
//                     $(ev.target).closest(appendables.selector).removeClass('over');
//                     return true;
//                 }
//             });
//         }
        
//         droppables.on({
//             'mouseup': function(ev){
//                 $('.active').removeClass('active');
//                 return true;
//             },
//             'dragenter': function(ev){
//                 ev.preventDefault();
//                 return true;
//             },
//             'drop': function(ev){
//                 var data = ev.originalEvent.dataTransfer.getData("Text");
//                 var draggedEl = document.getElementById(data);
//                 var destinationEl = $(ev.target);
            
//                 destinationEl.closest(draggables.selector).before(draggedEl);
//                 $('.active').removeClass('active');
//                 $('.over').removeClass('over');
//                 ev.stopPropagation();
//                 return false;
//             },
//             'dragover': function(ev){
//                 ev.preventDefault();
//                 $(ev.target).closest(draggables.selector).addClass('over');
//                 return true;
//             },
//             'dragleave': function(ev){
//                 ev.preventDefault();
//                 $(ev.target).closest(draggables.selector).removeClass('over');
//                 return true;
//             }
//         });
        
        
//         draggables.on({
//             'mousedown': function(ev){
//                 $(ev.target).closest(draggables.selector).addClass('active');
//                 return true;
//             },
//             'mouseup': function(ev){
//                 $('.active').removeClass('active');
//                 return true;
//             },
//             'dragstart': function(ev){
//                 ev.originalEvent.dataTransfer.effectAllowed='move';
//                 ev.originalEvent.dataTransfer.setData("Text", ev.target.getAttribute('id'));
//                 ev.originalEvent.dataTransfer.setDragImage(ev.target,100,20);
//                 return true;
//             },
//             'dragend': function(ev){
//                 return true;
//             }
//         });
    
//     };

//     // A really lightweight plugin wrapper around the constructor, 
//     // preventing against multiple instantiations
//     $.fn[pluginName] = function ( options ) {
//         return this.each(function () {
//             if (!$.data(this, 'plugin_' + pluginName)) {
//                 $.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
//             }
//         });
//     }

// }(jQuery, window));

// $(document).ready(function(){
//   $("#drag-list").dragDrop({
//           draggableSelector: ".task",
//           droppableSelector: ".task-list",
//       appendToSelector: ".drag-column-header"
//     });
  
// });



// DATE AND TIME PICKER

jQuery(document).ready(function(){
    jQuery('#date-format').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm'
    });
    jQuery(".tasks-priority #date-fr").bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm'
    });

    jQuery('#date').bootstrapMaterialDatePicker({ 
        weekStart : 0, 
        time: false 
    });

    jQuery('#date2').bootstrapMaterialDatePicker({ 
        weekStart : 0, 
        time: false 
    });

    jQuery.material.init()
});



// TAGS

dragula([
    document.getElementById('1'),
    document.getElementById('2'),
    document.getElementById('3'),
    document.getElementById('4'),
    document.getElementById('5')
])

.on('drag', function(el) {
    
    // add 'is-moving' class to element being dragged
    el.classList.add('is-moving');
})
.on('dragend', function(el) {
    
    // remove 'is-moving' class from element after dragging has stopped
    el.classList.remove('is-moving');
    
    // add the 'is-moved' class for 600ms then remove it
    window.setTimeout(function() {
        el.classList.add('is-moved');
        window.setTimeout(function() {
            el.classList.remove('is-moved');
        }, 600);
    }, 100);
});


var createOptions = (function() {
    var dragOptions = document.querySelectorAll('.drag-options');
    
    // these strings are used for the checkbox labels
    var options = ['Research', 'Strategy', 'Inspiration', 'Execution'];
    
    // create the checkbox and labels here, just to keep the html clean. append the <label> to '.drag-options'
    function create() {
        for (var i = 0; i < dragOptions.length; i++) {

            options.forEach(function(item) {
                var checkbox = document.createElement('input');
                var label = document.createElement('label');
                var span = document.createElement('span');
                checkbox.setAttribute('type', 'checkbox');
                span.innerHTML = item;
                label.appendChild(span);
                label.insertBefore(checkbox, label.firstChild);
                label.classList.add('drag-options-label');
                dragOptions[i].appendChild(label);
            });

        }
    }
    
    return {
        create: create
    }
    
    
}());

var showOptions = (function () {
    
    // the 3 dot icon
    var more = document.querySelectorAll('.drag-header-more');
    
    function show() {
        // show 'drag-options' div when the more icon is clicked
        var target = this.getAttribute('data-target');
        var options = document.getElementById(target);
        options.classList.toggle('active');
    }
    
    
    function init() {
        for (i = 0; i < more.length; i++) {
            more[i].addEventListener('click', show, false);
        }
    }
    
    return {
        init: init
    }
}());

createOptions.create();
showOptions.init();


// LINE CHART

var chart = AmCharts.makeChart("chartdiv2", {
    "type": "serial",
    "theme": "light",
    "marginTop":0,
    "marginRight": 80,
    "dataProvider": [{
        "year": "1997",
        "value": -0.353
    }, {
        "year": "1998",
        "value": 0.548
    }, {
        "year": "1999",
        "value": 0.298
    }, {
        "year": "2000",
        "value": -0.267
    }, {
        "year": "2001",
        "value": 0.411
    }, {
        "year": "2002",
        "value": 0.462
    }, {
        "year": "2003",
        "value": 0.47
    }, {
        "year": "2004",
        "value": 0.445
    }, {
        "year": "2005",
        "value": 0.47
    }],
    "valueAxes": [{
        "axisAlpha": 0,
        "position": "left"
    }],
    "graphs": [{
        "id":"g1",
        "balloonText": "[[category]]<br><b><span style='font-size:14px;'>[[value]]</span></b>",
        "bullet": "round",
        "bulletSize": 8,
        "lineColor": "#d1655d",
        "lineThickness": 2,
        "negativeLineColor": "#637bb6",
        "type": "smoothedLine",
        "valueField": "value"
    }],
    "chartScrollbar": {
        "graph":"g1",
        "gridAlpha":0,
        "color":"#888888",
        "scrollbarHeight":55,
        "backgroundAlpha":0,
        "selectedBackgroundAlpha":0.1,
        "selectedBackgroundColor":"#888888",
        "graphFillAlpha":0,
        "autoGridCount":true,
        "selectedGraphFillAlpha":0,
        "graphLineAlpha":0.2,
        "graphLineColor":"#c2c2c2",
        "selectedGraphLineColor":"#888888",
        "selectedGraphLineAlpha":1

    },
    "chartCursor": {
        "categoryBalloonDateFormat": "YYYY",
        "cursorAlpha": 0,
        "valueLineEnabled":true,
        "valueLineBalloonEnabled":true,
        "valueLineAlpha":0.5,
        "fullWidth":true
    },
    "dataDateFormat": "YYYY",
    "categoryField": "year",
    "categoryAxis": {
        "minPeriod": "YYYY",
        "parseDates": true,
        "minorGridAlpha": 0.1,
        "minorGridEnabled": true
    },
    "export": {
        "enabled": true
    }
});

chart.addListener("rendered", zoomChart);
if(chart.zoomChart){
    chart.zoomChart();
}

function zoomChart(){
    chart.zoomToIndexes(Math.round(chart.dataProvider.length * 0.4), Math.round(chart.dataProvider.length * 0.55));
}

// PIE VALUE

var chart = AmCharts.makeChart( "chartdiv3", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [ {
    "title": "New",
    "value": 4852
  },
  {
    "title": "User",
    "value": 2658
  }, {
    "title": "Returning",
    "value": 9899
  } ],
  "titleField": "title",
  "valueField": "value",
  "labelRadius": 5,

  "radius": "42%",
  "innerRadius": "60%",
  "labelText": "[[title]]",
  "export": {
    "enabled": true
  }
} );

// BAR CHART

var chart = AmCharts.makeChart( "chartdiv4", {
  "type": "serial",
  "theme": "light",
  "dataProvider": [ {
    "country": "USA",
    "visits": 2025
  }, {
    "country": "China",
    "visits": 1882
  }, {
    "country": "Japan",
    "visits": 1809
  }, {
    "country": "Germany",
    "visits": 1322
  }, {
    "country": "UK",
    "visits": 1122
  }, {
    "country": "France",
    "visits": 1114
  }, {
    "country": "India",
    "visits": 984
  }, {
    "country": "Spain",
    "visits": 711
  }, {
    "country": "Netherlands",
    "visits": 665
  }, {
    "country": "Russia",
    "visits": 580
  }, {
    "country": "South Korea",
    "visits": 443
  }, {
    "country": "Canada",
    "visits": 441
  }, {
    "country": "Brazil",
    "visits": 395
  } ],
  "valueAxes": [ {
    "gridColor": "#FFFFFF",
    "gridAlpha": 0.2,
    "dashLength": 0
  } ],
  "gridAboveGraphs": true,
  "startDuration": 1,
  "graphs": [ {
    "balloonText": "[[category]]: <b>[[value]]</b>",
    "fillAlphas": 0.8,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits"
  } ],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "gridAlpha": 0,
    "tickPosition": "start",
    "tickLength": 20
  },
  "export": {
    "enabled": true
  }

} );
