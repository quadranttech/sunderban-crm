jQuery("#save-company-details").click(function(){
	var fileName="";
	var data_time=jQuery(this).attr("data-time");
	var uploaded_image=jQuery("input[name='uploaded_image']").val();
	if(checkValidation(jQuery("input[name='company_name']"))==false || checkValidation(jQuery("input[name='street_address']"))==false || checkValidation(jQuery("input[name='city']"))==false || checkValidation(jQuery("input[name='state']"))==false || checkValidation(jQuery("input[name='pin_code']"))==false)
	{
		return false;
	}
	saveCompanyDetails(fileName,data_time,uploaded_image);
});

function saveCompanyDetails(fileName,data_time,uploaded_image)
{
	var company_name=jQuery("input[name='company_name']").val();
	var buisness_type=jQuery("select[name='buisness_type']").val();
	var street_address=jQuery("input[name='street_address']").val();
	var city=jQuery("input[name='city']").val();
	var state=jQuery("input[name='state']").val();
	var country=jQuery("select[name='country']").val();
	var company_id=jQuery("input[name='company_id']").val();
	var pin_code=jQuery("input[name='pin_code']").val();
	var portal_name=jQuery("input[name='portal_name']").val();
	var language=jQuery("select[name='language']").val();
	var date_format=jQuery("select[name='date_format']").val();
	portal_name=portal_name.replace(/\s+/g, "-");
	fileName=jQuery("input[name='uploaded_image_name']").val();
	if(data_time!="install")
	{
		startLoader(jQuery("#save-company-details"));
	}
	jQuery.ajax({
		url:'save-company-details',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'company_name':company_name,'buisness_type':buisness_type,'address':street_address,'city':city,'state':state,'country':country,'company_id':company_id,'logo':fileName,'post_code':pin_code,'portal_name':portal_name,'language':language,'date_format':date_format,'uploaded_image':uploaded_image,'timezone':jQuery("select[name='timezone']").val()},
      	type:'post',
      	success:function(data){
        	if(data.response="success")
        	{
        		if(data_time!="install")
        		{
        			stopLoader(jQuery("#save-company-details"));
        		}
        		if(data_time=="install")
        		{
        			window.location.href=APP_URL+'/dashboard';
        		}
        	}
        	else
        	{
        		if(data_time!="install")
        		{
        			stopLoader(jQuery("#save-company-details"));
        		}
        	}
      	},
	});
}

//AUTO FILL PORTAL NAME
jQuery("input[name='company_name']").keyup(function(){
	jQuery("input[name='portal_name']").val(jQuery(this).val()+" CRM");
	jQuery("input[name='portal_name']").closest("label").removeClass("active");
});

jQuery("input[name='company-logo']").change(function (element){
	// var fileName = element.target.value.split('\\').pop();
	readURL(this);
});

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#company-logo').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

jQuery(".email-settings #test5").on("change",function(){
	if(jQuery(this).is(':checked'))
	{
		jQuery("input[name='settings_default_email']").val(jQuery("input[name='default_email']").val());
	}
});
jQuery("#save-comopany-email").click(function(){
	var email_array=[];
	jQuery('input[name^="email[]"]').each(function() {
	    email_array.push(jQuery(this).val());
	});
	var checked="no";
	if(jQuery('#test5').is(':checked'))
	{
		checked="yes";
		jQuery("input[name='settings_default_email']").val(jQuery("input[name='default_email']").val());
	}
	startLoader(jQuery("#save-comopany-email"));
	jQuery.ajax({
		url:'get-company-email',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'company_id':jQuery("input[name='company_id']").val(),'email':email_array,'default_email':jQuery("input[name='default_email']").val(),'marked_as_default':checked},
      	type:'post',
      	success:function(data){
        	if(data.response=="success")
        	{
        		stopLoader(jQuery("#save-comopany-email"));
        		jQuery(".alert-message p").text("Saved Succssfully");
        		jQuery(".alert-message").addClass("success");
        		jQuery(".alert-wrapper").show();
        		setTimeout(function(){ 
        			jQuery(".alert-wrapper").hide(); 
        		}, 3000)
        	}
        	else
        	{
        		stopLoader(jQuery("#save-comopany-email"));
        	}
      	},
	});
});

//ADD ANOTHER COMPANY EMAIL
jQuery("#add-another-company-email").click(function(){
	var company_extra_email_html='<div class="input-field"><input type="email" name="email[]" class="validate"><label for="email">Email</label></div>';
	var company_remove_extra_email='<div class="input-field"><a href="javascript:void(0);" class="remove-input remove-company-extra-email"><span></span> </a></div>';
	jQuery(company_extra_email_html).appendTo(".company-extra-email");
	jQuery(company_remove_extra_email).appendTo(".company-remove-extra-email");
});

jQuery(document).on("click",".remove-company-extra-email",function(){
	var index=jQuery(this).parent(".input-field").index();
	jQuery(this).parent(".input-field").remove();
	jQuery(".company-extra-email").children().eq(index).remove();
});

jQuery(".settings-li-a").click(function(){
	jQuery(".settings-tabs").removeClass("show-content");
	var getQueryString=getUrlQueryVars();
	history.pushState('obj', jQuery(this).attr("data-query"), APP_URL+'/settings?page='+jQuery(this).attr("data-query"));
	if(jQuery(this).parent("li").index()=="0")
	{
		jQuery(".settings-tabs:first").attr('style','display: block !important');
	}
});

var first_tab=getUrlQueryVars();
if(first_tab=="company_details" || typeof first_tab==="undefined")
{
	jQuery(".settings-tabs:first").attr('style','display: block !important');
}
function getUrlQueryVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars.page;
}

//CREATE USER
jQuery("input[name='first-name']").focusout(function(){
	checkError(jQuery(this));
});

jQuery("input[name='last-name']").focusout(function(){
	checkError(jQuery(this));
});

jQuery("input[name='user-email']").focusout(function(){
	checkError(jQuery(this));
});

jQuery("input[name='user-phone']").focusout(function(){
	checkError(jQuery(this));
});

function checkError(target)
{
	if(target.val()!="")
	{
		target.parent(".input-field").removeClass("input-invalid");
	}
	else
	{
		target.parent(".input-field").addClass("input-invalid");
	}
}

jQuery(".create-user").click(function(){
	var createUser=true;
	checkError(jQuery("input[name='add-first-name']"));
	checkError(jQuery("input[name='add-last-name']"));
	checkError(jQuery("input[name='add-user-email']"));
	// checkError(jQuery("input[name='user-phone']"));
	jQuery("#modal4").find(".input-field").each(function(index,element){
		if(jQuery(element).hasClass("input-invalid"))
		{
			createUser=false;
			return false;
		}
	});
	if(createUser==true)
	{
		createUserFunction();
	}
});

function createUserFunction()
{
	startLoader(jQuery(".create-user"));
	jQuery.ajax({
		url:'create-user',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'first_name':jQuery("input[name='add-first-name']").val(),'last_name':jQuery("input[name='add-last-name']").val(),'email':jQuery("input[name='add-user-email']").val(),'phone':jQuery("input[name='add-user-phone']").val(),'company':jQuery("select[name='add-company']").val(),'role':jQuery("select[name='add-role']").val(),'department':jQuery("input[name='user-department']").val()},
      	type:'post',
      	success:function(data){
      		console.log(data.response);
        	if(data.response!=0)
        	{
        		stopLoader(jQuery(".create-user"));
        		jQuery("#modal4").find(".success-message").html(jQuery(".list_of_possible_errors .message").text());
        		jQuery("#modal4").find(".success-message").show();
        		var li_html='<li><a href="#!" data-user-id="0" class="user-edit invited"><div class="profile-wrapper"><span class="profile-image"><img src="http://dealpipe.co/demo/assets/img/images.jpg" alt="avatar"></span><div class="profile-name-designation"><span class="name">'+jQuery("input[name='add-first-name']").val()+' '+jQuery("input[name='add-last-name']").val()+'</span><span class="designation">Invited</span><span class="label">Invited</span></div></div></a></li>';
        		jQuery(".user-search").next("ul").append(li_html);
        		setTimeout(function(){ 
        			jQuery(".modal-close").trigger("click");
        			jQuery(".success-message").html("");
        		}, 3000)
        	}
        	else
        	{
        		jQuery(".modal-close").trigger("click");
        		jQuery(".alert-wrapper .alert-message p").html(jQuery(".list_of_possible_errors .user-exist").text());
        		jQuery(".alert-message").addClass("warning");
        		jQuery(".alert-wrapper").show();
        	}
      	},
	});
}

//DELETE USER
jQuery(".remove-user").click(function(){
	var confirmation=confirm("Press a button!");
	var target=jQuery(this);
	if(confirmation==true)
	{
		jQuery.ajax({
			url:'delete-user',
	      	dataType:'json',
	      	data:{'_token':jQuery("input[name='_token']").val(),'user_id':jQuery(this).attr("data-id")},
	      	type:'post',
	      	success:function(data){
	        	target.parents(".content-table-row").remove();
	      	},
		});
	}
});

//ADD EXTRA USER EMAIL
jQuery(document).on("click",".add-extra-user-email",function(){
	jQuery(".user-extra-email:first").clone().insertAfter(".user-extra-email:last");
	jQuery(".user-extra-email:last").find(".add-input").addClass("remove-input remove-extra-user-email").removeClass("add-input add-extra-user-email");
	jQuery(".user-extra-email:last").find("select").material_select();
	jQuery(".user-extra-email:last").find(".select-dropdown:first").remove();
});

//REMOVE EXTRA USER EMAIL
jQuery(document).on("click",".remove-extra-user-email",function(){
});

jQuery("input[name='first_name']").focusout(function(){
	checkError(jQuery(this));
});

jQuery("input[name='last_name']").focusout(function(){
	checkError(jQuery(this));
});

jQuery("input[name='login_email']").focusout(function(){
	checkError(jQuery(this));
});

jQuery(".save-manage-account").click(function(){
	if(jQuery("input[name='user_image']").val()!="")
	{
    	updateAccountDetails(jQuery("input[name='user_image']").val());
	}
	else
	{
		updateAccountDetails('');
	}
});

function updateAccountDetails(fileName)
{
	var extra_email=[];
	var extra_email_purpose=[];
	jQuery("input[name='extra_email[]'").each(function(){
		extra_email.push(jQuery(this).val());
	});
	jQuery("select[name='extra_email_purpose[]'").each(function(){
		extra_email_purpose.push(jQuery(this).val());
	});
	startLoader(jQuery(".save-manage-account"));
	jQuery.ajax({
		url:APP_URL+'/save-account-details',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'first_name':jQuery("input[name='first_name']").val(),'last_name':jQuery("input[name='last_name']").val(),'login_email':jQuery("input[name='login_email']").val(),'extra_email':extra_email,'extra_email_purpose':extra_email_purpose,'work_title':jQuery("input[name='work_title']").val(),'bio':jQuery("input[name='bio']").val(),'address':jQuery("input[name='address']").val(),'city':jQuery("input[name='city']").val(),'state':jQuery("input[name='state']").val(),'country':jQuery("select[name='country'").val(),'post_code':jQuery("input[name='post_code']").val(),'department':jQuery("input[name='department']").val(),'image':fileName},
      	type:'post',
      	success:function(data){
        	if(data.status=="success")
        	{
        		stopLoader(jQuery(".save-manage-account"));
        		jQuery(".alert-message p").html("Succssfully Saved");
        		jQuery(".alert-wrapper").addClass("success");
        		jQuery(".alert-wrapper").show();
        		setTimeout(function(){ 
        			jQuery(".alert-wrapper").hide(); 
        		}, 3000)
        	}
        	else
        	{
        		stopLoader(jQuery(".save-manage-account"));
        	}
      	},
	});
}

//USER SEARCH
var all_user_html=jQuery(".content-table-body").html();
jQuery("input[name='user_search']").keyup(function(){
	var user_html=jQuery(".content-table-body .content-table-row:first").clone();
	
	jQuery.ajax({
		url: 'user-search',
	    type: 'post',
	    data: {'_token':jQuery("input[name='_token']").val(),user_name:jQuery("input[name='user_search']").val()},
	    dataType: 'JSON',
	    success: function (data) {
	    	if(data.response!="")
	    	{
	    		if(data.status=="success")
	    		{
	    			jQuery(".user-grid .content-table-body").html(user_html);
	    			jQuery(".content-table-body .content-table-row .content-table-data").each(function(index,element){
	    				if(index==1)
	    				{
	    					jQuery(element).html(data.user.first_name);
	    				}
	    				if(index==2)
	    				{
	    					jQuery(element).html(data.user.last_name);
	    				}
	    				if(index==3)
	    				{
	    					jQuery(element).find("a").text(data.user.email);
	    				}
	    				if(index==4)
	    				{
	    					jQuery(element).html(data.user.phone_no);
	    				}
	    				if(index==5)
	    				{
	    					jQuery(element).html(data.user.department);
	    				}
	    				if(index==6)
	    				{
	    					jQuery(element).html(data.roles[data.user.role]);
	    				}
	    			});
	    		}
	    		else
	    		{
	    			jQuery(".user-grid .content-table-body").html(all_user_html);
	    		}
	    	}
	    }
	});
});

//IMAGE CROPPER
jQuery(document).on("click",".iEdit-img-edit-save",function(e){
	var offset = $("input[name='company-logo']").offset();

 	var offset_t = $("input[name='company-logo']").offset().top - $(window).scrollTop();
    var offset_l = $("input[name='company-logo']").offset().left - $(window).scrollLeft();

    var left = Math.round( (e.clientX - offset_l) );
    var top = Math.round( (e.clientY - offset_t) );


});

//SETTINGS PROPOSAL
jQuery(".settings-proposal").click(function(){
	history.pushState('obj', 'settings-proposal', APP_URL+'/settings?page='+jQuery(this).attr("data-query"));
});

//ADD EXTRA REMINDER PROPOSAL
jQuery(document).on("click",".add-extra-reminder-proposal",function(){
	jQuery(".reminder-proposal").append(jQuery(".reminder-proposal").find(".row:last").clone());
	if(jQuery(".reminder-proposal").find(".row").length==2)
	{
		jQuery(".reminder-proposal").find(".row:last select option:first").text("2nd Reminder Period");
	}
	if(jQuery(".reminder-proposal").find(".row").length==3)
	{
		jQuery(".reminder-proposal").find(".row:last select option:first").text("3rd Reminder Period");
		jQuery(".reminder-proposal").find(".row:eq(1) .add-extra-reminder-proposal").removeClass("add-extra-reminder-proposal").addClass("remove-extra-reminder-proposal remove-input");
		jQuery(".reminder-proposal").find(".row:eq(0) .form-layout-10").remove();
	}
	if(jQuery(".reminder-proposal").find(".row").length>3)
	{
		jQuery(".reminder-proposal").find(".row:last select option:first").text(jQuery(".reminder-proposal").find(".row").length+"th Reminder Period");
	}
	jQuery(".reminder-proposal").find(".row:last select").material_select();
	jQuery(".reminder-proposal").find(".row:last span:first").remove();
	jQuery(".reminder-proposal").find(".row:last input:first").remove();
	var last_reminder_row=jQuery('.reminder .row').length-1;
	jQuery(".reminder-proposal .row:eq("+last_reminder_row+")").find(".add-extra-reminder-proposal").removeClass("add-input add-extra-reminder-proposal").addClass("remove-extra-reminder-proposal remove-input");
});

//REMOVE EXTRA REMINDER PROPOSAL
jQuery(document).on("click",".remove-extra-reminder-proposal",function(event){
	jQuery(event.target).closest(".row").remove();
	if(jQuery(".reminder-proposal").find(".row").length==1)
	{
		if(jQuery(".reminder-proposal").find(".row .form-layout-10").length==0)
		{
			var add_extra_reminder_proposal_html='<div class="form-layout-10"><div class="field-group"><div class="input-field"><a href="javascript:void(0);" class="add-input add-extra-reminder-proposal"> <span></span></a></div></div></div>';
			jQuery(add_extra_reminder_proposal_html).insertAfter(jQuery(".reminder-proposal").find(".row:eq(0) .form-layout-80"));
		}
	}
});

//ADD EXTRA REMINDER ESTIMATE
jQuery(document).on("click",".add-extra-reminder-estimate",function(){
	jQuery(".reminder-estimate").append(jQuery(".reminder-estimate").find(".row:last").clone());
	if(jQuery(".reminder-estimate").find(".row").length==2)
	{
		jQuery(".reminder-estimate").find(".row:last select option:first").text("2nd Reminder Period");
	}
	if(jQuery(".reminder-estimate").find(".row").length==3)
	{
		jQuery(".reminder-estimate").find(".row:last select option:first").text("3rd Reminder Period");
		jQuery(".reminder-estimate").find(".row:eq(1) .add-extra-reminder-estimate").removeClass("add-extra-reminder-estimate").addClass("remove-extra-reminder-estimate remove-input");
		jQuery(".reminder-estimate").find(".row:eq(0) .form-layout-10").remove();
	}
	if(jQuery(".reminder-estimate").find(".row").length>3)
	{
		jQuery(".reminder-estimate").find(".row:last select option:first").text(jQuery(".reminder-estimate").find(".row").length+"th Reminder Period");
	}
	jQuery(".reminder-estimate").find(".row:last select").material_select();
	jQuery(".reminder-estimate").find(".row:last span:first").remove();
	jQuery(".reminder-estimate").find(".row:last input:first").remove();
	var last_reminder_row=jQuery('.reminder .row').length-1;
	jQuery(".reminder-estimate .row:eq("+last_reminder_row+")").find(".add-extra-reminder-estimate").removeClass("add-input add-extra-reminder-estimate").addClass("remove-extra-reminder-estimate remove-input");
});

//REMOVE EXTRA REMINDER ESTIMATE
jQuery(document).on("click",".remove-extra-reminder-estimate",function(event){
	jQuery(event.target).closest(".row").remove();
	if(jQuery(".reminder-estimate").find(".row").length==1)
	{
		if(jQuery(".reminder-estimate").find(".row .form-layout-10").length==0)
		{
			var add_extra_reminder_estimate_html='<div class="form-layout-10"><div class="field-group"><div class="input-field"><a href="javascript:void(0);" class="add-input add-extra-reminder-estimate"> <span></span></a></div></div></div>';
			jQuery(add_extra_reminder_estimate_html).insertAfter(jQuery(".reminder-estimate").find(".row:eq(0) .form-layout-80"));
		}
	}
});

//ADD EXTRA TAX PROPOSAL
jQuery(document).on("click",".add-extra-tax-proposal",function(){
	jQuery(".tax-proposal").append(jQuery(".tax-proposal").find(".row:last").clone());
	jQuery(".tax-proposal").find(".row:last input").val("");
	jQuery(".tax-proposal").find(".row:last label").removeClass("active");
	jQuery(".tax-proposal").find(".row:last select").material_select();
	jQuery(".tax-proposal").find(".row:last span.caret:first").remove();
	jQuery(".tax-proposal").find(".row:last input.select-dropdown:first").remove();
	var last_tax_row=jQuery('.tax-proposal .row').length-1;
	jQuery(".tax-proposal .row:eq("+last_tax_row+")").find(".add-extra-tax").removeClass("add-input add-extra-tax-proposal").addClass("remove-extra-tax-proposal remove-input");
});

//REMOVE EXTRA TAX PROPOSAL
jQuery(document).on("click",".remove-extra-tax-proposal",function(event){
	jQuery(event.target).closest(".row").remove();
});

//ADD EXTRA TAX ESTIMATE
jQuery(document).on("click",".add-extra-tax-estimate",function(){
	jQuery(".tax-estimate").append(jQuery(".tax-estimate").find(".row:last").clone());
	jQuery(".tax-estimate").find(".row:last input").val("");
	jQuery(".tax-estimate").find(".row:last label").removeClass("active");
	jQuery(".tax-estimate").find(".row:last select").material_select();
	jQuery(".tax-estimate").find(".row:last span.caret:first").remove();
	jQuery(".tax-estimate").find(".row:last input.select-dropdown:first").remove();
	var last_tax_row=jQuery('.tax-estimate .row').length-1;
	jQuery(".tax-estimate .row:eq("+last_tax_row+")").find(".add-extra-tax-estimate").removeClass("add-input add-extra-tax-estimate").addClass("remove-extra-tax-estimate remove-input");
});

//REMOVE EXTRA TAX ESTIMATE
jQuery(document).on("click",".remove-extra-tax-estimate",function(event){
	jQuery(event.target).closest(".row").remove();
});

//ADD EXTRA DISCOUNT PROPOSAL
jQuery(document).on("click",".add-extra-discount-proposal",function(){
	jQuery(".discount-proposal").append(jQuery(".discount-proposal").find(".row:first").clone());
	jQuery(".discount-proposal").find(".row:last input").val("");
	jQuery(".discount-proposal").find(".row:last label").removeClass("active");
	jQuery(".discount-proposal").find(".row:last select").material_select();
	jQuery(".discount-proposal").find(".row:last span.caret:first").remove();
	jQuery(".discount-proposal").find(".row:last input.select-dropdown:first").remove();
	var last_discount_row=jQuery('.discount-proposal .row').length-1;
	jQuery(".discount-proposal .row:eq("+last_discount_row+")").find(".add-extra-discount-proposal").removeClass("add-input add-extra-discount-proposal").addClass("remove-extra-discount-proposal remove-input");
});

//REMOVE EXTRA DISCOUNT PROPOSAL
jQuery(document).on("click",".remove-extra-discount-proposal",function(event){
	jQuery(event.target).closest(".row").remove();
});

//ADD EXTRA DISCOUNT ESTIMATE
jQuery(document).on("click",".add-extra-discount-estimate",function(){
	jQuery(".discount-estimate").append(jQuery(".discount-estimate").find(".row:first").clone());
	jQuery(".discount-estimate").find(".row:last input").val("");
	jQuery(".discount-estimate").find(".row:last label").removeClass("active");
	jQuery(".discount-estimate").find(".row:last select").material_select();
	jQuery(".discount-estimate").find(".row:last span.caret:first").remove();
	jQuery(".discount-estimate").find(".row:last input.select-dropdown:first").remove();
	var last_discount_row=jQuery('.discount-estimate .row').length-1;
	jQuery(".discount-estimate .row:eq("+last_discount_row+")").find(".add-extra-discount-estimate").removeClass("add-input add-extra-discount-estimate").addClass("remove-extra-discount-estimate remove-input");
});

//REMOVE EXTRA DISCOUNT ESTIMATE
jQuery(document).on("click",".remove-extra-discount-estimate",function(event){
	jQuery(event.target).closest(".row").remove();
});

jQuery(".save-settings-proposal").click(function(){
	var default_email=jQuery("input[name='settings_default_email']").val();
	var sender_name=jQuery("input[name='sender_name']").val();
	var invoice_number_prefix=jQuery("input[name='invoice_number_prefix']").val();
	var estimate_number_prefix=jQuery("input[name='estimate_number_prefix']").val();
	var reminder_array=[];
	jQuery(".reminder-"+jQuery(this).attr("data-purpose")+" select[name='reminder[]']").each(function(index,element){
		if(jQuery(this).val()!=null)
		{
			reminder_array.push(jQuery(this).val());
		}
	});
	var tax_name_array=[];
	var tax_type_array=[];
	var tax_amount_array=[];
	var tax_id_array=[];
	var discount_id_array=[];
	var purpose=jQuery(this).attr("data-purpose");
	if(purpose=="proposal")
	{
		default_email=jQuery("#tabs-5").find("input[name='settings_default_email']").val();
	}
	else
	{
		default_email=jQuery("#tabs-4").find("input[name='settings_default_email']").val();
	}
	jQuery('.tax-'+purpose+' input[name="tax_name[]"]').each(function(index,element) {
		if(jQuery(this).val()!="")
		{
			tax_name_array.push(jQuery(this).val());
		}
	});
	jQuery(".tax-"+purpose+" select[name='tax_type[]']").each(function(index,element){
		if(jQuery(this).val()!="")
		{
			tax_type_array.push(jQuery(this).val());
		}
	});
	jQuery(".tax-"+purpose+" input[name='tax_amount[]']").each(function(index,element){
		if(jQuery(this).val()!="")
		{
			tax_amount_array.push(jQuery(this).val());
		}
	});
	jQuery(".tax-"+purpose+" input[name='tax_id[]']").each(function(index,element){
		tax_id_array.push(jQuery(this).val());
	});
	jQuery(".discount-"+purpose+" input[name='discount_id[]']").each(function(index,element){
		discount_id_array.push(jQuery(this).val());
	});
	// return false;
	var discount_name_array=[];
	var discount_type_array=[];
	var discount_amount_array=[];
	jQuery(".discount-"+purpose+" input[name='discount_name[]']").each(function(index,element){
		if(jQuery(this).val()!="")
		{
			discount_name_array.push(jQuery(this).val());
		}
	});
	jQuery(".discount-"+purpose+" select[name='discount_type[]']").each(function(index,element){
		if(jQuery(this).val()!="")
		{
			discount_type_array.push(jQuery(this).val());
		}
	});
	jQuery(".discount-"+purpose+" input[name='discount_amount[]']").each(function(index,element){
		if(jQuery(this).val()!="")
		{
			discount_amount_array.push(jQuery(this).val());
		}
	});
	var default_payment_term=jQuery("select[name='default_payment_term']").val();
	if(jQuery(this).attr("data-purpose")=="proposal")
	{
		var terms_and_conditions=jQuery("#terms-proposal").val();
	}
	else
	{
		var terms_and_conditions=jQuery("#terms-estimate").val();
	}
	// return false;
	startLoader(jQuery(".save-settings-proposal"));
	jQuery.ajax({
		url:APP_URL+'/save-settings-proposal',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'default_email':default_email,'sender_name':sender_name,'invoice_number_prefix':invoice_number_prefix,'estimate_number_prefix':estimate_number_prefix,'reminder_array':reminder_array,'tax_name_array':tax_name_array,'tax_type_array':tax_type_array,'tax_amount_array':tax_amount_array,'discount_name_array':discount_name_array,'discount_type_array':discount_type_array,'discount_amount_array':discount_amount_array,'default_payment_term':default_payment_term,'company_id':jQuery("input[name='company_id']").val(),'terms_and_conditions':terms_and_conditions,'tax_id_array':tax_id_array,'discount_id_array':discount_id_array,'purpose':purpose},
      	type:'post',
      	success:function(data){
        	stopLoader(jQuery(".save-settings-proposal"));
      	},
	});
});

//SMTP DETAILS
jQuery("#save-smtp-details").click(function(){
	if(checkValidation(jQuery("input[name='mail_host']"))==false || checkValidation(jQuery("input[name='mail_port']"))==false || checkValidation(jQuery("input[name='mail_username']"))==false || checkValidation(jQuery("input[name='mail_password']"))==false)
	{
		return false;
	}
	startLoader(jQuery("#save-smtp-details"));
	jQuery.ajax({
		url:APP_URL+'/save-smtp-details',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'smtp_host':jQuery("input[name='mail_host']").val(),'smtp_port':jQuery("input[name='mail_port']").val(),'smtp_username':jQuery("input[name='mail_username']").val(),'smtp_password':jQuery("input[name='mail_password']").val(),'company_id':jQuery("input[name='company_id']").val()},
      	type:'post',
      	success:function(data){
        	stopLoader(jQuery("#save-smtp-details"));
      	},
	});
});

jQuery(".settings-smtp").click(function(){
	history.pushState('obj', 'settings-smtp', APP_URL+'/settings?page='+jQuery(this).attr("data-query"));
});

jQuery(".settings-payment-gateway").click(function(){
	history.pushState('obj', 'settings-payment-gateway', APP_URL+'/settings?page='+jQuery(this).attr("data-query"));
});

jQuery(".deal-source").click(function(){
	history.pushState('obj', 'deal-source', APP_URL+'/settings?page='+jQuery(this).attr("data-query"));
})

jQuery("#send-test-mail").click(function(){
	jQuery("#tabs-5 form input").each(function(index,element){
		if(jQuery(element).val()=="")
		{
			return false;
		}
	});
	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
	if(!pattern.test(jQuery("input[name='test_email']").val()))
	{
		return false;
	}
	else
	{
		jQuery(".showbox-wrapper").show();
		jQuery.ajax({
			url:APP_URL+'/send-test-mail',
	      	dataType:'json',
	      	data:{'_token':jQuery("input[name='_token']").val(),'email':jQuery("input[name='test_email']").val()},
	      	type:'post',
	      	success:function(data){
	        	if(data.status=="ok")
	        	{
	        		jQuery(".error-log").html("");
	        		jQuery(".test-mail").text("Test mail send succssfully");
	        		setTimeout(function(){ 
	        			jQuery(".test-mail").text(""); 
	        		}, 3000)
	        	}
	        	else
	        	{
	        		jQuery(".error-log").html(data.status);
	        	}
	        	jQuery(".showbox-wrapper").hide();
	      	},
	      	error : function(e){
	      		jQuery(".error-log").html("SMTP is not working");
	      	},
		});
	}
});

jQuery(".edit-user").click(function(){
	if(checkValidation(jQuery("#first-name"))==false || checkValidation(jQuery("#last-name"))==false || checkValidation(jQuery("#user-email"))==false)
	{
		return false;
	}
	else
	{
		startLoader(jQuery(".edit-user"));
		var contact_permission={};
		var deal_permission={};
		var setting_permission={};
		var invoice_permission={};
		var estimate_permission={};
		var task_permission={};
		contact_permission['add']=checkIfPermissionIsOn("input[name='contact-add-permission']");
		contact_permission['edit']=checkIfPermissionIsOn("input[name='contact-edit-permission']");
		contact_permission['view']=checkIfPermissionIsOn("input[name='contact-view-permission']");
		contact_permission['all']=checkIfPermissionIsOn("input[name='contact-all-permission']");

		deal_permission['add']=checkIfPermissionIsOn("input[name='deal-add-permission']");
		deal_permission['edit']=checkIfPermissionIsOn("input[name='deal-edit-permission']");
		deal_permission['view']=checkIfPermissionIsOn("input[name='deal-view-permission']");
		deal_permission['all']=checkIfPermissionIsOn("input[name='deal-all-permission']");

		setting_permission['add']=checkIfPermissionIsOn("input[name='setting-add-permission']");
		setting_permission['edit']=checkIfPermissionIsOn("input[name='setting-add-permission']");
		setting_permission['view']=checkIfPermissionIsOn("input[name='setting-view-permission']");
		setting_permission['all']=checkIfPermissionIsOn("input[name='setting-all-permission']");

		invoice_permission['add']=checkIfPermissionIsOn("input[name='invoice-add-permission']");
		invoice_permission['edit']=checkIfPermissionIsOn("input[name='invoice-edit-permission']");
		invoice_permission['view']=checkIfPermissionIsOn("input[name='invoice-view-permission']");
		invoice_permission['all']=checkIfPermissionIsOn("input[name='invoice-all-permission']");

		estimate_permission['add']=checkIfPermissionIsOn("input[name='estimate-add-permission']");
		estimate_permission['edit']=checkIfPermissionIsOn("input[name='estimate-edit-permission']");
		estimate_permission['view']=checkIfPermissionIsOn("input[name='estimate-view-permission']");
		estimate_permission['all']=checkIfPermissionIsOn("input[name='estimate-all-permission']");

		task_permission['add']=checkIfPermissionIsOn("input[name='task-add-permission']");
		task_permission['edit']=checkIfPermissionIsOn("input[name='task-edit-permission']");
		task_permission['view']=checkIfPermissionIsOn("input[name='task-view-permission']");
		task_permission['all']=checkIfPermissionIsOn("input[name='task-all-permission']");

		jQuery.ajax({
			url:APP_URL+'/edit-user',
	      	dataType:'json',
	      	data:{'_token':jQuery("input[name='_token']").val(),'first_name':jQuery("#first-name").val(),'last_name':jQuery("#last-name").val(),'email':jQuery("#user-email").val(),'user_id':jQuery("input[name='user_id']").val(),'phone_no':jQuery("input[name='user-phone']").val(),'role':jQuery('select[name="user-role"]').find(':selected').attr("data-value"),'contact_permission':contact_permission,'deal_permission':deal_permission,'setting_permission':setting_permission,'invoice_permission':invoice_permission,'estimate_permission':estimate_permission,'task_permission':task_permission,'status':jQuery(".dropdown-button").html()},
	      	type:'post',
	      	success:function(data){
	        	stopLoader(jQuery(".edit-user"));
	        	jQuery(".user-search").next("ul").find("li.active span.designation").text(jQuery('select[name="user-role"]').find(':selected').val());
	      	},
		});
	}
});

function checkIfPermissionIsOn(target)
{
	if(jQuery(target).is(':checked'))
	{
		return "on";
	}
	else
	{
		return "off";
	}
}

jQuery("#fileSelected").change(function(){
	readURL(this);
	jQuery('#cropimage').Jcrop({
	    onSelect: updateCoords
	});
	jQuery(".modal-overlay").show();
	jQuery("#image-cropper").show();
});

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#cropimage').attr('src', e.target.result);
            $('input[name="image"]').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

jQuery('#cropimage').Jcrop({
    onSelect: updateCoords
});

function updateCoords(c) {
    jQuery('#x').val(c.x);
    jQuery('#y').val(c.y);
    jQuery('#w').val(c.w);
    jQuery('#h').val(c.h);
};

jQuery(document).on("click",".image-crop",function(){
	jQuery(".modal-overlay").hide();
	jQuery.ajax({
		url:'upload-company-logo',
      	dataType:'json',
      	async:true,
      	data:new FormData($("#company-form")[0]),
      	type:'post',
      	processData: false,
      	contentType: false,
      	success:function(data){
        	jQuery("input[name='uploaded_image_name']").val(data.fileName);
        	jQuery("#image-cropper").hide();
        	jQuery("#result").attr("src",APP_URL+'/uploads/company-logo/'+data.fileName);
      	},
	});
});

jQuery(".cancel-image-crop-signin").click(function(){
	jQuery("#image-cropper").hide();
	jQuery(".modal-overlay").hide();
});

jQuery(document).on("click",".user-edit",function(){
	jQuery(".user-search").next("ul").find("li").removeClass("active");
	jQuery(this).closest("li").addClass("active");
	if(jQuery(this).attr("class")!="user-edit invited")
	{
		var user_id=jQuery(this).attr("data-user-id");
		var target=jQuery(this);
		jQuery.ajax({
			url:APP_URL+'/user-edit',
	      	dataType:'json',
	      	data:{'_token':jQuery("input[name='_token']").val(),'user_id':user_id},
	      	type:'post',
	      	success:function(data){
	      		if(data.id!=1)
	      		{
	      			jQuery("select[name='user-role']").closest(".row").show();
	      		}
	      		else
	      		{
	      			jQuery("select[name='user-role']").closest(".row").hide();
	      		}
	        	jQuery("input[name='first-name']").val(data.first_name);
	        	jQuery("label[for='first-name']").addClass("active");
	        	jQuery("input[name='last-name']").val(data.last_name);
	        	jQuery("label[for='last-name']").addClass("active");
	        	jQuery("input[name='user-email']").val(data.email);
	        	jQuery("label[for='user-email']").addClass("active");
	        	jQuery("input[name='user-phone']").val(data.phone_no);
	        	jQuery("label[for='user-phone']").addClass("active");
				jQuery("select[name='user-role']").val(data.role);
				jQuery("label[for='user-role']").addClass("active");
				jQuery(".vertical-tabs-content span.designation").html(data.role);
				jQuery(".vertical-tabs-content div.profile-name-designation span.name").html(data.first_name+" "+data.last_name);
	        	jQuery("select[name='user-role']").material_select();
	        	jQuery(".user-role").slideDown();
	        	jQuery(".dropdown-button").text(data.status);
	        	jQuery("#dropdown1 li").removeClass("selected");
	        	jQuery("#dropdown1 li a:contains("+data.status+")").parent("li").addClass("selected");
	        	jQuery("input[name='user-email']").attr("disabled",true);
	        	jQuery("#tabs-3").find(".vertical-tabs-content").removeClass("invited");
	        	if(data.image!=null)
	        	{
	        		jQuery(".vertical-tabs-content .profile-image img").attr("src",APP_URL+'/uploads/profile-image/'+data.image);
	        	}
	        	else
	        	{
	        		jQuery(".vertical-tabs-content .profile-image img").attr("src",APP_URL+'/assets/img/images.jpg');
	        	}
	        	setPermission("contact-add-permission",data.permission.contact.add);
	        	setPermission("contact-edit-permission",data.permission.contact.edit);
	        	setPermission("contact-view-permission",data.permission.contact.view);
	        	setPermission("contact-all-permission",data.permission.contact.all);

	        	setPermission("setting-add-permission",data.permission.setting.add);
	        	setPermission("setting-view-permission",data.permission.setting.view);
	        	setPermission("setting-all-permission",data.permission.setting.all);

	        	setPermission("deal-add-permission",data.permission.deal.add);
	        	setPermission("deal-edit-permission",data.permission.deal.edit);
	        	setPermission("deal-view-permission",data.permission.deal.view);
	        	setPermission("deal-all-permission",data.permission.deal.all);

	        	setPermission("invoice-add-permission",data.permission.invoice.add);
	        	setPermission("invoice-edit-permission",data.permission.invoice.edit);
	        	setPermission("invoice-view-permission",data.permission.invoice.view);
	        	setPermission("invoice-all-permission",data.permission.invoice.all);

	        	setPermission("estimate-add-permission",data.permission.estimate.add);
	        	setPermission("estimate-edit-permission",data.permission.estimate.edit);
	        	setPermission("estimate-view-permission",data.permission.estimate.view);
	        	setPermission("estimate-all-permission",data.permission.estimate.all);

	        	setPermission("task-add-permission",data.permission.task.add);
	        	setPermission("task-edit-permission",data.permission.task.edit);
	        	setPermission("task-view-permission",data.permission.task.view);
	        	setPermission("task-all-permission",data.permission.task.all);
	        	jQuery("input[name='user_id']").val(data.id);
	      	},
		});
	}
	else
	{
		jQuery("#tabs-3").find(".vertical-tabs-content .profile-name-designation .name").text(jQuery(this).find("span.name").text());
		jQuery("#tabs-3").find(".vertical-tabs-content .profile-name-designation .designation").text("Invited");
		jQuery("#tabs-3").find(".vertical-tabs-content .dropdown-button").text("Invited");
		jQuery("#tabs-3").find(".vertical-tabs-content #first-name").val("");
		jQuery("#tabs-3").find(".vertical-tabs-content label[for='first-name']").removeClass("active");
		jQuery('#tabs-3').find(".vertical-tabs-content #last-name").val("");
		jQuery("#tabs-3").find(".vertical-tabs-content label[for='last-name']").removeClass("active");
		jQuery("#tabs-3").find(".vertical-tabs-content #user-email").val("");
		jQuery("#tabs-3").find(".vertical-tabs-content label[for='user-email']").removeClass("active");
		jQuery("#tabs-3").find(".vertical-tabs-content #user-phone").val("");
		jQuery("#tabs-3").find(".vertical-tabs-content label[for='user-phone']").removeClass("active");
		jQuery("#tabs-3").find(".vertical-tabs-content .user-role").slideUp();
		jQuery("#tabs-3").find(".vertical-tabs-content").addClass("invited");
		setPermission("contact-add-permission","off");
    	setPermission("contact-edit-permission","off");
    	setPermission("contact-view-permission","off");
    	setPermission("contact-all-permission","off");

    	setPermission("setting-add-permission","off");
    	setPermission("setting-view-permission","off");
    	setPermission("setting-all-permission","off");

    	setPermission("deal-add-permission","off");
    	setPermission("deal-edit-permission","off");
    	setPermission("deal-view-permission","off");
    	setPermission("deal-all-permission","off");

    	setPermission("invoice-add-permission","off");
    	setPermission("invoice-edit-permission","off");
    	setPermission("invoice-view-permission","off");
    	setPermission("invoice-all-permission","off");

    	setPermission("estimate-add-permission","off");
    	setPermission("estimate-edit-permission","off");
    	setPermission("estimate-view-permission","off");
    	setPermission("estimate-all-permission","off");

    	setPermission("task-add-permission","off");
    	setPermission("task-edit-permission","off");
    	setPermission("task-view-permission","off");
    	setPermission("task-all-permission","off");
	}
});

function setPermission(target,permission)
{
	if(permission=="on")
	{
		jQuery("input[name="+target+"]").prop("checked",true);
	}
	else
	{
		jQuery("input[name="+target+"]").prop("checked",false);
	}
}

jQuery("input[name='task-all-permission']").change(function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input").prop("checked",true);
	}
});

jQuery("input[name='estimate-all-permission']").change(function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input").prop("checked",true);
	}
});

jQuery("input[name='invoice-all-permission']").change(function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input").prop("checked",true);
	}
});

jQuery("input[name='setting-all-permission']").change(function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input").prop("checked",true);
	}
});

jQuery("input[name='deal-all-permission']").change(function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input").prop("checked",true);
	}
});

jQuery("input[name='contact-all-permission']").change(function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input").prop("checked",true);
	}
});

jQuery(document).on("change","input[name='task-add-permission'] , input[name='task-edit-permission']",function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input[name='task-view-permission']").prop("checked",true);
	}
});

jQuery(document).on("change","input[name='contact-add-permission'] , input[name='contact-edit-permission']",function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input[name='contact-view-permission']").prop("checked",true);
	}
});

jQuery(document).on("change","input[name='deal-add-permission'] , input[name='deal-edit-permission']",function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input[name='deal-view-permission']").prop("checked",true);
	}
});

jQuery(document).on("change","input[name='invoice-add-permission'] , input[name='invoice-edit-permission']",function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input[name='invoice-view-permission']").prop("checked",true);
	}
});

jQuery(document).on("change","input[name='estimate-add-permission'] , input[name='estimate-edit-permission']",function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input[name='estimate-view-permission']").prop("checked",true);
	}
});

jQuery(document).on("change","input[name='setting-add-permission']",function(){
	if(this.checked)
	{
		jQuery(this).closest(".permissions-list").find("li input[name='setting-view-permission']").prop("checked",true);
	}
});

//SAVE PAYMENT CREDENTIALS
jQuery("input[name='group1']").change(function(){
	jQuery(".select-payment-method li.selected").removeClass("selected");
	jQuery(this).closest("li").addClass("selected");
	jQuery(".select-payment").hide();
	jQuery("."+jQuery(this).val()).show();
	jQuery("#default-payment-gateway").toggle();
});

jQuery("#save-payment-credential").click(function(){
	if(jQuery(".select-payment-method li.selected").attr("data-payment-method")=="paypal")
	{
		var client_id=jQuery("input[name='client_id']").val();
		var secret_key=jQuery("input[name='secret_key']").val();
	}
	else
	{
		var client_id=jQuery("input[name='stripe_client_id']").val();
		var secret_key=jQuery("input[name='stripe_secret_key']").val();
	}
	startLoader(jQuery("#save-payment-credential"));
	jQuery.ajax({
		url:APP_URL+'/save-payment-credential',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'company_id':jQuery("input[name='company_id']").val(),'client_id':client_id,'secret_key':secret_key,'payment_method':jQuery(".select-payment-method li.selected").attr("data-payment-method")},
      	type:'post',
      	success:function(data){
        	stopLoader(jQuery("#save-payment-credential"));
      	},
	});
});

//CHANGE DEFAULT PAYMENT GATEWAY
jQuery("#default-payment-gateway").click(function(){
	var changeDefaultPaymentGateway=true;
	jQuery("."+jQuery(".select-payment-method li.selected").attr("data-payment-method")+" form input").each(function(index,element){
		if(jQuery(element).val()=="")
		{
			jQuery("<span class='error'>Field is empty</span>").insertAfter(jQuery(element));
			changeDefaultPaymentGateway=false;
			return false;
		}
		else
		{
			jQuery("<span class='error'></span>").insertAfter(jQuery(element));
		}
	});
	if(changeDefaultPaymentGateway==true)
	{
		startLoader(jQuery("#default-payment-gateway"));
		jQuery.ajax({
			url:APP_URL+'/default-payment-gateway',
	      	dataType:'json',
	      	data:{'_token':jQuery("input[name='_token']").val(),'company_id':jQuery("input[name='company_id']").val(),'payment_method':jQuery(".select-payment-method li.selected").attr("data-payment-method")},
	      	type:'post',
	      	success:function(data){
	        	stopLoader(jQuery("#default-payment-gateway"));
	      	},
		});
	}
});



//STRIPE

//ADD DEAL SOURCE
jQuery("#add-deal-source").click(function(){
	if(jQuery("input[name='add-deal-source']").val()!="")
	{
		// startLoader(jQuery("#add-deal-source"));
		jQuery.ajax({
			url:APP_URL+'/add-deal-source',
	      	dataType:'json',
	      	data:{'_token':jQuery("input[name='_token']").val(),'company_id':jQuery("input[name='company_id']").val(),'source':jQuery("input[name='add-deal-source']").val()},
	      	type:'post',
	      	success:function(data){
	        	// stopLoader(jQuery("#add-deal-source"));
	        	var add_deal_source='<div class="chip scale-up-center">'+jQuery("input[name='add-deal-source']").val()+'<i class="close material-icons" data-deal-source="'+jQuery("input[name='add-deal-source']").val()+'">close</i></div>';
	        	jQuery("#tabs-8 .chip-container").append(add_deal_source);
	        	jQuery("#tabs-8 .scale-up-center:last").addClass(data.deal_source_color);
	        	jQuery("#tabs-8 input[name='add-deal-source']").val("");
	      	},
		});
	}
});

//REMOVE DEAL SOURCE
jQuery(document).on("click",".chip .close",function(){
	if(jQuery(this).attr("data-deal-source")!=undefined)
	{
		var ajax_url=APP_URL+'/remove-deal-source';
		var source=jQuery(this).attr("data-deal-source");
		var data={'_token':jQuery("input[name='_token']").val(),'company_id':jQuery("input[name='company_id']").val(),'source':source};
	}
	else if(jQuery(this).attr("data-currency")!=undefined)
	{
		var ajax_url=APP_URL+'/remove-currency';
		var currency=jQuery(this).attr("data-currency");
		var data={'_token':jQuery("input[name='_token']").val(),'company_id':jQuery("input[name='company_id']").val(),'currency':currency};
	}
	jQuery.ajax({
		url:ajax_url,
      	dataType:'json',
      	data:data,
      	type:'post',
      	success:function(data){
      	},
	});
});

//USER ACTIVATE DROPDOWN
jQuery(".dropdown li a").click(function(){
	var target=jQuery(this).closest(".dropdown");
	jQuery(target).closest("li").removeClass("selected");
	jQuery(this).parent("li").addClass("selected");
	jQuery(target).prev(".dropdown-button").text(jQuery(this).text());
});

jQuery(document).keypress(function(e){
    if (e.which == 13){
        jQuery("#add-deal-source").trigger("click");
    }
});

jQuery("select[name='contact-company").change(function(){
	if(jQuery(this).val()!="select_company")
	{
		jQuery(".add-company-contact").slideUp();
	}
});

//RESET ADD USER FORM FIELD
jQuery("a[href='#modal4']").click(function(){
	jQuery("#add-user")[0].reset();
	jQuery("#add-user").find("select[name='add-role'] option[value=priority]").prop("selected",false);
	jQuery("#add-user").find("select[name='add-role'] option[value=2]").prop("selected",true);
	jQuery("#add-user").find("select[name='add-role'] option[value=1]").prop("disabled",true);
	jQuery("#add-user").find("label").removeClass("active");
	jQuery("select[name='add-role']").material_select();
});

//ADD CURRENCY
jQuery(".add-currency").click(function(){
	if(jQuery("select[name='currency']").val()!="")
	{
		jQuery.ajax({
			url:APP_URL+'/add-currency',
	      	dataType:'json',
	      	data:{'_token':jQuery("input[name='_token']").val(),'company_id':jQuery("input[name='company_id']").val(),'currency':jQuery("select[name='currency']").val()},
	      	type:'post',
	      	success:function(data){
	      		if(data.response!="failed")
	      		{
	      			var add_deal_source='<div class="chip scale-up-center">'+jQuery("select[name='currency']").val()+'<i class="close material-icons" data-currency="'+jQuery("select[name='currency']").val()+'">close</i></div>';
		        	jQuery(".currency-list").append(add_deal_source);
		        	jQuery(".currency-list .scale-up-center:last").addClass(data.currency_color_array);
		        	jQuery("select[name='currency']").next("span").remove();
	      		}
	      		else
	      		{
	      			if(jQuery("select[name='currency']").next("span").length==0)
	      			{
	      				jQuery('<span class="">Currency already added</span>').insertAfter(jQuery("select[name='currency']"));
	      			}
	      		}
	      	},
		});
	}
});

//CHANE STAGE VISIBLITY
jQuery(document).on("click",".change-deal-visiblity",function(){
	jQuery(".deal-stage #dropdown2").find("li").removeClass("selected");
	jQuery(".deal-stage #dropdown2").find("a[value='"+jQuery(this).attr("data-stage-visiblity")+"']").parent("li").addClass("selected");
	jQuery(".deal-stage #dropdown2").prev("a").text(jQuery(this).attr("data-stage-visiblity"));
	jQuery(".deal-stage input[name='change-deal-title']").attr("data-stage-id",jQuery(this).attr("data-stage-id")).val(jQuery(this).attr("data-value"));
});

jQuery("#change-deal-visiblity").click(function(){
	startLoader(jQuery("#change-deal-visiblity"));
	jQuery.ajax({
		url:APP_URL+'/change-deal-visiblity',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'id':jQuery(".deal-stage input[name='change-deal-title']").attr("data-stage-id"),'value':jQuery("input[name='change-deal-title']").val(),'visiblity':jQuery("#dropdown2 li.selected").find("a").attr("value")},
      	type:'post',
      	success:function(data){
        	stopLoader(jQuery("#change-deal-visiblity"));
        	jQuery(".deal-stage li a[data-stage-id='"+jQuery(".deal-stage input[name='change-deal-title']").attr("data-stage-id")+"']").attr("data-stage-visiblity",jQuery("#dropdown2 li.selected").find("a").attr("value"));
      	},
	});
});

//ADD DEAL STAGE
jQuery(".add-deal-stage").click(function(){
	jQuery.ajax({
		url:APP_URL+'/add-deal-stage',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'deal_stage':jQuery("input[name='add-deal-stage']").val()},
      	type:'post',
      	success:function(data){
        	if(data.response!=0)
        	{
        		var li_html='<li><a href="javascript:void(0)" data-stage-id="'+data.response+'" data-stage-visiblity="active" data-value="'+jQuery("input[name='add-deal-stage']").val().toLowerCase()+'" class="change-deal-visiblity">'+jQuery("input[name='add-deal-stage']").val().toLowerCase()+'</a></li>';
        		jQuery(".deal-stage .vertical-nav-wrap ul").append(li_html);
        		jQuery("#add-stage .modal-close").trigger("click");
        	}
      	},
	});
});

jQuery("a[href='#add-stage']").click(function(){
	jQuery("input[name='add-deal-stage']").val("");
	jQuery("label[for='add-deal-stage']").removeClass("active");
});

//AUTOMATION COPY
jQuery(".copy-automation").click(function(){
	console.log("fgfghgf");
	// jQuery("input[name='add-automation']").trigger("click");
	jQuery(this).text("Copied");
	setTimeout(function(){ 
		jQuery("input[name='add-automation']").text("Copy");
	}, 3000)
});
