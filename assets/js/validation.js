//COMPANY CREATE VALIDATION
jQuery(".create-company").click(function(event){
	var first_name=checkValidation(jQuery("input[name='first_name']"));
	var last_name=checkValidation(jQuery("input[name='last_name']"));
	var email=checkValidation(jQuery("input[name='email[]']"));
	var phone=checkValidation(jQuery("input[name='phone[]']"));
	if(first_name==false || last_name==false || email==false || phone==false)
	{
		event.preventDefault();
	}
});

//EDIT CONTACT VALIDATION
jQuery(".edit-contact").click(function(){
	var first_name=checkValidation(jQuery("input[name='first_name']"));
	var last_name=checkValidation(jQuery("input[name='last_name']"));
	var email=checkValidation(jQuery("input[name='edit_contact_work_email[]']"));
	var phone=checkValidation(jQuery("input[name='edit_contact_work_phone[]']"));
	if(first_name==false || last_name==false || email==false || phone==false)
	{
		event.preventDefault();
	}
});

function checkValidation(target)
{
	if(target.val()=="")
	{
		target.parent(".input-field").addClass("input-invalid");
		return false;
	}
	else
	{
		target.parent(".input-field").removeClass("input-invalid");
		return true;
	}
}

var unsaved = false;
//STATE CHANGE
jQuery("form#contact-form :input").change(function(){
	jQuery(this).closest('form').data('changed', true);
	unsaved = true;
});

var somethingChanged = false;
jQuery(document).on("keyup","input",function() { 
    somethingChanged = true; 
    jQuery(this).closest("form").nextAll().find(".save:first").prop("disabled",false);
    jQuery(this).closest("form").find(".save").prop("disabled",false);
    jQuery(this).closest("form").closest(".buttons:first").find(".save").prop("disabled",false);
    // jQuery(".save").prop("disabled",false);
}); 

jQuery(document).on("change","select",function() { 
    somethingChanged = true; 
    jQuery(this).closest("form").nextAll().find(".save:first").prop("disabled",false);
}); 

jQuery(document).on("keyup","textarea",function(){
	somethingChanged = true; 
    jQuery(this).closest("form").nextAll().find(".save:first").prop("disabled",false);
});

jQuery(document).on("change","input",function(){
	somethingChanged = true; 
    jQuery(this).closest("form").nextAll().find(".save:first").prop("disabled",false);
});

jQuery(document).on("keyup",".select-payment-fields input",function(){
	jQuery("#save-payment-credential").prop("disabled",false);
	jQuery("#default-payment-gateway").prop("disabled",false);
});

jQuery(document).on("click",".avatar-save",function(){
	somethingChanged = true; 
    jQuery("#save-company-details").prop("disabled",false);
    jQuery(".save-manage-account").prop("disabled",false);
});

jQuery(document).on("change",".permissions-list li input[type='checkbox']",function(){
	jQuery(".edit-user").prop("disabled",false);
});

jQuery(document).on("click","textarea,.add-new-item",function(){
	jQuery(".save").prop("disabled",false);
});

jQuery(".save").prop("disabled",true);

jQuery("select[name='country[]']").each(function(index,element){
	if(jQuery(this).attr("data-country")!="")
	{
		var country=jQuery(this).attr("data-country");
		jQuery(this).find("option").each(function(option_index,option_value){
			if(jQuery(this).val()==country)
			{
				jQuery(this).attr('selected', true);
			}
		});
	}
});