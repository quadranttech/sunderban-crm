jQuery("#show-feed").click(function(){
  history.pushState('obj', 'feed', APP_URL+'/edit-contact/'+window.location.href.split("edit-contact/").pop().substr(0, window.location.href.split("edit-contact/").pop().indexOf('?'))+'?page='+'feed');
  jQuery("#tabs-1").attr("style","display:block!important");
	jQuery.ajax({
		url:'show-feed',
      	dataType:'json',
      	data:{'_token':jQuery("input[name='_token']").val(),'contact_id':jQuery("input[name='contact_id']").val()},
      	type:'post',
      	success:function(data){
        	var feedCount=0;
        	var feedHtml="";
        	while(feedCount<data.feed.length)
        	{
        		feedHtml=feedHtml+data.content;
        		feedCount++;
        	}
        	jQuery(".feeds-list .content-table").html(feedHtml);
        	jQuery(".feeds-list").find(".content-table-row").each(function(index,element){
        		jQuery(element).find(".title").html(data.feed[index].feed);
        		jQuery(element).find(".date").html(data.date_format_array[index]);
        		jQuery(element).find(".time").html(data.time_format_array[index]);
            jQuery(element).find(".timeline").addClass(data.feed_color_array[Math.floor(Math.random()*data.feed_color_array.length)]);
            if(data.userImage[data.feed[index].id]!=null)
            {
              jQuery(element).find(".avatar-pic img").attr("src",APP_URL+'/uploads/profile-image/'+data.userImage[data.feed[index].id]);
            }
            else
            {
              jQuery(element).find(".avatar-pic img").attr("src",APP_URL+'/assets/img/images.jpg');
            }
        	});
      	},
	});
});

jQuery("#show-deal-feed").click(function(){
  var deal_id=window.location.href.split("?deal_id=").pop().substr(0, window.location.href.split("?deal_id=").pop().indexOf('&'));
  jQuery.ajax({
    url:'show-deal-feed',
    dataType:'json',
    data:{'_token':jQuery("input[name='_token']").val(),'deal_id':deal_id},
    type:'post',
    success:function(data){
      if(data.status=="success")
      {
        var count=0;
        var feedHtml="";
        while(count<data.response.length)
        {
          feedHtml=feedHtml+data.content;
          count++;
        }
        jQuery(".feeds-list").find(".content-table").html(feedHtml);
        jQuery(".feeds-list").find(".content-table-row").each(function(index,element){
          jQuery(element).find(".title").text(data.response[index].feed);
          jQuery(element).find(".date").text(jQuery.datepicker.formatDate('dd M yy', new Date(data.response[index].created_at.split(' ')[0].split('-'))));
          var d=formatDate(data.response[index].created_at);
          jQuery(element).find(".time").text(d.split(' ')[1]+' '+d.split(' ')[2]);
          jQuery(element).find(".timeline").addClass(data.feed_color_array[Math.floor(Math.random()*data.feed_color_array.length)]);
          if(data.userImage[data.response[index].id]!=null)
          {
            jQuery(element).find(".avatar-pic img").attr("src",APP_URL+'/uploads/profile-image/'+data.userImage[data.response[index].id]);
          }
          else
          {
            jQuery(element).find(".avatar-pic img").attr("src",APP_URL+'/assets/img/images.jpg');
          }
        });
      }
    },
  });
});

function formatDate(date) {
  var d = new Date(date);
  var hh = d.getHours();
  var m = d.getMinutes();
  var s = d.getSeconds();
  var dd = "AM";
  var h = hh;
  if (h >= 12) {
    h = hh - 12;
    dd = "PM";
  }
  if (h == 0) {
    h = 12;
  }
  m = m < 10 ? "0" + m : m;

  s = s < 10 ? "0" + s : s;

  /* if you want 2 digit hours:
  h = h<10?"0"+h:h; */

  var pattern = new RegExp("0?" + hh + ":" + m + ":" + s);

  if(h<10)
  {
    h='0'+h;
  }
  var replacement = h + ":" + m;
  /* if you want to add seconds
  replacement += ":"+s;  */
  replacement += " " + dd;

  return date.replace(pattern, replacement);
}
