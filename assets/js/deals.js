//CREATE DEAL
jQuery(document).on("click",".add-deals",function(){
	jQuery("input[name='pre_stage']").val(jQuery(this).attr("data-stage"));
	jQuery("select[name='stage'] option[value='"+jQuery(this).attr("data-stage")+"']").attr("selected",true);
	var selectedOptionIndex=jQuery("select[name='stage']").prop('selectedIndex');
	jQuery("select[name='stage']").parents("div.select-wrapper").find("ul li:eq("+selectedOptionIndex+")").addClass("active selected");
	jQuery("select[name='stage']").material_select();
	
});

var currency_symbols = {
    'USD': '$', // US Dollar
    'EUR': '€', // Euro
    'CRC': '₡', // Costa Rican Colón
    'GBP': '£', // British Pound Sterling
    'ILS': '₪', // Israeli New Sheqel
    'INR': '₹', // Indian Rupee
    'JPY': '¥', // Japanese Yen
    'KRW': '₩', // South Korean Won
    'NGN': '₦', // Nigerian Naira
    'PHP': '₱', // Philippine Peso
    'PLN': 'zł', // Polish Zloty
    'PYG': '₲', // Paraguayan Guarani
    'THB': '฿', // Thai Baht
    'UAH': '₴', // Ukrainian Hryvnia
    'VND': '₫', // Vietnamese Dong
};


var intRegex = /^\d+$/;
var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
jQuery(".create-deal").click(function(){
	var ajax_url=APP_URL+'/ajax/create-new-deal';
	var title=jQuery("input[name='title']").val();
	var company=jQuery("select[name='company']").val();
	var owner=jQuery("select[name='owner']").val();
	var status=jQuery("select[name='status']").val();
	var source=jQuery("select[name='source']").val();
	var stage=jQuery("input[name='pre_stage']").val();
	stage=jQuery("input[name='stage']:checked").val();
	var priority=jQuery("select[name='priority']").val();
	var value=jQuery("input[name='value']").val();
	var win_probability=jQuery("input[name='win_probability']").val();
	var description=jQuery("input[name='description']").val();
	var contact=jQuery("select[name='contact']").val();
	var currency=jQuery("select[name='currency']").val();
	var purpose=jQuery(this).attr("data-purpose");
	var created_by=jQuery("input[name='created_by']").val();
	var product_id=jQuery("select[name='product_id']").val();
	var deal_start=jQuery("input[name='deal-start']").val();
	var deal_close=jQuery("input[name='deal-end']").val();
	var person=jQuery("select[name='person']").val();
	if(title=="")
	{
		title="untitled";
	}
	// if(value=="" || !intRegex.test(value) || !floatRegex.test(value))
	// {
	// 	jQuery("input[name='value']").parent(".input-field").addClass("input-invalid");
	// 	return false;
	// }
	// else if(value!="")
	// {
	// 	jQuery("input[name='value']").parent(".input-field").removeClass("input-invalid");
	// }
	if(product_id==null)
	{
		jQuery('<span class="error-message">'+jQuery(".list_of_possible_errors .select-product").text()+'</span>').insertAfter(jQuery(".create-deal"));
		return false;
	}
	else
	{
		jQuery(".error-message").remove();
	}
	if(contact=="" || contact==null)
	{
		jQuery("select[name='contact']").parent(".input-field").addClass("input-invalid");
		jQuery('<span class="error-message">'+jQuery(".list_of_possible_errors .contact-required").text()+'</span>').insertAfter(jQuery(".create-deal"));
		return false;
	}
	else
	{
		jQuery("select[name='contact']").parent(".input-field").removeClass("input-invalid");
		jQuery(".error-message").remove();
	}
	if(stage=="" || stage==null)
	{
		jQuery('<span class="error-message">'+jQuery(".list_of_possible_errors .select-stage").text()+'</span>').insertAfter(jQuery(".create-deal"));
		return false;
	}
	else
	{
		jQuery(".error-message").remove();
	}
	if(deal_start=="")
	{
		jQuery('<span class="error-message">'+jQuery(".list_of_possible_errors .select-deal-start").text()+'</span>').insertAfter(jQuery(".create-deal"));
		jQuery("input[name='deal-start']").parents(".input-field").addClass("input-invalid");
		return false;
	}
	else
	{
		jQuery("input[name='deal-start']").parents(".input-field").removeClass("input-invalid");
		jQuery(".error-message").remove();
	}
	if(deal_close=="")
	{
		jQuery('<span class="error-message">'+jQuery(".list_of_possible_errors .select-deal-end").text()+'</span>').insertAfter(jQuery(".create-deal"));
		jQuery("input[name='deal-end']").parents(".input-field").addClass("input-invalid");
		return false;
	}
	else
	{
		jQuery("input[name='deal-end']").parents(".input-field").removeClass("input-invalid");
		jQuery(".error-message").remove();
	}
	if(person==null)
	{
		jQuery('<span class="error-message">'+jQuery(".list_of_possible_errors .select-person").text()+'</span>').insertAfter(jQuery(".create-deal"));
		return false;
	}
	else
	{
		jQuery(".error-message").remove();
	}
	if(company==null)
	{
		company=0;
	}
	startLoader(jQuery(".create-deal"));
	jQuery.ajax({
	    url: ajax_url,
	    type: 'get',
	    data: {title : title,contact : contact,company : company,owner : owner,status : status,source : source,stage : stage,priority : priority,value : value,win_probability : win_probability,description : description,currency : currency,purpose : purpose,created_by : created_by,product_id : product_id,deal_start : deal_start,deal_close : deal_close},
	    dataType: 'JSON',
	    success: function (data) {
	    	console.log(data);
	      if(data.response="success")
	      {
	      	var appendDealHtml=data.content;
      		appendDealHtml=appendDealHtml;
      		jQuery("ul.drag-list").find("li.drag-column-"+data.stage+" ul.drag-inner-list").prepend(appendDealHtml);
   //    		var stageTotalValuePerCurrency=$.map(data.stageTotalValuePerCurrency, function(value, index) {
			//     return [value];
			// });
			console.log(data.stageTotalValuePerCurrency);
			changeDealStageValue(stage,data.stageTotalValuePerCurrency);
			// console.log(stageTotalValuePerCurrency[3]);
			// console.log(jQuery("ul.drag-list").find("li.drag-column-"+data.stage+" .drag-header span."+stageTotalValuePerCurrency[0]).length);
			
	      	var target=jQuery("li.drag-column-"+data.stage+" ul.drag-inner-list li:first");
	      	jQuery(target).find(".card-title").html('<a href="'+APP_URL+'/deal-contexual?deal_id='+data.id+'"&page=deal_feed>'+data.title+'</a>');
	      	jQuery(target).find("span.price").html('<span>'+data.stageTotalValuePerCurrency.currency_symbol+'</span> '+data.value);
	      	jQuery(target).find("span.card-type").text(data.status);
	      	jQuery(target).find("span.card-id").text('#'+data.id);
	      	jQuery(target).attr("data-deal-id",data.id);
      		jQuery(target).find(".task-assigned img").attr("src",jQuery(".profile-pic img").attr("src"));
	      	// var total_price=jQuery("li.drag-column-"+data.stage).find("span.total-price").text();
	      	// total_price=total_price.substr(total_price.indexOf("$") + 1);
	      	// total_price=parseInt(total_price)+parseInt(data.value);
	      	// jQuery("li.drag-column-"+data.stage).find(".total-price").text('$'+total_price);
	      	stopLoader(jQuery(".create-deal"));
	      	if(data.message!=undefined && data.message!="")
	      	{
	      		jQuery('<span class="error-message">Smtp is not set</span>').insertAfter(jQuery(".create-deal"));
	      	}
	      	else
	      	{
	      		jQuery(".modal-close").trigger("click");
	      		jQuery("#create-deal")[0].reset();
	      	}
	      }
	    }
    });
});

jQuery(".card-title a").click(function(){
});

var stage_before_move="";
var stage_after_move="";
var deal_id="";
jQuery(document).on("mousedown","li.drag-item",foodown);
jQuery(document).on("mousedown",".drag-item-deal",foodown);

function foodown(e){
	if($(e.target).is("a"))
	{
		window.location.href=$(e.target).attr("href");
		return false;
	}
  jQuery(window).mousemove(foomove).mouseup(fooup);
  stage_before_move=jQuery(this).parents("li.drag-column").attr("data-li-stage");
  jQuery(this).addClass("currently-moving");
  deal_id=jQuery(this).attr("data-deal-id");
}

function foomove(){
}

function fooup(){
  jQuery(window).unbind('mousemove', foomove).unbind('mouseup', fooup);
  stage_after_move=jQuery("li.currently-moving").parents("li.drag-column").attr("data-li-stage");
  if(stage_before_move==stage_after_move)
  {
  }
  else
  {
  	console.log("stage_before_move = "+stage_before_move);
  	console.log("stage_after_move = "+stage_after_move);
  	var ajax_url=APP_URL+'/ajax/changeDealStage';
  	jQuery.ajax({
	    url: ajax_url,
	    type: 'get',
	    data: {stage_id:stage_after_move,deal_id:deal_id,contact_id:jQuery("input[name='contact_id']").val(),stage_id_before_move:stage_before_move},
	    dataType: 'JSON',
	    success: function (data) {
	    	console.log(data);
	    	if(data.response=="1")
	    	{
	    		var price=jQuery("li.currently-moving").find("span.price").text().split("$ ").pop();
	    		var stage_before_move_price=jQuery("li.drag-column[data-li-stage='"+stage_before_move+"']").find(".total-price").text().split("$").pop();
	    		var stage_after_move_price=jQuery("li.drag-column[data-li-stage='"+stage_after_move+"']").find(".total-price").text().split("$").pop();
	    		// jQuery("li.drag-column[data-li-stage='"+stage_before_move+"']").find(".total-price").text(parseInt(stage_before_move_price)-parseInt(price));
	    		// jQuery("li.drag-column[data-li-stage='"+stage_after_move+"']").find(".total-price").text(parseInt(stage_after_move_price)+parseInt(price));
	    		changeDealStageValue(stage_before_move,data.getBeforeStageTotalValueCurrency);
	    		changeDealStageValue(stage_after_move,data.stageTotalValuePerCurrency);
	    	}
	    }
	});
  }
}

function changeDealStageValue(stage_id,stageTotalValue)
{
	if(jQuery("li.drag-column[data-li-stage='"+stage_id+"']").find(".drag-header span."+stageTotalValue.currency).length>0 && stageTotalValue.deal_value!=0)
	{
		jQuery("li.drag-column[data-li-stage='"+stage_id+"']").find(".drag-header span."+stageTotalValue.currency).text(stageTotalValue.value);
	}
	else if(jQuery("li.drag-column[data-li-stage='"+stage_id+"']").find(".drag-header span."+stageTotalValue.currency).length==0 && stageTotalValue.deal_value!=0)
	{
		if(jQuery("li.drag-column[data-li-stage='"+stage_id+"']").find(".drag-header ul").length>0)
		{
			jQuery("li.drag-column[data-li-stage='"+stage_id+"']").find(".drag-header ul").append('<li><span class="total-price '+stageTotalValue.currency+'">'+stageTotalValue.value+'</span></li>');
		}
		else
		{
			jQuery("li.drag-column[data-li-stage='"+stage_id+"']").find(".drag-header").append('<span class="total-price '+stageTotalValue.currency+'">'+stageTotalValue.value+'</span>');
		}
	}
	else
	{
		jQuery("li.drag-column[data-li-stage='"+stage_id+"']").find(".drag-header span."+stageTotalValue.currency).parent("li").remove();
		jQuery("li.drag-column[data-li-stage='"+stage_id+"']").find(".drag-header span."+stageTotalValue.currency).remove();
	}
}

//GET DEAL
jQuery("#show-deal").click(function(){
	jQuery("#tabs-5").find(".btn-toggle button:last").addClass("active");
	jQuery("#tabs-5").find(".btn-toggle button:first").removeClass("active");
	jQuery.ajax({
	    url: 'get-deal',
	    type: 'post',
	    data: {'_token':jQuery("input[name='_token']").val(),'contact_id':jQuery("input[name='contact_id']").val(),'type':'grid'},
	    dataType: 'JSON',
	    success: function (data) {
	    	console.log(data);
	    	if(data.response=="success")
	    	{
	    		var dealCount=0;
	    		var count=1;
	    		var dealLiHtml='<div class="drag-wrapper"><ul class="drag-list">';
	    		var dealObject=data.deals;
	    		var stages_id_array=$.map(data.stages_id_array, function(value, index) {
				    return [value];
				});
				var total_value_per_stage=$.map(data.total_value_per_stage, function(value, index) {
				    return [value];
				});
				var status=$.map(data.status, function(value, index) {
				    return [value];
				});
				var stages=$.map(data.stages, function(value, index) {
				    return [value];
				});
	    		jQuery.map(dealObject, function(val, key) { 
	    			console.log("key = "+key);
	    			dealLiHtml=dealLiHtml+'<li class="drag-column drag-column-'+key+'" data-li-stage="'+stages_id_array[dealCount]+'"><div class="drag-header"><h4 class="drag-column-header blue-text">'+key+'</h4>';
	    // 			console.log(data.currency_array);
					// console.log(data.currency_array.key);
					var currency_array=$.map(data.currency_array[key], function(value, index) {
					    return [value];
					});
					var currency_name_array=$.map(data.currency_array[key], function(value, index) {
					    return [index];
					});
					console.log(currency_array);
					console.log(currency_name_array);
					jQuery.each(currency_array, function(currency_index,currency_value){
						console.log(currency_index);
						dealLiHtml=dealLiHtml+'<span class="total-price '+currency_name_array[currency_index]+'">'+currency_value+'</span>';
					});
	    			dealLiHtml=dealLiHtml+'<div class="drag-header-more"><a href="#modal2" data-stage="'+stages_id_array[dealCount]+'" data-owner="'+data.current_user+'" class="modal-trigger add-deals"></a></div></div>';
	    			dealLiHtml=dealLiHtml+'<ul class="drag-inner-list" id="'+count+'">';
	    			if(val.length>0)
	    			{
	    				var inner_value=0;
	    				while(inner_value<val.length)
	    				{
	    					var task_id='task-'+key+'-'+inner_value;
	    					dealLiHtml=dealLiHtml+'<li class="drag-item-deal" data-deal-id="'+val[inner_value].id+'">';
	    					var win_probability=(100-parseInt(val[inner_value].win_probability))/3;
	    					var color1=Math.round(win_probability)+Math.round(win_probability);
	    					var color2=Math.round(win_probability);
    						dealLiHtml=dealLiHtml+'<div id="'+task_id+'" class="card task"><span class="card-bg" style="background:linear-gradient(to bottom, #92283f '+color2+'%, #ff695c '+color1+'%, #ffca6e '+val[inner_value].win_probability+'%);"></span><div class="card-content clear-fix"><span class="card-title blue-text"><a href="'+APP_URL+'/deal-contexual?deal_id='+val[inner_value].id+'">'+val[inner_value].title+'</a></span>';
    						dealLiHtml=dealLiHtml+'<div class="task-assigned">';
    						if(data.userImage[key]!=undefined)
    						{
    							var user_image_count=0;
	    						while(user_image_count<data.userImage[key].length)
	    						{
	    							dealLiHtml=dealLiHtml+'<span class="avatar-pic-wrapper"><span class="avatar-pic"><img src="'+data.userImage[key][user_image_count]+'"></span></span>';
	    							user_image_count++
	    						}
    						}
    						else
    						{
    							var user_image_count=0;
	    						while(user_image_count<data.userImage[key].length)
	    						{
	    							dealLiHtml=dealLiHtml+'<span class="avatar-pic-wrapper"><span class="avatar-pic"><img src="'+APP_URL+'/assets/img/images.jpg"></span></span>';
	    							user_image_count++;
	    						}
    						}
    						dealLiHtml=dealLiHtml+'</div><div class="task-price"><span class="price"><span>'+val[inner_value].currency_symbol+'</span> '+val[inner_value].value+'</span></div></div><div class="card-action"><span class="card-id">#'+val[inner_value].id+'</span><span class="card-status">Follow up</span></div><span class="card-type">'+data.status_per_deal[val[inner_value].id]+'</span></div></li>';
    						inner_value++;
	    				}			
	    			}
	    			dealLiHtml=dealLiHtml+'</ul>';
	    			dealLiHtml=dealLiHtml+'</li>';
	    			dealCount++;
	    			count++;
	    		});
				dealLiHtml=dealLiHtml+'</ul></div>';
				jQuery(".drag-container").html(dealLiHtml);
				jQuery(".drag-container").width(jQuery(".drag-container").closest(".col").width());
				setTimeout(function(){ 
        			if(jQuery(".drag-container .drag-list li").length>4)
			    {
			        var li_count=4;
			        var width=0+jQuery(".drag-container .drag-wrapper").width();
			        while(li_count<jQuery(".drag-container .drag-list li.drag-column").length)
			        {
			            width=width+178;
			            li_count++;
			        }
			        jQuery(".drag-container .drag-wrapper").width(width);
			    }
        		}, 100)
				
				// 
				jQuery(".card-lists").hide();
				jQuery(".drag-container").show();
				dragulaaaaa();
	    	}
	    }
	});
});

//GET DEAL CARD
jQuery(".show-deal-card").click(function(){
	history.pushState('obj', 'deal', APP_URL+'/edit-contact/'+window.location.href.split("edit-contact/").pop().substr(0, window.location.href.split("edit-contact/").pop().indexOf('?'))+'?page='+'deal');
	jQuery(".showbox-wrapper").show();
	jQuery(".drag-container").hide();
	jQuery(".card-lists").show();
	jQuery("#tabs-5").find(".btn-toggle button:first").addClass("active");
	jQuery("#tabs-5").find(".btn-toggle button:last").removeClass("active");
	jQuery.ajax({
	    url: 'get-deal',
	    type: 'post',
	    data: {'_token':jQuery("input[name='_token']").val(),'contact_id':jQuery("input[name='contact_id']").val(),'type':'card'},
	    dataType: 'JSON',
	    success: function (data) {
	    	console.log(data);
	    	jQuery(".showbox-wrapper").hide();
	    	if(data.response=="success")
	    	{
	    		var dealCount=0;
	    		var count=1;
	    		var dealLiHtml='<ul class="drag-list">';
	    		var dealObject=data.deals;
	    		var stages_id_array=$.map(data.stages_id_array, function(value, index) {
				    return [value];
				});
				var total_value_per_stage=$.map(data.total_value_per_stage, function(value, index) {
				    return [value];
				});
				var status=$.map(data.status, function(value, index) {
				    return [value];
				});
				var stages=$.map(data.stages, function(value, index) {
				    return [value];
				});
				var card_html='<div class="card-lists">';
				var count=1;
				jQuery.map(dealObject, function(val, key) { 
	    			if(val.length>0)
	    			{
	    				var inner_value=0;
	    				while(inner_value<val.length)
	    				{
	    					if(count==4)
	    					{
	    						card_html=card_html+'</div><div class="card-lists">';
	    						count=1;
	    					}
	    					card_html=card_html+'<div class="card"><div class="card-content clear-fix"><div class="task-price"><span class="price"><span>'+val[inner_value].currency_symbol+'</span> '+val[inner_value].value+'</span></div>';
	    					if(data.userImage[key]!=undefined)
	    					{
	    						var user_image_count=0;
	    						card_html=card_html+'<div class="task-assigned">';
	    						while(user_image_count<data.userImage[key].length)
	    						{
	    							card_html=card_html+'<span class="avatar-pic-wrapper"><span class="avatar-pic"><img src="'+data.userImage[key][user_image_count]+'"></span></span>';
	    							user_image_count++
	    						}
	    					}
	    					card_html=card_html+'<span class="card-id">#'+val[inner_value].id+'</span></div></div>';
		    				card_html=card_html+'<div class="card-action"><span class="card-title">';
		    				if(data.permission.deal.edit=="on")
		    				{
		    					card_html=card_html+'<a href="'+APP_URL+'/deal-contexual?deal_id='+val[inner_value].id+'&page=deal_feed">'+val[inner_value].title+'</a>';
		    				}
		    				else
		    				{
		    					card_html=card_html+'<a href="#!">'+val[inner_value].title+'</a>';
		    				}
		    				card_html=card_html+'</span><span class="card-status green"><a href="#!">'+data.status_per_deal[val[inner_value].id]+'</a></span><span class="card-status green"><a href="#!">'+key+'</a></span></div>';
		    				card_html=card_html+'<div class="card-footer"><a href="'+APP_URL+'/deals"> View in sales pipe line </a></div></div>';
		    				inner_value++;
		    				count++;
	    				}
	    			}
	    		});
				jQuery(".card-view .card-lists").html(card_html);
	    	}
	    }
	});
});

//UPDATE DEAL
jQuery(".update-deal").click(function(){
	startLoader(jQuery(".update-deal"));
	jQuery.ajax({
	    url: 'update-deal',
	    type: 'post',
	    data: {'_token':jQuery("input[name='_token']").val(),'deal_title':jQuery("input[name='deal_title']").val(),'contact':jQuery("input[name='contact']").val(),'contact':jQuery("select[name='contact']").val(),'company':jQuery("select[name='company']").val(),'deal_priority':jQuery("select[name='deal_priority']").val(),'deal_value':jQuery("input[name='deal_value']").val(),'deal_description':jQuery("textarea[name='deal_description']").val(),'deal-owner':jQuery("select[name='deal_owner']").val(),'deal_id':jQuery("input[name='deal_id']").val()},
	    dataType: 'JSON',
	    success: function (data) {
	    	if(data.response!=0)
	    	{
	    		stopLoader(jQuery(".update-deal"));
	    		jQuery(".alert-wrapper .alert-message p").text("Deal has been updated");
	    		jQuery(".alert-message").addClass("success");
	    		jQuery(".alert-wrapper").show();
	    		setTimeout(function(){ 
        			jQuery(".alert-wrapper").hide(); 
        		}, 3000)
	    	}
	    	else
	    	{
	    		stopLoader(jQuery(".update-deal"));
	    	}
	    }
	});
});

//EDIT DEAL CONTEXUAL
jQuery(".deal-contexual").click(function(){
	startLoader(jQuery(".deal-contexual"));
	jQuery.ajax({
	    url: 'edit-deal-contexual',
	    type: 'post',
	    data: jQuery("#deal-contexual").serialize(),
	    dataType: 'JSON',
	    success: function (data) {
	    	if(data.response!=0)
	    	{
	    		stopLoader(jQuery(".deal-contexual"));
	    		jQuery(".alert-wrapper .alert-message p").text("Deal has been updated");
	    		jQuery(".alert-message").addClass("success");
	    		jQuery(".alert-wrapper").show();
	    		setTimeout(function(){ 
        			jQuery(".alert-wrapper").hide(); 
        		}, 3000)
	    	}
	    	else
	    	{
	    		stopLoader(jQuery(".deal-contexual"));
	    	}
	    }
	});
});

//GET DEAL CONTEXUAL TASK
jQuery("#show-deal-task").click(function(){
	jQuery(".tasks-list .tasks").remove();
	jQuery(jQuery(this).attr("href")+" a").hide();
	var href=jQuery(this).attr("href");
	jQuery(".showbox-wrapper").show();
	jQuery.ajax({
	    url: 'show-deal-task',
	    type: 'post',
	    data: {'_token':jQuery("input[name='_token']").val(),'deal_id':jQuery("input[name='deal_id']").val()},
	    dataType: 'JSON',
	    success: function (data) {
	    	jQuery(href+" a").show();
	    	if(data.status=="success")
	    	{
	    		var count=0;
	      		var contexual_task_li_html="";
	      		while(count<data.response.length)
	      		{
	      			contexual_task_li_html=contexual_task_li_html+data.content;
	      			count++;
	      		}
	      		jQuery(".tasks-list").prepend(contexual_task_li_html);
	      		jQuery(".tasks").each(function(index,element){
	      	jQuery(element).find("input[name='markTask']").attr("id","id-"+index).val(data.response[index].id);
	      	jQuery(element).find("label").attr('for',"id-"+index);
	      	jQuery(element).find("a.icons").addClass("more-actions edit edit-task").text("Edit").attr("data-edit-task-id",data.response[index].id);
	      	jQuery(element).find("span:first").addClass("border-right "+data.response[index].color);
	      	jQuery(element).find("label:first").text(data.response[index].task_title);
	      	jQuery(element).find(".task-assignee").text(data.response[index].assigned_to);
	      	jQuery(element).attr("data-task-id",data.response[index].id);
	      	if(data.response[index].tagsNameArray.length>0)
	      	{
	      		var tagCount=0;
	      		var tagsNameHtml="";
	      		while(tagCount<data.response[index].tagsNameArray.length)
	      		{
	      			if(tagCount<3)
	      			{
	      				tagsNameHtml=tagsNameHtml+'<span class="tags '+data.response[index].tagsColorArray[tagCount]+'">'+data.response[index].tagsNameArray[tagCount]+'</span>';
	      			}
	      			tagCount++;
	      		}
	      		if(data.response[index].tagsNameArray.length>3)
	      		{
	      			var more_tag=parseInt(data.response[index].tagsNameArray.length)-3;
	      			tagsNameHtml=tagsNameHtml+'<span class="more-tags" data-more-task="'+more_tag+'">'+more_tag+' more tags</span>';
	      		}
	      		jQuery(element).find(".tag-container").html(tagsNameHtml);
	      		if(data.agent=="mobile")
	      		{
	      			jQuery(element).find(".mobile-tag-container").html(tagsNameHtml);
	      		}
	      	}
	      	jQuery(element).find(".tasks-priority").text(data.response[index].due_date).addClass(data.response[index].color);
	      	if(data.agent=="mobile")
	      	{
	      		jQuery(element).find(".tasks-priority-view").text(data.response[index].due_date).addClass(data.response[index].color);
	      	}
	      });
	    	}
	    	jQuery(".showbox-wrapper").hide();
	    }
	});
});

//CONTEXUAL DEAL VALUE VALIDATION
jQuery("#contexual-deal-value").focusout(function(){
	if(!intRegex.test(jQuery(this).val()) || !floatRegex.test(jQuery(this).val()) || jQuery(this).val()=="")
	{
		jQuery(this).parent(".input-field").addClass("input-invalid");
	}
});

//SELECT COMPANY ON OWNER CHANGE
jQuery("select[name='deal_owner']").change(function(){
	if(jQuery(this).attr("data-company")!=0)
	{
		jQuery("select[name='company'] option[value='"+jQuery(this).find(':selected').attr("data-company")+"']").prop('selected', true);
		jQuery("select[name='company']").material_select();
	}
});

//CHANGE COMPANY ON CONTACT CHANGE
jQuery("#create-deal select[name='contact']").change(function(){
	var contact_id=jQuery(this).val();
	jQuery.ajax({
	    url: APP_URL+'/get-contact',
	    type: 'post',
	    data: {'_token':jQuery("input[name='_token']").val(),'contact_id':contact_id},
	    dataType: 'JSON',
	    success: function (data) {
	    	if(data.response=="success")
	    	{
	    		var option_html='<option value="'+data.company.id+'" selected>'+data.company.company_name+'</option>';
	    	}
	    	else
	    	{
	    		var option_html='<option value="" selected>No company</option>';
	    	}
	    	jQuery("#create-deal select[name='company']").html(option_html);
    		jQuery("#create-deal select[name='company']").material_select();
	    }
	});
});

//DELETE PRODUCT FROM DEAL
jQuery(document).on("click",".delete-product-from-deal",function(){
	var target = jQuery(this);
	jQuery.ajax({
	    url: APP_URL+'/delete-product-from-deal',
	    type: 'get',
	    data: {'deal_id':jQuery("input[name='deal_id']").val(),'product_id':jQuery(this).attr("data-id")},
	    dataType: 'JSON',
	    success: function (data) {
	    	console.log(data);
	    	target.closest(".content-table-row").remove();
	    	if(jQuery(".content-table-body").find(".content-table-row").length == 0)
	    	{
	    		jQuery(".content-table-body").addClass("demo-content");
	    	}
	    }
	});
});

//ADD PRODUCT TO DEAL
jQuery(document).on("click",".add-product-to-deal",function(){
	console.log(jQuery("select[name='product_id']").val());
	var product_id_array = jQuery("select[name='product_id']").val();
	var product_id_to_be_added = [];
	console.log(product_id_array);
	jQuery(product_id_array).each(function(index,element){
		console.log(element);
		if(jQuery(document).find(".content-table-body .content-table-row[data-product='"+element+"']").length == 0)
		{
			product_id_to_be_added.push(element);
		}
	});
	if(product_id_to_be_added.length != 0)
	{
		jQuery.ajax({
		    url: APP_URL+'/update-product-deal',
		    type: 'get',
		    data: {'product_id_to_be_added':product_id_to_be_added,'deal_id':jQuery("input[name='deal_id']").val()},
		    dataType: 'JSON',
		    success: function (data) {
		    	console.log(data);
		    	var count = 0;
		    	var new_product_array = data.new_product_array;
		    	var product_html = "";
		    	while(count < new_product_array.length)
		    	{
		    		product_html = product_html+'<div class="content-table-row content-table-row-sort" data-product="'+new_product_array[count].id+'"><div class="content-table-data"> <span class="avatar-pic-wrapper contactFirstCharParent"> <span class="avatar-pic pink lighten-1 contactFirstChar">W</span> </span> </div><div class="content-table-data">'+new_product_array[count].name+'</div><div class="content-table-data">'+new_product_array[count].description+'</div><div class="content-table-data">'+new_product_array[count].price+'</div><div class="content-table-data contactMore"><div class="more-actions"><button type="button" class="waves-effect waves-light btn btn-green delete-product-from-deal" data-id="'+new_product_array[count].id+'">Delete</button></div></div></div>';
                    count++;
		    	}
		    	jQuery(".content-table-body").append(product_html);
		    	jQuery(".content-table-body").removeClass("demo-content");
                jQuery(".modal-close").trigger("click");
		    }
		});
	}
	// console.log(jQuery(".content-table-body .content-table-row[data-product='"++"']").length);
});

//REINITIALIZE SELECT ON MODAL OPEN
jQuery("a[href='#modal2'], a[href='#add-product-to-deal']").click(function(){
	// jQuery("#mdp-demo").multiDatesPicker('resetDates');
	// jQuery("#mdp-demo-2").multiDatesPicker('resetDates');
	// jQuery('#mdp-demo').multiDatesPicker({
 //        minDate: 0, // today
 //        maxDate: 30, // +30 days from today
 //        mode: 'daysRange',
 //        autoselectRange: [0,1],
 //        altField: '#altField'
 //    });
 //    jQuery('#mdp-demo-2').multiDatesPicker({
 //        minDate: 0, // today
 //        maxDate: 30, // +30 days from today
 //        mode: 'daysRange',
 //        autoselectRange: [0,1],
 //        altField: '#altField2'
 //    });
	$('.deal-start').pickadate({
	    selectMonths: true, // Creates a dropdown to control month
	    selectYears: 15, // Creates a dropdown of 15 years to control year,
	    today: 'Today',
	    clear: 'Clear',
	    close: 'Ok',
	    closeOnSelect: false // Close upon selecting a date,
	  });
});

//CREATE DEAL PRICE
jQuery(document).on("change","select[name='product_id'],select[name='person'],input[name='deal-start'],input[name='deal-end']",function(event){
	var product_id = jQuery("select[name='product_id']").val();
	var product_price = jQuery("select[name='product_id'] option[value='"+product_id+"']").attr("data-product-price");
	var person_price = jQuery("select[name='product_id'] option[value='"+product_id+"']").attr("data-person-price");
	var person = jQuery("select[name='person']").val();
	var deal_start = jQuery("input[name='deal-start']").val();
	var deal_close = jQuery("input[name='deal-end']").val();
	if(deal_start != "" && deal_close != "" && product_id != null && person != null)
	{
		console.log("diffDays = "+daydiff(parseDate(deal_start), parseDate(deal_close)));
		var price = (parseInt(person) * parseFloat(person_price)) + (parseFloat(product_price) * parseInt(daydiff(parseDate(deal_start), parseDate(deal_close))));
		jQuery("#contexual-deal-value").val(price);
		jQuery("#contexual-deal-value").parent(".input-field").find("label").addClass("active");
	}
	if(jQuery(event.target).attr("name") == "product_id")
	{
		jQuery(".modal-content").trigger("click");
	}
	console.log("product_id = "+product_id);
	console.log("product_price = "+product_price);
	console.log("person_price = "+person_price);
	console.log("person = "+person);
	console.log("price = "+price);
});

function parseDate(str) {
    var mdy = str.split('-');
    return new Date(mdy[0], mdy[1]-1, mdy[2]);
}

function daydiff(first, second) {
    return Math.round((second-first)/(1000*60*60*24));
}

//GET HIGHEST UL HEIGHT
var heights = $(".drag-inner-list").map(function ()
{
    return $(this).height();
}).get(),

maxHeight = Math.max.apply(null, heights);
jQuery(".drag-inner-list").css("height",maxHeight);