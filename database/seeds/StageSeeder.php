<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class StageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('stages')->insert([
            [
	            'slug' => 'opportunity',
	            'stage' => 'opportunity',
	            'created_at' => date('Y-m-d H:i:s'),
	            'updated_at' => date('Y-m-d H:i:s')
            ],
	        [
	        	'slug' => 'proposal',
	            'stage' => 'proposal',
	            'created_at' => date('Y-m-d H:i:s'),
	            'updated_at' => date('Y-m-d H:i:s')
	        ],
	        [
	        	'slug' => 'negotiation',
	            'stage' => 'negotiation',
	            'created_at' => date('Y-m-d H:i:s'),
	            'updated_at' => date('Y-m-d H:i:s')
	        ],
	        [
	        	'slug' => 'complete',
	            'stage' => 'complete',
	            'created_at' => date('Y-m-d H:i:s'),
	            'updated_at' => date('Y-m-d H:i:s')
	        ]
        ]);
    }
}
