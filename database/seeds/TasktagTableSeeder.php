<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TasktagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('task_tags')->insert([
            'tag' => 'DONE',
            'tag_slug' => 'done',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
