<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AddrolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles=array("1" => "admin","2" => "staff");
        $serialized_roles=serialize($roles);
        DB::table('settings')->insert([
        	[
	            'meta_key' => 'roles',
	            'meta_value' => $serialized_roles,
                'company_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'meta_key' => 'invoice_start',
                'meta_value' => 'INV',
                'company_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'meta_key' => 'smtp_host',
                'meta_value' => '',
                'company_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'meta_key' => 'smtp_port',
                'meta_value' => '',
                'company_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'meta_key' => 'smtp_username',
                'meta_value' => '',
                'company_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'meta_key' => 'smtp_password',
                'meta_value' => '',
                'company_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'meta_key' => 'estimate_start',
                'meta_value' => 'EST',
                'company_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'meta_key' => 'terms_and_conditions',
                'meta_value' => '',
                'company_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'meta_key' => 'est_terms_and_conditions',
                'meta_value' => '',
                'company_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'meta_key' => 'default_payment_gateway',
                'meta_value' => 'paypal',
                'company_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'meta_key' => 'date_format',
                'meta_value' => 'dd/mm/yy',
                'company_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
        $currency_list=array('0' => 'USD');
        $serialized_currency_list=serialize($currency_list);
        DB::table('currency')->insert([
            'currency_list' => $serialized_currency_list,
            'company_id' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
