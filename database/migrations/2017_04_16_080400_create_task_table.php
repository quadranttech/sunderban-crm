<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by')->unsigned()->default(0);
            $table->string('task_title')->nullable();
            $table->string('task_description')->nullable();
            $table->integer('assigned_to')->unsigned()->default(0);
            $table->integer('related_to')->unsigned()->default(0);
            $table->integer('related_deal')->unsigned()->default(0);
            $table->timestamp('due_date')->nullable();
            $table->string('marked_as')->nullable();
            $table->string('task_tag')->nullable();
            $table->string('color')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task');
    }
}
