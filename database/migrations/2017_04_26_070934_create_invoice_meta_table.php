<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inv_id')->unsigned();
            $table->foreign('inv_id')->references('id')->on('invoices');
            $table->longText('attachment',1000)->nullable();
            $table->float('tax', 8, 2)->nullable();
            $table->integer('tax_id');
            $table->float('discount', 8, 2)->nullable();
            $table->integer('discount_id');
            $table->float('shipping_cost', 8, 2)->nullable();
            $table->float('inv_total', 8, 2);
            $table->float('adjustment', 8, 2)->nullable();
            $table->string('part_pay')->default('no_part_pay');
            $table->integer('part_pay_value');
            $table->float('due', 8, 2)->nullable();
            $table->string('payment_method')->nullable();
            $table->string('payment_id')->nullable();
            $table->longText('customer_note',10000)->nullable();
            $table->longText('admin_note',10000)->nullable();
            $table->string('terms_and_conditions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_meta');
    }
}
