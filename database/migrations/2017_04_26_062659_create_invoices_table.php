<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('inv_no');
            $table->integer('company_id')->unsigned();
            $table->integer('deal_id')->unsigned();
            $table->string('invoice_title')->nullable();
            $table->string('order_no')->nullable();
            $table->integer('contact')->unsigned();
            $table->foreign('contact')->references('id')->on('contacts');
            $table->string('contact_type')->nullable();
            $table->date('inv_date')->nullable();
            $table->date('due_date')->nullable();
            $table->string('default_payment_term')->nullable();
            $table->integer('sales_person')->unsigned();
            $table->foreign('sales_person')->references('id')->on('users');
            $table->string('status')->nullable();
            $table->float('paid', 8, 2)->nullable();
            $table->float('due', 8, 2)->nullable();
            $table->string('payment_status')->nullable();
            $table->string('currency')->nullable();
            $table->date('send_date')->nullable();
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
