<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inv_id')->unsigned();
            $table->foreign('inv_id')->references('id')->on('invoices');
            $table->string('item_details')->nullable();
            $table->float('rate', 8, 2)->nullable();
            $table->string('quantity')->nullable();
            $table->string('quantity_type')->nullable();
            $table->string('currency')->nullable();
            $table->float('total', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_items');
    }
}
