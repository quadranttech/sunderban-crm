<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('contact')->nullable();
            $table->integer('related_to')->nullable();
            $table->string('related_product')->nullable();
            $table->date('deal_start')->nullable();
            $table->date('deal_end')->nullable();
            $table->integer('owner')->nullable();
            $table->integer('status')->nullable();
            $table->string('source')->nullable();
            $table->integer('stage')->nullable();
            $table->string('priority')->nullable();
            $table->integer('value')->nullable();
            $table->string('currency')->nullable();
            $table->integer('win_probability')->nullable();
            $table->string('description')->nullable();
            $table->integer('created_by')->nullable();
            $table->date('created_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
