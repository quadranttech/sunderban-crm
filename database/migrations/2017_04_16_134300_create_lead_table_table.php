<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_table', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id')->default('0');
            $table->string('content_type')->nullable();
            $table->string('industry')->nullable();
            $table->string('lead_source')->nullable();
            $table->integer('lead_stage')->unsigned();
            $table->foreign('lead_stage')->references('id')->on('lead_stage');
            $table->string('territory')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_table');
    }
}
