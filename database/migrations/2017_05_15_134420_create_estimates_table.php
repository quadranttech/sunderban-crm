<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('est_no');
            $table->integer('created_by');
            $table->string('estimate_title')->nullable();
            $table->integer('company_id')->unsigned();
            $table->integer('deal_id')->unsigned();
            $table->string('order_no')->nullable();
            $table->string('contact_type')->nullable();
            $table->date('est_date')->nullable();
            $table->date('due_date')->nullable();
            $table->string('default_payment_term')->nullable();
            $table->string('status')->nullable();
            $table->string('currency')->nullable();
            $table->date('send_date')->nullable();
            $table->string('link')->nullable();
            $table->integer('contact')->unsigned();
            $table->foreign('contact')->references('id')->on('contacts');
            $table->date('estimate_date')->nullable();
            $table->integer('sales_person')->unsigned();
            $table->foreign('sales_person')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimates');
    }
}
