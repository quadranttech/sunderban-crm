<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => '134958301259-tu56dss02nvodij3mnuas97h8p8knv7g.apps.googleusercontent.com',
        'client_secret' => '6hzq9i5-bMYef3P3DxwgwutR',
        'redirect' => 'http://quadrant.technology/pulse/auth/google/callback',
    ],

    'linkedin' => [
        'client_id' => '810k4ec3zfehsw',
        'client_secret' => 'SSRVEdJTwaZtn7EZ',
        'redirect' => 'http://quadrant.technology/pulse/auth/linkedin/callback',
    ],

    'paypal' => [
        'client_id' => env('PAYPAL_CLIENT_ID'),
        'secret' => env('PAYPAL_SECRET')
    ],

    'stripe' => [
        'secret' => env('STRIPE_SECRET'),
    ],

];
