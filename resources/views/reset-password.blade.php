@extends('layouts.home-header')
@section('content')
<div class="container">
	<div class="row">
		<div class="col s8 offset-s2">
			<div class="login-inner-content-wrapper">
				<div class="login-inner-content">
					<div class="login-inner border clear-fix">
						<div class="login-left-side left">
							<div class="signin-form-wrapper">
								<h2 class="blue-text">{{ Lang::get('invoice-estimate-common-template-lang.welcome-to') }} </br>@php echo str_replace('-', ' ', env('PORTAL_NAME')) @endphp</h2>
								<form class="signin-form" id="login-form" action="{{ url('/') }}/session/login" role="form" method="POST">
									{{ csrf_field() }}
									<div class="row">
										<div class="col s12">
											<div class="form-layout form-layout-50x50 clear-fix">
												<div class="form-layout-50">
													<div class="field-group">
														<div class="input-field">
															<input type="hidden" name="email" value="{{ $email }}">
															<input type="hidden" name="token" value="{{ $token }}">
												          	<input id="new-password" name="new_password" type="password" class="validate" autofocus>
												          	<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												          	<label for="log-email">{{ Lang::get('common.password') }}</label>
												        </div>
													</div>
												</div>
												<div class="form-layout-50 wrong-password">
													<div class="field-group">
														<div class="input-field">
												          	<input id="confirm-password" name="confirm_password" type="password" class="validate" autofocus>
												          	<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												          	<label for="log-email">{{ Lang::get('forgot-password-lang.reset-password') }}</label>
												        </div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col s12 center submit-button">
											<button class="waves-effect waves-light btn green" id="reset-password" disabled>{{ Lang::get('forgot-password-lang.reset-password') }}</button>
										</div>
									</div>
									<div class="row success-password-reset center" style="display:none">
										<div class="col s12">
											<a href="{{ url('/') }}" class="waves-effect waves-light btn green">{{ Lang::get('forgot-password-lang.back-to-login') }}</a>
											<div class="success-message-text">{{ Lang::get('forgot-password-lang.password-changed-successfully') }}</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="login-right-side right">
							<span class="orange-overlay"></span>
							<img src="{{ url('/assets/img/login-img.jpg') }}" alt="login-img">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection