<!DOCTYPE html>
<html lang="en">
  <!-- HEAD SECTION -->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Invoice Template 1</title>
    <!-- STYLE -->
    <style type="text/css">
      /****CUSTOM STYLESHEET*****/
        @font-face {
            font-family: 'Montserrat', sans-serif;
            src: url('assets/fonts/montserrat-regular-webfont.woff2') format('woff2'),
                 url('assets/fonts/montserrat-regular-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: 'Open Sans', sans-serif;
            src: url('assets/fonts/opensans-regular-webfont.woff2') format('woff2'),
                 url('assets/fonts/opensans-regular-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;
        }
        @media (max-width: 767px) {
          .table-responsive {
            width: 100%;
            margin-bottom: 15px;
            overflow-y: hidden;
            overflow-x: scroll;
            -ms-overflow-style: -ms-autohiding-scrollbar;
            -webkit-overflow-scrolling: touch; }
            .table-responsive > .table {
              margin-bottom: 0; }
              .table-responsive > .table > thead > tr > th,
              .table-responsive > .table > thead > tr > td,
              .table-responsive > .table > tbody > tr > th,
              .table-responsive > .table > tbody > tr > td,
              .table-responsive > .table > tfoot > tr > th,
              .table-responsive > .table > tfoot > tr > td {
                white-space: nowrap; }
            .table-responsive > .table-bordered {
              border: 0; }
        }
    </style>
    <!-- STYLE -->
  </head>
  <!-- HEAD SECTION -->
  <!-- BODY SECTION -->
  <body style="background: #f4f4f8;padding: 20px 0;">
  <div class="table-responsive">
    <table class="table" style="max-width: 742px;width: 100%;height: auto;margin: 0 auto;">
    <tr>
      <td>
        <p style="margin-top: 0;margin-left: 0;margin-right: 0;margin-bottom: 20px;padding-top: 0;padding-bottom: 0;padding-left: 0;padding-right: 0;-webkit-text-size-adjust: none;font-family: Arial, Helvetica, sans-serif;font-size: 13px;line-height: 22px;">{{ Lang::get('invoice-payment-done-lang.dear') }} {{ $contact_name }},</p>
      </td>
    </tr>
    <tr>
      <td>
        <p style="margin-top: 0;margin-left: 0;margin-right: 0;margin-bottom: 20px;padding-top: 0;padding-bottom: 0;padding-left: 0;padding-right: 0;-webkit-text-size-adjust: none;font-family: Arial, Helvetica, sans-serif;font-size: 13px;line-height: 22px;">{{ Lang::get('invoice-payment-done-lang.thank-you-for-making-payment-of') }} {{ $id }}.</p>
      </td>
    </tr>
    <tr>
      <td>
        <p style="margin-top: 0;margin-left: 0;margin-right: 0;margin-bottom: 20px;padding-top: 0;padding-bottom: 0;padding-left: 0;padding-right: 0;-webkit-text-size-adjust: none;font-family: Arial, Helvetica, sans-serif;font-size: 13px;line-height: 22px;">{{ Lang::get('invoice-payment-done-lang.download-the-invoice-with-update-payment-status-from-here:') }}: </p>
      </td>
    </tr>
    <tr>
      <td>
        <p style="margin-top: 0;margin-left: 0;margin-right: 0;margin-bottom: 20px;padding-top: 0;padding-bottom: 0;padding-left: 0;padding-right: 0;-webkit-text-size-adjust: none;font-family: Arial, Helvetica, sans-serif;font-size: 13px;line-height: 22px;">{{ Lang::get('invoice-payment-done-lang.looking-forward-to-working-together-soon!') }} </p>
      </td>
    </tr>
    <tr>
      <td>
        <p style="margin-top: 0;margin-left: 0;margin-right: 0;margin-bottom: 20px;padding-top: 0;padding-bottom: 0;padding-left: 0;padding-right: 0;-webkit-text-size-adjust: none;font-family: Arial, Helvetica, sans-serif;font-size: 13px;line-height: 22px;">{{ Lang::get('invoice-payment-done-lang.best') }},<br>
        <span style="margin-top: 0;margin-left: 0;margin-right: 0;margin-bottom: 20px;padding-top: 0;padding-bottom: 0;padding-left: 0;padding-right: 0;-webkit-text-size-adjust: none;font-family: Arial, Helvetica, sans-serif;font-size: 13px;line-height: 22px;font-style: italic;">{{ Lang::get('invoice-estimate-common-template-lang.this-invoice-is-generated-using') }} @php echo str_replace('-', ' ', env('PORTAL_NAME')) @endphp</span><br>
      </p>
      </td>
    </tr>
    </table>
    <table class="table" style="max-width: 742px;width: 100%;height: auto;margin: 0 auto;border: 1px solid #95c3d8;-webkit-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);-moz-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);border-collapse: collapse;border-spacing: 0;background-color: #fff; ">
      <tr class="whitebackground" style="background: #fff;page-break-inside: avoid; ">
        <td style="padding: 10px 30px;border-top: transparent;line-height: 1.42857;
            vertical-align: top;">
          <table style="width: 100%;">
            <tr style="page-break-inside: avoid; ">
              <td class="headertag" style="padding: 10px 0px;font-family: 'Montserrat', sans-serif;font-weight: 900;font-size: 40px;text-transform: uppercase;color: #4d4d4d;border-top: transparent;">INVOICE</td>
              <td class="headerinfo" style="font-family: 'Open Sans', sans-serif;font-weight: 500;font-size: 14px;
                color: #4d4d4d;border-top: transparent;">ID: {{ $id }}  |  Due on: {{ $due_date }}</td>
              <td style="padding: 10px 30px;border-top: transparent;"><a style="height: 120px; width:120px; display:block;" href="#!"><img style="max-width: 100%;" src="{{ url('/') }}/uploads/company-logo/company-logo.png" alt="logo.png" /></a></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr class="whitebackground" style="background: #fff;page-break-inside: avoid; ">
        <td style="padding: 10px 30px;border-top: transparent; text-align: right;">
          <img style=" max-width: 120px; padding-right: 40px;" src="https://pulse.quadrant.technology/pulse-hosted/assets/img/badge.png" alt="paid-badge">
        </td>
      </tr>
      <tr class="whitebackground" style="background: #fff;page-break-inside: avoid; ">
        <td style="padding: 10px 30px;border-top: transparent;">
          <table style="width: 100%;border-collapse: collapse;
          border-spacing: 0; ">
            <tr style="border-top: transparent;page-break-inside: avoid; ">
              <td class="secondrow largewidth" style="padding: 20px 0 50px 0;border-top: transparent;width: 68%;">
                <p class="secondrowtitle" style="font-family: 'Montserrat', sans-serif;font-weight: 900; font-size: 16px;letter-spacing: 1px;margin-bottom: 10px;border-top: transparent;    color: #4d4d4d; "><span style="font-family: 'Playfair Display', serif;font-style: italic;font-size: 25px;font-weight: 400;">from </span>{{ $company_name }}</p>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-bottom: 5px;">{{ $company_address }},</p>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-top: 5px;">{{ $company_country }}, {{ $contact_state }}- {{ $contact_post_code }}</p>
              </td>
              <td class="secondrow smallwidth" style="    border-top: transparent;width: 25%;padding: 20px 0 50px 0;">
                <p class="secondrowtitle" style="font-family: 'Montserrat', sans-serif;font-weight: 900; font-size: 16px;letter-spacing: 1px;margin-bottom: 10px;border-top: transparent;     color: #4d4d4d;"><span style="font-family: 'Playfair Display', serif;font-style: italic;font-size: 25px;font-weight: 400;">to </span>{{ $contact_name }}</p>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-bottom: 5px;">{{ $contact_address }},</p>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-top: 5px;">{{ $contact_country }}, {{ $contact_state }}- {{ $contact_post_code }}</p>
              </td>
            </tr>
            @if(isset($message_to_customer))
            <tr style="border-top: transparent;page-break-inside: avoid; ">
              <td class="secondrow largewidth" style="padding: 20px 0 50px 0;border-top: transparent;width: 68%;">
                <p class="secondrowtitle" style="font-family: 'Montserrat', sans-serif;font-weight: 900; font-size: 16px;letter-spacing: 1px;margin-bottom: 10px;border-top: transparent;    color: #4d4d4d; ">Message to customer</p>
                <p class="message_to_customer" style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-bottom: 5px;">{{ $message_to_customer }},</p>
              </td>
            </tr>
            @endif
          </table>
        </td>
      </tr>
      <tr class="bluetable" style="background: #d0e5fd;page-break-inside: avoid; ">
        <td style="padding: 10px 30px;border-top: transparent;">
          <table style="width: 100%;border-collapse: collapse;
          border-spacing: 0; ">
            <tr class="bluetableheader" style="border-top: transparent;page-break-inside: avoid; ">
              <th class="item" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;border-top: transparent;    text-align: left;">Item Name</th>
              <th class="desc" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;width: 250px;border-top: transparent;    text-align: left;"></th>
              <th class="rate" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;    text-align: left;">Rate</th>
              <th class="quantity" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;    text-align: left;">Quantity</th>
              <th class="total" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;    text-align: left;">Total</th>
            </tr>
            @foreach($items_array as $key=>$value)
            <tr class="bluetableheader bordertop" style="border-top: 1px solid #95c3d8;page-break-inside: avoid; ">
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;">{{ $value['item_details'] }}</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;"></td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;">{{ $value['rate'] }}</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;">{{ $value['quantity'] }}</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;">${{ $value['total'] }}</td>
            </tr>
            @endforeach
          </table>
        </td>
      </tr>
      <tr class="bluetableview withoutpadding" style="background: #d0e5fd;page-break-inside: avoid; ">
        <td style="font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;">
          <table style="width: 100%;border-collapse: collapse;border-spacing: 0; ">
            <tr class="bluetablefooter" style="border-top: transparent;page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 530px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;">Sub-Total - ({{ $items_array[0]['currency'] }})</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ $total }}</td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 430px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;">- Adjustment(s)</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ $adjustment }}</td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 530px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;">- Discount</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ $discount }}</td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 530px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;">Tax</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ $tax }}</td>
            </tr>
            <tr class="bluetablefooter bluetablefooterView" style="page-break-inside: avoid; ">
              <td class="footerblank" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;width: 68%;"></td>
              <td class="blueborder" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;width: 20%;border-bottom: 1px solid #95c3d8;"></td>
              <td class="blueborder" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;width: 20%;border-bottom: 1px solid #95c3d8;"></td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspaceview" style="padding: 10px 30px;width: 530px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;">Total - ({{ $items_array[0]['currency'] }})</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ $inv_total }}</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr class="companyinfo" style="page-break-inside: avoid; ">
        <td style="border-top: transparent;background: #fff;padding: 20px;">
          <table style="width: 100%;border-collapse: collapse;border-spacing: 0; ">
            <tr style="page-break-inside: avoid; ">
              <td style="padding: 10px 30px;border-top: transparent;width: 50%;vertical-align: top;background: #fff;
            padding: 20px;">
                <h1 style="font-family: 'Open Sans', sans-serif;font-size: 16px;font-weight: 600;color: #4d4d4d;text-transform: uppercase;margin: 0;padding: 20px 0;">Questions - Contact us</h1>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 12px;font-weight: 400;color: #4d4d4d;line-height: 20px;
                  margin-bottom: 30px;">All the terms has has been updated and we have <br>deliveredthe project with all the <br>requisite documentation</p>
              </td>
              <td style="padding: 10px 30px;border-top: transparent;width: 50%;vertical-align: top;background: #fff;
            padding: 20px; ">
                <h1 style="    font-family: 'Open Sans', sans-serif;font-size: 16px;font-weight: 600;color: #4d4d4d;text-transform: uppercase;margin: 0;padding: 20px 0;">Terms &amp; Conditions</h1>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 12px;font-weight: 400;color: #4d4d4d;line-height: 20px;margin-bottom: 30px;">- {{ $terms_and_conditions }}.</p>
              </td>
            </tr>
            <tr style="page-break-inside: avoid; "></tr>
          </table>
        </td>
      </tr>
      <tr class="footer">
        <td style="padding: 10px 30px;padding-bottom: 140px !important;
            background: #fff;">
          <table style="width: 100%;border-collapse: collapse;border-spacing: 0; ">
            <tr style="border-top: 1px solid #f0f0f0;border-bottom: 1px solid #f0f0f0;page-break-inside: avoid;">
              <td style="padding: 10px 30px;"><p style="font-family: 'Open Sans', sans-serif;font-size: 16px;font-weight: 600;color: #4d4d4d;text-align: center;"><span class="thankyou" style="font-family: 'Open Sans', sans-serif;font-size: 46px;
            font-weight: 600;color: #4d4d4d;">THANK YOU </span><span class="cursive" style="font-family: 'Playfair Display', serif;font-style: italic;font-size: 25px;font-weight: 400;">for </span> doing business with {{ $company_name }}</p></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr class="footer">
        <td style="padding: 10px 30px;padding-bottom: 140px !important;
            background: #fff;">
          <table style="width: 100%;border-collapse: collapse;border-spacing: 0; ">
            <tr style="">
              <td style="padding: 10px 30px;"><p style="font-family: 'Open Sans', sans-serif;font-size: 16px;font-weight: 600;color: #4d4d4d;text-align: center;"><a href="{{ $pdf_link }}">Show PDF</a></p></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table class="footerportion" style="max-width: 742px;width: 100%;height: auto;margin: 0 auto;border-collapse: collapse;
          border-spacing: 0; ">
      <tr style="page-break-inside: avoid;">
        <td style="padding: 10px 30px;border-top: transparent;">
          <table style="width: 100%;border-top: transparent;border-collapse: collapse;
          border-spacing: 0; ">
            <tr style="page-break-inside: avoid;">
              <td style="padding: 10px 30px;border-top: transparent;"><p style="text-align: center;font-family: 'Open Sans', sans-serif;font-size: 12px;font-weight: 600;color: #9a9a9a;margin: 0;padding: 50px 0 20px 0;">The invoice was generated using @php echo str_replace('-', ' ', env('PORTAL_NAME')) @endphp</p></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
  </body>
</html>