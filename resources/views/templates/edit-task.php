<div class="edit-tasks">
	<div class="edit-task-header">
		<div class="row">
			<div class="col s7">
				<input type="checkbox" class="filled-in" id="filled-in-box-7" />
				<label for="filled-in-box-7"> Follow up with the customer </label>
				<input type="text" name="task_title" id="task-title" value="Follow up with the customer">
				<a href="javascript:void(0);" class="task-removed"></a>
			</div>
		
			<div class="col s4">
				<span class="tasks-priority">
					<a href="javascript:void(0);" class="date-fr">Due Today</a>
					<input type="text" id="date-fr">
				</span>
				<div class="mark-done">
					<input type="checkbox" class="filled-in" id="mark-complete" />
					<label for="mark-complete">Mark as done</label>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s10">
			<div class="task-description">
				<textarea id="task-description-id">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vel tincidunt quam. Pellentesque facilisis, arcu in congue pharetra, leo mauris convallis erat, in vestibulum nisi justo vitae velit.</textarea>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<div class="edit-task-footer">
				<div class="row">
					<div class="col s3">
						<div class="task-assignee"><span>Assign to</span> 
							<select name="assign_to" id="change-assigned-to">
								<option>Arnab</option>
								<option>Ayan</option>
								<option>Jay</option>
								<option>Kunal</option>
								<option>Maria</option>
								<option>Sayani</option>
							</select>
						</div>
					</div>
					<div class="col s6">
						<div class="tag-container">
							<div class="tag-container-child">
							</div>
							<div class="add-tags">
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field">
											<input id="tags" name="add_new_tag" type="text" placeholder="Type tag">
											<a href="javascript:void(0);" class="add-input" id="add-new-tag"> <span></span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col s3">
						<!-- <div class="mark-done">
							<input type="checkbox" class="filled-in" id="mark-complete" />
							<label for="mark-complete">Mark as done</label>
						</div> -->
						<div class="buttons right-align">
							<button class="waves-effect waves-light btn btn-green editTask">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>