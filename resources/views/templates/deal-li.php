<li class="drag-item currently-moving" data-deal-id="4">
	<div id="task-opportunity-0" class="card task">
		<span class="card-bg"></span>
		<div class="card-content clear-fix">
			<span class="card-title blue-text"><a href="#!"></a>new proposal</span>
			<div class="task-assigned">
				<span class="avatar-pic-wrapper"><span class="avatar-pic"><img src="assets/img/avatar-1.jpg"></span></span>
				<span class="avatar-pic-wrapper"><span class="avatar-pic"><img src="assets/img/avatar-1.jpg"></span></span>
			</div>
			<div class="task-price">
				<span class="price"><span>$</span> 15000</span>
			</div>
		</div>

		<div class="card-action">
			<span class="card-id">#4</span>
			<span class="card-status">Follow up</span>
		</div>
		<span class="card-type">NEW</span>
	</div>
</li>