<div id="task-1" class="card">
	<div class="card-content clear-fix">
		<div class="task-price">
			<span class="price"><span>$</span> 35000</span>
		</div>
		<div class="task-assigned">
			<span class="avatar-pic-wrapper"><span class="avatar-pic"><img src="assets/img/avatar-1.jpg"></span></span>
			<span class="avatar-pic-wrapper"><span class="avatar-pic"><img src="assets/img/avatar-1.jpg"></span></span>
			<span class="card-id">#22</span>
		</div>
		
	</div>

	<div class="card-action">
		<span class="card-title"><a href="#!">Deal with Acme, inc.</a></span>
		<span class="card-status green"><a href="#!">Follow up</a></span>
		<span class="card-status green"><a href="#!">New sales lead</a></span>
	</div>

	<div class="card-footer">
		<a href="#!"> View in sales pipe line </a>
	</div>
</div>