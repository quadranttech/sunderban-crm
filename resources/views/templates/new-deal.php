<li class="drag-item" data-deal-id="">
	<div id="task-1" class="card task">
		<span class="card-bg"></span>
		<div class="card-content clear-fix">
			<span class="card-title blue-text">Deal with Acme, inc.</span>
			<div class="task-assigned">
				<span class="avatar-pic-wrapper"><span class="avatar-pic"><img src="assets/img/avatar-1.jpg"></span></span>
			</div>
			<div class="task-price">
				<span class="price"><span>$</span> 35000</span>
			</div>
		</div>

		<div class="card-action">
			<span class="card-id">#22</span>
			<span class="card-status">Follow up</span>
		</div>
		<span class="card-type">New</span>
	</div>
</li>