<li class="document-li">
	<a class="example-image-link" href="javascript:void(0);" data-featherlight="image">
		<div class="document-area">
			<div class="document-wrapper">
				<span>
					<img src="assets/img/doc.png">
				</span>
			</div>
			<div class="desc">
				proposal.doc
			</div>
		</div>
	</a>
	<ul class="document-actions">
		<li class="document-expand"><a href="javascript:void(0);" data-featherlight="image"></a></li>
		<li class="document-downld"><a href="" download></a></li>
		<li class="document-remove"><a href="javascript:void(0);"></a></li>
	</ul>
</li>