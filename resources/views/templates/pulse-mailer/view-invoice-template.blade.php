<!DOCTYPE html>
<html lang="en">
  <!-- HEAD SECTION -->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Invoice Template</title>
    <!-- STYLE -->
    <style type="text/css">
      /****CUSTOM STYLESHEET*****/
        @font-face {
            font-family: 'Montserrat', sans-serif;
            src: url('assets/fonts/montserrat-regular-webfont.woff2') format('woff2'),
                 url('assets/fonts/montserrat-regular-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: 'Open Sans', sans-serif;
            src: url('assets/fonts/opensans-regular-webfont.woff2') format('woff2'),
                 url('assets/fonts/opensans-regular-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;
        }
        @media (max-width: 767px) {
          .table-responsive {
            width: 100%;
            margin-bottom: 15px;
            overflow-y: hidden;
            overflow-x: scroll;
            -ms-overflow-style: -ms-autohiding-scrollbar;
            -webkit-overflow-scrolling: touch; }
            .table-responsive > .table {
              margin-bottom: 0; }
              .table-responsive > .table > thead > tr > th,
              .table-responsive > .table > thead > tr > td,
              .table-responsive > .table > tbody > tr > th,
              .table-responsive > .table > tbody > tr > td,
              .table-responsive > .table > tfoot > tr > th,
              .table-responsive > .table > tfoot > tr > td {
                white-space: nowrap; }
            .table-responsive > .table-bordered {
              border: 0; }
        }
    </style>
    <!-- STYLE -->
  </head>
  <!-- HEAD SECTION -->
  <!-- BODY SECTION -->
  <body style="background: #fff;padding: 0px 0; margin: 0px;">
  <div class="table-responsive">
    <table class="table" style="width: 100%; background-color: #f4f4f8; border-bottom: 1px solid #95c3d8; margin-bottom: 30px;">
      <tr>
        <td>
          <table class="table" style="width: 100%;height: auto;margin: 0 auto;">
            <tr>
              <td style="padding: 10px 0px;border-top: transparent; text-align: right;">
                <a href="{{ $pdf_link }}" style="display: inline-block;border: none; color: #fff;  background-color: #5572f7; text-transform: none; box-shadow: none; border-radius: 3px; padding: 10px 25px; font-size: 14px; font-family: 'Montserrat', sans-serif; font-weight: 600; color: #fff; text-decoration: none;"> {{ Lang::get('invoice-estimate-common-template-lang.download-pdf') }} </a>
                @if($payment=="on")
                  <a href="{{ $payment_link }}" style="display: inline-block;border: none; color: #fff; background-color: #68c096; text-transform: none; box-shadow: none; border-radius: 3px; padding: 10px 25px; font-size: 14px; font-family: 'Montserrat', sans-serif; font-weight: 600; text-decoration: none;"> {{ Lang::get('invoice-estimate-common-template-lang.payment') }} </a>
                @endif
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <table class="table" style="max-width: 742px;width: 100%;height: auto;margin: 0 auto;border: 1px solid #95c3d8;border-collapse: collapse;border-spacing: 0;background-color: #fff; ">
     
      <tr class="whitebackground" style="background: #fff;page-break-inside: avoid; ">
        <td style="padding: 10px 30px;border-top: transparent;line-height: 1.42857;
            vertical-align: top;">
          <table style="width: 100%;">
            <tr style="page-break-inside: avoid; ">
              <td class="headertag" style="padding: 10px 0px;font-family: 'Montserrat', sans-serif;font-weight: 900;font-size: 40px;text-transform: uppercase;color: #4d4d4d;border-top: transparent;">{{ Lang::get('invoice.Invoice') }}</td>
              <td class="headerinfo" style="font-family: 'Open Sans', sans-serif;font-weight: 500;font-size: 14px;
                color: #4d4d4d;border-top: transparent;">{{ Lang::get('invoice-estimate-common-template-lang.id') }}: {{ $id }}  |  {{ Lang::get('task.due-on') }}: {{ $due_date }}</td>
              <td class="eBody pdTp32" style="margin-top: 0;margin-left: 0;margin-right: 0;margin-bottom: 0;padding-top: 20px;padding-bottom: 0;padding-left: 0px;padding-right: 0px;border-collapse: collapse;border-spacing: 0;-webkit-text-size-adjust: none;font-family: Arial, Helvetica, sans-serif;text-align: right;font-size: 0 !important;font-weight: bold;color: #ffffff;background-color: #ffffff;">
                <a href="#" style="height: 65px; overflow: hidden; width: 120px; display: inline-block; padding-top: 0;padding-bottom: 0;padding-left: 0;padding-right: 0;display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;font-family: Arial, Helvetica, sans-serif;color: #ffffff;text-align: left;font-size: 18px;font-weight: bold;line-height: 0;">
                <img class="imageFix" src="{{ url('/') }}/uploads/company-logo/company-logo.png" alt="logo" style="max-width: 100%; margin-top: 0;margin-left: 0;margin-right: 0;margin-bottom: 0;padding-top: 0;padding-bottom: 0;padding-left: 0;padding-right: 0;height: auto;width: auto;line-height: 100%;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;border: none;display: block;vertical-align: top;"></a>
                </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr class="whitebackground" style="background: #fff;page-break-inside: avoid; ">
        <td style="padding: 10px 30px;border-top: transparent;">
          <table style="width: 100%;border-collapse: collapse;
          border-spacing: 0; ">
            <tr style="border-top: transparent;page-break-inside: avoid; ">
              <td class="secondrow largewidth" style="padding: 20px 0 50px 0;border-top: transparent;width: 68%;">
                <p class="secondrowtitle" style="font-family: 'Montserrat', sans-serif;font-weight: 900; font-size: 16px;letter-spacing: 1px;margin-bottom: 10px;border-top: transparent;    color: #4d4d4d; "><span style="font-family: 'Playfair Display', serif;font-style: italic;font-size: 25px;font-weight: 400;">{{ Lang::get('invoice-estimate-common-template-lang.from') }} </span>{{ $company_name }}</p>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-bottom: 5px;">{{ $company_address }},</p>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-top: 5px;">@if($company_country!=""){{ $company_country }}, @endif  @if($company_state!=""){{ $company_state }}- @endif {{ $company_post_code }}</p>
              </td>
              <td class="secondrow smallwidth" style="    border-top: transparent;width: 25%;padding: 20px 0 50px 0;">
                <p class="secondrowtitle" style="font-family: 'Montserrat', sans-serif;font-weight: 900; font-size: 16px;letter-spacing: 1px;margin-bottom: 10px;border-top: transparent;     color: #4d4d4d;"><span style="font-family: 'Playfair Display', serif;font-style: italic;font-size: 25px;font-weight: 400;">{{ Lang::get('invoice-estimate-common-template-lang.to') }} </span>{{ $contact_name }}</p>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-bottom: 5px;">@if($contact_address!=""){{ $contact_address }}, @endif</p>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-top: 5px;">@if($contact_country!=""){{ $contact_country }}, @endif  @if($contact_state!=""){{ $contact_state }}- @endif {{ $contact_post_code }}</p>
              </td>
            </tr>
            @if(isset($message_to_customer))
            <tr style="border-top: transparent;page-break-inside: avoid; ">
              <td class="secondrow largewidth" style="padding: 20px 0 50px 0;border-top: transparent;width: 68%;">
                <p class="secondrowtitle" style="font-family: 'Montserrat', sans-serif;font-weight: 900; font-size: 16px;letter-spacing: 1px;margin-bottom: 10px;border-top: transparent;    color: #4d4d4d; ">{{ Lang::get('estimate.message-to-customer') }}</p>
                <p class="message_to_customer" style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-bottom: 5px;">{{ $message_to_customer }},</p>
              </td>
            </tr>
            @endif
          </table>
        </td>
      </tr>
      <tr class="bluetable" style="background: #d0e5fd;page-break-inside: avoid; ">
        <td style="padding: 10px 30px;border-top: transparent;">
          <table style="width: 100%;border-collapse: collapse;
          border-spacing: 0; ">
            <tr class="bluetableheader" style="border-top: transparent;page-break-inside: avoid; ">
              <th class="item" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;border-top: transparent;    text-align: left;">{{ Lang::get('invoice-estimate-common-template-lang.item-name') }}</th>
              <th class="desc" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;width: 250px;border-top: transparent;    text-align: left;"></th>
              <th class="rate" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;    text-align: left;">{{ Lang::get('invoice-estimate-common-template-lang.rate') }} / ({{ $quantity_type }})</th>
              <th class="quantity" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;    text-align: left;">{{ Lang::get('invoice-estimate-common-template-lang.quantity') }}</th>
              <th class="total" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;    text-align: left;">{{ Lang::get('invoice-estimate-common-template-lang.total') }}</th>
            </tr>
            @foreach($items_array as $key=>$value)
            <tr class="bluetableheader bordertop" style="border-top: 1px solid #95c3d8;page-break-inside: avoid; ">
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;">{{ $value['item_details'] }}</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;"></td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;">{{ $value['rate'] }}</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;">{{ $value['quantity'] }}</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;">{{ $value['total'] }}</td>
            </tr>
            @endforeach
          </table>
        </td>
      </tr>
      <tr class="bluetableview withoutpadding" style="background: #d0e5fd;page-break-inside: avoid; ">
        <td style="font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;">
          <table style="width: 100%;border-collapse: collapse;border-spacing: 0; ">
            <tr class="bluetablefooter" style="border-top: transparent;page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 430px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ Lang::get('invoice-estimate-common-template-lang.sub-total') }} - ({{ $currency_symbol }})</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ $total }}</td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 430px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;">- {{ Lang::get('invoice-estimate-common-template-lang.adjustments') }}</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ $adjustment }}</td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 430px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;">- {{ Lang::get('invoice-estimate-common-template-lang.discount') }}</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ $discount }}</td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 430px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ Lang::get('invoice-estimate-common-template-lang.tax') }}</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ $tax }}</td>
            </tr>
            <tr class="bluetablefooter bluetablefooterView" style="page-break-inside: avoid; ">
              <td class="footerblank" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;width: 60%;"></td>
              <td class="blueborder" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;width: 25%;border-bottom: 1px solid #95c3d8;"></td>
              <td class="blueborder" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;width: 15%;border-bottom: 1px solid #95c3d8;"></td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspaceview" style="padding: 10px 30px;width: 430px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ Lang::get('invoice-estimate-common-template-lang.total') }} - ({{ $currency_symbol }})</td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ $inv_total }}</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr class="companyinfo" style="page-break-inside: avoid; ">
        <td style="border-top: transparent;background: #fff;padding: 20px;">
          <table style="width: 100%;border-collapse: collapse;border-spacing: 0; ">
            <tr style="page-break-inside: avoid; ">
              
              <td style="padding: 10px 30px;border-top: transparent;width: 50%;vertical-align: top;background: #fff;
            padding: 20px; ">
                <h1 style="    font-family: 'Open Sans', sans-serif;font-size: 16px;font-weight: 600;color: #4d4d4d;text-transform: uppercase;margin: 0;padding: 20px 0;">{{ Lang::get('estimate.terms-and-condition') }}</h1>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 12px;font-weight: 400;color: #4d4d4d;line-height: 20px;margin-bottom: 30px;">@if($terms_and_conditions!="")  {{ $terms_and_conditions }} @endif</p>
              </td>
            </tr>
            <tr style="page-break-inside: avoid; "></tr>
          </table>
        </td>
      </tr>
      <tr class="footer">
        <td style="padding: 10px 30px 40px; background: #fff;">
          <table style="width: 100%;border-collapse: collapse;border-spacing: 0; ">
            <tr style="border-top: 1px solid #f0f0f0;page-break-inside: avoid;">
              <td style="padding: 10px 30px;"><p style="font-family: 'Open Sans', sans-serif;font-size: 16px;font-weight: 600;color: #4d4d4d;text-align: center;"><span class="thankyou" style="font-family: 'Open Sans', sans-serif;font-size: 46px; position: relative; top: 10px;
            font-weight: 600;color: #4d4d4d;">{{ Lang::get('invoice-estimate-common-template-lang.thank-you') }} </span><span class="cursive" style="font-family: 'Playfair Display', serif;font-style: italic;font-size: 25px;font-weight: 400;">{{ Lang::get('invoice-estimate-common-template-lang.for') }} </span> {{ Lang::get('invoice-estimate-common-template-lang.doing-business-with') }} {{ $company_name }}</p></td>
            </tr>
          </table>
        </td>
      </tr>
      
    </table>
    <table class="footerportion" style="max-width: 742px;width: 100%;height: auto;margin: 0 auto;border-collapse: collapse;
          border-spacing: 0; ">
      <tr style="page-break-inside: avoid;">
        <td style="padding: 10px 30px;border-top: transparent;">
          <table style="width: 100%;border-top: transparent;border-collapse: collapse;
          border-spacing: 0; ">
            <tr style="page-break-inside: avoid;">
              <td style="padding: 10px 30px;border-top: transparent;"><p style="text-align: center;font-family: 'Open Sans', sans-serif;font-size: 12px;font-weight: 600;color: #9a9a9a;margin: 0;padding: 50px 0 20px 0;">{{ Lang::get('invoice-estimate-common-template-lang.this-invoice-is-generated-using') }} @php echo str_replace('-', ' ', env('PORTAL_NAME')) @endphp</p></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
  </body>
</html>