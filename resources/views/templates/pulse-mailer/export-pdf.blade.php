<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table style="max-width: 742px; width: 100%; height: auto; margin: 0 auto; border-collapse: collapse; border-spacing: 0; background-color: #fff;">
		<thead>
			<tr>
				<th style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;border-top: transparent;    text-align: left;">Date</th>
				<th style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;border-top: transparent;    text-align: left;">Contact</th>
				<th style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;border-top: transparent;    text-align: left;">Deal Title</th>
				<th style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;border-top: transparent;    text-align: left;">Deal Status</th>
				<th style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;border-top: transparent;    text-align: left;">Owner</th>
				<th style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;border-top: transparent;    text-align: left;">Deal Value</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $key=>$value)
			<tr>
				@foreach($value as $value2)
					<td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 400;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;border-top: transparent;    text-align: left;">{{ $value2 }}</td>
				@endforeach
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>
