<!DOCTYPE html>
<html lang="en">
  <!-- HEAD SECTION -->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Estimate Preview</title>
    <!-- STYLE -->
    <style type="text/css">
      /****CUSTOM STYLESHEET*****/
        @font-face {
            font-family: 'Montserrat', sans-serif;
            src: url('assets/fonts/montserrat-regular-webfont.woff2') format('woff2'),
                 url('assets/fonts/montserrat-regular-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;

        }
        @font-face {
            font-family: 'Open Sans', sans-serif;
            src: url('assets/fonts/opensans-regular-webfont.woff2') format('woff2'),
                 url('assets/fonts/opensans-regular-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;

        }
        @media (max-width: 767px) {
          .table-responsive {
            width: 100%;
            margin-bottom: 15px;
            overflow-y: hidden;
            overflow-x: scroll;
            -ms-overflow-style: -ms-autohiding-scrollbar;
            
            -webkit-overflow-scrolling: touch; }
            .table-responsive > .table {
              margin-bottom: 0; }
              .table-responsive > .table > thead > tr > th,
              .table-responsive > .table > thead > tr > td,
              .table-responsive > .table > tbody > tr > th,
              .table-responsive > .table > tbody > tr > td,
              .table-responsive > .table > tfoot > tr > th,
              .table-responsive > .table > tfoot > tr > td {
                white-space: nowrap; }
            .table-responsive > .table-bordered {
              border: 0; }
        }
    </style>
    <!-- STYLE -->
    
  </head>
  <!-- HEAD SECTION -->
  <!-- BODY SECTION -->
  <body style="background: #fff;padding: 0px 0; margin: 0px;">
  <div class="table-responsive">
    <table class="table" style="width: 100%; background-color: #f4f4f8; border-bottom: 1px solid #95c3d8; margin-bottom: 30px;">
      <tr>
        <td>
          <table class="table" style="width: 100%;height: auto;margin: 0 auto;">
            <tr>
              <td style="padding: 10px 0px;border-top: transparent; text-align: right;">
                <a href="javascript:void(0)" style="text-transform: none; box-shadow: none; border-radius: 3px; padding: 0 20px; font-size: 14px; font-family: 'Open Sans',sans-serif; font-weight: 600; background-color: #5572f7; margin-right: 15px; display: inline-block; text-decoration: none; text-align: center; color: #fff; height: 36px; line-height: 36px;"><span class="download-pdf"></span></a>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table class="table" style="max-width: 742px;width: 100%;height: auto;margin: 0 auto;border: 1px solid #95c3d8;border-collapse: collapse;border-spacing: 0;background-color: #fff; ">
      <tr class="whitebackground" style="background: #fff;page-break-inside: avoid; ">
        <td style="padding: 10px 30px;border-top: transparent;line-height: 1.42857;
            vertical-align: top;">
          <table style="width: 100%;">
            <tr style="page-break-inside: avoid; ">
              <td class="headertag" style="padding: 10px 30px 10px 0;font-family: 'Montserrat', sans-serif;font-weight: 900;font-size: 20px;text-transform: uppercase;color: #4d4d4d;border-top: transparent;"><span class="estimate"></span> </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr class="whitebackground" style="background: #fff;page-break-inside: avoid; ">
        <td style="padding: 10px 30px;border-top: transparent;">
          <table style="width: 100%;border-collapse: collapse;
          border-spacing: 0; ">
            <tr style="border-top: transparent;page-break-inside: avoid; ">
              <td class="secondrow largewidth" style="padding: 20px 0 50px 0;border-top: transparent;width: 68%;">
                <p class="secondrowtitle" style="font-family: 'Montserrat', sans-serif;font-weight: 900; font-size: 16px;letter-spacing: 1px;margin-bottom: 10px;border-top: transparent;    color: #4d4d4d; "><span class="company_name" style="font-family: 'Playfair Display', serif;font-style: italic;font-size: 25px;font-weight: 400;"><span class="from"></span> </span></p>
                <p class="company_address" style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-bottom: 5px;"></p>
                <p class="country-state-post-codes" style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-top: 5px;"></p>
              </td>
              <td class="secondrow smallwidth" style="    border-top: transparent;width: 25%;padding: 20px 0 50px 0;">
                <p class="secondrowtitle" style="font-family: 'Montserrat', sans-serif;font-weight: 900; font-size: 16px;letter-spacing: 1px;margin-bottom: 10px;border-top: transparent;     color: #4d4d4d;"><span class="contact_name" style="font-family: 'Playfair Display', serif;font-style: italic;font-size: 25px;font-weight: 400;"><span class="to"></span> </span></p>
                <p class="contact_address" style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-bottom: 5px;"></p>
                <p class="contact-country-state-post-code" style="font-family: 'Open Sans', sans-serif;font-size: 14px;color: #4d4d4d;margin-top: 5px;"></p>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr class="bluetable" style="background: #d0e5fd;page-break-inside: avoid; ">
        <td style="padding: 10px 30px;border-top: transparent;">
          <table style="width: 100%;border-collapse: collapse;
          border-spacing: 0; ">
            <tr class="bluetableheader" style="border-top: transparent;page-break-inside: avoid; ">
              <th class="item" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;border-top: transparent;    text-align: left;"><span class="item_name"></span></th>
              <th class="desc" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;width: 250px;border-top: transparent;    text-align: left;"></th>
              <th class="rate" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;    text-align: left;"><span class="item_rate"></span> / (<span class='quantity'></span>)</th>
              <th style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;    text-align: left;"><span class="item_quantity"></th>
              <th class="total" style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;
              padding: 10px;padding-top: 30px;border-top: transparent;    text-align: left;"><span class="total"></span></th>
            </tr>
            
            <tr class="bluetableheader bordertop item-clone" style="border-top: 1px solid #95c3d8;page-break-inside: avoid; ">
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;"></td>
             <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;"></td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;"></td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;"></td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;
                  padding: 10px;border-top: transparent;padding-top: 20px !important;"></td>
            </tr>
            
          </table>
        </td>
      </tr>
      <tr class="bluetableview withoutpadding" style="background: #d0e5fd;page-break-inside: avoid; ">
        <td style="font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;">
          <table style="width: 100%;border-collapse: collapse;border-spacing: 0; ">
            <tr class="bluetablefooter" style="border-top: transparent;page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 430px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;"><span class="sub-total"></span> - (<span class="currency"></span>)</td>
              <td class="sub_total" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">$</td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 430px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;">- <span class="adjustment"></span></td>
              <td class="adjustment" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">{{ $adjustment }}</td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 430px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;">- <span class="discount"></span></td>
              <td class="discount" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">$</td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspace" style="padding: 10px 30px;width: 430px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="font-family: 'Open Sans', sans-serif;   font-size: 14px;font-weight: 600;color: #4d4d4d;padding: 10px;border-top: transparent;"><span class="tax"></span></td>
              <td class="tax" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;">$</td>
            </tr>
            <tr class="bluetablefooter bluetablefooterView" style="page-break-inside: avoid; ">
              <td class="footerblank" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;width: 68%;"></td>
              <td class="blueborder" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;width: 20%;border-bottom: 1px solid #95c3d8;"></td>
              <td class="blueborder" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;width: 20%;border-bottom: 1px solid #95c3d8;"></td>
            </tr>
            <tr class="bluetablefooter" style="page-break-inside: avoid; ">
              <td class="blankspaceview" style="padding: 10px 30px;width: 430px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
              <td style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 500;color: #4d4d4d;padding: 10px;border-top: transparent;"><span class="total"></span> - (<span class="currency"></span>)</td>
              <td class="inv_total" style="padding: 10px 30px;font-family: 'Open Sans', sans-serif;   font-size: 12px;font-weight: 400;color: #4d4d4d;padding: 10px;border-top: transparent;"></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr class="companyinfo" style="page-break-inside: avoid; ">
        <td style="border-top: transparent;background: #fff;padding: 20px;">
          <table style="width: 100%;border-collapse: collapse;border-spacing: 0; ">
            <tr style="page-break-inside: avoid; ">
              
              <td style="padding: 10px 30px;border-top: transparent;width: 50%;vertical-align: top;background: #fff;
            padding: 20px; ">
                <h1 style="    font-family: 'Open Sans', sans-serif;font-size: 16px;font-weight: 600;color: #4d4d4d;text-transform: uppercase;margin: 0;padding: 20px 0;"><span class="terms_and_conditions"></span></h1>
                <p class="terms_and_conditions" style="font-family: 'Open Sans', sans-serif;font-size: 12px;font-weight: 400;color: #4d4d4d;line-height: 20px;margin-bottom: 30px;"></p>
              </td>
            </tr>
            <tr style="page-break-inside: avoid; "></tr>
          </table>
        </td>
      </tr>
      <tr class="footer">
        <td style="padding: 10px 30px 40px; background: #fff;">
          <table style="width: 100%;border-collapse: collapse;border-spacing: 0; ">
            <tr style="border-top: 1px solid #f0f0f0;page-break-inside: avoid;">
              <td style="padding: 10px 30px;"><p style="font-family: 'Open Sans', sans-serif;font-size: 16px;font-weight: 600;color: #4d4d4d;text-align: center;"><span class="thankyou" style="font-family: 'Open Sans', sans-serif;font-size: 46px; position: relative; top: 10px;
            font-weight: 600;color: #4d4d4d;"><span class="thank_you"></span> </span><span class="cursive" style="font-family: 'Playfair Display', serif;font-style: italic;font-size: 25px;font-weight: 400;"><span class="for"></span> </span> <span class="doing-business-with"></span> <span class="company_name_2"></span></p></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table class="footerportion" style="max-width: 742px;width: 100%;height: auto;margin: 0 auto;border-collapse: collapse;
          border-spacing: 0; ">
      <tr style="page-break-inside: avoid;">
        <td style="padding: 10px 30px;border-top: transparent;">
          <table style="width: 100%;border-top: transparent;border-collapse: collapse;
          border-spacing: 0; ">
            <tr style="page-break-inside: avoid;">
              <td style="padding: 10px 30px;border-top: transparent;"><p style="text-align: center;font-family: 'Open Sans', sans-serif;font-size: 12px;font-weight: 600;color: #9a9a9a;margin: 0;padding: 50px 0 20px 0;"><span class="this-invoice-is-generated-using"></span> <span class="portal_name"></span> </p></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
  </body>
</html>