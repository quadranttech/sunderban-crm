<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>@if(isset($session) && $session=='sign_up')dealpipe CRM Installation Step 2 @else dealpipe CRM Installation Step 1 @endif</title>
		<!--Import Google Icon Font-->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:300i,400,600,700|Roboto:300,400,400i,500,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="{{ url('/') }}/assets/css/materialize.css" />
		<link type="text/css" rel="stylesheet" href="{{ url('/') }}/assets/css/installation.css" />
		<link type="text/css" rel="stylesheet" href="{{ url('/assets/css/animate.css') }}" />
		<!-- <link type="text/css" rel="stylesheet" href="assets/css/style.css" /> -->
		<link type="text/css" rel="stylesheet" href="{{ url('/assets/css/theme-style.css') }}" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.min.css" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/js/Jcrop.min.js"></script>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="installation-wrapper">
			<span class="blue-overlay"></span>
			<div class="installation-frame-outer">
				<div class="installation-frame-inner">
					<div class="installation-frame-contain clear-fix">
						<div class="full-width">
							<div class="form-wrapper">
								<div class="logo-wrapper">
									<img src="{{ url('/') }}/assets/img/dealpipe-logo.png">
								</div>
								<div class="step-slider-wrapper">
									<div class="step-slider">
										<span class="step @if(isset($session) && $session=='sign_up')half @endif">
									</div>
									<span class="steps one active @if(isset($session) && $session=='sign_up')activate @endif">
										<span class="number">1</span>
										<!-- <i class="material-icons">done</i> -->
										<span>
											<svg class="checkMark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
												<circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
												<path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
											</svg>
										</span>
										<span class="form-title">Database Configaration</span>
									</span>
									<span class="steps two @if(isset($session) && $session=='sign_up')active @endif">
										<span class="number">2</span>
										<!-- <i class="material-icons">done</i> -->
										<span>
											<svg class="checkMark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
												<circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
												<path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
											</svg>
										</span>
										<span class="form-title">Admin Configaration</span>
									</span>
									<span class="steps three">
										<span class="number">3</span>
										<!-- <i class="material-icons">done</i> -->
										<span>
											<svg class="checkMark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
												<circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
												<path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
											</svg>
										</span>
										<span class="form-title">Company Details</span>
									</span>
								</div>
								<div class="form-one" style="display:@if($session=='install') block @else none @endif">
									<form class="installer-form" id="login-form" action="{{ url('/') }}/import-db" role="form" method="POST">
										{{ csrf_field() }}
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50x50 clear-fix">
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<input name="database_host" type="text" class="validate" value="localhost" autofocus>
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="log-email">Database Host</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<input name="database_name" type="text" class="validate" value="pulsecrm" autofocus>
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="log-email">Database Name</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<input name="database_username" type="text" class="validate">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="password">Database Username</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<input name="database_password" type="password" class="validate">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="password">Database Password</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<select name="timezone">
															      <option value="" disabled="" selected="">Timezone</option>
                                                                  @include('timezone');
											    				</select>
													        </div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
									<div class="button center">
										<button class="waves-effect waves-light btn btn-green" id="check-database-connectivity">Next</button>
									</div>
								</div>
								<div class="form-two" style="display:@if($session=='sign_up') block @else none @endif">
									<form class="signup-form" id="signup-form" role="form" method="POST" action="{{ route('register') }}" style="display:@if($session=='sign_up') block @else none @endif">
										{{ csrf_field() }}
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50x50 clear-fix">
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
																<input type="hidden" name="role" value="1">
																<input id="full_name" name="name" type="hidden" class="validate" value="">
													          	<input id="first_name" name="first_name" type="text" class="validate" value="@if(isset($first_name)) {{ $first_name }} @endif">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="first_name">First Name</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
													          	<input id="last_name" name="last_name" type="text" class="validate" value="@if(isset($last_name)) {{ $last_name }} @endif">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="last_name">Last Name</label>
													        </div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50x50 clear-fix">
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<input id="signup-email" name="email" type="email" class="validate" value="">
													          	@if ($errors->has('email'))
								                                    <span class="help-block">
								                                        <strong>{{ $errors->first('email') }}</strong>
								                                    </span>
								                                @endif
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="email">Email</label>
													        </div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50x50 clear-fix">
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
													          	<input id="signup-password" name="password" type="password" class="validate">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="password">Password</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
													          	<input id="password-confirm" name="password_confirmation" type="password" class="validate">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="password-confirm">Repeat Password</label>
													        </div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
									<div class="button center">
										<button class="waves-effect waves-light btn btn-green" id="signup-button" data-signup="install" disabled>Next</button>
									</div>
								</div>
								<div class="form-three">
									<form class="clear-fix" id="company-form" name="company-form" method="POST" action="save-company-details" enctype="multipart/form-data">
										<div class="left-side">
											<div class="row">
												<div class="col s12">
													<div class="form-layout form-layout-50x20x30 clear-fix">
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
																	<input type="hidden" name="_token" value="{{ csrf_token()}}">
										          					<input id="company_name" name="company_name" type="text" class="validate" value="">
										          					<label for="company_name">Company Name</label>
										        				</div>
															</div>
														</div>
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
																	<select name="buisness_type">
																      <option value="" disabled="" selected="">Business Type</option>
                                                                      <option value="digital-agency" selected="">Digital Agency</option>
                                                                      <option value="it-telecom">IT/ Telecom</option>
                                                                      <option value="govt-military">Government/ Military</option>
                                                                      <option value="large-enterprise">Large Enterprise</option>
                                                                      <option value="mang-service-provider">Management Service Provider</option>
                                                                      <option value="small-medium-enterprise">Small/ Medium Enterprise</option>
                                                                      <option value="real-estate">Real Estate</option>
												    				</select>
																</div>	
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col s12">
													<div class="form-layout clear-fix">
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
										          					<input id="street_address" name="street_address" type="text" class="validate" value="">
										          					<label for="street_address">Street Address</label>
										        				</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col s12">
													<div class="form-layout form-layout-50x50 clear-fix">
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
										          					 <input id="city" name="city" type="text" class="validate" value="">
										          				<label for="city">City</label>
										        				</div>
															</div>
														</div>
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
										          					<input id="state/region" name="state" type="text" class="validate" value="">
										          					<label for="state/region">State/Region</label>
										        				</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col s12">
													<div class="form-layout form-layout-50x50 clear-fix">
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
										          					<select name="country">
														          		<option value="" disabled selected>Country</option>
																	    @include('country');
												    				</select>
										        				</div>
															</div>
														</div>
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
										          					<input id="pin_code" name="pin_code" type="text" class="validate" value="">
										          					<input type="hidden" name="company_id" value="">
										          					<label for="pin_code">Pin Code</label>
										        				</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col s12">
													<div class="form-layout form-layout-50x50 clear-fix">
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
										          					<input id="portal_name" name="portal_name" type="text" class="validate" value="@if(!empty($getCompanyDetails)){{ $getCompanyDetails->portal_name }}@endif">
										          					<label for="portal_name"> Portal Name</label>
										        				</div>
															</div>
														</div>
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
										          					<select name="language">
                                                  						@include('language');
												    				</select>
												    				<label>Languages</label>
										        				</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>	
									</form>
									<div class="button center">
										<button class="waves-effect waves-light btn btn-green" id="save-company-details" data-time="install">Finish</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal modal-fixed-footer" id="image-cropper" style="z-index: 1003;">
			<button class="modal-close"></button>
		    <div class="modal-content">
		      	<img src="" id="cropimage">
		      	<div class="buttons right">
			    	<button class="waves-effect waves-light btn btn-dark-orange cancel-image-crop-signin">Cancel</button>
			    	<a href="#!" class="waves-effect waves-light btn btn-green image-crop-signin">Save</a>
			    </div>
		    </div>
		</div>
		<div class="modal-overlay" id="materialize-modal-overlay-2" style="z-index: 1002; display: none; opacity: 0.8;"></div>
		<script>
		var APP_URL = {!! json_encode(url('/')) !!}
		</script>
		<script type="text/javascript" src="{{ url('/') }}/assets/js/materialize.min.js"></script>
		<script type="text/javascript" src="{{url('/assets/js/jquery-ui.js')}}"></script>
		<script src="{{url('/assets/js/frontend.js')}}"></script>
	</body>
</html>