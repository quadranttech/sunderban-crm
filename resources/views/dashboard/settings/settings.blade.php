@extends('layouts.dashboard-header')
@section('content')
			<div class="bottom-header">
			    <div class="had-container"> 
			      	<div class="row">
			      		<div class="col s8">
			      		</div>
			        	<div class="col s4 right-align">
			        	</div> <!-- END COL -->
			      	</div> <!-- END ROW -->
			    </div> <!-- END CONTAINER -->
			</div>  <!-- END BOTTOM HEADER -->
			<div class="contents contextual-section-wrapper content-tab-view">
		  		<div class="had-container">
					<div class="contextual-section settings">
						<div class="row">
							<div class="col s12">
								<!-- LEFT PANEL -->
							    <div class="content border">
							    	<h3>{{ Lang::get('settings.settings') }}</h3>
							    	<div id="tabs">
										<ul class="clear-fix">
											<li class="@if($page=='company_details') ui-tabs-active @endif"><a href="#tabs-1" data-query="company_details" class="settings-li-a">{{ Lang::get('common.company') }}</a></li>
											<li class="@if($page=='email') ui-tabs-active @endif"><a href="#tabs-2" data-query="email" class="settings-li-a">{{ Lang::get('common.email') }}</a></li>
											<li class="@if($page=='user_roles') ui-tabs-active @endif"><a href="#tabs-3" data-query="user_roles" class="settings-li-a">{{ Lang::get('settings.user-and-roles') }}</a></li>
											<li class="@if($page=='estimate') ui-tabs-active @endif"><a href="#tabs-4" class="settings-proposal" data-query="estimate">{{ Lang::get('estimate.Estimate') }}</a></li>
											<li class="@if($page=='proposal') ui-tabs-active @endif"><a href="#tabs-5" class="settings-proposal" data-query="proposal">{{ Lang::get('invoice.Invoice') }}</a></li>
											<!-- <li class="@if($page=='smtp') ui-tabs-active @endif"><a href="#tabs-6" class="settings-smtp" data-query="smtp">Smtp</a></li> -->
											<li class="@if($page=='payment_gateway') ui-tabs-active @endif"><a href="#tabs-7" class="settings-payment-gateway" data-query="payment_gateway">{{ Lang::get('settings.payment-gateway') }}</a></li>
											<li class="@if($page=='deal_source') ui-tabs-active @endif"><a href="#tabs-8" class="deal-source" data-query="deal_source">{{ Lang::get('deal.Deal') }}</a></li>
											<li class="@if($page=='financials') ui-tabs-active @endif"><a href="#tabs-9" class="deal-source" data-query="financials">{{ Lang::get('settings.financial') }}</a></li>
											<li class="@if($page=='automation') ui-tabs-active @endif"><a href="#tabs-10" class="settings-li-a automation" data-query="automation">{{ Lang::get('settings.automation') }}</a></li>
											<li class="@if($page=='import') ui-tabs-active @endif"><a href="#tabs-11" class="settings-li-a automation" data-query="import">{{ Lang::get('settings.import') }}</a></li>
										</ul>
										<div id="tabs-1" class="settings-tabs @if($page=='company_details') show-content @endif">
											<div class="tabs-wrapper">
												<div class="row">
													<div class="col s7">
														<form class="clear-fix" id="company-form" name="company-form" method="POST" action="save-company-details" enctype="multipart/form-data">
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-50x20x30 clear-fix">
																		<div class="form-layout-50">
																			<div class="field-group">
																				<div class="input-field">
																					<input type="hidden" name="_token" value="{{ csrf_token()}}">
														          					<input id="company_name" name="company_name" type="text" class="validate" value="@if(!empty($getCompanyDetails)){{ $getCompanyDetails->company_name }} @endif">
														          					<label for="company_name">{{ Lang::get('common.company') }} {{ Lang::get('common.name') }}</label>
														          					<span class="error-mark"></span>
												          							<span class="check-mark"></span>
														        				</div>
																			</div>
																		</div>
																		<div class="form-layout-50">
																			<div class="field-group">
																				<div class="input-field select-field">
																					<select name="buisness_type">
																				      	<option value="" disabled selected>{{ Lang::get('settings.buisness-type') }}</option>
																				      	<option value="digital-agency" @if(!empty($getCompanyDetails) && $getCompanyDetails->buisness_type == "digital-agency") selected @endif>{{ Lang::get('settings.digital-agency') }}</option>
																				      	<option value="it-telecom" @if(!empty($getCompanyDetails) && $getCompanyDetails->buisness_type == "it-telecom") selected @endif>{{ Lang::get('settings.it-telecom') }}</option>
																				      	<option value="govt-military" @if(!empty($getCompanyDetails) && $getCompanyDetails->buisness_type == "govt-military") selected @endif>{{ Lang::get('settings.government-military') }}</option>
																				      	<option value="large-enterprise" @if(!empty($getCompanyDetails) && $getCompanyDetails->buisness_type == "large-enterprise") selected @endif>{{ Lang::get('settings.large-enterprise') }}</option>
																				      	<option value="mang-service-provider" @if(!empty($getCompanyDetails) && $getCompanyDetails->buisness_type == "mang-service-provider") selected @endif>{{ Lang::get('settings.management-service-provider') }}</option>
																				      	<option value="small-medium-enterprise" @if(!empty($getCompanyDetails) && $getCompanyDetails->buisness_type == "small-medium-enterprise") selected @endif>{{ Lang::get('settings.small-medium-enterprise') }}</option>
																				      	<option value="real-estate" @if(!empty($getCompanyDetails) && $getCompanyDetails->buisness_type == "real-estate") selected @endif>{{ Lang::get('settings.real-estate') }}</option>
																    				</select>
																    				<label>Business Type</label>
																				</div>	
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col s12">
																	<div class="form-layout clear-fix">
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="street_address" name="street_address" type="text" class="validate" value="@if(!empty($getCompanyDetails)){{ $getCompanyDetails->address }}@endif">
														          					<label for="street_address">{{ Lang::get('contact.street_address') }}</label>
														          					<span class="error-mark"></span>
												          							<span class="check-mark"></span>
														        				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-50x50 clear-fix">
																		<div class="form-layout-50">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="city" name="city" type="text" class="validate" value="@if(!empty($getCompanyDetails)){{ $getCompanyDetails->city }}@endif">
														          					<label for="city">{{ Lang::get('contact.city') }}</label>
														          					<span class="error-mark"></span>
												          							<span class="check-mark"></span>
														        				</div>
																			</div>
																		</div>
																		<div class="form-layout-50">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="state/region" name="state" type="text" class="validate" value="@if(!empty($getCompanyDetails)){{ $getCompanyDetails->state }}@endif">
														          					<label for="state/region">{{ Lang::get('contact.state-region') }}</label>
														          					<span class="error-mark"></span>
												          							<span class="check-mark"></span>
														        				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-50x50 clear-fix">
																		<div class="form-layout-50">
																			<div class="field-group">
																				<div class="input-field select-field">
														          					<select name="country">
																		          		<option value="" disabled selected>{{ Lang::get('contact.country') }}</option>
																				      	@include('country',['country' => $getCompanyDetails->country]);
																    				</select>
																    				<label>Country</label>
														        				</div>
																			</div>
																		</div>
																		<div class="form-layout-50">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="pin_code" name="pin_code" type="text" class="validate" value="@if(!empty($getCompanyDetails)){{ $getCompanyDetails->post_code }}@endif">
														          					<input type="hidden" name="company_id" value="@if(!empty($getCompanyDetails)){{ $getCompanyDetails->id }}@endif">
														          					<label for="pin_code">{{ Lang::get('contact.post-code') }}</label>
														          					<span class="error-mark"></span>
												          							<span class="check-mark"></span>
														        				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-50x50 clear-fix">
																		<div class="form-layout-50">
																			<div class="field-group">
																				<div class="input-field select-field">
														          					<select name="timezone">
                                                                  						@include('timezone',['timezone' => $timezone]);
																    				</select>
																    				<label>Timezone</label>
														        				</div>
																			</div>
																		</div>
																		<div class="form-layout-50">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="portal_name" name="portal_name" type="text" class="validate" value="@if(!empty($getCompanyDetails)){{ $getCompanyDetails->portal_name }}@endif">
														          					<label for="portal_name">{{ Lang::get('settings.portal-name') }}</label>
														        				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-50x50 clear-fix">
																		<div class="form-layout-50">
																			<div class="field-group">
																				<div class="input-field select-field">
														          					<select name="language">
                                                                  						@foreach($language as $key=>$value)
                                                                  							<option value="{{ $key }}" @if($key==env('LANG'))selected @endif>{{ $value }}</option>
                                                                  						@endforeach
																    				</select>
																    				<label>Language</label>
														        				</div>
																			</div>
																		</div>
																		<div class="form-layout-50">
																			<div class="field-group">
																				<div class="input-field select-field">
														          					<select name="date_format">
                                                                  						<option value="dd/mm/yy" @if($date_format['meta_value']=="dd/mm/yy")selected @endif>DD/MM/YY</option>
                                                                  						<option value="mm/dd/yy" @if($date_format['meta_value']=="mm/dd/yy")selected @endif>MM/DD/YY</option>
																    				</select>
																    				<label>Date Format</label>
														        				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col s4">
																<input type="hidden" name="uploaded_image" value="">
															</div>
														</form>
														<div class="buttons center">
															@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>@endif
															@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<button class="waves-effect waves-light btn btn-green save" id="save-company-details" data-time="after_install">{{ Lang::get('common.save') }}</button>@endif
														</div>
													</div>
													<div class="col s5">
														<div id="crop-avatar">
														    <!-- Current avatar -->
														    <div class="avatar-view">
														      	<img id="result" src="{{ url('/') }}/uploads/company-logo/company-logo.png" alt="company logo">
														      	<div class="logo-hover-state">
														      		<span class="file-upload"></span>
														      		<span>{{ Lang::get('settings.click-to-upload') }}</span>
														      	</div>
														    </div>

														    <p class="img-size">Suggested Size: 216 x 118, Format: .jpg, .gif, .png</p>

														    <!-- Cropping modal -->
														    <div class="modal fade modal-fixed-footer" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="z-index: 1005">
														    	<button class="modal-close avatar-modal-close"></button>
														    	<div class="modal-wrapper">
															      	<div class="modal-dialog modal-lg">
															        	<div class="modal-content">
															          		<form class="avatar-form" action="{{ url('/') }}/crop" enctype="multipart/form-data" method="post">
															            		<div class="modal-header">
															              			<h3 id="avatar-modal-label">{{ Lang::get('settings.upload-and-crop-image') }}</h3>
															            		</div>
															            		<div class="modal-body">
															              			<div class="avatar-body">
																	                	<!-- Upload image and data -->
																	                	<div class="avatar-upload">
																		                	<input type="hidden" name="_token" value="{{ csrf_token()}}">
																		                	<input type="hidden" name="company_id" value="@if(!empty($getCompanyDetails)){{ $getCompanyDetails->id }}@endif">
																		                	<input type="hidden" name="purpose" value="company_logo">
																	                  		<input type="hidden" class="avatar-src" name="avatar_src">
																	                  		<input type="hidden" class="avatar-data" name="avatar_data">
																	                  		<label for="avatarInput">{{ Lang::get('settings.local-upload') }}</label>
																	                  		<input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
																	                	</div>
																		                <!-- Crop and preview -->
																		                <div class="showImage">
																	                    	<div class="avatar-wrapper"></div>
																	                    	<p>{{ Lang::get('settings.scroll-to-fit-the-image-wihtin-the-cropper-bouding-box') }}.</p>
																		                </div>
															                			
														                				<div class="avatar-btns">
															                  				<div class="right-align">
															                    				<button type="submit" class="btn btn-primary green avatar-save">{{ Lang::get('settings.upload-and-save') }}</button>
															                  				</div>
															                  			</div>
															              			</div>
															            		</div>
															          		</form>
															        	</div>
															      	</div>
														      	</div>
														    </div><!-- /.modal -->
														    <!-- Loading state -->
														    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="tabs-2" class="settings-tabs @if($page=='email') show-content @endif">
											<div class="tabs-wrapper">
												<div class="email-settings">
													<div class="row">
														<div class="col s6">
															<div class="row">
																<div class="col s12">
																	<h3>{{ Lang::get('settings.email-settings') }}</h3>
																</div>
															</div>
															<form>
																<div class="row">
																	<div class="col s12">
																		<div class="form-layout form-layout-50X30 clear-fix">
																			<div class="form-layout-50">
																				<div class="field-group">
																					<div class="input-field">
																			          	<input name="default_email" type="email" class="validate" value="@if($default_email!='') {{ $default_email }} @endif">
																			          	<label for="email">{{ Lang::get('common.email') }}</label>
																			        </div>
																				</div>
																			</div>
																			<div class="form-layout-30">
																				<input type="checkbox" class="filled-in" id="test5" @if($getCompanyDetails->marked_as_default=="yes") checked @endif />
																					<label for="test5">{{ Lang::get('settings.mark-as-default') }}</label>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col s12">
																		<div class="form-layout form-layout-50X20 clear-fix">
																			<div class="form-layout-50">
																				<div class="field-group company-extra-email">
																					@if(!empty($company_email_array))
																						@foreach($company_email_array as $key=>$value)
																							<div class="input-field">
																					          	<input type="email" name="email[]" class="validate" value="{{ $value }}">
																					          	<label for="email">{{ Lang::get('common.email') }}</label>
																					        </div>
																					    @endforeach
																					@endif
																			    </div>
																			</div>
																			<div class="form-layout-20">
																				<div class="field-group company-remove-extra-email">
																					@if(!empty($company_email_array))
																						@foreach($company_email_array as $key=>$value)
																							<div class="input-field">
																					          	<a href="javascript:void(0);" class="remove-input remove-company-extra-email"><span></span> </a>
																					        </div>
																					    @endforeach
																				    @endif
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col s12">
																		<div class="form-layout form-layout-50X20 clear-fix">
																			<div class="form-layout-50">
																				<div class="field-group">
																					<div class="input-field">
																			          	<input type="email" name="email[]" class="validate">
																			          	<label for="email">{{ Lang::get('common.email') }}</label>
																			        </div>
																				</div>
																			</div>
																			<div class="form-layout-20">
																				<div class="input-field">
																					<a href="javascript:void(0);" id="add-another-company-email" class="add-input open-address"><span></span> </a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</form>
															<div class="buttons center">
																@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>@endif
																@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<button class="waves-effect waves-light btn btn-green save" id="save-comopany-email">{{ Lang::get('common.save') }}</button>@endif
															</div>
														</div>
														<div class="col s6 left-border">
															<div class="row">
																<div class="col s12">
																	<h3>{{ Lang::get('settings.smtp-settings') }}</h3>
																</div>
															</div>
															<form class="clear-fix" id="company-form" name="company-form" method="POST" action="save-company-details" enctype="multipart/form-data">
																<div class="row">
																	<div class="col s12">
																		<div class="form-layout form-layout-50x50 clear-fix">
																			<div class="form-layout-50">
																				<div class="field-group">
																					<div class="input-field">
																						<input type="text" name="mail_host" class="validate" value="{{ $smtp_details['smtp_host'] }}">
																						<span class="error-mark"></span>
													          							<span class="check-mark"></span>
															          					<label for="mail_host">{{ Lang::get('settings.smtp-mail-host') }}</label>
																					</div>	
																				</div>
																			</div>
																			<div class="form-layout-50">
																				<div class="field-group">
																					<div class="input-field">
															          					<input type="text" name="mail_port" class="validate" value="{{ $smtp_details['smtp_port'] }}">
															          					<span class="error-mark"></span>
													          							<span class="check-mark"></span>
															          					<label for="mail_port">{{ Lang::get('settings.smtp-mail-port') }}</label>
															        				</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col s12">
																		<div class="form-layout form-layout-50x50 clear-fix">
																			<div class="form-layout-50">
																				<div class="field-group">
																					<div class="input-field">
															          					 <input type="text" name="mail_username" class="validate" value="{{ $smtp_details['smtp_username'] }}">
															          					 <span class="error-mark"></span>
													          							 <span class="check-mark"></span>
															          					<label for="mail_username">{{ Lang::get('settings.smtp-mail-username') }}</label>
															        				</div>
																				</div>
																			</div>
																			<div class="form-layout-50">
																				<div class="field-group">
																					<div class="input-field">
															          					<input type="text" name="mail_password" class="validate" value="{{ $smtp_details['smtp_password'] }}">
															          					<span class="error-mark"></span>
													          							<span class="check-mark"></span>
															          					<label for="mail_password">{{ Lang::get('settings.smtp-mail-password') }}</label>
															        				</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</form>
															<div class="row">
																<div class="col s12">
																	<div class="buttons right-align">
																		@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>@endif
																		@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<button class="waves-effect waves-light btn btn-green save" id="save-smtp-details">{{ Lang::get('common.save') }}</button>@endif
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col s12">
																	<div class="form-layout">
																		<div class="form-layout-60">
																			<div class="field-group send-mail">
																				<div class="input-field">
																					<input type="text" name="test_email" class="validate" value="">
														          					<label for="mail_host">{{ Lang::get('common.email-address') }}</label>
																				</div>	
																			</div>
																		</div>
																		<div class="form-layout-40">
																			<a href="javascript:void(0);" class="waves-effect waves-light btn btn-green send-test-mail" id="send-test-mail">{{ Lang::get('settings.send-test-mail') }}</a>
																			<!-- <button class="waves-effect waves-light btn btn-green" id="send-test-mail">Send</button> -->
																			<span class="test-mail"></span>
																			<span class="error-log"></span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="tabs-3" class="settings-tabs @if($page=='user_roles') show-content @endif">
											<div class="tabs-wrapper">
												<div class="contacts-grid user-grid">
													<div class="row">
														<div class="col s5">
															<h3>{{ Lang::get('settings.users') }}<span class="total-contact">(@php echo sizeof($users) @endphp)</span><a href="#modal4"> </a></h3>
														</div>
													</div>
													<div class="vetical-tab-nav border clear-fix">
														<div class="vertical-nav-wrap">
															<div class="user-search ui-widget">
																<button class="search"></button>
																<input id="tags" type="text" name="search_user" placeholder="Search">
															</div>
															@if(!empty($users))
															<ul>
															@foreach($users as $key=>$value)
																@if($key==0)
																<li class="active">
																@else
																<li>
																@endif
																	<a href="javascript:void(0)" data-user-id="{{ $value['id'] }}" class="user-edit">
																		<div class="profile-wrapper">
																    		<span class="profile-image">
																    			@if($user_profile_image_array[$key]['image']!="")
																    				<img src="{{ url('/') }}/uploads/profile-image/{{ $user_profile_image_array[$key]['image'] }}" alt="avatar">
																    			@else
																    				<img src="{{ url('/') }}/assets/img/images.jpg" alt="avatar">
																    			@endif
																    		</span>
																    		<div class="profile-name-designation">
																    			<span class="name">{{ $value['first_name'] }} {{ $value['last_name'] }}</span>
																    			<span class="designation">{{ $roles[$value['role']] }}</span>
																    			@if($key==0)<span class="label">{{ Lang::get('settings.super-admin') }}</span> @endif
																    		</div>
																    	</div>
																	</a>
																</li>
															@endforeach
															@endif
															@if(!empty($invitation_array))
															@foreach($invitation_array as $key=>$value)
																@if($key==0)
																<li>
																@else
																<li>
																@endif
																	<a href="javascript:void(0)" data-user-id="{{ $value['id'] }}" class="user-edit invited">
																		<div class="profile-wrapper">
																    		<span class="profile-image">
															    				<img src="{{ url('/') }}/assets/img/images.jpg" alt="avatar">
																    		</span>
																    		<div class="profile-name-designation">
																    			<span class="name">{{ $value['first_name'] }} {{ $value['last_name'] }}</span>
																    			<span class="designation">{{ Lang::get('settings.invited') }}</span>
																    			<span class="label">{{ Lang::get('settings.invited') }}</span>
																    		</div>
																    	</div>
																	</a>
																</li>
															@endforeach
															@endif
															</ul>
														</div>
														<div class="vertical-tabs-content">
															@if(!empty($users))
															@foreach($users as $key=>$value)
															@if($key==0)
															<div class="ver-tabs-panel">
																<div class="profile-wrapper">
														    		<span class="profile-image">
														    			@if($user_profile_image_array[$key]['image']!="")
														    				<img src="{{ url('/') }}/uploads/profile-image/{{ $user_profile_image_array[$key]['image'] }}" alt="avatar">
														    			@else
														    				<img src="{{ url('/') }}/assets/img/images.jpg" alt="avatar">
														    			@endif
														    		</span>
														    		<div class="profile-name-designation">
														    			<span class="name">{{ $value['first_name'] }} {{ $value['last_name'] }}</span>
														    			<span class="designation">{{ $roles[$value['role']] }}</span>
														    		</div>
														    		<!-- Dropdown Trigger -->
																	<a class="dropdown-button btn green select-button" href="#" data-activates="dropdown1">{{ Lang::get('settings.active') }}</a>
																	<!-- Dropdown Structure -->
																	<ul id="dropdown1" class="dropdown-content dropdown">
																		<li><a href="javascript:void(0)">{{ Lang::get('settings.active') }}</a></li>
																		<li><a href="javascript:void(0)">{{ Lang::get('settings.inactive') }}</a></li>
																	</ul>
														    	</div>
														    	<form>
																	<div class="row">
																		<div class="col s12">
																			<div class="form-layout form-layout-50x50 clear-fix">
																				<div class="form-layout-50">
																					<div class="field-group">
																						<div class="input-field">
																							<input type="hidden" name="_token" value="">
																				          	<input id="first-name" name="first-name" type="text" class="validate" value="{{ $value['first_name'] }}">
																				          	<span class="error-mark"></span>
																				          	<span class="check-mark"></span>
																				          	<label for="first-name" class="">{{ Lang::get('common.first') }} {{ Lang::get('common.name') }}</label>
																				        </div>
																					</div>
																				</div>
																				<div class="form-layout-50">
																					<div class="field-group">
																						<div class="input-field">
																				          	<input id="last-name" name="last-name" type="text" class="validate" value="{{ $value['last_name'] }}">
																				          	<span class="error-mark"></span>
																				          	<span class="check-mark"></span>
																				          	<label for="last-name" class="">{{ Lang::get('common.last') }} {{ Lang::get('common.name') }}</label>
																				        </div>
																					</div>
																				</div>
																			</div> <!-- END FORM LAYOUT -->
																		</div> <!-- END COL -->
																	</div> <!-- END ROW -->
																	<div class="row">
																		<div class="col s12">
																			<div class="form-layout form-layout-50x50 clear-fix">
																				<div class="form-layout-50">
																					<div class="field-group">
																						<div class="input-field">
																				          	<input id="user-email" name="user-email" type="text" class="validate" value="{{ $value['email'] }}">
																				          	<span class="error-mark"></span>
																				          	<span class="check-mark"></span>
																				          	<label for="user-email" class="">{{ Lang::get('common.email-address') }}</label>
																				        </div>
																					</div>
																				</div>
																				<div class="form-layout-50">
																					<div class="field-group">
																						<div class="input-field">
																				          	<input id="user-phone" name="user-phone" type="text" class="validate" value="{{ $value['phone_no'] }}">
																				          	<span class="error-mark"></span>
																				          	<span class="check-mark"></span>
																				          	<label for="user-phone" class="">{{ Lang::get('common.phone') }} {{ Lang::get('common.no') }}</label>
																				        </div>
																					</div>
																				</div>
																			</div> <!-- END FORM LAYOUT -->
																		</div> <!-- END COL -->
																	</div> <!-- END ROW -->
																	<div class="row" style="display:none">
																		<div class="col s12">
																			<div class="form-layout form-layout-50x50 clear-fix">
																				<div class="form-layout-100 user-role">
																					<div class="field-group">
																						<div class="input-field select-field">
																							<select name="user-role">
																								<option value="priority" disabled selected>{{ Lang::get('settings.role') }}</option>
																								@foreach($roles as $role_key=>$role_value)
																									<option value="{{ $role_value }}" data-value="{{ $role_key }}" @if($value['role']==$role_key)selected @endif>{{ $role_value }}</option>
																								@endforeach
																							</select>
																						</div>
																					</div>
																				</div>
																			</div> <!-- END FORM LAYOUT -->
																		</div> <!-- END COL -->
																	</div> <!-- END ROW -->
																</form> <!-- END FORM -->
																<div class="permissions">
																	<h4>{{ Lang::get('settings.Permissions') }}</h4>
																	<div class="permissions-lists">
																		<ul class="permissions-list">
																			<li><h4>{{ Lang::get('contact.Contact') }}</h4></li>
																			<li>
																				<input type="checkbox" class="filled-in" name="contact-add-permission" id="filled-in-box" @if($users_permission[$value['id']]['contact']['add']=="on") checked @endif />
      																			<label for="filled-in-box">{{ Lang::get('common.add') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="contact-edit-permission" id="filled-in-box-1" @if($users_permission[$value['id']]['contact']['edit']=="on") checked @endif />
      																			<label for="filled-in-box-1">{{ Lang::get('common.edit') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="contact-view-permission" id="filled-in-box-2" @if($users_permission[$value['id']]['contact']['view']=="on") checked @endif />
      																			<label for="filled-in-box-2">{{ Lang::get('common.view') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="contact-all-permission" id="filled-in-box-3" @if($users_permission[$value['id']]['contact']['all']=="on") checked @endif />
      																			<label for="filled-in-box-3">{{ Lang::get('settings.admin') }}</label>
      																		</li>
																		</ul>
																		<ul class="permissions-list">
																			<li><h4>{{ Lang::get('deal.Deal') }}</h4></li>
																			<li>
																				<input type="checkbox" class="filled-in" name="deal-add-permission" id="filled-in-box-4" @if($users_permission[$value['id']]['deal']['add']=="on") checked @endif />
      																			<label for="filled-in-box-4">{{ Lang::get('common.add') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="deal-edit-permission" id="filled-in-box-5" @if($users_permission[$value['id']]['deal']['edit']=="on") checked @endif />
      																			<label for="filled-in-box-5">{{ Lang::get('common.edit') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="deal-view-permission" id="filled-in-box-6" @if($users_permission[$value['id']]['deal']['view']=="on") checked @endif />
      																			<label for="filled-in-box-6">{{ Lang::get('common.edit') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="deal-all-permission" id="filled-in-box-7" @if($users_permission[$value['id']]['deal']['all']=="on") checked @endif />
      																			<label for="filled-in-box-7">{{ Lang::get('settings.admin') }}</label>
      																		</li>
																		</ul>
																		<ul class="permissions-list">
																			<li><h4>{{ Lang::get('settings.settings') }}</h4></li>
																			<li>
																				<input type="checkbox" class="filled-in" name="setting-add-permission" id="filled-in-box-8" @if($users_permission[$value['id']]['setting']['add']=="on") checked @endif />
      																			<label for="filled-in-box-8">{{ Lang::get('common.update') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="setting-view-permission" id="filled-in-box-9" @if($users_permission[$value['id']]['setting']['edit']=="on") checked @endif />
      																			<label for="filled-in-box-9">{{ Lang::get('common.view') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="setting-all-permission" id="filled-in-box-10" @if($users_permission[$value['id']]['setting']['view']=="on") checked @endif />
      																			<label for="filled-in-box-10">{{ Lang::get('settings.admin') }}</label>
      																		</li>
																		</ul>
																		<ul class="permissions-list">
																			<li><h4>{{ Lang::get('invoice.Invoice') }}</h4></li>
																			<li>
																				<input type="checkbox" class="filled-in" name="invoice-add-permission" id="filled-in-box-11" @if($users_permission[$value['id']]['invoice']['add']=="on") checked @endif />
      																			<label for="filled-in-box-11">{{ Lang::get('common.add') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="invoice-edit-permission" id="filled-in-box-12" @if($users_permission[$value['id']]['invoice']['edit']=="on") checked @endif />
      																			<label for="filled-in-box-12">{{ Lang::get('common.edit') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="invoice-view-permission" id="filled-in-box-13" @if($users_permission[$value['id']]['invoice']['view']=="on") checked @endif />
      																			<label for="filled-in-box-13">{{ Lang::get('common.view') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="invoice-all-permission" id="filled-in-box-14" @if($users_permission[$value['id']]['invoice']['all']=="on") checked @endif />
      																			<label for="filled-in-box-14">{{ Lang::get('settings.admin') }}</label>
      																		</li>
																		</ul>
																		<ul class="permissions-list">
																			<li><h4>Estimate</h4></li>
																			<li>
																				<input type="checkbox" class="filled-in" name="estimate-add-permission" id="filled-in-box-15" @if($users_permission[$value['id']]['estimate']['add']=="on") checked @endif />
      																			<label for="filled-in-box-15">{{ Lang::get('common.add') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="estimate-edit-permission" id="filled-in-box-16" @if($users_permission[$value['id']]['estimate']['edit']=="on") checked @endif />
      																			<label for="filled-in-box-16">{{ Lang::get('common.edit') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="estimate-view-permission" id="filled-in-box-17" @if($users_permission[$value['id']]['estimate']['view']=="on") checked @endif />
      																			<label for="filled-in-box-17">{{ Lang::get('common.view') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="estimate-all-permission" id="filled-in-box-18" @if($users_permission[$value['id']]['estimate']['all']=="on") checked @endif />
      																			<label for="filled-in-box-18">{{ Lang::get('settings.admin') }}</label>
      																		</li>
																		</ul>
																		<ul class="permissions-list">
																			<li><h4>Task</h4></li>
																			<li>
																				<input type="checkbox" class="filled-in" name="task-add-permission" id="filled-in-box-19" @if($users_permission[$value['id']]['task']['add']=="on") checked @endif />
      																			<label for="filled-in-box-19">{{ Lang::get('common.add') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="task-edit-permission" id="filled-in-box-20" @if($users_permission[$value['id']]['task']['edit']=="on") checked @endif />
      																			<label for="filled-in-box-20">{{ Lang::get('common.edit') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="task-view-permission" id="filled-in-box-21" @if($users_permission[$value['id']]['task']['view']=="on") checked @endif />
      																			<label for="filled-in-box-21">{{ Lang::get('common.view') }}</label>
      																		</li>
      																		<li>
																				<input type="checkbox" class="filled-in" name="task-all-permission" id="filled-in-box-22" @if($users_permission[$value['id']]['task']['all']=="on") checked @endif />
      																			<label for="filled-in-box-22">{{ Lang::get('settings.admin') }}</label>
      																		</li>
																		</ul>
																	</div>
																</div>
																<div class="buttons center">
																	@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<a href="{{ url('/') }}/settings?page=user_roles" class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</a>@endif
																	@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<button class="waves-effect waves-light btn btn-green edit-user save">{{ Lang::get('common.save') }}</button>@endif
																</div> <!-- END BUTTONS -->
															</div>
															@endif
															@endforeach
															@endif
														</div>
													</div>
													<input type="hidden" name="user_id" value="@if(!empty($users)){{ $users[0]['id'] }}@endif">
												</div>
											</div>
										</div>
										<div id="tabs-4" class="">
											<div class="tabs-wrapper">
												<form>
													<div class="row">
														<div class="col s12">
															<div class="form-layout form-layout-50x50 clear-fix">
																<div class="form-layout-50">
																	<div class="field-group">
																		<div class="input-field">
																          	<input name="settings_default_email" type="email" class="validate" value="@if(!empty($user_company)) {{ $user_company['default_email'] }} @endif">
																          	<label for="email" class="">{{ Lang::get('settings.default-email-to-send-estimate') }}</label>
																        </div>
																	</div>
																</div>
																<div class="form-layout-50">
																	<div class="field-group">
																		<div class="input-field">
																          	<input name="sender_name" type="text" class="validate" value="@if(!empty($user_company)) {{ $user_company['sender_name'] }} @endif">
																          	<label for="sender-name" class="">{{ Lang::get('settings.sender-name') }}</label>
																        </div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col s12">
															<div class="form-layout form-layout-30X30X30X10 clear-fix">
																<div class="form-layout-50">
																	<div class="field-group">
																		<div class="input-field">
																          	<input name="estimate_number_prefix" type="text" class="validate" value="{{ $startEstimateNumber }}">
																          	<label for="amount" class="">{{ Lang::get('settings.estimate-number-prefix') }}</label>
																        </div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="reminder-estimate">
													@if(!empty($reminder))
														@foreach($reminder as $key=>$value)
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-80X10 clear-fix">
																		<div class="form-layout-80">
																			<div class="field-group">
																				<div class="input-field select-field">
																		          	<select name="reminder[]">
																				    	<option value="" disabled selected>@if($key==0)1st @elseif($key==1)2nd @elseif($key==2)3rd @endif Reminder Period</option>
																						<option value="15" @if($value=="15") selected @endif>15 {{ Lang::get('task.days') }}</option>
																						<option value="30" @if($value=="30") selected @endif>30 {{ Lang::get('task.days') }}</option>
																						<option value="45" @if($value=="45") selected @endif>45 {{ Lang::get('task.days') }}</option>
																						<option value="60" @if($value=="60") selected @endif>60 {{ Lang::get('task.days') }}</option>
																					</select>
																					<label>Reminder Period</label>
																		        </div>
																			</div>
																		</div>
																		<div class="form-layout-10">
																			<div class="field-group">
																				<div class="input-field">
																					<a href="javascript:void(0);" class="@if($key>0)remove-extra-reminder-estimate remove-input @else add-input add-extra-reminder-estimate @endif"> <span></span></a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														@endforeach
													@elseif((empty($reminder)) || (!empty($reminder) && sizeof($reminder)<3))
														<div class="row">
															<div class="col s12">
																<div class="form-layout form-layout-80X10 clear-fix">
																	<div class="form-layout-80">
																		<div class="field-group">
																			<div class="input-field select-field">
																	          	<select name="reminder[]">
																			    	<option value="" disabled selected>@if(!empty($reminder)) @if(sizeof($reminder)=="1")2nd Reminder Period @elseif(sizeof($reminder)=="2")3rd Reminder Period @else {{ sizeof($reminder)+1 }}th Reminder Period @endif @else 1st Reminder Period @endif</option>
																					<option value="15">15 {{ Lang::get('task.days') }}</option>
																					<option value="30">30 {{ Lang::get('task.days') }}</option>
																					<option value="45">45 {{ Lang::get('task.days') }}</option>
																					<option value="60">60 {{ Lang::get('task.days') }}</option>
																				</select>
																				<label>Reminder Period</label>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-10">
																		<div class="field-group">
																			<div class="input-field">
																				<a href="javascript:void(0);" class="add-input add-extra-reminder-estimate"> <span></span></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													@endif
													</div>
													<div class="tax-estimate">
														@if(!empty($tax))
															@foreach($tax as $key=>$value)
																<div class="row">
																	<div class="col s12">
																		<div class="form-layout form-layout-30X30X30X10 clear-fix">
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																						<input type="hidden" name="tax_id[]" value="{{ $value['id'] }}">
																			          	<input name="tax_name[]" type="text" class="validate" value="{{ $value['tax_name'] }}">
																			          	<label for="tax-name" class="">{{ Lang::get('settings.tax-name') }}</label>
																			        </div>
																				</div>
																			</div>
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																			          	<select name="tax_type[]">
																							<option value="percentile" @if($value['tax_type']=="percentile") selected @endif>{{ Lang::get('estimate.percentile') }}</option>
																							<option value="fixed" @if($value['tax_type']=="fixed") selected @endif>{{ Lang::get('settings.fixed') }}</option>
																						</select>
																			        </div>
																				</div>
																			</div>
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																			          	<input name="tax_amount[]" type="text" class="validate" value="{{ $value['tax_amount'] }}">
																			          	<label for="amount" class="">{{ Lang::get('settings.amount') }}</label>
																			        </div>
																				</div>
																			</div>
																			<!-- <div class="form-layout-10">
																				<div class="field-group">
																					<div class="input-field">
																						<a href="javascript:void(0);" class="add-input"> <span></span></a>
																					</div>
																				</div>
																			</div> -->
																		</div>
																	</div>
																</div>
															@endforeach
														@endif
														<div class="row">
															<div class="col s12">
																<div class="form-layout form-layout-30X30X30X10 clear-fix">
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																				<input type="hidden" name="tax_id[]" value="0">
																	          	<input name="tax_name[]" type="text" class="validate">
																	          	<label for="tax-name" class="">{{ Lang::get('settings.tax-name') }}</label>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																	          	<select name="tax_type[]">
																					<option value="percentile">{{ Lang::get('estimate.percentile') }}</option>
																					<option value="fixed">{{ Lang::get('settings.fixed') }}</option>
																				</select>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																	          	<input name="tax_amount[]" type="text" class="validate">
																	          	<label for="amount" class="">{{ Lang::get('settings.amount') }}</label>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-10">
																		<div class="field-group">
																			<div class="input-field">
																				<a href="javascript:void(0);" class="add-input add-extra-tax-estimate"> <span></span></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="discount-estimate">
														@if(!empty($discount))
															@foreach($discount as $key=>$value)
																<div class="row">
																	<div class="col s12">
																		<div class="form-layout form-layout-30X30X30X10 clear-fix">
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																						<input type="hidden" name="discount_id[]" value="{{ $value['id'] }}">
																			          	<input name="discount_name[]" type="text" class="validate" value="{{ $value['discount_name'] }}">
																			          	<label for="discount" class="">{{ Lang::get('settings.discount') }}</label>
																			        </div>
																				</div>
																			</div>
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																			          	<select name="discount_type[]">
																							<option value="percentile" @if($value['discount_type']=="percentile") selected @endif>{{ Lang::get('estimate.percentile') }}</option>
																							<option value="fixed" @if($value['discount_type']=="fixed") selected @endif>{{ Lang::get('settings.fixed') }}</option>
																						</select>
																			        </div>
																				</div>
																			</div>
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																			          	<input name="discount_amount[]" type="text" class="validate" value="{{ $value['discount_amount'] }}">
																			          	<label for="amount" class="">{{ Lang::get('settings.amount') }}</label>
																			        </div>
																				</div>
																			</div>
																			<!-- <div class="form-layout-10">
																				<div class="field-group">
																					<div class="input-field">
																						<a href="javascript:void(0);" class="add-input"> <span></span></a>
																					</div>
																				</div>
																			</div> -->
																		</div>
																	</div>
																</div>
															@endforeach
														@endif
														<div class="row">
															<div class="col s12">
																<div class="form-layout form-layout-30X30X30X10 clear-fix">
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																				<input type="hidden" name="discount_id[]" value="0">
																	          	<input name="discount_name[]" type="text" class="validate">
																	          	<label for="discount" class="">{{ Lang::get('settings.discount') }}</label>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																	          	<select name="discount_type[]">
																					<option value="percentile">{{ Lang::get('estimate.percentile') }}</option>
																					<option value="fixed">{{ Lang::get('settings.fixed') }}</option>
																				</select>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																	          	<input name="discount_amount[]" type="text" class="validate">
																	          	<label for="amount" class="">{{ Lang::get('settings.amount') }}</label>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-10">
																		<div class="field-group">
																			<div class="input-field">
																				<a href="javascript:void(0);" class="add-input add-extra-discount-estimate"> <span></span></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col s12">
															<div class="form-layout form-layout-100 clear-fix">
																<div class="form-layout-100">
																	<div class="field-group">
																		<div class="input-field">
																          	<textarea id="terms-estimate" onkeyup="textAreaAdjust(this)" style="overflow:hidden" class="materialize-textarea" value="{{ $est_terms_and_conditions }}">{{ $est_terms_and_conditions }}</textarea>
																          	<label for="terms-estimate">{{ Lang::get('settings.terms-and-condition') }}</label>
																        </div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</form>
												<div class="row">
													<div class="col s12">
														<div class="buttons center">
															@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>@endif
															@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<button class="waves-effect waves-light btn btn-green save-settings-proposal save" data-purpose="estimate">{{ Lang::get('common.save') }}</button>@endif
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="tabs-5" class="settings-tabs @if($page=='proposal') show-content @endif">
											<div class="tabs-wrapper">
												<form>
													<div class="row">
														<div class="col s12">
															<div class="form-layout form-layout-50x50 clear-fix">
																<div class="form-layout-50">
																	<div class="field-group">
																		<div class="input-field">
																          	<input name="settings_default_email" type="email" class="validate" value="@if(!empty($user_company)) {{ $user_company['default_email'] }} @endif">
																          	<label for="email" class="">{{ Lang::get('settings.default-email-to-send-proposal') }}</label>
																        </div>
																	</div>
																</div>
																<div class="form-layout-50">
																	<div class="field-group">
																		<div class="input-field">
																          	<input name="sender_name" type="text" class="validate" value="@if(!empty($user_company)) {{ $user_company['sender_name'] }} @endif">
																          	<label for="sender-name" class="">{{ Lang::get('settings.sender-name') }}</label>
																        </div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col s12">
															<div class="form-layout form-layout-30X30X30X10 clear-fix">
																<div class="form-layout-50">
																	<div class="field-group">
																		<div class="input-field">
																          	<input name="invoice_number_prefix" type="text" class="validate" value="{{ $startInvoiceNumber }}">
																          	<label for="amount" class="">{{ Lang::get('settings.invoice-number-prefix') }}</label>
																        </div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="reminder-proposal">
													@if(!empty($reminder))
														@foreach($reminder as $key=>$value)
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-80X10 clear-fix">
																		<div class="form-layout-80">
																			<div class="field-group">
																				<div class="input-field select-field">
																		          	<select name="reminder[]">
																				    	<option value="" disabled selected>{{ Lang::get('settings.first-reminder-period') }}</option>
																						<option value="15" @if($value=="15") selected @endif>15 {{ Lang::get('task.days') }}</option>
																						<option value="30" @if($value=="30") selected @endif>30 {{ Lang::get('task.days') }}</option>
																						<option value="45" @if($value=="45") selected @endif>45 {{ Lang::get('task.days') }}</option>
																						<option value="60" @if($value=="60") selected @endif>60 {{ Lang::get('task.days') }}</option>
																					</select>
																					<label>Reminder Period</label>
																		        </div>
																			</div>
																		</div>
																		<div class="form-layout-10">
																			<div class="field-group">
																				<div class="input-field">
																					<a href="javascript:void(0);" class="@if($key>0)remove-extra-reminder-proposal remove-input @else add-input add-extra-reminder-proposal @endif"> <span></span></a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														@endforeach
													@elseif((empty($reminder)) || (!empty($reminder) && sizeof($reminder)<3))
														<div class="row">
															<div class="col s12">
																<div class="form-layout form-layout-80X10 clear-fix">
																	<div class="form-layout-80">
																		<div class="field-group">
																			<div class="input-field select-field">
																	          	<select name="reminder[]">
																			    	<option value="" disabled selected>@if(!empty($reminder)) @if(sizeof($reminder)=="1")2nd Reminder Period @elseif(sizeof($reminder)=="2")3rd Reminder Period @else{{ sizeof($reminder)+1 }}th Reminder Period @endif @else 1st Reminder Period @endif</option>
																					<option value="15">15 {{ Lang::get('task.days') }}</option>
																					<option value="30">30 {{ Lang::get('task.days') }}</option>
																					<option value="45">45 {{ Lang::get('task.days') }}</option>
																					<option value="60">60 {{ Lang::get('task.days') }}</option>
																				</select>
																				<label>Reminder Period</label>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-10">
																		<div class="field-group">
																			<div class="input-field">
																				<a href="javascript:void(0);" class="add-input add-extra-reminder-proposal"> <span></span></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													@endif
													</div>
													<div class="tax-proposal">
														@if(!empty($tax))
															@foreach($tax as $key=>$value)
																<div class="row">
																	<div class="col s12">
																		<div class="form-layout form-layout-30X30X30X10 clear-fix">
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																						<input type="hidden" name="tax_id[]" value="{{ $value['id'] }}">
																			          	<input name="tax_name[]" type="text" class="validate" value="{{ $value['tax_name'] }}">
																			          	<label for="tax-name" class="">{{ Lang::get('settings.tax-name') }}</label>
																			        </div>
																				</div>
																			</div>
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																			          	<select name="tax_type[]">
																							<option value="percentile" @if($value['tax_type']=="percentile") selected @endif>{{ Lang::get('estimate.percentile') }}</option>
																							<option value="fixed" @if($value['tax_type']=="fixed") selected @endif>{{ Lang::get('settings.fixed') }}</option>
																						</select>
																			        </div>
																				</div>
																			</div>
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																			          	<input name="tax_amount[]" type="text" class="validate" value="{{ $value['tax_amount'] }}">
																			          	<label for="amount" class="">{{ Lang::get('settings.amount') }}</label>
																			        </div>
																				</div>
																			</div>
																			<!-- <div class="form-layout-10">
																				<div class="field-group">
																					<div class="input-field">
																						<a href="javascript:void(0);" class="add-input"> <span></span></a>
																					</div>
																				</div>
																			</div> -->
																		</div>
																	</div>
																</div>
															@endforeach
														@endif
														<div class="row">
															<div class="col s12">
																<div class="form-layout form-layout-30X30X30X10 clear-fix">
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																				<input type="hidden" name="tax_id[]" value="0">
																	          	<input name="tax_name[]" type="text" class="validate">
																	          	<label for="tax-name" class="">{{ Lang::get('settings.tax-name') }}</label>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																	          	<select name="tax_type[]">
																					<option value="percentile">{{ Lang::get('estimate.percentile') }}</option>
																					<option value="fixed">{{ Lang::get('settings.fixed') }}</option>
																				</select>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																	          	<input name="tax_amount[]" type="text" class="validate">
																	          	<label for="amount" class="">{{ Lang::get('settings.amount') }}</label>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-10">
																		<div class="field-group">
																			<div class="input-field">
																				<a href="javascript:void(0);" class="add-input add-extra-tax-proposal"> <span></span></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="discount-proposal">
														@if(!empty($discount))
															@foreach($discount as $key=>$value)
																<div class="row">
																	<div class="col s12">
																		<div class="form-layout form-layout-30X30X30X10 clear-fix">
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																						<input type="hidden" name="discount_id[]" value="{{ $value['id'] }}">
																			          	<input name="discount_name[]" type="text" class="validate" value="{{ $value['discount_name'] }}">
																			          	<label for="discount" class="">{{ Lang::get('settings.discount') }}</label>
																			        </div>
																				</div>
																			</div>
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																			          	<select name="discount_type[]">
																							<option value="percentile" @if($value['discount_type']=="percentile") selected @endif>{{ Lang::get('estimate.percentile') }}</option>
																							<option value="fixed" @if($value['discount_type']=="fixed") selected @endif>{{ Lang::get('settings.fixed') }}</option>
																						</select>
																			        </div>
																				</div>
																			</div>
																			<div class="form-layout-30">
																				<div class="field-group">
																					<div class="input-field">
																			          	<input name="discount_amount[]" type="text" class="validate" value="{{ $value['discount_amount'] }}">
																			          	<label for="amount" class="">{{ Lang::get('settings.amount') }}</label>
																			        </div>
																				</div>
																			</div>
																			<div class="form-layout-10">
																				<div class="field-group">
																					<div class="input-field">
																						<a href="javascript:void(0);" class="add-input"> <span></span></a>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															@endforeach
														@endif
														<div class="row">
															<div class="col s12">
																<div class="form-layout form-layout-30X30X30X10 clear-fix">
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																				<input type="hidden" name="discount_id[]" value="0">
																	          	<input name="discount_name[]" type="text" class="validate">
																	          	<label for="discount" class="">{{ Lang::get('settings.discount') }}</label>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																	          	<select name="discount_type[]">
																					<option value="percentile">{{ Lang::get('estimate.percentile') }}</option>
																					<option value="fixed">{{ Lang::get('settings.fixed') }}</option>
																				</select>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-30">
																		<div class="field-group">
																			<div class="input-field">
																	          	<input name="discount_amount[]" type="text" class="validate">
																	          	<label for="amount" class="">{{ Lang::get('settings.amount') }}</label>
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-10">
																		<div class="field-group">
																			<div class="input-field">
																				<a href="javascript:void(0);" class="add-input add-extra-discount-proposal"> <span></span></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col s12">
															<div class="form-layout form-layout-50X10 clear-fix">
																<div class="form-layout-50">
																	<div class="field-group">
																		<div class="input-field">
																          	<select name="default_payment_term">
																				<option value="net-15" @if(!empty($default_payment_term) && $default_payment_term['meta_value']=="net-15") selected @endif>Net 15</option>
																				<option value="net-30" @if(!empty($default_payment_term) && $default_payment_term['meta_value']=="net-30") selected @endif>Net 30</option>
																				<option value="net-45" @if(!empty($default_payment_term) && $default_payment_term['meta_value']=="net-45") selected @endif>Net 45</option>
																				<option value="net-60" @if(!empty($default_payment_term) && $default_payment_term['meta_value']=="net-60") selected @endif>Net 60</option>
																				<option value="due-on-receive" @if(!empty($default_payment_term) && $default_payment_term['meta_value']=="due-on-receive") selected @endif>Due on receive</option>
																				<option value="due-at-end-of-month" @if(!empty($default_payment_term) && $default_payment_term['meta_value']=="due-at-end-of-month") selected @endif>Due at the end of the month</option>
																			</select>
																        </div>
																	</div>
																</div>
																<!-- <div class="form-layout-10">
																	<div class="field-group">
																		<div class="input-field">
																			<a href="javascript:void(0);" class="add-input"> <span></span></a>
																		</div>
																	</div>
																</div> -->
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col s12">
															<div class="form-layout form-layout-100 clear-fix">
																<div class="form-layout-100">
																	<div class="field-group">
																		<div class="input-field">
																          	<textarea id="terms-proposal" onkeyup="textAreaAdjust(this)" style="overflow:hidden" class="materialize-textarea" value="{{ $terms_and_conditions }}">{{ $terms_and_conditions }}</textarea>
																          	<label for="terms-proposal">{{ Lang::get('settings.terms-and-condition') }}</label>
																        </div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</form>
												<div class="row">
													<div class="col s12">
														<div class="buttons center">
															@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>@endif
															@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")<button class="waves-effect waves-light btn btn-green save-settings-proposal save" data-purpose="proposal">{{ Lang::get('common.save') }}</button>@endif
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="tabs-7" class="settings-tabs @if($page=='payment_gateway') show-content @endif">
											<div class="tabs-wrapper">
												<div class="row">
													<div class="col s12">
														<div class="select-payment-method">
															<ul>
																<li class="selected" data-payment-method="paypal">
															      	<input class="with-gap" name="group1" type="radio" id="test1" value="paypal" @if($defaultPaymentMethod=="paypal") checked @endif  />
															      	<label for="test1">Paypal</label>
																</li>
																<li class="" data-payment-method="stripe">
															      	<input class="with-gap" name="group1" type="radio" id="test2" value="stripe" @if($defaultPaymentMethod=="stripe") checked @endif  />
															      	<label for="test2">Stripe</label>
																</li>
															</ul>
														</div>
														<div class="select-payment paypal" style="display:@if($defaultPaymentMethod=='paypal') block @else none @endif">
															<form class="select-payment-fields ">
																<div class="row">
																	<div class="col s12">
																		<div class="form-layout form-layout-50x50 clear-fix">
																			<div class="form-layout-50">
																				<div class="field-group">
																					<div class="input-field">
																						<input type="text" name="client_id" class="validate" value="@if(!empty($paypal_credentials)) {{ $paypal_credentials['client_id'] }} @endif">
															          					<label for="client_id">{{ Lang::get('settings.client-id') }}</label>
																					</div>	
																				</div>
																			</div>
																			<div class="form-layout-50">
																				<div class="field-group">
																					<div class="input-field">
															          					<input type="text" name="secret_key" class="validate" value="@if(!empty($paypal_credentials)) {{ $paypal_credentials['secret_key'] }} @endif">
															          					<label for="secret_key">{{ Lang::get('settings.secret-key') }}</label>
															        				</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</form>
														</div>
														<div class="select-payment stripe" style="display:@if($defaultPaymentMethod=='stripe') block @else none @endif">
															<form class="select-payment-fields">
																<div class="row">
																	<div class="col s12">
																		<div class="form-layout form-layout-50x50 clear-fix">
																			<div class="form-layout-50">
																				<div class="field-group">
																					<div class="input-field">
																						<input type="text" class="validate" name="stripe_client_id" value="@if(!empty($stripe_credentials)) {{ $stripe_credentials['client_id'] }} @endif">
															          					<label for="client_id">{{ Lang::get('settings.client-id') }}</label>
																					</div>	
																				</div>
																			</div>
																			<div class="form-layout-50">
																				<div class="field-group">
																					<div class="input-field">
															          					<input type="text" class="validate" name="stripe_secret_key" value="@if(!empty($stripe_credentials)) {{ $stripe_credentials['secret_key'] }} @endif">
															          					<label for="secret_key">{{ Lang::get('settings.secret-key') }}</label>
															        				</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</form>
														</div>
														<div class="row">
															<div class="col s12">
																<div class="buttons center">
																	@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")
																		<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>
																		<button class="waves-effect waves-light btn btn-green save" id="save-payment-credential">{{ Lang::get('common.save') }}</button>
																		<button class="waves-effect waves-light btn btn-green save" id="default-payment-gateway" style="display: none;">{{ Lang::get('settings.make-default') }}</button>
																	@endif
																</div>
															</div>
														</div>
													</div> 
												</div>
											</div>
										</div>
										<div id="tabs-8" class="settings-tabs @if($page=='deal_source') show-content @endif">
											<div class="tabs-wrapper">
												<div class="row">
													<div class="col s12">
														<h3>{{ Lang::get('settings.deal-source') }}</h3>
														<div class="form-layout form-layout-50x20x30 clear-fix">
															<div class="form-layout-50">
																<div class="field-group">
																	<div class="input-field">
																		@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")
											          						<input id="deal_source" name="add-deal-source" type="text" class="validate">
											          					@endif
											          					<label for="deal_source">{{ Lang::get('settings.deal-source') }}</label>
											          					<span class="error-mark"></span>
									          							<span class="check-mark"></span>
											        				</div>
																</div>
															</div>
															<div class="form-layout-50">
																<div class="input-field">
																	<a href="javascript:void(0);" id="add-deal-source" class="waves-effect waves-light btn green"> {{ Lang::get('common.add') }} </a>
																</div>
															</div>
														</div>
													</div> 
												</div>
												<div class="row">
													<div class="col s12">
														<div class="chip-container">
															@if(!empty($deal_source))
																@foreach($deal_source as $key=>$value)
																	<div class="chip scale-up-center {{ $deal_source_color_array[array_rand($deal_source_color_array)] }}">
																	    {{ $value }}
																	    <i class="close material-icons" data-deal-source="{{ $value }}">close</i>
																  	</div>	
															  	@endforeach
															@endif
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col s12">
														<div class="deal-stage">
															<h3><a href="#add-stage">Deal Stages</a></h3>
															<div class="vetical-tab-nav border clear-fix">
																<div class="vertical-nav-wrap">
																	<ul>
																		@foreach($stages as $key=>$value)
																			<li>
																				<a href="javascript:void(0)" data-stage-id="{{ $value['id'] }}" data-stage-visiblity="{{ $value['visiblity'] }}" data-value="{{ $value['slug'] }}" class="change-deal-visiblity">{{ $value['stage'] }}</a>
																			</li>
																		@endforeach
																	</ul>
																</div>
																<div class="vertical-tabs-content">
																	<div class="ver-tabs-panel" style="display: block">
																    	<form>
																			<div class="row">
																				<div class="col s12">
																					<div class="form-layout form-layout-50x50 clear-fix">
																						<div class="form-layout-50">
																							<div class="field-group">
																								<div class="input-field">
																						          	<input id="change-deal-title" name="change-deal-title" type="text" class="validate" value="{{ $stages[0]['stage'] }}" data-stage-id="{{ $stages[0]['id'] }}">
																						          	<label for="opportunity" class=""></label>
																						        </div>
																							</div>
																						</div>
																						<div class="form-layout-50">
																							<div class="input-field">
																								<!-- Dropdown Trigger -->
																								<a class="dropdown-button btn green select-button" href="#" data-activates="dropdown2">{{ Lang::get('settings.show') }}</a>
																								<!-- Dropdown Structure -->
																								<ul id="dropdown2" class="dropdown-content dropdown">
																									<li class="@if($stages[0]['visiblity']=='active')selected @endif"><a href="javascript:void(0)" value="active">{{ Lang::get('settings.show') }}</a></li>
																									<li class="@if($stages[0]['visiblity']=='hidden')selected @endif"><a href="javascript:void(0)" value="hidden">{{ Lang::get('settings.hidden') }}</a></li>
																								</ul>
																							</div>
																						</div>
																					</div> <!-- END FORM LAYOUT -->
																				</div> <!-- END COL -->
																			</div> <!-- END ROW -->
																		</form> <!-- END FORM -->
																		<div class="buttons center">
																			@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")
																				<a href="javascript:void(0)" class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</a>
																				<button class="waves-effect waves-light btn btn-green save" id="change-deal-visiblity">{{ Lang::get('common.save') }}</button>
																			@endif
																		</div> <!-- END BUTTONS -->
																	</div>

																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="tabs-9" class="settings-tabs @if($page=='financials') show-content @endif">
											<div class="tabs-wrapper">
												<div class="row">
													<div class="col s12">
														<div class="form-layout form-layout-80X10 clear-fix">
															<div class="form-layout-50">
																<div class="field-group">
																	<div class="input-field select-field">
															          	<select name="currency">
															          		<option value="">{{ Lang::get('settings.select-currency') }}</option>
																	    	@include('currency');
																		</select>
															        </div>
																</div>
															</div>
															<div class="form-layout-20">
																<div class="field-group">
																	<div class="input-field">
																		@if($permission['setting']['add']=="on" || $permission['setting']['edit']=="on")
																			<a href="javascript:void(0);" class="add-currency waves-effect waves-light btn green">{{ Lang::get('common.add') }}</a>
																		@endif
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col s12">
														<div class="chip-container currency-list">
															@if(!empty($company_currency))
																@foreach($company_currency as $key=>$value)
																	<div class="chip scale-up-center {{ $deal_source_color_array[array_rand($deal_source_color_array)] }}">
																	    {{ $value }}
																	    <i class="close material-icons" data-currency="{{ $value }}">close</i>
																  	</div>	
															  	@endforeach
															@endif
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="tabs-10" class="settings-tab @if($page=='automation') show-content @endif">
											<div class="tabs-wrapper">
												<div class="row">
													<div class="col s6">
														<form>
															<div class="form-layout form-layout-80x20 clear-fix">
																<div class="form-layout-80">
																	<div class="field-group">
																		<div class="input-field">
												          					<input disabled id="copyTarget" name="add-automation" type="text" class="validate" value="@php echo base_path(); @endphp/artisan schedule2:run >> /dev/null 2>&1">
												          					<label for="automation">{{ Lang::get('settings.corn-command') }}</label>
												          					<p>{{ Lang::get('settings.copy-this-url-and-create-a-cron-job-in-your-cpanel-The-cron-job-should-run-every-day') }}.</p>
												        				</div>
																	</div>
																</div>
																<div class="form-layout-20">
																	<div class="field-group">
																		<div class="input-field">
																			<a href="javascript:void(0)" id="copyButton" class="waves-effect waves-light btn btn-green copy-automation">{{ Lang::get('common.copy') }}</a>
																		</div>
																	</div>
																</div>
															</div>
														</form>
														<div class="buttons center">
															<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('settings.cancel') }}</button>
															<button class="waves-effect waves-light btn btn-green save" data-purpose="">{{ Lang::get('settings.save') }}</button>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="tabs-11" class="settings-tab @if($page=='automation') show-content @endif">
											<div class="tabs-wrapper">
												<div class="row">
										          <div class="col s6">
										            <h3> Import Contact List </h3>
										            <form id="uploadForm" name="upload-document" enctype="multipart/form-data" method="POST" action="{{ url('/') }}/import/contact">
										              <input type="hidden" name="_token" value="{{ csrf_token()}}">
										              <input type="file" id="contact" name="contact">
										              <!-- <input type="submit" name="import-contact" value="Import"> -->
										              <span class="import-contact-message"></span>
										              <span class="verifying"></span>
										              <div id="progress-div"><div id="progress-bar"></div></div>
										            </form>
										          </div>
										        </div>
											</div>
										</div>
									</div>
							    </div>
							</div>
						</div>
				    </div>
				</div>
			</div>
			<!-- Modal Structure -->
			<div id="modal4" class="modal modal-fixed-footer">
				<button class="modal-close"></button>
				<div class="modal-wrapper">
					<div class="modal-content">
						<div class="row">
							<div class="col s12">
								<h3>{{ Lang::get('common.add') }} {{ Lang::get('common.user') }}</h3>
							</div>
						</div>
						<form id="add-user">
							<div class="row">
								<div class="col s12">
									<div class="form-layout form-layout-50x50 clear-fix">
										<div class="form-layout-50">
											<div class="field-group">
												<div class="input-field">
										          	<input id="add-first-name" name="add-first-name" type="text" class="validate">
										          	<span class="error-mark"></span>
										          	<span class="check-mark"></span>
										          	<label for="add-first-name" class="">{{ Lang::get('common.first') }} {{ Lang::get('common.name') }}</label>
										        </div>
											</div>
										</div>
										<div class="form-layout-50">
											<div class="field-group">
												<div class="input-field">
										          	<input id="add-last-name" name="add-last-name" type="text" class="validate">
										          	<span class="error-mark"></span>
										          	<span class="check-mark"></span>
										          	<label for="add-last-name" class="">{{ Lang::get('common.last') }} {{ Lang::get('common.name') }}</label>
										        </div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<div class="form-layout form-layout-50x50 clear-fix">
										<div class="form-layout-50">
											<div class="field-group">
												<div class="input-field">
										          	<input id="add-user-email" name="add-user-email" type="email" class="validate">
										          	<span class="error-mark"></span>
										          	<span class="check-mark"></span>
										          	<label for="add-user-email" class="">{{ Lang::get('common.email-address') }}</label>
										        </div>
											</div>
										</div>
										<div class="form-layout-50">
											<div class="field-group">
												<div class="input-field">
										          	<input id="add-user-phone" name="add-user-phone" type="text" class="validate">
										          	<span class="error-mark"></span>
										          	<span class="check-mark"></span>
										          	<label for="add-user-phone" class="">{{ Lang::get('common.phone-no') }}</label>
										        </div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<div class="form-layout form-layout-50x50 clear-fix">
										<div class="form-layout-50">
											<div class="field-group">
												<div class="input-field select-field">
													<select name="add-company" disabled>
														<option value="stage" disabled selected>{{ Lang::get('common.company') }}</option>
														@if(!empty($company))
															@foreach($company as $key=>$value)
																<option value="{{ $value['id'] }}" selected>{{ $value['company_name'] }}</option>
															@endforeach
														@endif
													</select>
												</div>
											</div>
										</div>
										<div class="form-layout-50">
											<div class="field-group">
												<div class="input-field select-field">
													<select name="add-role">
														<option value="priority" disabled selected>{{ Lang::get('settings.role') }}</option>
														@foreach($roles as $key=>$value)
															@if($value!="admin")
																<option value="{{ $key }}">{{ $value }}</option>
															@endif
														@endforeach
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<div class="form-layout clear-fix">
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field">
										          	<input id="add-user-department" name="user-department" type="text" class="validate">
										          	<span class="error-mark"></span>
										          	<span class="check-mark"></span>
										          	<label for="add-user-department" class="">{{ Lang::get('settings.department') }}</label>
										        </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div class="buttons center">
							<button class="waves-effect waves-light btn btn-dark-orange cancel-modal">{{ Lang::get('common.cancel') }}</button>
							<button class="waves-effect waves-light btn btn-green create-user save">{{ Lang::get('common.save') }}</button>
							<span class="success-message"></span>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal Structure -->
			<div id="add-stage" class="modal modal-fixed-footer">
				<button class="modal-close"></button>
				<div class="modal-wrapper">
					<div class="modal-content">
						<div class="row">
							<div class="col s12">
								<h3>{{ Lang::get('settings.add-deal-stage') }}</h3>
							</div>
						</div>
						<form id="add-user">
							<div class="row">
								<div class="col s12">
									<div class="form-layout clear-fix">
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field">
										          	<input id="add-deal-stage" name="add-deal-stage" type="text" class="validate">
										          	<span class="error-mark"></span>
										          	<span class="check-mark"></span>
										          	<label for="add-deal-stage" class="">{{ Lang::get('settings.add-deal-stage') }}</label>
										        </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div class="buttons center">
							<button class="waves-effect waves-light btn btn-dark-orange cancel-modal">{{ Lang::get('common.cancel') }}</button>
							<button class="waves-effect waves-light btn btn-green save add-deal-stage">{{ Lang::get('common.save') }}</button>
							<span class="success-message"></span>
						</div>
					</div>
				</div>
			</div>

			<div class="list_of_possible_errors" style="display:none">
				<span class="message">{{ Lang::get('validation.success-message') }}</span>
				<span class="user-exist">{{ Lang::get('validation.user-exist') }}</span>
			</div>
@endsection