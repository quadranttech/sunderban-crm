@extends('layouts.dashboard-mobile-header')
@section('content')

	<div class="bottom-header">
	    <div class="container"> 
	      	<div class="row">
	      		<div class="col s8">
	      		</div>
	        	<div class="col s4">
	        	</div> <!-- END COL -->
	      	</div> <!-- END ROW -->
	    </div> <!-- END CONTAINER -->
	</div>  <!-- END BOTTOM HEADER -->

	<div class="contents">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<div class="row">
						<div class="col s12">
							<div class="content border">
								<div class="row">
									<div class="col s12">
										<h3>Edit User</h3>
									</div>
								</div>
								<form id="edit-user">
									<div class="row">
										<div class="col s12">
											<div class="form-layout form-layout-50x50 clear-fix">
												<div class="form-layout-100">
													<div class="field-group">
														<div class="input-field">
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<input type="hidden" name="user_id" value="{{ $userDetails['id'] }}">
												          	<input id="first-name" name="first-name" type="text" class="validate" value="{{ $userDetails['first_name'] }}">
												          	<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												          	<label for="first-name" class="">First Name</label>
												        </div>
													</div>
												</div>
												<div class="form-layout-100">
													<div class="field-group">
														<div class="input-field">
												          	<input id="last-name" name="last-name" type="text" class="validate" value="{{ $userDetails['last_name'] }}">
												          	<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												          	<label for="last-name" class="">Last Name</label>
												        </div>
													</div>
												</div>
											</div> <!-- END FORM LAYOUT -->
										</div> <!-- END COL -->
									</div> <!-- END ROW -->
									<div class="row">
										<div class="col s12">
											<div class="form-layout form-layout-50x50 clear-fix">
												<div class="form-layout-100">
													<div class="field-group">
														<div class="input-field">
												          	<input id="user-email" name="user-email" type="text" class="validate" value="{{ $userDetails['email'] }}">
												          	<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												          	<label for="user-email" class="">Email Address</label>
												        </div>
													</div>
												</div>
												<div class="form-layout-100">
													<div class="field-group">
														<div class="input-field">
												          	<input id="user-phone" name="user-phone" type="text" class="validate" value="{{ $userDetails['phone_no'] }}">
												          	<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												          	<label for="user-phone" class="">Phone No</label>
												        </div>
													</div>
												</div>
											</div> <!-- END FORM LAYOUT -->
										</div> <!-- END COL -->
									</div> <!-- END ROW -->
									<div class="row">
										<div class="col s12">
											<div class="form-layout form-layout-50x50 clear-fix">
												<div class="form-layout-100">
													<div class="field-group">
														<div class="input-field">
															<select name="company">
																<option value="stage" disabled selected>Compnay</option>
																@foreach($company as $key=>$value)
																	<option value="{{ $value['id'] }}" @if($userDetails['company']==$value['id']) selected @endif>{{ $value['company_name'] }}</option>
																@endforeach
															</select>
														</div>
													</div>
												</div>
												<div class="form-layout-100">
													<div class="field-group">
														<div class="input-field">
															<select name="role">
																<option value="priority" disabled selected>Role</option>
																@foreach($roles as $key=>$value)
																	<option value="{{ $key }}" @if($key==$userDetails['role']) selected @endif>{{ $value }}<option>
																@endforeach
															</select>
														</div>
													</div>
												</div>
											</div> <!-- END FORM LAYOUT -->
										</div> <!-- END COL -->
									</div> <!-- END ROW -->
									<div class="row">
										<div class="col s12">
											<div class="form-layout clear-fix">
												<div class="form-layout-100">
													<div class="field-group">
														<div class="input-field">
												          	<input id="user-department" name="user-department" type="text" class="validate" value="{{ $userDetails['department'] }}">
												          	<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												          	<label for="user-email" class="">Department</label>
												        </div>
													</div>
												</div>
											</div> <!-- END FORM LAYOUT -->
										</div> <!-- END COL -->
									</div> <!-- END ROW -->
								</form> <!-- END FORM -->
								<div class="buttons center">
									<a href="{{ url('/') }}/settings?page=user_roles" class="waves-effect waves-light btn btn-dark-orange">Cancel</a>
									<button class="waves-effect waves-light btn btn-green edit-user save">Save</button>
								</div> <!-- END BUTTONS -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- END CONTENTS -->

@endsection