@extends('layouts.dashboard-header')
@section('content')
	<div class="bottom-header">
	    <div class="had-container"> 
	      	<div class="row">
	      		<div class="col s8">
	      		</div>
	        	<div class="col s4 right-align">
	        		<!-- <a class="waves-effect waves-light btn blue">Add Contact</a> -->
	        	</div> <!-- END COL -->
	      	</div> <!-- END ROW -->
	    </div> <!-- END CONTAINER -->
	</div>  <!-- END BOTTOM HEADER -->
	<div class="contents contextual-section-wrapper">
  		<div class="had-container">
			<div class="contextual-section">
				<div class="row">
					<div class="col s8">
						<!-- LEFT PANEL -->
					    <div class="content border">
					    	<h3>{{ Lang::get('common.edit') }} {{ Lang::get('deal.Deal') }}</h3>
					    	<div id="tabs">
								<ul class="clear-fix">
									<li><a href="#tabs-1" id="show-deal-feed">Feed</a></li>
									<li><a href="#tabs-2" id="show-deal-details">{{ Lang::get('deal.Deal') }} {{ Lang::get('common.details') }}</a></li>
									<li><a href="#tabs-3" id="show-deal-task">{{ Lang::get('task.tasks') }}</a></li>
									<li><a href="#tabs-4" id="show-deal-document">{{ Lang::get('common.documents') }}</a></li>
									<li><a href="#tabs-5" id="show-deal-note">{{ Lang::get('common.notes') }}</a></li>
									<li><a href="#tabs-6" id="show-deal-product">Product</a></li>
								</ul>
								<div id="tabs-1">
									<div class="tabs-wrapper">
										<div class="feeds-list">
											<div class="content-table">
											</div> <!-- END CONTENT TABLE -->
										</div> <!-- END FEED LIST -->
									</div> <!-- END TAB WRAPPER -->
								</div> <!-- END TAB 1 -->
								<div id="tabs-2">
									<div class="tabs-wrapper">
										<form id="deal-contexual">
											<div class="row">
												<div class="col s12">
													<div class="form-layout form-layout-50x50 clear-fix">
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field select-field">
																	<input type="hidden" name="_token" value="{{ csrf_token() }}">
																	<input type="hidden" name="deal_id" value="{{ $deal['id'] }}">
															        <select name="status">
																		<option value="status">{{ Lang::get('estimate.status') }}</option>
																		@foreach($status as $key=>$value)
																			@if(!empty($deal['status'] && $value['id']==$deal['status']))
																				<option value="{{ $value['id'] }}" selected>{{ $value['status']}}</option>
																			@else
																				<option value="{{ $value['id'] }}">{{ $value['status']}}</option>
																			@endif
																		@endforeach
																	</select>
																	<label>Deal status</label>
																</div>
															</div>
														</div>
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
														          	<input id="source" name="source" type="text" class="validate" value="@if($deal['source']!='no_source') {{ $deal['source'] }} @endif">
														          	<label for="source">{{ Lang::get('deal.source') }}</label>
														        </div>
															</div>
														</div>
													</div> <!-- END FORM LAYOUT -->
												</div> <!-- END COL -->
											</div> <!-- END ROW -->
				
											<div class="row">
												<div class="col s12 deals-wrapper">
													<h6>{{ Lang::get('deal.Deal') }} {{ Lang::get('deal.stage') }}</h6>
													<div class="deal-stage-wrapper clear-fix">
														@foreach($stages as $key=>$value)
															<div class="deal-stages">
																@if(!empty($deal['stage']) && $value['id']==$deal['stage'])
																	<input class="with-gap" name="stage" type="radio" id="test-{{ $key }}" checked="" value="{{ $value['id'] }}">
																@else
																	<input class="with-gap" name="stage" type="radio" id="test-{{ $key }}" value="{{ $value['id'] }}">
																@endif
																<label for="test-{{ $key }}">{{ $value['stage'] }}</label>
															</div>	
														@endforeach
													</div>
												</div><!-- END COL -->
											</div><!-- END ROW -->
											
											<div class="row">
												<div class="col s12">
													<div class="form-layout form-layout-50x50 clear-fix">
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
														          	<select name="priority">
																		<option value="low" @if($deal['priority']=="low") selected @endif>{{ Lang::get('common.low') }}</option>
																		<option value="medium" @if($deal['priority']=="medium") selected @endif>{{ Lang::get('common.medium') }}</option>
																		<option value="large" @if($deal['priority']=="large") selected @endif>{{ Lang::get('common.high') }}</option>
																	</select>
														        </div>
															</div>
														</div>
													</div> <!-- END FORM LAYOUT -->
												</div> <!-- END COL -->
											</div> <!-- END ROW -->
											<div class="row">
												<div class="col s12">
													<div class="form-layout form-layout-50x50 clear-fix">
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
															        <input id="value" name="value" type="text" class="validate" value="{{ $deal['value'] }}">
														          	<label for="value">{{ Lang::get('deal.Deal') }} {{ Lang::get('common.value') }}</label>
																</div>
															</div>
														</div>
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
														          	<input id="win_probability" name="win_probability" type="text" class="validate" value="{{ $deal['win_probability'] }}">
														          	<label for="win_probability">{{ Lang::get('deal.win-probability') }}</label>
														        </div>
															</div>
														</div>
													</div> <!-- END FORM LAYOUT -->
												</div> <!-- END COL -->
											</div> <!-- END ROW -->
											<div class="row">
												<div class="col s12">
													<div class="form-layout form-layout-50x50 clear-fix">
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field">
																	<input type="text" name="ex-revenue" id="revenue">
																	<label for="revenue">{{ Lang::get('deal.expected-revenue') }}</label>
																</div>
															</div>
														</div>
														<div class="form-layout-50">
															<div class="field-group">
																<div class="input-field date-field">
																	<input type="text" id="date" class="form-control" data-dtp="dtp_kiaQI" placeholder="Expected Closing Date">
																	<span></span>
														        </div>
															</div>
														</div>
													</div> <!-- END FORM LAYOUT -->
												</div> <!-- END COL -->
											</div> <!-- END ROW -->
											<div class="row">
												<div class="col s12">
													<div class="form-layout clear-fix">
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
														          	<textarea id="description" name="description" class="materialize-textarea" value="{{ $deal['description'] }}">{{ $deal['description'] }}</textarea>
														          	<label for="description">Deal Description</label>
														        </div>
															</div>
														</div>
													</div> <!-- END FORM LAYOUT -->
												</div> <!-- END COL -->
											</div> <!-- END ROW -->
											<div class="row">
												<div class="col s12">
													<div class="form-layout clear-fix">
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
														          	<input type="text" id="created_date" name="created_date" value="{{ $deal['created_date'] }}" disabled>
														          	<label for="created_date">Created Date</label>
														        </div>
															</div>
														</div>
													</div> <!-- END FORM LAYOUT -->
												</div> <!-- END COL -->
											</div> <!-- END ROW -->
											<div class="row">
												<div class="col s6">
													<div class="form-layout clear-fix">
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
														          	<input type="text" id="date2" name="deal_start" class="materialize-textarea" value="{{ $deal['deal_start'] }}">
														          	<label for="date">Booking Start</label>
														        </div>
															</div>
														</div>
													</div> <!-- END FORM LAYOUT -->
												</div> <!-- END COL -->
												<div class="col s6">
													<div class="form-layout clear-fix">
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
														          	<input type="text" id="date3" name="deal_end" class="materialize-textarea" value="{{ $deal['deal_end'] }}">
														          	<label for="date">Booking End</label>
														        </div>
															</div>
														</div>
													</div> <!-- END FORM LAYOUT -->
												</div> <!-- END COL -->
											</div> <!-- END ROW -->
											<div class="row">
												<div class="col s12">
													<div class="selectContainer">
														<div class="selectedItem">
															<h3>Items selected</h3>
															<ul>
																<li>
																	<span class="desc">Package</span>
																	<span class="productPrice"><span>{{ $package }}</span>
																</li>
																<li>
																	<span class="desc">Person</span>
																	<span class="productPrice"><span>{{ $deal['person'] }}</span>
																</li>
																@if(!empty($room_array))
																	@foreach($room_array as $key=>$value)
																		@php
																			$price = 0;
																			if($special_food == true)
																			{
																				$percentage = (25 / 100) * $value['price'];
																				$price = $value['price'] + $percentage;
																			}
																			if($foreign_guest == "true")
																			{
																				$percentage = (25 / 100) * $price;
																				$price = $price + $percentage;
																			}
																			if($special_food != "true" && $foreign_guest != "true")
																			{
																				$price = $value['price'];
																			}
																		@endphp
																		<li>
																			<span class="desc">{{ $value['name'] }}</span>
																			<span class="productPrice"><span>{{ $price }}</span>
																		</li>
																		<li>
																			<span class="desc">Per Person Price</span>
																			<span class="productPrice"><span>{{ $value['per_person_price'] }}</span>
																		</li>
																	@endforeach
																@endif
																@if(!empty($add_on_array))
																	@foreach($add_on_array as $key=>$value)
																		<li>
																			<span class="desc">{{ $value['name'] }}</span>
																			@php
																			if (strpos(strtolower($value['name']), 'food') !== false)
														                    {
														                        $price = $value['price'] * ($day * $person);
														                    }
														                    else
														                    {
														                    	$price = $value['price'];
														                	}
																			@endphp
																			<span class="productPrice"><span>{{ $price }}</span>
																		</li>
																	@endforeach
																@endif
																<li>
																	<span class="desc">Total Price</span>
																	<span class="productPrice"><span>{{ $deal['value_without_gst'] }}</span>
																</li>
																<li>
																	<span class="desc">Total Price(with gst 5%)</span>
																	<span class="productPrice"><span>{{ $deal['value'] }}</span>
																</li>
															</ul>
														</div>
													</div>
												</div> <!-- END COL -->
											</div> <!-- END ROW -->
										</form> <!-- END FORM -->
										<div class="buttons center">
											<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>
											<button class="waves-effect waves-light btn btn-green deal-contexual save">{{ Lang::get('common.save') }}</button>
										</div> <!-- END BUTTONS -->
									</div> <!-- END TAB WRAPPER -->
								</div> <!-- END TAB 2 -->
								<div id="tabs-3">
									<div class="tabs-wrapper">
										<div class="tasks-list">
										</div> <!-- END TASKS LIST -->
									</div> <!-- END TAB WRAPPER -->
									<div class="right-align">
										<a href="#modal3" class="waves-effect waves-light btn blue" style="display:none">{{ Lang::get('common.add') }} {{ Lang::get('task.task') }}</a>
									</div>
								</div> <!-- END TAB 3 -->
								<div id="tabs-4">
									<div class="tabs-wrapper">
										<div class="row">
											<div class="col s12 document">
												
												<ul class="all-documents">
													<li>
														<div class="drag-area">
															<form id="upload-document" name="upload-document" enctype="multipart/form-data" method="POST" action="">
																<input type="file" name="document-upload" id="file" class="input-file">
																<input type="hidden" name="_token" value="{{ csrf_token()}}">
																<input type="hidden" name="deal_id" value="{{ $deal['id'] }}">
																<input type="hidden" name="contact_id" value="0">
																<input type="hidden" name="type" value="deal">
																<label for="file" class="drag-label border">
																	<i class="icon fa fa-check"></i>
																	<span class="file-upload"></span>
																	<span class="file-name"></span>
																</label>
																<div class="desc">
																	Click to browse to upload files
																</div>
															</form>
														</div>
													</li>
												</ul>
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
									</div> <!-- END TAB WRAPPER -->
								</div> <!-- END TAB 4 -->
								<div id="tabs-5">
									<div class="tabs-wrapper">
										<div class="note-lists clear-fix">
										</div> <!-- END NOTE LISTS -->
										<div class="notes-form border clear-fix">
											<span class="note-border"></span>
											<a href="javascript:void(0)" class="note-close"></a>
											<form id="add-note-att" name="add-note-att" enctype="multipart/form-data" method="POST" action="">
												<div class="form-layout form-layout-100 clear-fix">
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
																<input type="hidden" name="deal_id" value="{{ $deal['id'] }}">
																<input type="hidden" name="contact_id" value="0">
																<input type="hidden" name="current_note_id" value="1">
																<input type="hidden" name="_token" value="{{ csrf_token()}}">
													          	<input id="first_name" type="text" placeholder="Untitled Note">
													        </div>
														</div>
													</div>
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field text-wrapper">
													          	<textarea id="textarea1" class="materialize-textarea" placeholder="Start Writing Here"></textarea>
													        </div>
														</div>
													</div>
												</div>  <!-- END FORM LAYOUT -->
											</form>  <!-- END FORM -->
											<div class="buttons right">
												<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>
												<button class="waves-effect waves-light btn btn-green save-note save">{{ Lang::get('common.save') }} {{ Lang::get('common.note') }}</button>
											</div> <!-- END BUTTONS -->
										</div> <!-- END NOTES FORM -->
									</div> <!-- END TAB WRAPPER -->
								</div> <!-- END TAB 5 -->
								<div id="tabs-6">
									<div class="right-align">
										<a href="" class="waves-effect waves-light btn blue open-modal" data-target="add-product-to-deal">Add Product</a>
									</div>
									<div class="tabs-wrapper">
										<div class="product-lists clear-fix">

											<div id="contacts-list" class="deal-product-table">
												
												<div class="content-table">
													<div class="content-table-header border">
	                      								<div class="content-table-row">
	                      									<div class="content-table-data">Product Name</div>
	                      									<div class="content-table-data description">Product Description</div>
	                      									<div class="content-table-data">Product Price</div>
	                      									<div class="content-table-data"></div>
	                      									<div class="content-table-data"></div>
	                      								</div>
	                      							</div>

	                      							@php
													  	$count=1;
													@endphp
	                      							<div class="content-table-body contactTable list @if(empty($related_product)) demo-content @endif"> 
	                      								<input type="hidden" name="status_message" value="{{ session('status') }}">
	                      								@if(!empty($related_product))
   														@foreach ($related_product as $value)
	                      								<div class="content-table-row content-table-row-sort" data-product="{{ $value['id'] }}">
	                      									<div class="content-table-data item-name">{{ $value['name'] }}</div>
	                      									<div class="content-table-data description item-description">{{ $value['description'] }}</div>
	                      									<div class="content-table-data item-price">{{ $value['price'] }}@if($value['price_type'] == "percentile")%@endif</div>
	                      									<div class="content-table-data contactMore">
													          	<div class="more-actions"><button type="button" class="delete-product-from-deal" data-id="{{ $value['id'] }}">Delete</button></div>
													        </div>
													        <div class="content-table-data contactMore">
													          	<div class="more-actions"><a href="javascript:void(0)" class="edit-product-from-deal" data-product-id="{{ $value['id'] }}" data-deal-id="{{ $deal['id'] }}">Edit</a></div>
													        </div>
	                      								</div>
	                      								@endforeach 
  														@endif
	                      							</div>
												</div>
											</div>


											
										</div> <!-- END NOTE LISTS -->
										<div class="notes-form border clear-fix">
											<span class="note-border"></span>
											<a href="javascript:void(0)" class="note-close"></a>
											<?php
											// echo "<pre>";
											// print_r($product);
											// echo "</pre>";
											?>
											
											<form id="add-note-att" name="add-note-att" enctype="multipart/form-data" method="POST" action="">
												<div class="form-layout form-layout-100 clear-fix">
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
																<input type="hidden" name="deal_id" value="{{ $deal['id'] }}">
																<input type="hidden" name="contact_id" value="0">
																<input type="hidden" name="current_note_id" value="1">
																<input type="hidden" name="_token" value="{{ csrf_token()}}">
													          	<input id="first_name" type="text" placeholder="Untitled Note">
													        </div>
														</div>
													</div>
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field text-wrapper">
													          	<textarea id="textarea1" class="materialize-textarea" placeholder="Start Writing Here"></textarea>
													        </div>
														</div>
													</div>
												</div>  <!-- END FORM LAYOUT -->
											</form>  <!-- END FORM -->
											<div class="buttons right">
												<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>
												<button class="waves-effect waves-light btn btn-green save-note save">{{ Lang::get('common.save') }} {{ Lang::get('common.note') }}</button>
											</div> <!-- END BUTTONS -->
										</div> <!-- END NOTES FORM -->
									</div> <!-- END TAB WRAPPER -->
								</div> <!-- END TAB 5 -->
							</div> <!-- END TAB -->
					    </div> <!-- END CONTENT -->
					</div> <!-- END COL -->
					<div class="col s4">
						<!-- RIGHT PANEL -->
					    <div class="right-side-bar border" >
					    	<form>
						    	<div class="profile-wrapper">
						    		<!-- <span class="profile-image"><img src="{{ url('/') }}/assets/img/images.jpg" alt="avatar"></span> -->
						    		<div class="profile-name-designation">
						    			@foreach($contact as $key=>$value)
						    				@if($value['id']==$deal['contact'])
						    					<span class="name">{{ $value['title'] }}. {{ $value['first_name'] }} {{ $value['last_name'] }}</span>
						    				@endif
						    			@endforeach
						    			@if(!empty($contact_company))
						    				<span class="designation">{{ $contact_company['company_name'] }}</span>
						    			@endif
						    		</div>
						    	</div> <!-- END PROFILE WRAPPER -->
						    	<div class="widgets deal-details clear-fix ui-state-default">
									<div class="widgets-header">
										<h3><span class="drag-handle"></span>{{ Lang::get('deal.Deal') }} {{ Lang::get('common.details') }}</h3>
									</div> <!-- END INFORMATION HEADER -->
									<div class="profile-information">
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-100 clear-fix">
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
																<input type="hidden" name="_token" value="{{ csrf_token() }}">
																<input type="hidden" name="deal_id" value="{{ $deal['id'] }}">
													          	<input id="deal-title" name="deal_title" type="text" value="{{ $deal['title'] }}" class="validate" >
													          	<label for="opp-name">{{ Lang::get('deal.Deal') }} {{ Lang::get('common.Title') }}</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	@foreach($contact as $key=>$value)
												          			@if($value['id']==$deal['contact'])
											          					<input name="contact" type="text" value="{{ $value['first_name'] }} {{ $value['last_name'] }}" class="validate valid" disabled>
												          			@endif
												          		@endforeach
													          	<label>Contact Name</label>
													        </div>
														</div>
													</div>
													<!-- <div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<select name="company">
													          		<option value="0">{{ Lang::get('common.select') }} {{ Lang::get('common.company') }}</option>
													          		@foreach($company as $key=>$value)
													          			<option value="{{ $value['id'] }}">{{ $value['company_name'] }}</option>
													          		@endforeach
													          	</select>
													        </div>
														</div>
													</div> -->
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<select name="deal_priority">
													          		<option value="high" @if($deal['priority']=="high") selected @endif>{{ Lang::get('common.high') }}</option>
													          		<option value="medium" @if($deal['priority']=="medium") selected @endif>{{ Lang::get('common.medium') }}</option>
													          		<option value="low" @if($deal['priority']=="low") selected @endif>{{ Lang::get('common.low') }}</option>
													          	</select>
													        </div>
														</div>
													</div>
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<input id="deal-value" name="deal_value" type="text" value="{{ $deal['value'] }}" class="validate" >
													          	<label for="opp-name">{{ Lang::get('deal.Deal') }} {{ Lang::get('common.value') }}</label>
													        </div>
														</div>
													</div>
													
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field select-field">
													          	<select id="deal-owner" name="deal_owner">
													          	@foreach($users as $key=>$value)
													          		@if($value['id']==$deal['owner'])
													          			<option value="{{ $value['id'] }}" selected data-company="{{ $value['company'] }}">{{ $value['first_name'] }} {{ $value['last_name'] }}</option>
													          		@else
													          			<option value="{{ $value['id'] }}" data-company="{{ $value['company'] }}">{{ $value['first_name'] }} {{ $value['last_name'] }}</option>
													          		@endif
													          	@endforeach
													          	</select>
													          	<label for="deal-owner">Deal Owner</label>
													        </div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
									</div> <!-- END PROFILE INFORMATION -->
								</div>  <!-- END WIDGET -->
							</form> <!-- END FORM -->
							<div class="buttons center">
								<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>
								<button class="waves-effect waves-light btn btn-green update-deal save">{{ Lang::get('common.save') }}</button>
							</div> <!-- END BUTTON -->
					    </div> <!-- END RIGHT SIDE BAR -->
					</div> <!-- END COL -->
				</div> <!-- END ROW -->
		    </div> <!-- END CONTEXTUAL SECTION -->
		</div> <!-- END CONTAINER -->
	</div> <!-- END CONTENTS -->

	@if(!empty($contact))
		<div id="modal3" class="modal modal-fixed-footer">
			<button class="modal-close"></button>
			<div class="modal-wrapper">
				<div class="modal-content">
					<div class="row">
						<div class="col s12">
							<h3>{{ Lang::get('common.add') }} {{ Lang::get('task.task') }}</h3>
						</div> <!-- END COL -->
					</div> <!-- END ROW -->
					<form action="save-new-task" id="add-new-task-form" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="row">
							<div class="col s12">
								<div class="form-layout clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field">
									          	<select name="related_deal" id="related_deal">
									          		@if(!empty($deal))
									          			<option value="{{ $deal['id'] }}">{{ $deal['title'] }}</option>
									          		@else
									          			<option value="0">{{ Lang::get('common.no') }} {{ Lang::get('deal.Deal') }}</option>
									          		@endif
									          	</select>
									        </div>
										</div>
									</div>
								</div> <!-- END FORM LAYOUT -->
								<div class="form-layout clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field">
									          	<input name="task_title" type="text" class="validate">
									          	<span class="error-mark"></span>
									          	<span class="check-mark"></span>
									          	<label for="new-task">{{ Lang::get('common.untitled') }} {{ Lang::get('task.task') }}</label>
									        </div>
										</div>
									</div>
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field">
									          	<input name="task_details" type="text" class="validate">
									          	<span class="error-mark"></span>
									          	<span class="check-mark"></span>
									          	<label for="details">{{ Lang::get('task.task') }} {{ Lang::get('common.details') }}</label>
									        </div>
										</div>
									</div>
								</div> <!-- END FORM LAYOUT -->
								<div class="form-layout form-layout-50x50 clear-fix">
									<div class="form-layout-50">
										<div class="field-group">
											<div class="input-field">
											    <select name="assigned_to">
											    	<option value="" disabled selected>{{ Lang::get('task.assigned-to') }}</option>
											    	@foreach($contact as $value)
											    		<option value="{{ $value['id'] }}">{{ $value['first_name']}} {{$value['last_name']}}</option>
													@endforeach
												</select>
												<span class="error-mark"></span>
									          	<span class="check-mark"></span>
											</div>
										</div>
									</div>
									<div class="form-layout-50">
										<div class="field-group">
											<div class="input-field date-field">
									          	<input type="text" name="due_date" id="date2" class="form-control" data-dtp="dtp_kiaQI" placeholder="Due on">
									          	<span class="error-mark"></span>
									          	<span class="check-mark"></span>
									          	<span></span>
									        </div>
										</div>
									</div>
								</div> <!-- END FORM LAYOUT -->
							</div> <!-- END COL -->
						</div> <!-- END ROW -->
						<div class="buttons center">
							<button class="waves-effect waves-light btn btn-dark-orange cancel-modal">{{ Lang::get('common.cancel') }}</button>
							<button class="waves-effect waves-light btn createTask btn-green" disabled>{{ Lang::get('common.save') }}</button>
						</div> <!-- END BUTTONS -->
					</form> <!-- END FORM -->
				</div> <!-- END MODAL CONTENT -->
			</div><!-- END MODAL WRAPPER -->
		</div> <!-- END MODAL -->
	@endif

	<!-- ADD PRODUCT MODAL -->
	<div id="add-product-to-deal" class="modal modal-fixed-footer" style="display:none">
		<button class="modal-close"></button>
		<div class="modal-wrapper">
			<div class="modal-content">
				<div class="row">
					<div class="col s12">
						<h3>{{ Lang::get('common.add') }} Product</h3>
					</div><!--END COL -->
				</div><!-- END ROW -->
				<form id="create-deal">
					<div class="row">
						<div class="col s12">
							<div class="form-layout form-layout-50x50 clear-fix">
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field select-field">
								          	<select multiple name="product_id">
										      <option value="" disabled selected>Choose your option</option>
										      @foreach($product as $key=>$value)
										      	<option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
										      @endforeach
										    </select>
								          	<label for="untitle-opportunity" class="">Product</label>
								        </div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM  LAYOUT -->
							</div><!-- END FIELD LAYOUT -->
						</div><!-- END COL -->
					</div><!-- END ROW -->
				</form><!-- END FORM -->
				<div class="buttons center">
					<button class="waves-effect waves-light btn btn-dark-orange cancel-modal">{{ Lang::get('common.cancel') }}</button>
					<button class="waves-effect waves-light btn btn-green save add-product-to-deal">{{ Lang::get('common.save') }}</button>
				</div><!-- END BUTTONS -->
			</div><!-- END MODAL CONTENT -->
		</div><!-- END MODAL WRAPPER -->
	</div><!-- END MODAL -->
@endsection