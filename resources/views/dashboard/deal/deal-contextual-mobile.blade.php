@extends('layouts.dashboard-mobile-header')
@section('content')
	<div class="bottom-header">
	    <div class="container"> 
	      	<div class="row">
	        	<div class="col s12 left-align">
	        	</div> <!-- END COL -->
	      	</div> <!-- END ROW -->
	    </div> <!-- END CONTAINER -->
	</div>  <!-- END BOTTOM HEADER -->
	<div class="contents contextual-section-wrapper">
  		<div class="container">
  			<div class="row">
					<div class="contextual-section">
						<div class="col s12">
							<!-- LEFT PANEL -->
						    <div class="content border">
						    	<h3>{{ Lang::get('common.edit') }} {{ Lang::get('deal.Deal') }}</h3>
						    	<ul class="collapsible" data-collapsible="accordion">
								    <li>
								      	<div class="collapsible-header active" id="show-deal-feed">{{ Lang::get('deal.feed') }}</div>
								      	<div class="collapsible-body">
								      		<div id="tabs-1">
												<div class="tabs-wrapper">
													<div class="feeds-list">
														<div class="content-table">
														</div> <!-- END CONTENT TABLE -->
													</div> <!-- END FEED LIST -->
												</div> <!-- END TAB WRAPPER -->
											</div> <!-- END TAB 1 -->
								      	</div>
								    </li>
								    <li>
								      	<div class="collapsible-header" id="show-deal-details">{{ Lang::get('deal.Deal') }} {{ Lang::get('common.details') }}</div>
								      	<div class="collapsible-body">
								      		<div id="tabs-2">
												<div class="tabs-wrapper">
													<form id="deal-contexual">
														<div class="row">
															<div class="col s12">
																<div class="form-layout form-layout-50x50 clear-fix">
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																				<input type="hidden" name="_token" value="{{ csrf_token() }}">
																				<input type="hidden" name="deal_id" value="{{ $deal['id'] }}">
																		        <select name="status">
																					<option value="status">Status</option>
																					@foreach($status as $key=>$value)
																						@if(!empty($deal['status'] && $value['id']==$deal['status']))
																							<option value="{{ $value['id'] }}" selected>{{ $value['status']}}</option>
																						@else
																							<option value="{{ $value['id'] }}">{{ $value['status']}}</option>
																						@endif
																					@endforeach
																				</select>
																			</div>
																		</div>
																	</div>
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																	          	<input id="source" name="source" type="text" class="validate" value="@if($deal['source']!='no_source') {{ $deal['source'] }} @endif">
																	          	<label for="source">{{ Lang::get('deal.source') }}</label>
																	        </div>
																		</div>
																	</div>
																</div> <!-- END FORM LAYOUT -->
															</div> <!-- END COL -->
														</div> <!-- END ROW -->

														<div class="row">
															<div class="col s12 deals-wrapper">
																<h6>{{ Lang::get('deal.Deal') }} {{ Lang::get('deal.stage') }}</h6>
																<div class="deal-stage-wrapper clear-fix">
																	@foreach($stages as $key=>$value)
																		<div class="deal-stages">
																			@if(!empty($deal['stage']) && $value['id']==$deal['stage'])
																				<input class="with-gap" name="stage" type="radio" id="test-{{ $key }}" checked="" value="{{ $value['id'] }}">
																			@else
																				<input class="with-gap" name="stage" type="radio" id="test-{{ $key }}" value="{{ $value['id'] }}">
																			@endif
																			<label for="test-{{ $key }}">{{ $value['stage'] }}</label>
																		</div>	
																	@endforeach
																</div>
															</div><!-- END COL -->
														</div><!-- END ROW -->
				
														<div class="row">
															<div class="col s12">
																<div class="form-layout form-layout-50x50 clear-fix">
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																	          	<select name="priority">
																					<option value="low" @if($deal['priority']=="low") selected @endif>{{ Lang::get('common.low') }}</option>
																					<option value="medium" @if($deal['priority']=="medium") selected @endif>{{ Lang::get('common.medium') }}</option>
																					<option value="large" @if($deal['priority']=="large") selected @endif>{{ Lang::get('common.high') }}</option>
																				</select>
																	        </div>
																		</div>
																	</div>
																</div> <!-- END FORM LAYOUT -->
															</div> <!-- END COL -->
														</div> <!-- END ROW -->
														<div class="row">
															<div class="col s12">
																<div class="form-layout form-layout-50x50 clear-fix">
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																		        <input id="value" name="value" type="text" class="validate" value="{{ $deal['value'] }}">
																	          	<label for="value">{{ Lang::get('deal.Deal') }} {{ Lang::get('common.value') }}</label>
																			</div>
																		</div>
																	</div>
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																	          	<input id="win_probability" name="win_probability" type="text" class="validate" value="{{ $deal['win_probability'] }}">
																	          	<label for="win_probability">{{ Lang::get('deal.win-probability') }}</label>
																	        </div>
																		</div>
																	</div>
																</div> <!-- END FORM LAYOUT -->
															</div> <!-- END COL -->
														</div> <!-- END ROW -->
														<div class="row">
															<div class="col s12">
																<div class="form-layout form-layout-50x50 clear-fix">
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																		        <input type="text" name="ex-revenue" id="revenue">
																				<label for="revenue">{{ Lang::get('deal.expected-revenue') }}</label>
																			</div>
																		</div>
																	</div>
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field date-field">
																				<input type="text" id="date" class="form-control" data-dtp="dtp_kiaQI" placeholder="Expected Closing Date">
																				<span></span>
																	        </div>
																		</div>
																	</div>
																</div> <!-- END FORM LAYOUT -->
															</div> <!-- END COL -->
														</div> <!-- END ROW -->
														<div class="row">
															<div class="col s12">
																<div class="form-layout form-layout-100 clear-fix">
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																	          	<textarea id="description" name="description" class="materialize-textarea" value="{{ $deal['description'] }}">{{ $deal['description'] }}</textarea>
																	          	<label for="description">{{ Lang::get('deal.description') }}</label>
																	        </div>
																		</div>
																	</div>
																</div> <!-- END FORM LAYOUT -->
															</div> <!-- END COL -->
														</div> <!-- END ROW -->
													</form> <!-- END FORM -->
													<div class="buttons center">
														<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>
														<button class="waves-effect waves-light btn btn-green deal-contexual save">{{ Lang::get('common.save') }}</button>
													</div> <!-- END BUTTONS -->
												</div> <!-- END TAB WRAPPER -->
											</div> <!-- END TAB 2 -->
								      	</div>
								    </li>
								    <li>
								      	<div class="collapsible-header" id="show-deal-task">{{ Lang::get('task.tasks') }}</div>
								      	<div class="collapsible-body">
								      		<div id="tabs-3">
												<div class="tabs-wrapper">
													<div class="tasks-list task-structure">
													</div> <!-- END TASKS LIST -->
												</div> <!-- END TAB WRAPPER -->
												<div class="right-align">
													<a href="#modal3" class="waves-effect waves-light btn blue">{{ Lang::get('common.add') }} {{ Lang::get('task.task') }}</a>
												</div>
											</div> <!-- END TAB 3 -->
								      	</div>
								    </li>
								    <li>
								      	<div class="collapsible-header" id="show-deal-document">{{ Lang::get('common.documents') }}</div>
								      	<div class="collapsible-body">
								      		<div id="tabs-4">
												<div class="tabs-wrapper">
												<div class="row">
													<div class="col s12 document">
														<ul class="all-documents">
															<li>
																<div class="drag-area">
																	<form id="upload-document" name="upload-document" enctype="multipart/form-data" method="POST" action="">
																		<input type="file" name="document-upload" id="file" class="input-file">
																		<input type="hidden" name="_token" value="{{ csrf_token()}}">
																		<input type="hidden" name="deal_id" value="{{ $deal['id'] }}">
																		<input type="hidden" name="contact_id" value="0">
																		<input type="hidden" name="type" value="deal">
																		<label for="file" class="drag-label border">
																			<i class="icon fa fa-check"></i>
																			<span class="file-upload"></span>
																			<span class="file-name"></span>
																		</label>
																		<div class="desc">
																			Click to browse to upload files
																		</div>
																	</form>
																</div>
															</li>
														</ul>
													</div> <!-- END COL -->
												</div> <!-- END ROW -->
											</div> <!-- END TAB WRAPPER -->
											</div> <!-- END TAB 4 -->
								      	</div>
								    </li>
								    <li>
								      	<div class="collapsible-header" id="show-deal-note">{{ Lang::get('common.notes') }}</div>
								      	<div class="collapsible-body">
								      		<div id="tabs-5">
												<div class="tabs-wrapper">
												<div class="note-lists clear-fix">
												</div> <!-- END NOTE LISTS -->
												<div class="notes-form border clear-fix">
													<span class="note-border"></span>
													<a href="javascript:void(0)" class="note-close"></a>
													<form id="add-note-att" name="add-note-att" enctype="multipart/form-data" method="POST" action="">
														<div class="form-layout form-layout-100 clear-fix">
															<div class="form-layout-100">
																<div class="field-group">
																	<div class="input-field">
																		<input type="hidden" name="deal_id" value="{{ $deal['id'] }}">
																		<input type="hidden" name="contact_id" value="0">
																		<input type="hidden" name="current_note_id" value="1">
																		<input type="hidden" name="_token" value="{{ csrf_token()}}">
															          	<input id="first_name" type="text" placeholder="Untitled Note">
															        </div>
																</div>
															</div>
															<div class="form-layout-100">
																<div class="field-group">
																	<div class="input-field text-wrapper">
															          	<textarea id="textarea1" class="materialize-textarea" placeholder="Start Writing Here"></textarea>
															        </div>
																</div>
															</div>
														</div>  <!-- END FORM LAYOUT -->
													</form>  <!-- END FORM -->
													<div class="buttons right">
														<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>
														<button class="waves-effect waves-light btn btn-green save-note save">{{ Lang::get('common.save') }} {{ Lang::get('common.note') }}</button>
													</div> <!-- END BUTTONS -->
												</div> <!-- END NOTES FORM -->
											</div> <!-- END TAB WRAPPER -->
											</div> <!-- END TAB 5 -->
								      	</div>
								    </li>
								</ul>
						    </div>
						</div>
						<div class="col s12">
							<!-- RIGHT PANEL -->
						    <div class="right-side-bar border">
						    	<form>
							    	<div class="profile-wrapper">
							    		<!-- <span class="profile-image"><img src="{{ url('/') }}/assets/img/images.jpg" alt="avatar"></span> -->
							    		<div class="profile-name-designation">
							    			@foreach($contact as $key=>$value)
							    				@if($value['id']==$deal['contact'])
							    					<span class="name">{{ $value['title'] }}. {{ $value['first_name'] }} {{ $value['last_name'] }}</span>
							    				@endif
							    			@endforeach
							    			@if(!empty($contact_company))
							    				<span class="designation">{{ $contact_company['company_name'] }}</span>
							    			@endif
							    		</div>
							    	</div> <!-- END PROFILE WRAPPER -->
							    	<div class="widgets deal-details clear-fix ui-state-default">
										<div class="widgets-header">
											<h3><span class="drag-handle"></span>{{ Lang::get('deal.Deal') }} {{ Lang::get('common.details') }}</h3>
										</div> <!-- END INFORMATION HEADER -->
										<div class="profile-information">
											<div class="row">
												<div class="col s12">
													<div class="form-layout form-layout-100 clear-fix">
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
																	<input type="hidden" name="_token" value="{{ csrf_token() }}">
																	<input type="hidden" name="deal_id" value="{{ $deal['id'] }}">
																	<input type="hidden" name="current_note_id" value="1">
																	<input type="hidden" name="_token" value="{{ csrf_token()}}">
														          	<input id="deal-title" name="deal_title" type="text" value="{{ $deal['title'] }}" class="validate" >
														          	<label for="opp-name">{{ Lang::get('deal.Deal') }} {{ Lang::get('common.title') }}</label>
														        </div>
															</div>
														</div>
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
														          	<select name="contact">
														          		@foreach($contact as $key=>$value)
														          			<option value="{{ $value['id'] }}" @if($value['id']==$deal['contact']) selected @endif>{{ $value['first_name'] }} {{ $value['last_name'] }}</option>
														          		@endforeach
														          	</select>
														        </div>
															</div>
														</div>
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
														          	<select name="company">
														          		<option value="0">{{ Lang::get('common.select') }} {{ Lang::get('common.company') }}</option>
														          		@foreach($company as $key=>$value)
														          			<option value="{{ $value['id'] }}">{{ $value['company_name'] }}</option>
														          		@endforeach
														          	</select>
														        </div>
															</div>
														</div>
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
														          	<select name="deal_priority">
														          		<option value="high" @if($deal['priority']=="high") selected @endif>{{ Lang::get('common.high') }}</option>
														          		<option value="medium" @if($deal['priority']=="medium") selected @endif>{{ Lang::get('common.medium') }}</option>
														          		<option value="low" @if($deal['priority']=="low") selected @endif>{{ Lang::get('common.low') }}</option>
														          	</select>
														        </div>
															</div>
														</div>
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
														          	<input id="deal-value" name="deal_value" type="text" value="{{ $deal['value'] }}" class="validate" >
														          	<label for="opp-name">{{ Lang::get('deal.Deal') }} {{ Lang::get('common.value') }}</label>
														        </div>
															</div>
														</div>
														<div class="form-layout-100">
															<div class="field-group">
																<div class="input-field">
														          	<select id="deal-owner" name="deal_owner">
														          	@foreach($contact as $key=>$value)
														          		@if($value['id']==$deal['owner'])
														          			<option value="{{ $value['id'] }}" selected data-company="{{ $value['company'] }}">{{ $value['first_name'] }} {{ $value['last_name'] }}</option>
														          		@else
														          			<option value="{{ $value['id'] }}" data-company="{{ $value['company'] }}">{{ $value['first_name'] }} {{ $value['last_name'] }}</option>
														          		@endif
														          	@endforeach
														          	</select>
														        </div>
															</div>
														</div>
													</div> <!-- END FORM LAYOUT -->
												</div> <!-- END COL -->
											</div> <!-- END ROW -->
										</div> <!-- END PROFILE INFORMATION -->
									</div>  <!-- END WIDGET -->
								</form> <!-- END FORM -->
								<div class="buttons center">
									<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>
									<button class="waves-effect waves-light btn btn-green update-deal save">{{ Lang::get('common.save') }}</button>
								</div> <!-- END BUTTON -->
						    </div>
						</div>
				    </div>
		    </div>
		</div>
	</div>

	@if(!empty($contact))
		<div id="modal3" class="modal modal-fixed-footer">
			<button class="modal-close"></button>
			<div class="modal-wrapper">
				<div class="modal-content">
					<div class="row">
						<div class="col s12">
							<h3>{{ Lang::get('common.add') }} {{ Lang::get('task.task') }}</h3>
						</div> <!-- END COL -->
					</div> <!-- END ROW -->
					<form action="save-new-task" id="add-new-task-form" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="row">
							<div class="col s12">
								<div class="form-layout clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field">
									          	<select name="related_deal" id="related_deal">
									          		<option value="">Selected Deal</option>
									          		@if(!empty($deal))
									          			<option value="{{ $deal['id'] }}">{{ $deal['title'] }}</option>
									          		@else
									          			<option value="0">{{ Lang::get('common.no') }} {{ Lang::get('deal.Deal') }}</option>
									          		@endif
									          	</select>
									        </div>
										</div>
									</div>
								</div> <!-- END FORM LAYOUT -->
								<div class="form-layout clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field">
									          	<input name="task_title" type="text" class="validate">
									          	<span class="error-mark"></span>
									          	<span class="check-mark"></span>
									          	<label for="new-task">{{ Lang::get('common.untitled') }} {{ Lang::get('task.task') }}</label>
									        </div>
										</div>
									</div>
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field">
									          	<input name="task_details" type="text" class="validate">
									          	<span class="error-mark"></span>
									          	<span class="check-mark"></span>
									          	<label for="details">{{ Lang::get('task.task') }} {{ Lang::get('common.details') }}</label>
									        </div>
										</div>
									</div>
								</div> <!-- END FORM LAYOUT -->
								<div class="form-layout form-layout-50x50 clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field">
											    <select name="assigned_to">
											    	<option value="" disabled selected>{{ Lang::get('task.assigned-to') }}</option>
											    	@foreach($contact as $value)
											    		<option value="{{ $value['id'] }}">{{ $value['first_name']}} {{$value['last_name']}}</option>
													@endforeach
												</select>
												<span class="error-mark"></span>
									          	<span class="check-mark"></span>
											</div>
										</div>
									</div>
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field date-field">
									          	<input type="text" name="due_date" id="date2" class="form-control" data-dtp="dtp_kiaQI" placeholder="Due on">
									          	<span class="error-mark"></span>
									          	<span class="check-mark"></span>
									          	<span></span>
									        </div>
										</div>
									</div>
								</div> <!-- END FORM LAYOUT -->
							</div> <!-- END COL -->
						</div> <!-- END ROW -->
						<div class="buttons center">
							<button class="waves-effect waves-light btn btn-dark-orange cancel-modal">{{ Lang::get('common.cancel') }}</button>
							<button class="waves-effect waves-light btn createTask btn-green" disabled>{{ Lang::get('common.save') }}</button>
						</div> <!-- END BUTTONS -->
					</form> <!-- END FORM -->
				</div> <!-- END MODAL CONTENT -->
			</div><!-- END MODAL WRAPPER -->
		</div> <!-- END MODAL -->
	@endif
@endsection