@extends('layouts.dashboard-mobile-header')
@section('content')
	<div class="bottom-header">
	    <div class="container"> 
	      	<div class="row">
	      		<div class="col s12 left-align">
	        		@if($permission['deal']['add']=="on")<a class="modal-trigger waves-effect waves-light btn blue" href="#modal2">{{ Lang::get('common.add') }} {{ Lang::get('deal.Deal') }}</a>@endif
	        	</div> <!-- END COL -->
	      	</div> <!-- END ROW -->
	    </div> <!-- END CONTAINER -->
	</div>  <!-- END BOTTOM HEADER -->
	<div class="contents">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<div class="drag-container border">
						<div class="drag-wrapper">
							<a href="javascript:void(0);" class="pre">pre</a>
							<a href="javascript:void(0);" class="nxt">next</a>
							<ul class="drag-list">
								@php
									$count=1;
								@endphp
								@foreach($deals as $key=>$value)
									<li class="drag-column drag-column-{{ $key }}" data-li-stage="{{ $stages_id_array[$key] }}">
										<div class="drag-header">
											<h4 class="drag-column-header blue-text">{{ $key }}</h4>
											<ul>
												@foreach($currency_array[$key] as $currency_key=>$currency_value)
													<li>
														<span class="total-price">{{ $currency_value }}</span>
													</li>
												@endforeach
											</ul>
											<!-- <span class="total-price">${{ $total_value_per_stage[$key] }}</span> -->
											<div class="drag-header-more">
												@if($permission['deal']['add']=="on")<a href="#modal2" data-stage="{{ $stages_id_array[$key] }}" data-owner="{{ $current_user }}" class="modal-trigger add-deals"></a>@endif
												<!-- <a href="javascript:void(0);" class="icons"><i class="material-icons">more_vert</i></a> -->
											</div>
										</div>
										<ul class="drag-inner-list" id="{{ $count }}">
										@if(!empty(array_filter($value)))
												@foreach($value as $inner_key=>$inner_value)
													<li class="drag-item" data-deal-id="{{ $inner_value['id'] }}">
														<div id="task-{{ $key }}-{{ $inner_key }}" class="card task">
															@php
															$color=(100-$inner_value['win_probability'])/3;
															$color1=round($color)+round($color);
															$color2=round($color);
															@endphp
															<span class="card-bg" style="background:linear-gradient(to bottom, #92283f {{ $color2 }}%, #ff695c {{ $color1 }}%, #ffca6e {{ $inner_value['win_probability'] }}%);"></span>
															<div class="card-content clear-fix">
																<span class="card-title"><a href="{{url('/')}}/deal-contexual?deal_id={{ $inner_value['id'] }}&page=deal_feed">{{ $inner_value['title'] }}</a></span>
																<div class="task-assigned">
																	@if(!empty($userImage[$key]))
																		@foreach($userImage[$key] as $userImage_key=>$userImage_value)
																			<span class="avatar-pic-wrapper"><span class="avatar-pic"><img src="{{ $inner_value['image'] }}"></span></span>
																		@endforeach
																	@endif
																</div>
																<div class="task-price">
																	<span class="price"><span>$</span> {{ $inner_value['value'] }}</span>
																</div>
															</div>
															<div class="card-action">
																<span class="card-id">#{{ $inner_value['id'] }}</span>
																<span class="card-status">Follow up</span>
															</div>
															<span class="card-type">{{ $status_per_deal[$inner_value['id']] }}</span>
														</div>
													</li>
												@endforeach
										@endif
										</ul>
									</li>
									@php
										$count++;
									@endphp
								@endforeach
							</ul>
						</div>
					</div><!-- END DRAG CONTAINER -->
				</div> <!--END COL -->
			</div><!-- END ROW -->
		</div><!-- END CONTAINER -->
	</div><!-- END CONTENTS -->
	<!-- ADD DEAL MODAL STRUCTURE -->
	<div id="modal2" class="modal modal-fixed-footer">
		<button class="modal-close"></button>
		<div class="modal-wrapper">
			<div class="modal-content">
				<div class="row">
					<div class="col s12">
						<h3>{{ Lang::get('common.add') }} {{ Lang::get('deal.Deal') }}</h3>
					</div><!--END COL -->
				</div><!-- END ROW -->
				<form id="create-deal">
					<div class="row">
						<div class="col s12">
							<div class="form-layout form-layout-50x50 clear-fix">
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field">
								          	<input id="untitle-opportunity" name="title" type="text" class="validate">
								          	<label for="untitle-opportunity" class="">{{ Lang::get('deal.Deal') }} {{ Lang::get('common.title') }}</label>
								        </div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM  LAYOUT -->
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field select-field">
											<select name="contact">
												<option value="" disabled selected>{{ Lang::get('common.choose') }} {{ Lang::get('common.Contact') }}</option>
												@foreach($contacts as $contacts_key=>$contacts_value)
													<option value="{{ $contacts_value['id'] }}">{{ $contacts_value['first_name'] }} {{ $contacts_value['last_name'] }}</option>
												@endforeach
											</select>
										</div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM LAYOUT -->
							</div><!-- END FIELD LAYOUT -->
						</div><!-- END COL -->
					</div><!-- END ROW -->
					<div class="row">
						<div class="col s12">
							<div class="form-layout form-layout-50x50 clear-fix">
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field select-field">
								          	<select multiple name="product_id">
										      <option value="" disabled selected>Choose your option</option>
										      @foreach($product as $key=>$value)
										      	<option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
										      @endforeach
										    </select>
								          	<label for="untitle-opportunity" class="">Product</label>
								        </div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM  LAYOUT -->
							</div><!-- END FIELD LAYOUT -->
						</div><!-- END COL -->
					</div><!-- END ROW -->
					<div class="row">
						<div class="col s12">
							<div class="form-layout form-layout-50x50 clear-fix">
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field select-field">
											<select name="company">
													<option value="" disabled selected>{{ Lang::get('common.choose') }} {{ Lang::get('common.company') }}</option>
												@foreach($company as $company_key=>$company_value)
													<option value="{{ $company_value['id'] }}">{{ $company_value['company_name'] }}</option>
												@endforeach
											</select>
										</div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM LAYOUT -->
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field select-field">
								          	<select name="owner">
								          		<option value="" disabled>{{ Lang::get('deal.Deal') }} {{ Lang::get('deal.owner') }}</option>
								          		@foreach($users as $user_key=>$user_value)
								          			@if($user_value['id']==$current_user)
								          				<option value="{{ $user_value['id'] }}" selected>{{ $user_value['name'] }}</option>
								          			@else
								          				<option value="{{ $user_value['id'] }}">{{ $user_value['name'] }}</option>
								          			@endif
								          		@endforeach
											</select>
								        </div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM LAYOUT -->
							</div><!-- END FORM LAYOUT -->
						</div><!-- END COL -->
					</div><!-- END ROW -->

					<div class="row">
						<div class="col s12 deals-wrapper">
							<h6>{{ Lang::get('deal.Deal') }} {{ Lang::get('deal.stage') }}</h6>
							<div class="deal-stage-wrapper clear-fix">
								@foreach($stages as $stages_key=>$stages_value)
									<div class="deal-stages">
										@if($stages_key==0)
											<input class="with-gap" name="stage" type="radio" id="test-{{ $stages_key }}" checked="" value="{{ $stages_value['id'] }}">
										@else
											<input class="with-gap" name="stage" type="radio" id="test-{{ $stages_key }}" value="{{ $stages_value['id'] }}">
										@endif
										<label for="test-{{ $stages_key }}">{{ $stages_value['stage'] }}</label>
									</div>	
								@endforeach
							</div>
						</div><!-- END COL -->
					</div><!-- END ROW -->

					<div class="row">
						<div class="col s12">
							<div class="form-layout form-layout-50x50 clear-fix">
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field select-field">
								          	<select id="source" name="source">
								          			<option value="" disabled selected>{{ Lang::get('deal.Deal') }} {{ Lang::get('deal.source') }}</option>
								          		@if(!empty($deal_source))
								          			@foreach($deal_source as $key=>$value)
								          				<option value="{{ $value }}">{{ $value }}</option>
								          			@endforeach
								          		@else
								          			<option value="">{{ Lang::get('common.no') }} {{ Lang::get('deal.source') }}</option>
								          		@endif
								          	</select>
								        </div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM LAYOUT -->
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<div class="form-layout form-layout-50x50 clear-fix">
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field select-field">
											<select name="status">
												<option value="" disabled selected>{{ Lang::get('deal.Deal') }} {{ Lang::get('deal.status') }}</option>
												@foreach($status as $status_key=>$status_value)
													<option value="{{ $status_value['id'] }}">{{ $status_value['status'] }}</option>
												@endforeach
											</select>
										</div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM LAYOUT -->

								

								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field select-field">
								          	<select name="priority">
								          		<option value="" disabled selected>{{ Lang::get('deal.Deal') }} {{ Lang::get('deal.priority') }}</option>
												<option value="low">{{ Lang::get('common.low') }}</option>
												<option value="medium">{{ Lang::get('common.medium') }}</option>
												<option value="high">{{ Lang::get('common.high') }}</option>
											</select>
								       </div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM LAYOUT -->
							</div><!-- END FORM LAYOUT -->
						</div><!-- END COL -->
					</div><!-- END ROW -->
					<div class="row">
						<div class="col s12">
							<div class="form-layout-100">
								<div class="field-group">
									<div class="input-field select-field">
										<select name="currency">
											<option value="" disabled selected>{{ Lang::get('deal.Deal') }} {{ Lang::get('deal.currency') }}</option>
											@foreach($currency as $currency_key=>$currency_value)
												<option value="{{ $currency_value }}">{{ $currency_value }}</option>
											@endforeach
										</select>
									</div><!-- END INPUT-FIELD -->
								</div><!-- END FIELD GROUP -->
							</div><!-- END FORM LAYOUT -->
						</div><!-- END COL -->
					</div><!-- END ROW -->
					<div class="row">
						<div class="col s12">
							<div class="form-layout form-layout-50x50 clear-fix">
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field">
											<input type="text" id="contexual-deal-value" name="value">
											<span class="error-mark"></span>
							          		<span class="check-mark"></span>
											<label for="value">{{ Lang::get('deal.Deal') }} {{ Lang::get('common.value') }}</label>
										</div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM LAYOUT -->
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field">
								          	<input type="text" name="win_probability">
								          	<label for="win-probability">{{ Lang::get('deal.win-probability') }} (%)</label>
								       </div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM LAYOUT -->
							</div><!-- END FORM LAYOUT -->
						</div><!-- END COL -->
					</div><!-- END ROW -->
					<div class="row">
						<div class="col s12">
							<div class="form-layout clear-fix">
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field">
											<input id="description" name="description" type="text" class="validate">
											<input type="hidden" name="created_by" value="{{ $current_user }}">
											<input type="hidden" name="pre_stage" value="">
											<input type="hidden" name="user_agent" value="{{ $user_agent }}">
								          	<label for="description">{{ Lang::get('deal.Deal') }} {{ Lang::get('common.details') }}</label>
										 </div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM LAYOUT -->
							</div><!-- END FORM LAYOUT -->
						</div><!-- END COL -->
					</div><!-- END ROW -->
				</form><!-- END FORM -->
				<div class="buttons center">
					<button class="waves-effect waves-light btn btn-dark-orange cancel-modal">{{ Lang::get('common.cancel') }}</button>
					<button class="waves-effect waves-light btn btn-green save create-deal" data-purpose="deal">{{ Lang::get('common.save') }}</button>
				</div><!-- END BUTTONS -->
			</div><!-- END MODAL CONTENT -->
		</div><!-- END MODAL WRAPPER -->
	</div><!-- END MODAL -->
	<div class="list_of_possible_errors" style="display:none">
		<span class="contact-required">{{ Lang::get('validation.contact-required') }}</span>
		<span class="select-stage">{{ Lang::get('validation.deal-stage') }}</span>
	</div>
@endsection