@extends('layouts.dashboard-mobile-header')
@section('content')

	<div class="bottom-header">
	    <div class="container"> 
	      	<div class="row">
	      		<div class="col s8">
	      		</div>
	        	<div class="col s4 left-align">
	        	</div> <!-- END COL -->
	      	</div> <!-- END ROW -->
	    </div> <!-- END CONTAINER -->
	</div>

	<div class="contents contextual-section-wrapper content-tab-view">
  		<div class="container">
  			<div class="row">
  				<div class="col s12">
					<div class="contextual-section">
					    <div class="content border">
					    	<h3>{{ Lang::get('contact.personal-information') }}</h3>
					    	<ul class="collapsible" data-collapsible="accordion">
					    		<li>
					    			<div class="collapsible-header">Personal Info</div>
							      	<div class="collapsible-body">
							      		<div id="tabs-1">
											<div class="tabs-wrapper">
												<div class="row">
													<div class="col s12">
														<form class="clear-fix" id="manage-account-form" action="{{ url('/') }}/save-account-details" method="POST">
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-50x50 clear-fix">
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
																					<input type="hidden" name="_token" value="{{ csrf_token()}}">
														          					<input id="first-name" name="first_name" type="text" class="validate" value="{{ $user['first_name'] }}">
														          					<span class="error-mark"></span>
							          												<span class="check-mark"></span>
														          					<label for="first-name">{{ Lang::get('common.first') }} {{ Lang::get('common.name') }}</label>
														        				</div><!-- END INPUT-FIELD -->
																			</div><!-- END FIELD GROUP -->
																		</div><!-- END FORM LAYOUT -->
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="last-name" name="last_name" type="text" class="validate" value="{{ $user['last_name'] }}">
														          					<span class="error-mark"></span>
							          												<span class="check-mark"></span>
														          					<label for="last-name">{{ Lang::get('common.last') }} {{ Lang::get('common.name') }}</label>
														        				</div><!-- END INPUT-FIELD -->
																			</div><!-- END FIELD GROUP -->
																		</div><!-- END FORM LAYOUT -->
																	</div><!-- END FIELD LAYOUT -->
																</div><!-- END COL -->
															</div><!-- END ROW -->
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-50x20x10 clear-fix">
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
																		          	<input id="email" name="login_email" type="email" class="validate" value="{{ $user['email'] }}" disabled>
																		          	<span class="error-mark"></span>
							          												<span class="check-mark"></span>
																		          	<label for="email">{{ Lang::get('common.email') }}</label>
																		       </div><!-- END INPUT-FIELD -->
																			</div><!-- END FIELD GROUP -->
																		</div><!-- END FORM LAYOUT -->
																	</div><!-- END FIELD LAYOUT -->
																</div><!-- END COL -->
															</div><!-- END ROW -->
															@if(!empty($extra_email['home']))
																@foreach($extra_email['home'] as $key=>$value)
																	<div class="row user-extra-email">
																		<div class="col s12">
																			<div class="form-layout form-layout-50x20x10 clear-fix">
																				<div class="form-layout-100">
																					<div class="field-group">
																						<div class="input-field">
																				          	<input id="email" name="extra_email[]" type="email" class="validate" value="{{ $value }}">
																				          	<label for="email">{{ Lang::get('common.email') }}</label>
																				        </div><!-- END INPUT-FIELD -->
																					</div><!-- END FIELD GROUP -->
																				</div><!-- END FORM LAYOUT -->
																				<div class="form-layout-20">
																					<div class="field-group">
																						<div class="input-field">
																				          	<select name="extra_email_purpose[]">
																								<option value="work">{{ Lang::get('common.work') }}</option>
																								<option value="home" selected>{{ Lang::get('common.home') }}</option>
																							</select>
																				        </div><!-- END INPUT-FIELD -->
																					</div><!-- END FIELD GROUP -->
																				</div><!-- END FORM LAYOUT -->
																				<div class="form-layout-10">
																					<div class="field-group">
																						<div class="input-field">
																							<a href="javascript:void(0);" class="add-input add-extra-user-email"> <span></span></a>
																						</div><!-- END INPUT-FIELD -->
																					</div><!-- END FIELD GROUP -->
																				</div><!-- END FORM LAYOUT -->
																			</div><!-- END FORM LAYOUT -->
																		</div><!-- END COL -->
																	</div><!-- END ROW -->
																@endforeach
															@endif
															@if(!empty($extra_email['work']))
																@foreach($extra_email['work'] as $key=>$value)
																	<div class="row user-extra-email">
																		<div class="col s12">
																			<div class="form-layout form-layout-50x20x10 clear-fix">
																				<div class="form-layout-100">
																					<div class="field-group">
																						<div class="input-field">
																				          	<input name="extra_email[]" type="email" class="validate" value="{{ $value }}">
																				          	<label for="email">{{ Lang::get('common.email') }}</label>
																				       </div><!-- END INPUT-FIELD -->
																					</div><!-- END FIELD GROUP -->
																				</div><!-- END FORM LAYOUT -->
																				<div class="form-layout-20">
																					<div class="field-group">
																						<div class="input-field">
																				          	<select name="extra_email_purpose[]">
																								<option value="work" selected>{{ Lang::get('common.work') }}</option>
																								<option value="home">{{ Lang::get('common.home') }}</option>
																							</select>
																				        </div><!-- END INPUT-FIELD -->
																					</div><!-- END FIELD GROUP -->
																				</div><!-- END FORM LAYOUT -->
																				<div class="form-layout-10">
																					<div class="field-group">
																						<div class="input-field">
																							<a href="javascript:void(0);" class="add-input add-extra-user-email"> <span></span></a>
																						</div><!-- END INPUT-FIELD -->
																					</div><!-- END FIELD GROUP -->
																				</div><!-- END FORM LAYOUT -->
																			</div><!-- END FIELD LAYOUT -->
																		</div><!-- END COL -->
																	</div><!-- END ROW -->
																@endforeach
															@endif
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-50x50 clear-fix">
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="work-title" name="work_title" type="text" class="validate" value="{{ $user_meta['work_title'] }}">
														          					<label for="work-title">{{ Lang::get('common.work') }} {{ Lang::get('common.Title') }}</label>
														        				</div><!-- END INPUT-FIELD -->
																			</div><!-- END FIELD GROUP -->
																		</div><!-- END FORM LAYOUT -->
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="department" name="department" type="text" class="validate" value="@if($user['department']!='no_department' && $user['department']!="") {{ $user['department'] }} @endif">
														          					<label for="department">{{ Lang::get('settings.department') }}</label>
														        				</div><!-- END INPUT-FIELD -->
																			</div><!-- END FIELD GROUP -->
																		</div><!-- END FORM LAYOUT -->
																	</div><!-- END FIELD LAYOUT -->
																</div><!-- END COL -->
															</div><!-- END ROW -->
															<div class="row">
																<div class="col s12">
																	<div class="form-layout clear-fix">
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="bio" name="bio" type="text" class="validate" value="{{ $user_meta['bio'] }}">
														          					<label for="bio">{{ Lang::get('settings.bio') }}</label>
														        				</div><!-- END INPUT-FIELD -->
																			</div><!-- END FIELD GROUP -->
																		</div><!-- END FORM LAYOUT -->
																	</div><!-- END FIELD LAYOUT -->
																</div><!-- END COL -->
															</div><!-- END ROW -->
															<div class="row">
																<div class="col s12">
																	<div class="form-layout clear-fix">
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="street-address" name="address" type="text" class="validate" value="{{ $user_meta['address'] }}">
														          					<label for="street-address">{{ Lang::get('contact.street_address') }}</label>
														        				</div><!-- END INPUT-FIELD -->
																			</div><!-- END FIELD GROUP -->
																		</div><!-- END FORM LAYOUT -->
																	</div><!-- END FIELD LAYOUT -->
																</div><!-- END COL -->
															</div><!-- END ROW -->
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-50x50 clear-fix">
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="city" name="city" type="text" class="validate" value="{{ $user_meta['city'] }}">
														          					<label for="city">{{ Lang::get('contact.city') }}</label>
														        				</div><!-- END INPUT-FIELD -->
																			</div><!-- END FIELD GROUP -->
																		</div><!-- END FORM LAYOUT -->
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="state/region" name="state" type="text" class="validate" value="{{ $user_meta['state'] }}">
														          					<label for="state/region">{{ Lang::get('contact.state-region') }}</label>
														        				</div><!-- END INPUT-FIELD -->
																			</div><!-- END FIELD GROUP -->
																		</div><!-- END FORM LAYOUT -->
																	</div><!-- END FIELD LAYOUT -->
																</div><!-- END COL -->
															</div><!-- END ROW -->
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-50x50 clear-fix">
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field select-field">
														          					<select name="country">
																		          		<option value="" disabled selected>{{ Lang::get('common.country') }}</option>
																				      	@include('country',['country' => $user_meta['country']]);
																    				</select>
														        				</div><!-- END INPUT-FIELD -->
																			</div><!-- END FIELD GROUP -->
																		</div><!-- END FORM LAYOUT -->
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
														          					<input id="pin-code" name="post_code" type="text" class="validate" value="{{ $user_meta['post_code'] }}">
														          					<label for="pin-code">{{ Lang::get('contact.post-code') }}</label>
														        				</div><!-- END INPUT-FIELD -->
																			</div><!-- END FIELD GROUP -->
																		</div><!-- END FORM LAYOUT -->
																	</div><!-- END FIELD LAYOUT -->
																</div><!-- END COL -->
															</div><!-- END ROW -->
														</form>	<!-- END FORM -->
														<div class="buttons center">
															<button class="waves-effect waves-light btn btn-green save-manage-account save">{{ Lang::get('common.save') }}</button>
														</div>
													</div><!-- END COL -->
													<div class="col s12">
														<div id="crop-avatar">
														    <!-- Current avatar -->
														    <div class="avatar-view">
														      	<img id="result" src="@if($user_meta['image']!='') {{ url('/') }}/uploads/profile-image/{{ $user_meta['image'] }} @else {{ url('/') }}/assets/img/avatar.png @endif" alt="Avatar">
														      	<div class="logo-hover-state">
														      		<span class="file-upload"></span>
														      		<span>{{ Lang::get('seetings.click-to-upload') }}</span>
														      	</div>
														    </div>

														    <p class="img-size">Suggested Size: 216 x 216, Format: .jpg, .gif, .png</p>

														    <!-- Cropping modal -->
														    <div class="modal fade modal-fixed-footer" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="z-index: 1005">
														    	<button class="modal-close avatar-modal-close"></button>
														    	<div class="modal-wrapper">
															      	<div class="modal-dialog modal-lg">
															        	<div class="modal-content">
															          		<form class="avatar-form" action="{{ url('/') }}/crop" enctype="multipart/form-data" method="post">
															            		<div class="modal-header">
															            			<div class="row">
																		                <div class="col s12">
															              					<h3 id="avatar-modal-label">{{ Lang::get('settings.upload-and-crop-image') }}</h3>
															              				</div>
															              			</div>
															            		</div>
															            		<div class="modal-body">
															              			<div class="avatar-body">
															              				<div class="row">
																		                  	<div class="col s12">
																			                	<!-- Upload image and data -->
																			                	<div class="avatar-upload">
																				                	<input type="hidden" name="_token" value="{{ csrf_token()}}">
																				                	<input type="hidden" name="company_id" value="@if(!empty($getCompanyDetails)){{ $getCompanyDetails->id }}@endif">
																				                	<input type="hidden" name="purpose" value="user_image">
																			                  		<input type="hidden" class="avatar-src" name="avatar_src">
																			                  		<input type="hidden" class="avatar-data" name="avatar_data">
																			                  		<label for="avatarInput">{{ Lang::get('settings.local-upload') }}</label>
																			                  		<input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
																			                	</div>

																				                <!-- Crop and preview -->
																				                <div class="showImage">
																				                    <div class="avatar-wrapper"></div>
																				                    <p class="img-size">{{ Lang::get('settings.scroll-to-fit-the-image-wihtin-the-cropper-bouding-box') }}.</p>
																				                </div>
																				            </div>
																				        </div>

															                			<div class="row">
															                				<div class="avatar-btns">
																                  				<div class="col s6">
																                  				</div>
																                  				<div class="col s6 right-align">
																                    				<button type="submit" class="btn btn-primary green avatar-save">{{ Lang::get('settings.upload-and-save') }}</button>
																                  				</div>
																                  			</div>
															                			</div>
															              			</div>
															            		</div>
															          		</form>
															        	</div>
															      	</div>
														      	</div>
														    </div><!-- /.modal -->
														    <!-- Loading state -->
														    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
														</div>
													</div>
												</div><!-- END ROW -->
											</div><!-- END TABS WRAPPER -->
										</div><!-- END TABS-1 -->
							      	</div>
					    		</li>
					    	</ul>
					    </div><!-- END CONTENT -->
				    </div><!-- END CONTEXTUAL SECTION -->
		    	</div><!-- END COL -->
		    </div><!-- END ROW -->
		</div><!-- END CONTAINER -->
	</div><!-- END CONTEXTUAL SECTION WRAPPER -->


@endsection