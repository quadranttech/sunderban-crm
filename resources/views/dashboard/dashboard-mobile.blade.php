@extends('layouts.dashboard-mobile-header')@section('content')

<div class="bottom-header">
    <div class="had-container">
      <div class="row">
          <div class="col s12 left-align"> 
            <a href="{{url('/contact/new')}}" class="waves-effect waves-light btn blue">{{ Lang::get('common.add') }} {{ Lang::get('contact.Contact') }}</a> 
            <a href="{{url('/deals')}}" class="modal-trigger waves-effect waves-light btn green">{{ Lang::get('common.add') }} {{ Lang::get('deal.Deal') }}</a> 
          </div><!-- END COL -->
      </div><!-- END ROW -->
    </div><!-- END CONTAINER -->
</div>

<!-- END BOTTOM HEADER -->
<div class="dashboard-contents-wrapper">
  <div class="had-container">
    <div class="row">
      <div class="col s12">
        <div class="dashboard-contents border">

          <div class="row">
            <div class="col s12">
              <div class="select-dashboard">
                <ul>
                  <li @if($role=='admin')class='active' @endif><a href="javascript:void(0)">{{ Lang::get('dashboard.company-dashboard') }}</a></li>
                  <li @if($role=='staff')class='active' @endif><a href="javascript:void(0)">{{ Lang::get('dashboard.personal-dashboard') }}</a></li>
                </ul>
              </div>
            </div>
          </div>

          <div class="company-dashboard" @if($role=='admin')style='display:block' @else style='display:none' @endif>
            <div class="row">
              <div class="col s12">
                <div class="deal-card-list clear-fix">
                  <div class="card @if(empty($deals_proposal_array['company_proposal_value']))no-data @endif">
                    <h2>{{ Lang::get('deal.Deal') }} - proposal</h2>
                    @if(!empty($deals_proposal_array['company_proposal_value']))
                    <div class="flexslider">
                      <ul class="slides">
                        @foreach($deals_proposal_array['company_proposal_value'] as $key=>$value)
                        <li>
                          <span class="price">
                            <span>{{ $deals_proposal_array['currency_symbol'][$key] }}{{ $value }}</span>
                          </span> 
                        </li>
                        @endforeach
                      </ul>
                    </div>
                    @endif
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if(empty($deals_proposal_array['company_proposal_value']))
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/deals" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create') }} {{ Lang::get('deal.Deal') }}</a> 
                    </div>
                    @endif
                  </div><!-- END CARD -->
                  <div class="card @if(empty($deals_proposal_array['company_won_value']))no-data @endif">
                    <h2>{{ Lang::get('deal.Deal') }} - won</h2>
                    @if(!empty($deals_proposal_array['company_won_value']))
                    <div class="flexslider">
                      <ul class="slides">
                        @foreach($deals_proposal_array['company_won_value'] as $key=>$value)
                        <li>
                          <span class="price">
                            <span>{{ $deals_proposal_array['currency_symbol'][$key] }}{{ $value }}</span>
                          </span> 
                        </li>
                        @endforeach
                      </ul>
                    </div>
                    @endif
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if(empty($deals_proposal_array['company_won_value']))
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/deals" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create') }} {{ Lang::get('deal.Deal') }}</a>  
                    </div>
                    @endif
                  </div> <!-- END CARD -->
                </div> <!-- END DEAL CARD LIST -->
                <div class="graphs-wrapper">
                  <div class="graph border sales-graph">
                    <h3>{{ Lang::get('dashboard.monthly-sales-figures') }}</h3>
                    <canvas id="canvas"></canvas>
                  </div>
                </div> <!-- END GRAPH WRAPPER -->
              </div> <!-- END COL -->
              <div class="col s12">
                <div class="deal-card-list clear-fix">
                  <div class="card card-small @if(empty($invoice['due']))no-data @endif">
                    <h2>{{ Lang::get('invoice.Invoice') }} {{ Lang::get('task-due.due') }}</h2>
                     @if(!empty($invoice['due']))
                    <div class="flexslider">
                      <ul class="slides">
                        @foreach($invoice['due'] as $key=>$value)
                        <li>
                          <span class="price">
                            <span>{{ $deals_proposal_array['currency_symbol'][$key] }}{{ $value }}</span>
                          </span> 
                        </li>
                        @endforeach
                      </ul>
                    </div>
                    @endif
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if(empty($invoice['due']))
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/invoice" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create') }} {{ Lang::get('invoice.Invoice') }}</a>
                    </div>
                    @endif
                  </div><!-- END CARD -->
                  <div class="card card-small @if($invoice['paid']==0)no-data @endif">
                    <h2>{{ Lang::get('dashboard.invoices-paid') }}</h2>
                    @if(!empty($invoice['paid']))
                    <div class="flexslider">
                      <ul class="slides">
                        @foreach($invoice['paid'] as $key=>$value)
                        <li>
                          <span class="price">
                            <span>{{ $deals_proposal_array['currency_symbol'][$key] }}{{ $value }}</span>
                          </span> 
                        </li>
                        @endforeach
                      </ul>
                    </div>
                    @endif
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if(empty($invoice['paid']))
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/invoice" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create') }} {{ Lang::get('invoice.Invoice') }}</a>
                    </div>
                    @endif
                  </div><!-- END CARD -->
                </div> <!-- END DEAL CARD LIST -->
                <div class="graphs-wrapper">
                  <div class="graph border funnel-graph">
                    <h3>{{ Lang::get('dashboard.lead-figures-current-month') }}</h3>
                    <div id="canvas-holder">
                        <canvas id="chart-area" height="160"></canvas>
                    </div>
                  </div>
                </div> <!-- END GRAPH WRAPPER -->
              </div> <!-- END COL -->
            </div> <!-- END ROW -->

            <div class="row">
              <div class="col s12">
                <div class="deal-card-list clear-fix">
                  <div class="card card-small @if(empty($invoice['send']))no-data @endif">
                    <h2>{{ Lang::get('dashboard.invoices-sent') }}</h2>
                    @if(!empty($invoice['send']))
                    <div class="flexslider">
                      <ul class="slides">
                        @foreach($invoice['send'] as $key=>$value)
                        <li>
                          <span class="price">
                            <span>{{ $deals_proposal_array['currency_symbol'][$key] }}{{ $value }}</span>
                          </span> 
                        </li>
                        @endforeach
                      </ul>
                    </div>
                    @endif 
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if(empty($invoice['send']))
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/invoice" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create') }} {{ Lang::get('invoice.Invoice') }}</a> 
                    </div>
                    @endif
                  </div><!-- END CARD -->
                  <div class="card card-small @if($invoice['paid']==0)no-data @endif">
                    <h2>{{ Lang::get('dashboard.invoices-paid') }}</h2>
                    @if(!empty($invoice['paid']))
                    <div class="flexslider">
                      <ul class="slides">
                        @foreach($invoice['paid'] as $key=>$value)
                        <li>
                          <span class="price">
                            <span>{{ $deals_proposal_array['currency_symbol'][$key] }}{{ $value }}</span>
                          </span> 
                        </li>
                        @endforeach
                      </ul>
                    </div>
                    @endif
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if(empty($invoice['paid']))
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/invoice" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create') }} {{ Lang::get('invoice.Invoice') }}</a> 
                    </div>
                    @endif
                  </div><!-- END CARD -->
                  <div class="card card-small @if(empty($estimate['send']))no-data @endif">
                    <h2>{{ Lang::get('dashboard.estimates-sent') }}</h2>
                    @if(!empty($estimate['send']))
                    <div class="flexslider">
                      <ul class="slides">
                        @foreach($estimate['send'] as $key=>$value)
                        <li>
                          <span class="price">
                            <span>{{ $deals_proposal_array['currency_symbol'][$key] }}{{ $value }}</span>
                          </span> 
                        </li>
                        @endforeach
                      </ul>
                    </div>
                    @endif
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if(empty($estimate['send']))
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/estimate" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create ') }} {{ Lang::get('estimate.Estimate') }}</a> 
                    </div>
                    @endif
                  </div><!-- END CARD -->
                  <div class="card card-small @if(empty($estimate['accepted']))no-data @endif">
                    <h2>{{ Lang::get('dashboard.estimates-accepted') }}</h2>
                    @if(!empty($estimate['accepted']))
                    <div class="flexslider">
                      <ul class="slides">
                        @foreach($estimate['accepted'] as $key=>$value)
                        <li>
                          <span class="price">
                            <span>{{ $deals_proposal_array['currency_symbol'][$key] }}{{ $value }}</span>
                          </span> 
                        </li>
                        @endforeach
                      </ul>
                    </div>
                    @endif
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if(empty($estimate['accepted']))
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/estimate" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create ') }} {{ Lang::get('estimate.Estimate') }}</a> 
                    </div>
                    @endif
                  </div><!-- END CARD -->
                </div> <!-- END DEAL CARD LIST -->
              </div>
            </div> <!-- END ROW -->
          </div> <!-- END COMPANY DASHBOARD -->

          <div class="personal-dashboard" @if($role=='staff')style='display:block' @else style='display:none' @endif>
            <div class="row">
              <div class="col s12">
                <div class="deal-card-list clear-fix">
                  <div class="card card-small @if($deals_proposal_array['user_won_value']==0)no-data @endif">
                    <h2>{{ Lang::get('dashboard.deals-won-by-you') }}</h2>
                     @if(!empty($deals_proposal_array['user_won_value']))
                    <div class="flexslider">
                      <ul class="slides">
                        @foreach($deals_proposal_array['user_won_value'] as $key=>$value)
                        <li>
                          <span class="price">
                            <span>{{ $deals_proposal_array['currency_symbol'][$key] }}{{ $value }}</span>
                          </span> 
                        </li>
                        @endforeach
                      </ul>
                    </div>
                    @endif
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if(empty($deals_proposal_array['user_won_value']))
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/estimate" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create') }} {{ Lang::get('deal.Deal') }}</a> 
                    </div>
                    @endif
                  </div><!-- END CARD -->
                  <div class="card card-small @if($deals_proposal_array['user_created_deal']==0)no-data @endif">
                    <h2>{{ Lang::get('dashboard.new-deals-by-you') }}</h2>
                    @if(!empty($deals_proposal_array['user_created_deal']))
                    <div class="flexslider">
                      <ul class="slides">
                        @foreach($deals_proposal_array['user_created_deal'] as $key=>$value)
                        <li>
                          <span class="price">
                            <span>{{ $deals_proposal_array['currency_symbol'][$key] }}{{ $value }}</span>
                          </span> 
                        </li>
                        @endforeach
                      </ul>
                    </div>
                    @endif
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if(empty($deals_proposal_array['user_created_deal']))
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/deals" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create') }} {{ Lang::get('deal.Deals') }}</a> 
                    </div>
                    @endif
                  </div><!-- END CARD -->
                  <div class="card card-small @if($userDueTask==0)no-data @endif">
                    <h2>{{ Lang::get('dashboard.your-due-tasks') }}</h2>
                    <span class="price">
                      {{ $userDueTask }}
                    </span> 
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if($userDueTask==0)
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/tasks" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create') }} {{ Lang::get('task.tasks') }}</a> 
                    </div>
                    @endif
                  </div><!-- END CARD -->
                  <div class="card card-small @if($contactCreatedByUser==0)no-data @endif">
                    <h2>{{ Lang::get('dashboard.contacts-added-by-you') }}</h2>
                    <span class="price">
                      {{ $contactCreatedByUser }}
                    </span> 
                    <span class="year">
                      {{ $deals_proposal_array['current_month'] }}
                    </span>
                    @if($contactCreatedByUser==0)
                    <div class="no-data-screen">
                      <h4>{{ Lang::get('dashboard.no-data-yet-lets-start-now') }}</h4>
                      <a href="{{ url('/') }}/contacts" class="modal-trigger waves-effect waves-light btn blue">{{ Lang::get('common.create') }} {{ Lang::get('contact.Contacts') }}</a> 
                    </div>
                    @endif
                  </div><!-- END CARD -->
                </div> <!-- END DEAL CARD LIST -->
              </div>
            </div>

            <div class="row"> 
              @if(!empty($feeds))
              <div class="col s6">
                <div class="feeds-list">
                  <h3>{{ Lang::get('common.feed') }}</h3>
                  <div class="border">
                    <div class="content-table"> 
                      @if(!empty($feeds))                          
                        @foreach($feeds as $key=>$value)
                        <div class="content-table-row">
                          <div class="content-table-data"><span class="timeline @php echo $feed_color_array[array_rand($feed_color_array)] @endphp"></span></div>
                          <div class="content-table-data"> {{ $value['feed'] }}</div>
                          <div class="content-table-data">@php echo date("d M,Y", strtotime($value['created_at'])); @endphp</div>
                          <div class="content-table-data">@php echo date("h:i a", strtotime($value['created_at'])); @endphp</div>
                        </div>
                        @endforeach                        
                      @endif 
                    </div>
                  </div>
                </div> <!-- END FEED LIST -->
              </div> <!-- END COL -->
              @endif              

              @if(!empty($tasks))
              <div class="col s6">
                <h3>{{ Lang::get('task.task') }}</h3>
                <div class="tasks-list border"> 
                  @foreach($tasks as $key=>$value)
                  <div class="tasks @if($value['marked_as']=="done") task-complete @endif" data-task-id="{{ $value['id'] }}"> 
                    <span class="border-right {{ $value['color'] }}"></span>
                    <div class="tasks-short-description">
                      <input type="checkbox" name="markTask" class="filled-in" id="id-{{ $key }}" value="{{ $value['id'] }}" />
                      <label for="id-{{ $key }}"> {{ $value['task_title']}} </label>
                      <div class="task-assignee">{{ $value['assigned_to'] }} </div>
                      <span class="tasks-priority {{ $value['color'] }}">{{ $value['due_date'] }}</span>
                      <a href="javascript:void(0)" class="icons more-actions edit edit-task" data-edit-task-id="{{ $value['id'] }}">{{ Lang::get('common.edit') }}</a> 
                    </div>
                  </div>
                  @endforeach 
                </div>
              </div>
              @endif 
            </div> <!-- END ROW -->
          </div> <!-- END PERSONAL DASHBOARD -->
          
          

        </div> <!-- END DASHBOARD CONTENTS -->
      </div> <!-- END COL -->
    </div> <!-- END ROW -->
  </div> <!-- END CONTAINER -->
</div> <!-- END DASHBOARD WRAPPER -->

<!-- POPUP VIEW -->
<div class="more-task-popup border contextual-tasks">
  <div class="popup-header">
    <ul>
      <li><a href="javascript:void(0)" class="edit edit-task" data-edit-task-id="">Edit</a></li>
      <li><a href="javascript:void(0)" class="complete">Mark complete</a></li>
      <li><a href="javascript:void(0)" class="change-date active">Change due date</a></li>
      <li><a href="javascript:void(0)" class="assign-task">Assign others</a></li>
    </ul>
  </div>
</div><!-- END OF POPUP WINDOW -->

@endsection