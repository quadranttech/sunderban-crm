@extends('layouts.dashboard-header')
@section('content')
<div class="bottom-header">
  <div class="had-container">
    <div class="row">
      <div class="col s8"> </div>
      <div class="col s4"> </div>
      <!-- END COL -->
    </div>
    <!-- END ROW -->
  </div>
  <!-- END CONTAINER -->
</div>

<div class="row">
	<div class="col s12">
	  <div class="contents">
	    <div class="had-container">
	      <div class="contacts-grid white border">
	        <div class="row">
	          <div class="col s8">
	            <h3>Call <span class="total-contact">(@php echo sizeof($data); @endphp)</span></h3>
	          </div>
	          <!-- END COL -->
	        </div>
	        <!-- END ROW -->
	        <div id="contacts-list" class="call-list-table">
	          <div class="list-search">
	            <button type="button" class="search-icon"></button>
	            <input class="search" placeholder="Search" />
	          </div>
	          <div class="content-table">
	            <div class="content-table-header border">
	              <div class="content-table-row">
	                <div class="content-table-data sortable" data-sort="date" data-value="desc" data-clone="content-table-row">Date</div>
	                <div class="content-table-data">Time</div>
	                <div class="content-table-data">Contact Number</div>
	                <div class="content-table-data">Contact Name</div>
	                <div class="content-table-data"></div>
	              </div>
	              <!-- END CONTENT TABLE ROW -->
	            </div>
	            <!-- END CONTENT TABLE HEADER -->
	            @php
	              $count=1;
	            @endphp
	            <div class="content-table-body contactTable list @if(empty($data)) demo-content @endif"> 
	              <input type="hidden" name="status_message" value="{{ session('status') }}">
	              @if(!empty($data))
	                @foreach ($data as $value)
	                  <div class="content-table-row content-table-row-sort">
	                  	<div class="content-table-data created_at" data-date="{{ $value['created_at'] }}">{{ $value['created_at'] }}</div>
	                  	<div class="content-table-data">@php echo explode("at",$value['date'])[1]; @endphp</div>
	                    <div class="content-table-data phoneContactNumber">{{ $value['number'] }}</div>
	                    <div class="content-table-data phoneContactName">{{ $value['contact_name'] }}</div>
	                  	<div class="content-table-data"><div class="icons"><a href="javascript:void(0)" data-target="call-record-modal" data-phone="{{ $value['id'] }}" class="open-modal show-call-activity-log"><i class="material-icons">visibility</i></a></div></div>
	                  </div>
	                  <!-- END CONTENT TABLE ROW -->
	                @endforeach 
	              @else
	                <div class="content-table-row">
	                	<div class="content-table-data"></div>
	                  	<div class="content-table-data"></div>
	                    <div class="content-table-data phoneContactName">unknown</div>
	                    <div class="content-table-data phoneContactNumber">98989898</div>
	                  	<div class="content-table-data"><div class="icons"><i class="material-icons">visibility</i></div></div>
	                  </div>
	                  <div class="content-table-row">
	                  	<div class="content-table-data"></div>
	                  	<div class="content-table-data"></div>
	                    <div class="content-table-data phoneContactName">unknown</div>
	                    <div class="content-table-data phoneContactNumber">98989898</div>
	                  	<div class="content-table-data"><div class="icons"><i class="material-icons">visibility</i></div></div>
	                  </div>
	                  <div class="content-table-row">
	                  	<div class="content-table-data"></div>
	                  	<div class="content-table-data"></div>
	                    <div class="content-table-data phoneContactName">unknown</div>
	                    <div class="content-table-data phoneContactNumber">98989898</div>
	                  	<div class="content-table-data"><div class="icons"><i class="material-icons">visibility</i></div></div>
	                  </div>
	                  <div class="content-table-row">
	                  	<div class="content-table-data"></div>
	                  	<div class="content-table-data"></div>
	                    <div class="content-table-data phoneContactName">unknown</div>
	                    <div class="content-table-data phoneContactNumber">98989898</div>
	                  	<div class="content-table-data"><div class="icons"><i class="material-icons">visibility</i></div></div>
	                  </div>
	              @endif
	            </div>
	            <!-- END CONTENT TABLE BODY -->
	          </div>
	        </div>
	        <!-- END CONTENT TABLE -->
	      </div>
	      <!-- END CONTACT GRID-->
	    </div>
	    <!-- END CONTAINER -->
	  </div>
	</div>
</div>

<!-- ADD PRODUCT MODAL -->
<div id="call-record-modal" class="modal modal-fixed-footer" style="display:none">
	<button class="modal-close"></button>
	<div class="modal-wrapper">
		<div class="modal-content">
			<div class="row">
				<div class="col s12">
					<h3>Call Record</h3>
				</div><!--END COL -->
			</div><!-- END ROW -->
			<div class="row">
				<div class="col s6">
					<h5>Phone</h5>
				</div><!--END COL -->
				<div class="col s6">
					<p class="phone-contact-phone-no"></p>
				</div><!--END COL -->
			</div><!-- END ROW -->
			<div class="row">
				<div class="col s6">
					<h5>Contact Name</h5>
				</div><!--END COL -->
				<div class="col s6">
					<p class="phone-contact-phone-name"></p>
				</div><!--END COL -->
			</div><!-- END ROW -->
			<div class="row">
				<div class="col s12">
					<div class="call-date-table">
						<table class="phone-contact-call-activity-log">
							
						</table>
					</div>
				</div>
			</div>
		</div><!-- END MODAL CONTENT -->
	</div><!-- END MODAL WRAPPER -->
</div><!-- END MODAL -->
@endsection