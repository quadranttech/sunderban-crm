@extends('layouts.dashboard-mobile-header')
@section('content')
	
	<div class="invalid-message">
		<span class="message clear-fix">
			<i class="material-icons">info_outline</i>
			<span>{{ Lang::get('invoice.invoice-id-exists') }}</span>
		</span>
	</div>

	<div class="contents">	
		<div class="container">
			<div class="row">
				<div class="col s12">
					<div class="invoice-wrapper border">
						<div class="row" style="@if($inv_no=='') display:block @else display:none @endif">
							<div class="col s12">
								{{ Lang::get('invoice-estimate-common-template-lang.go-to-settigns-tab') }}
							</div>
						</div>
						<form id="invoice-form" name="invoice-form" action="{{ url('/') }}/save-invoice" method="POST" enctype="multipart/form-data" style="@if($inv_no=='') display:none @else display:block @endif">
							<div class="row">
								<div class="col s2">
									<div class="company-logo">
										<a href="javascript:void(0)"> <img src="{{ url('/') }}/uploads/company-logo/company-logo.png"> </a>
									</div>
								</div>
							</div>

							<div class="row">
								
								<div class="col s12">
									<div class="form-layout clear-fix">
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field select-field">
													<input type="hidden" name="_token" value="{{ csrf_token() }}">
													<input type="hidden" name="default_payment_term" value="{{ $default_payment_term }}">
										          	<select name="invoice_contact">
										          		<option value="select_contact">{{ Lang::get('common.select') }} {{ Lang::get('contact.Contact') }}</option>
										          		@if(!empty($contacts) && empty($company))
										          			@foreach($contacts as $key=>$value)
										          				<option value="{{ $value['id'] }}" data-contact="contact">{{ $value['first_name']}} {{ $value['last_name'] }}</option>
										          			@endforeach
									          			@elseif(!empty($company) && empty($contacts))
									          				@foreach($company as $key=>$value)
									          					<option value="{{ $value['id'] }}" data-contact="company">{{ $value['company_name'] }}</option>
									          				@endforeach
									          			@elseif(!empty($company) && !empty($contacts))
									          				@foreach($contacts as $key=>$value)
										          				<option value="{{ $value['id'] }}" data-contact="contact">{{ $value['first_name']}} {{ $value['last_name'] }}</option>
										          			@endforeach
										          			@foreach($company as $key=>$value)
									          					<option value="{{ $value['id'] }}" data-contact="company">{{ $value['company_name'] }}</option>
									          				@endforeach
									          			@else
									          			<option value="no_contact">{{ Lang::get('common.no') }} {{ Lang::get('contact.Contact') }}</option>
										          		@endif
										          	</select>
										        </div>
											</div>
										</div>
									</div>
								</div>
								<div class="col s12">
									<div class="form-layout clear-fix">
										<div class="form-layout-100">
											<div class="field-group">
										        <a href="{{ url('/') }}/contact/new" class="add-input">{{ Lang::get('common.create') }} {{ Lang::get('common.new') }} <span></span></a>
											</div>
										</div>
									</div>
								</div>
						
								<div class="col s12">
									<div class="form-layout form-layout-50x50 clear-fix">
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field">
										          	<input id="invoice-title" name="invoice_title" type="text" class="validate">
										          	<label for="invoice-title">{{ Lang::get('invoice.Invoice') }} {{ Lang::get('common.Title') }}</label>
										        </div>
											</div>
										</div>
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field">
										          	<input id="invoice-no" name="invoice_no" data-purpose="invoice" type="text" class="validate" value="{{ $inv_no }}">
										          	<label for="invoice-no" class="active">{{ Lang::get('invoice.Invoice') }} {{ Lang::get('common.no') }}</label>
										        </div>
											</div>
										</div>
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field">
										          	<input id="order-no" name="order_no" type="text" class="validate">
										          	<label for="order-no">{{ Lang::get('invoice.order-no') }}</label>
										        </div>
											</div>
										</div>
									</div>
								</div>
							
								<div class="col s12">
									<div class="form-layout form-layout-50x50 clear-fix">
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field date-field">
													<input type="text" id="date" name="invoice_date" class="form-control" placeholder="Invoice Date" value="@php echo date('Y-m-d'); @endphp">
													<span></span>
										        </div>
											</div>
										</div>
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field date-field date-settings">
													<input type="text" id="date2" name="due_date" class="form-control" placeholder="Invoice Due Date">
													<span></span>
													<a href="#modal5" class="settings"></a>
										        </div>
											</div>
										</div>
									</div>
								</div>
							
								<div class="col s12">
									<div class="form-layout clear-fix">
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field select-field">
										          	<select name="sales_person">
										          		@if(!empty($users))
										          			<option value="select_user">{{ Lang::get('estimate.select-sales-person') }}</option>
										          			@foreach($users as $key=>$value)
										          				<option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
										          			@endforeach
										          		@else
										          			<option value="no_user">{{ Lang::get('estimate.no-sales-person') }}</option>
										          		@endif
										          	</select>
										        </div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col s12">
									<div class="invoice-grid">
										<div class="content-table">
											<div class="content-table-header border">
												<div class="content-table-row">
													<div class="content-table-data">{{ Lang::get('estimate.item-details') }}</div>
													<div class="content-table-data">
														<div class="input-field">
															<select name="quantity">
																<option value="quantity">{{ Lang::get('estimate.quantity') }}</option>
																<option value="hr">hr</option>
																<option value="item">Item</option>
																<option value="hr-item">hr + Item</option>
															</select>
														</div>
													</div>
													<div class="content-table-data">{{ Lang::get('estimate.currency-rate') }}</div>
													<div class="content-table-data">{{ Lang::get('estimate.total') }}</div>
													<div class="content-table-data"></div>
												</div>
											</div>
											<div class="content-table-body invoice-item" id="sortable">
												<div class="content-table-row invoice-item-clone  ui-state-default" style="display:none">
													<div class="content-table-data item_details">
														<div class="input-field">
															<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
															<!-- <textarea name="item_details[]"></textarea> -->
															<input type="text" class="autocomplete" name="item_details[]">
															<span class="error-mark"></span>
															<span class="check-mark"></span>
														</div>
													</div>
													<div class="content-table-data invoice_quantity">
														<div class="input-field">
															<input type="number" class="invoice_quantity" name="invoice_quantity[]" min="1" max="20" value="">
															<span class="error-mark"></span>
															<span class="check-mark"></span>
														</div>
													</div>
													<div class="content-table-data currency">
														<div class="input-field">
															<select name="currency[]">
																@if(!empty($currency))
																	@foreach($currency as $key=>$value)
																		<option value="{{ $value }}">{{ $value }}</option>
																	@endforeach
																@else
																	<option value="USD">USD</option>
																@endif
															</select>
															<input type="text" class="tax" name="tax[]" value="">
															<span class="error-mark"></span>
															<span class="check-mark"></span>
														</div>
													</div>
													<div class="content-table-data total input-field">
														<div class="input-field">
															<input type="text" class="total" name="total[]" value="">
															<span class="error-mark"></span>
															<span class="check-mark"></span>
														</div>
													</div>
													<div class="content-table-data"><a href="javascript:void(0)" class="add-input add-new-item"> <span></span></a></div>
												</div>
												<div class="add-table-item">
													<a href="javascript:void(0);" class="add-input add-new-item">{{ Lang::get('estimate.add-line-item') }} <span></span></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col s12">
									<div class="text-areas">
										<div class="form-layout-100">
											<div class="text-area message">
												<input type="hidden" name="invoice_id" value="">
												<textarea onkeyup="textAreaAdjust(this)" name="customer_note" style="overflow:hidden" placeholder="Customer Note"></textarea>
												<span></span>
											</div>
										</div>
										<div class="form-layout-100">
											<div class="text-area message">
												<textarea onkeyup="textAreaAdjust(this)" name="admin_note" style="overflow:hidden" placeholder="Admin Note"></textarea>
												<span></span>
											</div>
										</div>
										<div class="form-layout-100">
											<div class="text-area settings">
												<textarea onkeyup="textAreaAdjust(this)" name="terms_and_conditions" style="overflow:hidden" placeholder="Terms and Conditions" value="@if(!empty($terms_and_conditions) && $terms_and_conditions['meta_value']!="") {{ $terms_and_conditions['meta_value'] }} @endif">@if(!empty($terms_and_conditions) && $terms_and_conditions['meta_value']!="") {{ $terms_and_conditions['meta_value'] }} @endif</textarea>
												<span></span>
											</div>
										</div>
									</div>
								</div>
								<div class="col s12">
									<div class="form-layout clear-fix">
										<input type="hidden" name="shipping-cost" value="0">
										<div class="form-layout-100">
											<div class="input-area clear-fix">
												<label>{{ Lang::get('estimate.adjustments') }} </label>
												<input type="text" name="adjustment" value="-5">
											</div>
										</div>
										<div class="form-layout-100">
											<div class="input-field">
									          	<select name="discount">
									          		@if(!empty($discount))
									          			<option value="select_discount">{{ Lang::get('common.select') }} {{ Lang::get('invoice-estimate-common-template-lang.discount') }}</option>
									          			@foreach($discount as $key=>$value)
									          				<option value="{{ $value['discount_amount'] }}" data-discount-type="{{ $value['discount_type'] }}" data-discount-id="{{ $value['id'] }}">{{ $value['discount_name'] }} {{ $value['discount_amount'] }}@if($value['discount_type']=="percentile") % @endif</option>
									          			@endforeach
									          		@else
									          			<option value="no_discount">{{ Lang::get('common.no') }} {{ Lang::get('invoice-estimate-common-template-lang.discount') }}</option>
									          		@endif
									          	</select>
									        </div>
										</div>
										<div class="form-layout-100">
											<div class="input-field">
									          	<select name="tax">
									          		@if(!empty($tax))
									          			<option value="select_tax">{{ Lang::get('common.select') }} {{ Lang::get('invoice-estimate-common-template-lang.tax') }}</option>
									          			@foreach($tax as $key=>$value)
									          				<option value="{{ $value['tax_amount'] }}" data-tax-type="{{ $value['tax_type'] }}" data-tax-id="{{ $value['id'] }}">{{ $value['tax_name'] }} {{ $value['tax_amount'] }}@if($value['tax_type']=="percentile") % @endif</option>
									          			@endforeach
									          		@else
									          			<option value="no_tax">{{ Lang::get('common.no') }} {{ Lang::get('invoice-estimate-common-template-lang.tax') }}</option>
									          		@endif
									          	</select>
									        </div>
										</div>
										<div class="form-layout-100">
											<div class="input-area clear-fix">
												<label>{{ Lang::get('estimate.total') }}: </label>
												<input type="text" name="invoice_total" value="27">
											</div>
										</div>
										<div class="form-layout-100">
											<div class="field-group">
												<input type="checkbox" class="filled-in" id="filled-in-box" />
												<label for="filled-in-box" class="part-pay">{{ Lang::get('estimate.allow-part-pay') }}</label>
											</div>
										</div>
										<div class="partpay-toggle clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field">
														<select name="part_pay_type">
															<option value="percentile">{{ Lang::get('estimate.percentile') }}</option>
															<option value="fixed">{{ Lang::get('estimate.fixed') }}</option>
														</select>
													</div>
												</div>
											</div>
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field">
														<input type="text" name="part_pay_value" value="50%">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col s12">
									<div class="buttons clear-fix right-align">
										<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>
										<button class="waves-effect waves-light btn btn-green save-invoice save" data-name="Save" data-status="draft">{{ Lang::get('common.save') }}</button>
										<button class="waves-effect waves-light btn btn-green save-invoice save" data-name="Save &amp; Send" data-status="send">{{ Lang::get('invoice-estimate-common-template-lang.save-and-send') }}</button>
										<span class="invoice-error"></span>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="modal5" class="modal modal-fixed-footer select-terms">
		<button class="modal-close default-payment-terms-close"></button>
		<div class="modal-wrapper">
			<div class="modal-content">
				<form>
					<div class="row">
						<div class="col s12">
							<div class="form-layout form-layout-50x50 clear-fix">
								<div class="form-layout-100">
									<div class="field-group">
										<h3>{{ Lang::get('invoice.select-default-payment-terms') }}</h3>
										<div class="input-field">
											<select name="default_payment_term_select">
												<option value="net-15" @if($default_payment_term=='15') selected @endif>Net 15</option>
												<option value="net-30" @if($default_payment_term=='30') selected @endif>Net 30</option>
												<option value="net-45">Net 45</option>
												<option value="net-60">Net 60</option>
												<option value="due-on-receive">Due on receive</option>
												<option value="due-at-end-of-month">Due at the end of the month</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="buttons center">
					<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>
					<button class="waves-effect waves-light btn btn-green default-payment-terms save">{{ Lang::get('common.save') }}</button>
				</div>
				<span class="default-payment-terms-error"></span>
			</div>
		</div>
	</div>

	<div id="show-invoice-template-preview" class="modal modal-fixed-footer">
		<button class="modal-close"></button>
		<div class="modal-wrapper">
			<div class="modal-content">
				<div class="row">
					<div class="col s12">
						<h3>{{ Lang::get('invoice.send-invoice') }}</h3>
					</div><!--END COL -->
				</div><!-- END ROW -->
				<form id="send-invoice">
					<div class="row">
						<div class="col s12">
							<div class="form-layout form-layout-50x50 clear-fix">
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field">
											<input type="hidden" name="inv_id" value="">
											<input type="hidden" name="invoice_title" value="">
											<input type="hidden" name="inv_no" value="">
								          	<input id="invoice-subject-title" name="invoice-subject-title" type="text" class="validate" value="">
								          	<label for="invoice-subject-title" class="">{{ Lang::get('invoice.invoice-subject') }}</label>
								        </div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM  LAYOUT -->
								<div class="form-layout-100">
									<div class="field-group">
										<div class="input-field">
								          	<input id="message-to-customer" name="message-to-customer" type="text" class="validate" value="">
								          	<label for="message-to-customer" class="">{{ Lang::get('estimate.message-to-customer') }}</label>
								        </div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM  LAYOUT -->
							</div><!-- END FIELD LAYOUT -->
						</div><!-- END COL -->
					</div><!-- END ROW -->
					<div class="row">
						<div class="col s12">
							<div class="form-layout form-layout-50x50 clear-fix">
								<div class="form-layout-100">
									<div class="field-group">
										<div class="invoice-template-preview">
											
										</div><!-- END INPUT-FIELD -->
									</div><!-- END FIELD GROUP -->
								</div><!-- END FORM LAYOUT -->
							</div><!-- END FORM LAYOUT -->
						</div><!-- END COL -->
					</div><!-- END ROW -->
				</form><!-- END FORM -->
				<div class="buttons center">
					<button class="waves-effect waves-light btn btn-dark-orange cancel-modal cancel-invoice">{{ Lang::get('common.cancel') }}</button>
					<button class="waves-effect waves-light btn btn-green send-invoice">{{ Lang::get('common.save') }}</button>
				</div><!-- END BUTTONS -->
			</div><!-- END MODAL CONTENT -->
		</div>
	</div><!-- END MODAL -->
	<div class="modal-overlay" id="materialize-modal-overlay-1" style="z-index: 1002; display: none; opacity: 0.8;"></div>

@endsection