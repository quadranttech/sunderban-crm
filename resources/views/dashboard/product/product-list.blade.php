@extends('layouts.dashboard-header')
@section('content')
<div class="bottom-header">
  <div class="had-container">
    <div class="row">
      <div class="col s8"> </div>
      <div class="col s4 right-align"> 
        <a href="{{ url('/') }}/create-product" class="waves-effect waves-light btn blue">Add Product</a>
      </div>
      <!-- END COL -->
    </div>
    <!-- END ROW -->
  </div>
  <!-- END CONTAINER -->
</div>
<div class="row">
    <div class="col s12">
      <div class="contents">
        <div class="had-container">
          <div class="contacts-grid white border">
            <div class="row">
              <div class="col s8">
                <h3>Products <span class="total-contact">(@php echo sizeof($product_list); @endphp)</span></h3>
              </div>
              <!-- END COL -->
            </div>

                <div id="contacts-list" class="product-table">

                  <div class="content-table">
                    <div class="content-table-header border">
                      <div class="content-table-row">
                        <div class="content-table-data">Product Name</div>
                        <div class="content-table-data description">Product Description</div>
                        <div class="content-table-data">Product Price</div>
                        <div class="content-table-data"></div>
                      </div>
                    </div>

                    @php
                      $count=1;
                    @endphp
                    <div class="content-table-body">
                      <input type="hidden" name="status_message" value="{{ session('status') }}">
                      @if(!empty($product_list))
                        @foreach ($product_list as $value)
                          <div class="content-table-row content-table-row-sort">
                            <div class="content-table-data">{{ $value['name'] }}</div>
                            <div class="content-table-data description">{{ $value['description'] }}</div>
                            <div class="content-table-data">{{ $value['price'] }}</div>
                            <div class="content-table-data contactMore">
                              <div class="more-actions"><a href="{{url('/edit-product')}}/{{ $value['id'] }}" class="edit-icon edit-contact">Edit</a></div>
                            </div>
                          </div>
                          <!-- END CONTENT TABLE ROW -->
                        @endforeach
                      @else
                      <div class="content-table-row">
                            <div class="content-table-data">Website</div>
                            <div class="content-table-data">Website Description</div>
                            <div class="content-table-data">20.00</div>
                            <div class="content-table-data contactMore">
                              <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
                            </div>
                          </div>
                          <div class="content-table-row">
                            <div class="content-table-data">Website</div>
                            <div class="content-table-data">Website Description</div>
                            <div class="content-table-data">20.00</div>
                            <div class="content-table-data contactMore">
                              <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
                            </div>
                          </div>
                          <div class="content-table-row">
                            <div class="content-table-data">Website</div>
                            <div class="content-table-data">Website Description</div>
                            <div class="content-table-data">20.00</div>
                            <div class="content-table-data contactMore">
                              <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
                            </div>
                          </div>
                          <div class="content-table-row">
                            <div class="content-table-data">Website</div>
                            <div class="content-table-data">Website Description</div>
                            <div class="content-table-data">20.00</div>
                            <div class="content-table-data contactMore">
                              <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
                            </div>
                          </div>
                      @endif
                    </div>
                  </div>

                </div>
               
          </div>
          <!-- END CONTACT GRID-->
        </div>
        <!-- END CONTAINER -->
      </div>
    </div>
</div>
<!-- END CONTENTS -->
@endsection