@extends('layouts.dashboard-header')
@section('content')
<div class="bottom-header">
    <div class="had-container"> 
      	<div class="row">
      		<div class="col s8">
      		</div>
        	<div class="col s4 right-align">
        	</div> <!-- END COL -->
      	</div> <!-- END ROW -->
    </div> <!-- END CONTAINER -->
</div>  <!-- END BOTTOM HEADER -->
<div class="contents">
	<div class="had-container">
		<div class="row">
			<div class="col s12">
				<div class="contacts border">
					<div class="contact-screen add-contact" id="contact-screen">
						<form action="{{url('/save-product')}}" id="contact-form" method="POST">
							<div class="information">
								<div class="information-header">
									<h3>Add Product</h3>
								</div> <!-- END INFORMATION HEADER -->
								<div class="row">
									<div class="col s6">
										<div class="form-layout form-layout-100 clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field select-field">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
													    <select name="type">
															<option value="budget">Budget</option>
															<option value="premium">Premium</option>
															<option value="add-on">Add On</option>
														</select>
														<label>Product Type</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>


								<div class="row">
									<div class="col s6">
										<div class="form-layout form-layout-100 clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field">
													    <input name="name" type="text" class="validate">
														<span class="error-mark"></span>
											          	<span class="check-mark"></span>
											          	<label for="name">Product Name</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s6">
										<div class="form-layout form-layout-100 clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
													    <input name="description" type="text" class="validate">
														<span class="error-mark"></span>
											          	<span class="check-mark"></span>
											          	<label for="description">Product Description</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s6">
										<div class="form-layout form-layout-100 clear-fix">
											<div class="form-layout-50">
												<div class="field-group">
													<div class="input-field">
											          	<div class="select-wrapper">
											          		<select name="price_type" class="initialized">
															<option value="percentile">Percentile</option>
															<option value="fixed">Fixed</option>
															</select>
														</div>
											        </div>
												</div>
											</div>
											<div class="form-layout-50">
												<div class="field-group">
													<div class="input-field">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
													    <input name="price" type="text" class="validate">
														<span class="error-mark"></span>
											          	<span class="check-mark"></span>
											          	<label for="price">Per Day Price</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s6">
										<div class="form-layout form-layout-100 clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
													    <input name="per_person_price" type="text" class="validate">
														<span class="error-mark"></span>
											          	<span class="check-mark"></span>
											          	<label for="price">Per person Price</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s6">
										<div class="buttons center">
											<button class="waves-effect waves-light btn btn-dark-orange back-to-product-grid">{{ Lang::get('common.cancel') }}</button>
											<button class="waves-effect waves-light btn btn-green create-product save">{{ Lang::get('common.save') }}</button>
										</div> <!-- END BUTTONS -->
									</div>
								</div>
										</div> <!-- END FORM LAYOUT -->
									</div> <!-- END COL -->
								</div> <!-- END ROW -->
							</div>
						</form>
					</div>  <!-- END PRODUCT SCREEN -->
				</div>  <!-- END CONTACTS -->
			</div> <!-- END COL -->
		</div> <!-- END ROW -->
	</div> <!-- END CONTAINER -->
</div>  <!-- END CONTENTS -->
@endsection