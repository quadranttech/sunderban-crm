@extends('layouts.dashboard-header')
@section('content')
<div class="bottom-header">
    <div class="had-container"> 
      	<div class="row">
      		<div class="col s8">
      		</div>
        	<div class="col s4 right-align">
        	</div> <!-- END COL -->
      	</div> <!-- END ROW -->
    </div> <!-- END CONTAINER -->
</div>  <!-- END BOTTOM HEADER -->
<div class="contents">
	<div class="had-container">
		<div class="row">
			<div class="col s12">
				<div class="contacts border">
					<div class="contact-screen" id="contact-screen">
						<form action="{{url('/edit-product/')}}/{{ $product['id'] }}" id="contact-form" method="POST">
							<div class="information">
								<div class="information-header">
									<h3>Add Product</h3>
								</div> <!-- END INFORMATION HEADER -->
								<div class="row">
									<div class="col s12">
										<div class="form-layout form-layout-20x40x40 clear-fix">
											<div class="form-layout-40">
												<div class="field-group">
													<div class="input-field select-field">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
													    @if($product['type'] != "main")
													    	<select name="type">
														    	@if($product['product_meta'] != "")
														    	{
														    		<option value="budget" @if(array_key_exists('budget',$product_meta_array)) description="{{ $product_meta_array['budget']['description'] }}" room-fare="{{ $product_meta_array['budget']['room_fare'] }}" per-person-price="{{ $product_meta_array['budget']['per_person_price'] }}" selected @endif>Budget</option>
																	<option value="premium" @if(array_key_exists('premium',$product_meta_array)) description="{{ $product_meta_array['premium']['description'] }}" room-fare="{{ $product_meta_array['premium']['room_fare'] }}" per-person-price="{{ $product_meta_array['premium']['per_person_price'] }}" @endif @if(!array_key_exists('budget',$product_meta_array)) selected @endif>Premium</option>
														    	}
														    	@else
														    	{
														    		<option value="add-on" description="{{ $product['description'] }}" room-fare="{{ $product['price'] }}" per-person-price="{{ $product['per_person_price'] }}" selected>Add On</option>
														    	}
														    	@endif
															</select>
															<label>Product Type</label>
														@else
															<input type="hidden" name="type" value="main">
														@endif
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12">
										<div class="form-layout form-layout-20x40x40 clear-fix">
											<div class="form-layout-40">
												<div class="field-group">
													<div class="input-field">
														<input type="hidden" name="id" value="{{ $product['id'] }}">
													    <input name="name" type="text" class="validate" value="{{ $product['name'] }}">
														<span class="error-mark"></span>
											          	<span class="check-mark"></span>
											          	<label for="name">Product Name</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12">
										<div class="form-layout form-layout-20x40x40 clear-fix">
											<div class="form-layout-40">
												<div class="field-group">
													<div class="input-field">
														@php
															$description = "";
														if($product['product_meta'] == "")
														{
															$description = $product['description'];
														}
														else
														{
															if(array_key_exists('budget',$product_meta_array))
															{
																$description = $product_meta_array['budget']['description'];
															}
															else
															{
																$description = $product_meta_array['premium']['description'];
															}
														}
														@endphp
													    <input name="description" type="text" class="validate" value="{{ $product['description'] }}">
														<span class="error-mark"></span>
											          	<span class="check-mark"></span>
											          	<label for="description">Product Description</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12">
										<div class="form-layout form-layout-20x40x40 clear-fix">
											<div class="form-layout-30" @if($product['type'] == "add-on")style="display:block" @else style="display:none"@endif>
												<div class="field-group">
													<div class="input-field">
											          	<div class="select-wrapper">
											          		<select name="price_type" class="initialized">
											          		@if($product['type'] == "add-on")
																<option value="percentile" @if($product['price_type'] == "percentile") selected @endif>Percentile</option>
																<option value="fixed" @if($product['price_type'] == "fixed") selected @endif>Fixed</option>
															@else
																<option value="fixed">Fixed</option>
															@endif
															</select>
														</div>
											        </div>
												</div>
											</div>
											<div class="@if($product['type'] != "add-on")form-layout-40 @else form-layout-30 @endif">
												<div class="field-group">
													<div class="input-field">
														@php
															$room_fare = "";
														if($product['product_meta'] == "")
														{
															$room_fare = $product['price'];
														}
														else
														{
															if(array_key_exists('budget',$product_meta_array))
															{
																$room_fare = $product_meta_array['budget']['room_fare'];
															}
															else
															{
																$room_fare = $product_meta_array['premium']['room_fare'];
															}
														}
														@endphp
													    <input name="price" type="number" class="validate" value="{{ $product['price'] }}">
														<span class="error-mark"></span>
											          	<span class="check-mark"></span>
											          	<label for="price">Per Day Price</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12">
										<div class="form-layout form-layout-20x40x40 clear-fix">
											<div class="form-layout-40">
												<div class="field-group">
													<div class="input-field">
														@php
															$per_person_price = "";
														if($product['product_meta'] == "")
														{
															$per_person_price = $product['per_person_price'];
														}
														else
														{
															if(array_key_exists('budget',$product_meta_array))
															{
																$per_person_price = $product_meta_array['budget']['per_person_price'];
															}
															else
															{
																$per_person_price = $product_meta_array['premium']['per_person_price'];
															}
														}
														@endphp
													    <input name="per_person_price" type="number" class="validate" value="{{ $product['per_person_price'] }}">
														<span class="error-mark"></span>
											          	<span class="check-mark"></span>
											          	<label for="price">Per person Price</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="buttons center">
									<button class="waves-effect waves-light btn btn-dark-orange back-to-product-grid">{{ Lang::get('common.cancel') }}</button>
									<button class="waves-effect waves-light btn btn-green create-product save">{{ Lang::get('common.save') }}</button>
								</div> <!-- END BUTTONS -->
										</div> <!-- END FORM LAYOUT -->
									</div> <!-- END COL -->
								</div> <!-- END ROW -->
							</div>
						</form>
					</div>  <!-- END PRODUCT SCREEN -->
				</div>  <!-- END CONTACTS -->
			</div> <!-- END COL -->
		</div> <!-- END ROW -->
	</div> <!-- END CONTAINER -->
</div>  <!-- END CONTENTS -->
@endsection