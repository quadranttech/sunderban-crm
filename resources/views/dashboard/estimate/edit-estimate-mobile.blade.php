@extends('layouts.dashboard-mobile-header')
@section('content')
<div class="contents">
	<div class="container">
		<div class="row">
			<div class="col s12">
				<div class="invoice-wrapper border">
					<div class="row" style="@if($est_no=='') display:block @else display:none @endif">
						<div class="col s12">
							{{ Lang::get('invoice-estimate-common-template-lang.go-to-settigns-tab') }}
						</div>
					</div>
					<form id="invoice-form" name="invoice-form" action="{{ url('/') }}/save-invoice" method="POST" enctype="multipart/form-data" style="@if($est_no=='') display:none @else display:block @endif">
						<div class="row">
							<div class="col s2">
								<div class="company-logo">
									<a href="javascript:void(0);"> <img src="{{ url('/') }}/uploads/company-logo/company-logo.png"> </a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="form-layout clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field select-field">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="default_payment_term" value="">
												<input type="hidden" name="invoice_title" value="{{ $invoice['estimate_title'] }}">
									          	<select name="invoice_contact" @if($invoice['status']=="send") disabled @endif>
									          		<option value="select_contact">{{ Lang::get('common.select') }} {{ Lang::get('contact.Contact') }}</option>
									          		@if(!empty($contacts) && empty($company))
									          			@foreach($contacts as $key=>$value)
									          				<option value="{{ $value['id'] }}" data-contact="contact" @if($invoice['contact']==$value['id']) selected @endif>{{ $value['first_name']}} {{ $value['last_name'] }}</option>
									          			@endforeach
								          			@elseif(!empty($company) && empty($contacts))
								          				@foreach($company as $key=>$value)
								          					<option value="{{ $value['id'] }}" data-contact="company" @if($invoice['contact']==$value['id']) selected @endif>{{ $value['company_name'] }}</option>
								          				@endforeach
								          			@elseif(!empty($company) && !empty($contacts))
								          				@foreach($contacts as $key=>$value)
									          				<option value="{{ $value['id'] }}" data-contact="contact" @if($invoice['contact']==$value['id'] && $invoice['contact_type']=="contact") selected @endif>{{ $value['first_name']}} {{ $value['last_name'] }}</option>
									          			@endforeach
									          			@foreach($company as $key=>$value)
								          					<option value="{{ $value['id'] }}" data-contact="company" @if($invoice['contact']==$value['id'] && $invoice['contact_type']=="company") selected @endif>{{ $value['company_name'] }}</option>
								          				@endforeach
								          			@else
								          			<option value="no_contact">{{ Lang::get('common.no') }} {{ Lang::get('contact.Contact') }}</option>
									          		@endif
									          	</select>
									        </div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s12">
								<div class="form-layout clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
									        <a href="{{ url('/') }}/contact/new" class="add-input">{{ Lang::get('common.create') }} {{ Lang::get('common.new') }} <span></span></a>
										</div>
									</div>
								</div>
							</div>
							<div class="col s12">
								<div class="form-layout form-layout-50x50 clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field">
									          	<input id="invoice-no" name="invoice_no" type="text" class="validate" value="{{ $invoice['est_no'] }}" @if($invoice['status']=="send") disabled @endif>
									          	<label for="invoice-no">{{ Lang::get('estimate.Estimate') }} {{ Lang::get('common.no') }}</label>
									        </div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s12">
								<div class="form-layout form-layout-50x50 clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field date-field">
												<input type="text" id="date" name="invoice_date" class="form-control" placeholder="Invoice Date" value="{{ $invoice['est_date'] }}" @if($invoice['status']=="send") disabled @endif>
												<span></span>
												<label for="invoice-no" class="active">{{ Lang::get('estimate.Estimate') }} {{ Lang::get('common.date') }}</label>
									        </div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s12">
								<div class="form-layout clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field select-field">
									          	<select name="sales_person" @if($invoice['status']=="send") disabled @endif>
									          		@if(!empty($users))
									          			<option value="select_user">{{ Lang::get('estimate.select-sales-person') }}</option>
									          			@foreach($users as $key=>$value)
									          				<option value="{{ $value['id'] }}" @if($invoice['sales_person']==$value['id']) selected @endif>{{ $value['name'] }}</option>
									          			@endforeach
									          		@else
									          			<option value="no_user">{{ Lang::get('estimate.no-sales-person') }}</option>
									          		@endif
									          	</select>
									        </div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="invoice-grid">
									<div class="content-table">
										<div class="content-table-header border">
											<div class="content-table-row">
												<div class="content-table-data">{{ Lang::get('estimate.item-details') }}</div>
												<div class="content-table-data">
													<div class="input-field">
														<select name="quantity">
															@if(!empty($invoice_items))
																<option value="quantity" @if($invoice_items[0]['quantity_type']=="quantity") selected @endif>Quantity</option>
																<option value="hr" @if($invoice_items[0]['quantity_type']=="hr") selected @endif>hr</option>
																<option value="item" @if($invoice_items[0]['quantity_type']=="item") selected @endif>Item</option>
																<option value="hr-item" @if($invoice_items[0]['quantity_type']=="hr-item") selected @endif>hr + Item</option>
															@endif
														</select>
													</div>
												</div>
												<div class="content-table-data">{{ Lang::get('estimate.currency-rate') }}</div>
												<div class="content-table-data">{{ Lang::get('estimate.total') }}</div>
											</div>
										</div>
										<div class="content-table-body invoice-item" id="sortable">
											<div class="content-table-row invoice-item-clone ui-state-default" style="display:none">
												<div class="content-table-data item_details input-field"><input class="autocomplete" name="item_details[]"><span class="error-mark"></span><span class="check-mark"></span></div>
												<div class="content-table-data invoice_quantity input-field">
													<input type="number" class="invoice_quantity" name="invoice_quantity[]" min="1" max="20" value="">
													<span class="error-mark"></span>
													<span class="check-mark"></span>
												</div>
												<div class="content-table-data currency">
													<div class="input-field">
														<select name="currency[]">
															@if(!empty($currency))
																	@foreach($currency as $key=>$value)
																		<option value="{{ $value }}">{{ $value }}</option>
																	@endforeach
																@else
																	<option value="USD">USD</option>
																@endif
														</select>
														<input type="text" class="tax" name="tax[]" value="">
														<span class="error-mark"></span>
														<span class="check-mark"></span>
													</div>
												</div>
												<!-- <div class="content-table-data"><input type="" name="" value=""></div>
												<div class="content-table-data"><input type="" name="" value=""></div> -->
												<div class="content-table-data total input-field"><input type="text" class="total" name="total[]" value=""><span class="error-mark"></span><span class="check-mark"></span></div>
											</div>
											@if(!empty($invoice_items))
												@foreach($invoice_items as $key=>$value)
													<div class="content-table-row line-item @if($invoice['status']) ui-state-default @endif">
														<div class="content-table-data item_details input-field"><input class="autocomplete" name="item_details[]" value="{{ $value['item_details'] }}" @if($invoice['status']=="send") disabled @endif><span class="error-mark"></span><span class="check-mark"></span></div>
														<div class="content-table-data invoice_quantity input-field">
															<input type="number" class="invoice_quantity" name="invoice_quantity[]" min="1" max="10" value="{{ $value['quantity'] }}" @if($invoice['status']=="send") disabled @endif>
															<span class="error-mark"></span>
															<span class="check-mark"></span>
														</div>
														<div class="content-table-data currency">
															<div class="input-field">
																<select name="currency[]" @if($invoice['status']=="send") disabled @endif>
																	@if(!empty($currency))
																		@foreach($currency as $currency_key=>$currency_value)
																			<option value="{{ $currency_value }}" @if($value['currency']==$currency_value) selected @endif>{{ $currency_value }}</option>
																		@endforeach
																	@else
																		<option value="USD">USD</option>
																	@endif
																</select>
																<input type="text" class="tax" name="tax[]" value="{{ $value['rate'] }}" @if($invoice['status']=="send") disabled @endif>
																<span class="error-mark"></span>
																<span class="check-mark"></span>
															</div>
														</div>
														<div class="content-table-data total input-field"><input type="text" class="total" name="total[]" value="{{ $value['total'] }}" @if($invoice['status']=="send") disabled @endif><span class="error-mark"></span><span class="check-mark"></span></div>
														<div style="display: none;"><input type="hidden" name="item_id_array[]" value="{{ $value['id'] }}"></div>
														<div class="content-table-data"><a href="javascript:void(0);" class="add-input add-new-item"> <span></span></a></div>
													</div>
												@endforeach
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="text-areas">
									<div class="form-layout-100">
										<div class="text-area message">
											<input type="hidden" name="estimate_id" value="{{ $invoice['id'] }}">
											<textarea onkeyup="textAreaAdjust(this)" name="customer_note" style="overflow:hidden" placeholder="Customer Note" value="@if(!empty($estimate_meta)) {{ $estimate_meta['customer_note'] }}@endif">{{ $estimate_meta['customer_note'] }}</textarea>
											<span></span>
										</div>
									</div>
									<div class="form-layout-100">
										<div class="text-area message">
											<textarea onkeyup="textAreaAdjust(this)" name="admin_note" style="overflow:hidden" placeholder="Admin Note" value="@if(!empty($estimate_meta)) {{ $estimate_meta['admin_note'] }} @endif">{{ $estimate_meta['admin_note'] }}</textarea>
											<span></span>
										</div>
									</div>
									<div class="form-layout-100">
										<div class="text-area settings">
											<textarea onkeyup="textAreaAdjust(this)" name="terms_and_conditions" style="overflow:hidden" placeholder="Terms and Conditions" value="@if(!empty($terms_and_conditions) && $terms_and_conditions['meta_value']!="") {{ $terms_and_conditions['meta_value'] }} @endif">@if(!empty($terms_and_conditions) && $terms_and_conditions['meta_value']!="") {{ $terms_and_conditions['meta_value'] }} @endif</textarea>
											<span></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col s12">
								<div class="form-layout clear-fix">
									<input type="hidden" name="shipping-cost" value="0">
									<div class="form-layout-100">
										<div class="input-area clear-fix">
											<label>{{ Lang::get('estimate.adjustments') }} </label>
											<input type="text" name="adjustment" value="@if(!empty($estimate_meta)) {{ $estimate_meta['adjustment'] }} @endif" @if($invoice['status']=="send") disabled @endif>
										</div>
									</div>
									<div class="form-layout-100">
										<div class="input-field">
								          	<select name="discount" @if($invoice['status']=="send") disabled @endif>
								          		@if(!empty($discount))
								          			<option value="select_discount">{{ Lang::get('common.select') }} {{ Lang::get('invoice-estimate-common-template-lang.discount') }}</option>
								          			@foreach($discount as $key=>$value)
								          				<option value="{{ $value['discount_amount'] }}" data-discount-type="{{ $value['discount_type'] }}" data-discount-id="{{ $value['id'] }}" @if($estimate_meta['discount_id']==$value['id']) selected @endif>{{ $value['discount_amount'] }}@if($value['discount_type']=="percentile") % @endif</option>
								          			@endforeach
								          		@else
								          			<option value="no_discount">{{ Lang::get('common.no') }} {{ Lang::get('invoice-estimate-common-template-lang.discount') }}</option>
								          		@endif
								          	</select>
								        </div>
									</div>
									<div class="form-layout-100">
										<div class="input-field">
								          	<select name="tax" @if($invoice['status']=="send") disabled @endif>
								          		@if(!empty($tax))
								          			<option value="select_tax">{{ Lang::get('common.select') }} {{ Lang::get('invoice-estimate-common-template-lang.tax') }}</option>
								          			@foreach($tax as $key=>$value)
								          				<option value="{{ $value['tax_amount'] }}" data-tax-type="{{ $value['tax_type'] }}" data-tax-id="{{ $value['id'] }}" @if($estimate_meta['tax_id']==$value['id']) selected @endif>{{ $value['tax_name'] }} {{ $value['tax_amount'] }}@if($value['tax_type']=="percentile") % @endif</option>
								          			@endforeach
								          		@else
								          			<option value="no_tax">{{ Lang::get('common.no') }} {{ Lang::get('invoice-estimate-common-template-lang.tax') }}</option>
								          		@endif
								          	</select>
								        </div>
									</div>
									<div class="form-layout-100">
										<div class="input-area clear-fix">
											<label>{{ Lang::get('estimate.total') }}: </label>
											<input type="text" name="invoice_total" value="@if(!empty($estimate_meta)) {{ $estimate_meta['inv_total'] }} @endif" @if($invoice['status']=="send") disabled @endif>
										</div>
									</div>
									<div class="form-layout-100">
										<div class="field-group">
											<input type="checkbox" class="filled-in" id="filled-in-box" @if(!empty($estimate_meta) && ($estimate_meta['part_pay']!="no_part_pay")) checked="checked" @endif @if($invoice['status']=="send") disabled @endif />
											<label for="filled-in-box" class="part-pay">{{ Lang::get('estimate.allow-part-pay') }}</label>
										</div>
									</div>
									<div class="partpay-toggle clear-fix">
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field">
													<select name="part_pay_type" @if($invoice['status']=="send") disabled @endif>
														<option value="percentile" @if(!empty($estimate_meta) && ($estimate_meta['part_pay']!="no_part_pay") && ($estimate_meta['part_pay']=="percentile")) selected @endif>{{ Lang::get('estimate.percentile') }}</option>
														<option value="fixed" @if(!empty($estimate_meta) && ($estimate_meta['part_pay']!="no_part_pay") && ($estimate_meta['part_pay']=="fixed")) selected @endif>{{ Lang::get('estimate.fixed') }}</option>
													</select>
												</div>
											</div>
										</div>
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field">
													@php
														if(!empty($estimate_meta) && ($estimate_meta['part_pay']!="no_part_pay"))
															$part_pay=$estimate_meta['inv_total']-$estimate_meta['due'];
														else
															$part_pay="";
													@endphp
													<input type="text" name="part_pay_value" value="{{ $part_pay }}" @if($invoice['status']=="send") disabled @endif>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="buttons clear-fix right-align">
									<button class="waves-effect waves-light btn btn-dark-orange">{{ Lang::get('common.cancel') }}</button>
									@if($permission['estimate']['edit']=="on")<button class="waves-effect waves-light btn btn-green edit-estimate save" data-name="Save" data-status="draft">{{ Lang::get('common.save') }}</button>@endif
									@if($permission['estimate']['edit']=="on")<button class="waves-effect waves-light btn btn-green edit-estimate save" data-name="Save &amp; Send" data-status="send">{{ Lang::get('invoice-estimate-common-template-lang.save-and-send') }}</button>@endif
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="show-invoice-template-preview" class="modal modal-fixed-footer">
	<button class="modal-close"></button>
	<div class="modal-wrapper">
		<div class="modal-content">
			<div class="row">
				<div class="col s12">
					<h3>{{ Lang::get('estimate.send-estimate') }}</h3>
				</div><!--END COL -->
			</div><!-- END ROW -->
			<form id="send-invoice">
				<div class="row">
					<div class="col s12">
						<div class="form-layout form-layout-50x50 clear-fix">
							<div class="form-layout-100">
								<div class="field-group">
									<div class="input-field">
										<input type="hidden" name="est_id" value="">
										<input type="hidden" name="est_no" value="">
							          	<input id="invoice-subject-title" name="invoice-subject-title" type="text" class="validate" value="{{ $invoice['estimate_title'] }}">
							          	<label for="invoice-subject-title" class="">{{ Lang::get('estimate.estimate-subject') }}</label>
							        </div><!-- END INPUT-FIELD -->
								</div><!-- END FIELD GROUP -->
							</div><!-- END FORM  LAYOUT -->
							<div class="form-layout-100">
								<div class="field-group">
									<div class="input-field">
							          	<input id="message-to-customer" name="message-to-customer" type="text" class="validate" value="">
							          	<label for="message-to-customer" class="">{{ Lang::get('estimate.message-to-customer') }}</label>
							        </div><!-- END INPUT-FIELD -->
								</div><!-- END FIELD GROUP -->
							</div><!-- END FORM  LAYOUT -->
						</div><!-- END FIELD LAYOUT -->
					</div><!-- END COL -->
				</div><!-- END ROW -->
				<div class="row">
					<div class="col s12">
						<div class="form-layout form-layout-50x50 clear-fix">
							<div class="form-layout-100">
								<div class="field-group">
									<div class="invoice-template-preview">
									</div><!-- END INPUT-FIELD -->
								</div><!-- END FIELD GROUP -->
							</div><!-- END FORM LAYOUT -->
						</div><!-- END FORM LAYOUT -->
					</div><!-- END COL -->
				</div><!-- END ROW -->
			</form><!-- END FORM -->
			<div class="buttons center">
				<button class="waves-effect waves-light btn btn-dark-orange cancel-modal">{{ Lang::get('common.cancel') }}</button>
				<button class="waves-effect waves-light btn btn-green send-estimate">{{ Lang::get('invoice-estimate-common-template-lang.send') }}</button>
			</div><!-- END BUTTONS -->
		</div><!-- END MODAL CONTENT -->
	</div>
</div><!-- END MODAL -->
<div class="modal-overlay" id="materialize-modal-overlay-1" style="z-index: 1002; display: none; opacity: 0.8;"></div>
@endsection