@extends('layouts.dashboard-mobile-header')
@section('content')
	<div class="bottom-header">
	    <div class="container"> 
	      	<div class="row">
	        	<div class="col s12 left-align">
	        		@if($permission['estimate']['add']=="on")<a href="{{ url('/') }}/estimate" class="waves-effect waves-light btn blue" id="">{{ Lang::get('common.create') }} {{ Lang::get('estimate.Estimate') }}</a>@endif
	        	</div> <!-- END COL -->
	      	</div> <!-- END ROW -->
	    </div> <!-- END CONTAINER -->
	</div>  <!-- END BOTTOM HEADER -->
	<div class="contents">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<div class="contacts-grid white border">
						<div class="quotes-wrapper">
							<form>
								<div class="row">
									<div class="col s12">
										<h3>{{ Lang::get('estimate.Estimates') }} <span class="total-contact">({{ $estimates_size}})</span></h3>
									</div>
								</div> <!-- END ROW -->
								<div class="row">
									<div class="col s12">
										<div class="content-table-wrapper">
											<div class="quotes-grid">
												<div id="estimate-list">
													<div class="list-search">
														<button type="button" class="search-icon"></button>
														<input class="search" placeholder="Search" />
													</div>
													<div class="content-table">
														<div class="content-table-header border">
															<div class="content-table-row">
																<div class="content-table-data sortable" data-sort="id" data-value="asc" data-clone="content-table-row">#Id</div>
																<div class="content-table-data">{{ Lang::get('contact.Contact') }}</div>
																<div class="content-table-data sortable" data-sort="date" data-value="desc" data-clone="content-table-row">{{ Lang::get('common.date') }} {{ Lang::get('common.created') }}</div>
																<div class="content-table-data sortable" data-sort="amount" data-value="desc" data-clone="content-table-row">{{ Lang::get('common.Amount') }}</div>
																<div class="content-table-data">{{ Lang::get('estimate.status') }}</div>
																<div class="content-table-data"></div>
															</div>
														</div> <!-- END CONTENT TABLE HEADER -->
														<div class="content-table-body list @if(empty($estimates)) demo-content @endif">
															@if(!empty($estimates))
																@foreach($estimates as $key=>$value)
																	<div class="content-table-row content-table-row-sort">
																		<div class="content-table-data est_no" data-id="{{ $key }}">
																			<span>{{ $value['est_no'] }}</span>
																		</div>
																		<div class="content-table-data contact">
																			<span>{{ $value['contact'] }}</span>
																		</div>
																		<div class="content-table-data created_at" data-date="{{ $value['date_created'] }}">
																			<span>{{ $value['date_created'] }}</span>
																		</div>
																		<div class="content-table-data value" data-amount="{{ $value['value'] }}">
																			<span>{{ $value['currency_symbol'] }} {{ $value['value'] }}</span>
																		</div>
																		<div class="content-table-data status">
																			<span class="chips @if($value['status']=='declined') red @elseif($value['status']=='draft') lime @else green @endif">@if($value['status']=="send") sent @else{{ $value['status'] }} @endif</span>
																		</div>
																		<div class="content-table-data">
		                                                                    <div class="more-actions">@if($permission['estimate']['edit']=="on" || $permission['estimate']['view']=="on")<a href="{{ url('/') }}/edit-estimate?est_no={{ $value['est_no'] }}" class="edit-icon edit-contact quote-edit">{{ Lang::get('common.edit') }}</a>@endif</div>
																		</div>
																	</div>
																@endforeach
															@else
																<div class="content-table-row">
																	<div class="content-table-data">INV-0001</div>
																	<div class="content-table-data">Jay Banik</div>
																	<div class="content-table-data">5-6-2017</div>
																	<div class="content-table-data">$ 2,000</div>
																	<div class="content-table-data"><span class="chips green">{{ Lang::get('invoice-estimate-common-template-lang.send') }}</span></div>
																	<div class="content-table-data"><div class="more-actions"><a href="#!" class="edit-icon edit-contact">{{ Lang::get('common.edit') }}</a></div></div>
																</div>
																<div class="content-table-row">
																	<div class="content-table-data">INV-0002</div>
																	<div class="content-table-data">Kunal Biswas</div>
																	<div class="content-table-data">15-6-2017</div>
																	<div class="content-table-data">$ 4,000</div>
																	<div class="content-table-data"><span class="chips green">{{ Lang::get('invoice-estimate-common-template-lang.draft') }}</span></div>
																	<div class="content-table-data"><div class="more-actions"><a href="#!" class="edit-icon edit-contact">{{ Lang::get('common.edit') }}</a></div></div>
																</div>
																<div class="content-table-row">
																	<div class="content-table-data">INV-0003</div>
																	<div class="content-table-data">Ayan Chowdhury</div>
																	<div class="content-table-data">15-6-2017</div>
																	<div class="content-table-data">$ 16,000</div>
																	<div class="content-table-data"><span class="chips red">{{ Lang::get('estimate-template-lang.decline') }}</span></div>
																	<div class="content-table-data"><div class="more-actions"><a href="#!" class="edit-icon edit-contact">{{ Lang::get('common.edit') }}</a></div></div>
																</div>
																<div class="content-table-row">
																	<div class="content-table-data">INV-0004</div>
																	<div class="content-table-data">Daniel Thomas</div>
																	<div class="content-table-data">20-6-2017</div>
																	<div class="content-table-data">$ 20,000</div>
																	<div class="content-table-data"><span class="chips green">{{ Lang::get('estimate-template-lang.accpeted') }}</span></div>
																	<div class="content-table-data"><div class="more-actions"><a href="#!" class="edit-icon edit-contact">{{ Lang::get('common.edit') }}</a></div></div>
																</div>
															@endif
														</div> <!-- END CONTENT TABLE BODY -->
													</div> <!-- END CONTENT TABLE -->
												</div>
											</div> <!-- END QUOTE GRID -->
										</div>
									</div> <!-- END COL -->
								</div> <!-- END ROW -->
							</form> <!-- END FORM-->
						</div> <!-- END QUOTES WRAPPER-->
					</div> <!-- END CONTACT GRID-->
				</div> <!-- END COL -->
			</div> <!-- END ROW -->
		</div> <!-- END CONTAINER -->
	</div>  <!-- END CONTENTS -->
@endsection