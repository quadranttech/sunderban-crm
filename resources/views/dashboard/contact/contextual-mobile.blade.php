@extends('layouts.dashboard-mobile-header')
@section('content')
	<div class="bottom-header">
	    <div class="container"> 
	      	<div class="row">
	        	<div class="col s12 left-align">
	        		<a href="{{url('/contact/new')}}" class="waves-effect waves-light btn blue">{{ Lang::get('common.add') }} {{ Lang::get('contact.Contact') }}</a>
	        	</div> <!-- END COL -->
	      	</div> <!-- END ROW -->
	    </div> <!-- END CONTAINER -->
	</div>  <!-- END BOTTOM HEADER -->
	<div class="contextual-section-wrapper">
  		<div class="container">
  			<div class="row">
  				<div class="col s12">
					<div class="contextual-section">
						<div class="row">
							<div class="col l12 m12 s12">
								<!-- LEFT PANEL -->
							    <div class="content border">
							    	<h3>{{ Lang::get('common.edit') }} {{ Lang::get('contact.Contact') }}</h3>
							    	<ul class="collapsible" data-collapsible="accordion">
							    		<li>
							    			<div class="collapsible-header active" id="show-feed">{{ Lang::get('common.feed') }}</div>
									      	<div class="collapsible-body">
									      		<div id="tabs-1" class="contexual-feed" style="display:@if($basic_info['page']=='feed') block!important @else none @endif">
													<div class="tabs-wrapper">
														<div class="feeds-list">
															<div class="content-table">
															</div> <!-- END CONTENT TABLE -->
														</div> <!-- END FEED LIST -->
													</div> <!-- END TAB WRAPPER -->
												</div> <!-- END TAB 1 -->
											</div>
										</li>
										<li>
							    			<div class="collapsible-header active" id="show-contexual-task">{{ Lang::get('task.tasks') }}</div>
									      	<div class="collapsible-body">
									      		<div id="tabs-2" style="display:@if($basic_info['page']=='task') block @else none @endif">
									      			<div class="right-align">
														<a href="#modal3" class="waves-effect waves-light btn blue" style="display:none">{{ Lang::get('common.add') }} {{ Lang::get('task.task') }}</a>
													</div>
													<div class="tabs-wrapper">
														<div class="tasks-list task-structure">
														</div> <!-- END TASKS LIST -->
													</div> <!-- END TAB WRAPPER -->
												</div> <!-- END TAB 2 -->
											</div>
										</li>
										<li>
							    			<div class="collapsible-header active" id="show-document">{{ Lang::get('common.documents') }}</div>
									      	<div class="collapsible-body">
									      		<div id="tabs-3">
													<div class="tabs-wrapper">
														<div class="row">
															<div class="col s12 document">
																<ul class="all-documents">
																	<li>
																		<div class="drag-area">
																			<form id="upload-document" name="upload-document" enctype="multipart/form-data" method="POST" action="">
																				<input type="file" name="document-upload" id="file" class="input-file">
																				<input type="hidden" name="_token" value="{{ csrf_token()}}">
																				<input type="hidden" name="contact_id" value="{{ $basic_info['id'] }}">
																				<input type="hidden" name="deal_id" value="0">
																				<input type="hidden" name="type" value="contact">
																				<label for="file" class="drag-label border">
																					<i class="icon fa fa-check"></i>
																					<span class="file-upload"></span>
																					<span class="file-name"></span>
																				</label>
																			</form>
																			<div class="desc">
																				Click to browse to upload files
																			</div>
																		</div>
																	</li>
																</ul>
															</div> <!-- END Col -->
														</div> <!-- END ROW -->
													</div> <!-- END TAB WRAPPER -->
												</div> <!-- END TAB 3 -->
											</div>
										</li>
										<li>
							    			<div class="collapsible-header active" id="show-note">Notes</div>
									      	<div class="collapsible-body">
									      		<div id="tabs-4">
													<div class="tabs-wrapper">
														<div class="note-lists clear-fix">
														</div> <!-- END NOTE LIST -->
														<div class="notes-form border clear-fix">
															<span class="note-border"></span>
															<div class="buttons right">
																<button class="waves-effect waves-light btn btn-dark-orange contexual-note-cancel">{{ Lang::get('common.cancel') }}</button>
																<button class="waves-effect waves-light btn btn-green save-note save">{{ Lang::get('common.save') }} {{ Lang::get('common.note') }}</button>
															</div>
															<form id="add-note-att" name="add-note-att" enctype="multipart/form-data" method="POST" action="">
																<div class="form-layout form-layout-100 clear-fix">
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																				<input type="hidden" name="_token" value="{{ csrf_token() }}">
																				<input type="hidden" name="contact_id" value="{{ $basic_info['id'] }}">
																				<input type="hidden" name="current_note_id" value="1">
																				<input type="hidden" name="_token" value="{{ csrf_token()}}">
																	          	<input id="first_name" type="text" placeholder="Untitled Note">
																	        </div>
																		</div>
																	</div>
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field text-wrapper">
																	          	<textarea id="textarea1" class="materialize-textarea" placeholder="Start writing from here"></textarea>
																	        </div>
																		</div>
																	</div>
																</div>
															</form>
															<div class="buttons right">
																<button class="waves-effect waves-light btn btn-dark-orange contexual-note-cancel">Cancel</button>
																<button class="waves-effect waves-light btn btn-green save-note save">Save Note</button>
															</div>
														</div> <!-- END NOTES FORM -->
													</div> <!-- END TAB WRAPPER -->
												</div> <!-- END TAB 4 -->
											</div>
										</li>
										<li>
							    			<div class="collapsible-header active show-deal-card">Deals</div>
									      	<div class="collapsible-body">
									      		<div id="tabs-5">
													<div class="tabs-wrapper">
														<div class="btn-group btn-toggle"> 
														    <button class="btn btn-one btn-lg btn-default show-deal-card">Card</button>
														    <!-- <button class="btn btn-two btn-lg btn-primary active" id="show-deal">Pipe Line</button> -->
														</div>
														<div class="row">
															<div class="col s12">
																<div class="drag-container border">
																</div>
															</div>
														</div>
														<div class="card-view">
															<div class="card-lists">
															</div> <!-- END CARD LISTS -->
														</div> <!-- END CARD VIEW -->
													</div> <!-- END TAB WRAPPER -->
												</div> <!-- END TAB 5 -->
											</div>
										</li>
									</ul>
							    </div>
							</div>
							<div class="col l12 m12 s12">
								<!-- RIGHT PANEL -->
							    <div class="right-side-bar border">
							    	<form action="{{url('/edit-contacts')}}" id="edit-contacts" method="POST">
							    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
							    		<input type="hidden" name="contact_id" value="{{ $basic_info['id'] }}">
								    	<div class="profile-wrapper">
								    		<div class="profile-name-designation">
								    			<span class="name">{{ $basic_info['title'] }}. {{ $basic_info['first_name'] }} {{ $basic_info['last_name'] }}</span>
								    		</div>
								    	</div> <!-- END PROFILE WRAPPER -->
								    	<div class="widgets clear-fix">
											<div class="widgets-header">
												<div class="row">
													<div class="col s12">
														<h3><span class="drag-handle"></span>Contact Details</h3>
													</div> <!-- END COL -->
												</div> <!-- END ROW -->
											</div>  <!-- END WIDGETS HEADER -->
											<div class="profile-information">
												<div class="row">
													<div class="col s12">
														<div class="form-layout clear-fix">
															<div class="form-layout-100">
																<div class="field-group">
																	<div class="input-field">
															          	<input id="first-name" name="first_name" type="text" value="{{ $basic_info['first_name'] }}" >
															          	<span class="error-mark"></span>
														          		<span class="check-mark"></span>
															          	<label for="first-name">First Name</label>
															        </div>
																</div>
															</div>
															<div class="form-layout-100">
																<div class="field-group">
																	<div class="input-field">
															          	<input id="last-name" name="last_name" type="text" value="{{ $basic_info['last_name'] }}" >
															          	<span class="error-mark"></span>
														          		<span class="check-mark"></span>
															          	<label for="last-name">Last Name</label>
															        </div>
																</div>
															</div>
															@if(!empty($work_email))
																@foreach($work_email as $value)
																<div class="form-layout-100">
																	<div class="field-group">
																		<div class="input-field">
																			<input class="edit-contact-work-email" name="edit_contact_work_email[]" type="email" value="{{ $value }}">
																			<span class="error-mark"></span>
														          			<span class="check-mark"></span>
																			<label for="work-email">Work Email</label>
																        </div>
																	</div>
																</div>
																@endforeach
															@endif
															@if(!empty($home_email))
																@foreach($home_email as $value)
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																				<input class="edit-contact-home-email" name="edit_contact_home_email[]" type="email" value="{{ $value }}">
																				<label for="home-email">Home Email</label>
																	        </div>
																		</div>
																	</div>
																@endforeach
															@endif
															@if(!empty($work_phone))
																@foreach($work_phone as $value)
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																				<input class="edit-contact-work-phone" name="edit_contact_work_phone[]" type="text" value="{{ $value }}">
																				<span class="error-mark"></span>
															          			<span class="check-mark"></span>
																				<label for="work-phone">Work Phone No</label>
																	        </div>
																		</div>
																	</div>
																@endforeach
															@endif
															@if(!empty($home_phone))
																@foreach($home_phone as $value)
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																			<input class="edit-contact-home-phone" name="edit_contact_home_phone[]" type="text" value="{{ $value }}">
																			<label for="home-phone">Home Phone No</label>
																	          	<a href="#!" class="add-more"></a>
																	        </div>
																		</div>
																	</div>
																@endforeach
															@endif
															<div class="phone-no-clone-parent">	          	
																<div class="form-layout-100 phone-no-clone parent-div">
																	<div class="field-group">
																		<div class="input-field select-phone">
																          	<select name="edit_phone_purpose[]">
																          		<option value="" disabled selected>Phone No</option>
																          		<option value="home">Home</option>
																          		<option value="work">Work</option>
																          	</select>
																          	<input type="text" name="add_extra_phone_contact[]" value="" placeholder="phone" >
																          	<a href="#!" class="add-more add-extra-phone-no"></a>
																        </div>
																	</div>
																</div>
															</div> <!-- END PHONE CLONE -->
															<div class="email-no-clone-parent">	          	
																<div class="form-layout-100 email-no-clone parent-div">
																	<div class="field-group">
																		<div class="input-field select-email">
																          	<select name="edit_email_purpose[]">
																          		<option value="" disabled selected>Email</option>
																          		<option value="home">Home</option>
																          		<option value="work">Work</option>
																          	</select>
																          	<input type="email" name="add_extra_email_contact[]" value="" placeholder="email">
																          	<a href="#!" class="add-more add-extra-email-no"></a>
																        </div>
																	</div>
																</div>
															</div> <!-- END EMAIL CLONE -->
															@if($basic_info['contact_type']=="company")
															<div class="form-layout-100">
																<div class="edit-default-contact">
																	<div class="field-group">
																		<div class="input-field">
																			<input name="default_contact" type="checkbox" class="filled-in" id="default_contact" @if($basic_info['default_contact']==true) checked @endif>
																			<label for="default_contact">Default Contact</label>
																		</div>
																	</div>
																</div>
															</div>
															@endif
														</div> <!-- END FORM LAYOUT -->
													</div> <!-- END COL -->
												</div> <!-- END ROW -->
												<div class="row">
													<div class="col s12">
														<div class="form-layout clear-fix">
															<div class="form-layout-100">
																<div class="field-group">
																	<div class="input-field">
																		<a href="#!" class="add-input"> Add Address <span></span></a>
																	</div>
																</div>
															</div>
															<div class="form-layout-100">
																<div class="field-group">
																	<div class="input-field">
																		<a href="#!" class="add-input"> IM Profile <span></span></a>
																	</div>
																</div>
															</div>
															<div class="form-layout-100">
																<div class="field-group">
																	<div class="input-field">
																		<a href="#!" class="add-input"> Social Profile <span></span></a>
																	</div>
																</div>
															</div>
														</div> <!-- END FORM LAYOUT -->
													</div> <!-- END COL -->
												</div> <!-- END ROW -->
											</div>
										</div> <!-- END WIDGETS -->
										<div class="widgets clear-fix">
											<div class="widgets-header">
												<div class="row">
													<div class="col s12">
														<h3><span class="drag-handle"></span>Address Information</h3>
													</div> <!-- END COL -->
												</div> <!-- END ROW -->
											</div>  <!-- END WIDGETS HEADER -->
											<div class="edit-address-clone-parent">
												@if(!empty($address))
													@foreach($address as $value)
														<div class="address-form-wrapper edit-address-clone parent-div">
															<input type="hidden" name="address_array[]" value="{{ $value['id'] }}">
															<div class="row">
																<div class="col s12">
																	<div class="form-layout form-layout-100 clear-fix">
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field address-section">
																		          	<select name="address_type[]">
																		          		<option value="" disabled selected>Address</option>
																		          		<option value="home_address" @if($value['office_address']=="home_address") selected @endif>Home</option>
																		          		<option value="office_address" @if($value['office_address']=="office_address") selected @endif>Office</option>
																		          	</select>
																		          	<a href="#!" class="add-more add-extra-address"></a>
																		        </div>
																			</div>
																		</div>
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
																		          	<input name="street_address[]" type="text" value="{{ $value['street_address'] }}" >
																		          	<label for="city" class="">Street Address</label>
																		        </div>
																			</div>
																		</div>
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
																		          	<input name="city[]" type="text" value="{{ $value['city'] }}">
																		          	<label for="city" class="">City</label>
																		        </div>
																			</div>
																		</div>
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
																		          	<input name="state[]" type="text" value="{{ $value['state'] }}">
																		          	<label for="state" class="">State / Region</label>
																		        </div>
																			</div>
																		</div>
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field country">
																					<select name="country[]">
																						<option value="" disabled selected>Country</option>
																						@include('country');
																					</select>
																				</div>
																			</div>
																		</div>
																		<div class="form-layout-100">
																			<div class="field-group">
																				<div class="input-field">
																		          	<input name="post_code[]" type="text" value="{{ $value['post_code'] }}">
																		          	<label for="postcode" class="">Postal Code</label>
																		        </div>
																			</div>
																		</div>
																	</div> <!-- END FORM LAYOUT -->
																</div> <!-- END COL -->
															</div> <!-- END ROW -->
														</div> <!-- END ADDRESS WRAPPER -->
													@endforeach
												@else
												<div class="address-form-wrapper edit-address-clone parent-div">
													<input type="hidden" name="address_array[]" value="0">
													<div class="row">
														<div class="col s12">
															<div class="form-layout form-layout-100 clear-fix">
																<div class="form-layout-100">
																	<div class="field-group">
																		<div class="input-field address-section">
																          	<select name="address_type[]">
																          		<option value="" disabled selected>Address</option>
																          		<option value="home_address">Home</option>
																          		<option value="office_address">Office</option>
																          	</select>
																          	<a href="#!" class="add-more add-extra-address"></a>
																        </div>
																	</div>
																</div>
																<div class="form-layout-100">
																	<div class="field-group">
																		<div class="input-field">
																          	<input name="street_address[]" type="text" value="" >
																          	<label for="city" class="">Street Address</label>
																        </div>
																	</div>
																</div>
																<div class="form-layout-100">
																	<div class="field-group">
																		<div class="input-field">
																          	<input name="city[]" type="text" value="">
																          	<label for="city" class="">City</label>
																        </div>
																	</div>
																</div>
																<div class="form-layout-100">
																	<div class="field-group">
																		<div class="input-field">
																          	<input name="state[]" type="text" value="">
																          	<label for="state" class="">State / Region</label>
																        </div>
																	</div>
																</div>
																<div class="form-layout-100">
																	<div class="field-group">
																		<div class="input-field country">
																			<select name="country[]">
																				<option value="" disabled selected>Country</option>
																				@include('country');
																			</select>
																		</div>
																	</div>
																</div>
																<div class="form-layout-100">
																	<div class="field-group">
																		<div class="input-field">
																          	<input name="post_code[]" type="text" value="">
																          	<label for="postcode" class="">Postal Code</label>
																        </div>
																	</div>
																</div>
															</div> <!-- END FORM LAYOUT -->
														</div> <!-- END COL -->
													</div> <!-- END ROW -->
												</div> <!-- END ADDRESS WRAPPER -->
											@endif
											</div>
										</div> <!-- END WIDGETS -->
										<div class="widgets clear-fix">
											<div class="widgets-header">
												<div class="row">
													<div class="col s12">
														<h3><span class="drag-handle"></span>IM Profiles</h3>
													</div> <!-- END COL -->
												</div> <!-- END ROW -->
											</div>  <!-- END WIDGETS HEADER -->
											<div class="row">
												<div class="col s12">
													<div class="add-extra-im-profile-parent">
														@if(!empty($im_profile))
															@foreach($im_profile as $value)
																<div class="improfile-form-wrapper add-extra-im-profile-clone parent-div">
																	<input type="hidden" name="im_profile_id_array[]" value="{{ $value['id'] }}">
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																	          	<select name="im_profile[]">
																	          		<option value="" disabled selected>IM Profile</option>
																	          		<option value="skype" @if($value['profile']=="skype") selected @endif>Skype</option>
																	          		<option value="other" @if($value['profile']=="other") selected @endif>Other</option>
																	          	</select>
																	          	<input name="im_profile_url[]" type="text" value="{{ $value['profile_url'] }}" >
																	          	<a href="#!" class="add-more add-extra-im-profile"></a>
																	        </div>
																		</div>
																	</div>
																</div>
															@endforeach
														@else
															<div class="improfile-form-wrapper add-extra-im-profile-clone parent-div">
																<input type="hidden" name="im_profile_id_array[]" value="0">
																<div class="form-layout-100">
																	<div class="field-group">
																		<div class="input-field">
																          	<select name="im_profile[]">
																          		<option value="" disabled selected>IM Profile</option>
																          		<option value="skype">Skype</option>
																          		<option value="other">Other</option>
																          	</select>
																          	<input name="im_profile_url[]" type="text" value="" >
																          	<a href="#!" class="add-more add-extra-im-profile"></a>
																        </div>
																	</div>
																</div>
															</div>
														@endif
													</div> <!-- END IM PROFILE -->
												</div> <!-- END COL -->
											</div> <!-- END ROW -->
										</div> <!-- END WIDGETS -->
										<div class="widgets clear-fix">
											<div class="widgets-header">
												<div class="row">
													<div class="col s12">
														<h3><span class="drag-handle"></span>Social Profiles</h3>
													</div> <!-- END COL -->
												</div> <!-- END ROW -->
											</div>  <!-- END WIDGETS HEADER -->
											<div class="row">
												<div class="col s12">
													<div class="add-extra-social-profile-parent">
														@if(!empty($social_profile))
															@foreach($social_profile as $value)
																<div class="improfile-form-wrapper add-extra-social-profile-clone parent-div">
																	<input type="hidden" name="social_profile_id_array[]" value="{{ $value['id'] }}">
																	<div class="form-layout-100">
																		<div class="field-group">
																			<div class="input-field">
																	          	<select name="social_profile[]">
																	          		<option value="" disabled selected>Social Profile</option>
																	          		<option value="facebook" @if($value['profile']=="facebook") selected @endif>Facebook</option>
																	          		<option value="linkedin" @if($value['profile']=="linkedin") selected @endif>Linkedin</option>
																	          		<option value="google" @if($value['profile']=="google") selected @endif>Google +</option>
																	          	</select>
																	          	<input name="social_profile_url[]" type="text" value="{{ $value['profile_url'] }}" >
																	          	<a href="#!" class="add-more add-extra-social-profile"></a>
																	        </div>
																		</div>
																	</div>
																</div>
															@endforeach
														@else
															<div class="improfile-form-wrapper add-extra-social-profile-clone parent-div">
																<input type="hidden" name="social_profile_id_array[]" value="0">
																<div class="form-layout-100">
																	<div class="field-group">
																		<div class="input-field">
																          	<select name="social_profile[]">
																          		<option value="" disabled selected>Social Profile</option>
																          		<option value="facebook">Facebook</option>
																          		<option value="linkedin">Linkedin</option>
																          		<option value="google">Google +</option>
																          	</select>
																          	<input name="social_profile_url[]" type="text" value="" >
																          	<a href="#!" class="add-more add-extra-social-profile"></a>
																        </div>
																	</div>
																</div>
															</div>
														@endif
													</div> <!-- END SOCIAL PROFILE -->
												</div> <!-- END COL -->
											</div> <!-- END ROW -->
										</div> <!-- END WIDGETS -->
										<div class="buttons center">
											<button class="waves-effect waves-light btn btn-dark-orange">Cancel</button>
											<button class="waves-effect waves-light btn btn-green edit-contact save">Save</button>
										</div> <!-- END BUTTONS -->
									</form>  <!-- END FORM -->
							    </div>
							</div>
						</div>  <!-- END ROW -->
				    </div>  <!-- END CONTEXTUAL SECTION -->
		    	</div>  <!-- END COL -->
		    </div>  <!-- END ROW -->
		</div>  <!-- END CONTAINER -->
	</div>  <!-- END CONTEXTUAL WRAPPER -->
	<!-- POPUP VIEW -->
	<div class="more-task-popup border contextual-tasks">
		<div class="popup-header">
			<ul>
				<li><a href="#!" class="edit edit-task" data-edit-task-id="">Edit</a></li>
				<li><a href="#!" class="complete">Mark complete</a></li>
				<li><a href="#!" class="change-date active">Change due date</a></li>
				<li><a href="#!" class="assign-task">Assign others</a></li>
			</ul>
		</div>
	</div> <!-- END OF POPUP WINDOW -->
	<!-- ADD DEAL MODAL STRUCTURE -->
	<div id="modal2" class="modal modal-fixed-footer">
		<button class="modal-close"></button>
		<div class="modal-content">
			<div class="row">
				<div class="col s12">
					<h3>Add Deal</h3>
				</div>
			</div>
			<form id="create-deal">
				<div class="row">
					<div class="col s12">
						<div class="form-layout form-layout-50x50 clear-fix">
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field">
							          	<input id="untitle-opportunity" name="title" type="text" class="validate">
							          	<label for="untitle-opportunity" class="">Untitle Opportunity</label>
							        </div>
								</div>
							</div>
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field select-field">
										<select name="contact">
											@foreach($basic_info['contacts'] as $contacts_key=>$contacts_value)
												@if($basic_info['id']==$contacts_value['id'])
													<option value="{{ $contacts_value['id'] }}" selected>{{ $contacts_value['first_name'] }} {{ $contacts_value['last_name'] }}</option>
												@else
													<option value="{{ $contacts_value['id'] }}">{{ $contacts_value['first_name'] }} {{ $contacts_value['last_name'] }}</option>
												@endif
											@endforeach
										</select>
									</div>
								</div>
							</div>
						</div> <!-- END FORM LAYOUT -->
					</div> <!-- END COL -->
				</div> <!-- END ROW -->
				<div class="row">
					<div class="col s12">
						<div class="form-layout form-layout-50x50 clear-fix">
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field select-field">
										<select name="company">
												<option value="" disabled selected>Choose Company</option>
											@foreach($basic_info['company'] as $company_key=>$company_value)
												<option value="{{ $company_value['id'] }}">{{ $company_value['company_name'] }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field select-field">
							          	<select name="owner">
							          		@foreach($basic_info['users'] as $user_key=>$user_value)
							          			@if($user_value['id']==$basic_info['current_user'])
							          				<option value="{{ $user_value['id'] }}" selected>{{ $user_value['name'] }}</option>
							          			@endif
							          				<option value="{{ $user_value['id'] }}">{{ $user_value['name'] }}</option>
							          		@endforeach
											<option value="owner">Owner</option>
											<option value="owner-1">Owner 1</option>
										</select>
							        </div>
								</div>
							</div>
						</div> <!-- END FORM LAYOUT -->
					</div> <!-- END COL -->
				</div> <!-- END ROW -->
				<div class="row">
					<div class="col s12">
						<div class="form-layout form-layout-50x50 clear-fix">
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field  select-field">
							          	<select name="stage">
							          		@foreach($basic_info['stages'] as $stages_key=>$stages_value)
												<option value="{{ $stages_value['id'] }}">{{ $stages_value['stage'] }}</option>
											@endforeach
										</select>
							        </div>
								</div>
							</div>
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field select-field">
							          	<select id="source" name="source">
							          			<option value="" disabled selected>Deal Source</option>
							          		@if(!empty($deal_source))
							          			@foreach($deal_source as $key=>$value)
							          				<option value="{{ $value }}">{{ $value }}</option>
							          			@endforeach
							          		@else
							          			<option value="">No Source</option>
							          		@endif
							          	</select>
							        </div>
								</div>
							</div>
						</div> <!-- END FORM LAYOUT -->
					</div> <!-- END COL -->
				</div> <!-- END ROW -->
				<div class="row">
					<div class="col s12">
						<div class="form-layout form-layout-50x50 clear-fix">
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field select-field">
										<select name="status">
											@foreach($basic_info['status'] as $status_key=>$status_value)
												<option value="{{ $status_value['id'] }}">{{ $status_value['status'] }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field select-field">
							          	<select name="priority">
											<option value="low">Low</option>
											<option value="medium">Medium</option>
											<option value="high">High</option>
										</select>
							        </div>
								</div>
							</div>
						</div> <!-- END FORM LAYOUT -->
					</div> <!-- END COL -->
				</div> <!-- END ROW -->
				<div class="row">
					<div class="col s12">
						<div class="form-layout form-layout-50x50 clear-fix">
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field">
										<input type="text" id="contexual-deal-value" name="value">
										<span class="error-mark"></span>
							          	<span class="check-mark"></span>
										<label for="value">Value</label>
									</div>
								</div>
							</div>
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field">
							          	<input type="text" name="win_probability">
							          	<label for="win-probability">Win Probability</label>
							        </div>
								</div>
							</div>
						</div> <!-- END FORM LAYOUT -->
					</div> <!-- END COL -->
				</div> <!-- END ROW -->
				<div class="row">
					<div class="col s12">
						<div class="form-layout form-layout-100 clear-fix">
							<div class="form-layout-100">
								<div class="field-group">
									<div class="input-field">
										<input id="description" name="description" type="text" class="validate">
										<input type="hidden" name="created_by" value="{{ $basic_info['current_user'] }}">
										<input type="hidden" name="pre_stage" value="">
							          	<label for="description">Description</label>
									</div>
								</div>
							</div>
						</div> <!-- END FORM LAYOUT -->
					</div> <!-- END COL -->
				</div> <!-- END ROW -->
			</form> <!-- END FORM -->
			<div class="buttons center">
				<button class="waves-effect waves-light btn orange">Cancel</button>
				<button class="waves-effect waves-light btn teal-light create-deal save" data-purpose="contexual">Save</button>
			</div> <!-- END BUTTONS -->
		</div> <!-- END MODAL CONTENT -->
	</div> <!-- END DEAL MODAL STRUCTURE -->
	<!-- ADD TASK MODAL STRUCTURE -->
	<div id="modal3" class="modal modal-fixed-footer">
		<button class="modal-close"></button>
		<div class="modal-content">
			<div class="row">
				<div class="col s12">
					<h3>Add Task</h3>
				</div>
			</div>
			<form action="save-new-task" id="add-new-task-form" method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row">
					<div class="col s12">
						<div class="form-layout clear-fix">
							<div class="form-layout-100">
								<div class="field-group">
									<div class="input-field select-field">
							          	<select name="related_deal" id="related_deal">
							          		<option value="">Selected Deal</option>
							          		@if(!empty($basic_info['deal']))
							          			@foreach($basic_info['deal'] as $key=>$value)
							          				<option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
							          			@endforeach
							          		@else
							          			<option value="0">No deal</option>
							          		@endif
							          	</select>
							          	<label>Select Deal</label>
							        </div>
								</div>
							</div> <!-- END FORM LAYOUT -->
							<div class="form-layout-100">
								<div class="field-group">
									<div class="input-field">
							          	<input name="task_title" type="text" class="validate">
							          	<span class="error-mark"></span>
							          	<span class="check-mark"></span>
							          	<label for="new-task">Untitled Task</label>
							        </div>
								</div>
							</div>
							<div class="form-layout-100">
								<div class="field-group">
									<div class="input-field">
							          	<input name="task_details" type="text" class="validate">
							          	<span class="error-mark"></span>
							          	<span class="check-mark"></span>
							          	<label for="details">Task Details</label>
							        </div>
								</div>
							</div>
						</div> <!-- END FORM LAYOUT -->
						<div class="form-layout form-layout-50x50 clear-fix">
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field select-field">
									    <select name="assigned_to" disabled>
									    	<option value="" disabled selected>Assigned to</option>
									    	@foreach($basic_info['users'] as $value)
									    		<option value="{{ $value['id'] }}" @if($value['id']==$basic_info['user_id']) selected @endif>{{ $value['first_name']}} {{$value['last_name']}}</option>
											@endforeach
										</select>
										<label>Assigned to</label>
										<span class="error-mark"></span>
							          	<span class="check-mark"></span>
									</div>
								</div>
							</div>
							<div class="form-layout-50">
								<div class="field-group">
									<div class="input-field date-field">
							          	<input type="text" name="due_date" id="date" class="form-control" data-dtp="dtp_kiaQI" placeholder="Due on">
							          	<span class="error-mark"></span>
							          	<span class="check-mark"></span>
							          	<span></span>
							        </div>
								</div>
							</div>
						</div> <!-- END FORM LAYOUT -->
					</div> <!-- END COL -->
				</div> <!-- END ROW -->
				<div class="buttons center">
					<button class="waves-effect waves-light btn btn-dark-orange contexual-task-cancel">Cancel</button>
					<button class="waves-effect waves-light btn createTask btn-green save" disabled>Save</button>
				</div> <!-- END BUTTONS -->
			</form>	<!-- END FORM -->				
		</div> 	<!-- END MODAL CONTENT -->	
	</div> <!-- END TASK MODAL STRUCTURE -->
@endsection