@extends('layouts.dashboard-header')

@section('content')



	<div class="bottom-header">

	    <div class="container"> 

	      	<div class="row">

	      		<div class="col s8">

	      		</div>

	        	<div class="col s4 right-align">

	        		<a href="{{url('/contact/create-separation')}}" class="waves-effect waves-light btn blue">Add Contact</a>

    				<a class="modal-trigger waves-effect waves-light btn green" href="#modal1">Add Company</a>

	        	</div> <!-- END COL -->

	      	</div> <!-- END ROW -->

	    </div> <!-- END CONTAINER -->

	</div>  <!-- END BOTTOM HEADER -->



		<div class="contents">

		<div class="container">

			<div class="row">

				<div class="col s12">

					<div class="row">

						<div class="col s12">

							<div class="contacts border">

								<h3>Add Contacts</h3>

								<div class="select-screen" id="select-screen">

									<div class="row">

										<div class="col s3">

											<div class="screens">

												<a href="{{url('/contact/individual')}}">

													<img src="{{url('/')}}/assets/img/contact.svg">

													<h4>individual</h4>

												</a>

											</div> <!-- END SCREENS -->

										</div> <!-- END COL -->

										<div class="col s3">

											<div class="screens screen-selected ">

												<a href="{{url('/contact/company')}}">

													<img src="{{url('/')}}/assets/img/company.svg">

													<h4>Company</h4>

												</a>

											</div> <!-- END SCREENS -->

										</div> <!-- END COL -->

									</div> <!-- END ROW -->

								</div> <!-- END SELECT SCREENS -->



								<?php

									if(!empty($errors->messages()))

									{

										$validation_message="error";

										$validation_color="error";

									}

									else

									{

										$validation_message="";

										$validation_color=" ";

									}

								?>

							</div>

						</div> <!-- END COL -->

					</div> <!-- END ROW -->

				</div> <!-- END COL -->

			</div> <!-- END ROW -->

		</div> <!-- END CONTAINER -->

	</div>  <!-- END CONTENTS -->





	<!-- POPUP VIEW -->

	<div class="more-task-popup border contextual-tasks">

		<div class="popup-header">

			<ul>

				<li><a href="#!" class="edit edit-task" data-edit-task-id="">Edit</a></li>

				<li><a href="#!" class="complete">Mark complete</a></li>

				<li><a href="#!" class="change-date active">Change due date</a></li>

				<li><a href="#!" class="assign-task">Assign others</a></li>

			</ul>

		</div>

	</div>

	<!-- END OF POPUP WINDOW -->



@endsection