@extends('layouts.dashboard-mobile-header')



@section('content')





			<div class="bottom-header">

			    <div class="container"> 

			      	<div class="row">

			      		<div class="col s12 left-align">

			        		<a href="{{url('/contact/create-separation')}}" class="waves-effect waves-light btn blue">Add Contact</a>

    						<a class="modal-trigger waves-effect waves-light btn green" href="#modal1">Add Company</a>

			        	</div> <!-- END COL -->

			      	</div> <!-- END ROW -->

			    </div> <!-- END CONTAINER -->

			</div>  <!-- END BOTTOM HEADER -->



	  		<div class="contents">

				<div class="container">

					<div class="row">

						<div class="col s12">

							<div class="row">

								<div class="col s12">

									<div class="contacts border">

										<h3>Add Informations</h3>

										<?php

											if(!empty($errors->messages()))

											{

												$validation_message="error";

												$validation_color="error";

											}

											else

											{

												$validation_message="";

												$validation_color=" ";

											}

										?>

										<div class="contact-screen" id="contact-screen">

											<form action="{{url('/add-contacts')}}" id="contact-form" method="POST">

												<div class="information">

													<div class="information-header">

														<h3>Personal Information</h3>

													</div> <!-- END INFORMATION HEADER -->



													<div class="row">

														<div class="col s12">

															<div class="form-layout form-layout-20x40x40 clear-fix">

																<div class="form-layout-100">

																	<div class="field-group">

																		<div class="input-field">

																		    <select name="contact_title">

																		    	<!-- <option value="" disabled selected>Title</option> -->

																				<option value="Mr">Mr.</option>

																				<option value="Mrs.">Mrs.</option>

																			</select>

																			<span class="error-mark"></span>

																          	<span class="check-mark"></span>

																		</div>

																	</div>

																</div>

																<div class="form-layout-100">

																	<div class="field-group">

																		<div class="input-field">

																          	<input id="first_name" name="first_name" type="text" class="validate" value="{{ old('first_name') }}">

																          	<span class="error-mark"></span>

																          	<span class="check-mark"></span>

																          	<label for="first_name">First Name</label>

																          	<span class="input_error"><?php if(!empty($errors->messages()['first_name'][0])) echo $errors->messages()['first_name'][0]; ?></span>

																        </div>

																	</div>

																</div>

																<div class="form-layout-100">

																	<div class="field-group">

																		<div class="input-field">

																          	<input id="last_name" name="last_name" type="text" class="validate">

																          	<span class="error-mark"></span>

																          	<span class="check-mark"></span>

																          	<label for="last_name">Last Name</label>

																          	<span class="input_error"><?php if(!empty($errors->messages()['last_name'][0])) echo $errors->messages()['last_name'][0]; ?></span>

																        </div>

																	</div>

																</div>

															</div> <!-- END FORM LAYOUT -->

														</div> <!-- END COL -->

													</div> <!-- END ROW -->

													<div class="row">

														<div class="col s12 contact-email">

															<div class="form-layout form-layout-60x40 clear-fix contact-email-clone">

																<div class="form-layout-100">

																	<div class="field-group">

																		<div class="input-field">

																          	<input name="email[]" type="email" class="validate">

																          	<span class="error-mark"></span>

																          	<span class="check-mark"></span>

																          	<label for="email">Email</label>

																          	<span class="input_error"><?php if(!empty($errors->messages()['first_name'][0])) echo $errors->messages()['email'][0]; ?></span>

																        </div>

																	</div>

																</div>

																<div class="form-layout-50">

																	<div class="field-group">

																		<div class="input-field">

																          	<select name="email_purpose[]">

																				<option value="work">Work</option>

																				<option value="home">Home</option>

																			</select>

																			<span class="error-mark"></span>

																          	<span class="check-mark"></span>

																        </div>

																	</div>

																</div>

																<div class="form-layout-30">

																	<div class="field-group">

																		<div class="input-field">

																			<a href="javascript:void(0);" class="add-input addAnotherContactEmail"> <span></span></a>

																			<!-- <a href="#!" class="add-input addAnotherContactEmail"> Add Another <span></span></a> -->

																		</div>

																	</div>

																</div>

															</div> <!-- END FORM LAYOUT -->

														</div> <!-- END COL -->

													</div> <!-- END ROW -->

													<div class="row">

														<div class="col s12 contact-phone">

															<div class="form-layout form-layout-60x40 clear-fix contact-phone-clone">

																<div class="form-layout-100">

																	<div class="field-group">

																		<div class="input-field">

																          	<input name="phone[]" type="text" class="validate">

																          	<span class="error-mark"></span>

																          	<span class="check-mark"></span>

																          	<label for="phone">Phone</label>

																          	<span class="input_error"><?php if(!empty($errors->messages()['phone'][0])) echo $errors->messages()['phone'][0]; ?></span>

																        </div>

																	</div>

																</div>

																<div class="form-layout-50">

																	<div class="field-group">

																		<div class="input-field">

																          	<select name="phone_purpose[]">

																				<option value="work">Work</option>

																				<option value="home">Home</option>

																			</select>

																        </div>

																	</div>

																</div>

																<div class="form-layout-30">

																	<div class="field-group">

																		<div class="input-field">

																			<a href="#!" class="add-input add-another-contact-phone"> <span></span></a>

																			<!-- <a href="#!" class="add-input add-another-contact-phone"> Add Another <span></span></a> -->

																		</div>

																	</div>

																</div>

															</div> <!-- END FORM LAYOUT -->

														</div> <!-- END COL -->

													</div> <!-- END ROW -->

													<div class="row">

														<div class="col s12">

															<div class="form-layout form-layout-20x20x30 clear-fix">

																<div class="form-layout-100">

																	<div class="field-group">

																		<div class="input-field">

																			<a href="javascript:void(0);" class="add-input open-address"> <span></span> Address </a>

																		</div>

																	</div>

																</div>

																<div class="form-layout-100">

																	<div class="field-group">

																		<div class="input-field">

																			<a href="#!" class="add-input open-im">  <span></span> IM Profiles </a>

																		</div>

																	</div>

																</div>

																<div class="form-layout-100">

																	<div class="field-group">

																		<div class="input-field">

																			<a href="#!" class="add-input open-social"> <span></span> Social Profiles </a>

																		</div>

																	</div>

																</div>

															</div> <!-- END FORM LAYOUT -->

														</div> <!-- END COL -->

													</div> <!-- END ROW -->

												</div> <!-- END INFORMATION -->



												<div class="information address-information">

													<div class="information-header">

														<h3>Address Information</h3>

														<a href="#!" class="add-input add-anothe-contact-address"> <span></span></a>

													</div> <!-- END INFORMATION HEADER -->

													<div class="address-clone">

														<div class="row">

															<div class="col s12">

																<h5 data-address="1">Address 1</h5>

															</div> <!-- END COL -->

														</div> <!-- END ROW -->

														<div class="row">

															<div class="col s12">

																<div class="form-layout form-layout-50x50 clear-fix">

																	<div class="form-layout-100">

																		<div class="field-group">

																			<div class="input-field">

																				<select name="addresstype[]">

																					<option value="" disabled selected>Choose your option</option>

																					<option value="office_address">Office Address</option>

																					<option value="home_address">Home Address</option>

																				</select>

																			</div>

																		</div>

																	</div>

																	<div class="form-layout-100">

																		<div class="field-group">

																			<div class="input-field">

																	          	<input type="text" name="street_address[]" class="validate">

																	          	<label for="streetaddress">Street Address</label>

																	        </div>

																		</div>

																	</div>

																</div> <!-- END FORM LAYOUT -->

															</div> <!-- END COL -->

														</div> <!-- END ROW -->

														<div class="row">

															<div class="col s12">

																<div class="form-layout form-layout-50x50 clear-fix">

																	<div class="form-layout-100">

																		<div class="field-group">

																			<div class="input-field">

																	          	<input type="text" name="city_address[]" class="validate">

																	          	<label for="city">City</label>

																	        </div>

																		</div>

																	</div>

																	<div class="form-layout-100">

																		<div class="field-group">

																			<div class="input-field">

																	          	<input type="text" name="state[]" class="validate">

																	          	<label for="state">State / Region</label>

																	        </div>

																		</div>

																	</div>

																</div> <!-- END FORM LAYOUT -->

															</div> <!-- END COL -->

														</div> <!-- END ROW -->

														<div class="row">

															<div class="col s12">

																<div class="form-layout form-layout-50x50 clear-fix">

																	<div class="form-layout-100">

																		<div class="field-group">

																			<div class="input-field">

																				<select name="country[]">

																					<option value="" disabled selected>Country</option>

																					<option value="australia">Australia</option>

																					<option value="india">India</option>

																					<option value="united_states">United States</option>

																					<option value="brazil">Brazil</option>

																					<option value="japan">Japan</option>

																				</select>

																			</div>

																		</div>

																	</div>

																	<div class="form-layout-100">

																		<div class="field-group">

																			<div class="input-field">

																	          	<input type="text" name="post_code[]" class="validate">

																	          	<input type="hidden" name="_token" value="{{ csrf_token() }}">

																	          	<input type="hidden" name="contact-company" value="0">

																	          	<label for="postcode">Post Code</label>

																	        </div>

																		</div>

																	</div>

																</div> <!-- END FORM LAYOUT -->

															</div> <!-- END COL -->

														</div> <!-- END ROW -->

													</div> <!-- END ADDRESS CLONE -->

												</div> <!-- END INFORMATION -->



												<div class="information im-profile">

													<div class="information-header">

														<h3>IM Profiles</h3>

														<a href="#!" class="add-input add-another-contact-im-profile"> <span></span></a>

													</div> <!-- END INFORMATION HEADER -->

													<div class="im-profile-clone">

														<div class="row">

															<div class="col s12">

																<div class="form-layout form-layout-20x50x30 clear-fix">

																	<div class="form-layout-100">

																		<div class="field-group">

																			<div class="input-field">

																			    <select name="imProfile[]">

																					<option value="skype">Skype</option>

																					<option value="whats_app">Whats app</option>

																				</select>

																			</div>

																		</div>

																	</div>

																	<div class="form-layout-100">

																		<div class="field-group">

																			<div class="input-field">

																	          	<input type="text" name="profile_url[]" class="validate">

																	          	<label for="profile">IM Profile URl</label>

																	        </div>

																		</div>

																	</div>

																</div>

															</div> <!-- END COL -->

														</div> <!-- END ROW -->

													</div> <!-- END IM PROFILE CLONE -->

												</div> <!-- END INFORMATION -->



												<div class="information social-profile">

													<div class="information-header">

														<h3>Social Profiles</h3>

														<a href="#!" class="add-input add-another-social-profile"> <span></span></a>

													</div>  <!-- END INFORMATION HEADER -->

													

													<div class="social-profile-clone">

														<div class="row">

															<div class="col s12">

																<div class="form-layout form-layout-20x50x30 clear-fix">

																	<div class="form-layout-100">

																		<div class="field-group">

																			<div class="input-field">

																			    <select name="social_profile[]">

																					<option value="facebook">Facebook</option>

																					<option value="linkedin">Linkedin</option>

																					<option value="google">Google Plus</option>

																				</select>

																			</div>

																		</div>

																	</div>

																	<div class="form-layout-100">

																		<div class="field-group">

																			<div class="input-field">

																	          	<input name="social_profile_url[]" type="text" class="validate">

																	          	<label for="social-profile">Social Profile URl</label>

																	        </div>

																		</div>

																	</div>

																</div> <!-- END FORM LAYOUT -->

															</div> <!-- END COL -->

														</div> <!-- END ROW -->

													</div> <!-- END SOCIAL PROFILE CLONE -->

													

												</div> <!-- END INFORMATION -->



												<div class="buttons center">

													<button class="waves-effect waves-light btn btn-dark-orange back-to-contact-grid">Cancel</button>

													<button class="waves-effect waves-light btn btn-green create-company">Save</button>

												</div> <!-- END BUTTONS -->

											</form>  <!-- END FORM -->

										</div>  <!-- END CONTACT SCREEN -->

									</div>  <!-- END CONTACTS -->

								</div> <!-- END COL -->



								<div class="col s12">

									<div class="right-side-bar border" id="sortable">

										<div class="widgets pd0 ui-state-default">

											<div class="widgets-header">

												<h3>Tasks</h3>

											</div>  <!-- END WIDGETS HEADER -->



											<div class="widgets-collapse">

												<div class="widgets-sub-header">

													<h5>Today</h5>

												</div>  <!-- END WIDGETS SUB HEADER -->

												<div class="tasks-list task-structure clear-fix">

													@foreach($task as $key=>$value)

														<div class="tasks @if($value['marked_as']=="done") task-complete @endif" data-task-id="{{ $value['id'] }}">

															<span class="border-right {{ $value['color'] }}"></span>

								                          	<div class="tasks-short-description">

									                            <input type="checkbox" name="markTask" class="filled-in" id="id-{{ $key }}" value="{{ $value['id'] }}">

									                            <label for="id-{{ $key }}"> {{ $value['task_title']}} </label>

									                            <div class="task-assignee">{{ $value['assigned_to'] }}</div>

									                            <div class="mobile-tag-container clear-fix">

									                              @if(!empty($value['tagsNameArray']))

									                                @foreach($value['tagsNameArray'] as $tagsNameArray_key=>$tagsNameArray_value)

									                                  <span class="tags tag-red">{{ $tagsNameArray_value }}</span>

									                                @endforeach

									                                <span class="more-tags">@if(!empty($value['tagsNameArray']) && sizeof($value['tagsNameArray'])>3) {{ sizeof($value['tagsNameArray'])-3 }}more tags @endif</span>

									                              @endif

									                            </div>

									                            <div class="tasks-status">

									                              <span class="tasks-priority-view {{ $value['color'] }}">{{ $value['due_date'] }}</span>

									                            </div>

									                            <a href="#!" class="icons" data-task-id="{{ $value['id'] }}"><i class="material-icons">more_horiz</i></a>

								                          	</div>

														</div>

													@endforeach

												</div> <!-- END TASKS LIST -->

											</div> <!-- END WIDGETS COLLAPSE -->

										</div> <!-- END WIDGETS -->

									</div> <!-- END RIGHT SIDE BAR -->

								</div> <!-- END COL -->

							</div> <!-- END ROW -->

						</div> <!-- END COL -->

					</div> <!-- END ROW -->

				</div> <!-- END CONTAINER -->

			</div>  <!-- END CONTENTS -->



			<!-- POPUP VIEW -->

			<div class="more-task-popup border contextual-tasks">

				<div class="popup-header">

					<ul>

						<li><a href="#!" class="edit edit-task" data-edit-task-id="">Edit</a></li>

						<li><a href="#!" class="complete">Mark complete</a></li>

						<li><a href="#!" class="change-date active">Change due date</a></li>

						<li><a href="#!" class="assign-task">Assign others</a></li>

					</ul>

				</div>

			</div>

			<!-- END OF POPUP WINDOW -->

			

@endsection