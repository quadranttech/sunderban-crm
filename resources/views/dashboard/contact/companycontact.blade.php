@extends('layouts.dashboard-header')
@section('content')	<div class="bottom-header">
	    <div class="had-container"> 
	      	<div class="row">
	      		<div class="col s8">
	      		</div>
	        	<div class="col s4 right-align">
	        	</div> <!-- END COL -->
	      	</div> <!-- END ROW -->
	    </div> <!-- END CONTAINER -->
	</div>  <!-- END BOTTOM HEADER -->
		<div class="contents">
		<div class="had-container">
			<div class="row">
				<div class="col s12">
					<div class="contacts border">
						<?php
						if(!empty($errors->messages()))
						{
							$validation_message="error";
							$validation_color="error";
						}
						else
						{
							$validation_message="";
							$validation_color=" ";
						}
						?>
						<div class="contact-screen" id="contact-screen">
							<form action="{{url('/add-contacts')}}" id="contact-form" method="POST">
								<div class="information">
									<div class="information-header">
										<h3>{{ Lang::get('contact.personal-information') }}</h3>
									</div> <!-- END INFORMATION HEADER -->
									<div class="row">
										<div class="col s12">
											<div class="form-layout form-layout-20x40x40 clear-fix">
												<div class="form-layout-20">
													<div class="field-group">
														<div class="input-field select-field">
														    <select name="contact_title">
																<option value="Mr">Mr.</option>
																<option value="Mrs.">Mrs.</option>
																<option value="Ms.">Ms.</option>
																<option value="Dr.">Dr.</option>
															</select>
															<span class="error-mark"></span>
												          	<span class="check-mark"></span>
														</div>
													</div>
												</div>
												<div class="form-layout-40">
													<div class="field-group">
														<div class="input-field">
												          	<input id="first_name" name="first_name" type="text" class="validate" value="{{ old('first_name') }}">
												          	<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												          	<label for="first_name">{{ Lang::get('common.first') }} {{ Lang::get('common.name') }}</label>
												          	<span class="input_error"><?php if(!empty($errors->messages()['first_name'][0])) echo $errors->messages()['first_name'][0]; ?></span>
												        </div>
													</div>
												</div>
												<div class="form-layout-40">
													<div class="field-group">
														<div class="input-field">
												          	<input id="last_name" name="last_name" type="text" class="validate">
												          	<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												          	<label for="last_name">{{ Lang::get('common.last') }} {{ Lang::get('common.name') }}</label>
												          	<span class="input_error"><?php if(!empty($errors->messages()['last_name'][0])) echo $errors->messages()['last_name'][0]; ?></span>
												        </div>
													</div>
												</div>
											</div> <!-- END FORM LAYOUT -->
										</div> <!-- END COL -->
									</div> <!-- END ROW -->
									<div class="row">
										<div class="col s12 contact-email">
											<div class="form-layout form-layout-60x40 clear-fix contact-email-clone">
												<div class="form-layout-50">
													<div class="field-group">
														<div class="input-field">
												          	<input name="email[]" type="email" class="validate">
												          	<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												          	<label for="email">{{ Lang::get('common.email') }}</label>
												          	<span class="input_error"><?php if(!empty($errors->messages()['first_name'][0])) echo $errors->messages()['email'][0]; ?></span>
												        </div>
													</div>
												</div>
												<div class="form-layout-20">
													<div class="field-group">
														<div class="input-field select-field">
												          	<select name="email_purpose[]">
																<option value="work">{{ Lang::get('contact.work') }}</option>
																<option value="home">{{ Lang::get('contact.home') }}</option>
															</select>
															<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												        </div>
													</div>
												</div>
												<div class="form-layout-10">
													<div class="field-group">
														<div class="input-field">
															<a href="javascript:void(0);" class="add-input addAnotherContactEmail"> <span></span></a>
														</div>
													</div>
												</div>
											</div> <!-- END FORM LAYOUT -->
										</div> <!-- END COL -->
									</div> <!-- END ROW -->
									<div class="row">
										<div class="col s12 contact-phone">
											<div class="form-layout form-layout-60x40 clear-fix contact-phone-clone">
												<div class="form-layout-50">
													<div class="field-group">
														<div class="input-field">
												          	<input name="phone[]" type="text" class="validate">
												          	<span class="error-mark"></span>
												          	<span class="check-mark"></span>
												          	<label for="phone">{{ Lang::get('common.phone') }}</label>
												          	<span class="input_error"><?php if(!empty($errors->messages()['phone'][0])) echo $errors->messages()['phone'][0]; ?></span>
												        </div>
													</div>
												</div>
												<div class="form-layout-20">
													<div class="field-group">
														<div class="input-field select-field">
												          	<select name="phone_purpose[]">
																<option value="work">{{ Lang::get('contact.work') }}</option>
																<option value="home">{{ Lang::get('contact.home') }}</option>
															</select>
												        </div>
													</div>
												</div>
												<div class="form-layout-10">
													<div class="field-group">
														<div class="input-field">
															<a href="javascript:void(0)" class="add-input add-another-contact-phone"> <span></span></a>
														</div>
													</div>
												</div>
											</div> <!-- END FORM LAYOUT -->
										</div> <!-- END COL -->
									</div> <!-- END ROW -->											
									<div class="row">
										<div class="col s12">
											<div class="form-layout form-layout-20x20x30 clear-fix border-top">
												<div class="form-layout-20">
													<div class="field-group">
														<div class="input-field">
															<a href="javascript:void(0);" class="add-input open-address"> <span></span> {{ Lang::get('common.address') }} </a>
														</div>
													</div>
												</div>
												<div class="form-layout-20">
													<div class="field-group">
														<div class="input-field">
															<a href="javascript:void(0)" class="add-input open-im">  <span></span> {{ Lang::get('contact.im-profiles') }} </a>
														</div>
													</div>
												</div>
												<div class="form-layout-30">
													<div class="field-group">
														<div class="input-field">
															<a href="javascript:void(0)" class="add-input open-social"> <span></span> {{ Lang::get('contact.social-profiles') }} </a>
														</div>
													</div>
												</div>
											</div> <!-- END FORM LAYOUT -->
										</div> <!-- END COL -->
									</div> <!-- END ROW -->
								</div> <!-- END INFORMATION -->
								<div class="information address-information">
									<div class="information-header">
										<h3>{{ Lang::get('contact.address-information') }}</h3>
									</div> <!-- END INFORMATION HEADER -->
									<div class="address-clone">
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50x50 clear-fix">
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field select-field">
																<select name="addresstype[]">
																	<option value="" disabled selected>{{ Lang::get('contact.type-of-address') }}</option>
																	<option value="office_address">{{ Lang::get('contact.office-address') }}</option>
																	<option value="home_address">{{ Lang::get('contact.home-address') }}</option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
													          	<input type="text" name="street_address[]" class="validate">
													          	<label for="streetaddress">{{ Lang::get('contact.street_address') }}</label>
													        </div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50x50 clear-fix">
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
													          	<input type="text" name="city_address[]" class="validate">
													          	<label for="city">{{ Lang::get('contact.city') }}</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
													          	<input type="text" name="state[]" class="validate">
													          	<label for="state">{{ Lang::get('contact.state-region') }}</label>
													        </div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50x50 clear-fix">
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field select-field">
																<select name="country[]">
																	<option value="" disabled selected>{{ Lang::get('common.country') }}</option>
																	@include('country');
																</select>
															</div>
														</div>
													</div>
													<div class="form-layout-40">
														<div class="field-group">
															<div class="input-field">
													          	<input type="text" name="post_code[]" class="validate">
													          	<input type="hidden" name="contact_type" value="company">
													          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
													          	<label for="postcode">{{ Lang::get('contact.post-code') }}</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-10">
														<div class="field-group">
															<div class="input-field center">
																<a href="javascript:void(0)" class="add-input add-anothe-contact-address"> <span></span></a>
															</div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
									</div> <!-- END ADDRESS CLONE -->
								</div> <!-- END INFORMATION -->
								<div class="information im-profile">
									<div class="information-header">
										<h3>{{ Lang::get('contact.im-profiles') }}</h3>
									</div> <!-- END INFORMATION HEADER -->
									<div class="im-profile-clone">
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-20x50x30 clear-fix">
													<div class="form-layout-20">
														<div class="field-group">
															<div class="input-field select-field">
															    <select name="imProfile[]">
																	<option value="skype">Skype</option>
																	<option value="other">Other</option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
													          	<input type="text" name="profile_url[]" class="validate">
													          	<label for="profile">{{ Lang::get('contact.im-profile-url') }}</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-30">
														<div class="field-group">
															<div class="input-field">
																<a href="javascript:void(0)" class="add-input add-another-contact-im-profile"> <span></span></a>
															</div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
									</div> <!-- END IM PROFILE CLONE -->
								</div> <!-- END INFORMATION -->
								<div class="information social-profile">
									<div class="information-header">
										<h3>{{ Lang::get('contact.social-profiles') }}</h3>
									</div> <!-- END INFORMATION HEADER -->
									<div class="social-profile-clone">
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-20x50x30 clear-fix">
													<div class="form-layout-20">
														<div class="field-group">
															<div class="input-field select-field">
															    <select name="social_profile[]">
															    	<!-- <option value="" disabled selected>Choose your option</option> -->
																	<option value="facebook">Facebook</option>
																	<option value="linkedin">Linkedin</option>
																	<option value="google">Google Plus</option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
													          	<input name="social_profile_url[]" type="text" class="validate">
													          	<label for="social-profile">{{ Lang::get('contact.social-profile-url') }}</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-30">
														<div class="field-group">
															<div class="input-field">
																<a href="javascript:void(0)" class="add-input add-another-social-profile"> <span></span></a>
															</div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
									</div> <!-- END SOCIAL PROFILE CLONE -->
								</div> <!-- END INFORMATION -->
								<div class="information company-information">
									<div class="information-header">
										<h3>{{ Lang::get('contact.company-information') }}</h3>
									</div> <!-- END INFORMATION HEADER -->
									<div class="row">
										<div class="col s12">
											<div class="form-layout form-layout-50x20 clear-fix">
												<div class="form-layout-50">
													<div class="field-group">
														<div class="input-field select-field">
														    <select name="contact-company">
																<option value="select-company">{{ Lang::get('common.select') }} {{ Lang::get('common.company') }}</option>
																@foreach($company as $key=>$value)
																	<option value="{{ $value['id'] }}">{{ $value['company_name'] }}</option>
																@endforeach
															</select>
															<span class="error-mark"></span>
												          	<span class="check-mark"></span>
														</div>
													</div>
												</div>
												<div class="form-layout-20">
													<div class="field-group">
														<div class="input-field">
															<a href="javascript:void(0);" class="add-contact-company waves-effect waves-light btn green">{{ Lang::get('common.add') }} {{ Lang::get('common.new') }}</a>
														</div>
													</div>
												</div>
												<div class="form-layout-30">
													<div class="field-group">
														<div class="input-field">
															<input name="default_contact" type="checkbox" class="filled-in" id="default_contact">
															<label for="default_contact">{{ Lang::get('contact.make-default-contact') }}</label>
														</div>
													</div>
												</div>
											</div> <!-- END FORM LAYOUT -->
										</div> <!-- END COL -->
									</div> <!-- END ROW -->
									<div class="add-company-contact">
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50X50 clear-fix">
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
																<input name="com-name" type="text" class="validate">
													          	<span class="error-mark"></span>
														        <span class="check-mark"></span>
													          	<label for="com-name">{{ Lang::get('common.company') }} {{ Lang::get('common.name') }}</label>
															</div>
														</div>
													</div>
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
																<input name="street-address" type="text" class="validate">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="street-address">{{ Lang::get('contact.street_address') }}</label>
															</div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50X50 clear-fix">
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
																<input name="city" type="text" class="validate">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="city">{{ Lang::get('contact.city') }}</label>
															</div>
														</div>
													</div>
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
																<input name="company_state" type="text" class="validate">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="state">{{ Lang::get('contact.state-region') }}</label>
															</div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50X50 clear-fix">
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field select-field">
																<select name="company_country">
																	<option value="country">{{ Lang::get('contact.country') }}</option>
																	@include('country');
																</select>
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
															</div>
														</div>
													</div>
													<div class="form-layout-50">
														<div class="field-group">
															<div class="input-field">
																<input name="post-code" type="text" class="validate">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="post-code">{{ Lang::get('contact.post-code') }}</label>
															</div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
									</div> <!-- ADD COMPANY CONTACT -->
								</div> <!-- END INFORMATION -->
								<div class="buttons center">
									<button class="waves-effect waves-light btn btn-dark-orange back-to-contact-grid">{{ Lang::get('common.cancel') }}</button>
									<button class="waves-effect waves-light btn btn-green create-company save">{{ Lang::get('common.save') }}</button>
								</div> <!-- END BUTTONS -->
							</form>  <!-- END FORM -->
						</div>  <!-- END CONTACT SCREEN -->
					</div>  <!-- END CONTACTS -->
				</div> <!-- END COL -->
			</div> <!-- END ROW -->
		</div> <!-- END CONTAINER -->
	</div>  <!-- END CONTENTS -->
	<!-- POPUP VIEW -->
	<div class="more-task-popup border contextual-tasks">
		<div class="popup-header">
			<ul>
				<li><a href="javascript:void(0)" class="edit edit-task" data-edit-task-id="">Edit</a></li>
				<li><a href="javascript:void(0)" class="complete">Mark complete</a></li>
				<li><a href="javascript:void(0)" class="change-date active">Change due date</a></li>
				<li><a href="javascript:void(0)" class="assign-task">Assign others</a></li>
			</ul>
		</div>
	</div>
	<!-- END OF POPUP WINDOW -->
@endsection