@extends('layouts.dashboard-header')

@section('content')


<div class="bottom-header">
    <div class="container"> 
      	<div class="row">
      		<div class="col s8">
      			<div class="serach-wrapper">
      				<div class="file-field input-field">
						<div class="file-path-wrapper">
							<input type="text" class="validate">
							<label>Search</label>
						</div>	
						<div class="btn">
							<span>Go</span>
						</div>													
					</div>
      			</div>
      		</div>
        	<div class="col s4 right-align">
        		<a class="waves-effect waves-light btn orange" id="addContactButton">Add Contact</a>
        	</div> <!-- END COL -->
      	</div> <!-- END ROW -->
    </div> <!-- END CONTAINER -->
</div>  <!-- END BOTTOM HEADER -->

<div class="contents">
<div class="container">
	<div class="row">
		<div class="col s7">
			<div class="contacts border">
				<div class="select-screen" id="select-screen" v-on:click="say()">
					<div class="row">
						<div class="col s6">
							<div class="screens">
								<i class="large material-icons">chat_bubble_outline</i>
								<h4>individual</h4>
							</div>
						</div>
						<div class="col s6">
							<div class="screens screen-selected ">
								<a href="{{ url('/contact/company') }}">
									<i class="large material-icons">chat_bubble_outline</i>
									<h4>Company</h4>
								</a>
							</div>
						</div>
					</div>
				</div>

				<div class="contact-screen" id="contact-screen">
					<form action="{{url('/add-contacts')}}" method="POST">
						<div class="information">
							<div class="information-header">
								<h3>General Information</h3>
							</div>
								<div class="row">
									<div class="col s12">
										<div class="form-layout form-layout-20x40x40 clear-fix">
											<div class="form-layout-20">
												<div class="field-group">
													<div class="input-field">
													    <select multiple>
															<option value="Mr">Mr.</option>
															<option value="Mrs.">Mrs.</option>
														</select>
													    <label for="title">Title</label>
													</div>
												</div>
											</div>
											<div class="form-layout-40">
												<div class="field-group">
													<div class="input-field">
											          	<input id="first_name" name="first_name" type="text" class="validate">
											          	<label for="first_name">First Name</label>
											        </div>
												</div>
											</div>
											<div class="form-layout-40">
												<div class="field-group">
													<div class="input-field">
											          	<input id="last_name" type="text" class="validate">
											          	<label for="last_name">Last Name</label>
											        </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12 emailField">
										<div class="form-layout form-layout-60x40 clear-fix">
											<div class="form-layout-60">
												<div class="field-group">
													<div class="input-field">
											          	<input id="email" type="email" class="validate">
											          	<label for="email">Email</label>
											        </div>
												</div>
											</div>
											<div class="form-layout-40">
												<div class="field-group">
													<div class="input-field">
														<a href="#!" class="add-input addEmail"> Add Another <span></span></a>
													</div>
												</div>
											</div>
										</div>
										<div class="form-layout form-layout-60x40 clear-fix addExtraEmailField" style="display:none">
											<div class="form-layout-60">
												<div class="field-group">
													<div class="input-field">
											          	<input id="email" type="email" class="validate">
											          	<label for="email">Email</label>
											        </div>
												</div>
											</div>
											<div class="form-layout-40">
												<div class="field-group">
													<div class="input-field">
														<a href="#!" class="remove-input removeEmail"> Remove <span></span></a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12">
										<div class="form-layout form-layout-60x40 clear-fix">
											<div class="form-layout-60">
												<div class="field-group">
													<div class="input-field">
											          	<input id="phone" type="text" class="validate">
											          	<label for="phone">Phone</label>
											        </div>
												</div>
											</div>
											<div class="form-layout-40">
												<div class="field-group">
													<div class="input-field">
														<a href="#!" class="add-input addPhone"> Add Another <span></span></a>
													</div>
												</div>
											</div>
										</div>
										<div class="form-layout form-layout-60x40 clear-fix addExtraPhoneField" style="display:none">
											<div class="form-layout-60">
												<div class="field-group">
													<div class="input-field">
											          	<input id="phone" type="text" class="validate">
											          	<label for="phone">Phone</label>
											        </div>
												</div>
											</div>
											<div class="form-layout-40">
												<div class="field-group">
													<div class="input-field">
														<a href="#!" class="remove-input removePhone"> Remove <span></span></a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							
						</div>

						<div class="information">
							<div class="information-header">
								<h3>Address Information</h3>
							</div>
							<!-- <form novalidate @submit.stop.prevent="submit"> -->
								<div class="row">
									<div class="col s12">
										<div class="form-layout form-layout-100 clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field">
											          	<input id="address" type="text" class="validate">
											          	<label for="address">Street Address</label>
											        </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12">
										<div class="form-layout form-layout-50x50 clear-fix">
											<div class="form-layout-50">
												<div class="field-group">
													<div class="input-field">
											          	<input id="city" type="text" class="validate">
											          	<label for="city">City</label>
											        </div>
												</div>
											</div>
											<div class="form-layout-50">
												<div class="field-group">
													<div class="input-field">
											          	<input id="state" type="text" class="validate">
											          	<label for="state">State / Region</label>
											        </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12">
										<div class="form-layout form-layout-50x50 clear-fix">
											<div class="form-layout-50">
												<div class="field-group">
													<div class="input-field">
														<select name="country" id="country">
															<option value="" disabled selected>Choose your country</option>
															<option value="australia">Australia</option>
															<option value="india">India</option>
															<option value="united_states">United States</option>
															<option value="brazil">Brazil</option>
															<option value="japan">Japan</option>
														</select>
														<label for="country">Country</label>
													</div>
												</div>
											</div>
											<div class="form-layout-50">
												<div class="field-group">
													<div class="input-field">
											          	<input id="postcode" type="text" class="validate">
											          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
											          	<label for="postcode">Post Code</label>
											        </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<!-- </form> -->
						</div>
						<div class="information">
						<div class="information-header">
							<h3>Lead Information</h3>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="form-layout form-layout-50x50 clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field">
												<select name="lead_source" id="lead_source">
													<option value="" disabled selected>Choose your lead source</option>
													<option value="australia">Australia</option>
													<option value="india">India</option>
													<option value="united_states">United States</option>
													<option value="brazil">Brazil</option>
													<option value="japan">Japan</option>
												</select>
												<label for="country">Lead Source</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</form>

					<div class="information">
						<div class="information-header">
							<h3>Lead Information</h3>
						</div>
						<div class="information-table">
							<table class="bordered">
								<thead>
									<tr>
										<th data-field="title">Title</th>
										<th data-field="email">Email</th>
										<th data-field="source">Source</th>
										<th data-field="status">Status</th>
										<th></th>
									</tr>
								</thead>

								<tbody>
									<tr>
										<td>Alvin</td>
										<td>Eclair</td>
										<td>$0.87</td>
										<td>$0.87</td>
										<th><a href="#!" class="icons"><i class="material-icons">&#xE5D4;</i></a></th>
									</tr>
									<tr>
										<td>Alan</td>
										<td>Jellybean</td>
										<td>$3.76</td>
										<td>$3.76</td>
										<th><a href="#!" class="icons"><i class="material-icons">&#xE5D4;</i></a></th>
									</tr>
									<tr>
										<td>Jonathan</td>
										<td>Lollipop</td>
										<td>$3.76</td>
										<td>$3.76</td>
										<th><a href="#!" class="icons"><i class="material-icons">&#xE5D4;</i></a></th>
									</tr>
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div> <!-- END COL -->
		<div class="col s5">
			<div class="right-side-bar border" id="sortable">
					<a href="#!" class="more-widgets"><i class="material-icons">&#xE5D4;</i></a>
				
				<div class="widgets pd0 ui-state-default">
					<div class="widgets-header">
						<h3><span class="drag-handle"></span>Tasks</h3>
					</div>
					<div class="widgets-sub-header">
						<h5>Today</h5>
					</div>
					<div class="tasks border-right border-right-yellow">
						<input type="checkbox" class="filled-in" id="filled-in-box" />
						<label for="filled-in-box"> Follow up with the customer </label>
						<a href="#!" class="icons"><i class="material-icons">&#xE5D4;</i></a>
					</div>
					<div class="tasks border-right border-right-green">
						<input type="checkbox" class="filled-in" id="filled-in-box-2"/>
						<label for="filled-in-box-2"> Sending the quote to Johnath by 11pm </label>
						<a href="#!" class="icons"><i class="material-icons">&#xE5D4;</i></a>
					</div>
					<div class="tasks border-right border-right-red">
						<input type="checkbox" class="filled-in" id="filled-in-box-3"/>
						<label for="filled-in-box-3"> Schedule a meeting with Jeff </label>
						<a href="#!" class="icons"><i class="material-icons">&#xE5D4;</i></a>
					</div>
					<div class="tasks border-right border-right-grey">
						<input type="checkbox" class="filled-in" id="filled-in-box-4"/>
						<label for="filled-in-box-4"> Making sure everything is in place for </label>
						<a href="#!" class="icons"><i class="material-icons">&#xE5D4;</i></a>
					</div>
					<div class="tasks border-right border-right-blue">
						<input type="checkbox" class="filled-in" id="filled-in-box-5"/>
						<label for="filled-in-box-5"> Sending the quote to Johnath </label>
						<a href="#!" class="icons"><i class="material-icons">&#xE5D4;</i></a>
					</div>
				</div> <!-- END WIDGETS -->

				<div class="widgets ui-state-default">
					<div class="widgets-header">
						<h3><span class="drag-handle"></span>Deals</h3>
					</div>
					
					<div class="deals clear-fix">
						<div class="progress-bar-wrapper">
							<div class="progress-bar"></div>
						</div>
						<span class="left deal-title">AMC squire deal</span>
						<span class="right deal-complete">80% Probability</span>
					</div>
					<div class="deals clear-fix">
						<div class="progress-bar-wrapper">
							<div class="progress-bar"></div>
						</div>
						<span class="left deal-title">EMC<sup>2</sup> deal</span>
						<span class="right deal-complete">80% Probability</span>
					</div>
				</div> <!-- END WIDGETS -->

				<div class="widgets ui-state-default">
					<div class="widgets-header">
						<h3><span class="drag-handle"></span>Calender</h3>
					</div>
						<div class="calender-wrapper clear-fix">
							<div class="calender border">
								<div class="month" id="january">
									<aside>
										<div class="calender-day-months clear-fix">
											<span class="calender-week">Week</span>
											<span class="calender-day active">Today</span>
											<span class="calender-months">Month</span>
										</div>
										<div class="calender-date-month clear-fix">
											<span class="calender-date">28</span>
											<span class="calender-month">February</span>
										</div>
										<div class="add-event">
											<md-button class="md-icon-button md-raised">
											  	<md-icon>add</md-icon>
											</md-button>
										</div>
									</aside>

									<article>
										<div class="days">
										  <b>M</b>
										  <b>T</b>
										  <b>W</b>
										  <b>T</b>
										  <b>F</b>
										  <b>S</b>
										  <b>S</b>
										</div>
										<div class="dates">
										  	<span class="disable">30</span>
										  	<span class="disable">31</span>
										  
										  	<span>1</span>
										  	<span class="active">2</span>
										  	<span>3</span>
										  	<span>4</span>
										  	<span>5</span>
										  	<span>6</span>
										  	<span>7</span>
										  	<span>8</span>
										  	<span>9</span>
										  	<span>10</span>
										  	<span>11</span>
											<span>12</span>
											<span>13</span>
											<span>14</span>
											<span>15</span>
											<span>16</span>
											<span>17</span>
											<span>18</span>
											<span>19</span>
											<span>20</span>
											<span>21</span>
											<span>22</span>
											<span>23</span>
											<span>24</span>
											<span>25</span>
											<span>26</span>
											<span>27</span>
											<span>28</span>
										  
											<span class="disable">1</span>
											<span class="disable">2</span>
											<span class="disable">3</span>
											<span class="disable">4</span>
											<span class="disable">5</span>
										</div>
									</article>
								</div>
								<div class="events-wrapper clear-fix">
									<div class="events clear-fix">
										<span class="big-bullet lightpurple"></span>
										<div class="events-time">
											<span>1 <sup>PM</sup></span>
										</div>
										<div class="events-details">
											<h5>Meeting with John</h5>
											<p>Meeting with John with some details of the meeting</p>
										</div>
									</div>
									<div class="events clear-fix">
										<span class="big-bullet darkpurple"></span>
										<div class="events-time">
											<span>2 <sup>PM</sup></span>
										</div>
										<div class="events-details">
											<h5>Meeting with John</h5>
											<p>Meeting with John with some details of the meeting</p>
										</div>
									</div>
									<div class="events clear-fix">
										<span class="big-bullet lightred"></span>
										<div class="events-time">
											<span>4 <sup>PM</sup></span>
										</div>
										<div class="events-details">
											<h5>Meeting with John</h5>
											<p>Meeting with John with some details of the meeting</p>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div> <!-- END WIDGETS -->
			</div>
		</div> <!-- END COL -->
	</div> <!-- END ROW -->
</div> <!-- END CONTAINER -->
</div>  <!-- END CONTENTS -->

<footer class="footer">
    <md-bottom-bar>
	  	<div class="copy-right">
	  		&copy; 2017 copyrights. All Rights Reserved.
	  	</div>
	</md-bottom-bar>
</footer>
@endsection