@extends('layouts.dashboard-mobile-header')
@section('content')

<div class="bottom-header">
    <div class="container"> 
      	<div class="row">
        	<div class="col s12 left-align">
        		@if($permission['contact']['add']=="on")<a href="{{url('/contact/new')}}" class="waves-effect waves-light btn blue">Add Contact</a>@endif
        	</div> <!-- END COL -->
      	</div> <!-- END ROW -->
    </div> <!-- END CONTAINER -->
</div>  <!-- END BOTTOM HEADER -->

<?php
	if(!empty($_GET['sort_by']))
	{
		$sort_by=$_GET['sort_by'];
	}
	else
	{
		$sort_by=sizeof($data);
	}
?>

<div class="contents">
	<div class="container">
		<div class="row">
			<div class="col s12">
				<div class="contacts-grid white border">
					<div class="row">
						<div class="col s8">
							<h3>Contacts <span class="total-contact">(@php echo sizeof($data); @endphp)</span></h3>
						</div> <!-- END COL -->
					</div> <!-- END ROW -->

					<div class="content-table-wrapper">
						<div id="contacts-list">
							<div class="list-search">
			                    <button type="button" class="search-icon"></button>
			                    <input class="search" placeholder="Search" />
		                  	</div>
							<div class="content-table">
								<div class="content-table-header border">
									<div class="content-table-row">
										<div class="content-table-data">
											<span class="select-all">
												<input type="checkbox" class="filled-in" id="filled-in-box" />
												<label for="filled-in-box">A</label>
											</span>
										</div>
										<div class="content-table-data">{{ Lang::get('common.first') }} {{ Lang::get('common.name') }}</div>
										<div class="content-table-data">{{ Lang::get('common.last') }} {{ Lang::get('common.name') }}</div>
										<div class="content-table-data sortable" data-sort="date" data-value="desc" data-clone="content-table-row">{{ Lang::get('common.date') }} {{ Lang::get('common.created') }}</div>
										<div class="content-table-data">{{ Lang::get('common.email') }} {{ Lang::get('common.address') }}</div>
										<div class="content-table-data">{{ Lang::get('common.phone') }} Number</div>
										<div class="content-table-data filter" style="width: 5%;"><span></span></div>
										<div class="content-table-data address">{{ Lang::get('common.address') }}</div>
										<div class="content-table-data"></div>
									</div> <!-- END CONTENT TABLE ROW -->
								</div> <!-- END CONTENT TABLE HEADER -->

								@php
									$count=1;
								@endphp

								<div class="content-table-body contactTable list @if(empty($data)) demo-content @endif">
          					   	    @if(!empty($data))
										@foreach ($data as $value)
											@php 
												$grav_url = "https://www.gravatar.com/avatar/".md5( strtolower( trim( $value['email'] ) ) ); 
											@endphp
											<div class="content-table-row content-table-row-sort">
												<div class="content-table-data">
													<span class="avatar-pic-wrapper contactFirstCharParent">
														<span class="avatar-pic pink lighten-1 contactFirstChar">@php echo strtoupper($value['first_name'][0]); @endphp</span>
													</span>
												</div>
												<div class="content-table-data contacFirstName firstName">{{ $value['title'] }} {{ $value['first_name'] }}</div>
												<div class="content-table-data contactLastName lastName">{{ $value['last_name'] }}</div>
												<div class="content-table-data createdAt created_at">{{ $value['created_at'] }}</div>
												<div class="content-table-data contactEmail">
													<a href="javascript:void(0);" class"=com-mailid email">{{ $value['email'] }}</a>
												</div>
												<div class="content-table-data contactPhone phone">{{ $value['phone'] }}</div>
												<div class="content-table-data contactAddressWorkHome" style="width: 5%;">
													<span class="phone-number-type">{{ $value['address_work_home'] }}</span>
												</div>
												<div class="content-table-data address contactStreetAddress address">@if($value['street_address']!="" && $value['street_address']!="-"){{ $value['street_address'] }} @else &#8212; @endif</div>
												<div class="content-table-data contactMore">
													<div class="more-actions">
														@if($permission['contact']['edit']=="on")<a href="{{url('/edit-contact')}}/{{ $value['id'] }}?page=feed" class="edit-icon edit-contact">Edit</a>@endif
													</div>
												</div>
											</div><!-- END CONTENT TABLE ROW -->
											@if($sort_by == $loop->iteration)
												@break;
											@endif
										@endforeach
									@else
						                <div class="content-table-row">
						                    <div class="content-table-data"> <span class="avatar-pic-wrapper contactFirstCharParent"> <span class="avatar-pic green contactFirstChar">A</span> </span> </div>
						                    <div class="content-table-data contacFirstName">Airi</div>
						                    <div class="content-table-data contactLastName">Satou</div>
						                    <div class="content-table-data contactEmail"> <a href="javascript:void(0);" class="com-mailid">airi@gmail.com</a> </div>
						                    <div class="content-table-data contactPhone">8547896523</div>
						                    <div class="content-table-data contactAddressWorkHome" style="width: 5%;"> <span class="phone-number-type">W</span> </div>
						                    <div class="content-table-data address contactStreetAddress">57, Demo Street Address</div>
						                    <div class="content-table-data contactMore">
						                      <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
						                    </div>
					                  	</div>
					                  	<div class="content-table-row">
						                    <div class="content-table-data"> <span class="avatar-pic-wrapper contactFirstCharParent"> <span class="avatar-pic pink lighten-1 contactFirstChar">B</span> </span> </div>
						                    <div class="content-table-data contacFirstName">Bruno</div>
						                    <div class="content-table-data contactLastName">Nash</div>
						                    <div class="content-table-data contactEmail"> <a href="javascript:void(0);" class="com-mailid">bruno@gmail.com</a> </div>
						                    <div class="content-table-data contactPhone">8457965235</div>
						                    <div class="content-table-data contactAddressWorkHome" style="width: 5%;"> <span class="phone-number-type">W</span> </div>
						                    <div class="content-table-data address contactStreetAddress">Virtual hill state, south</div>
						                    <div class="content-table-data contactMore">
						                      <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
						                    </div>
					                  	</div>
					                  	<div class="content-table-row">
						                    <div class="content-table-data"> <span class="avatar-pic-wrapper contactFirstCharParent"> <span class="avatar-pic teal contactFirstChar">B</span> </span> </div>
						                    <div class="content-table-data contacFirstName">Bradley</div>
						                    <div class="content-table-data contactLastName">Greer</div>
						                    <div class="content-table-data contactEmail"> <a href="javascript:void(0);" class="com-mailid">bradley@gmail.com</a> </div>
						                    <div class="content-table-data contactPhone">9856235847</div>
						                    <div class="content-table-data contactAddressWorkHome" style="width: 5%;"> <span class="phone-number-type">H</span> </div>
						                    <div class="content-table-data address contactStreetAddress">Walmart store, Pali Hill</div>
						                    <div class="content-table-data contactMore">
						                      <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
						                    </div>
					                  	</div>
					                  	<div class="content-table-row">
						                    <div class="content-table-data"> <span class="avatar-pic-wrapper contactFirstCharParent"> <span class="avatar-pic orange contactFirstChar">D</span> </span> </div>
						                    <div class="content-table-data contacFirstName">Daniel</div>
						                    <div class="content-table-data contactLastName">Thomas</div>
						                    <div class="content-table-data contactEmail"> <a href="javascript:void(0);" class="com-mailid">danielthomas@gmail.com</a> </div>
						                    <div class="content-table-data contactPhone">2587412365</div>
						                    <div class="content-table-data contactAddressWorkHome" style="width: 5%;"> <span class="phone-number-type">W</span> </div>
						                    <div class="content-table-data address contactStreetAddress">Makeshift house, something</div>
						                    <div class="content-table-data contactMore">
						                      <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
						                    </div>
					                  	</div>
						            @endif
								</div> <!-- END CONTENT TABLE BODY -->
							</div> <!-- END CONTENT TABLE -->
						</div>
					</div> <!-- END CONTENT TABLE WRAPPER -->
				</div> <!-- END CONTACT GRID-->
			</div> <!-- END COL -->
		</div> <!-- END ROW -->
	</div> <!-- END CONTAINER -->
</div>  <!-- END CONTENTS -->

@endsection