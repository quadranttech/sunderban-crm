@extends('layouts.dashboard-header')
@section('content')
<div class="bottom-header">
  <div class="had-container">
    <div class="row">
      <div class="col s8"> </div>
      <div class="col s4 right-align"> 
        @if($permission['contact']['add']=="on")<a href="{{url('/contact/new')}}" class="waves-effect waves-light btn blue">{{ Lang::get('common.add') }} {{ Lang::get('contact.Contact') }}</a>
        <a href="#import-contact" class="waves-effect waves-light btn blue">Import Contacts</a>
        @endif
      </div>
      <!-- END COL -->
    </div>
    <!-- END ROW -->
  </div>
  <!-- END CONTAINER -->
</div>
<!-- END BOTTOM HEADER -->
      <?php
        if(!empty($_GET['sort_by']))
        {
          $sort_by=$_GET['sort_by'];
        }
        else
        {
          $sort_by=sizeof($data);
        }
      ?>
      <div class="row">
        <div class="col s12">
          <div class="contents white border">
            <div class="had-container">
              <div class="contacts-grid">
                <div class="row">
                  <div class="col s8">
                    <h3>{{ Lang::get('contact.Contacts') }} <span class="total-contact">(@php echo sizeof($data); @endphp)</span></h3>
                  </div>
                  <!-- END COL -->
                </div>
                <!-- END ROW -->
                <div id="contacts-list">
                  <div class="list-search">
                    <button type="button" class="search-icon"></button>
                    <input class="search" placeholder="Search" />
                  </div>
                  <div class="content-table">
                    <div class="content-table-header border">
                      <div class="content-table-row">
                        <div class="content-table-data"> <span class="select-all">
                          <input type="checkbox" class="filled-in" id="filled-in-box" />
                          <label for="filled-in-box">A</label>
                          </span> </div>
                        <div class="content-table-data">{{ Lang::get('common.first') }} {{ Lang::get('common.name') }}</div>
                        <div class="content-table-data">{{ Lang::get('common.last') }} {{ Lang::get('common.name') }}</div>
                        <div class="content-table-data sortable" data-sort="date" data-value="desc" data-clone="content-table-row">{{ Lang::get('common.date') }} {{ Lang::get('common.created') }}</div>
                        <div class="content-table-data">{{ Lang::get('common.email') }} {{ Lang::get('common.address') }}</div>
                        <div class="content-table-data">{{ Lang::get('common.phone') }} Number</div>
                        <div class="content-table-data filter" style="width: 5%;"></div>
                        <div class="content-table-data address">{{ Lang::get('common.address') }}</div>
                        <div class="content-table-data"></div>
                      </div>
                      <!-- END CONTENT TABLE ROW -->
                    </div>
                    <!-- END CONTENT TABLE HEADER -->
                    @php
                      $count=1;
                    @endphp
                    <div class="content-table-body contactTable list @if(empty($data)) demo-content @endif"> 
                      <input type="hidden" name="status_message" value="{{ session('status') }}">
                      @if(!empty($data))
                        @foreach ($data as $value)
                          @php 
                            $grav_url = "https://www.gravatar.com/avatar/".md5( strtolower( trim( $value['email'] ) ) ); 
                          @endphp
                          <div class="content-table-row content-table-row-sort">
                            <div class="content-table-data"> <span class="avatar-pic-wrapper contactFirstCharParent"> <span class="avatar-pic pink lighten-1 contactFirstChar">@php echo strtoupper($value['first_name'][0]); @endphp</span> </span> </div>
                            <div class="content-table-data contacFirstName firstName">{{ $value['title'] }} {{ $value['first_name'] }}</div>
                            <div class="content-table-data contactLastName lastName">{{ $value['last_name'] }}</div>
                            <div class="content-table-data createdAt created_at" data-date="{{ $value['created_at_2'] }}">{{ $value['created_at'] }}</div>
                            <div class="content-table-data contactEmail"> <a href="javascript:void(0);" class"=com-mailid email">{{ $value['email'] }}</a> </div>
                            <div class="content-table-data contactPhone phone">{{ $value['phone'] }}</div>
                            <div class="content-table-data contactAddressWorkHome" style="width: 5%;"> <span class="phone-number-type">@if($value['address_work_home']=="W")<span>Work</span> @else<span>Home</span> @endif{{ $value['address_work_home'] }}</span> </div>
                            <div class="content-table-data address contactStreetAddress address">@if($value['street_address']!="" && $value['street_address']!="-"){{ $value['street_address'] }} @else &#8212; @endif</div>
                            <div class="content-table-data contactMore">
                              <div class="more-actions"> @if($permission['contact']['edit']=="on")<a href="{{url('/edit-contact')}}/{{ $value['id'] }}?page=feed" class="edit-icon edit-contact">Edit</a>@endif </div>
                            </div>
                          </div>
                          <!-- END CONTENT TABLE ROW -->
                          @if($sort_by == $loop->iteration)
                            @break;
                          @endif
                        @endforeach 
                      @else
                        <div class="content-table-row">
                            <div class="content-table-data"> <span class="avatar-pic-wrapper contactFirstCharParent"> <span class="avatar-pic green contactFirstChar">A</span> </span> </div>
                            <div class="content-table-data contacFirstName">Airi</div>
                            <div class="content-table-data contactLastName">Satou</div>
                            <div class="content-table-data contactEmail"> <a href="javascript:void(0);" class="com-mailid">airi@gmail.com</a> </div>
                            <div class="content-table-data contactPhone">8547896523</div>
                            <div class="content-table-data contactAddressWorkHome" style="width: 5%;"> <span class="phone-number-type">W</span> </div>
                            <div class="content-table-data address contactStreetAddress">57, Demo Street Address</div>
                            <div class="content-table-data contactMore">
                              <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
                            </div>
                          </div>
                          <div class="content-table-row">
                            <div class="content-table-data"> <span class="avatar-pic-wrapper contactFirstCharParent"> <span class="avatar-pic pink lighten-1 contactFirstChar">B</span> </span> </div>
                            <div class="content-table-data contacFirstName">Bruno</div>
                            <div class="content-table-data contactLastName">Nash</div>
                            <div class="content-table-data contactEmail"> <a href="javascript:void(0);" class="com-mailid">bruno@gmail.com</a> </div>
                            <div class="content-table-data contactPhone">8457965235</div>
                            <div class="content-table-data contactAddressWorkHome" style="width: 5%;"> <span class="phone-number-type">W</span> </div>
                            <div class="content-table-data address contactStreetAddress">Virtual hill state, south</div>
                            <div class="content-table-data contactMore">
                              <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
                            </div>
                          </div>
                          <div class="content-table-row">
                            <div class="content-table-data"> <span class="avatar-pic-wrapper contactFirstCharParent"> <span class="avatar-pic teal contactFirstChar">B</span> </span> </div>
                            <div class="content-table-data contacFirstName">Bradley</div>
                            <div class="content-table-data contactLastName">Greer</div>
                            <div class="content-table-data contactEmail"> <a href="javascript:void(0);" class="com-mailid">bradley@gmail.com</a> </div>
                            <div class="content-table-data contactPhone">9856235847</div>
                            <div class="content-table-data contactAddressWorkHome" style="width: 5%;"> <span class="phone-number-type">H</span> </div>
                            <div class="content-table-data address contactStreetAddress">Walmart store, Pali Hill</div>
                            <div class="content-table-data contactMore">
                              <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
                            </div>
                          </div>
                          <div class="content-table-row">
                            <div class="content-table-data"> <span class="avatar-pic-wrapper contactFirstCharParent"> <span class="avatar-pic orange contactFirstChar">D</span> </span> </div>
                            <div class="content-table-data contacFirstName">Daniel</div>
                            <div class="content-table-data contactLastName">Thomas</div>
                            <div class="content-table-data contactEmail"> <a href="javascript:void(0);" class="com-mailid">danielthomas@gmail.com</a> </div>
                            <div class="content-table-data contactPhone">2587412365</div>
                            <div class="content-table-data contactAddressWorkHome" style="width: 5%;"> <span class="phone-number-type">W</span> </div>
                            <div class="content-table-data address contactStreetAddress">Makeshift house, something</div>
                            <div class="content-table-data contactMore">
                              <div class="more-actions"> <a href="#!" class="edit-icon edit-contact">Edit</a> </div>
                            </div>
                          </div>
                      @endif
                    </div>
                    <!-- END CONTENT TABLE BODY -->
                  </div>
                </div>
                <!-- END CONTENT TABLE -->
              </div>
              <!-- END CONTACT GRID-->
              <div class="pagination">
                <ul>
                  <li><a href="">&lt;</a></li>
                  <li><a href="">1</a></li>
                  <li><a href="">2</a></li>
                  <li><a href="">3</a></li>
                  <li><a href="">4</a></li>
                  <li><a href="">&gt;</a></li>
                </ul>
              </div>
              <!-- END OF PAGINATION -->
            </div>
            <!-- END CONTAINER -->
          </div>
        </div>
      </div>
<!-- END CONTENTS -->

  <div id="import-contact" class="modal modal-fixed-footer">
    <button class="modal-close"></button>
    <div class="modal-wrapper">
      <div class="modal-content">
        <div class="row">
          <div class="col s12">
            <h3> Import Contact List </h3>
            <form id="uploadForm" name="upload-document" enctype="multipart/form-data" method="POST" action="{{ url('/') }}/import/contact">
              <input type="hidden" name="_token" value="{{ csrf_token()}}">
              <input type="file" id="contact" name="contact">
              <!-- <input type="submit" name="import-contact" value="Import"> -->
              <span class="import-contact-message"></span>
              <span class="verifying"></span>
              <div id="progress-div"><div id="progress-bar"></div></div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>


@if (session('status'))
    <script>
    Push.create("New contact created!", {
        body: jQuery("input[name='status_message']").val(),
        icon: '/icon.png',
        timeout: 4000,
        onClick: function () {
            window.focus();
            this.close();
        }
    });
    </script>
@endif
@endsection