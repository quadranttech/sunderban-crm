@extends('layouts.dashboard-mobile-header')
@section('content')
	<div class="bottom-header">
	    <div class="container"> 
	      	<div class="row">
	      		<div class="col s12 left-align">
	        		@if($permission['task']['add']=="on")<a class="modal-trigger waves-effect waves-light btn blue" href="#modal3">{{ Lang::get('common.add') }} {{ Lang::get('task.task') }}</a>@endif
	        	</div> <!-- END COL -->
	      	</div> <!-- END ROW -->
	    </div> <!-- END CONTAINER -->
	</div>  <!-- END BOTTOM HEADER -->
	<div class="contents">
		<div class="container">
  			<div class="row">
  				<div class="col s12">
					<div class="tasks-wrapper border">
						<div class="row">
							<div class="col s12">
								<div class="select-assignee">
									@if($permission['task']['add']=="on" || $permission['task']['all']=="on")
										<select name="show-task-assigned-to-me">
											<option value="assigned_to_all">{{ Lang::get('task.assigned-to-all') }}</option>
											<option value="assigned_to_me">{{ Lang::get('task.assign-to-me') }}</option>
										</select>
									@endif
								</div>
							</div>
						</div>
						<div class="tasks-list task-structure clear-fix @if(empty($tasks)) demo-content @endif">
							@if(!empty($tasks))
							@foreach($tasks as $key=>$value)
		                        <div class="tasks @if($value['marked_as']=='done')task-complete @endif @if($value['assigned_to_id']==$user_id)own-task @endif" data-task-id="{{ $value['id'] }}">
		                          	<span class="border-right {{ $value['color'] }}"></span>
		                          	<div class="tasks-short-description">
		                            	<input type="checkbox" name="markTask" class="filled-in" id="id-{{ $key }}" value="{{ $value['id'] }}" @if($value['marked_as']=="done") checked disabled @endif>
		                            	<label for="id-{{ $key }}"> {{ $value['task_title']}} </label>
		                            	<div class="task-assignee">{{ $value['assigned_to'] }}</div>
		                            	<div class="mobile-tag-container clear-fix">
			                              	@if(!empty(array_filter($value['tagsNameArray'])))
			                                	@foreach($value['tagsNameArray'] as $inner_key=>$inner_value)
			                                  		@if($inner_key<=2)
			                                    		<span class="tags {{ $value['tagsColorArray'][$inner_key] }} {{ $inner_value }}">{{ $inner_value }}</span>
			                                  		@endif
			                                	@endforeach
			                             	 @endif
			                              	@if(sizeof($value['tagsNameArray'])>3)
			                                	<span class="more-tags" data-more-task="{{ sizeof($value['tagsNameArray'])-3}}">{{ sizeof($value['tagsNameArray'])-3}} more tags</span>
			                              	@endif
		                            	</div>
		                            	<div class="tasks-status">
		                              		<span class="tasks-priority-view {{ $value['color'] }}">{{ $value['due_date'] }}</span>
		                            	</div>
		                            	@if($permission['task']['edit']=="on" && $value['marked_as']!="done")<a href="javascript:void(0)" class="icons more-actions edit edit-task" data-edit-task-id="{{ $value['id'] }}">{{ Lang::get('common.edit') }}</a>@endif
		                          	</div>
		                        </div>
	                      	@endforeach
							@else
								<div class="tasks demo-task" data-task-id="">
									<span class="border-right indigo"></span>
									<div class="tasks-short-description">
										<input type="checkbox" name="markTask" class="filled-in" id="" value="" />
										<label for="">{{ Lang::get('common.new') }} {{ Lang::get('task.task') }}</label>
										<div class="task-assignee">Airi</div>
										<div class="mobile-tag-container">
											<span class="tags tag-indigo">{{ Lang::get('common.done') }}</span>
											<span class="more-tags">2 {{ Lang::get('common.more-tags') }}</span>
										</div>
										<div class="tasks-status">
											<span class="tasks-priority-view purple">{{ Lang::get('task.due-in') }} in 2 {{ Lang::get('task.day') }}</span>
										</div>
										<a href="javascript:void(0)" class="icons more-actions edit edit-task" data-edit-task-id="">{{ Lang::get('common.edit') }}</a>
									</div>
								</div> <!-- END TASK -->
								<div class="tasks demo-task" data-task-id="">
									<span class="border-right red"></span>
									<div class="tasks-short-description">
										<input type="checkbox" name="markTask" class="filled-in" id="" value="" />
										<label for="">{{ Lang::get('common.new') }} {{ Lang::get('task.task') }} 2</label>
										<div class="task-assignee">Daniel</div>
										<div class="mobile-tag-container">
											<span class="tags tag-indigo">{{ Lang::get('common.done') }}</span>
											<span class="more-tags">2 {{ Lang::get('common.more-tags') }}</span>
										</div>
										<div class="tasks-status">
											<span class="tasks-priority-view purple">{{ Lang::get('task.due-in') }} in 2 {{ Lang::get('task.day') }}</span>
										</div>
										<a href="javascript:void(0)" class="icons more-actions edit edit-task" data-edit-task-id="">{{ Lang::get('common.edit') }}</a>
									</div>
								</div> <!-- END TASK -->
								<div class="tasks demo-task" data-task-id="">
									<span class="border-right blue"></span>
									<div class="tasks-short-description">
										<input type="checkbox" name="markTask" class="filled-in" id="" value="" />
										<label for="">{{ Lang::get('common.new') }} {{ Lang::get('task.task') }} 3</label>
										<div class="task-assignee">Bruno</div>
										<div class="mobile-tag-container">
											<span class="tags tag-indigo">{{ Lang::get('common.done') }}</span>
											<span class="more-tags">2 {{ Lang::get('common.more-tags') }}</span>
										</div>
										<div class="tasks-status">
											<span class="tasks-priority-view purple">{{ Lang::get('task.due-in') }} in 2 {{ Lang::get('task.day') }}</span>
										</div>
										<a href="javascript:void(0)" class="icons more-actions edit edit-task" data-edit-task-id="">{{ Lang::get('common.edit') }}</a>
									</div>
								</div> <!-- END TASK -->
								<div class="tasks demo-task" data-task-id="">
									<span class="border-right blue"></span>
									<div class="tasks-short-description">
										<input type="checkbox" name="markTask" class="filled-in" id="" value="" />
										<label for="">{{ Lang::get('common.new') }} {{ Lang::get('task.task') }} 3</label>
										<div class="task-assignee">Bruno</div>
										<div class="mobile-tag-container">
											<span class="tags tag-indigo">{{ Lang::get('common.done') }}</span>
											<span class="more-tags">2 {{ Lang::get('common.more-tags') }}</span>
										</div>
										<div class="tasks-status">
											<span class="tasks-priority-view purple">{{ Lang::get('task.due-in') }} in 2 {{ Lang::get('task.day') }}</span>
										</div>
										<a href="javascript:void(0)" class="icons more-actions edit edit-task" data-edit-task-id="">{{ Lang::get('common.edit') }}</a>
									</div>
								</div> <!-- END TASK -->
							@endif
						</div> <!-- END TASK LIST -->
					</div> <!-- END TASK WRAPPER -->
				</div> <!-- END COL -->
			</div> <!-- END ROW -->
		</div> <!-- END CONTAINER -->
	</div> <!-- END CONTENTS -->
	<!-- POPUP VIEW -->
	<div class="more-task-popup border contextual-tasks">
		<div class="popup-header">
			<ul>
				<li><a href="javascript:void(0)" class="edit edit-task" data-edit-task-id="">Edit</a></li>
				<li><a href="javascript:void(0)" class="complete">Mark complete</a></li>
				<li><a href="javascript:void(0)" class="change-date active">Change due date</a></li>
				<li><a href="javascript:void(0)" class="assign-task">Assign others</a></li>
			</ul>
		</div>
	</div>
	<!-- END OF POPUP WINDOW -->
@endsection