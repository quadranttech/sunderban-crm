@extends('layouts.dashboard-mobile-header')
@section('content')

<div class="bottom-header">
    <div class="had-container"> 
      	<div class="row">
      		<div class="col s12">
      		</div><!-- END COL -->
      	</div> <!-- END ROW -->
    </div> <!-- END CONTAINER -->
</div>  <!-- END BOTTOM HEADER -->

<div class="contents">
	<div class="had-container">
		<div class="row">
			<div class="col s12">
				<div class="contacts-grid reports-grid white border">
					<div class="row">
			      		<div class="col s12">
			      			<h3> {{ Lang::get('report.reports') }} </h3>
			      		</div><!-- END COL -->
			      	</div> <!-- END ROW -->
					<div class="row">
						<div class="col s12">
							<div class="filter-options">
								<ul class="select-fields clear-fix">
									<li>
										<div class="form-layout clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field select-field">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
							          					<select name="total">
											          		<option value="" selected>{{ Lang::get('report.total-sales') }}</option>
											          		<option value="">{{ Lang::get('report.total-received') }}</option>
									    				</select>
							        				</div>
												</div>
											</div>
										</div>
									</li> <!-- END SELECT FIELDS LI -->
									<li>
										<div class="form-layout clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field select-field">
							          					<select name="filter-by-deal-source">
											          		<option value="" selected>{{ Lang::get('report.sales-source') }}</option>
											          		@if(!empty($deal_source))
											          			@foreach($deal_source as $key=>$value)
											          				<option value="{{ $value }}">{{ $value }}</option>
											          			@endforeach
											          		@endif
									    				</select>
							        				</div>
												</div>
											</div>
										</div>
									</li> <!-- END SELECT FIELDS LI -->
									<li>
										<div class="form-layout clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field select-field">
							          					<select name="filter-by-contact-name">
											          		<option value="" selected>{{ Lang::get('report.contact-name') }}</option>
											          		@if(!empty($contact))
											          			@foreach($contact as $key=>$value)
											          				<option value="{{ $value['id'] }}">{{ $value['first_name'] }} {{ $value['last_name'] }}</option>
											          			@endforeach
											          		@endif
									    				</select>
							        				</div>
												</div>
											</div>
										</div>
									</li> <!-- END SELECT FIELDS LI -->
									<li>
										<div class="form-layout clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field select-field">
							          					<select name="filter-by-staff-name">
											          		<option value="" selected>{{ Lang::get('report.staff-name') }}</option>
											          		@if(!empty($user))
											          			@foreach($user as $key=>$value)
											          				<option value="{{ $value['id'] }}">{{ $value['first_name'] }} {{ $value['last_name'] }}</option>
											          			@endforeach
											          		@endif
									    				</select>
							        				</div>
												</div>
											</div>
										</div>
									</li> <!-- END SELECT FIELDS LI -->
									<li>
										<div class="form-layout clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field select-field">
							          					<select name="filter-by-invoice-status">
											          		<option value="" selected>{{ Lang::get('report.invoice-status') }}</option>
											          		<option value="draft" >Draft</option>
											          		<option value="send">Sent</option>
											          		<option value="paid">Paid</option>
									    				</select>
							        				</div>
												</div>
											</div>
										</div>
									</li> <!-- END SELECT FIELDS LI -->
									<li>
										<div class="form-layout clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field select-field">
							          					<select name="filter-by-estimate-status">
											          		<option value="" selected>{{ Lang::get('report.estimate_status') }}</option>
											          		<option value="draft" >Draft</option>
											          		<option value="accepted">Accepted</option>
											          		<option value="declined">Declined</option>
									    				</select>
							        				</div>
												</div>
											</div>
										</div>
									</li> <!-- END SELECT FIELDS LI -->
									<li>
										<div class="form-layout clear-fix">
											<div class="form-layout-100">
												<div class="field-group">
													<div class="input-field daterange-field">
														<label>Date Range</label>
														<input class="raw" id="e2">
													</div>
												</div>
											</div>
										</div>
									</li> <!-- END SELECT FIELDS LI -->
								</ul> <!-- END SELECT FIELDS -->
								
								<div class="export-buttons">
									<button type="button" name="filter_export" data-export="pdf" class="pdf-button">Export as pdf</button>
									<button type="button" name="filter_export" data-export="xls" class="xls-button">Export as xls</button>
								</div>
								<form action="{{ url('/') }}/export-report" data-action="{{ url('/') }}/export-report" id="filterTable" method="post" style="display:none">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" id="filterData" value="yourValue" name="data[]">
									<button type="submit">Download</button>
								</form>
							</div>
						</div> <!-- END COL -->
					</div> <!-- END ROW -->
					<div class="row">
                        <div class="col s12">
                            <div class="content-table">
                                <div class="content-table-header border">
                                    <div class="content-table-row">
                                        <div class="content-table-data"> Date </div>
                                        <div class="content-table-data"> Contact </div>
                                        <div class="content-table-data"> Deal Title </div>
                                        <div class="content-table-data"> Deal Status </div>
                                        <div class="content-table-data"> Owner </div>
                                        <div class="content-table-data"> Deal Value </div>
                                    </div>
                                </div>
                                <div class="content-table-body filter-data demo-content">
                                    <div class="content-table-row">
                                        <div class="content-table-data created_at"> 2017-09-05 </div>
                                        <div class="content-table-data name"> Ayan Chowdhury </div>
                                        <div class="content-table-data deal"> New Deal </div>
                                        <div class="content-table-data status"> Follow Up </div>
                                        <div class="content-table-data owner"> Arnab Kumar Basu </div>
                                        <div class="content-table-data value"> 8000 </div>
                                    </div>
                                    <div class="content-table-row">
                                        <div class="content-table-data created_at"> 2017-09-05 </div>
                                        <div class="content-table-data name"> Ayan Chowdhury </div>
                                        <div class="content-table-data deal"> New Deal </div>
                                        <div class="content-table-data status"> Follow Up </div>
                                        <div class="content-table-data owner"> Arnab Kumar Basu </div>
                                        <div class="content-table-data value"> 8000 </div>
                                    </div>
                                    <div class="content-table-row">
                                        <div class="content-table-data created_at"> 2017-09-05 </div>
                                        <div class="content-table-data name"> Ayan Chowdhury </div>
                                        <div class="content-table-data deal"> New Deal </div>
                                        <div class="content-table-data status"> Follow Up </div>
                                        <div class="content-table-data owner"> Arnab Kumar Basu </div>
                                        <div class="content-table-data value"> 8000 </div>
                                    </div>
                                    <div class="content-table-row">
                                        <div class="content-table-data created_at"> 2017-09-05 </div>
                                        <div class="content-table-data name"> Ayan Chowdhury </div>
                                        <div class="content-table-data deal"> New Deal </div>
                                        <div class="content-table-data status"> Follow Up </div>
                                        <div class="content-table-data owner"> Arnab Kumar Basu </div>
                                        <div class="content-table-data value"> 8000 </div>
                                    </div>
                                    <div class="content-table-row clone" style="display:none">
                                        <div class="content-table-data created_at"> 2017-09-05 </div>
                                        <div class="content-table-data name"> Ayan Chowdhury </div>
                                        <div class="content-table-data deal"> New Deal </div>
                                        <div class="content-table-data status"> Follow Up </div>
                                        <div class="content-table-data owner"> Arnab Kumar Basu </div>
                                        <div class="content-table-data value"> 8000 </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>  <!-- END CONTACTS GRID -->
			</div> <!-- END COL -->
		</div> <!-- END ROW -->
	</div> <!-- END CONTAINER -->
</div> <!-- END CONTENTS -->


@endsection