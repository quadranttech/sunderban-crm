<!DOCTYPE html>

<html lang="en">

  <!-- HEAD SECTION -->

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Invoice Template 1</title>

    <!-- STYLE -->

    <style type="text/css">

      /****CUSTOM STYLESHEET*****/

        @import url('https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');

        @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i');

        @import url('https://fonts.googleapis.com/css?family=Niconne');

        body

        {

            background: #f4f4f8;

            padding: 20px 0;

        }

        table.table

        {

            max-width: 742px;

            width: 100%;

            height: auto;

            margin: 0 auto;

            border: 1px solid #95c3d8;

            -webkit-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);

        -moz-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);

        box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);

        }

        table

        {

            width: 100%;

        }

        table.table> tbody > tr > td

        {

            padding: 10px 30px;

        }

        table tr td table tr td.headertag

        {

            font-family: 'Montserrat', sans-serif;

            font-weight: 900;

            font-size: 40px;

            text-transform: uppercase;

            color: #4d4d4d;

        }

        table tr td table tr td.headerinfo

        {

            font-family: 'Open Sans', sans-serif;

            font-weight: 500;

            font-size: 14px;

            color: #4d4d4d;

        }



        table tr td table tr td.secondrow p.secondrowtitle

        {

            font-family: 'Montserrat', sans-serif;

            font-weight: 900; 

            font-size: 16px;

            letter-spacing: 1px;

            margin-bottom: 10px; 

        }

        table tr td table tr td.secondrow p.secondrowtitle span

        {

            font-family: 'Niconne', cursive;

            font-size: 25px;

            font-weight: 400;

        }

        table tr td table tr td.secondrow p

        {

            font-family: 'Open Sans', sans-serif;

            font-size: 14px;

            color: #4d4d4d;

            margin-bottom: 5px;

        }

        table tr.bluetable,

        table tr.bluetableview

        {

            background: #d0e5fd;



        }

        table tr.bluetableview td.blankspace,

        table tr.bluetableview td.blankspaceview

        {

            width: 530px;

        }

        table tr.bluetableview td

        {

            font-family: 'Open Sans', sans-serif;   

            font-size: 12px;

            font-weight: 500;

            color: #4d4d4d;

            padding: 10px;

        }

        table tr.bluetable td table tr.bluetableheader th

        {

            

            font-family: 'Open Sans', sans-serif;   

            font-size: 14px;

            font-weight: 600;

            color: #4d4d4d;

            padding: 10px;

            padding-top: 30px;

        }

        table tr.bluetable td table tr.bluetableheader td

        {

           

            font-family: 'Open Sans', sans-serif;   

            font-size: 12px;

            font-weight: 500;

            color: #4d4d4d;

            padding: 10px;

        }

        tr.bordertop

        {

            border-top: 1px solid #95c3d8;

        }

        tr.borderbottom

        {

            border-bottom: 1px solid #95c3d8;

        }

        table tr.bluetable td table tr.bluetablefooter td

        {

           

            font-family: 'Open Sans', sans-serif;   

            font-size: 12px;

            font-weight: 500;

            color: #4d4d4d;

            padding: 10px;

        }

        table tr.bluetableview td table tr.bluetablefooter th

        {

            font-family: 'Open Sans', sans-serif;

            font-size: 14px;

            font-weight: 600;

            color: #4d4d4d;

            padding: 10px;

        }

        table tr.bluetable td table tr.bluetableheader th.desc

        {

            width: 250px;

        }

        .table > thead > tr > th, .table > thead > tr > td, .table > tbody > tr > th, .table > tbody > tr > td, .table > tfoot > tr > th, .table > tfoot > tr > td

        {

            border-top: transparent;

        }

        table tr td table tr td.secondrow

        {

            padding: 20px 0 50px 0;

        }

        table tr td table tr td.largewidth

        {

           width: 68%;

        }

        table tr td table tr td.smallwidth

        {

           width: 22%;

        }

        table tr.companyinfo td table tr td h1

        {

            font-family: 'Open Sans', sans-serif;

            font-size: 16px;

            font-weight: 600;

            color: #4d4d4d;

            text-transform: uppercase;

            margin: 0;

            padding: 20px 0;

        }

        table tr.companyinfo td table tr td p

        {

            font-family: 'Open Sans', sans-serif;

            font-size: 12px;

            font-weight: 400;

            color: #4d4d4d;

            line-height: 20px;

            margin-bottom: 30px;

        }

        table tr.companyinfo td table tr td

        {

            width: 50%;

            vertical-align: top;

        }

        table tr.footer  td table tr

        {

            border-top: 1px solid #f0f0f0;

            border-bottom: 1px solid #f0f0f0;

        }

        table tr.footer > td

        {

            padding-bottom: 140px !important;

            background: #fff;

        }

        table tr.footer td table tr td p span.thankyou

        {

            font-family: 'Open Sans', sans-serif;

            font-size: 54px;

            font-weight: 600;

            color: #4d4d4d;

        }

        table tr.footer td table tr td p span.cursive

        {

            font-family: 'Niconne', cursive;

            font-size: 25px;

            font-weight: 400;

        }

        table tr.footer td table tr td p

        {

            font-family: 'Open Sans', sans-serif;

            font-size: 16px;

            font-weight: 600;

            color: #4d4d4d;

            text-align: center;

        }

        table.footerportion tr td table tr td p

        {

            text-align: center;

            font-family: 'Open Sans', sans-serif;

            font-size: 12px;

            font-weight: 600;

            color: #9a9a9a;

            margin: 0;

            padding: 50px 0 20px 0;

        }

        tr.companyinfo td {

            background: #fff;

            padding: 20px;

        }

        table.footerportion

        {

            max-width: 742px;

            width: 100%;

            height: auto;

            margin: 0 auto;

        }

        table.table tr.whitebackground

        {

            background: #fff;

        }

        tr.bordertop td

        {

            padding-top: 20px !important;

        }

        tr.borderbottom td

        {

            padding-bottom: 20px !important;

        }

        tr.bluetableview td table tr.bluetablefooterView td.footerblank

        {

            width: 68%;

        }

        tr.bluetableview td table tr.bluetablefooterView td.blueborder

        {

            width: 20%;

            border-bottom: 1px solid #95c3d8;

        }



      /****BOOTSTRAP STYLE*****/

        table {

          border-collapse: collapse;

          border-spacing: 0; }

        td,

        th {

          padding: 0; }

        thead {

          display: table-header-group; }

        tr,

        img {

          page-break-inside: avoid; }

        

        .table {

          border-collapse: collapse !important; }

        .table-bordered th,

        .table-bordered td {

          border: 1px solid #ddd !important; } } 

        table {

          max-width: 100%;

          background-color: transparent; }

        th {

          text-align: left; }



        .table {

          width: 100%;

          margin-bottom: 20px; }

          .table > thead > tr > th,

          .table > thead > tr > td,

          .table > tbody > tr > th,

          .table > tbody > tr > td,

          .table > tfoot > tr > th,

          .table > tfoot > tr > td {

            padding: 8px;

            line-height: 1.42857;

            vertical-align: top;

             }

          .table > thead > tr > th {

            vertical-align: bottom;

            border-bottom: 2px solid #ddd; }

          .table > caption + thead > tr:first-child > th,

          .table > caption + thead > tr:first-child > td,

          .table > colgroup + thead > tr:first-child > th,

          .table > colgroup + thead > tr:first-child > td,

          .table > thead:first-child > tr:first-child > th,

          .table > thead:first-child > tr:first-child > td {

            border-top: 0; }

          .table > tbody + tbody {

            border-top: 2px solid #ddd; }

          .table .table {

            background-color: #fff; }

        @media (max-width: 767px) {

          .table-responsive {

            width: 100%;

            margin-bottom: 15px;

            overflow-y: hidden;

            overflow-x: scroll;

            -ms-overflow-style: -ms-autohiding-scrollbar;

            

            -webkit-overflow-scrolling: touch; }

            .table-responsive > .table {

              margin-bottom: 0; }

              .table-responsive > .table > thead > tr > th,

              .table-responsive > .table > thead > tr > td,

              .table-responsive > .table > tbody > tr > th,

              .table-responsive > .table > tbody > tr > td,

              .table-responsive > .table > tfoot > tr > th,

              .table-responsive > .table > tfoot > tr > td {

                white-space: nowrap; }

            .table-responsive > .table-bordered {

              border: 0; }

        }

    </style>

    <!-- STYLE -->

    

  </head>

  <!-- HEAD SECTION -->

  <!-- BODY SECTION -->

  <body>

  <div class="table-responsive">

    <table class="table">

      <tr class="whitebackground">

        <td>

          <table>

            <tr>

              <td class="headertag">INVOICE</td>

              <td class="headerinfo">ID: QT-236541  |  Due on: 15th May, 2017</td>

              <td><a href="#!"><img src="assets/img/logo.png" alt="logo.png" /></a></td>

            </tr>

          </table>

        </td>

      </tr>

      <tr class="whitebackground">

        <td>

          <table>

            <tr>

              <td class="secondrow largewidth">

                <p class="secondrowtitle"><span>from </span>WebInnova LLC</p>

                <p>211 State Rd 25A,</p>

                <p>Lake City, Florida- 32055</p>

              </td>

              <td class="secondrow smallwidth">

                <p class="secondrowtitle"><span>to </span>QT LLP</p>

                <p>211 State Rd 25A,</p>

                <p>Lake City, Florida- 32055</p>

              </td>

            </tr>

          </table>

        </td>

      </tr>

      <tr class="bluetable">

        <td>

          <table>

            <tr class="bluetableheader">

              <th class="item">Item Name</th>

              <th class="desc">Description</th>

              <th class="rate">Rate</th>

              <th class="quantity">Quantity</th>

              <th class="total">Total</th>

            </tr>

            <tr class="bluetableheader bordertop">

              <td>Online sadasdasdas</td>

              <td>Website design and development</td>

              <td>$25</td>

              <td>100</td>

              <td>$2500</td>

            </tr>

            <tr class="bluetableheader">

              <td>Web Marketing</td>

              <td>Keyword research</td>

              <td>$30</td>

              <td>2</td>

              <td>$60</td>

            </tr>

            <tr class="bluetableheader borderbottom">

              <td>Designing</td>

              <td>Landing Page design and optimization of the page for google page speed</td>

              <td>$40</td>

              <td>2</td>

              <td>$80</td>

            </tr>

          </table>

        </td>

      </tr>

      <tr class="bluetableview withoutpadding">

        <td>

          <table>

            <tr class="bluetablefooter">

              <td class="blankspace"></td>

              <th>Sub-Total</td>

              <td>$2640</td>

            </tr>

            <tr class="bluetablefooter">

              <td class="blankspace"></td>

              <th>- Discount</td>

              <td>$60</td>

            </tr>

            <tr class="bluetablefooter">

              <td class="blankspace"></td>

              <th>Tax</td>

              <td>$10</td>

            </tr>

            <tr class="bluetablefooter bluetablefooterView">

              <td class="footerblank"></td>

              <td class="blueborder"></td>

              <td class="blueborder"></td>

            </tr>

            <tr class="bluetablefooter">

              <td class="blankspaceview"></td>

              <td></td>

              <td>$2590</td>

            </tr>

          </table>

        </td>

      </tr>

      <tr class="companyinfo">

        <td>

          <table>

            <tr>

              <td>

                <h1>Questions - Contact us</h1>

                <p>All the terms has has been updated and we have delivered the project with all the requisite documentation</p>

              </td>

              <td>

                <h1>Terms &amp; Conditions</h1>

                <p>- The invoice is due on next 15 days as per our agreement.</p>

                <p>- This is a sample terms and condition</p>

              </td>

            </tr>

            <tr></tr>

          </table>

        </td>

      </tr>

      <tr class="footer">

        <td>

          <table>

            <tr>

              <td><p><span class="thankyou">THANK YOU </span><span class="cursive">for </span> doing business with WebInnova</p></td>

            </tr>

          </table>

        </td>

      </tr>

    </table>

    <table class="footerportion">

      <tr>

        <td>

          <table>

            <tr>

              <td><p>The invoice was generated using @php echo str_replace('-', ' ', env('PORTAL_NAME')) @endphp</p></td>

            </tr>

          </table>

        </td>

      </tr>

    </table>

  </div>

  </body>

</html>