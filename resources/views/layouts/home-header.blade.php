<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>@php echo str_replace('-', ' ', env('PORTAL_NAME')) @endphp</title>
		<!--Import Google Icon Font-->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:300i,400,600,700|Roboto:300,400,400i,500,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="{{ url('/assets/css/materialize.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ url('/assets/css/animate.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ url('/assets/css/theme-style.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ url('/assets/css/responsive.css') }}" />
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="login-wrapper">
			@yield('content')
		</div>
		<!-- Scripts -->
		<script>
		var APP_URL = {!! json_encode(url('/')) !!}
		</script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script type="text/javascript" src="{{url('assets/js/materialize.min.js')}}"></script>
		<script type="text/javascript" src="{{url('/assets/js/jquery-ui.js')}}"></script>
		<script src="{{url('/assets/js/frontend.js')}}"></script>
		<script src="{{url('/assets/js/main.js')}}"></script>
	</body>
</html>