<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>@php echo str_replace('-', ' ', env('PORTAL_NAME')) @endphp</title>

		<link rel="shortcut icon" href="{{url('/assets/img/favicon.ico')}}" type="image/x-icon" />
		<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
		<link rel="apple-touch-icon" sizes="57x57" href="{{url('/assets/img/apple-icons/apple-touch-icon-57x57.png')}}" />
		<link rel="apple-touch-icon" sizes="72x72" href="{{url('/assets/img/apple-icons/apple-touch-icon-72x72.png')}}" />
		<link rel="apple-touch-icon" sizes="76x76" href="{{url('/assets/img/apple-icons/apple-touch-icon-76x76.png')}}" />
		<link rel="apple-touch-icon" sizes="114x114" href="{{url('/assets/img/apple-icons/apple-touch-icon-114x114.png')}}" />
		<link rel="apple-touch-icon" sizes="120x120" href="{{url('/assets/img/apple-icons/apple-touch-icon-120x120.png')}}" />
		<link rel="apple-touch-icon" sizes="144x144" href="{{url('/assets/img/apple-icons/apple-touch-icon-144x144.png')}}" />
		<link rel="apple-touch-icon" sizes="152x152" href="{{url('/assets/img/apple-icons/apple-touch-icon-152x152.png')}}" />
		<link rel="apple-touch-icon" sizes="180x180" href="{{url('/assets/img/apple-icons/apple-touch-icon-180x180.png')}}" />

		<!--Import Google Icon Font-->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:300i,400,600,700|Roboto:300,400,400i,500,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="{{url('assets/css/materialize.css')}}" />
		<link type="text/css" rel="stylesheet" href="{{url('assets/css/theme-style.css')}}" />
		<link type="text/css" rel="stylesheet" href="{{url('assets/css/responsive.css')}}" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.min.css" />
		<link rel="stylesheet" href="{{url('/assets/css/cropper.min.css')}}">
  		<link rel="stylesheet" href="{{url('/assets/css/main.css')}}">
  		<link rel="stylesheet" href="{{url('/assets/css/featherlight.min.css')}}">
  		<link rel="stylesheet" type="text/css" href="{{url('/assets/css/flexslider.css')}}">

        <script type="text/javascript" src="{{url('/assets/js/jquery.min.js')}}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/js/Jcrop.min.js"></script>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="wrapper">
			<div class="mobile-menu">
				<ul id="slide-out" class="side-nav">
				    <li>
				    	<div class="profile clear-fix">
		            		<a href="javascript:void(0);">
		              			<span class="profile-pic">
		                			<img src="assets/img/avatar.jpg" alt="avatar">
		              			</span> <!-- END PROFILE PIC -->  
		             			<div class="profile-info">
		                			<h5>Hi {{ Auth::user()->first_name }}, {{ Lang::get('invoice-estimate-common-template-lang.welcome-to') }} @php echo str_replace('-', ' ', env('PORTAL_NAME')) @endphp</h5>
		              			</div>  <!-- END PROFILE INFO -->  
		            		</a>
		          		</div> <!-- END PROFILE -->
				    </li>
				    <li><a href="{{ url('/') }}/dashboard">{{ Lang::get('menu.dashboard') }}</a></li>
				    <li><a href="{{ url('/') }}/settings">{{ Lang::get('settings.settings') }}</a></li>
				    <li><a href="{{ url('/') }}/manage-account">{{ Lang::get('settings.manage-account') }}</a></li>
				    @if($show_tutorial!="never")<li><a class="" href="#tutorial">{{ Lang::get('settings.tutorial') }}</a></li>@endif
				    <li><a href="{{ route('logout') }}">{{ Lang::get('settings.logout') }}</a></li>
			  	</ul>
	  			<a href="javascript:void(0)" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
			</div>
			<div class="notification-wrapper border">
				<button class="chips red">Close</button>
				<ul>
					@if(!empty($notification))
						@foreach($notification as $key=>$value)
							<li><a href="javascript:void(0);">{{ $value['notification'] }}</a><span> {{ $notification_difference[$key] }}</span></li>
						@endforeach
					@else
						<li><a href="javascript:void(0);">{{ Lang::get('common.no-notification-available') }}</a></li>
					@endif
				</ul>
				<input type="hidden" name="user_2" value="@if(!empty($notification)){{ $notification[0]['user_2'] }}@endif">
			</div>
			<header class="header">
			    <div class="container">
			      	<div class="row">
			      		<div class="col s10">
			      			<div class="logo-wrapper">
			          			<a href="{{url('/dashboard')}}"><img src="{{url('/')}}/uploads/company-logo/company-logo.png" alt="logo"></a>
			          		</div>
			          	</div>
			          	<div class="col s2">
			        		<div class="right-nav right">
				          		<div class="notification">
				            		<button type="button" class="notify-icon show-notification"><span> @if(!empty($notification)){{ sizeof($notification) }} @endif </span></button>
				            		<a class="search-cross" href=""></a>
				          		</div>
				          	</div>
			          	</div> <!-- END COL -->
			      	</div>
			    </div>
			</header> <!-- END HEADER PART -->
			@yield('content')
			<!-- STIKY ON FOOTER -->
			<div class="stiky-menu-icons">
				<ul>
					<li><a href="{{ url('/') }}/contacts" class="contact-icon">{{ Lang::get('menu.contact') }}</a></li>
				    <li><a href="{{ url('/') }}/deals" class="deal-icon">{{ Lang::get('menu.deals') }}</a></li>
				    <li><a href="{{ url('/') }}/tasks" class="task-icon">{{ Lang::get('menu.tasks') }}</a></li>
          			<li><a href="{{url('/estimates')}}"  class="estimate-icon">{{ Lang::get('menu.estimate') }}</a></li>
				    <li><a href="{{url('/invoices')}}" class="invoice-icon">{{ Lang::get('menu.invoice') }}</a></li>
				</ul>
			</div>
			<!-- END STIKY ON FOOTER -->
			<!-- POPUP VIEW -->
			<div class="more-task-popup border contextual-tasks">
				<div class="popup-header">
					<ul>
						<li><a href="javascript:void(0)" class="edit">Edit</a></li>
						<li><a href="javascript:void(0)" class="complete">Mark complete</a></li>
						<li><a href="javascript:void(0)" class="change-date active">Change due date</a></li>
						<li><a href="javascript:void(0)" class="assign-task">Assign others</a></li>
					</ul>
				</div>
			</div>
			<!-- END OF POPUP WINDOW -->
			<!-- Modal Structure -->
			@if(!empty($contacts))
			<div id="modal3" class="modal modal-fixed-footer">
				<button class="modal-close"></button>
				<div class="modal-wrapper">
					<div class="modal-content">
						<div class="row">
							<div class="col s12">
								<h3>{{ Lang::get('common.add') }} {{ Lang::get('task.task') }}</h3>
							</div>
						</div>
						<form action="save-new-task" id="add-new-task-form" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="row">
								<div class="col s12">
									<div class="form-layout clear-fix">
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field select-field">
										          	<select name="related_deal" id="related_deal">
										          		<option value="" disabled selected>{{ Lang::get('common.select') }} {{ Lang::get('deal.Deal') }}</option>
										          		@if(!empty($deal))
										          			@foreach($deal as $key=>$value)
										          				<option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
										          			@endforeach
										          		@else
										          			<option value="0">{{ Lang::get('common.no') }} {{ Lang::get('deal.Deal') }}</option>
										          		@endif
										          	</select>
										        </div>
											</div>
										</div>
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field">
										          	<input name="task_title" type="text" class="validate">
										          	<span class="error-mark"></span>
										          	<span class="check-mark"></span>
										          	<label for="new-task">{{ Lang::get('common.new') }} {{ Lang::get('task.task') }}</label>
										        </div>
											</div>
										</div>
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field">
										          	<input name="task_details" type="text" class="validate">
										          	<span class="error-mark"></span>
										          	<span class="check-mark"></span>
										          	<label for="details">{{ Lang::get('common.details') }}</label>
										        </div>
											</div>
										</div>
									</div>
									<div class="form-layout form-layout-50x50 clear-fix">
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field select-field">
												    <select name="assigned_to">
												    	<option value="" disabled selected>{{ Lang::get('task.assigned-to') }}</option>
												    	@foreach($contacts as $value)
												    		<option value="{{ $value['id'] }}">{{ $value['first_name']}} {{$value['last_name']}}</option>
														@endforeach
													</select>
													<span class="error-mark"></span>
										          	<span class="check-mark"></span>
												</div>
											</div>
										</div>
										<div class="form-layout-100">
											<div class="field-group">
												<div class="input-field date-field">
										          	<input type="text" name="due_date" id="date" class="form-control" data-dtp="dtp_kiaQI" placeholder="Due on">
										          	<span class="error-mark"></span>
										          	<span class="check-mark"></span>
										          	<span></span>
										        </div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="buttons center">
								<button class="waves-effect waves-light btn btn-dark-orange cancel-modal">{{ Lang::get('common.cancel') }}</button>
								<button class="waves-effect waves-light btn createTask btn-green" disabled>{{ Lang::get('common.save') }}</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			@endif
			@php
			echo View::make('dashboard.modal.modal', array('name' => 'Taylor'));
			@endphp
			<div class="showbox-wrapper">
	            <div class="showbox">
	               <div class="loader-wrapper">
	               <div class="loader">
	                  <svg class="circular" viewBox="25 25 50 50">
	                     <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
	                  </svg>
	               </div>
	               </div>
	            </div>
	        </div>
		</div>

		<div id="tutorial" class="modal border modal-fixed-footer">
			<button class="modal-close skip_tutorial"></button>
			<div class="modal-wrapper">
				<div class="modal-content">
				  	<div class="flexslider">
				      	<ul class="slides">
					        <li>
					            <img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" />
					        </li>
					        <li>
					            <img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" />
					        </li>
					        <li>
					            <img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" />
					        </li>
				    	</ul>
				 	</div>
				  	<div class="buttons center">
				    	<button class="waves-effect waves-light btn blue view_later_tutorial" href="">{{ Lang::get('common.watch-later') }}</button>
				    	<button class="waves-effect waves-light btn green skip_tutorial" href="">{{ Lang::get('common.skip') }}</button>
				  	</div>
				</div>
			</div>
		</div>


		<script type="text/javascript" src="{{url('/assets/js/moment-with-locales.min.js')}}"></script>
		<script type="text/javascript" src="{{url('/assets/js/material.min.js')}}"></script>
		<script type="text/javascript" src="{{url('/assets/js/bootstrap-material-datetimepicker.js')}}"></script>
		<script type="text/javascript" src="{{url('/assets/js/materialize.min.js')}}"></script>
		<script type="text/javascript" src="{{url('/assets/js/jquery-ui.js')}}"></script>
		<script type="text/javascript" src="{{url('/assets/js/featherlight.min.js')}}"></script>
		<script type="text/javascript" src="{{url('/assets/js/dragula.min.js')}}"></script>

		<script type="text/javascript" src="{{url('/assets/js/list.min.js')}}"></script>
		<script type="text/javascript" src="{{url('/assets/js/Chart.bundle.js')}}"></script>
	    <script type="text/javascript" src="{{url('/assets/js/utils.js')}}"></script>

	    <script type="text/javascript" src="{{url('/assets/js/jquery.flexslider.js')}}"></script>

		<script type="text/javascript">
			var APP_URL = {!! json_encode(url('/')) !!}
		</script>

		<script type="text/javascript">

			//CONTACT LIST
			var contact_options = {
				valueNames: [ 'firstName', 'lastName','email','phone','address' ]
			};

			var userList = new List('contacts-list', contact_options);
			//INVOICE LIST
			var invoice_options = {
				valueNames: [ 'inv_no', 'contact','created_at','value','status' ]
			};

			var userList = new List('invoice-list', invoice_options);
			//ESTIMATE LIST
			var estimate_options = {
				valueNames: [ 'est_no', 'contact','created_at','value','status' ]
			};

			var userList = new List('estimate-list', estimate_options);

			console.log(randomScalingFactor());
		</script>

		<script type="text/javascript" src="{{url('/assets/js/cropper.min.js')}}"></script>
	  	<script type="text/javascript" src="{{url('/assets/js/custom-cropper.js')}}"></script>
		<script type="text/javascript" src="{{url('/assets/js/main.js')}}"></script>
		<script type="text/javascript" src="{{url('/assets/js/dashboard.js')}}"></script>
		<script type="text/javascript" src="{{url('/assets/js/img-edit.js')}}"></script>

		<script type="text/javascript" src="{{url('/assets/js/pace.js')}}"></script>

		<script type="text/javascript">
		    paceOptions = {
		    	ajax: true,
		      	elements: true,
		      	initialRate: .5
		    };
		    $('select').material_select();
			$(".button-collapse").sideNav();
		</script>
	</body>
</html>