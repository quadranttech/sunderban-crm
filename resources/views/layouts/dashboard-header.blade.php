<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>@php echo str_replace('-', ' ', env('PORTAL_NAME')) @endphp</title>

		<link rel="shortcut icon" href="{{url('/assets/img/favicon.ico')}}" type="image/x-icon" />
		<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
		<link rel="apple-touch-icon" sizes="57x57" href="{{url('/assets/img/apple-icons/apple-touch-icon-57x57.png')}}" />
		<link rel="apple-touch-icon" sizes="72x72" href="{{url('/assets/img/apple-icons/apple-touch-icon-72x72.png')}}" />
		<link rel="apple-touch-icon" sizes="76x76" href="{{url('/assets/img/apple-icons/apple-touch-icon-76x76.png')}}" />
		<link rel="apple-touch-icon" sizes="114x114" href="{{url('/assets/img/apple-icons/apple-touch-icon-114x114.png')}}" />
		<link rel="apple-touch-icon" sizes="120x120" href="{{url('/assets/img/apple-icons/apple-touch-icon-120x120.png')}}" />
		<link rel="apple-touch-icon" sizes="144x144" href="{{url('/assets/img/apple-icons/apple-touch-icon-144x144.png')}}" />
		<link rel="apple-touch-icon" sizes="152x152" href="{{url('/assets/img/apple-icons/apple-touch-icon-152x152.png')}}" />
		<link rel="apple-touch-icon" sizes="180x180" href="{{url('/assets/img/apple-icons/apple-touch-icon-180x180.png')}}" />

		<!--Import Google Icon Font-->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:300i,400,600,700|Roboto:300,400,400i,500,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		
		<!--Import materialize.css-->

		<link type="text/css" rel="stylesheet" href="{{url('/assets/css/materialize.css')}}" />

		<link type="text/css" rel="stylesheet" href="{{url('/assets/css/jquery.comiseo.daterangepicker.css')}}" />

		<link type="text/css" rel="stylesheet" href="{{url('/assets/css/theme-style.css')}}" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.min.css" />
		<link rel="stylesheet" href="{{url('/assets/css/cropper.min.css')}}">
  		<link rel="stylesheet" href="{{url('/assets/css/main.css')}}">
  		<link rel="stylesheet" href="{{url('/assets/css/featherlight.min.css')}}">
  		<link rel="stylesheet" type="text/css" href="{{url('/assets/css/flexslider.css')}}">
  		<link rel="stylesheet" type="text/css" href="{{url('/assets/css/jquery-ui.multidatespicker.css')}}">

        <script type="text/javascript" src="{{url('/assets/js/jquery.min.js')}}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/js/Jcrop.min.js"></script>
		<script type="text/javascript" src="{{url('/assets/js/push.min.js')}}"></script>


		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
	<div class="wrapper">
		<div class="notification-wrapper border">
			<button class="chips red">{{ Lang::get('common.close') }}</button>
			<ul>
				@if(!empty($notification))
					@foreach($notification as $key=>$value)
						<li><a href="javascript:void(0);">{{ $value['notification'] }}</a><span> {{ $notification_difference[$key] }}</span></li>
					@endforeach
				@else
					<li><a href="javascript:void(0);">{{ Lang::get('common.no-notification-available') }}</a></li>
				@endif
			</ul>
			<input type="hidden" name="user_2" value="@if(!empty($notification)){{ $notification[0]['user_2'] }}@endif">
		</div>
		<header class="header">
		    <div class="had-container">
		      	<div class="row">
		        	<div class="col s2">
		        		<div class="logo-wrapper">
		          			<a href="{{url('/dashboard')}}"><img src="{{url('/')}}/uploads/company-logo/company-logo.png" alt="logo"></a>
		          		</div>
		        	</div>
		        	<div class="col s7 search-show">
		          		<nav class="top-menu clear-fix">
		            		<ul>
		              			<li><a href="{{url('/dashboard')}}"  @if($active=="dashboard") class="active" @endif>{{ Lang::get('menu.dashboard') }}</a></li>
		              			@if($permission['contact']['view']=="on")<li><a href="{{url('/contacts')}}" @if($active=="contact") class="active" @endif>{{ Lang::get('menu.contact') }}</a></li>@endif
		              			@if($permission['deal']['view']=="on")<li><a href="{{url('/deals')}}" @if($active=="deal") class="active" @endif>{{ Lang::get('menu.deals') }}</a></li>@endif
		              			<!-- @if($permission['task']['view']=="on")<li><a href="{{url('/tasks')}}" @if($active=="task") class="active" @endif>{{ Lang::get('menu.tasks') }}</a></li>@endif -->
		              			@if($permission['estimate']['view']=="on")<li><a href="{{url('/estimates')}}" @if($active=="estimate") class="active" @endif>{{ Lang::get('menu.estimate') }}</a></li>@endif
		              			@if($permission['invoice']['view']=="on")<li><a href="{{url('/invoices')}}" @if($active=="invoice") class="active" @endif>{{ Lang::get('menu.invoice') }}</a></li>@endif
		              			<!-- <li><a href="{{url('/reports')}}" @if($active=="reports") class="" @endif> {{ Lang::get('menu.report') }} </a></li> -->
		              			<li><a href="{{url('/products')}}" @if($active=="products") class="active" @endif> {{ Lang::get('menu.product') }}</a></li>
		              			<li><a href="{{url('/call')}}" @if($active=="call") class="active" @endif> Call Record</a></li>
		            		</ul>
		          		</nav> <!-- END NAV -->
		        	</div> <!-- END COL -->
		          	<div class="col s3">
		          		<div class="right-nav">
			          		<div class="profile clear-fix">
			            		<a href="javascript:void(0);">
			              			<span class="profile-pic">
			                			<img src="{{ $profile_image }}" class="image-avatar" alt="avatar">
			              			</span> <!-- END PROFILE PIC -->  
			             			<div class="profile-info">
			                			<h5 class="username">Hi {{ Auth::user()->first_name }}, {{ Lang::get('invoice-estimate-common-template-lang.welcome-to') }} @php echo str_replace('-', ' ', env('PORTAL_NAME')) @endphp <span class="dwn-arrow"></span>
			                			</h5>
			                			<div class="dropdown-menu profile-usermenu border menu-close">
			                				<ul>
			                					@if($permission['setting']['view']=="on")<li><a href="{{ url('/') }}/settings" @if($active=="setting") class="active" @endif>{{ Lang::get('settings.settings') }}</a></li>@endif
			                					<li><a href="{{ url('/') }}/manage-account" @if($active=="manage") class="active" @endif>{{ Lang::get('settings.manage-account') }}</a></li>
			                					@if($show_tutorial!="never")<li><a class="" href="#tutorial">{{ Lang::get('settings.tutorial') }}</a></li>@endif
			                					<li><a href="{{ route('logout') }}">{{ Lang::get('settings.logout') }}</a></li>
			                				</ul>
			                			</div>
			              			</div>  <!-- END PROFILE INFO -->  
			            		</a>
			          		</div> <!-- END PROFILE -->
			          		<div class="notification">
			            		<button type="button" class="notify-icon show-notification"><span> @if(!empty($notification)){{ sizeof($notification) }} @endif </span></button>
			            		<a class="search-cross" href=""></a>
			          		</div>
			          	</div>
		          	</div> <!-- END COL -->
		        </div> <!-- END ROW -->
		    </div> <!-- END CONTAINER -->
		</header> <!-- END HEADER PART -->

		@yield('content')

		<!-- Modal Structure -->
		@if(!empty($contacts))
		<div id="modal3" class="modal modal-fixed-footer">
			<button class="modal-close"></button>
			<div class="modal-wrapper">
				<div class="modal-content">
					<div class="row">
						<div class="col s12">
							<h3>{{ Lang::get('common.add') }} {{ Lang::get('task.task') }}</h3>
						</div> <!-- END COL -->
					</div> <!-- END ROW -->
					<form action="save-new-task" id="add-new-task-form" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="row">
							<div class="col s12">
								<div class="form-layout clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field select-field">
												<input type="hidden" name="contact_id" value="0">
									          	<select name="related_deal" id="related_deal">
									          		<option value="" disabled selected>{{ Lang::get('common.select') }} {{ Lang::get('deal.Deal') }}</option>
									          		@if(!empty($deal))
									          			@foreach($deal as $key=>$value)
									          				<option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
									          			@endforeach
									          		@else
									          			<option value="0">{{ Lang::get('common.no') }} {{ Lang::get('deal.Deal') }}</option>
									          		@endif
									          	</select>
									        </div>
										</div>
									</div>
								</div> <!-- END FORM LAYOUT -->
								<div class="form-layout clear-fix">
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field">
									          	<input name="task_title" type="text" class="validate">
									          	<span class="error-mark"></span>
									          	<span class="check-mark"></span>
									          	<label for="new-task">{{ Lang::get('common.untitled') }} {{ Lang::get('task.task') }}</label>
									        </div>
										</div>
									</div>
									<div class="form-layout-100">
										<div class="field-group">
											<div class="input-field">
									          	<input name="task_details" type="text" class="validate">
									          	<span class="error-mark"></span>
									          	<span class="check-mark"></span>
									          	<label for="details">{{ Lang::get('task.task') }} {{ Lang::get('common.details') }}</label>
									        </div>
										</div>
									</div>
								</div> <!-- END FORM LAYOUT -->
								<div class="form-layout form-layout-50x50 clear-fix">
									<div class="form-layout-50">
										<div class="field-group">
											<div class="input-field select-field">
											    <select name="assigned_to">
											    	<option value="" disabled selected>{{ Lang::get('task.assigned-to') }}</option>
											    	@foreach($contacts as $value)
											    		<option value="{{ $value['id'] }}">{{ $value['first_name']}} {{$value['last_name']}}</option>
													@endforeach
												</select>
												<span class="error-mark"></span>
									          	<span class="check-mark"></span>
											</div>
										</div>
									</div>
									<div class="form-layout-50">
										<div class="field-group">
											<div class="input-field date-field">
									          	<input type="text" name="due_date" id="date" class="form-control" data-dtp="dtp_kiaQI" placeholder="Due on">
									          	<span class="error-mark"></span>
									          	<span class="check-mark"></span>
									          	<span></span>
									        </div>
										</div>
									</div>
								</div> <!-- END FORM LAYOUT -->
							</div> <!-- END COL -->
						</div> <!-- END ROW -->
						<div class="buttons center">
							<button class="waves-effect waves-light btn btn-dark-orange cancel-modal">{{ Lang::get('common.cancel') }}</button>
							<button class="waves-effect waves-light btn createTask btn-green" disabled>{{ Lang::get('common.save') }}</button>
						</div> <!-- END BUTTONS -->
					</form> <!-- END FORM -->
				</div> <!-- END MODAL CONTENT -->
			</div> <!-- END MODAL WRAPPER -->
		</div> <!-- END MODAL -->
		@endif
		@php
		echo View::make('dashboard.modal.modal', array('name' => 'Taylor'));
		@endphp
        <div class="showbox-wrapper">
            <div class="showbox">
               	<div class="loader-wrapper">
	               	<div class="loader">
	                  	<svg class="circular" viewBox="25 25 50 50">
	                     	<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
	                  	</svg>
	               	</div> <!-- END LOADER -->
               	</div> <!-- END LOADER WRAPPER -->
            </div> <!-- END SHOWBOX -->
        </div> <!-- END SHOWBOX WRAPPER -->
	</div> <!-- END WRAPPER -->


	<div id="tutorial" class="modal modal-fixed-footer">
		<button class="modal-close skip_tutorial"></button>
		<div class="modal-wrapper">
			<div class="modal-content">
			  	<div class="flexslider">
			      	<ul class="slides">
				        <li>
				            <img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" />
				        </li>
				        <li>
				            <img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" />
				        </li>
				        <li>
				            <img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" />
				        </li>
			    	</ul>
			 	</div>
			  	<div class="buttons center">
			    	<button class="waves-effect waves-light btn blue view_later_tutorial" href="">{{ Lang::get('common.watch-later') }}</button>
			    	<button class="waves-effect waves-light btn green skip_tutorial" href="">{{ Lang::get('common.skip') }}</button>
			  	</div>
			</div>
		</div>
	</div>

	


	<script type="text/javascript" src="{{url('/assets/js/moment-with-locales.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/assets/js/material.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/assets/js/bootstrap-material-datetimepicker.js')}}"></script>
	<script type="text/javascript" src="{{url('/assets/js/materialize.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/assets/js/jquery-ui.js')}}"></script>
	<script type="text/javascript" src="{{url('/assets/js/featherlight.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/assets/js/dragula.min.js')}}"></script>

	<script type="text/javascript" src="{{url('/assets/js/jquery.comiseo.daterangepicker.js')}}"></script>

	<script type="text/javascript" src="{{url('/assets/js/list.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/assets/js/Chart.bundle.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/utils.js')}}"></script>

    <script type="text/javascript" src="{{url('/assets/js/jquery.flexslider.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/chart.funnel.bundled.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/jquery.form.min.js')}}"></script>


	<script type="text/javascript">
		var APP_URL = {!! json_encode(url('/')) !!}
	</script>

	<script type="text/javascript">

		//CONTACT LIST
		var contact_options = {
			valueNames: [ 'firstName', 'lastName','email','phone','address' ]
		};

		var userList = new List('contacts-list', contact_options);
		//INVOICE LIST
		var invoice_options = {
			valueNames: [ 'inv_no', 'contact','created_at','value','status' ]
		};

		var userList = new List('invoice-list', invoice_options);
		//ESTIMATE LIST
		var estimate_options = {
			valueNames: [ 'est_no', 'contact','created_at','value','status' ]
		};

		var userList = new List('estimate-list', estimate_options);

		console.log(randomScalingFactor());
	</script>

	<script type="text/javascript" src="{{url('/assets/js/cropper.min.js')}}"></script>
  	<script type="text/javascript" src="{{url('/assets/js/custom-cropper.js')}}"></script>
	<script type="text/javascript" src="{{url('/assets/js/main.js')}}"></script>
	<script type="text/javascript" src="{{url('/assets/js/dashboard.js')}}"></script>
	<script type="text/javascript" src="{{url('/assets/js/img-edit.js')}}"></script>
	<script type="text/javascript" src="{{url('/assets/js/push.min.js')}}"></script>

	<script type="text/javascript" src="{{url('/assets/js/jquery-ui.multidatespicker.js')}}"></script>

	

	<script type="text/javascript">
	    paceOptions = {
	    	ajax: true,
	      	elements: true,
	      	initialRate: .5
	    };
	    $( function() {
		    $( "#files" ).selectmenu();
		});

     	$("#e2").daterangepicker({
			datepickerOptions : {
				numberOfMonths : 2
			}
		});
		
	</script>

</body>
</html>