@extends('layouts.home-header')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col s10 offset-s1">
				<div class="login-inner-content-wrapper">
					<div class="login-inner-content">
						<div class="login-inner border clear-fix">
							<div class="login-left-side left">
								<div class="signin-form-wrapper" style="opacity:@if($purpose=='login') 1 @else 0 @endif">
									<h2 class="blue-text">{{ Lang::get('invoice-estimate-common-template-lang.welcome-to') }} </br>{{ $portal_name }}</h2>
									<form class="signin-form" id="login-form" action="{{ url('/') }}/session/login" role="form" method="POST">
										{{ csrf_field() }}
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50x50 clear-fix">
													<div class="form-layout-100 add-email">
														<div class="field-group">
															<div class="input-field <?php if ($errors->has('email')) echo 'input-invalid'; ?>">
													          	<input id="signin_email" name="email" type="email" class="validate" value="{{ old('email') }}">
													          	@if ($errors->has('email'))
								                                    <span class="help-block">
								                                        <strong>{{ $errors->first('email') }}</strong>
								                                    </span>
								                                @endif
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="log-email">{{ Lang::get('common.email') }}</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-100 wrong-password">
														<div class="field-group">
															<div class="input-field">
													          	<input id="signin_password" name="password" type="password" class="validate">
													          	@if ($errors->has('password'))
								                                    <span class="help-block">
								                                        <strong>{{ $errors->first('password') }}</strong>
								                                    </span>
								                                @endif
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="password">{{ Lang::get('common.password') }}</label>
													          	<span class="wrong-pass">{{ Lang::get('common.forgot') }} {{ Lang::get('common.password') }}. <a href="javascript:void(0)" class="need-help">{{ Lang::get('forgot-password-lang.need-help') }}</a></span>
													        </div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
										<div class="row">
											<div class="col s12 center submit-button">
												<button class="waves-effect waves-light btn green" id="login-button" disabled>{{ Lang::get('forgot-password-lang.sign-in') }}</button>
												<button class="waves-effect waves-light btn teal-lighten-one green" id="sendResetPasswordEmail">{{ Lang::get('forgot-password-lang.send-the-link') }}</button>
												<span id="backToSignIn" style="display:none;">{{ Lang::get('forgot-password-lang.back-to-sign-in') }}</span>
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
									</form>  <!-- END FORM -->
									<div class="row">
										<div class="col s12">
											<span class="success-message">{{ Lang::get('forgot-password-lang.please-check-your-mail') }}</span>
										</div> <!-- END COL -->
									</div> <!-- END ROW -->
								</div> <!-- END SIGN IN FORM WRAPPER -->
								<div class="signup-form-wrapper"  style="@if($purpose=='signup')  opacity:1; z-index:2;  @else  opacity:0; z-index:-1 @endif">
									<h2 class="blue-text">{{ Lang::get('forgot-password-lang.welcome-to-pulse') }}</h2>
									<form class="signup-form" id="signup-form" role="form" method="POST" action="{{ route('register') }}">
										{{ csrf_field() }}
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50x50 clear-fix">
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
																<input type="hidden" name="role" value="{{ $role }}">
																<input id="full_name" name="name" type="hidden" class="validate" value="">
													          	<input id="first_name" name="first_name" type="text" class="validate" value="@if(isset($first_name)) {{ $first_name }} @endif">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="first_name" class="@if(isset($first_name)) active @endif">{{ Lang::get('common.first') }} {{ Lang::get('common.name') }}</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<input id="last_name" name="last_name" type="text" class="validate" value="@if(isset($last_name)) {{ $last_name }} @endif">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="last_name" class="@if(isset($last_name)) active @endif">{{ Lang::get('common.last') }} {{ Lang::get('common.name') }}</label>
													        </div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50x50 clear-fix">
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<input id="signup-email" name="email" type="email" class="validate" value="@if(isset($email)) {{ $email }} @endif">
													          	@if ($errors->has('email'))
								                                    <span class="help-block">
								                                        <strong>{{ $errors->first('email') }}</strong>
								                                    </span>
								                                @endif
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="email" class="@if(isset($email)) active @endif">{{ Lang::get('common.email') }}</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<select name="company_name" disabled>
													          		<option value="0" disabled selected>{{ Lang::get('common.choose') }} {{ Lang::get('common.company') }}</option>
													          		@foreach($company as $value)
													          			<option value="{{ $value['id'] }}" @if($user_company==$value['id']) selected @endif>{{ $value['company_name'] }}</option>
													          		@endforeach
													          	</select>
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													        </div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
										<div class="row">
											<div class="col s12">
												<div class="form-layout form-layout-50x50 clear-fix">
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<input id="signup-password" name="password" type="password" class="validate">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="password">{{ Lang::get('common.password') }}</label>
													        </div>
														</div>
													</div>
													<div class="form-layout-100">
														<div class="field-group">
															<div class="input-field">
													          	<input id="password-confirm" name="password_confirmation" type="password" class="validate">
													          	<span class="error-mark"></span>
													          	<span class="check-mark"></span>
													          	<label for="password-confirm">{{ Lang::get('common.repeat-password') }}</label>
													        </div>
														</div>
													</div>
												</div> <!-- END FORM LAYOUT -->
											</div> <!-- END COL -->
										</div> <!-- END ROW -->
									</form> <!-- END FORM -->
									<div class="row">
										<div class="col s12 center submit-button">
											<button class="waves-effect waves-light btn btn-orange" id="signup-button" disabled>{{ Lang::get('common.sign-up') }}</button>
										</div> <!-- END COL -->
									</div> <!-- END ROW -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection