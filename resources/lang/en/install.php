<?php
return array(
	'installation-step' => 'Installation Step',
	'database-configaration' => 'Database Configaration',
	'admin-configaration' => 'Admin Configaration',
	'company-details' => 'Company Details',
	'database-host' => 'Database Host',
	'database-name' => 'Database Name',
	'database-username' => 'Database Username',
	'database-password' => 'Database Password',
	'timezone' => 'Timezone',
	'Next' => 'Next'
);
?>