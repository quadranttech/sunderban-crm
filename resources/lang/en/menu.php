<?php
return array(
	'dashboard' => 'dashboard',
	'contact' => 'contact',
	'deals' => 'deals',
	'tasks' => 'tasks',
	'estimate' => 'estimate',
	'invoice' => 'invoice',
	'report' => 'Report',
	'product' => 'Product'
);
?>