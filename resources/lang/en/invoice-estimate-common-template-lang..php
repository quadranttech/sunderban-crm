<?php
return array(
	'from' => 'from',
	'to' => 'to',
	'item-name' => 'Item Name',
	'rate' => 'Rate',
	'quantity' => 'Quantity',
	'total' => 'Total',
	'sub-total' => 'Sub-Total',
	'adjustments' => 'Adjustment(s)',
	'discount' => 'Discount',
	'tax' => 'Tax',
	'thank-you' => 'THANK YOU',
	'this-invoice-is-generated-using' => 'This invoice is generated using',
	'team' => 'Team',
	'made-by' => 'Made by',
	'view-in-browser' => 'View in Browser',
	'download-pdf' => 'Download PDF'
);
?>