<?php
return array(
	'task' => 'Task',
	'tasks' => 'Tasks',
	'assigned-to' => 'Assigned to',
	'due-on' => 'Due on',
	'assigned-to-all' => 'Assign To All',
	'mark-complete' => 'Mark complete',
	'change-due-date' => 'Change due date',
	'assign-others' => 'Assign others',
	'more-tags' => 'more tags',
	'assign-to-me' => 'Assign to Me',
	'due-in' => 'Due in',
	'day' => 'day',
	'days' => 'days'
);
?>