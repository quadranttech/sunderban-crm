<?php
return array(
	'1st-phrase' => "And thanks for joining our sales team! Here's a quick guide to help you get started",
	'2nd-phrase' => 'Add leads as a contact. You can even group them by creating the company they are related to',
	'3rd-phrase' => 'Start recording opportunities by creating deals. Now move the deals in the sales pipe like you do in real life',
	'4th-phrase' => 'Send estimates, or quotes to your contacts. When the customer accepts the estimate, it automatically gets converted to an invoice',
	'5th-phrase' => 'Have a deal which has to be billed- just click that Create Invoice and you are good to go. You can do invoicing in multiple currencies and get paid using Stripe / Paypal',
	'happy-selling' => 'Happy Selling'
);
?>