<?php
return array(
	'Invoice' => 'Invoice',
	'Invoices' => 'Invoices',
	'invoice' => 'invoice',
	'invoices' => 'invoices',
	'order-no' => 'Order No',
	'select-default-payment-terms' => 'Select Default Payment Terms',
	'send-invoice' => 'Send Invoice',
	'invoice-subject' => 'Invoice subject',
	'invoice-preview' => 'Invoice Preview',
	'invoice-id-exists' => 'Invoice id exists'
);
?>