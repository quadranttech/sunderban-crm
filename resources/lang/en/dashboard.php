<?php
return array(
	'company-dashboard' => 'Company Dashboard',
	'personal-dashboard' => 'Personal Dashboard',
	'no-data-yet-lets-start-now' => 'No data yet, lets start now',
	'current-month' => 'Current Month',
	'monthly-sales-figures' => 'Monthly Sales Figures',
	'lead-figures-current-month' => 'Lead Figures - Current Month',
	'invoices-sent' => 'Invoices sent',
	'invoices-paid' => 'Invoices paid',
	'estimates-sent' => 'Estimates sent',
	'estimates-accepted' => 'Estimates Accepted',
	'deals-won-by-you' => 'Deals won by you',
	'new-deals-by-you' => 'New Deals by You',
	'your-due-tasks' => 'Your Due Tasks',
	'contacts-added-by-you' => 'Contacts Added by You'
);
?>