<?php
return array(
	'deal' => 'deal',
	'deals' => 'deals',
	'Deal' => 'Deal',
	'Deals' => 'Deals',
	'opportunity' => 'opportunity',
	'proposal' => 'proposal',
	'negotiation' => 'negotiation',
	'complete' => 'complete',
	'source' => 'source',
	'expected-revenue' => 'Expected Revenue',
	'win-probability' => 'Win Probability',
	'stage' => 'Stage',
	'card' => 'Card',
	'pipe' => 'Pipe',
	'deal-priority' => 'Deal Priority',
	'owner' => 'Owner',
	'priority' => 'Priority'
);
?>