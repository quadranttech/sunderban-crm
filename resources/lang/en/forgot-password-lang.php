<?php
return array(
	'forgot-password' => 'Forgot your password?',
	'nothing-to-worry' => "Nothing to worry here. Let's get you a new password.",
	'reset-password' => 'Reset Password',
	'still-facing-issues' => 'Still facing issues, please contact your CRM administrator.',
	'need-help' => 'Need Help?',
	'sign-in' => 'Sign In',
	'send-the-link' => 'Send the Link',
	'back-to-sign-in' => 'Back to Sign in',
	'please-check-your-mail' => 'Please check your mail',
	'welcome-to-pulse' => 'Welcome To Pulse',
	'back-to-login' => 'Back To Login',
	'password-changed-successfully' => 'Password changed successfully'
);
?>