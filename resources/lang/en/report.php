<?php
return array(
	'reports' => 'Reports',
	'total-sales' => 'Total Sales',
	'total-received' => 'Total Received',
	'sales-source' => 'Sales Source',
	'contact-name' => 'Contact Name',
	'staff-name' => 'Staff Name',
	'invoice-status' => 'Invoice Status',
	'estimate_status' => 'Estimate Status',
	'last-week' => 'Last Week',
	'current-week' => 'Curent Week',
	'current-month' => 'Curent Month',
	'last-month' => 'Last Month',
	'last-three-month' => 'Last Three Month'
);
?>