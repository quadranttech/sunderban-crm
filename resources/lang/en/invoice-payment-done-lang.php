<?php
return array(
	'dear' => 'Dear',
	'thank-you-for-making-payment-of' => 'Thank you for making payment of',
	'download-the-invoice-with-update-payment-status-from-here:' => 'Download the invoice with updated payment status from here:',
	'looking-forward-to-working-together-soon!' => 'Looking forward to working together soon!'
);
?>