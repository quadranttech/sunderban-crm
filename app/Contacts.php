<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Contacts extends Model
{
    //
    protected $fillable = ['title','first_name', 'last_name', 'email', 'phone', 'im_profile', 'social_profile', 'company','contact_type', 'created_by','created_at', 'updated_at'];
    protected $table = 'contacts';

    public function checkIfEmailExist($email)
    {
    	$result = DB::table('contacts')->whereRaw("email REGEXP '".$email."'")->first();
        $result = json_decode(json_encode($result), true);
        return $result;
    }
}
