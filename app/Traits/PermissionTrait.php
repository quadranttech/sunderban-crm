<?php
namespace App\Traits;

use Request;
use View;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use Carbon;
use File;
use App\Feed;
use Mail;

trait PermissionTrait {
    public function setPermissionToUser($user_id,$contact,$deal,$task,$invoice,$estimate,$setting) {
        $setPermissionToUser=DB::table('user_permissions')->insertGetId(['user_id' => $user_id, 'contact' => $contact, 'deal' => $deal, 'task' => $invoice, 'invoice' => $invoice, 'estimate' => $estimate, 'setting' => $setting]);
        return $setPermissionToUser;
    }

    public function getUserPermission($user_id)
    {
    	$getUserPermission=DB::table('user_permissions')->select('*')->where('user_id',$user_id)->first();
    	$getUserPermission=json_decode(json_encode($getUserPermission), true);
        $permissionArray=array();
        $permissionArray['id']=$getUserPermission['id'];
        $permissionArray['user_id']=$getUserPermission['user_id'];
        $permissionArray['contact']=unserialize($getUserPermission['contact']);
        $permissionArray['deal']=unserialize($getUserPermission['deal']);
        $permissionArray['task']=unserialize($getUserPermission['task']);
        $permissionArray['invoice']=unserialize($getUserPermission['invoice']);
        $permissionArray['estimate']=unserialize($getUserPermission['estimate']);
        $permissionArray['setting']=unserialize($getUserPermission['setting']);
    	return $permissionArray;
    }

    public function updateUserPermission($user_id,$contact,$deal,$task,$invoice,$estimate,$setting)
    {
        $updateUserPermission=DB::table('user_permissions')->where('user_id',$user_id)->update(['contact' => $contact, 'deal' => $deal, 'task' => $task, 'invoice' => $invoice, 'estimate' => $estimate, 'setting' => $setting]);
        return $updateUserPermission;
    }
}