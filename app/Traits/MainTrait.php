<?php
namespace App\Traits;

use Request;
use View;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use Carbon;
use File;
use App\Feed;
use Mail;
use Swift_SmtpTransport;
use Propaganistas\LaravelIntl\Facades\Currency;
use XmlParser;
use Lang;

trait MainTrait {
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->currency_symbol = [
            'USD'=> '$', // US Dollar
            'EUR'=> '€', // Euro
            'CRC'=> '₡', // Costa Rican Colón
            'GBP'=> '£', // British Pound Sterling
            'ILS'=> '₪', // Israeli New Sheqel
            'INR'=> '₹', // Indian Rupee
            'JPY'=> '¥', // Japanese Yen
            'KRW'=> '₩', // South Korean Won
            'NGN'=> '₦', // Nigerian Naira
            'PHP'=> '₱', // Philippine Peso
            'PLN'=> 'zł', // Polish Zloty
            'PYG'=> '₲', // Paraguayan Guarani
            'THB'=> '฿', // Thai Baht
            'UAH'=> '₴', // Ukrainian Hryvnia
            'VND'=> '₫' // Vietnamese Dong
        ];
    }

    public function getStructuredTaskArray($getTasks) {
    	$task_array=array();
        foreach($getTasks as $value)
        {
            $temp=array();
            if($value['due_date']!="0000-00-00 00:00:00")
            {
                $current_date=date('Y-m-d', strtotime(date("Y-m-d")));
                $due_date=date('Y-m-d', strtotime($value['due_date']));
                $difference=strtotime($due_date) - strtotime($current_date);
                $difference=floor($difference/(3600*24));
                // echo $difference;
                if($difference==0)
                {
                    $difference='Due Today';
                }
                else
                {
                    $difference='Due in '.$difference.' day';
                }
            }
            else
            {
                $difference="No due date";
            }
            if($value['assigned_to']!="0")
            {
                $getAssignedName=DB::table('users')->select('*')->where('id' , $value['assigned_to'])->get();
                $getAssignedName=json_decode(json_encode($getAssignedName), true);
                $getAssignedName="Assigned to ".$getAssignedName[0]['first_name'].' '.$getAssignedName[0]['last_name'];
            }
            else
            {
                $getAssignedName="Not Assigned to anybody";
            }
            $getTags=DB::table('task')->select('task_tag')->where('id',$value['id'])->first();
            $getTags=json_decode(json_encode($getTags), true);
            // echo "<pre>";
            // print_r($getTags);
            // echo "<pre>";
            // echo $getTags['task_tag'];
            // die();
            $tagsNameArray=array();
            $tagsColorArray=array();
            if($getTags['task_tag']!="")
            {
                $getTagsArray=unserialize($getTags['task_tag']);
                // echo "<pre>";
                // print_r($getTagsArray);
                // echo "</pre>";
                // die();
                
                foreach($getTagsArray as $inner_key=>$inner_value)
                {
                    $getTagName=DB::table('task_tags')->select('*')->where('id',$inner_value)->first();
                    $getTagName=json_decode(json_encode($getTagName), true);
                    array_push($tagsNameArray,$getTagName['tag']);
                    array_push($tagsColorArray,$getTagName['color']);
                }
            }
            
            // echo "<pre>";
            // print_r($tagsNameArray);
            // echo "</pre>";
            // echo "<pre>";
            // print_r(unserialize($getTags['task_tag']));
            // echo "</pre>";
            // die();
            $temp=['id' => $value['id'], 'task_title' => $value['task_title'], 'task_description' => $value['task_description'], 'due_date' => $difference, 'assigned_to' => $getAssignedName, 'assigned_to_id' => $value['assigned_to'], 'marked_as' => $value['marked_as'], 'tagsNameArray' => $tagsNameArray, 'tagsColorArray' => $tagsColorArray, 'color' => $value['color']];
            array_push($task_array,$temp);
            // echo "<pre>";
            // print_r(json_decode(json_encode($getAssignedName), true));
            // echo "</pre>";
            // echo $getAssignedName[0]['first_name'];
            // die();
        }
        return $task_array;
        
    }

    public function createContactCompany($data)
    {
    	$createCompany=DB::table('company')->insertGetId(['company_name' => $data['company_name'], 'address_type' => $data['address_type'], 'address' => $data['address'], 'city' => $data['city'], 'state' => $data['state'], 'country' => $data['country'], 'post_code' => $data['post_code'], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
    	return $createCompany;
    }

    public function get_meta($meta_key,$company_id)
    {
        $getMetaValue=DB::table('settings')->select('*')->where('company_id',$company_id)->where('meta_key',$meta_key)->first();
        return $getMetaValue;
    }

    public function update_meta($meta_key,$meta_value,$company_id)
    {
        $updateMeta=DB::table('settings')->where('meta_key',$meta_key)->where('company_id',$company_id)->update(['meta_value' => $meta_value]);
        return $updateMeta;
    }

    public function setEnvSmtpDetails($smtp_host,$smtp_port,$smtp_username,$smtp_password)
    {
        $path = base_path('.env');

        if (file_exists($path)) {
            file_put_contents($path, str_replace(
                'MAIL_HOST='.env("MAIL_HOST", ""), 'MAIL_HOST='.$smtp_host, file_get_contents($path)
            ));
            file_put_contents($path, str_replace(
                'MAIL_PORT='.env("MAIL_PORT", ""), 'MAIL_PORT='.$smtp_port, file_get_contents($path)
            ));
            file_put_contents($path, str_replace(
                'MAIL_USERNAME='.env("MAIL_USERNAME", ""), 'MAIL_USERNAME='.$smtp_username, file_get_contents($path)
            ));
            file_put_contents($path, str_replace(
                'MAIL_PASSWORD='.env("MAIL_PASSWORD", ""), 'MAIL_PASSWORD='.$smtp_password, file_get_contents($path)
            ));
            
            return response()->json(['status' => 'success']);
        }
    }

    public function sendEstimateMail($contact,$items_array,$inv_total,$shipping_cost,$discount,$tax,$adjustment,$total,$key,$show_on_page,$terms_and_conditions,$link,$estimate_title,$logo)
    {
        $getContactDetails=DB::table('contacts')->select('*')->where('id',$contact)->first();
        $getContactAddressDetails=DB::table('contacts_address')->join('contacts', 'contacts.id', '=', 'contacts_address.contact_id')->select('*')->where('contacts.id', '=',$contact)->first();
        $getContactAddressDetails=json_decode(json_encode($getContactAddressDetails), true);
        $contact_address="";
        $contact_country="";
        $contact_state="";
        $contact_post_code="";
        if($getContactAddressDetails!="")
        {
            $contact_address=$getContactAddressDetails['street_address'];
            $contact_country=$getContactAddressDetails['country'];
            $contact_state=$getContactAddressDetails['state'];
            $contact_post_code=$getContactAddressDetails['post_code'];
        }
        $getContactDetails=DB::table('contacts')->select('*')->where('id',$contact)->first();
        $getContactDetails=json_decode(json_encode($getContactDetails), true);
        $contact_name=$getContactDetails['first_name']." ".$getContactDetails['last_name'];
        $getAdminCompanyDetails=DB::table('admin_company')->select('*')->first();
        $getAdminCompanyDetails=json_decode(json_encode($getAdminCompanyDetails), true);
        $company_name=$getAdminCompanyDetails['company_name'];
        $company_address=$getAdminCompanyDetails['address'];
        $company_country=$getAdminCompanyDetails['country'];
        $company_state=$getAdminCompanyDetails['state'];
        $company_post_code=$getAdminCompanyDetails['post_code'];
        $sender_name=$getAdminCompanyDetails['sender_name'];
        if($sender_name=="")
        {
            $getAdminName=DB::table('users')->select('*')->where('role','1')->first();
            $getAdminName=json_decode(json_encode($getAdminName), true);
            $sender_name=$getAdminName['first_name'];
        }
        $sender_email=$getAdminCompanyDetails['default_email'];
        if($sender_email=="" || $getAdminCompanyDetails['marked_as_default']=="no")
        {
            $getAdminName=DB::table('users')->select('*')->where('role','1')->first();
            $getAdminName=json_decode(json_encode($getAdminName), true);
            $sender_email=$getAdminName['email'];
        }
        if(!empty(unserialize($getContactDetails['email'])['work']))
        {
            $email=unserialize($getContactDetails['email'])['work'][0];
        }
        else
        {
            $email=unserialize($getContactDetails['email'])['home'][0];
        }
        // return $email;
        $accept_link=$this->url->to('/').'/accept-estimate/'.$key;
        $decline_link=$this->url->to('/').'/decline-estimate/'.$key;
        $view_on_browser_link=$this->url->to('/').'/view-estimate-on-browser/'.$link;
        if($show_on_page==true)
        {
            $data=['items_array'=>$items_array,'inv_total'=>$inv_total,'discount'=>$discount,'tax'=>$tax,'total'=>$total,'contact_address'=>$contact_address,'contact_country'=>$contact_country,'contact_state'=>$contact_state,'contact_post_code'=>$contact_post_code,'contact_name'=>$contact_name,'company_name'=>$company_name,'company_address'=>$company_address,'company_country'=>$company_country,'company_state'=>$company_state,'company_post_code'=>$company_post_code,'accept_link'=>$accept_link,'decline_link'=>$decline_link];
            return $data;
        }
        $link=$this->url->to('/').'/show-estimate-pdf/'.$link;
        $send=$this->send_estimate_mail($email,$items_array,$inv_total,$discount,$tax,$adjustment,$total,$contact_address,$contact_country,$contact_state,$contact_post_code,$contact_name,$company_name,$company_address,$company_country,$company_state,$company_post_code,$sender_name,$sender_email,$accept_link,$decline_link,$terms_and_conditions,$link,$estimate_title,$items_array[0]['quantity_type'],$view_on_browser_link,$logo);
        return $email;
        // echo "<pre>";
        // print_r($getContactDetails);
        // echo "</pre>";
        // die();
    }

    public function send_estimate_mail($email,$items_array,$inv_total,$discount,$tax,$adjustment,$total,$contact_address,$contact_country,$contact_state,$contact_post_code,$contact_name,$company_name,$company_address,$company_country,$company_state,$company_post_code,$sender_name,$sender_email,$accept_link,$decline_link,$terms_and_conditions,$link,$estimate_title,$quantity_type,$view_on_browser_link,$logo){
        // echo $email;
        $currency_symbol=[
            'USD'=> '$', // US Dollar
            'EUR'=> '€', // Euro
            'CRC'=> '₡', // Costa Rican Colón
            'GBP'=> '£', // British Pound Sterling
            'ILS'=> '₪', // Israeli New Sheqel
            'INR'=> '₹', // Indian Rupee
            'JPY'=> '¥', // Japanese Yen
            'KRW'=> '₩', // South Korean Won
            'NGN'=> '₦', // Nigerian Naira
            'PHP'=> '₱', // Philippine Peso
            'PLN'=> 'zł', // Polish Zloty
            'PYG'=> '₲', // Paraguayan Guarani
            'THB'=> '฿', // Thai Baht
            'UAH'=> '₴', // Ukrainian Hryvnia
            'VND'=> '₫' // Vietnamese Dong
        ];
        $currency_symbol=$this->getCurrencySymbol('en',$items_array[0]['currency']);
        if($quantity_type=="quantity")
        {
            $quantity_type="Quantity";
        }
        else if($quantity_type=="hr")
        {
            $quantity_type="hr";
        }
        else if($quantity_type=="item")
        {
            $quantity_type="Item";
        }
        else
        {
            $quantity_type="hr + Item";
        }
      $data = array('name'=>'gfgf','items_array'=>$items_array,'inv_total'=>$inv_total,'discount'=>$discount,'tax'=>$tax,'adjustment'=>$adjustment,'total'=>$total,'contact_address'=>$contact_address,'contact_country'=>$contact_country,'contact_state'=>$contact_state,'contact_post_code'=>$contact_post_code,'contact_name'=>$contact_name,'company_name'=>$company_name,'company_address'=>$company_address,'company_country'=>$company_country,'company_state'=>$company_state,'company_post_code'=>$company_post_code,'accept_link'=>$accept_link,'decline_link'=>$decline_link,'view_on_browser_link'=>$view_on_browser_link,'currency_symbol'=>$currency_symbol,'logo'=>$logo,'quantity_type'=>$quantity_type);
      Mail::send('templates/pulse-mailer/estimate-template', $data, function($message) use ($email,$sender_name,$sender_email,$estimate_title) {
         $message->to($email, 'Tutorials Point')->subject
            ($estimate_title);
         $message->from($sender_email,$sender_name);
      });
      // echo "HTML Email Sent. Check your inbox.";
   }

    public function convertEstimateToInvoice($contact_type,$contact,$item_details,$company_id,$shipping_cost,$discount,$tax,$discount_id,$tax_id,$adjustment,$created_by,$part_pay_type,$part_pay,$terms_and_conditions,$quantity_type,$estimate_currency,$estimate_title)
    {
        $getInvoiceStartWith=DB::table('settings')->select('*')->where('meta_key','invoice_start')->where('company_id',$company_id)->first();
        $getInvoiceStartWith=json_decode(json_encode($getInvoiceStartWith), true);
        $getFirstInvoice=DB::table('invoices')->select('*')->where('company_id',$company_id)->get();
        $getFirstInvoice=json_decode(json_encode($getFirstInvoice), true);
        if(!empty($getInvoiceStartWith) && !empty($getFirstInvoice))
        {
            $lastInvoiceNo=explode("-",$getFirstInvoice[sizeof($getFirstInvoice)-1]['inv_no']);
            $lastInvoiceNo=sizeof($getFirstInvoice)+1;
            $generateInvoiceNo=$getInvoiceStartWith['meta_value'].'-'.sprintf('%04d', $lastInvoiceNo);
        }
        if(!empty($getInvoiceStartWith) && empty($getFirstInvoice))
        {
            $generateInvoiceNo=$getInvoiceStartWith['meta_value'].'-'.sprintf('%04d', 1);
        }
        if(empty($getInvoiceStartWith) && !empty($getFirstInvoice))
        {
            $lastInvoiceNo=explode("-",$getFirstInvoice[sizeof($getFirstInvoice)-1]['inv_no']);
            $lastInvoiceNo=sizeof($getFirstInvoice)+1;
            $generateInvoiceNo="INV-".sprintf('%04d', $lastInvoiceNo);
        }
        else if(empty($getInvoiceStartWith) && empty($getFirstInvoice))
        {
            $generateInvoiceNo="INV-".sprintf('%04d', 1);
        }
        $getAdminCompanyDetails=DB::table('admin_company')->join('users', 'users.company', '=', 'admin_company.id')->select('*')->where('users.id', '=', $created_by)->first();
        $getAdminCompanyDetails=json_decode(json_encode($getAdminCompanyDetails), true);
        $company_id=$getAdminCompanyDetails['company'];
        $company_logo=$getAdminCompanyDetails['logo'];
        if($company_logo=="")
        {
            $company_logo=$this->url->to('/').'/assets/img/logo.png';
        }
        else
        {
            $logo=$this->url->to('/').'/uploads/company-logo/'.$getAdminCompanyDetails['logo'];
        }
        $send_date=date("Y-m-d");
        $created_at=date('Y-m-d H:i:s');
        $getDefaultPaymentTerms=DB::table('settings')->select('meta_value')->where('company_id',$company_id)->where('meta_key','default_payment_term')->first();
        $getDefaultPaymentTerms=json_decode(json_encode($getDefaultPaymentTerms), true);
        if(!empty($getDefaultPaymentTerms))
        {
            $getDefaultPaymentTerms=$getDefaultPaymentTerms['meta_value'];
        }
        else
        {
            $getDefaultPaymentTerms="net-15";
        }
        if($terms_and_conditions=="")
        {
            $terms_and_conditions=$this->get_meta('terms_and_conditions',$company_id);
            $terms_and_conditions=json_decode(json_encode($terms_and_conditions), true);
            if(!empty($terms_and_conditions))
            {
                $terms_and_conditions=$terms_and_conditions['meta_value'];
            }
            else
            {
                $terms_and_conditions='';
            }
        }
        $due_date=explode("-",$getDefaultPaymentTerms)[1]-1;
        $due_date=date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + $due_date, date('Y')));
        $link=$this->generateRandomString();
        $createInvoice=DB::table('invoices')->insertGetId(['inv_no' => $generateInvoiceNo, 'company_id' =>$company_id, 'contact' => $contact, 'contact_type' => $contact_type, 'inv_date' => $created_at, 'due_date' => $due_date, 'default_payment_term' => $getDefaultPaymentTerms, 'sales_person' => $created_by, 'status' => 'send', 'currency' => $estimate_currency, 'send_date' => $send_date, 'link' => $link, 'created_at' => $created_at, 'updated_at' => $created_at]);
        $total=0;
        $items_array=array();
        foreach($item_details as $key=>$value)
        {
            $insertInvoiceItems=DB::table('invoice_items')->insertGetId(['inv_id' => $createInvoice, 'item_details' => $value['item_details'], 'rate' => $value['rate'], 'quantity' => $value['quantity'], 'quantity_type' => $quantity_type, 'currency' => $value['currency'], 'total' => $value['rate']*$value['quantity'], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            $total=$total+($value['rate']*$value['quantity']);
            $temp=array();
            $temp['item_details']=$value['item_details'];
            $temp['rate']=$value['rate'];
            $temp['quantity']=$value['quantity'];
            $temp['quantity_type']=$quantity_type;
            $temp['currency']=$value['currency'];
            $temp['total']=$value['rate']*$value['quantity'];
            array_push($items_array,$temp);
        }
        $inv_total=$total+$shipping_cost-$adjustment-$discount;
        $inv_total=$inv_total+$tax;
        $updateInvoicePaymentStatus=DB::table('invoices')->where('id',$createInvoice)->update(['paid' => '0.00', 'due' => $inv_total, 'payment_status' => 'unpaid']);
        if($part_pay_type=="no_part_pay")
        {
            $due=0;
        }
        else
        {
            if($part_pay_type=="fixed")
            {
                $due=$inv_total-$part_pay;
            }
            else
            {
                $due=(($inv_total)-(($part_pay / 100) * $inv_total));
            }
        }
        $insertInvoiceMeta=DB::table('invoice_meta')->insertGetId(['inv_id' => $createInvoice,'attachment' => '', 'tax' => 0, 'tax_id' => 0, 'discount' => 0, 'discount_id' => 0, 'shipping_cost' => 0, 'inv_total' => $inv_total, 'adjustment' => $adjustment, 'tax' => $tax, 'tax_id' => $tax_id, 'discount' => $discount, 'discount_id' => $discount_id, 'shipping_cost' => $shipping_cost, 'part_pay' => $part_pay_type, 'part_pay_value' => $part_pay, 'due' => $due, 'customer_note' => '', 'admin_note' => '', 'terms_and_conditions' => $terms_and_conditions, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        $data=array('id'=>$createInvoice,'inv_no'=>$generateInvoiceNo,'due_date'=>$due_date,'link'=>$link,'estimate_title'=>$estimate_title,'logo'=>$company_logo);
        return $data;
    }

    public function sendInvoiceMail($contact,$items_array,$inv_total,$shipping_cost,$discount,$tax,$adjustment,$total,$createInvoice,$due_date,$terms_and_conditions,$link,$invoices_title,$logo,$message_to_customer,$payment_status)
    {
        $getContactDetails=DB::table('contacts')->select('*')->where('id',$contact)->first();
        $getContactAddressDetails=DB::table('contacts_address')->join('contacts', 'contacts.id', '=', 'contacts_address.contact_id')->select('*')->where('contacts.id', '=',$contact)->first();
        $getContactAddressDetails=json_decode(json_encode($getContactAddressDetails), true);
        $contact_address="";
        $contact_country="";
        $contact_state="";
        $contact_post_code="";
        if($getContactAddressDetails!="")
        {
            $contact_address=$getContactAddressDetails['street_address'];
            $contact_country=$getContactAddressDetails['country'];
            $contact_state=$getContactAddressDetails['state'];
            $contact_post_code=$getContactAddressDetails['post_code'];
        }
        $getContactDetails=DB::table('contacts')->select('*')->where('id',$contact)->first();
        $getContactDetails=json_decode(json_encode($getContactDetails), true);
        $contact_name=$getContactDetails['first_name']." ".$getContactDetails['last_name'];
        $getAdminCompanyDetails=DB::table('admin_company')->select('*')->first();
        $getAdminCompanyDetails=json_decode(json_encode($getAdminCompanyDetails), true);
        $company_name=$getAdminCompanyDetails['company_name'];
        $company_address=$getAdminCompanyDetails['address'];
        $company_country=$getAdminCompanyDetails['country'];
        $company_state=$getAdminCompanyDetails['state'];
        $company_post_code=$getAdminCompanyDetails['post_code'];
        $sender_name=$getAdminCompanyDetails['sender_name'];
        if($sender_name=="")
        {
            $getAdminName=DB::table('users')->select('*')->where('role','1')->first();
            $getAdminName=json_decode(json_encode($getAdminName), true);
            $sender_name=$getAdminName['first_name'];
        }
        $sender_email=$getAdminCompanyDetails['default_email'];
        if($sender_email=="" || $getAdminCompanyDetails['marked_as_default']=="no")
        {
            $getAdminName=DB::table('users')->select('*')->where('role','1')->first();
            $getAdminName=json_decode(json_encode($getAdminName), true);
            $sender_email=$getAdminName['email'];
        }
        if(!empty(unserialize($getContactDetails['email'])['work']))
        {
            $email=unserialize($getContactDetails['email'])['work'][0];
        }
        else
        {
            $email=unserialize($getContactDetails['email'])['home'][0];
        }
        // return $email;
        $company_id=$getAdminCompanyDetails['id'];
        $getDefaultPaymentMethod=$this->get_meta('default_payment_gateway',$company_id);
        $getDefaultPaymentMethod=json_decode(json_encode($getDefaultPaymentMethod), true);
        if($getDefaultPaymentMethod['meta_value']!="")
        {
            $getPaymentGatewayCredentials=DB::table('payment_gateways')->select('*')->where('company_id',$company_id)->where('payment_gateway',$getDefaultPaymentMethod['meta_value'])->first();
            $getPaymentGatewayCredentials=json_decode(json_encode($getPaymentGatewayCredentials), true);
            if($getPaymentGatewayCredentials['credentials']!="")
            {
                $payment="on";
            }
            else
            {
                $payment="off";
            }
        }
        else
        {
            $payment="off";
        }
        $view_on_browser_link=$this->url->to('/').'/view-invoice-on-browser/'.$link;
        $getInvoiceNo=$createInvoice;
        $send=$this->send_invoice_mail($email,$items_array,$inv_total,$discount,$tax,$adjustment,$total,$contact_address,$contact_country,$contact_state,$contact_post_code,$contact_name,$company_name,$company_address,$company_country,$company_state,$company_post_code,$sender_name,$sender_email,$createInvoice,$due_date,$terms_and_conditions,$link,$invoices_title,$logo,'',$payment_status,$payment,$items_array[0]['quantity_type'],$view_on_browser_link,$getInvoiceNo);
        return $email;
        // echo "<pre>";
        // print_r($getContactDetails);
        // echo "</pre>";
        // die();
    }

    public function send_invoice_mail($email,$items_array,$inv_total,$discount,$tax,$adjustment,$total,$contact_address,$contact_country,$contact_state,$contact_post_code,$contact_name,$company_name,$company_address,$company_country,$company_state,$company_post_code,$sender_name,$sender_email,$createInvoice,$due_date,$terms_and_conditions,$link,$invoices_title,$logo,$message_to_customer,$payment_status,$payment,$quantity_type,$view_on_browser_link,$getInvoiceNo){
        // echo $email;
        $pdf_link=$this->url->to('/').'/show-invoice-pdf/'.$link;
        $payment_link=$this->url->to('/').'/invoice/payment/'.$link;
        $currency_symbol=[
            'USD'=> '$', // US Dollar
            'EUR'=> '€', // Euro
            'CRC'=> '₡', // Costa Rican Colón
            'GBP'=> '£', // British Pound Sterling
            'ILS'=> '₪', // Israeli New Sheqel
            'INR'=> '₹', // Indian Rupee
            'JPY'=> '¥', // Japanese Yen
            'KRW'=> '₩', // South Korean Won
            'NGN'=> '₦', // Nigerian Naira
            'PHP'=> '₱', // Philippine Peso
            'PLN'=> 'zł', // Polish Zloty
            'PYG'=> '₲', // Paraguayan Guarani
            'THB'=> '฿', // Thai Baht
            'UAH'=> '₴', // Ukrainian Hryvnia
            'VND'=> '₫' // Vietnamese Dong
        ];
        $currency_symbol=$this->getCurrencySymbol('en',$items_array[0]['currency']);
        if($quantity_type=="quantity")
        {
            $quantity_type="Quantity";
        }
        else if($quantity_type=="hr")
        {
            $quantity_type="hr";
        }
        else if($quantity_type=="item")
        {
            $quantity_type="Item";
        }
        else
        {
            $quantity_type="hr + Item";
        }
      $data = array('name'=>$sender_name,'items_array'=>$items_array,'inv_total'=>$inv_total,'discount'=>$discount,'tax'=>$tax,'adjustment'=>$adjustment,'total'=>$total,'contact_address'=>$contact_address,'contact_country'=>$contact_country,'contact_state'=>$contact_state,'contact_post_code'=>$contact_post_code,'contact_name'=>$contact_name,'company_name'=>$company_name,'company_address'=>$company_address,'company_country'=>$company_country,'company_state'=>$company_state,'company_post_code'=>$company_post_code,'id'=>$getInvoiceNo,'due_date' => $due_date,'terms_and_conditions' => $terms_and_conditions,'pdf_link'=>$pdf_link,'logo'=>$logo,'payment'=>$payment,'currency_symbol'=>$currency_symbol,'view_on_browser_link'=>$view_on_browser_link,'quantity_type'=>$quantity_type,'payment_link'=>$payment_link);
      if($payment_status=="unpaid")
      {
        Mail::send('templates/pulse-mailer/invoice-template', $data, function($message) use ($email,$sender_name,$sender_email,$invoices_title) {
         $message->to($email, 'Tutorials Point')->subject
            ($invoices_title);
         $message->from($sender_email,$sender_name);
        });
      }
      else
      {
        Mail::send('templates/pulse-mailer/invoice-payment-done-template', $data, function($message) use ($email,$sender_name,$sender_email,$invoices_title) {
         $message->to($email, 'Tutorials Point')->subject
            ($invoices_title);
         $message->from($sender_email,$sender_name);
        });
      }
      // echo "HTML Email Sent. Check your inbox.";
   }

   function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function changePaymenStatus($pay,$link)
    {
        $getInvoice=DB::table('invoices')->select('*')->where('link',$link)->first();
        $getInvoice=json_decode(json_encode($getInvoice), true);
        $getInvoiceItems=DB::table('invoice_items')->select('*')->where('inv_id',$getInvoice['id'])->get();
        $getInvoiceItems=json_decode(json_encode($getInvoiceItems), true);
        $getInvoiceMeta=DB::table('invoice_meta')->select('*')->where('inv_id',$getInvoice['id'])->first();
        $getInvoiceMeta=json_decode(json_encode($getInvoiceMeta), true);
        $getInvoiceItemTotal=DB::table('invoice_items')->where('inv_id',$getInvoice['id'])->sum('total');
        $getInvoiceItemTotal=json_decode(json_encode($getInvoiceItemTotal), true);
        $part_pay=$getInvoiceMeta['part_pay'];
        $part_pay_value=$getInvoiceMeta['part_pay_value'];
        $inv_total=$getInvoiceMeta['inv_total'];

        //CHANGE PAID,DUE,PAYMENT STATUS
        if($getInvoice['payment_status']=="unpaid" && $getInvoiceMeta['part_pay']!="no_part_pay")
        {
            $paid=number_format((double)$pay, 2, '.', '');
            $due=number_format((double)$inv_total, 2, '.', '')-$paid;
            $due=number_format((double)$due, 2, '.', '');
            $payment_status="partly_paid";
        }
        else if($getInvoice['payment_status']=="partly_paid")
        {
            $paid=number_format((double)$getInvoice['paid'], 2, '.', '')+number_format((double)$pay, 2, '.', '');
            $due=number_format((double)$inv_total, 2, '.', '')-$paid;
            $due=number_format((double)$due, 2, '.', '');
            $payment_status="paid";
        }
        else if($getInvoice['payment_status']=="unpaid" && $getInvoiceMeta['part_pay']=="no_part_pay")
        {
            $paid=number_format((double)$pay, 2, '.', '');
            $due=number_format((double)$inv_total, 2, '.', '')-$paid;
            $due=number_format((double)$due, 2, '.', '');
            $payment_status="paid";
        }

        //GET INVOICE CONTACT DETAILS
        $getInvoiceContactDetails=DB::table('contacts')->select('*')->where('id',$getInvoice['contact'])->first();
        $getInvoiceContactDetails=json_decode(json_encode($getInvoiceContactDetails), true);
        $contact_name=$getInvoiceContactDetails['first_name']." ".$getInvoiceContactDetails['last_name'];

        //GET INVOICE COMPANY DETAILS
        $getInvoiceCompanyDetails=DB::table('admin_company')->select('*')->where('id',$getInvoice['company_id'])->first();
        $getInvoiceCompanyDetails=json_decode(json_encode($getInvoiceCompanyDetails), true);
        $company_name=$getInvoiceCompanyDetails['company_name'];
        if($getInvoiceCompanyDetails['logo']!="")
        {
            $company_logo=$getInvoiceCompanyDetails['logo'];
        }
        else
        {
            $company_logo=$this->url->to('/').'/assets/img/logo.png';
        }
        $total=$getInvoiceMeta['inv_total'];
        $shipping_cost=$getInvoiceMeta['shipping_cost'];
        $discount=$getInvoiceMeta['discount'];
        $tax=$getInvoiceMeta['tax'];
        $adjustment=$getInvoiceMeta['adjustment'];
        $pdf_link=$this->url->to('/').'/show-invoice-pdf/'.$link;
        $ee=$this->sendInvoiceMail($getInvoice['contact'],$getInvoiceItems,$total,$shipping_cost,$discount,$tax,$adjustment,$getInvoiceItemTotal,$getInvoice['inv_no'],$getInvoice['due_date'],$getInvoiceMeta['terms_and_conditions'],$getInvoice['link'],$getInvoice['invoice_title'],$company_logo,'','paid');
        $changePaymenStatus=DB::table('invoices')->where('link',$link)->update(['paid' => $paid, 'due' => $due, 'payment_status' => $payment_status]);
        // $data=array(
        //     )
        return $changePaymenStatus;
    }

    public function sendTaskMail($created_by,$assigned_to,$task_title,$related_deal)
    {
        $getUser1=DB::table('users')->select('*')->where('id',$created_by)->first();
        $getUser1=json_decode(json_encode($getUser1), true);
        $getUser2=DB::table('users')->select('*')->where('id',$assigned_to)->first();
        $getUser2=json_decode(json_encode($getUser2), true);
        $getDeal=DB::table('deals')->select('*')->where('id',$related_deal)->first();
        $getDeal=json_decode(json_encode($getDeal), true);
        $deal_link=$this->url->to('/')."/deal-contexual?deal_id=".$related_deal."&page=deal_feed";
        $sender_name=$this->get_sender_name();
        $sender_email=$this->get_sender_email();
        $user1=$getUser1['first_name']." ".$getUser1['last_name'];
        $user2=$getUser2['first_name']." ".$getUser2['last_name'];
        $getAdminName=$this->get_admin_name_and_company();
        $send_task_mail=$this->task_email($user1,$user2,$task_title,$getDeal['title'],$deal_link,$sender_name,$sender_email,$getUser2['email'],$getAdminName['company_name'],$getAdminName['admin_name'],$getAdminName['role']);
        return true;
    }

    public function task_email($user1,$user2,$task_title,$deal,$deal_link,$sender_name,$sender_email,$email,$company_name,$admin_name,$role){
      $data = array('name'=>$user2,'user1'=>$user2,'user2'=>$user1,'task_title'=>$task_title,'deal'=>$deal,'deal_link'=>$deal_link,'company_name'=>$company_name,'admin_name'=>$admin_name,'role'=>$role,'logo'=>$this->url->to('/')."/uploads/company-logo/company-logo.png");
      $getPortalName=env('PORTAL_NAME');
      Mail::send('templates/pulse-mailer/task-assign', $data, function($message) use ($email,$sender_name,$sender_email,$getPortalName) {
         $message->to($email, 'Tutorials Point')->subject
            ($getPortalName.' Task');
         $message->from($sender_email,$sender_name);
      });
      return true;
   }

   public function get_sender_name()
   {
        $get_sender_name=DB::table('admin_company')->join('users', 'users.company', '=', 'admin_company.id')->select('sender_name')->where('users.id', '=',Auth::user()->id)->first();
        $get_sender_name=json_decode(json_encode($get_sender_name), true);
        if($get_sender_name['sender_name']!="")
        {
            $get_sender_name=$get_sender_name['sender_name'];
        }
        else
        {
            $getCurrentUserDetails=DB::table('users')->select('*')->where('id',Auth::getUser()->id)->first();
            $getCurrentUserDetails=json_decode(json_encode($getCurrentUserDetails), true);
            $get_sender_name=$getCurrentUserDetails['first_name']." ".$getCurrentUserDetails['last_name'];
        }
        return $get_sender_name;
   }

   public function get_sender_email()
   {
        $get_sender_email=DB::table('admin_company')->join('users', 'users.company', '=', 'admin_company.id')->select('default_email')->where('users.id', '=',Auth::user()->id)->first();
        $get_sender_email=json_decode(json_encode($get_sender_email), true);
        if($get_sender_email['default_email']!="")
        {
            $get_sender_email=$get_sender_email['default_email'];
        }
        else
        {
            $getCurrentUserDetails=DB::table('users')->select('*')->where('id',Auth::getUser()->id)->first();
            $getCurrentUserDetails=json_decode(json_encode($getCurrentUserDetails), true);
            $get_sender_email=$getCurrentUserDetails['email'];
        }
        return $get_sender_email;
   }

   public function sendDealMail($created_by,$owner,$title,$deal_id)
   {
        $getUser1=DB::table('users')->select('*')->where('id',$created_by)->first();
        $getUser1=json_decode(json_encode($getUser1), true);
        $getUser2=DB::table('users')->select('*')->where('id',$owner)->first();
        $getUser2=json_decode(json_encode($getUser2), true);
        $sender_name=$this->get_sender_name();
        $sender_email=$this->get_sender_email();
        $user1=$getUser1['first_name']." ".$getUser1['last_name'];
        $user2=$getUser2['first_name']." ".$getUser2['last_name'];
        $email=$getUser2['email'];
        $deal_link=$this->url->to('/')."/deal-contexual?deal_id=".$deal_id."&page=deal_feed";
        $getAdminName=$this->get_admin_name_and_company();
        $sendDealMail=$this->send_deal_mail($user1,$user2,$title,$deal_link,$sender_name,$sender_email,$email,$getAdminName['company_name'],$getAdminName['admin_name'],$getAdminName['role']);
        return true;
   }

   public function send_deal_mail($user1,$user2,$title,$deal_link,$sender_name,$sender_email,$email,$company_name,$admin_name,$role)
   {
        $data = array('name'=>$user2,'user1'=>$user2,'user2'=>$user1,'title'=>$title,'deal_link'=>$deal_link,'company_name'=>$company_name,'admin_name'=>$admin_name,'role'=>$role);
        $getPortalName=env('PORTAL_NAME');
        Mail::send('templates/pulse-mailer/deal-assign', $data, function($message) use ($email,$sender_name,$sender_email,$getPortalName) {
             $message->to($email, 'Tutorials Point')->subject
                ($getPortalName.' Deal');
             $message->from($sender_email,$sender_name);
        });
        return true;
   }

   public function get_admin_name_and_company()
   {
        $get_company_name=DB::table('admin_company')->join('users', 'users.company', '=', 'admin_company.id')->select('company_name')->where('users.id', '=',Auth::user()->id)->first();
        $get_company_name=json_decode(json_encode($get_company_name), true);
        $getCurrenUserCompanyId=DB::table('admin_company')->join('users', 'users.company', '=', 'admin_company.id')->select('admin_company.id')->where('users.id', '=',Auth::user()->id)->first();
        $getCurrenUserCompanyId=json_decode(json_encode($getCurrenUserCompanyId), true);
        $getCurrentUserCompanyAdminName=DB::table('users')->select('*')->where('company',$getCurrenUserCompanyId['id'])->where('role','1')->first();
        $getCurrentUserCompanyAdminName=json_decode(json_encode($getCurrentUserCompanyAdminName), true);
        $data=array(
            'company_name'=>$get_company_name['company_name'],
            'admin_name'=>$getCurrentUserCompanyAdminName['first_name']." ".$getCurrentUserCompanyAdminName['last_name'],
            'role'=>'admin'
        );
        return $data;
   }

   public function get_profile_image()
   {
        $get_profile_image=DB::table('user_meta')->select('*')->where('user_id',Auth::user()->id)->first();
        $get_profile_image=json_decode(json_encode($get_profile_image), true);
        if($get_profile_image['image']!="")
        {
            return $this->url->to('/')."/uploads/profile-image/".$get_profile_image['image'];
        }
        else
        {
            return $this->url->to('/')."/assets/img/avatar.png";
        }
   }

   public function get_company_currency()
   {
        $getCompanyCurrency=DB::table('currency')->join('users', 'users.company', '=', 'currency.company_id')->select('*')->where('users.id', '=',Auth::user()->id)->first();
        $getCompanyCurrency=json_decode(json_encode($getCompanyCurrency), true);
        return $getCompanyCurrency;
   }

   public function checkShowTutorial()
    {
        $checkShowTutorial=DB::table('users')->select('*')->where('id',Auth::user()->id)->first();
        $checkShowTutorial=json_decode(json_encode($checkShowTutorial), true);
        return $checkShowTutorial['show_tutorial'];
    }

    public function changeTutorialStatus($purpose)
    {
        if($purpose=="skip_tutorial")
        {
            $skipTutorial=DB::table('users')->where('id',Auth::user()->id)->update(['show_tutorial' => 'never']);
            return $skipTutorial;
        }
        else
        {
            $skipTutorial=DB::table('users')->where('id',Auth::user()->id)->update(['show_tutorial' => 'view_later']);
            return $skipTutorial;
        }
    } 

    public function checkSmtp()
    {
        try{
            $transport = Swift_SmtpTransport::newInstance(env('MAIL_HOST'), env('MAIL_PORT'), 'ssl');
            $transport->setUsername(env('MAIL_USERNAME'));
            $transport->setPassword(env('MAIL_PASSWORD'));
            $mailer = \Swift_Mailer::newInstance($transport);
            $mailer->getTransport()->start();
            return 'ok';
        } catch (Swift_TransportException $e) {
            return $e->getMessage();
        } catch (\Exception $e) {
          return $e->getMessage();
        }
    }

    public function getCurrencySymbol($locale, $currency)
    {
        return Currency::symbol($currency);
    }

    public function getLanguage()
    {
        $xml_link=$this->url->to('/').'/resources/lang/lang.xml';
        $xml = XmlParser::load($xml_link);
        $user = $xml->parse([
            'lang' => ['uses' => 'lang.lang'],
            'value' => ['uses' => 'lang.value'],
            'followers' => ['uses' => 'lang::followers'],
        ]);
        $lang=$user['lang'];
        $value=$user['value'];
        $lang_array=explode(',',$lang);
        $value_array=explode(",",$value);
        if(sizeof($lang_array)!=sizeof($value_array))
        {
            $lang_value_array=array('en' => 'English');
        }
        else
        {
            $lang_value_array=array();
            $lang_value_array=array_combine($value_array,$lang_array);
        }
        return $lang_value_array;
    }

    public function getLabelName()
    {
        $path = base_path().'/resources/lang/'.Lang::getLocale();
        $language_array=array();
        $language_array=array(
            'invoice' => 'INVOICE',
            'id' => 'ID',
            'due_on' => 'Due on',
            'from' => 'from',
            'to' => 'to',
            'item_name' => 'Item Name',
            'rate' => 'Rate',
            'quantity' => 'Quantity',
            'total' => 'Total',
            'sub_total' => 'Sub-Total',
            'adjustments' => 'Adjustment(s)',
            'discount' => 'Discount',
            'tax' => 'Tax',
            'terms_and_conditions' => 'Terms &amp; Conditions',
            'thank_you' => 'THANK YOU',
            'for' => 'for',
            'doing_business_with' => 'doing business with',
            'this_invoice_is_generated_using' => 'This invoice is generated using'
        );
        if(File::isDirectory($path))
        {
            if(!empty(Lang::get('invoice-estimate-common-template-lang')))
            {
                $language_array['from']=Lang::get('invoice-estimate-common-template-lang')['from'];
                $language_array['to']=Lang::get('invoice-estimate-common-template-lang')['to'];
                $language_array['item_name']=Lang::get('invoice-estimate-common-template-lang')['item-name'];
                $language_array['rate']=Lang::get('invoice-estimate-common-template-lang')['rate'];
                $language_array['quantity']=Lang::get('invoice-estimate-common-template-lang')['quantity'];
                $language_array['total']=Lang::get('invoice-estimate-common-template-lang')['total'];
                $language_array['sub_total']=Lang::get('invoice-estimate-common-template-lang')['sub-total'];
                $language_array['adjustments']=Lang::get('invoice-estimate-common-template-lang')['adjustments'];
                $language_array['discount']=Lang::get('invoice-estimate-common-template-lang')['discount'];
                $language_array['tax']=Lang::get('invoice-estimate-common-template-lang')['tax'];
                $language_array['download_pdf']=Lang::get('invoice-estimate-common-template-lang')['download-pdf'];
                $language_array['thank_you']=Lang::get('invoice-estimate-common-template-lang')['thank-you'];
                $language_array['for']=Lang::get('invoice-estimate-common-template-lang')['for'];
                $language_array['doing_business_with']=Lang::get('invoice-estimate-common-template-lang')['doing-business-with'];
                $language_array['this_invoice_is_generated_using']=Lang::get('invoice-estimate-common-template-lang')['this-invoice-is-generated-using'];
            }
            if(!empty(Lang::get('estimate')))
            {
                $language_array['terms_and_conditions']=Lang::get('estimate')['terms-and-condition'];

            }
            if(!empty(Lang::get('task')))
            {
                $language_array['due_on']=Lang::get('task')['due-on'];
            }
        }
        return $language_array;
    }

    public function getDateFormat($date)
    {
        $getDateFormat=$this->get_meta('date_format','1');
        $getDateFormat=json_decode(json_encode($getDateFormat), true);
        if($getDateFormat['meta_value']=='mm/dd/yy')
        {
            $newDate = date("m/d/Y", strtotime($date));
        }
        else
        {
            $newDate = date("d/m/Y", strtotime($date));
        }
        return $newDate;
    }

    public function getUsers()
    {
        $getUsers=DB::table('users')->select('*')->get();
        $getUsers=json_decode(json_encode($getUsers), true);
        return $getUsers;
    }

    public function searchForDealStageId($deal_stage,$deals)
    {
        $deal_stage_array=array();
        foreach($deal_stage as $deal_stage_key=>$deal_stage_value)
        {
            foreach ($deals as $key => $val) {
                if ($val['stage'] === $deal_stage_value['id']) {
                   array_push($deal_stage_array,$deal_stage_value);
                   break;
                }
            }
        }
        return $deal_stage_array;
    }
}