<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Feed extends Model
{
    //
    public function createFeed($feed_title,$user_id,$contact_id,$deal_id,$feed_type)
    {
    	$createFeed=DB::table('feeds')->insertGetId(['user_id' => $user_id , 'contact_id' => $contact_id , 'deal_id' => $deal_id , 'feed' => $feed_title , 'feed_description' => 'description' , 'feed_type' => $feed_type , 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s')]);
    	return $createFeed;
    }

    public function getFeedByContactId($user_id,$contact_id)
    {
    	$getFeedByContactId=DB::table('feeds')->select('*')->where('contact_id',$contact_id)->where('feed_type','contact')->get();
    	$getFeedByContactId=json_decode(json_encode($getFeedByContactId), true);
    	return $getFeedByContactId;
    }
}
