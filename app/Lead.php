<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lead extends Model
{
    //
    protected $fillable = ['lead_source','lead_stage','industry','territory','content_type','contact_id','lead_stage'];
    protected $table = 'lead_table';

    public static function getAddressByContactId($id)
    {
    	// $sql="SELECT * FROM `contacts_address` WHERE `contact_id` = '".$id."'";
     //    $result = DB::select($sql);
    	$result=array(
    		"ss" => "dd"
    		);
        // $result=json_decode(json_encode($result), true);
        return $result;
    }
}
