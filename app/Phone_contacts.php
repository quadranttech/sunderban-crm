<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Phone_contacts extends Model
{
    //
    protected $table = 'phone_contacts';

    public function getPhoneContacts()
    {
    	$getPhoneContacts = DB::table('phone_contacts')->join('call_activity_logs','phone_contacts.id','=','call_activity_logs.ref_phone_contact')->select('phone_contacts.*','call_activity_logs.date','call_activity_logs.created_at')->orderBy('call_activity_logs.created_at','desc')->get();
    	$getPhoneContacts = json_decode(json_encode($getPhoneContacts), true);
    	return $getPhoneContacts;
    }
}
