<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Improfiles extends Model
{
    //
    protected $timestamps = true;
    protected $fillable = ['contact_id'];
    protected $table = 'improfiles';
}
