<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use Mail;
use App\Traits\MainTrait;
use Illuminate\Routing\UrlGenerator;

class TaskNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Task:Notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Task Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    use MainTrait;
    public function __construct(UrlGenerator $url)
    {
        parent::__construct();
        $this->url = $url;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $getTask=DB::table('task')->select('*')->get();
        $getTask=json_decode(json_encode($getTask), true);
        foreach($getTask as $key=>$value)
        {
            date_default_timezone_set(env("TIME_ZONE"));
            $date1 = new DateTime(explode(" ",$value['due_date'])[0]);
            $date2 = new DateTime(date("Y-m-d"));

            // The diff-methods returns a new DateInterval-object...
            $diff = $date2->diff($date1);

            // Call the format method on the DateInterval-object
            if($diff->format('%a')==1)
            {
                $getDeal=DB::table('deals')->select('*')->where('id',$value['related_deal'])->first();
                $getDeal=json_decode(json_encode($getDeal), true);
                $getAdminCompany=DB::table('admin_company')->select('*')->where('id',1)->first();
                $getAdminCompany=json_decode(json_encode($getAdminCompany), true);
                $sender_email=$getAdminCompany['default_email'];
                $sender_name=$getAdminCompany['sender_name'];
                $getUser=DB::table('users')->select('*')->where('id',$value['created_by'])->first();
                $getUser=json_decode(json_encode($getUser), true);
                if($getAdminCompany['marked_as_default']!="yes" || $getAdminCompany['default_email']=="")
                {
                    $sender_email=$getUser['email'];
                }
                if($sender_name=="")
                {
                    $sender_name=$getUser['name'];
                }
                $getAssignUser=DB::table('users')->select('*')->where('id',$value['assigned_to'])->first();
                $getAssignUser=json_decode(json_encode($getAssignUser), true);
                $email=$getAssignUser['email'];
                $name=$getAssignUser['name'];
                $due_date=explode(" ",$value['due_date'])[0];
                $admin_name=$getUser['name'];
                $getSetting=$this->get_meta('roles',$getAdminCompany['id']);
                $getSetting=json_decode(json_encode($getSetting), true);
                $roles_array=unserialize($getSetting['meta_value']);
                $role=$roles_array[$getUser['role']];
                $company_name=$getAdminCompany['company_name'];
                $task_title=$value['task_title'];
                $company_logo=$getAdminCompany['logo'];
                if($company_logo=="")
                {
                    $company_logo=$this->url->to('/').'/assets/img/logo.png';
                }
                $link=$this->url->to('/')."/tasks";
                $data = array('name'=>$name,'due_date'=>$due_date,'admin_name'=>$admin_name,'role'=>$role,'company_name'=>$company_name,'task_title'=>$task_title,'company_logo'=>$company_logo,'link'=>$link);
                Mail::send('templates/pulse-mailer/task-due', $data, function($message) use ($email,$sender_name,$sender_email) {
                    $message->to($email, 'Tutorials Point')->subject
                        ('You have a task due');
                    $message->from($sender_email,$sender_name);
                });
            }
        }
    }
}
