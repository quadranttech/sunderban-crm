<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social_profile extends Model
{
    //
    protected $timestamps = true;
    protected $fillable = ['contact_id'];
    protected $table = 'social_profile';
}
