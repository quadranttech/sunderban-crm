<?php

namespace App\Http\Controllers;

use Request;
use Redirect;
use DB;
use Auth;
use View;
use Session;
use Artisan;
use Illuminate\Routing\UrlGenerator;
use File;
use mysqli;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Traits\PermissionTrait;

class InstallerController extends Controller
{
    //
    use PermissionTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }
    public function install()
    {
        $path = base_path('.env');  //GET ENVIRONMENT FILE
        if (file_exists($path)) {   // IF ENVIRONMENT FILE EXISTS
            if(env("DB_HOST", "")=="" && env("DB_DATABASE", "")=="" && env("DB_USERNAME", "")=="" && env("DB_PASSWORD", "")=="")    //IF ANY OF THESE VALUES ARE NULL IN ENVIRONMENT FILE
            {
                return view('install/install', ['session' => 'install']);
            }
            else
            {
                echo "already installed";
            }
        }
    }

    public function checkDatabaseConnectivity()
    {
    	$servername = Input::get('db_host');   //GET DATABASE HOST NAME
		$username = Input::get('db_username');    //GET DATABASE USER NAME
		$password = Input::get('db_password');    //GET DATABASE PASSWORD NAME
		$database_name=Input::get('db_name'); //GET DATABASE NAME

		// Create connection
		try {
		  $conn = new mysqli($servername, $username, $password, $database_name);
		}
		catch (\Exception $e) {
		    return response()->json(['status' => 'failed' , 'message' => 'connection_error']);
		}

        // CHECK IF DATABASE EMPTY
        $sql = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA LIKE '".$database_name."'";
        $result = mysqli_query($conn,$sql);
        if ($result->num_rows) {
            return response()->json(['status' => 'failed' , 'message' => 'database_not_empty']);
        }
		
		// Check connection
		if ($conn->connect_error) {
		    // die("Connection failed: " . $conn->connect_error);
		    return response()->json(['status' => 'failed' , 'message' => 'connection_error']);
		} 

        if (version_compare(PHP_VERSION, '5.6.0', '<')) {
            return response()->json(['status' => 'failed' , 'message' => 'php_version']);
        }

		// Create database
		else
		{
			$path = base_path('.env');   //GET ENVIRONMENT FILE

			if (file_exists($path)) {    //IF ENVIRONMENT FILE EXISTS CHANGE THESE VALUES
			    file_put_contents($path, str_replace(
			        'DB_HOST='.env("DB_HOST", ""), 'DB_HOST='.$servername, file_get_contents($path)
			    ));
			    file_put_contents($path, str_replace(
			        'DB_DATABASE='.env("DB_DATABASE", ""), 'DB_DATABASE='.$database_name, file_get_contents($path)
			    ));
			    file_put_contents($path, str_replace(
			        'DB_USERNAME='.env("DB_USERNAME", ""), 'DB_USERNAME='.$username, file_get_contents($path)
			    ));
			    file_put_contents($path, str_replace(
			        'DB_PASSWORD='.env("DB_PASSWORD", ""), 'DB_PASSWORD='.$password, file_get_contents($path)
			    ));
                file_put_contents($path, str_replace(
                    'TIME_ZONE='.env("TIME_ZONE", ""), 'TIME_ZONE='.Input::get('timezone'), file_get_contents($path)
                ));
			    
    			return response()->json(['status' => 'success' , 'message' => 'db_created']);
			}
		}

		$conn->close();
    }

    public function importDb()
    {
    	Artisan::call('migrate', array('--path' => 'database/migrations', '--force' => true)); //RUN DATABASE MIGRATION
		Artisan::call('db:seed'); //RUN DATABASE SEED
		return view('install/install', ['session' => 'sign_up']);
    }

    public function signUp()
    {
    	return view('install/install', ['session' => 'sign_up']);
    }

    public function createUser()
    {
    	$name=Input::get('first_name').' '.Input::get('last_name');    //GET USER FIRST AND LAST NAME
    	$createUser=DB::table('users')->insertGetId(['first_name' => Input::get('first_name'), 'last_name' => Input::get('last_name'), 'name' => $name, 'email' => Input::get('email'), 'password' => bcrypt(Input::get('password')), 'company' => '1', 'role' => '1', 'status' => 'Active', 'show_tutorial' => 'on', 'department' => 'no_department', 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s')]);   //CREATE USER
        $createUserMeta=DB::table('user_meta')->insertGetId(['user_id' => $createUser, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s')]);
        $contact=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));   //SET USER CONTACT PERMISSION
        $deal=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));  //SET USER DEAL PERMISSION
        $task=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));  //SET USER TASK PERMISSION
        $invoice=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));   //SET USER INVOICE PERMISSION
        $estimate=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));  //SET USER ESTIMATE PERMISSION
        $setting=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));   //SET USER SETTING PERMISSION
        $this->setPermissionToUser($createUser,$contact,$deal,$task,$invoice,$estimate,$setting);   //CREATE USER PERMISSION
        if($createUser)
        {
            Auth::loginUsingId($createUser);
        }
    	return response()->json(['response' => $createUser]);
    }
}
