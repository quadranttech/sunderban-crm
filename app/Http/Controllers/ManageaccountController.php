<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Routing\UrlGenerator;
use DB;
use Auth;
use App\Feed;
use Redirect;
use View;
use Illuminate\Support\Facades\Input;
use Jenssegers\Agent\Agent;
use App\Notification;
use DateTime;
use App\Traits\PermissionTrait;
use App\Traits\MainTrait;

class ManageaccountController extends ContactController
{
    //
    use PermissionTrait;
    use MainTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;  //GET BASE URL
        $this->middleware('auth');  //REDIRECTED TO LOGIN IF NOT AUTHENTICATED
    }

    public function index()
    {
    	$user_id=Auth::user()->id; //GET CURRENT USER ID
    	$getUser=DB::table('users')->where('id',$user_id)->first();    //GET CURRENT USER DETAILS
    	$getUser=json_decode(json_encode($getUser), true);
        $user_id=Auth::user()->id;
        $get_user_meta=DB::table('user_meta')->select('*')->where('user_id',$user_id)->first();  //GET CURRENT USER META
        $get_user_meta=json_decode(json_encode($get_user_meta), true);
        $deserializeEmail=unserialize($get_user_meta['email']);
        $notification_table=new Notification;   //CREATE NOTIFICATION CLASS OBJECT
        $getNotification=$notification_table->getNotification(Auth::getUser()->id); //CALL GET NOTIFICATION METHOD
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))    //IF NOTIFICATION EXISTS
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);
        }
        $get_user_profile_image=$this->get_profile_image();
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        $agent = new Agent();   //GET USER AGENT
        if($agent->isMobile())
        {
            return view('dashboard/manage/manageaccount-mobile', ['user' => $getUser, 'user_meta' => $get_user_meta, 'extra_email' => $deserializeEmail , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'profile_image' => $get_user_profile_image , 'active' => 'manage' , 'show_tutorial' => $checkShowTutorial , 'permission' => $getUserPermission]);
        }
    	return view('dashboard/manage/manageaccount', ['user' => $getUser, 'user_meta' => $get_user_meta, 'extra_email' => $deserializeEmail , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'profile_image' => $get_user_profile_image , 'active' => 'manage' , 'show_tutorial' => $checkShowTutorial ,   'permission' => $getUserPermission]);
    }

    public function saveAccountDetails()
    {
        $emailSerialized="";
        if(!empty(Input::get('extra_email')) && !empty(Input::get('extra_email_purpose')))  //IF EXTRA EMAIL AND EXTRA EMAIL PURPOSE ARRAY EXISTS
        {
            $email=$this->getStructeredEmailAndPhone(Input::get('extra_email'),Input::get('extra_email_purpose'));  //GET STRUCTURED EMAIL AND PHONE ARRAY
            $emailSerialized=$this->getSerializedData($email);  //GET SERIALIZED ARRAY
        }
        $work_title=Input::get('work_title');   //GET INPUT WORK TITLE
        $bio=Input::get('bio'); //GET INPUT BIO
        $address=Input::get('address'); //GET INPUT ADDRESS
        $city=Input::get('city');   //GET INPUT CITY
        $state=Input::get('state'); //GET INPUT STATE
        $country=Input::get('country'); //GET INPUT COUNTRY
        $post_code=Input::get('post_code'); //GET INPUT POST CODE
        $user_id=Auth::user()->id;
        $get_user_meta=DB::table('user_meta')->select('*')->where('user_id',$user_id)->first();  //GET CURRENT USER META
        $get_user_meta=json_decode(json_encode($get_user_meta), true);
        $changeUserDetails=DB::table('users')->where('id',$user_id)->update(['first_name' => Input::get('first_name'), 'last_name' => Input::get('last_name')]);    //EDIT USER DETAILS
        if(!empty($get_user_meta))  //IF USER META EXISTS THEN UPDATE
        {
            $update_user_meta=DB::table('user_meta')->where('user_id',$user_id)->update(['email' => $emailSerialized, 'work_title' => $work_title, 'bio' => $bio, 'address' => $address, 'city' => $city, 'state' => $state, 'country' => $country, 'post_code' => $post_code]);
            $update_user_information=DB::table('users')->where('id',$user_id)->update(['department' => Input::get('department')]);
            // return $update_user_meta;
            if(Input::get('image')!="") //IF USER IMAGE EXISTS THEN UPDATE USER IMAGE
            {
                $this->updateUserImage(Input::get('image'),$user_id);
            }
            return response()->json(['status' => 'success' , 'response' => $update_user_meta]);
        }
        else
        {
            $add_user_meta=DB::table('user_meta')->insertGetId(['user_id' => $user_id, 'email' => $emailSerialized, 'work_title' => $work_title, 'bio' => $bio, 'address' => $address, 'city' => $city, 'state' => $state, 'country' => $country, 'post_code' => $post_code]);
            if(Input::get('image')!="") //IF USER IMAGE EXISTS THEN UPDATE USER IMAGE
            {
                $this->updateUserImage(Input::get('image'),$user_id);
            }
            return response()->json(['status' => 'success' , 'response' => $add_user_meta]);
        }
    }

    public function updateUserImage($fileName,$user_id)
    {
        $updateUserImage=DB::table('user_meta')->where('id',$user_id)->update(['image' => $fileName]);  //UPDATE USER IMAGE
        return $updateUserImage;
    }

    public function uploadUserImage()
    {
        if (Input::hasFile('user_image'))
        {
           $re = Input::file('user_image')->getClientOriginalName();
           $destinationPath = base_path('uploads/profile-image'); // upload path
           $file_name=Input::file('user_image')->getClientOriginalName();
           $file_name = pathinfo($file_name, PATHINFO_FILENAME);
           $extension = Input::file('user_image')->getClientOriginalExtension(); // getting image extension
           $fileName = $file_name.'-'.rand(11111,99999).'.'.$extension; // renameing image
           Input::file('user_image')->move($destinationPath, $fileName); // uploading file to given path
           return response()->json(['status' => 'success' , 'fileName' => $fileName]);
        }
    }
}
