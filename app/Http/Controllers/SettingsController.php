<?php

// require base_path('vendor/autoload.php');
namespace App\Http\Controllers;

use Request;
use Illuminate\Routing\UrlGenerator;
use DB;
use Auth;
use App\Feed;
use Redirect;
use View;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManager;
use App\Setting;
use App\Traits\MainTrait;
use Jenssegers\Agent\Agent;
require 'vendor/autoload.php';
use Intervention\Image\ImageManagerStatic as Image;
use App\Notification;
use DateTime;
use App\Traits\PermissionTrait;

class SettingsController extends MailController
{
    //
    use MainTrait;
    use PermissionTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->middleware('auth');
    }

    public function index()
    {
        $name = Request::input('page'); //GET URL PAGE VALUE
        $getUsers=DB::table('users')->select('*')->get();   //GET USERS
        $getUsers=json_decode(json_encode($getUsers), true);
        $getRoles=DB::table('settings')->select('*')->where('meta_key','roles')->first();   //GET ROLES
        $getRoles=json_decode(json_encode($getRoles), true);
        $roles_array=unserialize($getRoles['meta_value']);  //SERIALIZED ROLES ARRAY
        $getCompany=DB::table('admin_company')->select('*')->get();
        $getCompany=json_decode(json_encode($getCompany), true);
    	$getCompanyDetails=DB::table('admin_company')->select('*')->first();   //GET ADMIN COMPANY
        $company_email_array=array();
        $default_email=array();
        if(!empty($getCompanyDetails))  //IF ADMIN COMPANY EXISTS
        {
            $company_email_array=unserialize($getCompanyDetails->email);    //UNSERIALIZED ADMIN COMPANY EMAIL SERIALIZED ARRAY
            if($getCompanyDetails->default_email!="")   //IF ADMIN COMPANY DEFAULT EMAIL EXISTS
            {
                $default_email=$getCompanyDetails->default_email;
            }
            else
            {
                $getUserDetails=DB::table('users')->select('*')->where('id',Auth::getUser()->id)->first();  //ELSE GET CURRENT USER EMAIL
                $getUserDetails=json_decode(json_encode($getUserDetails), true);
                $default_email=$getUserDetails['email'];
            }
        }
        $getUserCompany=DB::table('users')->select('company')->where('id',Auth::getUser()->id)->first();    //GET CURRENT USER COMPANY
        $getUserCompany=json_decode(json_encode($getUserCompany), true);
        $getUserCompanyDetails=DB::table('admin_company')->select('*')->where('id',$getUserCompany['company'])->first();    //GET CURRENT USER COMPANY DETAILS
        $getUserCompanyDetails=json_decode(json_encode($getUserCompanyDetails), true);
        $getInvoiceStartNumber=DB::table('settings')->select('*')->where('company_id',$getUserCompany['company'])->where('meta_key','invoice_start')->first();  //GET INVOICE START WITH BY COMPANY ID
        $getInvoiceStartNumber=json_decode(json_encode($getInvoiceStartNumber), true);
        $getEstimateStartNumber=$this->get_meta('estimate_start',$getUserCompany['company']);
        $getEstimateStartNumber=json_decode(json_encode($getEstimateStartNumber), true);
        $getReminder=DB::table('settings')->select('*')->where('company_id',$getUserCompany['company'])->where('meta_key','reminder')->first(); //GET COMPANY REMINDER
        $getReminder=json_decode(json_encode($getReminder), true);
        $getTax=DB::table('taxs')->select('*')->where('company_id',$getUserCompany['company'])->get();  //GET COMPANY TAXS
        $getTax=json_decode(json_encode($getTax), true);
        $getDiscount=DB::table('discounts')->select('*')->where('company_id',$getUserCompany['company'])->get();    //GET COMPANY DISCOUNTS
        $getDiscount=json_decode(json_encode($getDiscount), true);
        $getDefaultPaymentTerms=DB::table('settings')->select('*')->where('company_id',$getUserCompany['company'])->where('meta_key','default_payment_term')->first();
        $getDefaultPaymentTerms=json_decode(json_encode($getDefaultPaymentTerms), true);    //GET DEFAULT COMPANY DEFAULT PAYMENT TERM
        $getSmtpHost=$this->get_meta('smtp_host',$getUserCompany['company']);   //GET DEFAULT COMPANY SMTP DETAILS
        $getSmtpHost=json_decode(json_encode($getSmtpHost), true);
        $getSmtpPort=$this->get_meta('smtp_port',$getUserCompany['company']);
        $getSmtpPort=json_decode(json_encode($getSmtpPort), true);
        $getSmtpUsername=$this->get_meta('smtp_username',$getUserCompany['company']);
        $getSmtpUsername=json_decode(json_encode($getSmtpUsername), true);
        $getSmtpPassword=$this->get_meta('smtp_password',$getUserCompany['company']);
        $getSmtpPassword=json_decode(json_encode($getSmtpPassword), true);
        $notification_table=new Notification;   //CREATE NOTIFICATION CLASS OBJECT
        $getNotification=$notification_table->getNotification(Auth::getUser()->id); //CALL GET NOTIFICATION METHOD
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))    //IF NOTIFICATION EXISTS
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);
        }
        $smtp_details=array(
            'smtp_host' => $getSmtpHost['meta_value'],
            'smtp_port' => $getSmtpPort['meta_value'],
            'smtp_username' => $getSmtpUsername['meta_value'],
            'smtp_password' => $getSmtpPassword['meta_value']
            );
        if(!empty($getInvoiceStartNumber))  //IF INVOICE START NUMBER EXISTS
        {
            $startInvoiceNumber=$getInvoiceStartNumber['meta_value'];
        }
        else
        {
            $startInvoiceNumber="INV";
        }
        if(!empty($getEstimateStartNumber))  //IF INVOICE START NUMBER EXISTS
        {
            $startEstimateNumber=$getEstimateStartNumber['meta_value'];
        }
        else
        {
            $startEstimateNumber="EST";
        }
        if(!empty($getReminder))    //IF INVOICE REMINDER EXISTS
        {
            $reminder=unserialize($getReminder['meta_value']);
        }
        else
        {
            $reminder="";
        }
        if(!empty($getCompanyDetails) && $getCompanyDetails->logo!="")
        {
            $logo=$this->url->to('/').'/uploads/company-logo/'.$getCompanyDetails->logo;    //GET COMPANY LOGO
        }
        else
        {
            $logo="";
        }
        $terms_and_conditions_proposal=$this->get_meta('terms_and_conditions',$getUserCompany['company']);  //GET COMPANY PROPOSAL TERMS AND CONDITIONS
        $terms_and_conditions=json_decode(json_encode($terms_and_conditions_proposal), true);
        $estimate_terms_and_conditions_proposal=$this->get_meta('est_terms_and_conditions',$getUserCompany['company']); //GET COMPANY INVOICE TERMS AND CONDITIONS
        $estimate_terms_and_conditions_proposal=json_decode(json_encode($estimate_terms_and_conditions_proposal), true);
        $agent = new Agent();   //GET USER AGENT
        $getDealSource=array();
        $getDealSource=$this->get_meta('deal_source','1');
        $getDealSource=json_decode(json_encode($getDealSource), true);
        if(!empty($getDealSource['meta_value']))
        {
            $getDealSource=unserialize($getDealSource['meta_value']);
        }
        $get_user_profile_image=$this->get_profile_image();
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        //GET STAGES
        $getStages=DB::table('stages')->select('*')->get();
        $getStages=json_decode(json_encode($getStages), true);
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        $usersPermissionArray=array();
        if(!empty($getUsers))
        {
            foreach($getUsers as $key=>$value)
            {
                $usersPermissionArray[$value['id']]=$this->getUserPermission($value['id']);
            }
        }
        if($usersPermissionArray[Auth::getUser()->id]['setting']['view']=="off")
        {
            return redirect('/dashboard');
        }

        //GET PAYPAL CREDENTIALS
        $getPaypalCredentials=DB::table('payment_gateways')->select('*')->where('company_id',$getUserCompany['company'])->get();
        $getPaypalCredentials=json_decode(json_encode($getPaypalCredentials), true);
        $getDefaultPaymentMethod=$this->get_meta('default_payment_gateway',$getUserCompany['company']);
        $getDefaultPaymentMethod=json_decode(json_encode($getDefaultPaymentMethod), true);
        $getDefaultPaymentMethod=$getDefaultPaymentMethod['meta_value'];
        $paypal_credentials=array();
        $stripe_credentials=array();
        if(!empty($getPaypalCredentials))
        {
            foreach($getPaypalCredentials as $key=>$value)
            {
                if($value['payment_gateway']=="paypal")
                {
                    $paypal_credentials=unserialize($value['credentials']);
                }
                else
                {
                    $stripe_credentials=unserialize($value['credentials']);
                }
            }
        }
        $deal_source_color_array=['red','blue','light-blue','cyen','green','light-green','teal','light-teal','indigo','purple','lime','orange','brown','blue-grey'];
        $get_user_profile_image_array=DB::table('user_meta')->select('*')->get();
        $get_user_profile_image_array=json_decode(json_encode($get_user_profile_image_array), true);
        $getCompanyCurrency=array();
        $getCompanyCurrency=DB::table('currency')->select('*')->where('company_id',$getUserCompany['company'])->first();    //GET COMPANY CURRENCY
        $getCompanyCurrency=json_decode(json_encode($getCompanyCurrency), true);
        if(!empty($getCompanyCurrency))
        {
            $getCompanyCurrency=unserialize($getCompanyCurrency['currency_list']);
        }
        $getLanguage=$this->getLanguage();
        $getDateFormat=$this->get_meta('date_format',$getUserCompany['company']);
        $getDateFormat=json_decode(json_encode($getDateFormat), true);
        $getInvitation=DB::table('invitation')->select('*')->get();
        $getInvitation=json_decode(json_encode($getInvitation), true);
        $timezone=env("TIME_ZONE"); //GET ENVIRONMENT TIME ZONE
        if($agent->isMobile())
        {
            return View::make('dashboard/settings/settings-mobile',array('getCompanyDetails' => $getCompanyDetails , 'company_email_array' => $company_email_array , 'default_email' => $default_email , 'page' => $name , 'roles' => $roles_array , 'company' => $getCompany ,'users' => $getUsers , 'user_company' => $getUserCompanyDetails , 'startInvoiceNumber' => $startInvoiceNumber ,  'startEstimateNumber' => $startEstimateNumber , 'reminder' => $reminder , 'tax' => $getTax , 'discount' => $getDiscount , 'default_payment_term' => $getDefaultPaymentTerms , 'logo' => $logo , 'smtp_details' => $smtp_details , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'terms_and_conditions' => $terms_and_conditions['meta_value'] , 'est_terms_and_conditions' => $estimate_terms_and_conditions_proposal['meta_value'] , 'profile_image' => $get_user_profile_image , 'active' => 'setting' , 'user_profile_image_array' => $get_user_profile_image_array , 'show_tutorial' => $checkShowTutorial , 'stages' => $getStages , 'permission' => $getUserPermission , 'users_permission' => $usersPermissionArray , 'timezone' => $timezone , 'paypal_credentials' => $paypal_credentials , 'stripe_credentials' => $stripe_credentials , 'defaultPaymentMethod' => $getDefaultPaymentMethod , 'deal_source' => $getDealSource , 'deal_source_color_array' => $deal_source_color_array , 'invitation_array' => $getInvitation , 'date_format' => $getDateFormat , 'language' => $getLanguage , 'company_currency' => $getCompanyCurrency));
        }
        else
        {
            return View::make('dashboard/settings/settings',array('getCompanyDetails' => $getCompanyDetails , 'company_email_array' => $company_email_array , 'default_email' => $default_email , 'page' => $name , 'roles' => $roles_array , 'company' => $getCompany ,'users' => $getUsers , 'user_company' => $getUserCompanyDetails , 'startInvoiceNumber' => $startInvoiceNumber , 'startEstimateNumber' => $startEstimateNumber , 'reminder' => $reminder , 'tax' => $getTax , 'discount' => $getDiscount , 'default_payment_term' => $getDefaultPaymentTerms , 'logo' => $logo , 'smtp_details' => $smtp_details , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'terms_and_conditions' => $terms_and_conditions['meta_value'] , 'est_terms_and_conditions' => $estimate_terms_and_conditions_proposal['meta_value'] , 'profile_image' => $get_user_profile_image , 'active' => 'setting' , 'user_profile_image_array' => $get_user_profile_image_array , 'show_tutorial' => $checkShowTutorial , 'stages' => $getStages , 'permission' => $getUserPermission , 'users_permission' => $usersPermissionArray , 'timezone' => $timezone , 'paypal_credentials' => $paypal_credentials , 'stripe_credentials' => $stripe_credentials , 'defaultPaymentMethod' => $getDefaultPaymentMethod , 'deal_source' => $getDealSource , 'deal_source_color_array' => $deal_source_color_array , 'invitation_array' => $getInvitation , 'date_format' => $getDateFormat , 'language' => $getLanguage , 'company_currency' => $getCompanyCurrency));
        }
    	
    }

    public function uploadCompanyLogo()
    {
    	$message="failed_to_upload";
    	$fileName="";
    	if (Input::hasFile('company-logo'))    //IF INPUT COMPANY LOGO HAS FILE
	    {
            if(Input::get('x')=="" || Input::get('y')=="" || Input::get('h')=="" || Input::get('w')=="")    //IF X,Y,H,W ANY OF THESE ARE EMPTY THEN LOGO WILL BE UPLOADED WITH ITS FULL X,Y,H,W ELSE LOGO WILL BE CROPPED
            {
                $re = Input::file('company-logo')->getClientOriginalName();
                   $destinationPath = base_path('uploads/company-logo'); // upload path
                   $file_name=Input::file('company-logo')->getClientOriginalName();
                   $file_name = pathinfo($file_name, PATHINFO_FILENAME);
                   $extension = Input::file('company-logo')->getClientOriginalExtension(); // getting image extension
                   $fileName = $file_name.'-'.rand(11111,99999).'.'.$extension; // renameing image
                   Input::file('company-logo')->move($destinationPath, $fileName); // uploading file to given path
            }
            else
            {
                $file=Input::file('company-logo');
                $destinationPath = base_path('uploads/company-logo');
                $filename = $file->getClientOriginalName();
                $filename=pathinfo($filename, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $thumbName      =   $filename.'-'.rand(11111,99999).'.'.$extension;
                $img = Image::make($file->getRealPath())->crop(Input::get('w'), Input::get('h'), Input::get('x'), Input::get('y'));
                $img->save(base_path('uploads/company-logo'). '/'. $thumbName);
                $fileName=$thumbName;
            }
           $message="file_uploaded";
	    }
	    return response()->json(['response' => $message , 'fileName' => $fileName]);
    }

    public function saveCompanyDetails()
    {
        $path = base_path('.env');   //GET ENVIRONMENT FILE
        file_put_contents($path, str_replace(
            'PORTAL_NAME='.env("PORTAL_NAME", ""), 'PORTAL_NAME='.Input::get('portal_name'), file_get_contents($path)
        ));
        file_put_contents($path, str_replace(
            'LANG='.env("LANG", ""), 'LANG='.Input::get('language'), file_get_contents($path)
        ));
        file_put_contents($path, str_replace(
            'TIME_ZONE='.env("TIME_ZONE", ""), 'TIME_ZONE='.Input::get('timezone'), file_get_contents($path)
        ));
        $updateDateFormat=$this->update_meta('date_format',Input::get('date_format'),'1');
    	if(Input::get('company_id')=="")
    	{
            $getAdminEmail=DB::table('users')->select('*')->where('id','1')->first();
            $getAdminEmail=json_decode(json_encode($getAdminEmail), true);
            $getAdminEmail=$getAdminEmail['email'];
    		$createCompany=DB::table('admin_company')->insertGetId(['company_name' => Input::get('company_name'), 'default_email' => $getAdminEmail, 'marked_as_default' => 'yes', 'buisness_type' => Input::get('buisness_type'), 'address' => Input::get('address'), 'city' => Input::get('city'), 'state' => Input::get('state'), 'country' => Input::get('country'), 'post_code' => Input::get('post_code'), 'portal_name' => str_replace('-', ' ', Input::get('portal_name')), 'logo' => Input::get('logo'), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            if(Input::get('uploaded_image')!="")
            {
                $updateLogo=DB::table('admin_company')->where('id',Input::get('company_id'))->update(['logo' => Input::get('uploaded_image')]);
            }
    	}
    	else
    	{
    		$updateCompany=DB::table('admin_company')->select('id',Input::get('company_id'))->update(['company_name' => Input::get('company_name'), 'buisness_type' => Input::get('buisness_type'), 'address' => Input::get('address'), 'city' => Input::get('city'), 'state' => Input::get('state'), 'country' => Input::get('country'), 'post_code' => Input::get('post_code'), 'portal_name' => str_replace('-', ' ', Input::get('portal_name'))]);
    		if(Input::get('uploaded_image')!="")
    		{
    			$updateLogo=DB::table('admin_company')->where('id',Input::get('company_id'))->update(['logo' => Input::get('uploaded_image')]);
    		}
    	}
    	return response()->json(['response' => 'success']);
    }

    public function getCompanyEmail()
    {
        if(Input::get('email')!="")
        {
            $getCompanyEmail=DB::table('admin_company')->select('*')->where('id',Input::get('company_id'))->first();
            $getCompanyEmail=json_decode(json_encode($getCompanyEmail), true);
            $company_email_array=array();
            foreach(Input::get('email') as $key=>$value)
            {
                if($value!="")
                {
                    array_push($company_email_array,$value);
                }
            }
            $company_email=serialize($company_email_array);
            $updateCompanyEmail=DB::table('admin_company')->where('id',Input::get('company_id'))->update(['email' => $company_email]);
        }
        if(Input::get('default_email')!="")
        {
            $updateCompanyDefaultEmail=DB::table('admin_company')->where('id',Input::get('company_id'))->update(['default_email' => Input::get('default_email') , 'marked_as_default' => Input::get('marked_as_default')]);
        }
    	return response()->json(['response' => 'success']);
    }

    public function createUser()
    {
        $randomString=$this->generateRandomString();
        $getUser=DB::table('users')->select('*')->where('email',Input::get('email'))->first();
        $getUser=json_decode(json_encode($getUser), true);
        if(!empty($getUser))
        {
            return response()->json(['response' => 0]);
        }
        else
        {
            $createUser=DB::table('invitation')->insertGetId(['first_name' => Input::get('first_name'), 'last_name' => Input::get('last_name'), 'name' => Input::get('first_name')." ".Input::get('last_name'), 'email' => Input::get('email'), 'company' => Input::get('company'), 'phone_no' => Input::get('phone'), 'role' => Input::get('role'), 'department' => Input::get('department') , 'key' => $randomString, 'purpose' => 'sign-up', 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s')]);
            $name=Input::get('first_name')." ".Input::get('last_name');
            $invitation_link=$this->url->to('/').'/accept_invitation/link/'.$randomString.'?email='.urlencode(Input::get('email'));
            $getAdminCompanyDetails=DB::table('admin_company')->select('*')->first();
            $getAdminCompanyDetails=json_decode(json_encode($getAdminCompanyDetails), true);
            $getAdminName=DB::table('users')->select('*')->where('role','1')->first();
            $getAdminName=json_decode(json_encode($getAdminName), true);
            $sender_name=$getAdminCompanyDetails['sender_name'];
            $admin_name=$getAdminName['first_name']." ".$getAdminName['last_name'];
            $admin_company=$getAdminCompanyDetails['company_name'];
            $base_url=$this->url->to('/');
            if($sender_name=="")
            {
                $sender_name=$getAdminName['first_name'];
            }
            $sender_email=$getAdminCompanyDetails['default_email'];
            if($sender_email=="" || $getAdminCompanyDetails['marked_as_default']=="no")
            {
                $getAdminName=DB::table('users')->select('*')->where('role','1')->first();
                $getAdminName=json_decode(json_encode($getAdminName), true);
                $sender_email=$getAdminName['email'];
            }
            $this->invitation_email($name,Input::get('email'),$invitation_link,$sender_name,$sender_email,$admin_name,$admin_company,$base_url);
            return response()->json(['response' => $createUser]);
        }
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function deleteUser()
    {
        $deleteUser=DB::table('users')->where('id',Input::get('user_id'))->delete();
        return response()->json(['response' => Input::get('user_id')]);
    }

    public function editUser()
    {
        if(Input::get('user_id')=="")
        {
            return redirect('settings');
        }
        $getUserDetails=DB::table('users')->select('*')->where('id',Input::get('user_id'))->first();
        $getUserDetails=json_decode(json_encode($getUserDetails), true);
        $getCompany=DB::table('admin_company')->select('*')->get();
        $getCompany=json_decode(json_encode($getCompany), true);
        $getRoles=DB::table('settings')->select('*')->where('id',$getUserDetails['company'])->where('meta_key','roles')->first();
        $getRoles=json_decode(json_encode($getRoles), true);
        return view('dashboard/settings/user-edit', ['userDetails' => $getUserDetails , 'company' => $getCompany , 'roles' => unserialize($getRoles['meta_value'])]);
    }

    public function userSearch()
    {
        $user_name=Input::get('user_name');
        $getUser=DB::table('users')->select('*')->where('name',$user_name)->first();
        $getUser=json_decode(json_encode($getUser), true);
        $getRoles=DB::table('settings')->select('*')->where('meta_key','roles')->first();
        $getRoles=json_decode(json_encode($getRoles), true);
        $getRolesArray=unserialize($getRoles['meta_value']);
        if(!empty($getUser))
        {
            return response()->json(['status' => 'success' , 'user' => $getUser , 'roles' => $getRolesArray]);
        }
        else
        {
            return response()->json(['status' => 'failed']);
        }
    }

    public function saveSettingsProposal()
    {
        $changeDefaultEmail=DB::table('admin_company')->where('id',Input::get('company_id'))->update(['default_email' => Input::get('default_email') , 'sender_name' => Input::get('sender_name')]);
        if(Input::get('purpose')=="proposal")
        {
           $changeInvoiceNumberPrefix=DB::table('settings')->where('company_id',Input::get('company_id'))->where('meta_key','invoice_start')->update(['meta_value' => Input::get('invoice_number_prefix')]); 
        }
        else
        {
            $changeEstimateNumberPrefix=DB::table('settings')->where('company_id',Input::get('company_id'))->where('meta_key','estimate_start')->update(['meta_value' => Input::get('estimate_number_prefix')]);
        }
        $getCurrentUserCompany=DB::table('admin_company')->join('users', 'users.company', '=', 'admin_company.id')->select('*')->where('users.id', '=', Auth::getUser()->id)->first();
        $getCurrentUserCompany=json_decode(json_encode($getCurrentUserCompany), true);
        $company_id=$getCurrentUserCompany['id'];
        $settings=new Setting;
        $reminder=$settings::where('company_id', $company_id)->where('meta_key','reminder')->first();
        if(!empty($reminder))
        {
            $reminder->meta_value=serialize(Input::get('reminder_array'));
            $reminder->save();
        }
        else
        {
            $settings->meta_key="reminder";
            $settings->meta_value=serialize(Input::get('reminder_array'));
            $settings->company_id=Input::get('company_id');
            $settings->save();
        }
        $default_payment_term=$settings::where('company_id', $company_id)->where('meta_key','default_payment_term')->first();
        if(!empty($default_payment_term))
        {
            $default_payment_term->meta_value=Input::get('default_payment_term');
            $default_payment_term->save();
        }
        else
        {
            $settings->meta_key="default_payment_term";
            $settings->meta_value=Input::get('default_payment_term');
            $settings->company_id=Input::get('company_id');
            $settings->save();
        }
        if(Input::get('purpose')=="proposal")
        {
            $terms_and_conditions=$settings::where('company_id', $company_id)->where('meta_key','terms_and_conditions')->first();
            $terms_and_conditions->meta_value=Input::get('terms_and_conditions');
            $terms_and_conditions->save();
        }
        else
        {
            $est_terms_and_conditions=$settings::where('company_id', $company_id)->where('meta_key','est_terms_and_conditions')->first();
            $est_terms_and_conditions->meta_value=Input::get('terms_and_conditions');
            $est_terms_and_conditions->save();
        }
        if(!empty(Input::get('tax_name_array')))
        {
            foreach(Input::get('tax_name_array') as $key=>$value)
            {
                if(Input::get('tax_id_array')[$key]!=0)
                {
                    $updateTax=DB::table('taxs')->where('id',Input::get('tax_id_array')[$key])->update(['company_id' => $company_id, 'tax_name' => $value, 'tax_type' => Input::get('tax_type_array')[$key], 'tax_amount' => Input::get('tax_amount_array')[$key]]);
                }
                else
                {
                    $insertTax=DB::table('taxs')->insertGetId(['company_id' => $company_id, 'tax_name' => $value, 'tax_type' => Input::get('tax_type_array')[$key], 'tax_amount' => Input::get('tax_amount_array')[$key], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                }
            }
        }
        if(!empty(Input::get('discount_name_array')))
        {
            foreach(Input::get('discount_name_array') as $key=>$value)
            {
                if(Input::get('discount_id_array')[$key]!=0)
                {
                    $updateTax=DB::table('discounts')->where('id',Input::get('discount_id_array')[$key])->update(['company_id' => $company_id, 'discount_name' => $value, 'discount_type' => Input::get('discount_type_array')[$key], 'discount_amount' => Input::get('discount_amount_array')[$key]]);
                }
                else
                {
                    $insertDiscount=DB::table('discounts')->insertGetId(['company_id' => Input::get('company_id'), 'discount_name' => $value, 'discount_type' => Input::get('discount_type_array')[$key], 'discount_amount' => Input::get('discount_amount_array')[$key], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                }
            }
        }
        return response()->json(['status' => 'success']);
    }

    public function saveSmtpDetails()
    {
        $this->setEnvSmtpDetails(Input::get('smtp_host'),Input::get('smtp_port'),Input::get('smtp_username'),Input::get('smtp_password'));
        $updateSmtpHost=$this->update_meta('smtp_host',Input::get('smtp_host'),Input::get('company_id'));
        $updateSmtpPort=$this->update_meta('smtp_port',Input::get('smtp_port'),Input::get('company_id'));
        $updateSmtpUsername=$this->update_meta('smtp_username',Input::get('smtp_username'),Input::get('company_id'));
        $updateSmtpPassword=$this->update_meta('smtp_password',Input::get('smtp_password'),Input::get('company_id'));
        $path = base_path('.env');
        return response()->json(['status' => 'success']);
    }

    public function sendTestMail()
    {
        $getSenderName=DB::table('admin_company')->select('*')->where('id','1')->first();
        $getSenderName=json_decode(json_encode($getSenderName), true);
        $sender_email=$getSenderName['default_email'];
        $sender_name=$getSenderName['sender_name'];
        if(!empty($getSenderName))
        {
            $sender_email=$getSenderName['default_email'];
            if(!empty($getSenderName['default_email']) && $getSenderName['marked_as_default']=="yes")
            {
                $sender_name=$getSenderName['sender_name'];
            }
            else
            {
                $getAdminName=DB::table('users')->select('*')->where('role','1')->first();
                $getAdminName=json_decode(json_encode($getAdminName), true);
                $sender_name=$getAdminName['first_name'];
            }
        }
        else
        {
            $getAdminEmail=DB::table('users')->select('*')->where('role','1')->first();
            $getAdminEmail=json_decode(json_encode($getAdminEmail), true);
            $sender_name=$getAdminEmail['first_name'];
            $sender_email=$getAdminEmail['email'];
        }
        $checkSmtp=$this->checkSmtp();
        if($checkSmtp!="ok")
        {
            return response()->json(['status' => $checkSmtp]);
        }
        else
        {
            $getPortalName=env('PORTAL_NAME');
            $sendTestMail=$this->send_test_mail(Input::get('email'),$sender_name,$sender_email,$getPortalName);
            return response()->json(['status' => $checkSmtp]);
        }
    }

    public function saveEditUser()
    {
        $editUser=DB::table('users')->where('id',Input::get('user_id'))->update(['first_name' => Input::get('first_name'), 'last_name' => Input::get('last_name'), 'phone_no' => Input::get('phone_no'), 'role' => Input::get('role'), 'status' => Input::get('status')]);
        $changePermission=$this->updateUserPermission(Input::get('user_id'),serialize(Input::get('contact_permission')),serialize(Input::get('deal_permission')),serialize(Input::get('task_permission')),serialize(Input::get('invoice_permission')),serialize(Input::get('estimate_permission')),serialize(Input::get('setting_permission')));
        return response()->json(['status' => Input::get('user_id')]);
    }

    public function imageCrop()
    {
        $quality = 90;

        $src  = Input::get('image');
        return response()->json(['status' => $src]);
    }

    public function getNotificationDifference($getNotification)
    {
        date_default_timezone_set(env('TIME_ZONE'));
        $notification_difference_array=array();
        foreach($getNotification as $key=>$value)
        {
            $datetime1 = new DateTime($getNotification[$key]['created_at']);
            $datetime2 = new DateTime(date("Y-m-d h:i:s a"));
            $interval = $datetime1->diff($datetime2);
            if($interval->format('%h')>0)
            {
                $interval = $interval->format('%h')." hr ".$interval->format('%i')." min ago";
            }
            else
            {
                $interval = $interval->format('%i')." min ago";
            }
            array_push($notification_difference_array,$interval);
        }
        return $notification_difference_array;
    }

    public function userEdit()
    {
        $userDetails=array();
        $getUserDetails=DB::table('users')->select('*')->where('id',Input::get('user_id'))->first();
        $getUserDetails=json_decode(json_encode($getUserDetails), true);
        $getUserMeta=DB::table('user_meta')->select('*')->where('user_id',Input::get('user_id'))->first();
        $getUserMeta=json_decode(json_encode($getUserMeta), true);
        $userDetails['id']=$getUserDetails['id'];
        $userDetails['first_name']=$getUserDetails['first_name'];
        $userDetails['last_name']=$getUserDetails['last_name'];
        $userDetails['email']=$getUserDetails['email'];
        $userDetails['phone_no']=$getUserDetails['phone_no'];
        $getRole=$this->get_meta('roles',$getUserDetails['company']);
        $getRole=json_decode(json_encode($getRole), true);
        $roles_array=unserialize($getRole['meta_value']);
        $userDetails['role']=$roles_array[$getUserDetails['role']];
        $getUserPermission=$this->getUserPermission($getUserDetails['id']);
        $getUserPermission=json_decode(json_encode($getUserPermission), true);
        $userDetails['permission']=$getUserPermission;
        $userDetails['status']=$getUserDetails['status'];
        $userDetails['image']=$getUserMeta['image'];
        return $userDetails;
    }

    public function addDealSource()
    {
        $getDealSource=$this->get_meta('deal_source',Input::get('company_id'));
        $getDealSource=json_decode(json_encode($getDealSource), true);
        if(!empty($getDealSource) && !empty($getDealSource['meta_value']))
        {
            $unserializeDealSource=unserialize($getDealSource['meta_value']);
            array_push($unserializeDealSource,Input::get('source'));
            $serializeDealSource=serialize($unserializeDealSource);
            $add_deal_source=$this->update_meta('deal_source',$serializeDealSource,Input::get('company_id'));
        }
        else
        {
            $unserializeDealSource[]=Input::get('source');
            $serializeDealSource=serialize($unserializeDealSource);
            $add_deal_source=DB::table('settings')->insertGetId(['meta_key' => 'deal_source', 'meta_value' => $serializeDealSource, 'company_id' => Input::get('company_id'), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        }
        $deal_source_color_array=['red','blue','light-blue','cyen','green','light-green','teal','light-teal','indigo','purple','lime','orange','brown','blue-grey'];
        $deal_source_color=$deal_source_color_array[array_rand($deal_source_color_array)];
        return response()->json(['response' => $add_deal_source , 'deal_source_color' => $deal_source_color]);
    }

    public function removeDealSource()
    {
        $getDealSource=$this->get_meta('deal_source',Input::get('company_id'));
        $getDealSource=json_decode(json_encode($getDealSource), true);
        $unserializeDealSource=unserialize($getDealSource['meta_value']);
        $pos=array_search(Input::get('source'),$unserializeDealSource);
        unset($unserializeDealSource[$pos]);
        $serializeDealSource=serialize($unserializeDealSource);
        $update_deal_source=$this->update_meta('deal_source',$serializeDealSource,Input::get('company_id'));
        return response()->json(['response' => $update_deal_source]);
    }

    public function addCurrency()
    {
        $getCurrency=DB::table('currency')->select('*')->where('id',Input::get('company_id'))->first();
        $getCurrency=json_decode(json_encode($getCurrency), true);
        $currency_color_array=['red','blue','light-blue','cyen','green','light-green','teal','light-teal','indigo','purple','lime','orange','brown','blue-grey'];
        $currency_color_array=$currency_color_array[array_rand($currency_color_array)];
        $currency_array=array();
        if(!empty($getCurrency))    //CHECK IF CURRENCY ARRAY IS NOT EMPTY
        {
            $unserialize_currency_array=unserialize($getCurrency['currency_list']); //UNSERIALIZE CURRENCY LIST
            if(in_array(Input::get('currency'),$unserialize_currency_array))
            {
                return response()->json(['response' => 'failed']);
            }
            array_push($unserialize_currency_array,Input::get('currency'));
            $currency_array=serialize($unserialize_currency_array);
            $update_currency=DB::table('currency')->where('id',Input::get('company_id'))->update(['currency_list' => $currency_array]);
            return response()->json(['response' => $update_currency , 'currency_color_array' => $currency_color_array]);
        }
        else
        {
            $currency_array[]=Input::get('currency');
            $currency_array=serialize($currency_array);
            $add_currency=DB::table('currency')->insertGetId(['currency_list' => $currency_array, 'company_id' => Input::get('company_id'), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            return response()->json(['response' => $add_currency , 'currency_color_array' => $currency_color_array]);
        }
    }

    public function removeCurrency()
    {
        $getCurrency=DB::table('currency')->select('*')->where('id',Input::get('company_id'))->first();
        $getCurrency=json_decode(json_encode($getCurrency), true);
        $unserializeCurrency=unserialize($getCurrency['currency_list']);
        $pos=array_search(Input::get('currency'),$unserializeCurrency);
        unset($unserializeCurrency[$pos]);
        $serializeCurrency=serialize($unserializeCurrency);
        $update_currency=DB::table('currency')->where('id',Input::get('company_id'))->update(['currency_list' => $serializeCurrency]);
        return response()->json(['response' => $update_currency]);
    }

    public function changeDealVisiblity()
    {
        $updateDealVisiblity=DB::table('stages')->where('id',Input::get('id'))->update(['stage' => Input::get('value') , 'visiblity' => Input::get('visiblity')]);
        return response()->json(['response' => $updateDealVisiblity]);
    }

    public function addDealStage()
    {
        $addDealStage=DB::table('stages')->insertGetId(['slug' => strtolower(Input::get('deal_stage')) , 'stage' => strtolower(Input::get('deal_stage')) , 'visiblity' => 'active' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        return response()->json(['response' => $addDealStage]);
    }
}
