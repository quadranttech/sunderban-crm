<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Routing\UrlGenerator;
use DB;
use Auth;
use App\Feed;
use Redirect;
use View;
use Illuminate\Support\Facades\Input;

class FeedController extends Controller
{
    //
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;  //GET BASE URL
        $this->middleware('auth');  //REDIRECTED TO LOGIN IF NOT AUTHENTICATED
    }

    public function index()
    {
    	$feed_table=new Feed;  //CREATE FEED CLASS OBJECT
    	$date_format_array=array();
    	$time_format_array=array();
    	$method = Request::method();
    	$contact_id=Input::get('contact_id');  //GET CONTACT ID
    	$user_id=Auth::user()->id; //CURRENT USER ID
    	$getFeedByContactId=$feed_table->getFeedByContactId($user_id,$contact_id); //CALL GET FEED BY CONTACT METHOD
        $getUserImage=array();  //CREATE USER IMAGE ARRAY
        $userImage=array();
    	foreach($getFeedByContactId as $key=>$value)
    	{
    		// $structuredDate=DATE_FORMAT($value['created_at'],'%b %d %Y %h:%i %p');
            $getUserImage['feed_id-'.$value['id']]=$value['user_id'];
    		$structuredDate=date('d F, Y', strtotime($value['created_at']));  //FEED DATE
    		$structuredTime=date('h:i a', strtotime($value['created_at']));   //FEED TIME
    		array_push($date_format_array,$structuredDate);
    		array_push($time_format_array,$structuredTime);
    	}
        if(!empty($getUserImage))   //IF USER IMAGE ARRAY EXISTS
        {
            foreach($getUserImage as $key=>$value)
            {
                $tempUserImage=DB::table('user_meta')->select('*')->where('id',$value)->first();
                $tempUserImage=json_decode(json_encode($tempUserImage), true);
                $userImage[explode("-",$key)[1]]=$tempUserImage['image'];   //CREATE USER IMAGE ARRAY WITH USER ID INDEX AND USER IMAGE VALUE
            }
        }
    	$base_url=$this->url->to('/');
    	$content=file_get_contents($base_url.'/resources/views/templates/feed.php');   //GET FEED TEMPLATE
        $feed_color_array=['red','blue','light-blue','cyen','green','light-green','teal','light-teal','indigo','purple','lime','orange','brown','blue-grey'];   //FEED COLOR ARRAY
    	return response()->json(['status' => 'success' , 'feed' => $getFeedByContactId , 'date_format_array' => $date_format_array , 'time_format_array' => $time_format_array , 'content' => $content , 'feed_color_array' => $feed_color_array , 'userImage' => $userImage]);
    }

    public function showDealFeed()
    {
        $deal_id=Input::get('deal_id');
        $getContact=DB::table('deals')->select('*')->where('id',$deal_id)->first(); //GET DEAL ID
        $getFeed=DB::table('feeds')->select('*')->where('deal_id',$deal_id)->where('feed_type','deal')->limit(5)->orderBy('created_at','desc')->get();  //GET CONTACT FEED
        $getFeed=json_decode(json_encode($getFeed), true);
        if(!empty($getFeed))    //IF FEED EXISTS
        {
            $base_url=$this->url->to('/');
            $content=file_get_contents($base_url.'/resources/views/templates/feed.php');    //GET FEED TEMPLATE
            foreach($getFeed as $key=>$value)
            {
                $getUserImage['feed_id-'.$value['id']]=$value['user_id'];
            }
            if(!empty($getUserImage))
            {
                foreach($getUserImage as $key=>$value)
                {
                    $tempUserImage=DB::table('user_meta')->select('*')->where('id',$value)->first();
                    $tempUserImage=json_decode(json_encode($tempUserImage), true);
                    $userImage[explode("-",$key)[1]]=$tempUserImage['image'];
                }
            }
            $feed_color_array=['red','blue','light-blue','cyen','green','light-green','teal','light-teal','indigo','purple','lime','orange','brown','blue-grey'];
            return response()->json(['status' => 'success' , 'response' => $getFeed , 'content' => $content , 'feed_color_array' => $feed_color_array , 'userImage' => $userImage]);
        }
        else
        {
            return response()->json(['status' => 'failed']);
        }
        
    }
}
