<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Traits\MainTrait;
use App\Traits\PermissionTrait;

class BaseController extends Controller
{
    //
    use MainTrait;
    use PermissionTrait;
    // public function __construct(UrlGenerator $url)
    // {
    //     $this->url = $url;
    //     $this->agent = new Agent();
    //     $this->middleware('auth');
    // }

    public function createHeader()
    {
        $result = array();
        //GET PERMISSION
        $getUserPermission = $this->getUserPermission(Auth::getUser()->id);
        //GET PROFILE IMAGE
        $get_user_profile_image = $this->get_profile_image();
        //GET SHOW TUTORIAL
        $checkShowTutorial = $this->checkShowTutorial();
        $result = ['permission' => $getUserPermission , 'profile_image' => $get_user_profile_image , 'show_tutorial' => $checkShowTutorial];
        return $result;
    }
}
