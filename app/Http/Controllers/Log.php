<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use View;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Request;
use Jenssegers\Agent\Agent;

class Log extends Controller
{

	use AuthenticatesUsers;

	protected $redirectTo = '/dashboard';

	public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->agent=new Agent();
    }
    
    public function bull()
    {
        $path = base_path('.env');  //GET ENVIRONMENT FILE
        if (file_exists($path)) {   // IF ENVIRONMENT FILE EXISTS
            if(env("DB_HOST", "")=="" && env("DB_DATABASE", "")=="" && env("DB_USERNAME", "")=="" && env("DB_PASSWORD", "")=="")    //IF ANY OF THESE VALUES ARE NULL IN ENVIRONMENT FILE
            {
                return redirect('/install');
            }
        }
        if(Request::input('user_email')!="" && Request::input('key')!="")   //IF USER EMAIL AND KEY EXISTS
        {
            $getAuthenticatedInvitation=DB::table('invitation')->select('*')->where('email',urldecode(Request::input('user_email')))->where('key',Request::input('key'))->first();  //GET INVITIATION FROM USER EMAIL AND KEY
            if(!empty($getAuthenticatedInvitation)) //IF INVITIATION EXISTS
            {
                $getCompany=DB::table('admin_company')->select('*')->get(); //GET ADMIN COMPANY
                $getCompany=json_decode(json_encode($getCompany), true);
                if($this->agent->isMobile())
                {
                    return View::make('newlogin-mobile',array('company' => $getCompany , 'role' => $getAuthenticatedInvitation->role , 'purpose' => 'signup' , 'first_name' => $getAuthenticatedInvitation->first_name , 'last_name' => $getAuthenticatedInvitation->last_name , 'email' => $getAuthenticatedInvitation->email , 'user_company' => $getAuthenticatedInvitation->company , 'department' => $getAuthenticatedInvitation->department , 'phone_no' => $getAuthenticatedInvitation->phone_no , 'key' => Request::input('key') , 'portal_name' => $getCompany[0]['portal_name']));
                }
                return View::make('newlogin',array('company' => $getCompany , 'role' => $getAuthenticatedInvitation->role , 'purpose' => 'signup' , 'first_name' => $getAuthenticatedInvitation->first_name , 'last_name' => $getAuthenticatedInvitation->last_name , 'email' => $getAuthenticatedInvitation->email , 'user_company' => $getAuthenticatedInvitation->company , 'department' => $getAuthenticatedInvitation->department , 'phone_no' => $getAuthenticatedInvitation->phone_no , 'key' => Request::input('key') , 'portal_name' => $getCompany[0]['portal_name']));
            }
            else
            {
                $getCompany=DB::table('admin_company')->select('*')->get();
                $getCompany=json_decode(json_encode($getCompany), true);
                if($this->agent->isMobile())
                {
                    return View::make('newlogin-mobile',array('company' => $getCompany , 'role' => $getAuthenticatedInvitation->role , 'purpose' => 'login' , 'portal_name' => $getCompany[0]['portal_name']));
                }
                return View::make('newlogin',array('company' => $getCompany , 'role' => $getAuthenticatedInvitation->role , 'purpose' => 'login' , 'portal_name' => $getCompany[0]['portal_name']));
            }
        }
        else
        {
            $getCompany=DB::table('admin_company')->select('*')->get();
            $getCompany=json_decode(json_encode($getCompany), true);
            if($this->agent->isMobile())
            {
                return View::make('newlogin-mobile',array('company' => $getCompany , 'role' => '1' , 'purpose' => 'login' , 'user_company' => array() , 'portal_name' => $getCompany[0]['portal_name']));
            }
            return View::make('newlogin',array('company' => $getCompany , 'role' => '1' , 'purpose' => 'login' , 'user_company' => array() , 'portal_name' => $getCompany[0]['portal_name']));
        }
    }

    public function authenticate()
    {
        if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')])) {    //IF USER AUTHENTICATED
            // Authentication passed...
            return redirect('/dashboard');
        }
        else
        {
            return redirect('/welcome')->withErrors(array('email' => 'Invalid Email.'))->withInput();
        }
    }

    public function logout()
    {
        return View::make('newlogin',array('company' => $getCompany , 'role' => '1' , 'purpose' => 'logout'));
    }
}
