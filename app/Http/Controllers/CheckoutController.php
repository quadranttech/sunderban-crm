<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Paypal;
use DB;
use App\Traits\MainTrait;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Input;

class CheckoutController extends Controller
{

	use MainTrait;
	private $_apiContext;

    public function __construct(UrlGenerator $url)
    {
    	$this->url = $url;
        $this->_apiContext = PayPal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ));

    }

    public function checkout(Request $request,$link)
    {
    	$getInvoice=DB::table('invoices')->select('*')->where('link',$link)->first();
    	$getInvoice=json_decode(json_encode($getInvoice), true);
    	$getInvoiceItems=DB::table('invoice_items')->select('*')->where('inv_id',$getInvoice['id'])->get();
    	$getInvoiceItems=json_decode(json_encode($getInvoiceItems), true);
    	$getInvoiceMeta=DB::table('invoice_meta')->select('*')->where('inv_id',$getInvoice['id'])->first();
    	$getInvoiceMeta=json_decode(json_encode($getInvoiceMeta), true);
    	$getDefaultPaymentMethod=$this->get_meta('default_payment_gateway',$getInvoice['company_id']);
    	$getDefaultPaymentMethod=json_decode(json_encode($getDefaultPaymentMethod), true);
    	$getContactDetails=DB::table('contacts')->select('*')->where('id',$getInvoice['contact'])->first();
    	$getContactDetails=json_decode(json_encode($getContactDetails), true);
    	$getContactAddressDetails=DB::table('contacts_address')->select('*')->where('contact_id',$getContactDetails['id'])->first();
    	$getContactAddressDetails=json_decode(json_encode($getContactAddressDetails), true);
    	$getCompanyDetails=DB::table('admin_company')->select('*')->where('id',$getInvoice['company_id'])->first();
    	$getCompanyDetails=json_decode(json_encode($getCompanyDetails), true);
    	$getInvoiceItemTotal=DB::table('invoice_items')->where('inv_id',$getInvoice['id'])->sum('total');
        $getInvoiceItemTotal=json_decode(json_encode($getInvoiceItemTotal), true);
        $getTermsAndConditions=$this->get_meta('terms_and_conditions',$getInvoice['company_id']);
        $getTermsAndConditions=json_decode(json_encode($getTermsAndConditions), true);
    	$part_pay=$getInvoiceMeta['part_pay'];
		$part_pay_value=$getInvoiceMeta['part_pay_value'];
		$inv_total=$getInvoiceMeta['inv_total'];
    	if(session('checkout')!="done")
    	{
			if($getInvoice['payment_status']!="paid")
			{
				if($getInvoice['payment_status']=="unpaid")
		    	{
		    		if($part_pay=="fixed")
		    		{
		    			$payment_to_be_proceed=number_format((double)$part_pay_value, 2, '.', '');
		    		}
		    		else if($part_pay=="percentile")
		    		{
		    			$payment_to_be_proceed=$inv_total-(($inv_total*$part_pay_value)/100);
		    			$payment_to_be_proceed=number_format((double)$payment_to_be_proceed, 2, '.', '');
		    		}
		    		else
		    		{
		    			$payment_to_be_proceed=number_format((double)$inv_total, 2, '.', '');
		    		}
		    	}
		    	if($getInvoice['payment_status']=="partly_paid")
		    	{
					$payment_to_be_proceed=$inv_total-$getInvoice['paid'];
					$payment_to_be_proceed=number_format((double)$payment_to_be_proceed, 2, '.', '');
		    	}
		    	if($getDefaultPaymentMethod['meta_value']=="paypal")
		    	{
		    		return view('checkout', ['payment_link' => $this->url->to('/')."/paypal-checkout/".$payment_to_be_proceed."/".$link , 'id' => $getInvoice['inv_no'] , 'title' => $getInvoice['invoice_title'] , 'due_date' => $getInvoice['due_date'] , 'company_name' => $getCompanyDetails['company_name'] , 'company_address' => $getCompanyDetails['city'] , 'company_country' => $getCompanyDetails['country'] , 'company_state' => $getCompanyDetails['state'] , 'company_post_code' => $getCompanyDetails['post_code'] , 'contact_name' => $getContactDetails['first_name']." ".$getContactDetails['last_name'] , 'contact_address' => $getContactAddressDetails['street_address'] , 'contact_country' => $getContactAddressDetails['country'] , 'contact_state' => $getContactAddressDetails['state'] , 'contact_post_code' => $getContactAddressDetails['post_code'] , 'items_array' => $getInvoiceItems , 'total' => $getInvoiceItemTotal , 'discount' => $getInvoiceMeta['discount'] , 'tax' => $getInvoiceMeta['tax'] , 'adjustment' => $getInvoiceMeta['adjustment'] , 'inv_total' => $inv_total , 'logo' => $this->url->to('/')."/uploads/company-logo/".$getCompanyDetails['logo'] , 'terms_and_conditions' => $getTermsAndConditions['meta_value'] , 'pdf_link' => $this->url->to('/')."/show-invoice-pdf/".$link , 'payment_method' => 'paypal']);
		    	}
		    	if($getDefaultPaymentMethod['meta_value']=="stripe")
		    	{
		    		if(($payment_to_be_proceed*100<0.50 && $getInvoiceItems[0]['currency']=="USD") || ($payment_to_be_proceed*100<0.30 && $getInvoiceItems[0]['currency']=="GBP"))
		    		{
		    			
		    			return view('checkout', ['payment_link' => $this->url->to('/')."/paypal-checkout/".$payment_to_be_proceed."/".$link , 'id' => $getInvoice['inv_no'] , 'title' => $getInvoice['invoice_title'] , 'due_date' => $getInvoice['due_date'] , 'company_name' => $getCompanyDetails['company_name'] , 'company_address' => $getCompanyDetails['city'] , 'company_country' => $getCompanyDetails['country'] , 'company_state' => $getCompanyDetails['state'] , 'company_post_code' => $getCompanyDetails['post_code'] , 'contact_name' => $getContactDetails['first_name']." ".$getContactDetails['last_name'] , 'contact_address' => $getContactAddressDetails['street_address'] , 'contact_country' => $getContactAddressDetails['country'] , 'contact_state' => $getContactAddressDetails['state'] , 'contact_post_code' => $getContactAddressDetails['post_code'] , 'items_array' => $getInvoiceItems , 'total' => $getInvoiceItemTotal , 'discount' => $getInvoiceMeta['discount'] , 'tax' => $getInvoiceMeta['tax'] , 'adjustment' => $getInvoiceMeta['adjustment'] , 'inv_total' => $inv_total , 'logo' => $this->url->to('/')."/uploads/company-logo/".$getCompanyDetails['logo'] , 'terms_and_conditions' => $getTermsAndConditions['meta_value'] , 'pdf_link' => $this->url->to('/')."/show-invoice-pdf/".$link , 'payment_method' => 'paypal']);
		    		}
		    		else
		    		{
		    			return view('checkout', ['payment_link' => $this->url->to('/')."/paypal-checkout/".$payment_to_be_proceed."/".$link , 'id' => $getInvoice['inv_no'] , 'title' => $getInvoice['invoice_title'] , 'due_date' => $getInvoice['due_date'] , 'company_name' => $getCompanyDetails['company_name'] , 'company_address' => $getCompanyDetails['city'] , 'company_country' => $getCompanyDetails['country'] , 'company_state' => $getCompanyDetails['state'] , 'company_post_code' => $getCompanyDetails['post_code'] , 'contact_name' => $getContactDetails['first_name']." ".$getContactDetails['last_name'] , 'contact_address' => $getContactAddressDetails['street_address'] , 'contact_country' => $getContactAddressDetails['country'] , 'contact_state' => $getContactAddressDetails['state'] , 'contact_post_code' => $getContactAddressDetails['post_code'] , 'items_array' => $getInvoiceItems , 'total' => $getInvoiceItemTotal , 'discount' => $getInvoiceMeta['discount'] , 'tax' => $getInvoiceMeta['tax'] , 'adjustment' => $getInvoiceMeta['adjustment'] , 'inv_total' => $inv_total , 'logo' => $this->url->to('/')."/uploads/company-logo/".$getCompanyDetails['logo'] , 'terms_and_conditions' => $getTermsAndConditions['meta_value'] , 'pdf_link' => $this->url->to('/')."/show-invoice-pdf/".$link , 'payment_method' => 'stripe' , 'publishable_key' => env('STRIPE_KEY') , 'pay' => $payment_to_be_proceed*100 , 'link' => $link]);
		    		}
		    	}
			}
    	}
    	else
    	{
    		$request->session()->forget('checkout');
    		return view('payment-done', ['id' => $getInvoice['inv_no'] , 'title' => $getInvoice['invoice_title'] , 'due_date' => $getInvoice['due_date'] , 'company_name' => $getCompanyDetails['company_name'] , 'company_address' => $getCompanyDetails['city'] , 'company_country' => $getCompanyDetails['country'] , 'company_state' => $getCompanyDetails['state'] , 'company_post_code' => $getCompanyDetails['post_code'] , 'contact_name' => $getContactDetails['first_name']." ".$getContactDetails['last_name'] , 'contact_address' => $getContactAddressDetails['street_address'] , 'contact_country' => $getContactAddressDetails['country'] , 'contact_state' => $getContactAddressDetails['state'] , 'contact_post_code' => $getContactAddressDetails['post_code'] , 'items_array' => $getInvoiceItems , 'total' => $getInvoiceItemTotal , 'discount' => $getInvoiceMeta['discount'] , 'tax' => $getInvoiceMeta['tax'] , 'adjustment' => $getInvoiceMeta['adjustment'] , 'inv_total' => $inv_total , 'logo' => $this->url->to('/')."/uploads/company-logo/".$getCompanyDetails['logo'] , 'terms_and_conditions' => $getTermsAndConditions['meta_value'] , 'pdf_link' => $this->url->to('/')."/show-invoice-pdf/".$link , 'payment_method' => 'paypal']);
    	}
    }

    public function getCheckout($pay,$link)
	{
		$getInvoice=DB::table('invoices')->select('*')->where('link',$link)->first();
		$getInvoice=json_decode(json_encode($getInvoice), true);
	    $payer = PayPal::Payer();
	    $payer->setPaymentMethod('paypal');

	    $amount = PayPal:: Amount();
	    $amount->setCurrency($getInvoice['currency']);
	    $amount->setTotal($pay);

	    $transaction = PayPal::Transaction();
	    $transaction->setAmount($amount);
	    $transaction->setDescription('Buy Premium  Plan on ');

	    $redirectUrls = PayPal:: RedirectUrls();
	    $redirectUrls->setReturnUrl(route('getDone' , ['pay' => $pay , 'link' => $link]));
	    $redirectUrls->setCancelUrl(route('getCancel'));

	    $payment = PayPal::Payment();
	    $payment->setIntent('sale');
	    $payment->setPayer($payer);
	    $payment->setRedirectUrls($redirectUrls);
	    $payment->setTransactions(array($transaction));

	    try {
		    $response = $payment->create($this->_apiContext);
		    $redirectUrl = $response->links[1]->href;
		} catch (PayPal\Exception\PayPalConnectionException $ex) {
		    echo $ex->getCode(); // Prints the Error Code
		    echo $ex->getData(); // Prints the detailed error message 
		    die($ex);
		} catch (Exception $ex) {
		    die($ex);
		}

	    return redirect()->to( $redirectUrl );
	}

	public function getDone(Request $request,$pay,$link)
	{
		//GET INVOICE DETAILS FROM LINK
		$getInvoice=DB::table('invoices')->select('*')->where('link',$link)->first();
    	$getInvoice=json_decode(json_encode($getInvoice), true);
		$response=$this->changePaymenStatus($pay,$link);

	    $id = $request->get('paymentId');
	    $token = $request->get('token');
	    $payer_id = $request->get('PayerID');

	    $addPaymentMethod=DB::table('invoice_meta')->where('inv_id',$getInvoice['id'])->update(['payment_method' => 'paypal', 'payment_id' => $id]);

	    $payment = PayPal::getById($id, $this->_apiContext);

	    $paymentExecution = PayPal::PaymentExecution();

	    $paymentExecution->setPayerId($payer_id);
	    $executePayment = $payment->execute($paymentExecution, $this->_apiContext);

	    session(['checkout' => 'done','pay' => $pay]);

	    return redirect('invoice/payment/'.$link);
	}

	public function getCancel()
	{
	    return redirect()->route('payPremium');
	}
}
