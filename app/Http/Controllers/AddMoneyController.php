<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
class AddMoneyController extends HomeController
{
    
    // public function __construct()
    // {
    //     parent::__construct();
    //     $this->user = new User;
    // }
    
    /**
     * Show the application paywith stripe.
     *
     * @return \Illuminate\Http\Response
     */
    public function payWithStripe()
    {
        return view('paywithstripe');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postPaymentWithStripe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'card_no' => 'required',
            'ccExpiryMonth' => 'required',
            'ccExpiryYear' => 'required',
            'cvvNumber' => 'required',
            'amount' => 'required',
        ]);
        
        $input = $request->all();
        echo "<pre>";
        print_r($input);
        echo "</pre>";
        // if ($validator->passes()) {           
            $input = array_except($input,array('_token'));            
            $stripe = Stripe::make('sk_test_SSxnUNPNzmEEw2zFPOP6vCMY');
            try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => $request->get('card_no'),
                        'exp_month' => $request->get('ccExpiryMonth'),
                        'exp_year'  => $request->get('ccExpiryYear'),
                        'cvc'       => $request->get('cvvNumber'),
                    ],
                ]);
                if (!isset($token['id'])) {
                    \Session::put('error','The Stripe Token was not generated correctly');
                    echo "The Stripe Token was not generated correctly";
                    // return redirect()->route('addmoney.paywithstripe');
                }
                $charge = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => 'USD',
                    'amount'   => '300',
                    'description' => 'Add in wallet',
                ]);
                if($charge['status'] == 'succeeded') {
                    /**
                    * Write Here Your Database insert logic.
                    */
                    echo "<pre>";
                    print_r($charge);
                    echo "</pre>";
                    // \Session::put('success','Money add successfully in wallet');
                    // return redirect()->route('addmoney/paywithstripe');
                } else {
                    \Session::put('error','Money not add in wallet!!');
                    echo "Money not add in wallet!!";
                    // return redirect()->route('addmoney.paywithstripe');
                }
            } catch (Exception $e) {
                \Session::put('error',$e->getMessage());
                echo "1st error";
                echo "<pre>";
                print_r($e->getMessage());
                echo "</pre>";
                // return redirect()->route('addmoney/paywithstripe');
            } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
                \Session::put('error',$e->getMessage());
                echo "2nd error";
                echo "<pre>";
                print_r($e->getMessage());
                echo "</pre>";
                // return redirect()->route('addmoney/paywithstripe');
            } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                \Session::put('error',$e->getMessage());
                echo "3rd";
                echo "<pre>";
                print_r($e->getMessage());
                echo "</pre>";
                // return redirect()->route('addmoney/paywithstripe');
            }
        // }
        // else
        // {
        //     \Session::put('error','All fields are required!!');
        //     echo "All fields are required!!";
        //     // return redirect()->route('addmoney.paywithstripe');
        // }
    }    
}