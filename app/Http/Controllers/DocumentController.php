<?php

namespace App\Http\Controllers;

use Request;
use View;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use Carbon;
use File;
use App\Feed;
// use Auth;

class DocumentController extends Controller
{
    //
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->middleware('auth');
    }

    public function index()
    {
    	if (Input::hasFile('document-upload')) //IF DOCUMENT UPLOAD INPUT FIELD HAS FILE
	    {
	       $re = Input::file('document-upload')->getClientOriginalName();
	       $destinationPath = base_path('uploads/document'); // upload path
           $file_name=Input::file('document-upload')->getClientOriginalName();
           $file_name = pathinfo($file_name, PATHINFO_FILENAME);
           $extension = Input::file('document-upload')->getClientOriginalExtension(); // getting image extension
           $fileName = $file_name.'-'.rand(11111,99999).'.'.$extension; // renameing image
           Input::file('document-upload')->move($destinationPath, $fileName); // uploading file to given path
           $user_id=Auth::user()->id;
           $contact_id=(Input::get('contact_id')!="" ? Input::get('contact_id') : 0); //GET CONTACT ID IF NULL THEN 0
           $deal_id=(Input::get('deal_id')!="" ? Input::get('deal_id') : 0);  //GET DEAL ID IF NULL THEN 0
           $feed_type=Input::get('type');
           $feed_table=new Feed;  //CREATE FEED CLASS OBJECT
           $feed_title="created document ".$fileName;
           $createFeed=$feed_table->createFeed($feed_title,$user_id,$contact_id,$deal_id,$feed_type); //CALL CREATE FEED METHOD
           $documentId=DB::table('documents')->insertGetId(['user_id' => $user_id, 'document_path' => 'uploads/document/', 'file_type' => $extension, 'file_name' => $fileName, 'attachment_for' => 'document', 'contact_id' => $contact_id, 'deal_id' => $deal_id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
           $message="file_uploaded";
	    }
	    else{
	        $re = "file not present";
	        $message="file_can_not_be_uploaded";
	    }
    	return response()->json(['response' => $fileName , 'deal_id' => $documentId , 'file_type' => $extension]);
    }

    public function getDocuement()
    {
    	$user_id=Auth::user()->id;
    	$getDocuement=DB::table('documents')->select('*')->where('user_id',$user_id)->get(); //GET USER DOCUMENT
    	$base_url=$this->url->to('/');
    	$content=file_get_contents($base_url.'/resources/views/templates/document-li.php');  //GET DOCUMENT LI TEMPLATE
    	return response()->json(['response' => $getDocuement,'status' => 'success','content' => $content]);
    }

    public function getDocumentByContactId()
    {
      $user_id=Auth::user()->id;  //GET CURRENT USER ID
      $contact_id=Input::get('contact_id'); //GET INPUT CONTACT ID
      $getDocuement=DB::table('documents')->select('*')->where('user_id',$user_id)->where('contact_id',$contact_id)->where('attachment_for','document')->orderBy('created_at','desc')->get(); //GET DOCUMENTS DEPENEDS ON USER ID AND CONTACT ID
      $base_url=$this->url->to('/');
      $content=file_get_contents($base_url.'/resources/views/templates/document-li.php'); //GET DOCUMENT LI TEMPLATE
      return response()->json(['response' => $getDocuement,'status' => 'success','content' => $content]);
    }

    public function deleteDocument()
    {
      $user_id=Auth::user()->id;
      $document_id=Input::get('data_id'); //GET INPUT DEAL ID
      $deleteDocument=DB::table('documents')->where('id',$document_id)->delete(); //DELETE DOCUMENT
      return response()->json(['response' => $deleteDocument]);
    }

    public function showDealDocument()
    {
      $getDealDocument=DB::table('documents')->select('*')->where('deal_id',Input::get('deal_id'))->orderBy('created_at','desc')->get();
      $getDealDocument=json_decode(json_encode($getDealDocument), true);
      $base_url=$this->url->to('/');
      $content=file_get_contents($base_url.'/resources/views/templates/document-li.php');
      return response()->json(['status' => 'success' , 'response' => $getDealDocument , 'content' => $content]);
    }
}
