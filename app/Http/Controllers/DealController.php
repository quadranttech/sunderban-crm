<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Routing\UrlGenerator;
use App\Contacts;
use App\Lead;
use App\Contact_address;
use App\Improfiles;
use App\Social_profile;
use App\LeadStage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use View;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use DB;
use App\Quotation;
use Auth;
use App\Feed;
use Jenssegers\Agent\Agent;
use App\Notification;
use DateTime;
use App\Traits\PermissionTrait;
use App\Traits\MainTrait;
use Lang;
use App\Product;

class DealController extends Controller
{
    //
    use MainTrait;
    use PermissionTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->middleware('auth');
        $this->productTable = new Product();
    }

    public function index()
    {
        $method = Request::method();    //GET REQUEST METHOD POST OR GET
    	$getStages=DB::table('stages')->select('*')->where('visiblity','active')->get();    //GET STAGES
    	$getStages=json_decode(json_encode($getStages), true);
        if($method=="GET")
        {
            $getDeals=DB::table('deals')->select('*')->orderBy('created_at', 'desc')->get();   //IF REQUEST METHOD IS GET THEN GET ALL DEALS
        }
        else
        {
            $getDeals=DB::table('deals')->select('*')->where('contact',Input::get('contact_id'))->orderBy('created_at', 'desc')->get();    //ELSE GET CONTACT DEAL
        }
    	$getDeals=json_decode(json_encode($getDeals), true);
    	$getContacts=DB::table('contacts')->select('*')->get();    //GET ALL CONTACTS
    	$getContacts=json_decode(json_encode($getContacts), true);
    	$getCompany=DB::table('company')->select('*')->get();  //GET ALL COMPANY
    	$getCompany=json_decode(json_encode($getCompany), true);
    	$getStatus=DB::table('status')->select('*')->get();    //GET STATUS
    	$getStatus=json_decode(json_encode($getStatus), true);
    	$getCurrentUser=Auth::getUser()->id;
    	$getUsers=DB::table('users')->select('*')->get();  //GET USERS
    	$getUsers=json_decode(json_encode($getUsers), true);
        $getCurrency=DB::table('currency')->join('users', 'users.company', '=', 'currency.company_id')->select('*')->where('users.id', '=', $getCurrentUser)->first();
        $getCurrency=json_decode(json_encode($getCurrency), true);
        $getCurrency=unserialize($getCurrency['currency_list']);
        $deals=array();
        $total_value_per_stage=array();
        $stages_id_array=array();
        $status_per_deal=array();
        $currency_array=array();
    	foreach($getStages as $stages_key=>$stages_value)
    	{
            $stages_value['stage']=array();
            $temp_value=0;
            foreach($getDeals as $deals_key=>$deals_value)
            {
                if($stages_value['id']==$deals_value['stage'])
                {
                    array_push($stages_value['stage'],$deals_value);
                    $temp_value=$temp_value+$deals_value['value'];

                    foreach($getStatus as $status_key=>$status_value)
                    {
                        if($deals_value['status']==$status_value['id'])
                        {
                            $status_per_deal[$deals_value['id']]=$status_value['status'];
                        }
                    }
                }
            }
            
            array_push($deals,$stages_value['stage']);
            array_push($total_value_per_stage,$temp_value);
            array_push($stages_id_array,$stages_value['id']);
    	}
        foreach($getStages as $stages_key=>$stages_value)
        {
            $temp_currency=array();
            foreach($getCurrency as $currency_key=>$currency_value)
            {
                $temp_c=0;
                foreach($getDeals as $deals_key=>$deals_value)
                {
                    if($stages_value['id']==$deals_value['stage'] && $currency_value==$deals_value['currency'])
                    {
                        $temp_c=$temp_c+$deals_value['value'];
                    }
                }
                if($temp_c>0)
                {
                    $temp_currency[$currency_value]=$this->getCurrencySymbol('en',$currency_value)."".$temp_c;
                }
            }
            array_push($currency_array,$temp_currency);
        }
        $array_index=array();
        foreach($getStages as $key=>$value)
        {
            array_push($array_index,$value['stage']);
        }
        $deals=array_combine($array_index, $deals);
        $total_value_per_stage=array_combine($array_index,$total_value_per_stage);
        $stages_id_array=array_combine($array_index,$stages_id_array);
        $currency_array=array_combine($array_index,$currency_array);
        $userImage=array();
        foreach($deals as $key=>$value)
        {
            $stage=$key;
            if(!empty($value))
            {
                foreach($value as $inner_key=>$inner_value)
                {
                    
                    
                    $getTaskAssigne=DB::table('task')->join('deals', 'deals.id', '=', 'task.related_deal')->select('*')->where('deals.id', '=', $inner_value['id'])->get();
                    $getTaskAssigne=json_decode(json_encode($getTaskAssigne), true);
                    $assigneUser=array();
                    $tempUserImage=array();
                    foreach($getTaskAssigne as $key=>$value)
                    {
                        if(empty($assigneUser) || !in_array($value['assigned_to'],$assigneUser))
                        {
                            array_push($assigneUser,$value['assigned_to']);
                        }
                    }
                    $tempUserImage=DB::table('user_meta')->select('image')->where('user_id',$inner_value['owner'])->first();
                    $tempUserImage=json_decode(json_encode($tempUserImage), true);
                    if($tempUserImage['image']!="")
                    {
                        $deals[$stage][$inner_key]['image']=$this->url->to('/').'/uploads/profile-image/'.$tempUserImage['image'];
                    }
                    else
                    {
                        $deals[$stage][$inner_key]['image']=$this->url->to('/').'/assets/img/images.jpg';
                    }

                    $userImage[$stage]=$tempUserImage;
                    $deals[$stage][$inner_key]['currency_symbol']=$this->getCurrencySymbol('en',$inner_value['currency']);
                }
            }
            // $deals[$stage]['total_value_per_stage']=$currency_array[$stage];
        }
        //GET USER PROFILE IMAGE
        $get_user_profile_image=$this->get_profile_image();
        $getDealSource=array();
        $getDealSource=DB::table('settings')->join('users', 'users.company', '=', 'settings.company_id')->where('meta_key','deal_source')->select('*')->where('users.id', '=', $getCurrentUser)->first();
        $getDealSource=json_decode(json_encode($getDealSource), true);
        if(!empty($getDealSource['meta_value']))
        {
            $getDealSource=unserialize($getDealSource['meta_value']);
        }
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        //GET PRODUCT
        $getProduct = $this->productTable->getProductList();
        if($method=="GET")
        {
            $notification_table=new Notification;
            $getNotification=$notification_table->getNotification(Auth::getUser()->id);
            $getNotification=json_decode(json_encode($getNotification), true);
            $getNotificationDifference=array();
            if(!empty($getNotification))
            {
                $getNotificationDifference=$this->getNotificationDifference($getNotification);
            }
            if($getUserPermission['deal']['view']=="off")
            {
                return redirect('/dashboard');
            }
            $agent = new Agent();
            if($agent->isMobile())
            {
                return view('dashboard/deal/deals-mobile',array('stages' => $getStages , 'deals' => $deals , 'contacts' => $getContacts , 'company' => $getCompany , 'status' => $getStatus ,'current_user' => $getCurrentUser , 'users' => $getUsers , 'total_value_per_stage' => $total_value_per_stage , 'stages_id_array' => $stages_id_array , 'status_per_deal' => $status_per_deal , 'userImage' => $userImage , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'deal_source' => $getDealSource , 'active' => 'deal' , 'show_tutorial' => $checkShowTutorial , 'user_agent' => 'mobile' , 'permission' => $getUserPermission , 'currency' => $getCurrency , 'currency_array' => $currency_array , 'profile_image' => $get_user_profile_image , 'product' => $getProduct));
            }
            return view('dashboard/deal/deals',array('stages' => $getStages , 'deals' => $deals , 'contacts' => $getContacts , 'company' => $getCompany , 'status' => $getStatus ,'current_user' => $getCurrentUser , 'users' => $getUsers , 'total_value_per_stage' => $total_value_per_stage , 'stages_id_array' => $stages_id_array , 'status_per_deal' => $status_per_deal , 'userImage' => $userImage , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'deal_source' => $getDealSource , 'active' => 'deal' , 'show_tutorial' => $checkShowTutorial , 'user_agent' => '' , 'permission' => $getUserPermission , 'currency' => $getCurrency , 'currency_array' => $currency_array , 'profile_image' => $get_user_profile_image , 'product' => $getProduct));
        }
        else
        {
            $base_url=$this->url->to('/');
            if(Input::get('type')=="grid")
            {
                $content=file_get_contents($base_url.'/resources/views/templates/deal-li.php');
            }
            else
            {
                $content=file_get_contents($base_url.'/resources/views/templates/deal-card-li.php');
            }
            return response()->json(['response' => 'success','stages' => $getStages,'deals' => $deals , 'contacts' => $getContacts , 'company' => $getCompany , 'status' => $getStatus ,'current_user' => $getCurrentUser , 'users' => $getUsers , 'total_value_per_stage' => $total_value_per_stage , 'stages_id_array' => $stages_id_array , 'status_per_deal' => $status_per_deal , 'userImage' => $userImage, 'active' => 'deal' , 'currency' => $getCurrency , 'currency_array' => $currency_array , 'permission' => $getUserPermission , 'product' => $getProduct]);
        }
    	
    }

    public function create(Request $request)
    {
    	$title=(Request::input('title') != '' ? Request::input('title') : 'untitled'); //GET ALL INPUT
    	$contact=(Request::input('contact') != '' ? Request::input('contact') : '0');
    	$company=(Request::input('company') != '' ? Request::input('company') : '0');
    	$owner=(Request::input('owner') != '' ? Request::input('owner') : '0');
    	$status=(Request::input('status') != '' ? Request::input('status') : '0');
    	$source=(Request::input('source') != '' ? Request::input('source') : 'no_source');
    	$stage=(Request::input('stage') != '' ? Request::input('stage') : '0');
    	$priority=Request::input('priority');
    	$value=(Request::input('value') != '' ? Request::input('value') : '0');
        $currency=(Request::input('currency') != '' ? Request::input('currency') : 'USD');
    	$win_probability=(Request::input('win_probability') != '' ? Request::input('win_probability') : '0');
    	$description=(Request::input('description') != '' ? Request::input('description') : 'no_description');
    	$created_by=Request::input('created_by');
        $related_to=Request::input('company');
        $purpose=Request::input('purpose');
        $product_id=Request::input('product_id');
        $product_id=serialize($product_id);
        $deal_start=Request::input('deal_start');
        $deal_close=Request::input('deal_close');
        $getCurrentUser=DB::table('users')->select('*')->where('id',Auth::user()->id)->first();
        $getCurrentUser=json_decode(json_encode($getCurrentUser), true);
        $feed_table=new Feed;   //CREATE FEED CLASS OBJECT
        $created=Lang::get('ajax-lang.created');
        $by=Lang::get('ajax-lang.by');
        $feed_title=$created." deal ".$title.$by." ".$getCurrentUser['first_name']." ".$getCurrentUser['last_name'];
		$createDeal=DB::table('deals')->insertGetId(['title' => $title , 'contact' => $contact , 'related_to' => $related_to , 'related_product' => $product_id , 'deal_start' => $deal_start , 'deal_end' => $deal_close , 'owner' => $owner , 'status' => $status , 'source' => $source , 'stage' => $stage , 'priority' => $priority , 'value' => $value , 'currency' => $currency , 'win_probability' => $win_probability , 'description' => $description , 'created_by' => $created_by , 'created_date' => date('Y-m-d'), 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s')]);  //CREATE DEAL
        $createFeed=$feed_table->createFeed($feed_title,$created_by,$contact,$createDeal,'deal');
		if($createDeal)
		{
			$base_url=$this->url->to('/');
            $getStageTotalValuePerCurrency=$this->getStageTotalValuePerCurrency($stage,$currency,$contact,$purpose);
            $stage=DB::table('stages')->select('*')->where('id',$stage)->first();
            $stage=$stage->stage;
            $status=DB::table('status')->select('slug')->where('id',$status)->first();
            $status=$status->slug;
			$content=file_get_contents($base_url.'/resources/views/templates/new-deal.php');
            $image=DB::table('users')->select('*')->where('id',$owner)->first();
            $image=json_decode(json_encode($image), true);
            $checkSmtp=$this->checkSmtp();
            if($checkSmtp!="ok")
            {
                return response()->json(['response' => 'success','content' => $content,'id' => $createDeal,'stage' => $stage,'status' => $status,'value' => $value,'title' => $title,'image' => $image,'stageTotalValuePerCurrency' => $getStageTotalValuePerCurrency,'message' => 'smtp_error']);
            }
            else
            {
                $sendDealMail=$this->sendDealMail($created_by,$owner,$title,$createDeal);
                return response()->json(['response' => 'success','content' => $content,'id' => $createDeal,'stage' => $stage,'status' => $status,'value' => $value,'title' => $title,'stageTotalValuePerCurrency' => $getStageTotalValuePerCurrency,'image' => $image]);
            }
		}
		else
		{
			return response()->json(['response' => 'failed']);
		}
    }

    public function getStageTotalValuePerCurrency($stage,$currency,$contact,$purpose)
    {
        if($purpose!='contexual')
        {
            $cond="if";
            $getStageTotalValuePerCurrency=DB::table('deals')->where('stage','=',$stage)->where('currency','=',$currency)->sum('value');
        }
        if($purpose=='contexual')
        {
            $cond="elseif";
            $getStageTotalValuePerCurrency=DB::table('deals')->where('stage','=',$stage)->where('currency','=',$currency)->where('contact','=',$contact)->sum('value');
        }
        $getStageTotalValuePerCurrency=json_decode(json_encode($getStageTotalValuePerCurrency), true);
        $deal_value=$getStageTotalValuePerCurrency;
        $currency_symbol=$this->getCurrencySymbol('en',$currency);
        $getStageTotalValuePerCurrency=$currency_symbol.$getStageTotalValuePerCurrency;
        $result=array('currency' => $currency , 'stage' => $stage , 'cond' => $cond , 'currency_symbol' => $currency_symbol , 'deal_value' => $deal_value , 'value' => $getStageTotalValuePerCurrency);
        return $result;
    }

    public function getChangedStageTotalValuePerCurrency($stage,$currency,$contact,$purpose)
    {
        if($contact=='0')
        {
            $cond="if";
            $getStageTotalValuePerCurrency=DB::table('deals')->where('stage','=',$stage)->where('currency','=',$currency)->sum('value');
        }
        if($contact!='0')
        {
            $cond="elseif";
            $getStageTotalValuePerCurrency=DB::table('deals')->where('stage','=',$stage)->where('currency','=',$currency)->where('contact','=',$contact)->sum('value');
        }
        $getStageTotalValuePerCurrency=json_decode(json_encode($getStageTotalValuePerCurrency), true);
        $deal_value=$getStageTotalValuePerCurrency;
        $currency_symbol=$this->getCurrencySymbol('en',$currency);
        $getStageTotalValuePerCurrency=$currency_symbol.$getStageTotalValuePerCurrency;
        $result=array('currency' => $currency , 'stage' => $stage , 'cond' => $cond , 'currency_symbol' => $currency_symbol , 'deal_value' => $deal_value , 'value' => $getStageTotalValuePerCurrency);
        return $result;
    }

    public function changeDealStage()
    {
        $stage_id=Input::get('stage_id');   //GET INPUT STAGE ID
        $deal_id=Input::get('deal_id'); //GET INPUT DEAL ID
        $contact_id=Input::get('contact_id');   //GET INPUT CONTACT ID
        $changeDealStage=DB::table('deals')->where('id',$deal_id)->update(['stage' => $stage_id]);  //UPDATE DEAL BY STAGE ID
        $getDeal=DB::table('deals')->where('id',$deal_id)->where('stage',$stage_id)->first();
        $getDeal=json_decode(json_encode($getDeal), true);
        $stage=DB::table('stages')->select('slug')->where('id',$stage_id)->first();
        $stage=$stage->slug;
        $getStageTotalValuePerCurrency=$this->getChangedStageTotalValuePerCurrency($stage_id,$getDeal['currency'],$contact_id,'');
        $getBeforeStageTotalValueCurrency=$this->getChangedStageTotalValuePerCurrency(Input::get('stage_id_before_move'),$getDeal['currency'],$contact_id,'');
        return response()->json(['response' => $changeDealStage , 'stageTotalValuePerCurrency' => $getStageTotalValuePerCurrency , 'getBeforeStageTotalValueCurrency' => $getBeforeStageTotalValueCurrency , 'currency' => $getDeal['currency'] , 'contact' => $contact_id]);
    }

    public function getDeal()
    {
        $user_id=Auth::user()->id;
        $contact_id=Input::get('contact_id');   //GET INPUT CONTACT ID
        $getDeal=DB::table('deals')->select('*')->get();    //GET DEALS
        $getDeal=json_decode(json_encode($getDeal), true);
        $base_url=$this->url->to('/');
        $content=file_get_contents($base_url.'/resources/views/templates/deal-li.php'); //GET DEAL LI TEMPLATE
        return response()->json(['response' => 'success' , 'deal' => $getDeal , 'content' => $content]);
    }

    public function dealContexual()
    {
        $getDealDetails=DB::table('deals')->select('*')->where('id',Request::input('deal_id'))->first();    //GET DEALS
        $getDealDetails=json_decode(json_encode($getDealDetails), true);
        $getContact=DB::table('contacts')->select('*')->get();  //GET CONTACTS
        $getContact=json_decode(json_encode($getContact), true);
        $getCompany=DB::table('company')->select('*')->get();   //GET COMPANY
        $getCompany=json_decode(json_encode($getCompany), true);
        $getStatus=DB::table('status')->select('*')->get(); //GET STATUS
        $getStatus=json_decode(json_encode($getStatus), true);
        $getStages=DB::table('stages')->select('*')->get(); //GET STAGES
        $getStages=json_decode(json_encode($getStages), true);
        $getUsers=DB::table('users')->select('*')->get(); //GET USERS
        $getUsers=json_decode(json_encode($getUsers), true);
        $getRelatedProduct = $this->productTable->getProductRelatedDeal(Request::input('deal_id') , 'productData');    //GET RELATED PRODUCT
        $getProduct = $this->productTable->getProductList();    //GET PRODUCT
        $room_array = array();
        $add_on_array = array();
        $special_food = "false";
        $foreign_guest = "false";
        $diff = date_diff(date_create($getDealDetails['deal_start']),date_create($getDealDetails['deal_end']));
        $day = $diff->format("%a");
        if($day == 0)
        {
            $day = 1;
        }
        $person = $getDealDetails['person'];
        $package = $getDealDetails['package'];
        if(!empty($getRelatedProduct))
        {
            foreach($getRelatedProduct as $key=>$value)
            {
                if($value['type'] == "main")
                {
                    array_push($room_array,$value);
                }
                else
                {
                    if (strpos($value['name'], 'food') !== false || strpos($value['description'], 'food') !== false)
                    {
                        $special_food = "true";
                    }
                    if (strpos($value['name'], 'Foreign') !== false)
                    {
                        $foreign_guest = "true";
                    }
                    array_push($add_on_array,$value);
                }
            }
        }
        $getContactCompany=DB::table('company')->join('contacts', 'contacts.company', '=', 'company.id')->select('*')->where('contacts.id', '=', $getDealDetails['id'])->first();
        $getContactCompany=json_decode(json_encode($getContactCompany), true);
        $notification_table=new Notification;   //CREATE NOTIFICATION CLASS OBJECT
        $getNotification=$notification_table->getNotification(Auth::getUser()->id); //CALL GETNOTIFICATION METHOD
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);
        }
        //GET USER PROFILE IMAGE
        $get_user_profile_image=$this->get_profile_image();
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);   //GET USER PERMISSION
        if($getUserPermission['deal']['edit']=="off")
        {
            return redirect('/dashboard');
        }
        $agent = new Agent();
        if($agent->isMobile())
        {
            return view('dashboard/deal/deal-contextual-mobile', ['deal' => $getDealDetails], ['contact' => $getContact , 'company' => $getCompany ,    'contact_company' => $getContactCompany , 'status' => $getStatus , 'stages' => $getStages , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'deal' , 'show_tutorial' => $checkShowTutorial , 'users' => $getUsers , 'related_product' => $getRelatedProduct , 'product' => $getProduct , 'room_array' => $room_array , 'add_on_array' => $add_on_array , 'special_food' => $special_food , 'foreign_guest' => $foreign_guest , 'day' => $day , 'person' => $person , 'package' => $package , 'permission' => $getUserPermission]);
        }
        return view('dashboard/deal/deal-contextual', ['deal' => $getDealDetails], ['contact' => $getContact , 'company' => $getCompany ,   'contact_company' => $getContactCompany , 'status' => $getStatus , 'stages' => $getStages , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'deal' , 'show_tutorial' => $checkShowTutorial , 'users' => $getUsers , 'related_product' => $getRelatedProduct , 'product' => $getProduct , 'room_array' => $room_array , 'add_on_array' => $add_on_array , 'special_food' => $special_food , 'foreign_guest' => $foreign_guest , 'day' => $day , 'person' => $person , 'package' => $package , 'permission' => $getUserPermission]);
    }

    public function updateDeal()
    {
        $updateDeal=DB::table('deals')->where('id',Input::get('deal_id'))->update(['title' => Input::get('deal_title'), 'description' => Input::get('deal_description'), 'owner' => Input::get('deal-owner'), 'related_to' => Input::get('company'), 'priority' => Input::get('deal_priority'), 'value' => Input::get('deal_value')]);
        return response()->json(['response' => $updateDeal]);
    }

    public function getDealByContactId()
    {
        $getDeal=DB::table('deals')->select('*')->where('contact',Input::get('contact_id'))->get();
        $getDeal=json_decode(json_encode($getDeal), true);
        return response()->json(['response' => $getDeal]);
    }

    public function editDeal()
    {
        $editDeal=DB::table('deals')->where('id',Input::get('deal_id'))->update(['status' => Input::get('status'), 'stage' => Input::get('stage'), 'source' => Input::get('source'), 'priority' => Input::get('priority'), 'value' => Input::get('value'), 'win_probability' => Input::get('win_probability'), 'description' => Input::get('description')]);
        return response()->json(['response' => $editDeal]);
    }

    public function getNotificationDifference($getNotification)
    {
        date_default_timezone_set(env('TIME_ZONE'));
        $notification_difference_array=array();
        foreach($getNotification as $key=>$value)
        {
            $datetime1 = new DateTime($getNotification[$key]['created_at']);
            $datetime2 = new DateTime(date("Y-m-d h:i:s a"));
            $interval = $datetime1->diff($datetime2);
            if($interval->format('%h')>0)
            {
                $interval = $interval->format('%h')." hr ".$interval->format('%i')." min ago";
            }
            else
            {
                $interval = $interval->format('%i')." min ago";
            }
            array_push($notification_difference_array,$interval);
        }
        return $notification_difference_array;
    }

    public function checkBooking()
    {
        // $date_string = Input::get('date_string');
        return response()->json(['response' => 'success']);
    }

    public function updateDealProduct()
    {
        $editDealProduct = DB::table('deal_items')->where('product_id',Input::get('product_id'))->where('deal_id',Input::get('deal_id'))->update(['deal_items_description' => Input::get('item_description') , 'product_price' => Input::get('item_price')]);
        return response()->json(['response' => 'success']);
    }
}
