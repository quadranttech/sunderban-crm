<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Traits\MainTrait;
use Illuminate\Support\Facades\Input;
use App\Feed;
use Jenssegers\Agent\Agent;
use Session;
use Illuminate\Routing\UrlGenerator;
use App\Notification;
use DateTime;
use App\Traits\PermissionTrait;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use MainTrait;
    use PermissionTrait;

    protected $redirectTo = '/dashboard';

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/dashboard');
    }

    public function dashboard(Request $request)
    {
        //COMPANY DASHBOARD
        $getProposalStage=DB::table('stages')->select('*')->where('slug','proposal')->first();
        $getWonStage=DB::table('status')->select('*')->where('slug','won')->first();
        $getLostStage=DB::table('status')->select('*')->where('slug','lost')->first();
        $getCurrentDate=date("Y-m-d");
        $previousMonth=date("m", strtotime(date('F') . " last month"));
        $firstDate=date('Y-m-01');
        // echo "first = ".$firstDate;
        // echo "</br>";
        // echo "current = ".$getCurrentDate;
        $getCurrency=DB::table('currency')->join('users', 'users.company', '=', 'currency.company_id')->select('*')->where('users.id', '=', Auth::getUser()->id)->first();
        $getCurrency=json_decode(json_encode($getCurrency), true);
        $getCurrency=unserialize($getCurrency['currency_list']);
        $getCurrencySymbol=array();
        if(!empty($getCurrency))
        {
            foreach($getCurrency as $key=>$value)
            {
                $getCurrencySymbol[$value]=$this->getCurrencySymbol('en',$value);
            }
        }
        $getProposalDealValue=DB::table('deals')->select('*')->where('created_date','>=',$firstDate)->where('created_date','<=',$getCurrentDate)->where('stage',$getProposalStage->id)->get();
        $getProposalDealValue=json_decode(json_encode($getProposalDealValue), true);
        $company_proposal_value=array();
        foreach($getCurrency as $key=>$value)
        {
            $count=0;
            foreach($getProposalDealValue as $inner_key=>$inner_value)
            {
                if($inner_value['currency']==$value)
                {
                    $count=$count+$inner_value['value'];
                }
            }
            $company_proposal_value[$value]=$count;
        }
        $getWonDealValue=DB::table('deals')->select('*')->where('created_date','>=',$firstDate)->where('created_date','<=',$getCurrentDate)->where('stage',$getWonStage->id)->get();
        $getWonDealValue=json_decode(json_encode($getWonDealValue), true);
        $wonDealValue=array();
        foreach($getCurrency as $key=>$value)
        {
            $count=0;
            foreach($getWonDealValue as $inner_key=>$inner_value)
            {
                if($inner_value['currency']==$value)
                {
                    $count=$count+$inner_value['value'];
                }
            }
            $wonDealValue[$value]=$count;
        }
        $getWonDealValueByUser=DB::table('deals')->select('*')->where('created_date','>=',$firstDate)->where('created_date','<=',$getCurrentDate)->where('stage',$getWonStage->id)->where('owner',Auth::getUser()->id)->get();
        $getWonDealValueByUser=json_decode(json_encode($getWonDealValueByUser), true);
        $wonDealValueByUser=array();
        foreach($getCurrency as $key=>$value)
        {
            $count=0;
            foreach($getWonDealValue as $inner_key=>$inner_value)
            {
                if($inner_value['currency']==$value)
                {
                    $count=$count+$inner_value['value'];
                }
            }
            $wonDealValueByUser[$value]=$count;
        }
        $getDealCreatedByUser=DB::table('deals')->select('*')->where('created_date','>=',$firstDate)->where('created_date','<=',$getCurrentDate)->where('owner',Auth::getUser()->id)->get();
        $getDealCreatedByUser=json_decode(json_encode($getDealCreatedByUser), true);
        $dealCreateByUser=array();
        foreach($getCurrency as $key=>$value)
        {
            $count=0;
            foreach($getDealCreatedByUser as $inner_key=>$inner_value)
            {
                if($inner_value['currency']==$value)
                {
                    $count=$count+$inner_value['value'];
                }
            }
            $dealCreateByUser[$value]=$count;
        }
        //GET USER DUE TASK
        $getUserDueTask=DB::table('task')->where('assigned_to',Auth::getUser()->id)->whereDate('due_date','>=',$firstDate)->whereDate('due_date','<=',$getCurrentDate)->where('marked_as','due')->get();
        $getUserDueTask=json_decode(json_encode($getUserDueTask), true);
        $getUserDueTask=sizeof($getUserDueTask);
        //GET DUE INVOICE
        $getDueInvoiceValue=DB::table('invoices')->select('*')->whereDate('created_at','>=',$firstDate)->whereDate('created_at','<=',$getCurrentDate)->where('status','send')->where('payment_status','unpaid')->orWhere('payment_status','partly_paid')->get();
        $getDueInvoiceValue=json_decode(json_encode($getDueInvoiceValue), true);
        $dueInvoiceValue=array();
        foreach($getCurrency as $key=>$value)
        {
            $count=0.00;
            foreach($getDueInvoiceValue as $inner_key=>$inner_value)
            {
                if($inner_value['currency']==$value)
                {
                    $count=$count+$inner_value['due'];
                }
            }
            $dueInvoiceValue[$value]=$count;
        }
        $getPaidInvoiceValue=DB::table('invoices')->select('*')->whereDate('created_at','>=',$firstDate)->whereDate('created_at','<=',$getCurrentDate)->where('payment_status','paid')->get();
        $getPaidInvoiceValue=json_decode(json_encode($getPaidInvoiceValue), true);
        $paidInvoiceValue=array();
        foreach($getCurrency as $key=>$value)
        {
            $count=0.00;
            foreach($getPaidInvoiceValue as $inner_key=>$inner_value)
            {
                if($inner_value['currency']==$value)
                {
                    $count=$count+$inner_value['paid'];
                }
            }
            $paidInvoiceValue[$value]=$count;
        }
        $getSendInvoiceValue=DB::table('invoices')->join('invoice_meta', 'invoice_meta.inv_id', '=', 'invoices.id')->select('*')->whereDate('send_date','>=',$firstDate)->whereDate('send_date','<=',$getCurrentDate)->where('invoices.status', '=', 'send')->get();
        $getSendInvoiceValue=json_decode(json_encode($getSendInvoiceValue), true);
        $sendInvoiceValue=array();
        foreach($getCurrency as $key=>$value)
        {
            $count=0.00;
            foreach($getSendInvoiceValue as $inner_key=>$inner_value)
            {
                if($inner_value['currency']==$value)
                {
                    $count=$count+$inner_value['inv_total'];
                }
            }
            $sendInvoiceValue[$value]=$count;
        }

        $getSendEstimateValue=DB::table('estimates')->join('estimate_meta', 'estimate_meta.estimate_id', '=', 'estimates.id')->select('estimates.*','estimate_meta.*')->whereDate('estimates.send_date','>=',$firstDate)->whereDate('estimates.send_date','<=',$getCurrentDate)->where('estimates.status', '=', 'send')->orWhere('estimates.status', '=', 'accepted')->orWhere('estimates.status', '=', 'declined')->get();
        $getSendEstimateValue=json_decode(json_encode($getSendEstimateValue), true);
        $sendEstimateValue=array();
        foreach($getCurrency as $key=>$value)
        {
            $count=0.00;
            foreach($getSendEstimateValue as $inner_key=>$inner_value)
            {
                if($inner_value['currency']==$value)
                {
                    $count=$count+$inner_value['inv_total'];
                }
            }
            $sendEstimateValue[$value]=$count;
        }
        $getAcceptedEstimateValue=DB::table('estimates')->join('estimate_meta', 'estimate_meta.estimate_id', '=', 'estimates.id')->select('estimates.*','estimate_meta.*')->whereDate('estimate_meta.created_at','>=',$firstDate)->whereDate('estimate_meta.created_at','<=',$getCurrentDate)->where('estimate_meta.status','accepted')->get();
        $getAcceptedEstimateValue=json_decode(json_encode($getAcceptedEstimateValue), true);
        $acceptedEstimateValue=array();
        foreach($getCurrency as $key=>$value)
        {
            $count=0.00;
            foreach($getAcceptedEstimateValue as $inner_key=>$inner_value)
            {
                if($inner_value['currency']==$value)
                {
                    $count=$count+$inner_value['inv_total'];
                }
            }
            $acceptedEstimateValue[$value]=$count;
        }

        $getContactCreatedByUser=DB::table('contacts')->select('*')->whereDate('created_at','>=',$firstDate)->whereDate('created_at','<=',$getCurrentDate)->where('created_by',Auth::getUser()->id)->get();
        $getContactCreatedByUser=json_decode(json_encode($getContactCreatedByUser), true);
        $getContactCreatedByUser=sizeof($getContactCreatedByUser);

        //PERSONAL DASHBOARD
        $getProposalStage=DB::table('stages')->select('*')->where('slug','proposal')->first();
        $getWonStage=DB::table('status')->select('*')->where('slug','won')->first();
        $getLostStage=DB::table('status')->select('*')->where('slug','lost')->first();
        $getCurrentDate=date("Y-m-d");
        $previousMonth=date("m", strtotime(date('F') . " last month"));
        $firstDate=date('Y-m-01');
        $previouMonthFirstDate=date('Y').'-'.$previousMonth.'-'.'01';
        $previousMonthLastDate=date('Y').'-'.$previousMonth.'-'.date('t',strtotime(date('Y').'-'.'04'.'-'.'01'));
        $total_proposal_value=0;
        $total_won_value=0;
        $total_lost_value=0;
        $getProposalDeal=DB::table('deals')->select('*')->where('created_date','>=',$firstDate)->where('created_date','<=',$getCurrentDate)->where('stage',$getProposalStage->id)->get();
        $getProposalDeal=json_decode(json_encode($getProposalDeal), true);
        $getWonDeal=DB::table('deals')->select('*')->where('created_date','>=',$firstDate)->where('created_date','<=',$getCurrentDate)->where('status',$getWonStage->id)->get();
        $getWonDeal=json_decode(json_encode($getWonDeal), true);
        $getLostDeal=DB::table('deals')->select('*')->where('created_date','>=',$firstDate)->where('created_date','<=',$getCurrentDate)->where('status',$getLostStage->id)->get();
        $getLostDeal=json_decode(json_encode($getLostDeal), true);
        $getPreviousMonthProposalDeal=DB::table('deals')->select('*')->where('created_date','>=',$previouMonthFirstDate)->where('created_date','<=',$previousMonthLastDate)->where('stage',$getProposalStage->id)->get();
        $getPreviousMonthProposalDeal=json_decode(json_encode($getPreviousMonthProposalDeal), true);
        $getPreviousMonthWonDeal=DB::table('deals')->select('*')->where('created_date','>=',$previouMonthFirstDate)->where('created_date','<=',$previousMonthLastDate)->where('status',$getWonStage->id)->get();
        $getPreviousMonthWonDeal=json_decode(json_encode($getPreviousMonthWonDeal), true);
        $getPreviousMonthLostDeal=DB::table('deals')->select('*')->where('created_date','>=',$previouMonthFirstDate)->where('created_date','<=',$previousMonthLastDate)->where('status',$getLostStage->id)->get();
        $getPreviousMonthLostDeal=json_decode(json_encode($getPreviousMonthLostDeal), true);
        $getProposalDealPercentage=$this->getDealPercentage($getPreviousMonthProposalDeal,$getProposalDeal);
        $getWonDealPercentage=$this->getDealPercentage($getPreviousMonthWonDeal,$getWonDeal);
        $getLostDealPercentage=$this->getDealPercentage($getPreviousMonthLostDeal,$getLostDeal);

        //GET ROLES
        $getRoles=DB::table('settings')->select('*')->where('meta_key','roles')->first();
        $getRoles=json_decode(json_encode($getRoles), true);
        $getRole=array_search('admin',unserialize($getRoles['meta_value']));
        $getUserRole=DB::table('users')->select('*')->where('id',Auth::getUser()->id)->first();
        $getUserRole=json_decode(json_encode($getUserRole), true);
        if($getUserRole['role']==$getRole)
        {
            $role="admin";
        }
        else
        {
            $role="staff";
        }
        if(!empty($getProposalDeal))
        {
            foreach($getProposalDeal as $key=>$value)
            {
                $total_proposal_value=$total_proposal_value+$value['value'];
            }
        }
        if(!empty($getWonDeal))
        {
            foreach($getWonDeal as $key=>$value)
            {
                $total_won_value=$total_won_value+$value['value'];
            }
        }
        if(!empty($getLostDeal))
        {
            foreach($getLostDeal as $key=>$value)
            {
                $total_lost_value=$total_lost_value+$value['value'];
            }
        }
        $deal_value_of_current_month=$total_proposal_value;
        $deal_won_value_of_current_month=$total_won_value;
        $deal_lost_value_of_current_month=$total_lost_value;
        $deals_proposal_array=array(
            'current_month'=>date('F Y'),
            'currency_symbol'=>$getCurrencySymbol,
            'company_proposal_value'=>$company_proposal_value,
            'company_won_value'=>$wonDealValue,
            'user_won_value'=>$wonDealValueByUser,
            'user_created_deal'=>$dealCreateByUser
            );
        // echo "<pre>";
        // print_r($deals_proposal_array);
        // echo "</pre>";
        // if(!empty($deals_proposal_array['company_proposal_value']))
        // {
        //     echo "not empty";
        // }
        // else
        // {
        //     echo "empty";
        // }
        // die();
        $invoice=array(
            'due'=>$dueInvoiceValue,
            'paid'=>$paidInvoiceValue,
            'send'=>$sendInvoiceValue
            );
        $estimate=array(
            'send'=>$sendEstimateValue,
            'accepted'=>$acceptedEstimateValue
            );
        $user_id=Auth::getUser()->id;
        $getFeeds=DB::table('feeds')->select('*')->where('user_id',$user_id)->limit(5)->get();
        $getFeeds=json_decode(json_encode($getFeeds), true);
        $feed_color_array=['red','blue','light-blue','cyen','green','light-green','teal','light-teal','indigo','purple','lime','orange','brown','blue-grey'];
        $getTasks=DB::table('task')->where('created_by',$user_id)->where('marked_as','due')->limit(5)->orderBy('id','desc')->get();
        $getTasks=json_decode(json_encode($getTasks), true);
        $task_array=$this->getStructuredTaskArray($getTasks);
        $getContacts=DB::table('users')->get();
        $getContacts=json_decode(json_encode($getContacts), true);
        $getUserImage=DB::table('user_meta')->select('*')->where('id',$user_id)->first();
        $getUserImage=json_decode(json_encode($getUserImage), true);
        $getUserImage=$getUserImage['image'];
        if($getUserImage!="")
        {
            $userImage=$this->url->to('/').'/uploads/profile-image/'.$getUserImage;
        }
        else
        {
            $userImage=$this->url->to('/').'/assets/img/avatar.png';
        }
        $get_user_profile_image=$userImage;
        $notification_table=new Notification;
        $getNotification=$notification_table->getNotification(Auth::getUser()->id);
        $getNotification=json_decode(json_encode($getNotification), true);
        date_default_timezone_set(env('TIME_ZONE'));
        $notification_difference_array=array();
        foreach($getNotification as $key=>$value)
        {
            $datetime1 = new DateTime($getNotification[$key]['created_at']);
            $datetime2 = new DateTime(date("Y-m-d h:i:s a"));
            $interval = $datetime1->diff($datetime2);
            if($interval->format('%h')>0)
            {
                $interval = $interval->format('%h')." hr ".$interval->format('%i')." min ago";
            }
            else
            {
                $interval = $interval->format('%i')." min ago";
            }
            array_push($notification_difference_array,$interval);
        }
        if($request->session()->has('users'))
        {
            $show_tutorial=true;
            $request->session()->forget('show_tutorial');
        }
        else
        {
            $show_tutorial=false;
        }
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        $agent = new Agent();
        if($agent->isMobile())
        {
            return view('dashboard/dashboard-mobile', ['deals_proposal_array'=>$deals_proposal_array], ['invoice'=>$invoice , 'estimate'=>$estimate , 'userDueTask' => $getUserDueTask, 'contactCreatedByUser' => $getContactCreatedByUser, 'feeds'=>$getFeeds , 'feed_color_array'=>['red','blue','light-blue','cyen','green','light-green','teal','light-teal','indigo','purple','lime','orange','brown','blue-grey'], 'tasks'=>$task_array, 'contact'=>$getContacts , 'notification' => $getNotification , 'notification_difference' => $notification_difference_array , 'profile_image' => $get_user_profile_image , 'active' => 'dashboard' , 'show_tutorial' => $checkShowTutorial , 'role' => $role , 'permission' => $getUserPermission]);
        }
        return view('dashboard/dashboard', ['deals_proposal_array'=>$deals_proposal_array], ['invoice'=>$invoice , 'estimate'=>$estimate , 'userDueTask' => $getUserDueTask, 'contactCreatedByUser' => $getContactCreatedByUser, 'feeds'=>$getFeeds , 'feed_color_array'=>['red','blue','light-blue','cyen','green','light-green','teal','light-teal','indigo','purple','lime','orange','brown','blue-grey'], 'tasks'=>$task_array, 'contact'=>$getContacts , 'notification' => $getNotification , 'notification_difference' => $notification_difference_array , 'profile_image' => $get_user_profile_image , 'active' => 'dashboard' , 'show_tutorial' => $checkShowTutorial , 'role' => $role , 'permission' => $getUserPermission]);
    }

    public function getDealPercentage($previousMonthDeal,$currentMonthDeal)
    {
        $dealArray=array();
        if(!empty($previousMonthDeal) && !empty($currentMonthDeal))
        {
            $temp1=sizeof($currentMonthDeal)-sizeof($previousMonthDeal);
            $temp2=$temp1/sizeof($previousMonthDeal);
            $getDifference=$temp2*100;
        }
        else if(!empty($previousMonthDeal) && empty($currentMonthDeal))
        {
            $temp1=0-sizeof($previousMonthDeal);
            $temp2=$temp1/sizeof($previousMonthDeal);
            $getDifference=$temp2*100;
        }
        else if(empty($previousMonthDeal) && !empty($currentMonthDeal))
        {
            $getDifference=sizeof($currentMonthDeal)*100;
        }
        else
        {
            $getDifference='';
        }
        return $getDifference;
    }

    public function getDealDifference($previousMonthDeal,$currentMonthDeal)
    {
        if($currentMonthDeal<$previousMonthDeal)
        {
            $dealDifference='-';
        }
        else if($currentMonthDeal==$previousMonthDeal)
        {
            $dealDifference='';
        }
        else
        {
            $dealDifference='+';
        }
    }

    public function getAllDeals()
    {
        $getRecords=DB::table('deals')->select('*')->where( DB::raw('YEAR(created_at)'), '=', '2017' )->get();
        $getRecords=json_decode(json_encode($getRecords), true);
        $getWonStage=DB::table('status')->select('*')->where('slug','won')->first();
        $date_array=array();
        $deal_count_array=array();
        if(!empty($getRecords))
        {
            foreach($getRecords as $key=>$value)
            {
                if(empty($date_array) || !in_array($value['created_date'],$date_array))
                {
                    array_push($date_array,$value['created_date']);
                    $getCount=$this->getCount($getRecords,$value['created_date']);
                    array_push($deal_count_array,$getCount);
                }
            }
            $deal_value_array=$this->getDealValue($getRecords,$getWonStage->id);
            return response()->json(['status' => 'success' , 'date_array' => $date_array , 'deal_array' => $deal_count_array , 'deal_value_array' => $deal_value_array , 'dd' => DB::raw('YEAR(created_at)')]);
        }
        else
        {
            return response()->json(['status' => 'failed']);
        }
    }

    public function getCount($getRecords,$created_date)
    {
        $count=0;
        foreach($getRecords as $key=>$value)
        {
            if($value['created_date']==$created_date)
            {
                $count++;
            }
        }
        return $count;
    }

    public function getDealValue($getRecords,$wonStage)
    {
        $yearArray=array('01','02','03','04','05','06','07','08','09','10','11','12');
        $month=date('m', strtotime('-1 month'))+1;
        if($month<10)
        {
            $month="0".$month;
        }
        $dealValueArray=array();
        foreach($yearArray as $yearKey=>$yearValue)
        {
            if($yearValue<$month)
            {
                $dealValueCount=0;
                foreach($getRecords as $getRecordsKey=>$getRecordsValue)
                {
                    if(explode("-",$getRecordsValue['created_date'])[1]==$yearValue && $wonStage==$getRecordsValue['status'])
                    {
                        $dealValueCount=$dealValueCount+$getRecordsValue['value'];
                    }
                }
                array_push($dealValueArray,$dealValueCount);
            }
        }
        return $dealValueArray;
    }

    public function getAjaxNotification()
    {
        $getNotification=DB::table('notifications')->select('*')->where('user_2',Auth::getUser()->id)->where('marked_as','unread')->get();
        $getNotification=json_decode(json_encode($getNotification), true);
        $readNotificationPartialy=DB::table('notifications')->where('user_2',Auth::getUser()->id)->where('marked_as','unread')->update(['marked_as' => 'partialy_read']);
        return response()->json(['response' => $getNotification]);
    }
}
