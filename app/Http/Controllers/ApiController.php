<?php

namespace App\Http\Controllers;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
use Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\Product;
use Illuminate\Routing\UrlGenerator;
use App\Feed;
use App\Contacts;
use Mail;
use MailerLiteApi\MailerLite;
require 'vendor/autoload.php';

class ApiController extends Controller
{
    //
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->productTable = new Product();
        $this->feedTable = new Feed();
        $this->contactTable = new Contacts();
    }

    public function checkBooking()
    {
        $date_string = Input::get('date_string');
        $date_array = explode(",",$date_string);
        $start_date = date("Y-m-d", strtotime($date_array[0]));
		$end_date = date("Y-m-d", strtotime($date_array[1]));
        $getCompleteStageId = DB::table('stages')->select('*')->where('slug','complete')->first();
        $getCompleteStageId = json_decode(json_encode($getCompleteStageId), true);
        $deluxe = array();
        $superDeluxe1 = array();
        $superDeluxe2 = array();
        $room = array();
        if(Input::get('person') < 6)
        {
        	$getDeluxeBooking = DB::table('deals')->where('stage',$getCompleteStageId['id'])->where('deal_start','<=',$end_date)->where('deal_end','>=',$start_date)->whereRaw("FIND_IN_SET(1,related_product)")->count();
	        $getDeluxeBooking = json_decode(json_encode($getDeluxeBooking), true);
	        $getSuperDeluxe1Booking = DB::table('deals')->where('stage',$getCompleteStageId['id'])->where('deal_start','<=',$end_date)->where('deal_end','>=',$start_date)->whereRaw("FIND_IN_SET(2,related_product)")->count();
	        $getSuperDeluxe1Booking = json_decode(json_encode($getSuperDeluxe1Booking), true);
	        array_push($deluxe,$getDeluxeBooking);
			array_push($superDeluxe1,$getSuperDeluxe1Booking);
			if($getDeluxeBooking == 0)
			{
				$getDeluxeProduct = $this->productTable->getProductById(1);
				array_push($room,$getDeluxeProduct);
			}
			if($getSuperDeluxe1Booking == 0)
			{
				$getSuperDeluxe1Product = $this->productTable->getProductById(2);
				array_push($room,$getSuperDeluxe1Product);
			}
        }
        if(Input::get('person') >= 6)
        {
        	$getSuperDeluxe1Booking = DB::table('deals')->where('stage',$getCompleteStageId['id'])->where('deal_start','<=',$start_date)->where('deal_end','>=',$end_date)->whereRaw("FIND_IN_SET(2,related_product)")->count();
	        $getSuperDeluxe1Booking = json_decode(json_encode($getSuperDeluxe1Booking), true);
	        $getSuperDeluxe2Booking = DB::table('deals')->where('stage',$getCompleteStageId['id'])->where('deal_start','<=',$end_date)->where('deal_end','>=',$start_date)->whereRaw("FIND_IN_SET(3,related_product)")->count();
	        $getSuperDeluxe2Booking = json_decode(json_encode($getSuperDeluxe2Booking), true);
	        array_push($superDeluxe1,$getSuperDeluxe1Booking);
			array_push($superDeluxe2,$getSuperDeluxe2Booking);
			if($getSuperDeluxe1Booking == 0)
			{
				$getSuperDeluxe1Product = $this->productTable->getProductById(2);
				array_push($room,$getSuperDeluxe1Product);
			}
			if($getSuperDeluxe2Booking == 0)
			{
				$getSuperDeluxe2Product = $this->productTable->getProductById(3);
				array_push($room,$getSuperDeluxe2Product);
			}
        }
        return response()->json(['response' => 'success' , 'start_date' => $start_date , 'end_date' => $end_date , 'id' => $getCompleteStageId['id'] , 'deluxe' => $deluxe , 'superDeluxe1' => $superDeluxe1 , 'superDeluxe2' => $superDeluxe2 , 'room' => $room]);
    }

    public function getV()
    {
        $getProduct = $this->productTable->getProductList();
        return $getProduct;
    }

    public function requestBooking()
    {
        $customer_name = Input::get('customer_name');
        $first_name = $customer_name;
        $last_name = "";
        if ($customer_name == trim($customer_name) && strpos($customer_name, ' ') !== false)
        {
            $first_name = explode(" ",$customer_name)[0];
            $last_name = explode(" ",$customer_name)[1];
        }
        $phone_array = array('home' => array() , 'work' => array(Input::get('phone_no')));
        $email_array = array('home' => array() , 'work' => array(Input::get('email')));
        $phone = serialize($phone_array);
        $email = serialize($email_array);
        $company = 0;
        $created_by = 1;
        $created_date = date('Y-m-d');
        $created_at = date('Y-m-d H:i:s');
        $updated_at = date('Y-m-d H:i:s');
        $checkIfContactExist = $this->contactTable->checkIfEmailExist($email);
        if(!empty($checkIfContactExist))
        {
            $createContact = $checkIfContactExist['id'];
        }
        else
        {
            $createContact = DB::table('contacts')->insertGetId(['title' => 'Mr', 'first_name' => $first_name, 'last_name' => $last_name, 'phone' => $phone, 'email' => $email, 'created_at' => $created_at, 'updated_at' => $updated_at]);
        }
        $title = $first_name."-".Input::get('night')." night ".Input::get('day')." day";
        $contact = $createContact;
        $related_product = implode(",",Input::get('room'));
        $deal_start = Input::get('start_date');
        $deal_end = Input::get('end_date');
        $owner = 1;
        $status = 1;
        $stage = 1;
        $priority = 'medium';
        $room_array = implode(",",Input::get('room'));
        $price = Input::get('price');
        $price_array = array();
        $type_array = Input::get('type');
        $add_on_array = Input::get('add_on');
        $add_on_title = Input::get('add_on_title');
        $total_price = 0;
        $description = Input::get('specialrequest');
        $room_name_array = Input::get('room_name_array');
        $room_price_array = Input::get('price_array');
        $per_person_price_array = Input::get('budget_room_per_person_array');
        $add_on_title_array = Input::get('add_on_title');
        $add_on_price_array = Input::get('add_on_price_array');
        $trip_start_date = $this->getTripDate($deal_start);
        $trip_end_date = $this->getTripDate($deal_end);
        $other_amenities = Input::get('other_amenities');
        $package = Input::get('trip_package');
        $total_price_without_gst = Input::get('total_price_without_gst');
        $currency = "INR";
        $win_probability = 80;
        $createDeal = DB::table('deals')->insertGetId(['title' => $title, 'description' => $description, 'contact' => $contact, 'related_product' => $room_array, 'deal_start' => $deal_start, 'deal_end' => $deal_end, 'owner' => $owner, 'status' => $status, 'source' => 'Website', 'stage' => $stage, 'priority' => $priority, 'value' => $price, 'value_without_gst' => $total_price_without_gst, 'currency' => $currency, 'package' => $package, 'person' => Input::get('person'), 'win_probability' => $win_probability, 'created_by' => $created_by, 'created_date' => $created_date, 'created_at' => $created_at, 'updated_at' => $updated_at]);
        $related_product_array = explode(",",$room_array);
        foreach($related_product_array as $related_product_array_key=>$related_product_array_value)
        {
            $getProduct = Db::table('products')->select('*')->where('id',$related_product_array_value)->first();
            $getProduct = json_decode(json_encode($getProduct), true);
            $createDealItems = DB::table('deal_items')->insertGetId(['deal_id' => $createDeal , 'deal_items_name' => $getProduct['name'] , 'deal_items_description' => $getProduct['description'] , 'product_id' => $getProduct['id'] , 'product_price' => $getProduct['price']]);
        }
        //CREATE FEED
        $feed_title = "Deal ".$title." is created on ".$created_date;
        $user_id=1;
        $createFeed = $this->feedTable->createFeed($feed_title,$user_id,$contact,$createDeal,'deal');
        $data = array();
        $getAdminDetails=DB::table('users')->select('*')->where('role','1')->first();
        $getAdminDetails=json_decode(json_encode($getAdminDetails), true);
        $email = Input::get('email');
        $sender_name = "Sundarban";
        $sender_email = "info@sundarbanhouseboat.com";
        $getPortalName=env('PORTAL_NAME');
        $day = Input::get('day')+1;
        if(Input::get('night') > 1)
        {
            $trip_package = Input::get('night')." Nights";
        }
        else
        {
            $trip_package = Input::get('night')." Night";
        }
        if($day > 1)
        {
            $trip_package = $trip_package." - ".$day." Days";
        }
        else
        {
            $trip_package = $trip_package." - ".$day." Day";
        }
        $data = array(
            'person' => Input::get('person'),
            'trip_package' => $trip_package,
            'package' => $package,
            'trip_start_date' => $trip_start_date,
            'trip_end_date' => $trip_end_date,
            'other_amenities' => $other_amenities,
            'room_array' => $room_name_array,
            'room_price_array' => $room_price_array,
            'per_person_price_array' => $per_person_price_array,
            'add_on_title_array' => $add_on_title_array,
            'add_on_price_array' => $add_on_price_array,
            'other_amenities' => $other_amenities,
            'price' => $price,
            'price_without_gst' => $total_price_without_gst,
            'name' => $first_name." ".$last_name,
            'email' => Input::get('email'),
            'phone' => Input::get('phone_no'),
            'special_request' => $description
        );
        $mailerliteClient = new \MailerLiteApi\MailerLite('0746bf45a67af9e71321dccb94269c45');
        $groupsApi = $mailerliteClient->groups();
        $groups = $groupsApi->get();
        $subscriber = [
          'email' => Input::get('email'),
          'name' => Input::get('customer_name'),
          'fields' => [
            'phone' => Input::get('phone_no')
          ]
        ];
        $addedSubscriber = $groupsApi->addSubscriber(9697060, $subscriber);
        //MAIL TO CUSTOMER
        Mail::send('templates/pulse-mailer/thankyou', $data, function($message) use ($email,$sender_name,$sender_email,$getPortalName) {
             $message->to($email, 'Sundarban')->subject
                ($getPortalName.' Thank You Email');
             $message->from($sender_email,$sender_name);
        });
        //MAIL TO ADMIN
        $sender_email = $email;
        $email = $getAdminDetails['email'];
        Mail::send('templates/pulse-mailer/thankyou', $data, function($message) use ($email,$sender_name,$sender_email,$getPortalName) {
             $message->to($email, 'Sundarban')->subject
                ($getPortalName.' Thank You Email');
             $message->bcc('notifications@webinnova.net', 'Sundarban')->subject
                ($getPortalName.' Thank You Email');
             $message->from($sender_email,$sender_name);
        });
        return response()->json(['response' => 'success' , 'price' => $price , 'price_array' => $price_array]);
    }
    
    public function sendQuote()
    {
        $sender_name = "Sundarban";
        $sender_email = "info@sundarbanhouseboat.com";
        $getPortalName=env('PORTAL_NAME');
        $getAdminDetails=DB::table('users')->select('*')->where('role','1')->first();
        $getAdminDetails=json_decode(json_encode($getAdminDetails), true);
        $email = $getAdminDetails['email'];
        $data = array(
            'name' => Input::get('customer_name'),
            'phone_no' => Input::get('phone_no'),
            'email' => Input::get('email'),
            'trip_start_date' => Input::get('start_date'),
            'trip_end_date' => Input::get('end_date'),
            'person' => Input::get('person'),
            'trip_package' => Input::get('room'),
            'other_amenities' => Input::get('other_amenities')
        );
        Mail::send('templates/pulse-mailer/send-quote', $data, function($message) use ($email,$sender_name,$sender_email,$getPortalName) {
             $message->to($email, 'Sundarban')->subject
                ($getPortalName.' Thank You Email');
             $message->bcc('notifications@webinnova.net', 'Sundarban')->subject
                ($getPortalName.' First Stage Email');
             $message->from($sender_email,$sender_name);
        });
        $mailerliteClient = new \MailerLiteApi\MailerLite('0746bf45a67af9e71321dccb94269c45');
        $groupsApi = $mailerliteClient->groups();
        $groups = $groupsApi->get();
        $subscriber = [
          'email' => Input::get('email'),
          'name' => Input::get('customer_name'),
          'fields' => [
            'phone' => Input::get('phone_no')
          ]
        ];
        $addedSubscriber = $groupsApi->addSubscriber(9695528, $subscriber);
        return response()->json(['response' => 'success']);
    }

    public function sendQuoteV2()
    {
        $sender_name = "Sundarban";
        $sender_email = "info@sundarbanhouseboat.com";
        $getPortalName=env('PORTAL_NAME');
        $getAdminDetails=DB::table('users')->select('*')->where('role','1')->first();
        $getAdminDetails=json_decode(json_encode($getAdminDetails), true);
        $email = $getAdminDetails['email'];
        $data = array(
            'trip_start_date' => Input::get('start_date'),
            'trip_end_date' => Input::get('end_date'),
            'person' => Input::get('person'),
            'trip_package' => Input::get('room'),
            'other_amenities' => Input::get('other_amenities')
        );
        Mail::send('templates/pulse-mailer/send-quote-v2', $data, function($message) use ($email,$sender_name,$sender_email,$getPortalName) {
             $message->to('notifications@webinnova.net', 'Sundarban')->subject
                ($getPortalName.' Thank You Email');
             $message->from($sender_email,$sender_name);
        });
        $mailerliteClient = new \MailerLiteApi\MailerLite('0746bf45a67af9e71321dccb94269c45');
        $groupsApi = $mailerliteClient->groups();
        $groups = $groupsApi->get();
        $subscriber = [
          'email' => Input::get('email'),
          'name' => Input::get('customer_name'),
          'fields' => [
            'phone' => Input::get('phone_no')
          ]
        ];
        $addedSubscriber = $groupsApi->addSubscriber(9695528, $subscriber);
        return response()->json(['response' => 'success']);
    }

    // public function createContact($contact_name)
    // {
    //     $createIfttt = DB::table('ifttt')->insertGetId('ifttt' => $contact_name);
    // }

    public function phoneContact()
    {
        $number = trim($_GET['number']);
        $checkNumber = DB::table('phone_contacts')->where('number', '=' , $number)->first();
        $checkNumber = json_decode(json_encode($checkNumber), true);
        if(!empty($checkNumber))
        {
            $reference_phone_contact = $checkNumber['id'];
        }
        else
        {
            $addNumberToPhoneContacts = DB::table('phone_contacts')->insertGetId(['contact_name' => $_GET['contact'] , 'number' => $number]);
            $reference_phone_contact = $addNumberToPhoneContacts;
        }
        $addCallActivityLogs = DB::table('call_activity_logs')->insertGetId(['ref_phone_contact' => $reference_phone_contact , 'date' => $_GET['date'] , 'duration' => $_GET['duration'] , 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s')]);
    }

    public function getTripDate($date)
    {
        $month_array = array("01" => "January","02" => "February","03" => "March","04" => "April","05" => "May","06" => "June","07" => "July","08" => "August","09" => "September","10" => "October","11" => "November","12" => "December");
        $month_number = explode("-",$date)[1];
        if(explode("-",$date)[2] == 1)
        {
            $getTripDate = explode("-",$date)[2]."st ".$month_array[$month_number].",".explode("-",$date)[0];
        }
        if(explode("-",$date)[2] == 2)
        {
            $getTripDate = explode("-",$date)[2]."nd ".$month_array[$month_number].",".explode("-",$date)[0];
        }
        if(explode("-",$date)[2] == 3)
        {
            $getTripDate = explode("-",$date)[2]."rd ".$month_array[$month_number].",".explode("-",$date)[0];
        }
        if(explode("-",$date)[2] > 3)
        {
            $getTripDate = explode("-",$date)[2]."th ".$month_array[$month_number].",".explode("-",$date)[0];
        }
        return $getTripDate;
    }
}
