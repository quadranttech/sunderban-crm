<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Routing\UrlGenerator;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use App\Traits\MainTrait;
use Jenssegers\Agent\Agent;
use Mail;
use App\Notification;
use DateTime;
use App\Traits\PermissionTrait;

class EstimateController extends InvoiceController
{
    //
    use MainTrait;
    use PermissionTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;  //GET BASE URL
        $this->middleware('auth');  //REDIRECTED TO LOGIN IF NOT AUTHENTICATED
        $this->currency_symbol = [
            'USD'=> '$', // US Dollar
            'EUR'=> '€', // Euro
            'CRC'=> '₡', // Costa Rican Colón
            'GBP'=> '£', // British Pound Sterling
            'ILS'=> '₪', // Israeli New Sheqel
            'INR'=> '₹', // Indian Rupee
            'JPY'=> '¥', // Japanese Yen
            'KRW'=> '₩', // South Korean Won
            'NGN'=> '₦', // Nigerian Naira
            'PHP'=> '₱', // Philippine Peso
            'PLN'=> 'zł', // Polish Zloty
            'PYG'=> '₲', // Paraguayan Guarani
            'THB'=> '฿', // Thai Baht
            'UAH'=> '₴', // Ukrainian Hryvnia
            'VND'=> '₫' // Vietnamese Dong
        ];
    }

    public function createEstimate()
    {
    	$getContact=DB::table('contacts')->select('*')->orderBy('title', 'asc')->orderBy('first_name', 'asc')->orderBy('last_name', 'asc')->get(); //GET CONTACTS
    	$getContact=json_decode(json_encode($getContact), true);
    	$getUser=DB::table('users')->select('*')->get();   //GET USERS
    	$getUser=json_decode(json_encode($getUser), true);
        $getCurrentUserCompany=DB::table('admin_company')->join('users', 'users.company', '=', 'admin_company.id')->select('*')->where('users.id', '=', Auth::getUser()->id)->first();  //GET CURRENT USER COMPANY
        $getCurrentUserCompany=json_decode(json_encode($getCurrentUserCompany), true);
        $company_id=$getCurrentUserCompany['company'];
        $getEstimateStartWith=DB::table('settings')->select('*')->where('meta_key','estimate_start')->where('company_id',$company_id)->first(); //GET ESTIMATE START WITH BY COMPANY ID
        $getEstimateStartWith=json_decode(json_encode($getEstimateStartWith), true);
        $getFirstEstimate=DB::table('estimates')->select('*')->where('company_id',$company_id)->get();  //GET COMPANY FIRST ESTIMATE
        $getFirstEstimate=json_decode(json_encode($getFirstEstimate), true);
        $getCompany=DB::table('company')->select('*')->get();   //GET CONTACT COMPANY
        $getCompany=json_decode(json_encode($getCompany), true);
        $getDefaultPaymentTerms=DB::table('settings')->select('meta_value')->where('company_id',$company_id)->where('meta_key','default_payment_term')->first();    //GET DEFAULT COMPANY DEFAULT PAYMENT TERM
        $getDefaultPaymentTerms=json_decode(json_encode($getDefaultPaymentTerms), true);
        $getDefaultPaymentTerms=$getDefaultPaymentTerms['meta_value'];
        $getTax=DB::table('taxs')->select('*')->where('company_id',$company_id)->get(); //GET COMPANY TAXS
        $getTax=json_decode(json_encode($getTax), true);
        $getDiscount=DB::table('discounts')->select('*')->where('company_id',$company_id)->get();   //GET COMPANY DISCOUNTS
        $getDiscount=json_decode(json_encode($getDiscount), true);
        //GENERATE ESTIMATE STAT WITH
        if(!empty($getEstimateStartWith) && !empty($getFirstEstimate))
        {
            $lastEstimateNo=explode("-",$getFirstEstimate[sizeof($getFirstEstimate)-1]['est_no']);
            $lastEstimateNo=sizeof($getFirstEstimate)+1;
            $generateEstimateNo=$getEstimateStartWith['meta_value'].'-'.sprintf('%04d', $lastEstimateNo);
        }
        if(!empty($getEstimateStartWith) && empty($getFirstEstimate))
        {
            $generateEstimateNo=$getEstimateStartWith['meta_value'].'-'.sprintf('%04d', 1);
        }
        if(empty($getEstimateStartWith) && !empty($getFirstEstimate))
        {
            $lastEstimateNo=explode("-",$getFirstEstimate[sizeof($getFirstEstimate)-1]['est_no']);
            $lastEstimateNo=sizeof($getFirstEstimate)+1;
            $generateEstimateNo="EST-".sprintf('%04d', $lastEstimateNo);
        }
        else if(empty($getEstimateStartWith) && empty($getFirstEstimate))
        {
            $generateEstimateNo="EST-".sprintf('%04d', 1);
        }
        $notification_table=new Notification;
        $getNotification=$notification_table->getNotification(Auth::getUser()->id);
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);  
        }
        $get_user_profile_image=$this->get_profile_image();
        $get_terms_and_conditions=$this->get_meta('est_terms_and_conditions',$company_id);
        $get_terms_and_conditions=json_decode(json_encode($get_terms_and_conditions), true);
        $getCompanyCurrency=array();
        $getCompanyCurrency=$this->get_company_currency();
        if(!empty($getCompanyCurrency))
        {
            $getCompanyCurrency=unserialize($getCompanyCurrency['currency_list']);
        }
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getDealStage=DB::table('stages')->select('*')->get();
        $getDealStage=json_decode(json_encode($getDealStage), true);
        $getDeal=DB::table('deals')->select('*')->get();
        $getDeal=json_decode(json_encode($getDeal), true);
        $getValidDealStage=$this->searchForDealStageId($getDealStage,$getDeal);
        $getDealStage=$getValidDealStage;
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        $agent = new Agent();   //GET USER AGENT
        if(Request::input('est_no')!="")    //IF EST NO IS NOT NULL THEN REDIRECT TO EDIT ESTIMATE
        {
            $getEstimate=DB::table('estimates')->select('*')->where('est_no',Request::input('est_no'))->first();
            $getEstimate=json_decode(json_encode($getEstimate), true);
            $getInvoiceItems=array();
            $getInvoiceItems=DB::table('estimate_items')->select('*')->where('estimate_id',$getEstimate['id'])->get();
            $getInvoiceItems=json_decode(json_encode($getInvoiceItems), true);
            $getInvoiceMeta=DB::table('estimate_meta')->select('*')->where('estimate_id',$getEstimate['id'])->first();
            $getInvoiceMeta=json_decode(json_encode($getInvoiceMeta), true);
            if($agent->isMobile())
            {
                return view('dashboard/estimate/edit-estimate-mobile', ['contacts' => $getContact], ['users' => $getUser , 'est_no' => $generateEstimateNo , 'company' => $getCompany , 'default_payment_term' => $getDefaultPaymentTerms , 'invoice' => $getEstimate , 'invoice_items' => $getInvoiceItems , 'estimate_meta' => $getInvoiceMeta , 'user_company' => $getCurrentUserCompany , 'tax' => $getTax , 'discount' => $getDiscount , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'estimate' , 'terms_and_conditions' => $get_terms_and_conditions , 'currency' => $getCompanyCurrency , 'show_tutorial' => $checkShowTutorial , 'deal_stage' => $getDealStage , 'deal' => $getDeal , 'permission' => $getUserPermission]);
            }
            return view('dashboard/estimate/edit-estimate', ['contacts' => $getContact], ['users' => $getUser , 'est_no' => $generateEstimateNo , 'company' => $getCompany , 'default_payment_term' => $getDefaultPaymentTerms , 'invoice' => $getEstimate , 'invoice_items' => $getInvoiceItems , 'estimate_meta' => $getInvoiceMeta , 'user_company' => $getCurrentUserCompany , 'tax' => $getTax , 'discount' => $getDiscount , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'estimate' , 'terms_and_conditions' => $get_terms_and_conditions , 'currency' => $getCompanyCurrency , 'show_tutorial' => $checkShowTutorial , 'deal_stage' => $getDealStage , 'deal' => $getDeal , 'permission' => $getUserPermission]);
        }
        if($agent->isMobile())
        {
            return view('dashboard/estimate/estimate-mobile', ['contacts' => $getContact], ['users' => $getUser , 'est_no' => $generateEstimateNo , 'company' => $getCompany , 'default_payment_term' => $getDefaultPaymentTerms , 'user_company' => $getCurrentUserCompany , 'tax' => $getTax , 'discount' => $getDiscount , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'estimate' , 'terms_and_conditions' => $get_terms_and_conditions , 'currency' => $getCompanyCurrency , 'show_tutorial' => $checkShowTutorial , 'deal_stage' => $getDealStage , 'deal' => $getDeal , 'permission' => $getUserPermission]);
        }
    	return view('dashboard/estimate/estimate', ['contacts' => $getContact], ['users' => $getUser , 'est_no' => $generateEstimateNo , 'company' => $getCompany , 'default_payment_term' => $getDefaultPaymentTerms , 'user_company' => $getCurrentUserCompany , 'tax' => $getTax , 'discount' => $getDiscount , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'estimate' , 'terms_and_conditions' => $get_terms_and_conditions , 'currency' => $getCompanyCurrency , 'show_tutorial' => $checkShowTutorial , 'deal_stage' => $getDealStage , 'deal' => $getDeal , 'permission' => $getUserPermission]);
    }

    public function saveEstimate()
    {
        $sendDate="";
        if(Input::get('status')=="send")
        {
            $sendDate=date("Y-m-d");    //SEND DATE IS CURRENT DATE
        }
        $getCurrentUserDetails=DB::table('admin_company')->join('users', 'users.company', '=', 'admin_company.id')->select('*')->where('users.id', '=', Auth::getUser()->id)->first();  //GET CURRENT USER COMPANY
        $getCurrentUserDetails=json_decode(json_encode($getCurrentUserDetails), true);
        $company_id=$getCurrentUserDetails['company'];
        if(Input::get('status')=="send")    //IF STATUS IS SEND THEN CHECK SMTP
        {
            $getSmtpUsername=$this->get_meta('smtp_username',$company_id);
            $getSmtpUsername=json_decode(json_encode($getSmtpUsername), true);
            $getSmtpPassword=$this->get_meta('smtp_password',$company_id);
            $getSmtpPassword=json_decode(json_encode($getSmtpPassword), true);
            $getSmtpHost=$this->get_meta('smtp_host',$company_id);
            $getSmtpHost=json_decode(json_encode($getSmtpHost), true);
            if($getSmtpUsername['meta_value']=="" || $getSmtpPassword['meta_value']=="" || $getSmtpHost['meta_value']=="")
            {
                return response()->json(['response' => 'failed' , 'message' => 'smtp_not_found']);
            }
        }
        if(Input::get('id')=="" && Input::get('purpose')!="edit")
        {
            $getEstimateByInvNo=DB::table('estimates')->select('*')->where('est_no',Input::get('invoice_no'))->first();
            $getInvoiceItemTotal=json_decode(json_encode($getEstimateByInvNo), true);
            if(!empty($getEstimateByInvNo))
            {
                return response()->json(['response' => 'failed' , 'message' => 'est_no_already_exist']);
            }
        }
        if(Input::get('contact_type')=="company")
        {
            $contact=DB::table('company')->select('default_contact')->where('id',Input::get('invoice_contact'))->first();   //IF CONTACT TYPE COMPANY THEN GET DEFAULT CONTACT OF THAT COMPANY
            $contact=json_decode(json_encode($contact), true);
            $contact=$contact['default_contact'];
            if($contact=="0")   //IF COMPANY DOES NOT HAVE ANY DEFAULT CONTACT
            {
                return response()->json(['response' => 'failed' , 'message' => 'company_does_not_have_contact']);
            }
        }
        else
        {
            $contact=Input::get('invoice_contact'); //ELSE GET CONTACT
        }
        if(Input::get('id')!="" && Input::get('purpose')=="edit")   //IF ESTIMATE ID IS PRESENT AND PURPOSE IS EDIT THEN UPDATE ESTIMATE
        {
            $updateInvoice=DB::table('estimates')->where('id',Input::get('id'))->update(['company_id' =>$company_id, 'deal_id' => Input::get('deal_id'), 'contact' => $contact, 'contact_type' => Input::get('contact_type'), 'est_date' => Input::get('invoice_date'), 'default_payment_term' => Input::get('default_payment_term'), 'sales_person' => Input::get('sales_person'), 'status' => 'draft', 'currency' => Input::get('currency')[0], 'send_date' => $sendDate, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            $deleteInvoiceItems=DB::table('estimate_items')->where('estimate_id',Input::get('id'))->delete();
            $deleteInvoiceMeta=DB::table('estimate_meta')->where('estimate_id',Input::get('id'))->delete();
            $createInvoice=Input::get('id');
        }
        else
        {
            $link=$this->generateRandomString();    //GENERATE RANDOM STRING
            $sales_person=Input::get('sales_person');
            if($sales_person=="select_user")
            {
                $sales_person=Auth::getUser()->id;  //IF SALES PERSON IS NULL THEN CURRENT USER IS SALES PERSON
            }
            $createInvoice=DB::table('estimates')->insertGetId(['est_no' => Input::get('invoice_no'), 'estimate_title' => Input::get('estimate_title'), 'created_by' => Auth::getUser()->id, 'company_id' =>$company_id, 'deal_id' => Input::get('deal_id'), 'contact' => $contact, 'contact_type' => Input::get('contact_type'), 'est_date' => Input::get('invoice_date'), 'default_payment_term' => Input::get('default_payment_term'), 'sales_person' => $sales_person, 'status' => 'draft', 'currency' => Input::get('currency')[0], 'link' => $link, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);   //ELSE CREATE ESTIMATE
        }
        $log_title=(Input::get('status')=='draft' ? 'saved to draft' : 'sent to customer');
        $total=0;
        $items_array=array();
        foreach(Input::get('item_details_array') as $key=>$value)   //CREATE ITEM DETAILS ARRAY
        {
            $invoiceItemDetails=$value;
            $invoiceItemRate=Input::get('rate_array')[$key];
            $invoiceItemTotal=Input::get('quantity_array')[$key]*Input::get('rate_array')[$key];
            if($invoiceItemDetails="")
            {
                $invoiceItemDetails="";
            }
            if($invoiceItemRate=="" || !is_numeric($invoiceItemRate))
            {
                $invoiceItemRate=0.00;
            }
            if($invoiceItemTotal=="" || !is_numeric($invoiceItemTotal))
            {
                $invoiceItemTotal=0.00;
            }
            $insertInvoiceItems=DB::table('estimate_items')->insertGetId(['estimate_id' => $createInvoice, 'item_details' => $value, 'rate' => $invoiceItemRate, 'quantity' => Input::get('quantity_array')[$key], 'quantity_type' => Input::get('quantity_type'), 'currency' => Input::get('currency')[$key], 'total' => $invoiceItemTotal, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            $total=$total+(Input::get('quantity_array')[$key]*Input::get('rate_array')[$key]);
            $temp=array();
            $temp['item_details']=$value;
            $temp['rate']=$invoiceItemRate;
            $temp['quantity']=Input::get('quantity_array')[$key];
            $temp['quantity_type']=Input::get('quantity_type');
            $temp['currency']=Input::get('currency')[$key];
            $temp['total']=$invoiceItemTotal;
            array_push($items_array,$temp);
        }
        $inv_total=$total+Input::get('shipping_cost')-abs(Input::get('adjustment'))-abs(Input::get('discount'));
        $inv_total=$inv_total+Input::get('tax');    //ESTIMATE TOTAL
        if(Input::get('part_pay_type')=="no_part_pay")
        {
            $due=0; //IF PART PAY IS NOT CHECKED THEN DUE IS 0
        }
        else
        {
            if(Input::get('part_pay_type')=="fixed")
            {
                $due=$inv_total-Input::get('part_pay_value');   //IF PART PAY IS CHECKED AND VALUE IS FIXED
            }
            else
            {
                $due=(($inv_total)-((Input::get('part_pay_value') / 100) * $inv_total));    //IF PART PAY IS CHECKED AND VALUE IS PERCENTAGE
            }
        }
        $key=$this->generateRandomString(); //GENERATE RADOM STRING AS KEY
        $terms_and_conditions=Input::get('terms_and_conditions');   //GET TERMS AND CONDITIONS
        if($terms_and_conditions=="")   //IF TERMS AND CONDITIONS IS NULL
        {
            $terms_and_conditions=$this->get_meta('est_terms_and_conditions',$company_id);
            $terms_and_conditions=json_decode(json_encode($terms_and_conditions), true);
            $terms_and_conditions=$terms_and_conditions['meta_value'];
        }
        $insertInvoiceMeta=DB::table('estimate_meta')->insertGetId(['estimate_id' => $createInvoice,'attachment' => '', 'tax' => Input::get('tax'), 'tax_id' => Input::get('tax_id'), 'discount' => Input::get('discount'), 'discount_id' => Input::get('discount_id'), 'shipping_cost' => Input::get('shipping_cost'), 'inv_total' => $inv_total, 'adjustment' => Input::get('adjustment'), 'part_pay' => Input::get('part_pay_type'), 'part_pay_value' => Input::get('part_pay_value'), 'due' => $due, 'customer_note' => Input::get('customer_note'), 'admin_note' => Input::get('admin_note'), 'terms_and_conditions' => $terms_and_conditions, 'status' => 'due', 'key' => $key, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);   //CREATE ESTIMATE META
        $getContact=DB::table('contacts')->select('*')->where('id',$contact)->first();  //GET CONTACT
        $getContactDetails=json_decode(json_encode($getContact), true);
        $getContactAddressDetails=DB::table('contacts_address')->select('*')->where('contact_id',$contact)->first();    //GET CONTACT ADDRESS
        $getContactAddressDetails=json_decode(json_encode($getContactAddressDetails), true);
        $portalName=env('PORTAL_NAME');
        $getLabelName=$this->getLabelName();
        $base_url=$this->url->to('/');
        $content=file_get_contents($base_url.'/resources/views/templates/pulse-mailer/estimate-preview-template.php');
        return response()->json(['response' => $inv_total, 'items_array' => $items_array, 'admin_company' => $getCurrentUserDetails, 'contact_details' => $getContactDetails, 'terms_and_conditions' => $terms_and_conditions, 'est_id' => $createInvoice, 'content' => $content, 'estimate_title' => Input::get('estimate_title'), 'est_no' => Input::get('invoice_no'), 'discount' => abs(Input::get('discount')), 'tax' => Input::get('tax'), 'portalName' => $portalName, 'quantity_type' => Input::get('quantity_type'), 'label_name' => $getLabelName, 'currency_symbol' => $this->getCurrencySymbol('en',Input::get('currency')[0]), 'contact_address' => $getContactAddressDetails]);
    }

    public function createEstimateActivityLog($inv_id,$log_title)
    {
        $log_title='Invoice '.$log_title;   //LOG TITLE
        $createInvoiceActivityLog=DB::table('invoice_activity_logs')->insertGetId(['inv_id' => $inv_id, 'log_activity' => $log_title, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);   //CREATE ACTIVITY LOG
        return $createInvoiceActivityLog;
    }

    public function sendEstimateMail($contact,$items_array,$inv_total,$shipping_cost,$discount,$tax,$adjustment,$total,$key,$show_on_page,$terms_and_conditions,$link,$estimate_title,$logo)
    {
        $getContactDetails=DB::table('contacts')->select('*')->where('id',$contact)->first();   //GET CONTACT
        $getContactAddressDetails=DB::table('contacts_address')->join('contacts', 'contacts.id', '=', 'contacts_address.contact_id')->select('*')->where('contacts.id', '=',$contact)->first(); //GET CONTACT ADDRESS
        $getContactAddressDetails=json_decode(json_encode($getContactAddressDetails), true);
        $contact_address="";
        $contact_country="";
        $contact_state="";
        $contact_post_code="";
        if($getContactAddressDetails!="")
        {
            $contact_address=$getContactAddressDetails['street_address'];
            $contact_country=$getContactAddressDetails['country'];
            $contact_state=$getContactAddressDetails['state'];
            $contact_post_code=$getContactAddressDetails['post_code'];
        }
        $getContactDetails=DB::table('contacts')->select('*')->where('id',$contact)->first();
        $getContactDetails=json_decode(json_encode($getContactDetails), true);
        $contact_name=$getContactDetails['first_name']." ".$getContactDetails['last_name']; //GET CONTACT INFORMATION
        $getAdminCompanyDetails=DB::table('admin_company')->select('*')->first();   //GET ADMIN COMPANY
        $getAdminCompanyDetails=json_decode(json_encode($getAdminCompanyDetails), true);
        $company_name=$getAdminCompanyDetails['company_name'];  //GET ADMIN COMPANY INFORMATION
        $company_address=$getAdminCompanyDetails['address'];
        $company_country=$getAdminCompanyDetails['country'];
        $company_state=$getAdminCompanyDetails['state'];
        $company_post_code=$getAdminCompanyDetails['post_code'];
        $sender_name=$getAdminCompanyDetails['sender_name'];
        $logo=$getAdminCompanyDetails['logo'];
        if($logo=="")
        {
            $logo=$this->url->to('/').'/assets/img/logo.png';
        }
        else
        {
            $logo=$this->url->to('/').'/uploads/company-logo/'.$getAdminCompanyDetails['logo'];
        }
        if($sender_name=="")    //EMAIL SENDER NAME
        {
            $getAdminName=DB::table('users')->select('*')->where('role','1')->first();
            $getAdminName=json_decode(json_encode($getAdminName), true);
            $sender_name=$getAdminName['first_name'];
        }
        $sender_email=$getAdminCompanyDetails['default_email'];
        if($sender_email=="" || $getAdminCompanyDetails['marked_as_default']=="no")
        {
            $getAdminName=DB::table('users')->select('*')->where('role','1')->first();
            $getAdminName=json_decode(json_encode($getAdminName), true);
            $sender_email=$getAdminName['email'];
        }
        if(!empty(unserialize($getContactDetails['email'])['work']))
        {
            $email=unserialize($getContactDetails['email'])['work'][0];
        }
        else
        {
            $email=unserialize($getContactDetails['email'])['home'][0];
        }
        $accept_link=$this->url->to('/').'/accept-estimate/'.$key;  //ESTIMATE ACCEPT LINK
        $decline_link=$this->url->to('/').'/decline-estimate/'.$key;    //ESTIMATE DECLIE LINK
        $view_on_browser_link=$this->url->to('/').'/view-estimate-on-browser/'.$link;
        if($show_on_page==true) //IF ESTIMATE WILL SHOW IN PAGE OR NOT
        {
            $data=['items_array'=>$items_array,'inv_total'=>$inv_total,'discount'=>$discount,'tax'=>$tax,'total'=>$total,'contact_address'=>$contact_address,'contact_country'=>$contact_country,'contact_state'=>$contact_state,'contact_post_code'=>$contact_post_code,'contact_name'=>$contact_name,'company_name'=>$company_name,'company_address'=>$company_address,'company_country'=>$company_country,'company_state'=>$company_state,'company_post_code'=>$company_post_code,'accept_link'=>$accept_link,'quantity_type'=>$items_array[0]['quantity_type'],'decline_link'=>$decline_link];
            return $data;
        }
        $send=$this->send_estimate_mail($email,$items_array,$inv_total,$discount,$tax,$adjustment,$total,$contact_address,$contact_country,$contact_state,$contact_post_code,$contact_name,$company_name,$company_address,$company_country,$company_state,$company_post_code,$sender_name,$sender_email,$accept_link,$decline_link,$terms_and_conditions,$link,$estimate_title,$items_array[0]['quantity_type'],$view_on_browser_link,$logo);  //SEND ESTIMATE EMAIL
        if($send==1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function send_estimate_mail($email,$items_array,$inv_total,$discount,$tax,$adjustment,$total,$contact_address,$contact_country,$contact_state,$contact_post_code,$contact_name,$company_name,$company_address,$company_country,$company_state,$company_post_code,$sender_name,$sender_email,$accept_link,$decline_link,$terms_and_conditions,$link,$estimate_title,$quantity_type,$view_on_browser_link,$logo){
        $link=$this->url->to('/').'/show-estimate-pdf/'.$link;  //ESTIMATE PDF LINK
        $currency_symbol=$this->getCurrencySymbol('en',$items_array[0]['currency']);
        if($quantity_type=="quantity")
        {
            $quantity_type="Quantity";
        }
        else if($quantity_type=="hr")
        {
            $quantity_type="hr";
        }
        else if($quantity_type=="item")
        {
            $quantity_type="Item";
        }
        else
        {
            $quantity_type="hr + Item";
        }
      $data = array('name'=>'gfgf','items_array'=>$items_array,'inv_total'=>$inv_total,'discount'=>$discount,'tax'=>$tax,'adjustment'=>$adjustment,'total'=>$total,'contact_address'=>$contact_address,'contact_country'=>$contact_country,'contact_state'=>$contact_state,'contact_post_code'=>$contact_post_code,'contact_name'=>$contact_name,'company_name'=>$company_name,'company_address'=>$company_address,'company_country'=>$company_country,'company_state'=>$company_state,'company_post_code'=>$company_post_code,'accept_link'=>$accept_link,'decline_link'=>$decline_link,'view_on_browser_link'=>$view_on_browser_link,'terms_and_conditions' => $terms_and_conditions,'pdf_link'=>$link,'currency_symbol'=>$currency_symbol,'logo'=>$logo,'quantity_type'=>$quantity_type);
      try
      {
        Mail::send('templates/pulse-mailer/estimate-template', $data, function($message) use ($email,$sender_name,$sender_email,$estimate_title) {
             $message->to($email, 'Tutorials Point')->subject
                ($estimate_title);
             $message->from($sender_email,$sender_name);
          });
        return 1;
      }
      catch (Exception $e)
      {
        return 0;
      }
   }

   public function showEstimate()
   {
        $getEstimates=DB::table('estimates')->select('*')->orderBy('created_at', 'desc')->get();   //GET ALL ESTIMATES
        $getEstimates=json_decode(json_encode($getEstimates), true);
        $contacts_array=array();
        $estimates=array();
        foreach($getEstimates as $key=>$value)
        {
            $temp=array();
            $temp['id']=$value['id'];   //ESTIMATE INFORMATION
            $temp['est_no']=$value['est_no'];
            if($value['contact_type']=="company")
            {
                $company=DB::table('company')->join('contacts', 'contacts.company', '=', 'company.id')->select('*')->where('contacts.id', '=', $value['contact'])->first();
                $contact=$company['company_name'];
            }
            else
            {
                $contact_details=DB::table('contacts')->select('*')->where('id',$value['contact'])->first();
                $contact_details=json_decode(json_encode($contact_details), true);
                $contact=$contact_details['first_name']." ".$contact_details['last_name'];
            }
            $temp['contact']=$contact;
            $temp['proposal_title']="";
            $temp['date_created']=$value['created_at'];
            $get_value=DB::table('estimate_meta')->select('*')->where('estimate_id',$value['id'])->first();
            $get_value=json_decode(json_encode($get_value), true);
            $temp['value']=sprintf("%.2f", $get_value['inv_total']);
            $temp['status']=$value['status'];
            $temp['currency']=$value['currency'];
            $temp['currency_symbol']=$this->getCurrencySymbol('en',$value['currency']);
            array_push($estimates,$temp);
        }
        $notification_table=new Notification;
        $getNotification=$notification_table->getNotification(Auth::getUser()->id);
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);
        }
        $get_user_profile_image=$this->get_profile_image();
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        $agent = new Agent();   //GET USER AGENT
        if($agent->isMobile())
        {
            return view('dashboard/estimate/estimate-list-mobile', ['estimates' => $estimates , 'estimates_size' => sizeof($estimates) , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'estimate' , 'show_tutorial' => $checkShowTutorial , 'permission' => $getUserPermission]);
        }
        return view('dashboard/estimate/estimate-list', ['estimates' => $estimates , 'estimates_size' => sizeof($estimates) , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'estimate' , 'show_tutorial' => $checkShowTutorial , 'permission' => $getUserPermission]);
   }

   function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getNotificationDifference($getNotification)
    {
        date_default_timezone_set(env('TIME_ZONE'));    //GET ENVIRONMENT TIME ZONE
        $notification_difference_array=array(); //CREATE NOTIFICATION DIFFERENCE ARRAY
        foreach($getNotification as $key=>$value)
        {
            $datetime1 = new DateTime($getNotification[$key]['created_at']);    //NOTIFICATION CREATED DATE
            $datetime2 = new DateTime(date("Y-m-d h:i:s a"));   //CURRENT DATE
            $interval = $datetime1->diff($datetime2);   //NOTIFICATION DIFFERENCE
            if($interval->format('%h')>0)
            {
                $interval = $interval->format('%h')." hr ".$interval->format('%i')." min ago";  //NOTIFICATION DIFFERENCE IN HOUR AND MINUTE
            }
            else
            {
                $interval = $interval->format('%i')." min ago"; //NOTIFICATION DIFFERENCE IN MINUTE
            }
            array_push($notification_difference_array,$interval);
        }
        return $notification_difference_array;
    }

    public function sendEstimate()
    {
        $getInvoiceDetails=DB::table('estimates')->select('*')->where('id',Input::get('estimate_id'))->first(); //GET INPUT ESTIMATE ID AND GET ESTIMATE INFORMATION WITH ESTIMATE ITEMS AND ESTIMATE META AND SEND ESTIMATE EMAIL
        $getInvoiceDetails=json_decode(json_encode($getInvoiceDetails), true);
        $getInvoiceMeta=DB::table('estimate_meta')->select('*')->where('estimate_id',Input::get('estimate_id'))->first();
        $getInvoiceMeta=json_decode(json_encode($getInvoiceMeta), true);
        $getInvoiceItems=DB::table('estimate_items')->select('*')->where('estimate_id',Input::get('estimate_id'))->get();
        $getInvoiceItems=json_decode(json_encode($getInvoiceItems), true);
        $getInvoiceItemTotal=DB::table('estimate_items')->where('estimate_id',Input::get('estimate_id'))->sum('total');
        $getInvoiceItemTotal=json_decode(json_encode($getInvoiceItemTotal), true);
        $total=$getInvoiceMeta['inv_total'];
        $shipping_cost=$getInvoiceMeta['shipping_cost'];
        $discount=$getInvoiceMeta['discount'];
        $tax=$getInvoiceMeta['tax'];
        $adjustment=$getInvoiceMeta['adjustment'];
        $checkSmtp=$this->checkSmtp();
        if($checkSmtp!="ok")
        {
            return response()->json(['status' => $checkSmtp]);
        }
        $updateInvoiceStatus=DB::table('estimates')->where('id',Input::get('estimate_id'))->update(['status' => 'send']);
        $updateInvoiceSendDate=DB::table('estimates')->where('id',Input::get('estimate_id'))->update(['send_date' => date("Y-m-d")]);
        $estimate_title=Input::get('estimate_subject');
        if($estimate_title=="")
        {
            $estimate_title=env('PORTAL_NAME');
        }
        $ee=$this->sendEstimateMail($getInvoiceDetails['contact'],$getInvoiceItems,$total,$shipping_cost,$discount,$tax,$adjustment,$getInvoiceItemTotal,$getInvoiceMeta['key'],false,$getInvoiceMeta['terms_and_conditions'],$getInvoiceDetails['link'],$estimate_title,'');
        if($ee==true)
        {
            return response()->json(['response' => 'success']);
        }
        else
        {
            return response()->json(['response' => 'failed' , 'message' => 'failed to send email please check smtp details']);
        }
    }
}
