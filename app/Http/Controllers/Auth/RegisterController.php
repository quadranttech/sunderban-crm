<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;
use App\Traits\PermissionTrait;
use DB;
use Auth;
use App\Traits\MainTrait;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use PermissionTrait;
    use MainTrait;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        if(array_key_exists('company_name',$data))
        {
            $company=$data['company_name'];
        }
        else
        {
            $company=0;
        }
        $department=$data['department'];
        if($department=="")
        {
            $department='no_department';
        }
        $name=$data['first_name'].' '.$data['last_name'];
        $user_id = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'company' => '1',
            'department' => $department,
            'phone_no' => $data['phone_no'],
            'status' => 'Active',
            'show_tutorial' => 'on',
            'role' => $data['role'],
        ]);
        // $sender_name=$this->sender_name($user_id->id);
        // $sender_email=$this->sender_email($user_id->id);
        $this->sendWellcomeMail($name,$data['email']);
        if($data['role']=="2")
        {
            $contact=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));
            $deal=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));
            $task=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));
            $invoice=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));
            $estimate=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));
            $setting=serialize(array("add"=>"on","edit"=>"on","view"=>"on","all"=>"on"));
            $this->setPermissionToUser($user_id->id,$contact,$deal,$task,$invoice,$estimate,$setting);
        }
        $createUserMeta=DB::table('user_meta')->insertGetId(['user_id' => $user_id->id, 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s')]);
        if(isset($data['key']))
        {
            $deleteInvitation=DB::table('invitation')->where('key',$data['key'])->delete();
        }
        return $user_id;
        
    }

    public function sendWellcomeMail($name,$email)
    {
        $portalName=env('PORTAL_NAME');
        $sender_name=$this->sender_name();
        $sender_email=$this->sender_email();
        $get_admin_name_and_company=$this->admin_name_and_company();
        $data = array('name'=>$name,'admin_name'=>$get_admin_name_and_company['admin_name'],'admin_company'=>$get_admin_name_and_company['company_name'],'admin_email'=>$get_admin_name_and_company['admin_email']);
        Mail::send('templates/pulse-mailer/welcome', $data, function($message) use ($email,$sender_email,$sender_name,$portalName) {
             $message->to($email, 'Tutorials Point')->subject
                ('Welcome to '.$portalName);
             $message->from($sender_email,$sender_name);
        });
    }

    public function sender_name()
    {
        $get_sender_name=DB::table('admin_company')->select('*')->first();
        $get_sender_name=json_decode(json_encode($get_sender_name), true);
        if($get_sender_name['sender_name']!="")
        {
            $get_sender_name=$get_sender_name['sender_name'];
        }
        else
        {
            $getCurrentUserDetails=DB::table('users')->select('*')->where('role','1')->first();
            $getCurrentUserDetails=json_decode(json_encode($getCurrentUserDetails), true);
            $get_sender_name=$getCurrentUserDetails['first_name']." ".$getCurrentUserDetails['last_name'];
        }
        return $get_sender_name;
    }

   public function sender_email()
    {
        $get_sender_email=DB::table('admin_company')->select('*')->first();
        $get_sender_email=json_decode(json_encode($get_sender_email), true);
        if($get_sender_email['default_email']!="")
        {
            $get_sender_email=$get_sender_email['default_email'];
        }
        else
        {
            $getCurrentUserDetails=DB::table('users')->select('*')->where('role','1')->first();
            $getCurrentUserDetails=json_decode(json_encode($getCurrentUserDetails), true);
            $get_sender_email=$getCurrentUserDetails['email'];
        }
        return $get_sender_email;
    }

    public function admin_name_and_company()
    {
        $get_company_name=DB::table('admin_company')->first();
        $get_company_name=json_decode(json_encode($get_company_name), true);
        $getCurrentUserCompanyAdminName=DB::table('users')->select('*')->where('role','1')->first();
        $getCurrentUserCompanyAdminName=json_decode(json_encode($getCurrentUserCompanyAdminName), true);
        $data=array(
            'company_name'=>$get_company_name['company_name'],
            'admin_name'=>$getCurrentUserCompanyAdminName['first_name']." ".$getCurrentUserCompanyAdminName['last_name'],
            'admin_email'=>$getCurrentUserCompanyAdminName['email'],
            'role'=>'admin'
        );
        return $data;
    }
}
