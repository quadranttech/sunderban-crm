<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use Redirect;
use DB;
use Auth;

class LinkedinController extends Controller
{
    //
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('linkedin')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('linkedin')->user();
        $decodedUser=json_decode(json_encode($user), true);
        $name=$decodedUser['name'];
        $name_array=explode(" ",$name);
        $first_name=$name_array[0];
        $last_name=$name_array[sizeof($name_array)-1];
        $getUser=DB::table('users')->select('*')->where('email','=',$user->getEmail())->get();
        $getUser=json_decode(json_encode($getUser), true);
        if(!empty(array_filter($getUser)))
        {
            $userId=$getUser[0]['id'];
            $getSocialProvider=DB::table('social_providers')->select('*')->where('provider_email','=',$user->getEmail())->get();
            if(empty(array_filter(json_decode(json_encode($getSocialProvider), true))))
            {
                $addSocialProvider=DB::table('social_providers')->insertGetId(['provider_id' => $user->getId() , 'provider_email' => $user->getEmail()]);
            }
            Auth::loginUsingId($userId);
            return redirect('/dashboard');
        }
        else
        {
            $addSocialProvider=DB::table('social_providers')->insertGetId(['provider_id' => $user->getId() , 'provider_email' => $user->getEmail()]);
            date_default_timezone_set(env('TIME_ZONE', 'Asia/calcutta'));
            // $first_name=
            $addUser=DB::table('users')->insertGetId(['email' => $user->getEmail() , 'first_name' => $first_name , 'last_name' => $last_name , 'name' => $name , 'company' => '0' , 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s')]);
            Auth::loginUsingId($addUser);
            return redirect('/dashboard');
        }
    }
}
