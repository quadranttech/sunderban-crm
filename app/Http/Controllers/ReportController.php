<?php

namespace App\Http\Controllers;

require 'vendor/autoload.php';
use Request;
use Auth;
use Illuminate\Routing\UrlGenerator;
use App\Traits\MainTrait;
use App\Traits\PermissionTrait;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Excel;
use Dompdf\Dompdf;
use Dompdf\Options;
use App;
use PDF;
use Jenssegers\Agent\Agent;

class ReportController extends Controller
{
    //
    use MainTrait;
    use PermissionTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->middleware('auth');
    }

    public function report()
    {
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        $getUserPermission['report']['add']="on";
        $getUserPermission['report']['edit']="on";
        $getUserPermission['report']['view']="on";
        $getUserPermission['report']['all']="on";
        $get_user_profile_image=$this->get_profile_image();
        $checkShowTutorial=$this->checkShowTutorial();
        $getCurrentUserDetails=DB::table('users')->select('*')->where('id',Auth::getUser()->id)->first();
        $getCurrentUserDetails=json_decode(json_encode($getCurrentUserDetails), true);
        $getDealSource=$this->get_meta('deal_source',$getCurrentUserDetails['company']);
        $getDealSource=json_decode(json_encode($getDealSource), true);
        $getContact=DB::table('contacts')->orderBy('created_at', 'desc')->get();
        $getContact=json_decode(json_encode($getContact), true);
        $getUser=DB::table('users')->orderBy('created_at', 'desc')->get();
        $getUser=json_decode(json_encode($getUser), true);
        if(!empty($getDealSource) && $getDealSource['meta_value']!="")
        {
            $getDealSource=unserialize($getDealSource['meta_value']);
        }
        $agent = new Agent();
        if($agent->isMobile())
        {
            return view('dashboard/report/report-mobile', ['active' => 'reports' , 'permission' => $getUserPermission , 'profile_image' => $get_user_profile_image , 'show_tutorial' => $checkShowTutorial , 'deal_source' => $getDealSource , 'contact' => $getContact , 'user' => $getUser]);
        }
        return view('dashboard/report/report', ['active' => 'reports' , 'permission' => $getUserPermission , 'profile_image' => $get_user_profile_image , 'show_tutorial' => $checkShowTutorial , 'deal_source' => $getDealSource , 'contact' => $getContact , 'user' => $getUser]);
    }

    public function generateReport()
    {
        // $currentDate = \Carbon\Carbon::now();
        // $agoDate = $currentDate->subDays($currentDate->dayOfWeek)->subWeek();
        // echo $currentDate;
        // echo "</br>".$agoDate;
  //    $tomorrow = Carbon::now()->addDay();
        // $lastWeek = Carbon::now()->subWeek();
        // echo "tomorrow = ".$tomorrow."</br> lastWeek = ".$lastWeek."</br> weekStart = ".Carbon::now()->startOfWeek()."</br> weekEnd = ".Carbon::now()->endOfWeek()."</br> currentMonth = ".Carbon::now()->startOfMonth()."</br> endMonth = ".Carbon::now()->endOfMonth();
        // $start = new Carbon('first day of last month');
        // $end = new Carbon('last day of last month');
        // echo "</br> first day of previous month = ".$start;
        // echo "</br> last day of previous month = ".$end;
        $filter=Input::get('filter');
        $table=Input::get('table');
        $query=DB::table('deals')->select('deals.*');
        if($filter[3]!="")
        {
            $query->join('invoices', 'invoices.deal_id', '=', 'deals.id');
            if($filter[3]=="draft" || $filter[3]=="send")
            {
                $query->where('invoices.status',$filter[3]);
            }
            if($filter[3]!="draft" && $filter[3]!="send")
            {
                $query->where('invoices.payment_status',$filter[3]);
            }
            if(!empty($filter[5]))
            {
                $query->whereDate('invoices.created_at','>=',$filter[5][0]);
                if($filter[5][1]!="")
                {
                    $query->whereDate('invoices.created_at','<=',$filter[5][1]);
                }
            }
        }
        if($filter[4]!="")
        {
            $query->join('estimates', 'estimates.deal_id', '=', 'deals.id');
            $query->where('estimates.status',$filter[4]);
            if(!empty($filter[5]))
            {
                $query->whereDate('estimates.created_at','>=',$filter[5][0]);
                if($filter[5][1]!="")
                {
                    $query->whereDate('estimates.created_at','<=',$filter[5][1]);
                }
            }
        }
        foreach($filter as $key=>$value)
        {
            if($key<3 && $value!="")
            {
                $query->where($table[$key],$value);
            }
        }
        if($filter[3]=="" && $filter[4]=="")
        {
            if(!empty($filter[5]))
            {
                $query->whereDate('deals.created_at','>=',$filter[5][0]);
                if($filter[5][1]!="")
                {
                    $query->whereDate('deals.created_at','<=',$filter[5][1]);
                }
            }
        }
        if($filter[3]!="")
        {
            
        }
        $getReport=$query->get();
        $getReport=json_decode(json_encode($getReport), true);
        $getUser=$this->getUsers();
        $getDealStatus=DB::table('status')->get();
        $contact=DB::table('contacts')->get();
        $userdupe=array();
        $dealArray=array();
        foreach ($getReport as $index=>$t) {
            if (isset($userdupe[$t["id"]])) {
                unset($getReport[$index]);
                continue;
            }
            $userdupe[$t["id"]]=$getReport[$index];
            $dealArray[]=$getReport[$index];
        }
        $getReport=$dealArray;
        return response()->json(['response' => $dealArray , 'owner' => $getUser , 'status' => $getDealStatus , 'contact' => $contact]);
        // $getReport=json_decode(json_encode($getReport), true);
        // echo "<pre>";
        // print_r($getReport);
        // echo "</pre>";
    }

    public function exportReportXls()
    {
        Excel::create('New file', function($excel) {

            $excel->sheet('New sheet', function($sheet) {
                $temp_array=session('exportData');
                $sheet->loadView('folder',array('html' => $temp_array));

            });

        })->export('xls');
        session('exportData');
    }

    public function exportReportPdf()
    {
        $data=session('exportData');
        $pdf = App::make('dompdf.wrapper');
        $pdf = PDF::loadView('templates.pulse-mailer.export-pdf',array('data' => $data));
        return $pdf->download();
    }

    public function createReport()
    {
        session(['exportData' => Input::get('exportFilterDataArray')]);
        return response()->json(['response' => Input::get('exportFilterDataArray')]);
    }
}
