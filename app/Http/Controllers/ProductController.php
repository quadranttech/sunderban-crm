<?php

namespace App\Http\Controllers;

use Request;
use DB;
use Illuminate\Routing\UrlGenerator;
use App\Product;
use Auth;

class ProductController extends BaseController
{
    //
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->middleware('auth');
        $this->productTable = new Product();
    }

    public function productList()
    {
    	//GET PRODUCT LIST
    	$getProductList = $this->productTable->getProductList();
    	return view('dashboard/product/product-list', ['product_list' => $getProductList , 'active' => 'products'] , $this->createHeader());
    }

    public function createProduct()
    {
    	return view('dashboard/product/create-product', ['active' => 'products'] , $this->createHeader());
    }

    public function saveProduct()
    {
    	$data = array();
    	$data = $_REQUEST;
        $product_meta_array = array();
        $product_meta = "";
        $type = "add-on";
        if($data['type'] == "main")
        {
            $product_meta_array[$data['type']] = array('description' => $data['description'] , 'room_fare' => $data['price'] , 'per_person_price' => $data['per_person_price']);
            $product_meta = serialize($product_meta_array);
            $type = "main";
        }
        $data['product_meta'] = $product_meta;
    	$data['created_at'] = date('Y-m-d H:i:s');
    	$data['updated_at'] = date('Y-m-d H:i:s');
        // unset($data['type']);
    	$data = array_slice($data, 1);
        if(array_key_exists('XSRF-TOKEN',$data))
        {
            unset($data['XSRF-TOKEN']);
        }
        if(array_key_exists('laravel_session',$data))
        {
            unset($data['laravel_session']);
        }
    	$saveProduct = $this->productTable->saveProduct($data);
    	return redirect('/products');
    }

    public function editProduct($id)
    {
    	if(Request::isMethod('post'))
    	{
    		$data = $_REQUEST;
            $product_meta = "";
            if($_REQUEST['type'] == "main")
            {
                $getProductById = $this->productTable->getProductById($data['id']);
                $getProductMetaArray = unserialize($getProductById['product_meta']);
                //CHECK IF TYPE IS AVAILABLE
                $getProductMetaArray[$data['type']]['description'] = $data['description'];
                $getProductMetaArray[$data['type']]['room_fare'] = $data['price'];
                $getProductMetaArray[$data['type']]['per_person_price'] = $data['per_person_price'];
                $product_meta = serialize($getProductMetaArray);
            }
            $data['product_meta'] = $product_meta;
    		unset($data['type']);
            $data = array_slice($data, 1);
            if(array_key_exists('XSRF-TOKEN',$data))
            {
                unset($data['XSRF-TOKEN']);
            }
            if(array_key_exists('laravel_session',$data))
            {
                unset($data['laravel_session']);
            }
    		$updateProduct = $this->productTable->updateProduct($data);
    		return redirect('/edit-product/'.$data["id"]);
    	}
    	else
    	{
    		$getProductById = $this->productTable->getProductById($id);
            $getProductMetaArray = unserialize($getProductById['product_meta']);
    		return view('dashboard/product/edit-product', ['active' => 'products' , 'product' => $getProductById , 'product_meta_array' => $getProductMetaArray] , $this->createHeader());
    	}
    }

    public function getApi()
    {
        return response()->json(['response' => 'success']);
    }

    // public function saveEditedProduct($id)
    // {
    // 	$getProductById = $this->productTable->getProductById($id);
    // 	return view('dashboard/product/edit-product', ['active' => 'products' , 'product' => $getProductById] , $this->createHeader());
    // }
}
