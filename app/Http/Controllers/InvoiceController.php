<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Routing\UrlGenerator;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use Dompdf\Dompdf;
use PDF;
use Dompdf\Options;
use Mail;
use Jenssegers\Agent\Agent;
use App\Traits\MainTrait;
use App\Notification;
use App;
use DateTime;
use App\Traits\PermissionTrait;
use App\Product;

class InvoiceController extends Controller
{
    //
    use MainTrait;
    use PermissionTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;  //GET BASE URL
        $this->middleware('auth');  //REDIRECTED TO LOGIN IF NOT AUTHENTICATED
        $this->currency_symbol = [
            'USD'=> '$', // US Dollar
            'EUR'=> '€', // Euro
            'CRC'=> '₡', // Costa Rican Colón
            'GBP'=> '£', // British Pound Sterling
            'ILS'=> '₪', // Israeli New Sheqel
            'INR'=> '₹', // Indian Rupee
            'JPY'=> '¥', // Japanese Yen
            'KRW'=> '₩', // South Korean Won
            'NGN'=> '₦', // Nigerian Naira
            'PHP'=> '₱', // Philippine Peso
            'PLN'=> 'zł', // Polish Zloty
            'PYG'=> '₲', // Paraguayan Guarani
            'THB'=> '฿', // Thai Baht
            'UAH'=> '₴', // Ukrainian Hryvnia
            'VND'=> '₫' // Vietnamese Dong
        ];
        $this->productTable = new Product();
    }

    public function createInvoice()
    {
    	$getContact=DB::table('contacts')->select('*')->orderBy('title', 'asc')->orderBy('first_name', 'asc')->orderBy('last_name', 'asc')->get(); //GET CONTACTS
    	$getContact=json_decode(json_encode($getContact), true);
    	$getUser=DB::table('users')->select('*')->get();   //GET USERS
    	$getUser=json_decode(json_encode($getUser), true);
        $getCurrentUserCompany=DB::table('admin_company')->join('users', 'users.company', '=', 'admin_company.id')->select('*')->where('users.id', '=', Auth::getUser()->id)->first();  //GET CURRENT USER COMPANY
        $getCurrentUserCompany=json_decode(json_encode($getCurrentUserCompany), true);
        $company_id=$getCurrentUserCompany['company'];
        $getInvoiceStartWith=DB::table('settings')->select('*')->where('meta_key','invoice_start')->where('company_id',$company_id)->first();   //GET INVOICE START WITH BY COMPANY ID
        $getInvoiceStartWith=json_decode(json_encode($getInvoiceStartWith), true);
        $getFirstInvoice=DB::table('invoices')->select('*')->where('company_id',$company_id)->get();    //GET COMPANY FIRST INVOICE
        $getFirstInvoice=json_decode(json_encode($getFirstInvoice), true);
        $getCompany=DB::table('company')->select('*')->get();   //GET CONTACT COMPANY
        $getCompany=json_decode(json_encode($getCompany), true);
        $getDefaultPaymentTerms=DB::table('settings')->select('meta_value')->where('company_id',$company_id)->where('meta_key','default_payment_term')->first();    //GET DEFAULT COMPANY DEFAULT PAYMENT TERM
        $getDefaultPaymentTerms=json_decode(json_encode($getDefaultPaymentTerms), true);
        if(!empty($getDefaultPaymentTerms))
        {
            $getDefaultPaymentTerms=$getDefaultPaymentTerms['meta_value'];
        }
        else
        {
            $getDefaultPaymentTerms="net-15";
        }
        $getTax=DB::table('taxs')->select('*')->where('company_id',$company_id)->get(); //GET COMPANY TAXS
        $getTax=json_decode(json_encode($getTax), true);
        $getDiscount=DB::table('discounts')->select('*')->where('company_id',$company_id)->get();
        $getDiscount=json_decode(json_encode($getDiscount), true);  //GET COMPANY DISCOUNTS
        //GENERATE INVOICE STAT WITH
        if(!empty($getInvoiceStartWith) && !empty($getFirstInvoice))
        {
            $lastInvoiceNo=explode("-",$getFirstInvoice[sizeof($getFirstInvoice)-1]['inv_no']);
            $lastInvoiceNo=sizeof($getFirstInvoice)+1;
            $generateInvoiceNo=$getInvoiceStartWith['meta_value'].'-'.sprintf('%04d', $lastInvoiceNo);
        }
        if(!empty($getInvoiceStartWith) && empty($getFirstInvoice))
        {
            $generateInvoiceNo=$getInvoiceStartWith['meta_value'].'-'.sprintf('%04d', 1);
        }
        if(empty($getInvoiceStartWith) && !empty($getFirstInvoice))
        {
            $lastInvoiceNo=explode("-",$getFirstInvoice[sizeof($getFirstInvoice)-1]['inv_no']);
            $lastInvoiceNo=sizeof($getFirstInvoice)+1;
            $generateInvoiceNo="INV-".sprintf('%04d', $lastInvoiceNo);
        }
        else if(empty($getInvoiceStartWith) && empty($getFirstInvoice))
        {
            $generateInvoiceNo="INV-".sprintf('%04d', 1);
        }
        $notification_table=new Notification;   //CREATE NOTIFICATION CLASS OBJECT
        $getNotification=$notification_table->getNotification(Auth::getUser()->id); //CALL GET NOTIFICATION METHOD
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);
        }
        $get_user_profile_image=$this->get_profile_image();
        $get_terms_and_conditions=$this->get_meta('terms_and_conditions',$company_id);
        $get_terms_and_conditions=json_decode(json_encode($get_terms_and_conditions), true);
        $getCompanyCurrency=array();
        $getCompanyCurrency=$this->get_company_currency();
        if(!empty($getCompanyCurrency))
        {
            $getCompanyCurrency=unserialize($getCompanyCurrency['currency_list']);
        }
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getDealStage=DB::table('stages')->select('*')->get();
        $getDealStage=json_decode(json_encode($getDealStage), true);
        $getDeal=DB::table('deals')->select('*')->get();
        $getDeal=json_decode(json_encode($getDeal), true);
        $getValidDealStage=$this->searchForDealStageId($getDealStage,$getDeal);
        $getDealStage=$getValidDealStage;
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        //GET PRODUCT
        $getProduct=$this->productTable->getProductList();
        $agent = new Agent();   //GET USER AGENT
        if(Request::input('inv_no')!="")    //IF EST NO IS NOT NULL THEN REDIRECT TO EDIT INVOICE
        {
            $getInvoice=DB::table('invoices')->select('*')->where('inv_no',Request::input('inv_no'))->first();
            $getInvoice=json_decode(json_encode($getInvoice), true);
            $getInvoiceItems=array();
            $getInvoiceItems=DB::table('invoice_items')->select('*')->where('inv_id',$getInvoice['id'])->get();
            $getInvoiceItems=json_decode(json_encode($getInvoiceItems), true);
            $getInvoiceMeta=DB::table('invoice_meta')->select('*')->where('inv_id',$getInvoice['id'])->first();
            $getInvoiceMeta=json_decode(json_encode($getInvoiceMeta), true);
            if($agent->isMobile())
            {
                return view('dashboard/invoice/edit-invoice-mobile', ['contacts' => $getContact], ['users' => $getUser , 'inv_no' => $generateInvoiceNo , 'company' => $getCompany , 'default_payment_term' => $getDefaultPaymentTerms , 'invoice' => $getInvoice , 'invoice_items' => $getInvoiceItems , 'invoice_meta' => $getInvoiceMeta , 'user_company' => $getCurrentUserCompany , 'tax' => $getTax , 'discount' => $getDiscount , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'invoice' , 'terms_and_conditions' => $get_terms_and_conditions , 'currency' => $getCompanyCurrency , 'show_tutorial' => $checkShowTutorial , 'deal_stage' => $getDealStage , 'deal' => $getDeal , 'product' => $getProduct , 'permission' => $getUserPermission]);
            }
            return view('dashboard/invoice/edit-invoice', ['contacts' => $getContact], ['users' => $getUser , 'inv_no' => $generateInvoiceNo , 'company' => $getCompany , 'default_payment_term' => $getDefaultPaymentTerms , 'invoice' => $getInvoice , 'invoice_items' => $getInvoiceItems , 'invoice_meta' => $getInvoiceMeta , 'user_company' => $getCurrentUserCompany , 'tax' => $getTax , 'discount' => $getDiscount , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'invoice' , 'terms_and_conditions' => $get_terms_and_conditions , 'currency' => $getCompanyCurrency , 'show_tutorial' => $checkShowTutorial , 'deal_stage' => $getDealStage , 'deal' => $getDeal , 'product' => $getProduct , 'permission' => $getUserPermission]);
        }
        if($agent->isMobile())
        {
            return view('dashboard/invoice/invoice-mobile', ['contacts' => $getContact], ['users' => $getUser , 'inv_no' => $generateInvoiceNo , 'company' => $getCompany , 'default_payment_term' => $getDefaultPaymentTerms , 'user_company' => $getCurrentUserCompany , 'tax' => $getTax , 'discount' => $getDiscount , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'invoice' , 'terms_and_conditions' => $get_terms_and_conditions , 'currency' => $getCompanyCurrency , 'show_tutorial' => $checkShowTutorial , 'deal_stage' => $getDealStage , 'deal' => $getDeal , 'product' => $getProduct , 'permission' => $getUserPermission]);
        }
    	return view('dashboard/invoice/invoice', ['contacts' => $getContact], ['users' => $getUser , 'inv_no' => $generateInvoiceNo , 'company' => $getCompany , 'default_payment_term' => $getDefaultPaymentTerms , 'user_company' => $getCurrentUserCompany , 'tax' => $getTax , 'discount' => $getDiscount , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'invoice' , 'terms_and_conditions' => $get_terms_and_conditions , 'currency' => $getCompanyCurrency , 'show_tutorial' => $checkShowTutorial , 'deal_stage' => $getDealStage , 'deal' => $getDeal , 'product' => $getProduct , 'permission' => $getUserPermission]);
    }

    public function saveInvoice()
    {
        $sendDate="";
        $getCurrentUserDetails=DB::table('admin_company')->join('users', 'users.company', '=', 'admin_company.id')->select('*')->where('users.id', '=', Auth::getUser()->id)->first();  //GET CURRENT USER COMPANY
        $getCurrentUserDetails=json_decode(json_encode($getCurrentUserDetails), true);
        $company_id=$getCurrentUserDetails['company'];
        if(Input::get('status')=="send")    //IF STATUS IS SEND THEN CHECK SMTP
        {
            $getSmtpUsername=$this->get_meta('smtp_username',$company_id);
            $getSmtpUsername=json_decode(json_encode($getSmtpUsername), true);
            $getSmtpPassword=$this->get_meta('smtp_password',$company_id);
            $getSmtpPassword=json_decode(json_encode($getSmtpPassword), true);
            $getSmtpHost=$this->get_meta('smtp_host',$company_id);
            $getSmtpHost=json_decode(json_encode($getSmtpHost), true);
            if($getSmtpUsername['meta_value']=="" || $getSmtpPassword['meta_value']=="" || $getSmtpHost['meta_value']=="")
            {
                return response()->json(['response' => 'failed' , 'message' => 'smtp_not_found']);
            }
        }
        if(Input::get('id')=="" && Input::get('purpose')!="edit")
        {
            $getInvoiceByInvNo=DB::table('invoices')->select('*')->where('inv_no',Input::get('invoice_no'))->first();
            $getInvoiceItemTotal=json_decode(json_encode($getInvoiceByInvNo), true);
            if(!empty($getInvoiceByInvNo))
            {
                return response()->json(['response' => 'failed' , 'message' => 'inv_no_already_exist']);
            }
        }
        if(Input::get('contact_type')=="company")
        {
            $contact=DB::table('company')->select('default_contact')->where('id',Input::get('invoice_contact'))->first();   //IF CONTACT TYPE COMPANY THEN GET DEFAULT CONTACT OF THAT COMPANY
            $contact=json_decode(json_encode($contact), true);
            $contact=$contact['default_contact'];
            if($contact=="0")   //IF COMPANY DOES NOT HAVE ANY DEFAULT CONTACT
            {
                return response()->json(['response' => 'failed' , 'message' => 'company_does_not_have_contact']);
            }
        }
        else
        {
            $contact=Input::get('invoice_contact'); //ELSE GET CONTACT
        }
        $due_date=Input::get('due_date');   //GET DUE DATE
        $getDefaultPaymentTerms=Input::get('default_payment_term'); //GET DEFAULT PAYMENT TERM
        if(Input::get('due_date')=="")  //IF DUE DATE IS NULL
        {
            $getDefaultPaymentTerms=DB::table('settings')->select('meta_value')->where('company_id',$company_id)->where('meta_key','default_payment_term')->first();    //GET COMPANY DEFAULT PAYMENT TERM FROM SETTINGS
            $getDefaultPaymentTerms=json_decode(json_encode($getDefaultPaymentTerms), true);
            if(!empty($getDefaultPaymentTerms))
            {
                $getDefaultPaymentTerms=$getDefaultPaymentTerms['meta_value'];  //IF COMPANY DEFAULT PAYMENT TERM IS NOT NULL
            }
            else
            {
                $getDefaultPaymentTerms="net-15";   //ELSE
            }
            $terms_and_conditions=$this->get_meta('terms_and_conditions',$company_id);  //GET COMPANY INVOICE TERMS AND CONDITIONS
            $terms_and_conditions=json_decode(json_encode($terms_and_conditions), true);
            if(!empty($terms_and_conditions))
            {
                $terms_and_conditions=$terms_and_conditions['meta_value'];
            }
            else
            {
                $terms_and_conditions='';
            }
            $due_date=explode("-",$getDefaultPaymentTerms)[1]-1;
            $due_date=date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + $due_date, date('Y')));
        }
        // $deal_id=Input::get('deal_id');
        // if($deal_id!="")
        // {
        //     $deal_id="0";
        // }
        if(Input::get('id')!="" && Input::get('purpose')=="edit")   //IF ESTIMATE ID IS PRESENT AND PURPOSE IS EDIT THEN UPDATE INVOICE
        {
            $updateInvoice=DB::table('invoices')->where('id',Input::get('id'))->update(['company_id' =>$company_id, 'deal_id' => Input::get('deal_id'), 'order_no' => Input::get('order_no'), 'contact' => $contact, 'contact_type' => Input::get('contact_type'), 'inv_date' => Input::get('invoice_date'), 'due_date' => Input::get('due_date'), 'default_payment_term' => Input::get('default_payment_term'), 'sales_person' => Input::get('sales_person'), 'status' => 'draft', 'currency' => Input::get('currency')[0], 'send_date' => $sendDate, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            $deleteInvoiceItems=DB::table('invoice_items')->where('inv_id',Input::get('id'))->delete();
            $deleteInvoiceMeta=DB::table('invoice_meta')->where('inv_id',Input::get('id'))->delete();
            $createInvoice=Input::get('id');
        }
        else
        {
            $link=$this->generateRandomString();
            $sales_person=Input::get('sales_person');
            if($sales_person=="select_user")
            {
                $sales_person=Auth::getUser()->id;  //IF SALES PERSON IS NULL THEN CURRENT USER IS SALES PERSON
            }
            $createInvoice=DB::table('invoices')->insertGetId(['inv_no' => Input::get('invoice_no'), 'invoice_title' => Input::get('invoice_title'), 'company_id' =>$company_id, 'deal_id' => Input::get('deal_id'), 'order_no' => Input::get('order_no'), 'contact' => $contact, 'contact_type' => Input::get('contact_type'), 'inv_date' => Input::get('invoice_date'), 'due_date' => $due_date, 'default_payment_term' => $getDefaultPaymentTerms, 'sales_person' => $sales_person, 'status' => 'draft', 'currency' => Input::get('currency')[0], 'send_date' => $sendDate, 'link' => $link, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]); //ELSE CREATE INVOICE
        }
        $log_title=(Input::get('status')=='draft' ? 'saved to draft' : 'sent to customer');
        $createInvoiceActivityLog=$this->createInvoiceActivityLog($createInvoice,$log_title);
    	$total=0;
        $items_array=array();
    	foreach(Input::get('item_details_array') as $key=>$value)  //CREATE ITEM DETAILS ARRAY
    	{
            $invoiceItemDetails=$value;
            $invoiceItemRate=Input::get('rate_array')[$key];
            $invoiceItemTotal=Input::get('quantity_array')[$key]*Input::get('rate_array')[$key];
            if($invoiceItemDetails="")
            {
                $invoiceItemDetails="";
            }
            if($invoiceItemRate=="" || !is_numeric($invoiceItemRate))
            {
                $invoiceItemRate=0.00;
            }
            if($invoiceItemTotal=="" || !is_numeric($invoiceItemTotal))
            {
                $invoiceItemTotal=0.00;
            }
    		$insertInvoiceItems=DB::table('invoice_items')->insertGetId(['inv_id' => $createInvoice, 'item_details' => $value, 'rate' => $invoiceItemRate, 'quantity' => Input::get('quantity_array')[$key], 'quantity_type' => Input::get('quantity_type'), 'currency' => Input::get('currency')[$key], 'total' => $invoiceItemTotal, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
    		$total=$total+(Input::get('quantity_array')[$key]*Input::get('rate_array')[$key]);
            $temp=array();
            $temp['item_details']=$invoiceItemDetails;
            $temp['rate']=$invoiceItemRate;
            $temp['quantity']=Input::get('quantity_array')[$key];
            $temp['quantity_type']=Input::get('quantity_type');
            $temp['currency']=Input::get('currency')[$key];
            $temp['total']=$invoiceItemTotal;
            array_push($items_array,$temp);
    	}
    	$inv_total=$total+Input::get('shipping_cost')-abs(Input::get('adjustment'))-abs(Input::get('discount'));
    	$inv_total=$inv_total+Input::get('tax');   //INVOICE TOTAL
        $updateInvoicePaymentStatus=DB::table('invoices')->where('id',$createInvoice)->update(['paid' => '0.00', 'due' => $inv_total, 'payment_status' => 'unpaid']);
        if(Input::get('part_pay_type')=="no_part_pay")
        {
            $due=0; //IF PART PAY IS NOT CHECKED THEN DUE IS 0
        }
        else
        {
            if(Input::get('part_pay_type')=="fixed")
            {
                $due=$inv_total-Input::get('part_pay_value');   //IF PART PAY IS CHECKED AND VALUE IS FIXED
            }
            else
            {
                $due=(($inv_total)-((Input::get('part_pay_value') / 100) * $inv_total));    //IF PART PAY IS CHECKED AND VALUE IS PERCENTAGE
            }
        }
        $terms_and_conditions=Input::get('terms_and_conditions');   //GET TERMS AND CONDITIONS
        if($terms_and_conditions=="")   //IF TERMS AND CONDITIONS IS NULL
        {
            $terms_and_conditions=$this->get_meta('terms_and_conditions',$company_id);
            $terms_and_conditions=json_decode(json_encode($terms_and_conditions), true);
            $terms_and_conditions=$terms_and_conditions['meta_value'];
        }
    	$insertInvoiceMeta=DB::table('invoice_meta')->insertGetId(['inv_id' => $createInvoice,'attachment' => '', 'tax' => Input::get('tax'), 'tax_id' => Input::get('tax_id'), 'discount' => Input::get('discount'), 'discount_id' => Input::get('discount_id'), 'shipping_cost' => Input::get('shipping_cost'), 'inv_total' => $inv_total, 'adjustment' => Input::get('adjustment'), 'part_pay' => Input::get('part_pay_type'), 'part_pay_value' => Input::get('part_pay_value'), 'due' => $due, 'customer_note' => Input::get('customer_note'), 'admin_note' => Input::get('admin_note'), 'terms_and_conditions' => $terms_and_conditions , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]); //CREATE INVOICE META
        $getContact=DB::table('contacts')->select('*')->where('id',$contact)->first();  //GET CONTACT
        $getContactDetails=json_decode(json_encode($getContact), true);
        $getContactAddressDetails=DB::table('contacts_address')->select('*')->where('contact_id',$contact)->first();    //GET CONTACT ADDRESS
        $getContactAddressDetails=json_decode(json_encode($getContactAddressDetails), true);
        $portalName=env('PORTAL_NAME');
        $getLabelName=$this->getLabelName();
        $base_url=$this->url->to('/');
        $content=file_get_contents($base_url.'/resources/views/templates/pulse-mailer/invoice-preview-template.php');
    	return response()->json(['response' => $inv_total, 'items_array' => $items_array, 'admin_company' => $getCurrentUserDetails, 'contact_details' => $getContactDetails, 'terms_and_conditions' => $terms_and_conditions, 'inv_id' => $createInvoice, 'content' => $content, 'invoice_title' => Input::get('invoice_title'), 'inv_no' => Input::get('invoice_no'), 'due_date' => $due_date, 'discount' => abs(Input::get('discount')), 'tax' => Input::get('tax'), 'portalName' => $portalName, 'quantity_type' => Input::get('quantity_type'), 'label_name' => $getLabelName, 'currency_symbol' => $this->getCurrencySymbol('en',Input::get('currency')[0]), 'contact_address' => $getContactAddressDetails]);
    }

    public function invoiceAttachment()
    {
    	$destinationPath = base_path('uploads/invoice-attachment'); // upload path
        $file_name=Input::file('invoice_attachment')->getClientOriginalName();
        $file_name = pathinfo($file_name, PATHINFO_FILENAME);
        $extension = Input::file('invoice_attachment')->getClientOriginalExtension(); // getting image extension
        $fileName = $file_name.'-'.rand(11111,99999).'.'.$extension; // renameing image
        Input::file('invoice_attachment')->move($destinationPath, $fileName); // uploading file to given path
        return response()->json(['response' => 'success','file_name' => $fileName]);
    }

    public function createInvoiceActivityLog($inv_id,$log_title)
    {
        $log_title='Invoice '.$log_title;   //LOG TITLE
        $createInvoiceActivityLog=DB::table('invoice_activity_logs')->insertGetId(['inv_id' => $inv_id, 'log_activity' => $log_title, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);   //CREATE ACTIVITY LOG
        return $createInvoiceActivityLog;
    }

    public function showInvoices()
    {
        $getInvoices=DB::table('invoices')->select('*')->orderBy('created_at', 'desc')->get(); //GET ALL INVOICES
        $getInvoices=json_decode(json_encode($getInvoices), true);
        $contacts_array=array();
        $invoices=array();
        foreach($getInvoices as $key=>$value)
        {
            $temp=array();
            $temp['id']=$value['id'];   //INVOICE INFORMATION
            $temp['inv_no']=$value['inv_no'];
            if($value['contact_type']=="company")
            {
                $company=DB::table('company')->join('contacts', 'contacts.company', '=', 'company.id')->select('*')->where('contacts.id', '=', $value['contact'])->first();
                $contact=$company['company_name'];
            }
            else
            {
                $contact_details=DB::table('contacts')->select('*')->where('id',$value['contact'])->first();
                $contact_details=json_decode(json_encode($contact_details), true);
                $contact=$contact_details['first_name']." ".$contact_details['last_name'];
            }
            $temp['contact']=$contact;
            $temp['proposal_title']="";
            $temp['date_created']=$value['created_at'];
            $get_value=DB::table('invoice_meta')->select('*')->where('inv_id',$value['id'])->first();
            $get_value=json_decode(json_encode($get_value), true);
            $temp['value']=sprintf("%.2f", $get_value['inv_total']);
            $temp['status']=$value['status'];
            $temp['currency']=$value['currency'];
            $temp['currency_symbol']=$this->getCurrencySymbol('en',$value['currency']);
            array_push($invoices,$temp);
        }
        $notification_table=new Notification;
        $getNotification=$notification_table->getNotification(Auth::getUser()->id);
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);
        }
        $get_user_profile_image=$this->get_profile_image();
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        $agent = new Agent();   //GET USER AGENT
        if($agent->isMobile())
        {
            return view('dashboard/invoice/invoice-list-mobile', ['invoices' => $invoices , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'invoice' , 'show_tutorial' => $checkShowTutorial , 'permission' => $getUserPermission]);
        }
        return view('dashboard/invoice/invoice-list', ['invoices' => $invoices , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference , 'profile_image' => $get_user_profile_image , 'active' => 'invoice' , 'show_tutorial' => $checkShowTutorial , 'permission' => $getUserPermission]);
    }

    public function sendInvoiceMail($contact,$items_array,$inv_total,$shipping_cost,$discount,$tax,$adjustment,$total,$createInvoice,$due_date,$terms_and_conditions,$link,$invoice_title,$logo,$message_to_customer,$payment_status)
    {
        $getContactDetails=DB::table('contacts')->select('*')->where('id',$contact)->first();   //GET CONTACT
        $getContactAddressDetails=DB::table('contacts_address')->join('contacts', 'contacts.id', '=', 'contacts_address.contact_id')->select('*')->where('contacts.id', '=',$contact)->first(); //GET CONTACT ADDRESS
        $getContactAddressDetails=json_decode(json_encode($getContactAddressDetails), true);
        $contact_address="";
        $contact_country="";
        $contact_state="";
        $contact_post_code="";
        if($getContactAddressDetails!="")
        {
            $contact_address=$getContactAddressDetails['street_address'];
            $contact_country=$getContactAddressDetails['country'];
            $contact_state=$getContactAddressDetails['state'];
            $contact_post_code=$getContactAddressDetails['post_code'];
        }
        $getContactDetails=DB::table('contacts')->select('*')->where('id',$contact)->first();
        $getContactDetails=json_decode(json_encode($getContactDetails), true);
        $contact_name=$getContactDetails['first_name']." ".$getContactDetails['last_name']; //GET CONTACT INFORMATION
        $getAdminCompanyDetails=DB::table('admin_company')->select('*')->first();   //GET ADMIN COMPANY
        $getAdminCompanyDetails=json_decode(json_encode($getAdminCompanyDetails), true);
        $company_name=$getAdminCompanyDetails['company_name'];  //GET ADMIN COMPANY INFORMATION
        $company_address=$getAdminCompanyDetails['address'];
        $company_country=$getAdminCompanyDetails['country'];
        $company_state=$getAdminCompanyDetails['state'];
        $company_post_code=$getAdminCompanyDetails['post_code'];
        $sender_name=$getAdminCompanyDetails['sender_name'];
        $logo=$getAdminCompanyDetails['logo'];
        if($logo=="" || $logo==null)
        {
            $logo=$this->url->to('/').'/assets/img/logo.png';
        }
        else
        {
            $logo=$this->url->to('/').'/uploads/company-logo/'.$getAdminCompanyDetails['logo'];
        }
        if($sender_name=="")    //EMAIL SENDER NAME
        {
            $getAdminName=DB::table('users')->select('*')->where('role','1')->first();
            $getAdminName=json_decode(json_encode($getAdminName), true);
            $sender_name=$getAdminName['first_name'];
        }
        $sender_email=$getAdminCompanyDetails['default_email'];
        if($sender_email=="" || $getAdminCompanyDetails['marked_as_default']=="no")
        {
            $getAdminName=DB::table('users')->select('*')->where('role','1')->first();
            $getAdminName=json_decode(json_encode($getAdminName), true);
            $sender_email=$getAdminName['email'];
        }
        if(!empty(unserialize($getContactDetails['email'])['work']))
        {
            $email=unserialize($getContactDetails['email'])['work'][0];
        }
        else
        {
            $email=unserialize($getContactDetails['email'])['home'][0];
        }
        $company_id=$getAdminCompanyDetails['id'];
        $getDefaultPaymentMethod=$this->get_meta('default_payment_gateway',$company_id);
        $getDefaultPaymentMethod=json_decode(json_encode($getDefaultPaymentMethod), true);
        if($getDefaultPaymentMethod['meta_value']!="")
        {
            $getPaymentGatewayCredentials=DB::table('payment_gateways')->select('*')->where('company_id',$company_id)->where('payment_gateway',$getDefaultPaymentMethod['meta_value'])->first();
            $getPaymentGatewayCredentials=json_decode(json_encode($getPaymentGatewayCredentials), true);
            if($getPaymentGatewayCredentials['credentials']!="")
            {
                $payment="on";
            }
            else
            {
                $payment="off";
            }
        }
        else
        {
            $payment="off";
        }
        $view_on_browser_link=$this->url->to('/').'/view-invoice-on-browser/'.$link;
        $getInvoiceNo=DB::table('invoices')->select('*')->where('id',$createInvoice)->first();
        $getInvoiceNo=json_decode(json_encode($getInvoiceNo), true);
        $getInvoiceNo=$getInvoiceNo['inv_no'];
        $send=$this->send_invoice_mail($email,$items_array,$inv_total,$discount,$tax,$adjustment,$total,$contact_address,$contact_country,$contact_state,$contact_post_code,$contact_name,$company_name,$company_address,$company_country,$company_state,$company_post_code,$sender_name,$sender_email,$createInvoice,$due_date,$terms_and_conditions,$link,$invoice_title,$logo,$message_to_customer,$payment_status,$payment,$items_array[0]['quantity_type'],$view_on_browser_link,$getInvoiceNo); //SEND INVOICE EMAIL
        if($send==1)
        {
            return true;
        }
        else
        {
            return false;
        }
        // return $email;
    }

    public function send_invoice_mail($email,$items_array,$inv_total,$discount,$tax,$adjustment,$total,$contact_address,$contact_country,$contact_state,$contact_post_code,$contact_name,$company_name,$company_address,$company_country,$company_state,$company_post_code,$sender_name,$sender_email,$createInvoice,$due_date,$terms_and_conditions,$link,$invoice_title,$logo,$message_to_customer,$payment_status,$payment,$quantity_type,$view_on_browser_link,$getInvoiceNo){
        $payment_link=$this->url->to('/').'/invoice/payment/'.$link;
        $pdf_link=$this->url->to('/').'/show-invoice-pdf/'.$link;   //INVOICE PDF LINK
        $currency_symbol=$this->getCurrencySymbol('en',$items_array[0]['currency']);
        if($quantity_type=="quantity")
        {
            $quantity_type="Quantity";
        }
        else if($quantity_type=="hr")
        {
            $quantity_type="hr";
        }
        else if($quantity_type=="item")
        {
            $quantity_type="Item";
        }
        else
        {
            $quantity_type="hr + Item";
        }
      $data = array('name'=>$sender_name,'items_array'=>$items_array,'inv_total'=>$inv_total,'discount'=>$discount,'tax'=>$tax,'adjustment'=>$adjustment,'total'=>$total,'contact_address'=>$contact_address,'contact_country'=>$contact_country,'contact_state'=>$contact_state,'contact_post_code'=>$contact_post_code,'contact_name'=>$contact_name,'company_name'=>$company_name,'company_address'=>$company_address,'company_country'=>$company_country,'company_state'=>$company_state,'company_post_code'=>$company_post_code,'id'=>$getInvoiceNo,'due_date' => $due_date,'terms_and_conditions' => $terms_and_conditions,'pdf_link'=>$pdf_link,'logo'=>$logo,'message_to_customer'=>$message_to_customer,'payment'=>$payment,'currency_symbol'=>$currency_symbol,'payment_link'=>$payment_link,'view_on_browser_link'=>$view_on_browser_link,'quantity_type'=>$quantity_type);
      if($payment_status=="unpaid")
      {
        try
        {
            Mail::send('templates/pulse-mailer/invoice-template', $data, function($message) use ($email,$sender_name,$sender_email,$invoice_title) {
             $message->to($email, 'Tutorials Point')->subject
                ($invoice_title);
             $message->from($sender_email,$sender_name);
            });
            return 1;
        }
        catch (Exception $e)
        {
            return 0;
        }
      }
      else
      {
        try
        {
            Mail::send('templates/pulse-mailer/invoice-payment-done-template', $data, function($message) use ($email,$sender_name,$sender_email,$invoice_title) {
             $message->to($email, 'Tutorials Point')->subject
                ($invoice_title);
             $message->from($sender_email,$sender_name);
            });
            return 1;
        }
        catch (Exception $e)
        {
            return 0;
        }
      }
   }

   public function sendInvoice()
   {
        $getInvoiceDetails=DB::table('invoices')->select('*')->where('id',Input::get('invoice_id'))->first();   //GET INPUT INVOICE ID AND GET INVOICE INFORMATION WITH INVOICE ITEMS AND INVOICE META AND SEND INVOICE EMAIL
        $getInvoiceDetails=json_decode(json_encode($getInvoiceDetails), true);
        $getInvoiceMeta=DB::table('invoice_meta')->select('*')->where('inv_id',Input::get('invoice_id'))->first();
        $getInvoiceMeta=json_decode(json_encode($getInvoiceMeta), true);
        $getInvoiceItems=DB::table('invoice_items')->select('*')->where('inv_id',Input::get('invoice_id'))->get();
        $getInvoiceItems=json_decode(json_encode($getInvoiceItems), true);
        $getInvoiceItemTotal=DB::table('invoice_items')->where('inv_id',Input::get('invoice_id'))->sum('total');
        $getInvoiceItemTotal=json_decode(json_encode($getInvoiceItemTotal), true);
        $total=$getInvoiceMeta['inv_total'];
        $shipping_cost=$getInvoiceMeta['shipping_cost'];
        $discount=$getInvoiceMeta['discount'];
        $tax=$getInvoiceMeta['tax'];
        $adjustment=$getInvoiceMeta['adjustment'];
        $checkSmtp=$this->checkSmtp();
        if($checkSmtp!="ok")
        {
            return response()->json(['status' => $checkSmtp]);
        }
        $updateInvoiceStatus=DB::table('invoices')->where('id',Input::get('invoice_id'))->update(['status' => 'send']);
        $updateInvoiceSendDate=DB::table('invoices')->where('id',Input::get('invoice_id'))->update(['send_date' => date("Y-m-d")]);
        $invoice_title=Input::get('invoice_subject');
        if($invoice_title=="")
        {
            $invoice_title=env('PORTAL_NAME')." invoice";
        }
        $ee=$this->sendInvoiceMail($getInvoiceDetails['contact'],$getInvoiceItems,$total,$shipping_cost,$discount,$tax,$adjustment,$getInvoiceItemTotal,$getInvoiceDetails['id'],$getInvoiceDetails['due_date'],$getInvoiceMeta['terms_and_conditions'],$getInvoiceDetails['link'],$invoice_title,'',Input::get('message_to_customer'),'unpaid');
        if($ee==true)
        {
            return response()->json(['response' => 'success']);
        }
        else
        {
            return response()->json(['response' => 'failed' , 'message' => 'failed to send email please check smtp details']);
        }
   }

   function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getNotificationDifference($getNotification)
    {
        date_default_timezone_set(env('TIME_ZONE'));    //GET ENVIRONMENT TIME ZONE
        $notification_difference_array=array(); //CREATE NOTIFICATION DIFFERENCE ARRAY
        foreach($getNotification as $key=>$value)
        {
            $datetime1 = new DateTime($getNotification[$key]['created_at']);    //NOTIFICATION CREATED DATE
            $datetime2 = new DateTime(date("Y-m-d h:i:s a"));   //CURRENT DATE
            $interval = $datetime1->diff($datetime2);   //NOTIFICATION DIFFERENCE
            if($interval->format('%h')>0)
            {
                $interval = $interval->format('%h')." hr ".$interval->format('%i')." min ago";  //NOTIFICATION DIFFERENCE IN HOUR AND MINUTE
            }
            else
            {
                $interval = $interval->format('%i')." min ago"; //NOTIFICATION DIFFERENCE IN MINUTE
            }
            array_push($notification_difference_array,$interval);
        }
        return $notification_difference_array;
    }

    public function checkInvoiceNo()
    {
        if(Input::get('purpose')=="invoice")
        {
            $getInvoiceByInvNo=DB::table('invoices')->select('*')->where('inv_no',Input::get('inv_no'))->first();
        }
        else
        {
            $getInvoiceByInvNo=DB::table('estimates')->select('*')->where('est_no',Input::get('inv_no'))->first();
        }
        $getInvoiceByInvNo=json_decode(json_encode($getInvoiceByInvNo), true);
        if(!empty($getInvoiceByInvNo))
        {
            return response()->json(['response' => 'failed']);
        }
        else
        {
            return response()->json(['response' => 'success']);
        }
    }
}
