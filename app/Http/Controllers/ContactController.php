<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Routing\UrlGenerator;
use App\Contacts;
use App\Lead;
use App\Contact_address;
use App\Improfiles;
use App\social_profile;
use App\LeadStage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use View;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use DB;
use App\Quotation;
use Auth;
use Session;
use App\Traits\MainTrait;
use Jenssegers\Agent\Agent;
use App\Notification;
use DateTime;
use App\Traits\PermissionTrait;
use App\Feed;
use Lang;
use Excel;
use App\Phone_contacts;
use PaginateRoute;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class ContactController extends BaseController
{
    //
    use MainTrait;
    use PermissionTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->middleware('auth');
        $this->phoneContactTable = new Phone_contacts();
    }

    public function index()
    {
        //PAGINATE BY 6
        $contacts = DB::table('contacts')->orderBy('created_at', 'desc')->paginate(config('app.contact_pagination'));
        $pagination_array = array();
        $pagination_array['page_list'] = PaginateRoute::allUrls($contacts);
        $contacts = $contacts->toArray();
        $prev_page_url = $contacts['prev_page_url'];
        $next_page_url = $contacts['next_page_url'];
        $pagination_array['prev_page_url'] = $prev_page_url;
        $pagination_array['next_page_url'] = $next_page_url;
        $contacts = $contacts['data'];
        $contactsArray=array();
        foreach ($contacts as $value) {
            $individualContactArray = $this->getStructureForContentTable($value);
            array_push($contactsArray, $individualContactArray);
        }
        $notification_table=new Notification;
        $getNotification=$notification_table->getNotification(Auth::getUser()->id);
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);
        }
        $agent = new Agent();
        $get_user_profile_image=$this->get_profile_image();
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        if($getUserPermission['contact']['view']=="off")
        {
            return redirect('/dashboard');
        }
        if($agent->isMobile())
        {
            return view('dashboard/contact/contact-grid-mobile', ['data' => $contactsArray], ['message' => '' , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'profile_image' => $get_user_profile_image , 'active' => 'contact' , 'show_tutorial' => $checkShowTutorial , 'pagination_array' => $pagination_array , 'permission' => $getUserPermission]);
        }
        return view('dashboard/contact/contact-grid', ['data' => $contactsArray], ['message' => '' , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'profile_image' => $get_user_profile_image , 'active' => 'contact' , 'show_tutorial' => $checkShowTutorial , 'pagination_array' => $pagination_array , 'permission' => $getUserPermission]);
    }

    public function create(Request $request)
    {
        $first_name=Input::get('first_name');
        $last_name=Input::get('last_name');
        $email=$this->getStructeredEmailAndPhone(array_filter(Input::get('email')),array_filter(Input::get('email_purpose')));
        $email_serialized=$this->getSerializedData($email);
        $phone=$this->getStructeredEmailAndPhone(array_filter(Input::get('phone')),array_filter(Input::get('phone_purpose')));
        $phone_serialized=$this->getSerializedData($phone);
        $createImProfile="";
        $createSocialProfile="";
        $company=0;
        if(!empty(Input::get('imProfile')) && Input::get('profile_url')[0]!="") //GET SERIALIZED IM PROFILE ARRAY IF IM PROFILE FIELD EXISTS
        {
            $createImProfile=$this->createImProfile(Input::get('imProfile'),Input::get('profile_url'));
        }
        if(!empty(Input::get('social_profile')) && Input::get('profile_url')[0]!="")    //GET SERIALIZED SOCIAL PROFILE ARRAY IF SOCIAL PROFILE FIELD EXISTS
        {
            $createSocialProfile=$this->createSocialProfile(Input::get('social_profile'),Input::get('social_profile_url'));
        }
        if(Input::get('contact-company')=="select-company") //CREATE COMPANY IF NOT EXISTS
        {
            if(Input::get('com-name')!="")
            {
                $data['company_name']=Input::get('com-name');
                $data['address_type']=Input::get('company_address_purpose');
                $data['address']=Input::get('street-address');
                $data['city']=Input::get('city');
                $data['state']=Input::get('company_state');
                $data['country']=Input::get('company_country');
                $data['post_code']=Input::get('post-code');
                $createCompany=$this->createContactCompany($data);
                $company=$createCompany;
            }
        }
        if(Input::get('contact-company')!="select-company") //GET COMPANY ID IF COMPANY EXISTS
        {
            $company=Input::get('contact-company');
        }
    	$contact_id=Contacts::create([
            'title' => Input::get('contact_title'),
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email_serialized,
            'phone' => $phone_serialized,
            'company' => $company,
            'contact_type' => Input::get('contact_type'),
            'created_by' => Auth::getUser()->id
        ]);
        $last_contact_id=$contact_id->id;   //GET LAST CONTACT ID

        if($company!="0" && Input::get('default_contact')!="")
        {
            $default_contact=DB::table('company')->where('id',$company)->update(['default_contact' => $last_contact_id]);
        }

        //CREATE LEAD IF EXIST
        if(!empty(Input::get('lead_content_type')) || !empty(Input::get('lead_industry')) || !empty(Input::get('lead_source')) || !empty(Input::get('lead_stage')) || !empty(Input::get('lead_territory')) || !empty(Input::get('lead_visible_to')))
        {
            $lead=Lead::create([
            'contact_id' => $last_contact_id,
            'content_type' => Input::get('lead_content_type')[0],
            'industry' => Input::get('lead_industry')[0],
            'lead_source' => Input::get('lead_source')[0],
            'lead_stage' => "1",
            'territory' => Input::get('lead_territory')[0]
            ]);
            $last_lead_id = $lead->id;
        }


        //CREATE IM PROFILE IF EXIST
        if(!empty(Input::get('imProfile')) && Input::get('profile_url')[0]!="")
        {
            $imProfileUrl=Input::get('profile_url');
            $imProfile=Input::get('imProfile');
            foreach($imProfile as $key=>$value)
            {
                $imProfile_id=DB::table('Improfiles')->insertGetId(['profile' => $value,'profile_url' => $imProfileUrl[$key],'contact_id' => $last_contact_id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
                    ]);
            }
        }

        //CREATE SOCIAL PROFILE IF EXIST
        if(!empty(Input::get('social_profile')) && Input::get('social_profile_url')[0]!="")
        {
            $socialProfileUrl=Input::get('social_profile_url');
            $socialProfile=Input::get('social_profile');
            foreach($socialProfile as $key=>$value)
            {
                $socialProfile_id=DB::table('social_profile')->insertGetId(['profile' => $value,'profile_url' => $socialProfileUrl[$key],'contact_id' => $last_contact_id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')
                    ]);
            }
        }

        //CREATE ADDRESS IF EXIST
        if(sizeof(Input::get('addresstype'))>0 && sizeof(Input::get('country'))>0)
        {
            foreach(Input::get('addresstype') as $key=>$value)
            {
                $address_id=DB::table('contacts_address')->insertGetId(['contact_id' => $last_contact_id, 'office_address' => $value,'street_address' => Input::get('street_address')[$key], 'city' => Input::get('city_address')[$key], 'state' => Input::get('state')[$key], 'country' => Input::get('country')[$key], 'post_code' => Input::get('post_code')[$key], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            }
        }

        //CREATE FEED
        $getCurrentUser=DB::table('users')->select('*')->where('id',Auth::getUser()->id)->first();
        $getCurrentUser=json_decode(json_encode($getCurrentUser), true);
        $feed_table=new Feed;
        $new_contact=Lang::get('ajax-lang.new-contact');
        $has_been_created_by=Lang::get('ajax-lang.has-been-created-by');
        $feed_title='New contact '.Input::get('first_name')." ".Input::get('last_name').' has been created by '.$getCurrentUser['first_name']." ".$getCurrentUser['last_name'];
        $createFeed=$feed_table->createFeed($feed_title,Auth::getUser()->id,$contact_id,'0','contact');
        return redirect('/contacts')->with('status', 'Contact has been created');
    }

    public function edit(Request $request)
    {
        //EDIT FIRST NAME

        if(Input::get('default_contact')!="")   //EDIT DEFAULT CONTACT
        {
            $getContactCompany=DB::table('contacts')->select('*')->where('id',Input::get('contact_id'))->first();
            $getContactCompany=json_decode(json_encode($getContactCompany), true);
            if($getContactCompany['company']!="0")
            {
                $change_default_contact=DB::table('company')->where('id',$getContactCompany['company'])->update(['default_contact' => Input::get('contact_id')]);
            }
        }
        $edit_contact_work_email_array=Input::get('edit_contact_work_email');
        $edit_contact_home_email_array=Input::get('edit_contact_home_email');
        $edit_contact_work_phone_array=Input::get('edit_contact_work_phone');
        $edit_contact_home_phone_array=Input::get('edit_contact_home_phone');
        
        if(empty(Input::get('edit_contact_work_email')))    //IF CONTACT WORK EMAIL ARRAY IS EMPTY THEN CREATE AN EMPTY ARRAY
        {
            $edit_contact_work_email_array=array();
        }
        if(empty(Input::get('edit_contact_home_email')))    //IF CONTACT HOME EMAIL ARRAY IS EMPTY THEN CREATE AN EMPTY ARRAY
        {
            $edit_contact_home_email_array=array();
        }
        if(empty(Input::get('edit_contact_home_phone')))    //IF CONTACT HOME PHONE ARRAY IS EMPTY THEN CREATE AN EMPTY ARRAY
        {
            $edit_contact_home_phone_array=array();
        }
        if(empty(Input::get('edit_contact_work_phone')))    //IF CONTACT WORK PHONE ARRAY IS EMPTY THEN CREATE AN EMPTY ARRAY
        {
            $edit_contact_work_phone_array=array();
        }

        $add_extra_email_contact=false;
        $add_extra_phone_contact=false;
        foreach(Input::get('add_extra_email_contact') as $value)
        {
            if($value!="")
            {
                $add_extra_email_contact=true;
            }
        }
        foreach(Input::get('add_extra_phone_contact') as $value)
        {
            if($value!="")
            {
                $add_extra_phone_contact=true;
            }
        }
        if($add_extra_email_contact==true)
        {
            foreach(Input::get('add_extra_email_contact') as $key=>$value)
            {
                if($value!="")
                {
                    if(Input::get('edit_email_purpose')[$key]=="home")
                    {
                        array_push($edit_contact_home_email_array,$value);
                    }
                    else
                    {
                        array_push($edit_contact_work_email_array,$value);
                    }
                }
            }
        }
        if($add_extra_phone_contact==true)
        {
            foreach(Input::get('add_extra_phone_contact') as $key=>$value)
            {
                if($value!="")
                {
                    if(Input::get('edit_phone_purpose')[$key]=="home")
                    {
                        array_push($edit_contact_home_phone_array,$value);
                    }
                    else
                    {
                        array_push($edit_contact_work_phone_array,$value);
                    }
                }
            }
        }
        $email=array(
            'home' => $edit_contact_home_email_array,
            'work' => $edit_contact_work_email_array
        );
        $phone=array(
            'home' => $edit_contact_home_phone_array,
            'work' => $edit_contact_work_phone_array
        );

        $email_serialized=$this->getSerializedData($email);
        $phone_serialized=$this->getSerializedData($phone);

        //UPDATE BASIC INFO
        $update_basic_info=DB::table('contacts')->where('id', Input::get('contact_id'))->update(['first_name' => Input::get('first_name'), 'last_name' => Input::get('last_name'), 'email' => $email_serialized, 'phone' => $phone_serialized]);

        //UPDATE CONTACT ADDRESS IF EXIST
        if(sizeof(Input::get('address_type'))>0 || sizeof(Input::get('country'))>0)
        {
            if(in_array("0",Input::get('address_array'),TRUE))
            {
                
                foreach(Input::get('address_type') as $key=>$value)
                {
                    $address_id=DB::table('contacts_address')->insertGetId(['contact_id' => Input::get('contact_id'), 'office_address' => $value,'street_address' => Input::get('street_address')[$key], 'city' => Input::get('city')[$key], 'state' => Input::get('state')[$key], 'country' => Input::get('country')[$key], 'post_code' => Input::get('post_code')[$key], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                }
                
            }
            else
            {
                foreach(Input::get('address_type') as $key=>$value)
                {
                    if(Input::get('address_array')[$key]=="null")
                    {
                        $address_id=DB::table('contacts_address')->insertGetId(['contact_id' => Input::get('contact_id'), 'office_address' => $value,'street_address' => Input::get('street_address')[$key], 'city' => Input::get('city')[$key], 'state' => Input::get('state')[$key], 'country' => Input::get('country')[$key], 'post_code' => Input::get('post_code')[$key], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                    }
                    else
                    {
                        $address_id=DB::table('contacts_address')->where('id', Input::get('address_array')[$key])->update(['office_address' => $value , 'street_address' => Input::get('street_address')[$key], 'city' => Input::get('city')[$key], 'state' => Input::get('state')[$key], 'country' => Input::get('country')[$key], 'post_code' => Input::get('post_code')[$key], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                    }
                    
                }
            }
        }


        //UPDATE IM PROFILE IF EXIST
        if(sizeof(Input::get('im_profile'))>0)
        {
            if(in_array("0",Input::get('im_profile_id_array'),TRUE))
            {
                if(Input::get('im_profile_url')[$key]!="")
                {
                    $im_profile_id=DB::table('improfiles')->insertGetId(['contact_id' => Input::get('contact_id'), 'profile' => $value,'profile_url' => Input::get('im_profile_url')[$key], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                }
            }
            else
            {
                foreach(Input::get('im_profile') as $key=>$value)
                {
                    if(Input::get('im_profile_id_array')[$key]=="null")
                    {
                        if(Input::get('im_profile_url')[$key]!="")
                        {
                            $im_profile_id=DB::table('improfiles')->insertGetId(['contact_id' => Input::get('contact_id'), 'profile' => $value,'profile_url' => Input::get('im_profile_url')[$key], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                        }
                    }
                    else
                    {
                        $im_profile_id=DB::table('improfiles')->where('id', Input::get('im_profile_id_array')[$key])->update(['profile' => $value , 'profile_url' => Input::get('im_profile_url')[$key]]);
                    }
                }
            }
        }

        //UPDATE SOCIAL PROFILE IF EXIST
        if(sizeof(Input::get('social_profile'))>0)
        {
            if(in_array("0",Input::get('social_profile_id_array'),TRUE))
            {
                foreach(Input::get('social_profile') as $key=>$value)
                {
                    if(Input::get('social_profile_url')[$key]!="")
                    {
                        $social_profile_id=DB::table('social_profile')->insertGetId(['contact_id' => Input::get('contact_id'), 'profile' => $value,'profile_url' => Input::get('social_profile_url')[$key], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                    }
                    // 
                }
            }
            else
            {
                foreach(Input::get('social_profile') as $key=>$value)
                {
                    if(Input::get('social_profile_id_array')[$key]=="null")
                    {
                        if(Input::get('social_profile_url')[$key]!="")
                        {
                            $social_profile_id=DB::table('social_profile')->insertGetId(['contact_id' => Input::get('contact_id'), 'profile' => $value,'profile_url' => Input::get('social_profile_url')[$key], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                        }
                    }
                    else
                    {
                        $social_profile_id=DB::table('social_profile')->where('id', Input::get('social_profile_id_array')[$key])->update(['profile' => $value , 'profile_url' => Input::get('social_profile_url')[$key]]);
                    }
                }
            }
        }

        Session::flash('message', 'Contact has been updated'); 
        Session::flash('alert-class', 'success'); 
        return redirect('/edit-contact/'.Input::get('contact_id'));
    }

    public function individual()
    {
        $data=array('validation_message'=>'','validation_class'=>'');
        $getTasks=DB::table('task')->orderBy('id','desc')->limit(5)->get(); //GET LAST 5 TASKS
        $getTasks=json_decode(json_encode($getTasks), true);
        $task_array=$this->getStructuredTaskArray($getTasks);
        $notification_table=new Notification;
        $getNotification=$notification_table->getNotification(Auth::getUser()->id); //GET USER NOTIFICATION
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);
        }
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        if($getUserPermission['contact']['add']=="off")
        {
            return redirect('/dashboard');
        }
        $agent = new Agent();
        if($agent->isMobile())
        {
            return view('dashboard/contact/companyindividual-mobile', ['data' => $data], ['task' => $task_array , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'show_tutorial' => $checkShowTutorial , 'permission' => $getUserPermission]);
        }
        return view('dashboard/contact/companyindividual', ['data' => $data], ['task' => $task_array , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'show_tutorial' => $checkShowTutorial , 'permission' => $getUserPermission]);
    }

    public function company()
    {
        $data=array('validation_message'=>'','validation_class'=>'');
        $getTasks=DB::table('task')->orderBy('id','desc')->limit(5)->get(); //GET LAST 5 TASKS
        $getTasks=json_decode(json_encode($getTasks), true);
        $task_array=$this->getStructuredTaskArray($getTasks);
        $getCompany=DB::table('company')->select('*')->get();
        $getCompany=json_decode(json_encode($getCompany), true);
        $notification_table=new Notification;
        $getNotification=$notification_table->getNotification(Auth::getUser()->id); //GET USER NOTIFICATION
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);
        }
        $get_user_profile_image=$this->get_profile_image();
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        if($getUserPermission['contact']['add']=="off")
        {
            return redirect('/dashboard');
        }
        $agent = new Agent();
        if($agent->isMobile())
        {
            return view('dashboard/contact/companycontact-mobile', ['data' => $data], ['task' => $task_array , 'company' => $getCompany , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'profile_image' => $get_user_profile_image , 'active' => 'contact' , 'show_tutorial' => $checkShowTutorial , 'permission' => $getUserPermission]);
        }
        return view('dashboard/contact/companycontact', ['data' => $data], ['task' => $task_array , 'company' => $getCompany , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'profile_image' => $get_user_profile_image , 'active' => 'contact' , 'show_tutorial' => $checkShowTutorial , 'permission' => $getUserPermission]);
    }

    public function createImProfile($imProfile,$profile_url)
    {
        $createImProfile=array();
        foreach($imProfile as $key=>$value)
        {
            $profileValue=strtolower($value);
            $profileValue=str_replace(" ","_",$profileValue);
            if(empty($createImProfile) || !array_key_exists($profileValue, $createImProfile))
            {
                $createImProfile[$profileValue]=array();
            }
        }
        foreach($imProfile as $key=>$value)
        {
            $profileValue=strtolower($value);
            $profileValue=str_replace(" ","_",$profileValue);
            foreach($createImProfile as $inner_key=>$inner_value)
            {
                if($inner_key==$profileValue)
                {
                    array_push($createImProfile[$inner_key],$profile_url[$key]);
                }
                
            }
        }
        return serialize($createImProfile);
    }

    public function createSocialProfile($social_profile,$social_profile_url)
    {
        $createSocialProfile=array();
        foreach($social_profile as $key=>$value)
        {
            $profileValue=strtolower($value);
            $profileValue=str_replace(" ","_",$profileValue);
            if(empty($createSocialProfile) || !array_key_exists($profileValue, $createSocialProfile))
            {
                $createSocialProfile[$profileValue]=array();
            }
        }
        foreach($social_profile as $key=>$value)
        {
            $profileValue=strtolower($value);
            $profileValue=str_replace(" ","_",$profileValue);
            foreach($createSocialProfile as $inner_key=>$inner_value)
            {
                if($inner_key==$profileValue)
                {
                    array_push($createSocialProfile[$inner_key],$social_profile_url[$key]);
                }
                
            }
        }
        return serialize($createSocialProfile);
    }

    public function getSerializedData($data)
    {
        return serialize($data);
    }

    public function getStructeredEmailAndPhone($email,$purpose)
    {
        $email_array=array();
        $home=array();
        $work=array();
        foreach($purpose as $key=>$value)
        {
            if($value=="home")
            {
                array_push($home,$email[$key]);
            }
            else
            {
                array_push($work,$email[$key]);
            }
        }
        $email_array=["home"=>$home,"work"=>$work];
        return $email_array;
    }

    public function getStructureForContentTable($value)
    {
        $phone_array=unserialize($value->phone);
        $email_array=unserialize($value->email);
        $email = "";
        $phone_no = "";
        if(!empty(array_filter($phone_array['work'])))
        {
            $address_work_home="W";
            foreach($phone_array['work'] as $value_phone)
            {
                if($value_phone!="")
                {
                   $phone_no=$value_phone; 
                   break;
                }
            }
        }

        else
        {
            $address_work_home="H";
            foreach($phone_array['home'] as $value_phone)
            {
                if($value_phone!="")
                {
                   $phone_no=$value_phone; 
                   break;
                }
            }
        }
        if(!empty(array_filter($email_array['work'])))
        {
            foreach($email_array['work'] as $value_email)
            {
                if($value_email!="")
                {
                   $email=$value_email; 
                   break;
                }
            }
        }
        else
        {
            foreach($email_array['home'] as $value_email)
            {
                if($value_email!="")
                {
                   $email=$value_email; 
                   break;
                }
            }
        }
        $id=$value->id;
        $title=$value->title;
        $first_name=$value->first_name;
        $last_name=$value->last_name;
        $street_address="-";
        $lead_stage="-";
        $lead_source="-";
        $getAdd=DB::table('contacts_address')->select('*')->where('contact_id','=',$value->id)->get();
        $getAdd=json_decode(json_encode($getAdd), true);
        if(!empty(array_filter($getAdd)))
        {
            foreach($getAdd as $add_value)
            {
                if($add_value['street_address']!="")
                {
                    $street_address=$add_value['street_address'];
                    break;
                }
            }
        }
        $getContactLeadInfo=DB::table('lead_table')->select('*')->where('contact_id','=',$value->id)->get();
        $getContactLeadInfo=json_decode(json_encode($getContactLeadInfo), true);
        if(!empty(array_filter($getContactLeadInfo)))
        {
            foreach($getContactLeadInfo as $lead_value)
            {
                if($lead_value['lead_source']!="" && $lead_value['lead_stage']!="")
                {
                    $getStage=DB::table('lead_stage')->select('*')->where('id','=',$lead_value['lead_stage'])->get();
                    $getStage=json_decode(json_encode($getStage), true);
                    $lead_stage = $getStage[0]['stage'];
                    $lead_source = $lead_value['lead_source'];
                    break;
                }
            }
        }

        //GET DATE FORMAT
        $created_at=$this->getDateFormat(explode(" ",$value->created_at)[0]);

        $individualContactArray=array(
            "id" => $value->id,
            "title" => $title,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "created_at" => $created_at,
            "created_at_2" => $value->created_at,
            "email" => $email,
            "phone" => $phone_no,
            "address_work_home" => $address_work_home,
            "street_address" => $street_address,
            "lead_stage" => $lead_stage,
            "lead_source" => $lead_source
        );
        return $individualContactArray;
    }

    public function createSeparation()
    {
        $data=array();
        $getTasks=DB::table('task')->orderBy('id','desc')->limit(5)->get();
        $getTasks=json_decode(json_encode($getTasks), true);
        $task_array=$this->getStructuredTaskArray($getTasks);
        $notification_table=new Notification;
        $getNotification=$notification_table->getNotification(Auth::getUser()->id);
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);
        }
        $agent = new Agent();
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        if($agent->isMobile())
        {
            return view('dashboard/contact/contact-separation-mobile', ['data' => $data], ['task' => $task_array , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'permission' => $getUserPermission]);
        }
        return view('dashboard/contact/contact-separation', ['data' => $data], ['task' => $task_array , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'permission' => $getUserPermission]);
    }

    public function editContact($contact_id)
    {
        if(Request::input('page')=="")
        {
            return redirect('contacts');
        }
        $getContactDetails=DB::table('contacts')->select('*')->where('id','=',$contact_id)->first();
        $getContactDetails=json_decode(json_encode($getContactDetails), true);
        $getContactAddress=DB::table('contacts_address')->select('*')->where('contact_id','=',$contact_id)->get();
        $getContactAddress=json_decode(json_encode($getContactAddress), true);
        $getContacts=DB::table('contacts')->select('*')->get();
        $getContacts=json_decode(json_encode($getContacts), true);
        $getCompany=DB::table('company')->select('*')->get();
        $getCompany=json_decode(json_encode($getCompany), true);
        $getUsers=DB::table('users')->select('*')->get();
        $getUsers=json_decode(json_encode($getUsers), true);
        $getStages=DB::table('stages')->select('*')->get();
        $getStages=json_decode(json_encode($getStages), true);
        $getStatus=DB::table('status')->select('*')->get();
        $getStatus=json_decode(json_encode($getStatus), true);
        $getCurrentUser=Auth::getUser()->id;
        $unserializeEmailArray=unserialize($getContactDetails['email']);
        $unserializePhoneArray=unserialize($getContactDetails['phone']);

        $imProfileArray=DB::table('improfiles')->select('*')->where('contact_id','=',$contact_id)->get();
        $socialProfileArray=DB::table('social_profile')->select('*')->where('contact_id','=',$contact_id)->get();
        $work_email=$unserializeEmailArray['work'];
        $home_email=$unserializeEmailArray['home'];
        $work_phone=$unserializePhoneArray['work'];
        $home_phone=$unserializePhoneArray['home'];
        $getDeal=DB::table('deals')->select('*')->where('contact',$getContactDetails['id'])->get();
        $getDeal=json_decode(json_encode($getDeal), true);
        $default_contact="false";
        $getDealSource=array();
        $getDealSource=DB::table('settings')->join('users', 'users.company', '=', 'settings.company_id')->where('meta_key','deal_source')->select('*')->where('users.id', '=', Auth::getUser()->id)->first();
        $getDealSource=json_decode(json_encode($getDealSource), true);
        $getCurrency=DB::table('currency')->join('users', 'users.company', '=', 'currency.company_id')->select('*')->where('users.id', '=', $getCurrentUser)->first();
        $getCurrency=json_decode(json_encode($getCurrency), true);
        $getCurrency=unserialize($getCurrency['currency_list']);
        if(!empty($getDealSource['meta_value']))
        {
            $getDealSource=unserialize($getDealSource['meta_value']);
        }
        if($getContactDetails['contact_type']=="company")
        {
            foreach($getCompany as $key=>$value)
            {
                if($value['id']==$getContactDetails['company'])
                {
                    if($value['default_contact']==$getContactDetails['id'])
                    {
                        $default_contact="true";
                    }
                }
            }
        }
        $basic_info=array(
            'id' => $getContactDetails['id'],
            'title' => $getContactDetails['title'],
            'first_name' => $getContactDetails['first_name'],
            'last_name' => $getContactDetails['last_name'],
            'im_profile' => $imProfileArray,
            'social_profile' => $socialProfileArray,
            'contacts' => $getContacts,
            'company' => $getCompany,
            'status' => $getStatus,
            'users' => $getUsers,
            'stages' => $getStages,
            'current_user' => $getCurrentUser,
            'deal' => $getDeal,
            'page' => Request::input('page'),
            'contact_type' => $getContactDetails['contact_type'],
            'user_id' => Auth::getUser()->id,
            'currency' => $getCurrency,
            'default_contact' => $default_contact
        );
        $notification_table=new Notification;
        $getNotification=$notification_table->getNotification(Auth::getUser()->id);
        $getNotification=json_decode(json_encode($getNotification), true);
        $getNotificationDifference=array();
        if(!empty($getNotification))
        {
            $getNotificationDifference=$this->getNotificationDifference($getNotification);
        }
        $get_user_profile_image=$this->get_profile_image();
        $agent = new Agent();
        //GET SHOW TUTORIAL
        $checkShowTutorial=$this->checkShowTutorial();
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        if($getUserPermission['contact']['edit']=="off")
        {
            return redirect('/dashboard');
        }
        if($agent->isMobile())
        {
            return view('dashboard/contact/contextual-mobile', ['basic_info' => $basic_info], ['work_email' => $work_email, 'home_email' => $home_email, 'work_phone' => $work_phone, 'home_phone' => $home_phone, 'address' => $getContactAddress, 'im_profile' => json_decode($imProfileArray, true), 'social_profile' => json_decode($socialProfileArray, true) , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'profile_image' => $get_user_profile_image , 'active' => 'contact' , 'show_tutorial' => $checkShowTutorial , 'deal_source' => $getDealSource , 'permission' => $getUserPermission], ['contact_id' => $contact_id]);
        }
        return view('dashboard/contact/contextual', ['basic_info' => $basic_info], ['work_email' => $work_email, 'home_email' => $home_email, 'work_phone' => $work_phone, 'home_phone' => $home_phone, 'address' => $getContactAddress, 'im_profile' => json_decode($imProfileArray, true), 'social_profile' => json_decode($socialProfileArray, true) , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'profile_image' => $get_user_profile_image , 'active' => 'contact' , 'show_tutorial' => $checkShowTutorial , 'deal_source' => $getDealSource , 'permission' => $getUserPermission], ['contact_id' => $contact_id]);
    }

    public function deleteContact()
    {
        $deleteContact=DB::table('contacts')->where('id',Input::get('contact_id'))->delete();
        return response()->json(['response' => $deleteContact]);
    }

    public function createCompany()
    {
        $createCompany=DB::table('company')->insertGetId(['company_name' => Input::get('company_name'), 'address_type' => Input::get('addresstype'), 'address' => Input::get('streetaddress'), 'city' => Input::get('city'), 'state' => Input::get('state'), 'country' => Input::get('country'), 'post_code' => Input::get('postcode'), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        return response()->json(['response' => $createCompany]);
    }

    public function getNotificationDifference($getNotification)
    {
        date_default_timezone_set(env('TIME_ZONE'));
        $notification_difference_array=array();
        foreach($getNotification as $key=>$value)
        {
            $datetime1 = new DateTime($getNotification[$key]['created_at']);
            $datetime2 = new DateTime(date("Y-m-d h:i:s a"));
            $interval = $datetime1->diff($datetime2);
            if($interval->format('%h')>0)
            {
                $interval = $interval->format('%h')." hr ".$interval->format('%i')." min ago";
            }
            else
            {
                $interval = $interval->format('%i')." min ago";
            }
            array_push($notification_difference_array,$interval);
        }
        return $notification_difference_array;
    }

    public function getContact()
    {
        $getContact=DB::table('contacts')->join('company', 'company.id', '=', 'contacts.company')->select('company.*')->where('contacts.id',Input::get('contact_id'))->first();
        $getContact=json_decode(json_encode($getContact), true);
        if(!empty($getContact))
        {
            return response()->json(['response' => 'success' , 'company' => $getContact]);
        }
        else
        {
            return response()->json(['response' => 'failed']);
        }
    }

    public function importContact()
    {
        $re = Input::file('contact')->getClientOriginalName();
       $destinationPath = base_path('uploads/import-contact'); // upload path
       $file_name=Input::file('contact')->getClientOriginalName();
       $file_name = pathinfo($file_name, PATHINFO_FILENAME);
       $extension = Input::file('contact')->getClientOriginalExtension(); // getting image extension
       $fileName = $file_name.'-'.rand(11111,99999).'.'.$extension; // renameing image
       Input::file('contact')->move($destinationPath, $fileName); // uploading file to given path

       $result = Excel::load(base_path('uploads/import-contact/'.$fileName), function($reader) {

            // Getting all results
            $results1 = $reader->get();

            // ->all() is a wrapper for ->get() and will work the same
            $results2 = $reader->all();

        })->toArray();

       $error_array = array();
       $message = "file_uploaded";
       $contact_imported = false;
        if(!empty($result))
        {
            foreach($result as $result_key=>$result_value)
            {
                if(!empty($result_value))
                {
                    foreach($result_value as $key=>$value)
                    {
                        if(array_key_exists('first_name',$value)!="" || array_key_exists('last_name',$value)!="" || array_key_exists('email',$value)!="" || array_key_exists('phone',$value)!="")
                        {
                            $first_name = $value['first_name'];
                            $last_name = $value['last_name'];
                            $email_array = array($value['email']);
                            $email_purpose_array = array('work');
                            $phone_array = array($value['phone']);
                            $phone_serialized_array = array('work');
                            $email=$this->getStructeredEmailAndPhone(array_filter($email_array),array_filter($email_purpose_array));
                            $email_serialized=$this->getSerializedData($email);
                            $phone=$this->getStructeredEmailAndPhone(array_filter($phone_array),array_filter($phone_serialized_array));
                            $phone_serialized=$this->getSerializedData($phone);
                            $contact_imported = true;
                            $message="ffff";
                            $contact_id=Contacts::create([
                                'title' => 'Mr',
                                'first_name' => $first_name,
                                'last_name' => $last_name,
                                'email' => $email_serialized,
                                'phone' => $phone_serialized,
                                'company' => 0,
                                'contact_type' => 0,
                                'created_by' => Auth::getUser()->id
                            ]);
                        }
                    }
                }
            }
        }
        else
        {
            $message = "empty_file";
        }
        return response()->json(['response' => $error_array , 'contact_imported' => $contact_imported , 'message' => $message ,'result' => $result]);
    }

    public function call()
    {
        return view('dashboard/call/call-list', ['data' => $this->phoneContactTable->getPhoneContacts() , 'active' => 'call'] , $this->createHeader());
    }
}
