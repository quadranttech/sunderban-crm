<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use DB;

class CropController extends Controller
{
    public function getAvatar(Request $request)
    {
        $data = json_decode($request->get('avatar_data'), true);
        $file = $request->file('avatar_file');

        if (!empty($file)) {
            $file=Input::file('avatar_file');
            if($request->get('purpose')=="company_logo")
            {
                $destinationPath = base_path('uploads/company-logo');
            }
            else
            {
                $destinationPath = base_path('uploads/profile-image');
            }
            $filename = $file->getClientOriginalName();
            $filename=pathinfo($filename, PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $thumbName      =   $filename.'-'.rand(11111,99999).'.'.$extension;
            $logName = 'company-logo.png';
            $userImage = 'user-image.jpg';

            if($request->get('purpose')=="company_logo")
            {
                Image::make($file->getRealPath())->crop(
                    intval($data['height']),
                    intval($data['width']),
                    intval($data['x']),
                    intval($data['y'])
                )->save(base_path('uploads/company-logo'). '/'. $thumbName);

                Image::make($file->getRealPath())->crop(
                    intval($data['height']),
                    intval($data['width']),
                    intval($data['x']),
                    intval($data['y'])
                )->save(base_path('uploads/company-logo'). '/'. $logName);
                $changeCompanyLogo=DB::table('admin_company')->where('id',$request->get('company_id'))->update(['logo' => $thumbName]);
            }
            else
            {
                Image::make($file->getRealPath())->crop(
                    intval($data['height']),
                    intval($data['width']),
                    intval($data['x']),
                    intval($data['y'])
                )->save(base_path('uploads/profile-image'). '/'. $thumbName);

                Image::make($file->getRealPath())->crop(
                    intval($data['height']),
                    intval($data['width']),
                    intval($data['x']),
                    intval($data['y'])
                )->save(base_path('uploads/profile-image'). '/'. $userImage);
            }


            if($request->get('purpose')=="company_logo")
            {
                $response = array(
                  'state'  => 200,
                  'message' => '',
                  'result' => "uploads/company-logo/".$thumbName,
                  'purpose' => $request->get('purpose'),
                  'fileName' => $thumbName
                );
            }
            else
            {
                $response = array(
                  'state'  => 200,
                  'message' => '',
                  'result' => "uploads/profile-image/".$thumbName,
                  'purpose' => $request->get('purpose'),
                  'fileName' => $thumbName
                );
            }
        }

        echo json_encode($response);
    }
}
