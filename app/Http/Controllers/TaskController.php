<?php

namespace App\Http\Controllers;


use Request;
use View;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use Carbon;
use File;
use App\Feed;
use App\Traits\MainTrait;
use Jenssegers\Agent\Agent;
use App\Notification;
use DateTime;
use App\Traits\PermissionTrait;
use Lang;

class TaskController extends Controller
{
    //
    use MainTrait;
    use PermissionTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->middleware('auth');
    }

    public function index()
    {
        $method = Request::method();
        date_default_timezone_set(env('TIME_ZONE', 'Asia/calcutta'));
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        if($getUserPermission['task']['all']=="on")
        {
            $getTasks=DB::table('task')->orderBy('id','desc')->get();
        }
    	else
        {
            $getTasks=DB::table('task')->where('assigned_to',Auth::getUser()->id)->orderBy('id','desc')->get();
        }
    	$getTasks=json_decode(json_encode($getTasks), true);
        $task_array=$this->getStructuredTaskArray($getTasks);
        $new_task_array=array();
        foreach($task_array as $key=>$value)
        {
            if($value['marked_as']=="due")
            {
                array_push($new_task_array,$value);
            }
        }
        foreach($task_array as $key=>$value)
        {
            if($value['marked_as']=="done")
            {
                array_push($new_task_array,$value);
            }
        }
        $task_array=$new_task_array;
        $getContacts=DB::table('users')->get();
        $getContacts=json_decode(json_encode($getContacts), true);
        $getDeal=DB::table('deals')->select('*')->get();
        $getDeal=json_decode(json_encode($getDeal), true);
        $get_user_profile_image=$this->get_profile_image();
        $user_id=Auth::getUser()->id;
        if($method=="GET")
        {
            $agent = new Agent();
            $notification_table=new Notification;
            $getNotification=$notification_table->getNotification(Auth::getUser()->id);
            $getNotification=json_decode(json_encode($getNotification), true);
            $getNotificationDifference=array();
            //GET SHOW TUTORIAL
            $checkShowTutorial=$this->checkShowTutorial();
            if(!empty($getNotification))
            {
                $getNotificationDifference=$this->getNotificationDifference($getNotification);
            }
            if($agent->isMobile())
            {
                return View::make('dashboard/task/task-grid-mobile',array('tasks' => $task_array , 'contacts' => $getContacts , 'deal' => $getDeal , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'profile_image' => $get_user_profile_image , 'active' => 'task' , 'show_tutorial' => $checkShowTutorial , 'user_id' => $user_id , 'permission' => $getUserPermission));
            }
            return View::make('dashboard/task/task-grid',array('tasks' => $task_array , 'contacts' => $getContacts , 'deal' => $getDeal , 'notification' => $getNotification , 'notification_difference' => $getNotificationDifference, 'profile_image' => $get_user_profile_image , 'active' => 'task' , 'show_tutorial' => $checkShowTutorial , 'user_id' => $user_id , 'permission' => $getUserPermission));
        }
        else
        {
            $base_url=$this->url->to('/');
            $content=file_get_contents($base_url.'/resources/views/templates/task-li.php');
            $agent = new Agent();
            if($agent->isMobile())
            {
                $content=file_get_contents($base_url.'/resources/views/templates/task-mobile-li.php');
            }
            $basic_info=array(
                'contacts' => $getContacts
                );
            return response()->json(['tasks' => $task_array,'basic_info' => $basic_info,'content' => $content]);
        }
    	
    }

    public function create()
    {
    	$getContacts=DB::table('users')->get();
    	$getContacts=json_decode(json_encode($getContacts), true);
    	return View::make('dashboard/task/task',array('contacts' => $getContacts));
    }

    public function saveNewTask()
    {
        $task_color_array=['red','blue','light-blue','cyen','green','light-green','teal','light-teal','indigo','purple','lime','orange','brown','blue-grey'];
        $method = Request::method();
    	date_default_timezone_set(env('TIME_ZONE', 'Asia/calcutta'));
    	$task_title=Input::get('task_title');
    	$task_description=Input::get('task_details');
    	$assigned_to=(Input::get('assigned_to') == '' ? 0 : Input::get('assigned_to'));
        
    	$due_date=Input::get('due_date');
    	$mytime = Carbon\Carbon::now();
        if($due_date!="")
        {
            $due_date=date("Y-m-d", strtotime($due_date) ).' '.date('h:i:sa');
        }
        else
        {
            $due_date="0000-00-00 00:00:00";
        }
    	
        $getRelatedCompany=0;
        if($assigned_to!=0)
        {
            $getRelatedCompany=DB::table('users')->select('company')->where('id', '=', $assigned_to)->get();
            $getRelatedCompany=json_decode(json_encode($getRelatedCompany), true)[0]['company'];
        }
    	$created_by=Auth::user()->id;
    	$marked_as='due';
    	$task_id=DB::table('task')->insertGetId(['created_by' => $created_by, 'task_title' => $task_title, 'task_description' => $task_description, 'assigned_to' => $assigned_to, 'related_to' => $getRelatedCompany, 'due_date' => $due_date, 'marked_as' => $marked_as, 'color' => $task_color_array[array_rand($task_color_array)], 'related_deal' => Input::get('related_deal'), 'created_at' => date('Y-m-d h:i:sa'), 'updated_at' => date('Y-m-d h:i:sa')]);
        // $feed_table=new Feed;
        // $feed_title="created ".Input::get('task_title')." deal ";
        // $contact_id=Input::get('contact_id');
        // $deal_id=Input::get('deal_id');
        // $feed_type=Input::get('feed_type');
        // $createFeed=$feed_table->createFeed($feed_title,$created_by,$contact_id,$deal_id,$feed_type);
        $getAssignedName=DB::table('users')->select('*')->where('id' , '=' , $created_by)->first();
        $getAssignedName=json_decode(json_encode($getAssignedName), true);
        $notification_table=new Notification;
        $created=Lang::get('ajax-lang.created');
        $by=Lang::get('ajax-lang.by');
        $notification_title=$created." ".Input::get('task_title'). $by." ".$getAssignedName['first_name'];
        $createNotification=$notification_table->createNotification($created_by,$assigned_to,$notification_title);
        if($assigned_to!=0)
        {
            $checkSmtp=$this->checkSmtp();
            if($checkSmtp!="ok")
            {
                return response()->json(['status' => $checkSmtp]);
            }
            else
            {
                $sendTaskMail=$this->sendTaskMail($created_by,$assigned_to,$task_title,Input::get('related_deal'));
            }
        }
        return response()->json(['status' => 'success']);
    }

    public function markedCompleteTask(Request $request)
    {
        $task_id=Request::input('task_id');
        $updateTask=DB::table('task')->where('id',$task_id)->update(['marked_as' => 'done']);
        return response()->json(['response' => 'success']);
    }

    public function getTaskTemplate(Request $request)
    {
        $task_id=Request::input('task_id');
        $getTask=DB::table('task')->select('*')->where('id' , '=' , $task_id)->get();
        $base_url=$this->url->to('/');
        $agent = new Agent();
        $content = file_get_contents($base_url.'/resources/views/templates/edit-task.php');
        if($agent->isMobile())
        {
            $content = file_get_contents($base_url.'/resources/views/templates/edit-task-mobile.php');
        }
        return response()->json(['response' => $content]);
    }

    public function changeAssignedTo(Request $request)
    {
        $task_id=Request::input('task_id');
        $assigned_to=Request::input('assigned_to');
        $changeAssignedTo=DB::table('task')->where('id',$task_id)->update(['assigned_to' => $assigned_to]);
        $getContact=DB::table('users')->select('*')->where('id',$assigned_to)->first();
        return response()->json(['response' => $changeAssignedTo , 'assigned_to' => $getContact]);
    }

    public function addNewTagToTask(Request $request)
    {
        $tag_array=['tag-red','tag-purple','tag-indigo','tag-blue','tag-green','tag-light-green','tag-orange','tag-teal','tag-cyan','tag-brown','tag-grey','tag-lime'];
        $tag_color=$tag_array[array_rand($tag_array)];
        $task_id=Request::input('task_id');
        $tag=Request::input('tag');
        $tag_slug=Request::input('tag_slug');
        $getTaskTag=DB::table('task')->select('task_tag')->where('id',$task_id)->first();
        $getTaskTag=json_decode(json_encode($getTaskTag), true);
        $checkIfTagExist=DB::table('task_tags')->select('*')->where('tag_slug','=',$tag_slug)->first();
        $tag_id=0;
        if(!$checkIfTagExist)
        {
            $tag_id=DB::table('task_tags')->insertGetId(['tag' => $tag , 'tag_slug' => $tag_slug , 'color' => $tag_color , 'created_at' => date('Y-m-d H:i:s') , 'updated_at' => date('Y-m-d H:i:s')]);
        }
        else
        {
            $tag_id=$checkIfTagExist->id;
            $tag_color=$checkIfTagExist->color;
        }
        $task_array=array();
        if($getTaskTag['task_tag']!="")
        {
            $unserialized_task_tag_array=unserialize($getTaskTag['task_tag']);
            if(in_array($tag_id, $unserialized_task_tag_array))
            {
                $task_array=$unserialized_task_tag_array;
            }
            else
            {
                array_push($unserialized_task_tag_array,$tag_id);
                $task_array=$unserialized_task_tag_array;
            }
        }
        else
        {
            array_push($task_array,$tag_id);
        }
        $task_array_serialized=serialize($task_array);
        $updateTaskTag=DB::table('task')->where('id',$task_id)->update(['task_tag' => $task_array_serialized]);
        return response()->json(['response' => $updateTaskTag , 'tag_id' => $tag_id , 'tag' => $tag , 'color' => $tag_color]);
    }

    public function getTaskTagById(Request $request)
    {
        $task_id=Request::input('task_id');
        $getTask=DB::table('task')->select('*')->where('id',$task_id)->first();
        $getTask=json_decode(json_encode($getTask), true);
        $structuredTaskTagsArray=array();
        if($getTask['task_tag']!="")
        {
            $taskTagsArray=unserialize($getTask['task_tag']);
            foreach($taskTagsArray as $value)
            {
                $tagArray=DB::table('task_tags')->select('*')->where('id',$value)->first();
                $temp=array();
                $temp=['id' => $tagArray->id , 'tag' => $tagArray->tag , 'color' => $tagArray->color];
                array_push($structuredTaskTagsArray,$temp);
            }
        }
        return response()->json(['structuredTaskTagsArray' => $structuredTaskTagsArray , 'tasks' => $getTask]);
    }

    public function changeTaskTitle(Request $request)
    {
        $task_id=Request::input('task_id');
        $task_title=Request::input('task_title');
        $changeTaskTitle=DB::table('task')->where('id',$task_id)->update(['task_title' => $task_title]);
        return response()->json(['response' => $changeTaskTitle]);
    }

    public function changeTaskDescription(Request $request)
    {
        $task_id=Request::input('task_id');
        $task_description=Request::input('task_description');
        $changeTaskDescription=DB::table('task')->where('id',$task_id)->update(['task_description' => $task_description]);
        return response()->json(['response' => $changeTaskDescription]);
    }

    public function getTask(Request $request)
    {
        $task_id=Request::input('task_id');
        $task=DB::table('task')->select('*')->where('id',$task_id)->get();
        $task=json_decode(json_encode($task), true);
        return $task;
    }

    public function removeTagFromTask(Request $request)
    {
        $task_id=Request::input('task_id');
        $task_tag_id=Request::input('task_tag_id');
        $getTaskTagArray=DB::table('task')->select('task_tag')->where('id',$task_id)->first();
        $getTaskTagArray=json_decode(json_encode($getTaskTagArray), true);
        $getTaskTagArray=unserialize($getTaskTagArray['task_tag']);
        foreach($getTaskTagArray as $key=>$value)
        {
            if($value==$task_tag_id)
            {
                unset($getTaskTagArray[$key]);
            }
        }
        $updateTaskTag=DB::table('task')->where('id',$task_id)->update(['task_tag' => serialize(array_values($getTaskTagArray))]);
        return response()->json(['response' => $updateTaskTag]);
    }

    public function removeTask()
    {
        $task_id=Input::get('task_id');
        $deleteTask=DB::table('task')->where('id',$task_id)->delete();
        return response()->json(['response' => $deleteTask]);
    }

    public function getContexualTask()
    {
        $user_id=Auth::user()->id;
        $contact_id=Input::get('contact_id');
        $getContexualTask=DB::table('task')->where('created_by',$user_id)->where('assigned_to',$contact_id)->get();
        $getContexualTask=json_decode(json_encode($getContexualTask), true);
        $getStructuredTaskArray=$this->getStructuredTaskArray($getContexualTask);
        $base_url=$this->url->to('/');
        $getUserPermission=$this->getUserPermission(Auth::getUser()->id);
        $content = file_get_contents($base_url.'/resources/views/templates/task-li.php');
        $agent="";
        $agent = new Agent();
        if($agent->isMobile())
        {
            $content = file_get_contents($base_url.'/resources/views/templates/task-mobile-li.php');
            $agent="mobile";
        }
        return response()->json(['response' => $getStructuredTaskArray , 'content' => $content , 'permission' => $getUserPermission , 'agent' => $agent]);
    }

    public function setDueDate()
    {
        $date=Input::get('yearNumber')."-".Input::get('monthNumber')."-".Input::get('dateNumber');
        $changeDueDate=DB::table('task')->where('id',Input::get('task_id'))->update(['due_date' => $date]);
        $current_date=date('Y-m-d', strtotime(date("Y-m-d")));
        $due_date=date('Y-m-d', strtotime($date));
        $difference=strtotime($due_date) - strtotime($current_date);
        $difference=floor($difference/(3600*24));
        if($difference==0)
        {
            $difference='Due Today';
        }
        else
        {
            $difference='Due in '.$difference.' day';
        }
        return response()->json(['response' => $changeDueDate , 'difference' => $difference]);
    }

    public function showDealTask()
    {
        $getDealTask=DB::table('task')->select('*')->where('related_deal',Input::get('deal_id'))->get();
        $getDealTask=json_decode(json_encode($getDealTask), true);
        $getDealTask=$this->getStructuredTaskArray($getDealTask);
        $base_url=$this->url->to('/');
        $content = file_get_contents($base_url.'/resources/views/templates/task-li.php');
        $agent="";
        $agent = new Agent();
        if($agent->isMobile())
        {
            $content = file_get_contents($base_url.'/resources/views/templates/task-mobile-li.php');
            $agent="mobile";
        }
        if(!empty($getDealTask))
        {
            return response()->json(['status' => 'success' , 'response' => $getDealTask , 'content' => $content , 'agent' => $agent]);
        }
        else
        {
            return response()->json(['status' => 'failed']);
        }
        
    }

    public function readAllNotification()
    {
        $valuesArray=array('unread','partialy_read');
        $readAllNotification=DB::table('notifications')->where('user_2',Input::get('user_2'))->whereIn('marked_as',$valuesArray)->update(['marked_as' => 'read']);
        return response()->json(['response' => $readAllNotification]);
    }

    public function getNotificationDifference($getNotification)
    {
        date_default_timezone_set(env('TIME_ZONE'));
        $notification_difference_array=array();
        foreach($getNotification as $key=>$value)
        {
            $datetime1 = new DateTime($getNotification[$key]['created_at']);
            $datetime2 = new DateTime(date("Y-m-d h:i:s a"));
            $interval = $datetime1->diff($datetime2);
            if($interval->format('%h')>0)
            {
                $interval = $interval->format('%h')." hr ".$interval->format('%i')." min ago";
            }
            else
            {
                $interval = $interval->format('%i')." min ago";
            }
            array_push($notification_difference_array,$interval);
        }
        return $notification_difference_array;
    }
}
