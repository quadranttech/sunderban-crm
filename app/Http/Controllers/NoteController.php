<?php

namespace App\Http\Controllers;

use Request;
use View;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use Carbon;
use File;
use App\Feed;

class NoteController extends Controller
{
    //
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->middleware('auth');
    }

    public function createNote()
    {
        $note_color_array=['orange','blue','red','green','teal','light-teal','indigo','purple','lime','brown','cyen','blue-grey'];  //NOTE COLOR ARRAY
        $color=$note_color_array[array_rand($note_color_array)];    //GET RANDOM NOTE COLOR
    	$user_id=Auth::user()->id; //GET CURRENT USER ID
        $contact_id=Input::get('contact_id');   //GET INPUT CONTACT ID
        $deal_id=Input::get('deal_id');  //GET INPUT DEAL ID
        $feed_type=Input::get('feed_type'); //GET INPUT FEED TYPE
      	$createNote=DB::table('notes')->insertGetId(['user_id' => $user_id, 'note_title' => 'untitled', 'color' => $color, 'contact_id' => $contact_id, 'deal_id' => Input::get('deal_id'), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);  //CREATE NOTE
        $feed_table=new Feed;   //CREATE FEED CLASS OBJECT
        $feed_title="created note untitled";    //FEED TITLE
        $createFeed=$feed_table->createFeed($feed_title,$user_id,$contact_id,$deal_id,$feed_type);  //CALL CREATE FEED METHOD
        $base_url=$this->url->to('/');
        $content=file_get_contents($base_url.'/resources/views/templates/note-li.php'); //GET NOTE LI TEMPLATE
      	return response()->json(['response' => $createNote , 'note_title' => 'untitled' , 'color' => $color , 'content' => $content]);
    }

    public function editNote()
    {
    	$note_title=Input::get('note_title');  //GET INPUT NOTE TITLE
    	$note=Input::get('note');  //GET INPUT NOTE
    	$note_id=Input::get('note_id');    //GET INPUT NOTE ID
    	$saveNote=DB::table('notes')->where('id',$note_id)->update(['note_title' => $note_title, 'note' => $note, 'updated_at' => date('Y-m-d H:i:s')]);   //UPDATE NOTE
    	return response()->json(['response' => $saveNote]);
    }

    public function getNoteAttachment($note_id)
    {
    	$get=DB::table('notes')->select('*')->where('id',$note_id)->first();   //GET NOTE ATTACHMENT BY NOTE ID
    	$get=json_decode(json_encode($get), true);
    	return $get['attachment'];
    }

    public function noteAttachment()
    {
    	if (Input::hasFile('add-attachment'))  //IF INPUT ADD ATTACHMENT HAS FILE
	    {
	       $destinationPath = base_path('uploads/document'); // upload path
           $file_name=Input::file('add-attachment')->getClientOriginalName();
           $file_name = pathinfo($file_name, PATHINFO_FILENAME);
           $extension = Input::file('add-attachment')->getClientOriginalExtension(); // getting image extension
           $fileName = $file_name.'-'.rand(11111,99999).'.'.$extension; // renameing image
           Input::file('add-attachment')->move($destinationPath, $fileName); // uploading file to given path
           $user_id=Auth::user()->id;
           $contact_id=Input::get('contact_id');
           $deal_id=Input::get('deal_id');
           $documentId=DB::table('documents')->insertGetId(['user_id' => $user_id, 'document_path' => 'uploads/document/', 'file_type' => $extension, 'file_name' => $fileName, 'attachment_for' => 'notes', 'contact_id' => $contact_id, 'deal_id' => $deal_id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
           $serializeNoteAttachment="";
           $getNoteAttachment=$this->getNoteAttachment(Input::get('current_note_id'));
           if($getNoteAttachment!="")
            {
            	$getNoteAttachmentArray=unserialize($getNoteAttachment);
            	array_push($getNoteAttachmentArray,$documentId);
            }
            else
            {
            	$getNoteAttachmentArray=array();
            	array_push($getNoteAttachmentArray,$documentId);
            }
            $serializeNoteAttachment=serialize($getNoteAttachmentArray);
            $saveNote=DB::table('notes')->where('id',Input::get('current_note_id'))->update(['attachment' => $serializeNoteAttachment, 'updated_at' => date('Y-m-d H:i:s')]);
           $message="file_uploaded";
	    }
           return response()->json(['response' => $message]);
    }

    public function showNote()
    {
        $user_id=Auth::user()->id;
        $contact_id=Input::get('contact_id');   //GET INPUT CONTACT ID
        $getNote=DB::table('notes')->select('*')->where('user_id',$user_id)->where('contact_id',$contact_id)->get();
        $getNote=json_decode(json_encode($getNote), true);
        if(!empty($getNote))
        {
            foreach($getNote as $key=>$value)
            {
                $getUserImage=DB::table('user_meta')->select('*')->where('user_id',$value['user_id'])->first();
                $getUserImage=json_decode(json_encode($getUserImage), true);
                $userImage[$value['id']]=$getUserImage['image'];
            }
            $base_url=$this->url->to('/');
            $content=file_get_contents($base_url.'/resources/views/templates/note-li.php');
            return response()->json(['response' => $getNote,'status' => 'success','content' => $content,'userImage' => $userImage]);
        }
        else
        {
            return response()->json(['status' => 'failed']);
        }
    }

    public function getNoteById()
    {
        $note_id=Input::get('note_id');
        $getNote=DB::table('notes')->select('*')->where('id',$note_id)->first();
        $getNote=json_decode(json_encode($getNote), true);
        $noteAttachment_array=array();
        if($getNote['attachment']!="")
        {
            $noteAttachment=unserialize($getNote['attachment']);
            if($noteAttachment!="")
            {
                foreach($noteAttachment as $key=>$value)
                {
                    $getAttachment=DB::table('documents')->select('*')->where('id',$value)->first();
                    array_push($noteAttachment_array,$getAttachment->file_name);
                }
            }
        }
        return response()->json(['response' => $getNote,'attachment' => $noteAttachment_array]);
    }

    public function deleteNote()
    {
        $deleteNote=DB::table('notes')->where('id',Input::get('note_id'))->delete();
        return response()->json(['response' => 1]);
    }

    public function showDealNote()
    {
        $getNote=DB::table('notes')->select('*')->where('deal_id',Input::get('deal_id'))->get();
        $getNote=json_decode(json_encode($getNote), true);
        if(!empty($getNote))
        {
            foreach($getNote as $key=>$value)
            {
                $getUserImage=DB::table('user_meta')->select('*')->where('user_id',$value['user_id'])->first();
                $getUserImage=json_decode(json_encode($getUserImage), true);
                $userImage[$value['id']]=$getUserImage['image'];
            }
            $base_url=$this->url->to('/');
            $content=file_get_contents($base_url.'/resources/views/templates/note-li.php');
            return response()->json(['response' => $getNote,'status' => 'success','content' => $content,'userImage' => $userImage]);
        }
        else
        {
            return response()->json(['status' => 'failed']);
        }
    }
}
