<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class MailController extends Controller
{
    //
   public function invitation_email($name,$email,$invitation_link,$sender_name,$sender_email,$admin_name,$admin_company,$base_url){
      $data = array('name'=>$name,'invitation_link'=>$invitation_link,'admin_name'=>$admin_name,'admin_company'=>$admin_company,'base_url'=>$base_url);
      Mail::send('templates/pulse-mailer/invitation', $data, function($message) use ($email,$sender_name,$sender_email,$admin_name,$admin_company) {
         $message->to($email, 'Tutorials Point')->subject
            ($admin_name.' has invited you to join '.$admin_company.' CRM');
         $message->from($sender_email,$sender_name);
      });
   }

   public function send_test_mail($email,$name,$sender_email,$portalName){
      $data = array('name'=>$name);
      
      $portalName=env('PORTAL_NAME');
      Mail::send(['text'=>'mail'], $data, function($message) use ($email,$name,$sender_email,$portalName) {
         $message->to($email, 'Tutorials Point')->subject
            ($portalName.' Testing Mail');
         $message->from($sender_email,$name);
      });
   }
}
