<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
require 'vendor/autoload.php';
use DB;
use App\Traits\MainTrait;
use Illuminate\Routing\UrlGenerator;

class StripeController extends Controller
{
    //
    use MainTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }

    public function payWithStripe($link)
    {
        $stripe = array(
          "secret_key"      => env('STRIPE_SECRET'),
          "publishable_key" => env('STRIPE_KEY')
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        $getInvoice=DB::table('invoices')->select('*')->where('link',$link)->first();
        $getInvoice=json_decode(json_encode($getInvoice), true);
        if($getInvoice['payment_status']=="paid")
        {
            echo "already paid";
        }
        else
        {
            $payment_to_be_proceed=$this->getPaypentToBeProceed($link);
            return view('paywithstripe', ['pay' => $payment_to_be_proceed , 'link' => $link , 'publishable_key' => $stripe['publishable_key']]);
        }
    }

    public function postPaymentWithStripe(Request $request)
    {
        $stripe = array(
          "secret_key"      => env('STRIPE_SECRET'),
          "publishable_key" => env('STRIPE_KEY')
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $token  = $request->get('stripeToken');

          $customer = \Stripe\Customer::create(array(
              'email' => $this->getContact($request->get('link')),
              'source'  => $token
          ));

        $getInvoice=DB::table('invoices')->select('*')->where('link',$request->get('link'))->first();
        $getInvoice=json_decode(json_encode($getInvoice), true);

          $charge = \Stripe\Charge::create(array(
              'customer' => $customer->id,
              'amount'   => $this->getPaypentToBeProceed($request->get('link'))*100,
              'currency' => $getInvoice['currency']
          ));

          $response=$this->changePaymenStatus($this->getPaypentToBeProceed($request->get('link')),$request->get('link'));

          $getInvoice=DB::table('invoices')->select('*')->where('link',$request->get('link'))->first();
          $getInvoice=json_decode(json_encode($getInvoice), true);
          $addPaymentMethod=DB::table('invoice_meta')->where('inv_id',$getInvoice['id'])->update(['payment_method' => 'stripe', 'payment_id' => $token]);

          session(['checkout' => 'done']);

          return redirect('invoice/payment/'.$request->get('link'));
    }

    public function getPaypentToBeProceed($link)
    {
        $getInvoice=DB::table('invoices')->select('*')->where('link',$link)->first();
        $getInvoice=json_decode(json_encode($getInvoice), true);
        $getInvoiceMeta=DB::table('invoice_meta')->select('*')->where('inv_id',$getInvoice['id'])->first();
        $getInvoiceMeta=json_decode(json_encode($getInvoiceMeta), true);
        $part_pay=$getInvoiceMeta['part_pay'];
        $part_pay_value=$getInvoiceMeta['part_pay_value'];
        $inv_total=$getInvoiceMeta['inv_total'];
        if($getInvoice['payment_status']=="unpaid")
        {
            if($part_pay=="fixed")
            {
                $payment_to_be_proceed=number_format((double)$part_pay_value, 2, '.', '');
            }
            else if($part_pay=="percentile")
            {
                $payment_to_be_proceed=$inv_total-(($inv_total*$part_pay_value)/100);
                $payment_to_be_proceed=number_format((double)$payment_to_be_proceed, 2, '.', '');
            }
            else
            {
                $payment_to_be_proceed=number_format((double)$inv_total, 2, '.', '');
            }
        }
        if($getInvoice['payment_status']=="partly_paid")
        {
            $payment_to_be_proceed=$inv_total-$getInvoice['paid'];
            $payment_to_be_proceed=number_format((double)$payment_to_be_proceed, 2, '.', '');
        }
        return $payment_to_be_proceed;
    }

    public function getContact($link)
    {
      $getContact=DB::table('contacts')->join('invoices', 'invoices.contact', '=', 'contacts.id')->select('*')->where('invoices.link', '=', $link)->first();
      $getContact=json_decode(json_encode($getContact), true);
      if(!empty(unserialize($getContact['email'])['work']))
      {
          $email=unserialize($getContact['email'])['work'][0];
      }
      else
      {
          $email=unserialize($getContact['email'])['home'][0];
      }
      return $email;
    }
}
