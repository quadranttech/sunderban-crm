<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Routing\UrlGenerator;
use DB;
use Auth;
use App\Feed;
use Redirect;
use View;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManager;
use App\Setting;
use App\Traits\MainTrait;
use Jenssegers\Agent\Agent;
use Intervention\Image\ImageManagerStatic as Image;
use App\Notification;
use DateTime;
use App\Traits\PermissionTrait;
use File;

class PaymentController extends Controller
{
    //
    use MainTrait;
    use PermissionTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }

    public function savePaymentCredentials()
    {
		$response=$this->savePaypalCredentials(Input::get('client_id'),Input::get('secret_key'),Input::get('company_id'),Input::get('payment_method'));
		return response()->json(['response' => $response]);
    }

    protected function savePaypalCredentials($client_id,$secret_key,$company_id,$payment_method)
    {
    	$getPaypalCredentials=DB::table('payment_gateways')->select('*')->where('company_id',$company_id)->where('payment_gateway',$payment_method)->first();
    	$getPaypalCredentials=json_decode(json_encode($getPaypalCredentials), true);
    	if(!empty($getPaypalCredentials))
    	{
    		$paypal_credentials=array(
    			'client_id' => $client_id,
    			'secret_key' => $secret_key
    			);
            $paypal_credentials=serialize($paypal_credentials);
    		$upatePaypalCredentials=DB::table('payment_gateways')->where('company_id',$company_id)->where('payment_gateway',$payment_method)->update(['credentials' => $paypal_credentials]);
    	}
    	else
    	{
    		$paypal_credentials=array(
    			'client_id' => $client_id,
    			'secret_key' => $secret_key
    			);
    		$paypal_credentials=serialize($paypal_credentials);
    		$addPaypalCredentials=DB::table('payment_gateways')->insertGetId(['company_id' => $company_id, 'payment_gateway' => $payment_method, 'credentials' => $paypal_credentials, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
    	}
    	$path = base_path('.env');
        if($payment_method=="paypal")
        {
            file_put_contents($path, str_replace(
            'PAYPAL_CLIENT_ID='.env("PAYPAL_CLIENT_ID", ""), 'PAYPAL_CLIENT_ID='.$client_id, file_get_contents($path)
            ));
            file_put_contents($path, str_replace(
                'PAYPAL_SECRET='.env("PAYPAL_SECRET", ""), 'PAYPAL_SECRET='.$secret_key, file_get_contents($path)
            ));
        }
        else
        {
            file_put_contents($path, str_replace(
            'STRIPE_KEY='.env("STRIPE_KEY", ""), 'STRIPE_KEY='.$client_id, file_get_contents($path)
            ));
            file_put_contents($path, str_replace(
                'STRIPE_SECRET='.env("STRIPE_SECRET", ""), 'STRIPE_SECRET='.$secret_key, file_get_contents($path)
            ));
        }
        return true;
    }

    public function defaultPaymentCredentials()
    {
        $getDefaultPaymentGateway=$this->get_meta('default_payment_gateway',Input::get('company_id'));
        $getDefaultPaymentGateway=json_decode(json_encode($getDefaultPaymentGateway), true);
        if(!empty($getDefaultPaymentGateway))
        {
            $changeDefaultPaymentGateway=$this->update_meta('default_payment_gateway',Input::get('payment_method'),Input::get('company_id'));
            return $changeDefaultPaymentGateway;
        }
        else
        {
            $addDefaultPaymentGateway=DB::table('settings')->insertGetId(['meta_key' => 'default_payment_gateway', 'meta_value' => Input::get('payment_method'), 'company_id' => Input::get('company_id'), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            return $addDefaultPaymentGateway;
        }
    }
}
