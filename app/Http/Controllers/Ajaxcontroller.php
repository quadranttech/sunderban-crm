<?php

namespace App\Http\Controllers;

use Request;
use App\User;
use App\Contacts;
use App\Lead;
use App\Contact_address;
use App\Improfiles;
use App\Social_profile;
use App\LeadStage;
use DB;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Illuminate\Routing\UrlGenerator;
use Redirect;
use Illuminate\Support\Facades\Input;
use Mail;
use Auth;
require 'vendor/autoload.php';
use Intervention\Image\ImageManagerStatic as Image;
use App\Traits\MainTrait;
use Dompdf\Dompdf;
use PDF;
use Dompdf\Options;
use App;
use DateTime;
use Jenssegers\Agent\Agent;
use Excel;
use App\Product;

class Ajaxcontroller extends Controller
{
    //
    use MainTrait;
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
        $this->agent = new Agent();
        $this->productTable = new Product();
    }

    public function checkEmailExists(Request $request)
    {
        $user_table=new User;
        $user=DB::table('users')->where('email', Request::input('email'))->first();
        if(!empty($user))
        {
            return response()->json(['response' => $user->email]);
        }
        else
        {
            return response()->json(['response' => '']);
        }
    }

    public function sendPasswordResetNotification($token)
    {
        // echo $token;
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sendEmail(Request $request)
    {
        $checkEmailExists=DB::table('users')->where('email', Request::input('email'))->first(); //CHECK IF EMAIL EXISTS
        $checkEmailExists=json_decode(json_encode($checkEmailExists), true);
        if(!empty($checkEmailExists))   //IF EMAIL EXISTS THEN SEND MAIL
        {
            $password_token=bcrypt($this->generateRandomString());
            $getUser=DB::table('password_resets')->select('*')->where('email',Request::input('email'))->first();  //CHECK IF USER EXISTS IN PASSWORD RESET TABLE
            $getUser=json_decode(json_encode($getUser), true);
            if(!empty($getUser))
            {
                $changePasswordToken=DB::table('password_resets')->where('email',Request::input('email'))->update(['token' => $password_token]);    //TOKEN UPDATE IF USER EXISTS
            }
            else
            {
                $insertUser=DB::table('password_resets')->insertGetId(['email' => Request::input('email'), 'token' => $password_token, 'created_at' => date('Y-m-d H:i:s')]);  //CREATE RECORD IF USER NOT EXISTS
            }
            $getDefaultEmail=DB::table('admin_company')->select('*')->where('id',$checkEmailExists['company'])->first();    //GET ADMIN COMPANY DEFAULT EMAIL
            $getDefaultEmail=json_decode(json_encode($getDefaultEmail), true);
            $sender_name=$getDefaultEmail['sender_name'];
            if($getDefaultEmail['default_email']!="" && $getDefaultEmail['marked_as_default']=="yes")
            {
                $sender_email=$getDefaultEmail['default_email'];
            }
            else
            {
                $getAdminDetails=DB::table('users')->select('*')->where('role','1')->first();
                $getAdminDetails=json_decode(json_encode($getAdminDetails), true);
                $sender_email=$getAdminDetails['email'];
                if($sender_name=="")
                {
                    $sender_name=$getAdminDetails['first_name'];
                }
            }
            $name=$checkEmailExists['first_name'].' '.$checkEmailExists['last_name'];
            $getCompany=DB::table('admin_company')->select('*')->where('id',$checkEmailExists['company'])->first();
            $getCompany=json_decode(json_encode($getCompany), true);
            $link=$this->url->to('/').'/change-password?token='.$password_token;  //LINK TO CHANGE PASSWORD
            $data = array('name'=>$name,'link'=>$link,'company_name'=>$getCompany['company_name'],'portal_name'=>$getPortalName);
            $email=Request::input('email');
            $getPortalName=env('PORTAL_NAME');
            $checkSmtp=$this->checkSmtp();
            if($checkSmtp!="ok")
            {
                return response()->json(['response' => $checkSmtp]);
            }
            else
            {
                Mail::send('templates/pulse-mailer/forget-password', $data, function($message) use ($email,$sender_name,$sender_email,$getPortalName) {
                     $message->to($email, 'Tutorials Point')->subject
                        ($getPortalName.' Forgot Password Email');
                     $message->from($sender_email,$sender_name);
                });
                return response()->json(['response' => 'success']);
            }
        }
        else
        {
            return response()->json(['response' => 'failed']);
        }
    }

    public function getContactByName(Request $request)
    {
        $contact = Request::input('contact');
        $contact_array = DB::table('contacts')->where('first_name', Request::input('contact'))->first();    //CHECK IF CONTACT EXISTS
        if(!empty($contact_array))
        {
            $result=$this->getStructureForContentTable($contact_array);
            return response()->json(['response' => $result]);
        }
        else
        {
            return response()->json(['response' => '']);
        }
    }

    public function getStructureForContentTable($value)
    {
        $phone_array=unserialize($value->phone);    //UNSERIALIZE CONTACT PHONE FIELD INTO ARRAY
        $email_array=unserialize($value->email);    //UNSERIALIZE CONTACT EMAIL FIELD INTO ARRAY
        if(!empty(array_filter($phone_array['work'])))  //CHECK IF WORK ARRAY EXISTS IN PHONE ARRAY
        {
            $address_work_home="W";
            foreach($phone_array['work'] as $value_phone)
            {
                if($value_phone!="")
                {
                   $phone_no=$value_phone;
                   break;
                }
            }
        }

        else
        {
            $address_work_home="H";
            foreach($phone_array['home'] as $value_phone)
            {
                if($value_phone!="")
                {
                   $phone_no=$value_phone; 
                   break;
                }
            }
        }
        if(!empty(array_filter($email_array['work'])))  //CHECK IF WORK ARRAY EXISTS IN EMAIL ARRAY
        {
            foreach($email_array['work'] as $value_email)
            {
                if($value_email!="")
                {
                   $email=$value_email; 
                   break;
                }
            }
        }
        else
        {
            foreach($email_array['home'] as $value_email)
            {
                if($value_email!="")
                {
                   $email=$value_email; 
                   break;
                }
            }
        }
        $id=$value->id;
        $title=$value->title;
        $first_name=$value->first_name;
        $last_name=$value->last_name;
        $street_address="-";
        $lead_stage="-";
        $lead_source="-";
        $getAdd=DB::table('contacts_address')->select('*')->where('contact_id','=',$value->id)->get();  //GET CONTACT ADDRESS
        $getAdd=json_decode(json_encode($getAdd), true);
        if(!empty(array_filter($getAdd)))   //IF CONTACT ADDRESS EXISTS THEN GET STREET ADDRESS
        {
            foreach($getAdd as $add_value)
            {
                if($add_value['street_address']!="")
                {
                    $street_address=$add_value['street_address'];
                    break;
                }
            }
        }
        $getContactLeadInfo=DB::table('lead_table')->select('*')->where('contact_id','=',$value->id)->get();    //GET CONTACT LEAD INFORMATION
        $getContactLeadInfo=json_decode(json_encode($getContactLeadInfo), true);
        if(!empty(array_filter($getContactLeadInfo)))   //IF CONTACT LEAD INFORMATION EXISTS
        {
            foreach($getContactLeadInfo as $lead_value)
            {
                if($lead_value['lead_source']!="" && $lead_value['lead_stage']!="") //IF LEAD SOURCE OR LEAD STAGE EXISTS THEN GET LEAD SOURCE AND LEAD STAGE
                {
                    $getStage=DB::table('lead_stage')->select('*')->where('id','=',$lead_value['lead_stage'])->get();
                    $getStage=json_decode(json_encode($getStage), true);
                    $lead_stage = $getStage[0]['stage'];
                    $lead_source = $lead_value['lead_source'];
                    break;
                }
            }
        }

        $individualContactArray=array(
            "id" => $value->id,
            "title" => $title,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "email" => $email,
            "phone" => $phone_no,
            "address_work_home" => $address_work_home,
            "street_address" => $street_address,
            "lead_stage" => $lead_stage,
            "lead_source" => $lead_source
        );
        return $individualContactArray;
    }

    public function getUser()
    {
        $user=DB::table('users')->get();
        $user=json_decode(json_encode($user), true);
        return $user;
    }

    public function acceptInvitation($link)
    {
        $getUser=DB::table('invitation')->select('*')->where('email',Input::get('email'))->where('key',$link)->first(); //CHECK IF EMAIL EXISTS IN INVITATION TABLE
        $getUser=json_decode(json_encode($getUser), true);
        if(!empty($getUser))
        {
            if($getUser['key']!="")
            {
                return redirect()->route('sign-up',['user_email'=>urlencode(Input::get('email')),'key'=>$link]);
            }
            else
            {
                echo "already signed up";
            }
            
        }
        else
        {
            echo "user not found";
        }
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function changePassword()
    {
        if($user = Auth::user() || Request::input('token')=="")
        {
            return redirect()->action('Log@bull');
        }
        else
        {
            $getUser=DB::table('password_resets')->select('*')->where('token',Request::input('token'))->first(); //GET USER BY TOKEN
            $getUser=json_decode(json_encode($getUser), true);
            if(!empty($getUser))
            {
                if($this->agent->isMobile())
                {
                    return view('reset-password-mobile', ['email' => $getUser['email'] , 'token' => Request::input('token')]);
                }
                return view('reset-password', ['email' => $getUser['email'] , 'token' => Request::input('token')]);
            }
            else
            {
                echo "token has expired";
            }
        }
    }

    public function resetPassword()
    {
        $email=Input::get('email');
        $password=bcrypt(Input::get('password'));
        $token=Input::get('token');
        $changePasswordResetToken=DB::table('password_resets')->where('email',$email)->where('token',$token)->update(['token' => bcrypt($this->generateRandomString())]);
        $changePassword=DB::table('users')->where('email',$email)->update(['password' => $password]);   //UPDATE USER PASSWORD
        return response()->json(['response' => 'success']);
    }

    public function logout()
    {
        Auth::logout();
        $getCompany=DB::table('admin_company')->select('*')->get();
        $getCompany=json_decode(json_encode($getCompany), true);
        if($this->agent->isMobile())
        {
            return view('logout-mobile',array('company' => $getCompany , 'role' => '1' , 'purpose' => 'login'));
        }
        return view('logout',array('company' => $getCompany , 'role' => '1' , 'purpose' => 'login'));
    }

    public function uploadCompanyLogo()
    {
        $message="failed_to_upload";
        $fileName="";
        if (Input::hasFile('company-logo')) //IF COMPANY LOGO HAS FILE
        {
            if(Input::get('x')=="" || Input::get('y')=="" || Input::get('h')=="" || Input::get('w')=="")    //IF X,Y,H,W ANY OF THESE ARE EMPTY THEN LOGO WILL BE UPLOADED WITH ITS FULL X,Y,H,W ELSE LOGO WILL BE CROPPED
            {
                $re = Input::file('company-logo')->getClientOriginalName();
                   $destinationPath = base_path('uploads/company-logo'); // upload path
                   $file_name=Input::file('company-logo')->getClientOriginalName();
                   $file_name = pathinfo($file_name, PATHINFO_FILENAME);
                   $extension = Input::file('company-logo')->getClientOriginalExtension(); // getting image extension
                   $fileName = $file_name.'-'.rand(11111,99999).'.'.$extension; // renameing image
                   Input::file('company-logo')->move($destinationPath, $fileName); // uploading file to given path
            }
            else
            {
                $file=Input::file('company-logo');
                $destinationPath = base_path('uploads/company-logo');
                $filename = $file->getClientOriginalName();
                $filename=pathinfo($filename, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $thumbName      =   $filename.'-'.rand(11111,99999).'.'.$extension;
                $img = Image::make($file->getRealPath())->crop(Input::get('w'), Input::get('h'), Input::get('x'), Input::get('y'));
                $img->save(base_path('uploads/company-logo'). '/'. $thumbName);
                $fileName=$thumbName;
            }
           $message="file_uploaded";
        }
        return response()->json(['response' => $message , 'fileName' => $fileName]);
    }

    public function acceptEstimate($key)
    {
        $getEstimateMeta=DB::table('estimate_meta')->select('*')->where('key',$key)->first();   //CHECK IF KEY EXISTS IN ESTIMATE META TABLE
        $getEstimateMeta=json_decode(json_encode($getEstimateMeta), true);
        $getEstimate=DB::table('estimates')->select('*')->where('id',$getEstimateMeta['estimate_id'])->first(); //GET ESTIMATE FROM ESTIMATE META ESTIMATE ID
        $getEstimate=json_decode(json_encode($getEstimate), true);
        $getEstimateItems=DB::table('estimate_items')->select('*')->where('estimate_id',$getEstimateMeta['estimate_id'])->get();    //GET ESTIMATE ITEM FROM ESTIMATE META ESTIMATE ID
        $getEstimateItems=json_decode(json_encode($getEstimateItems), true);
        $getEstimateItemTotal=DB::table('estimate_items')->where('estimate_id',$getEstimateMeta['estimate_id'])->sum('total');
        $getEstimateItemTotal=json_decode(json_encode($getEstimateItemTotal), true);
        $total=$getEstimateMeta['inv_total'];
        $shipping_cost=$getEstimateMeta['shipping_cost'];
        $discount=$getEstimateMeta['discount'];
        $tax=$getEstimateMeta['tax'];
        $adjustment=$getEstimateMeta['adjustment'];
        if($getEstimateMeta['status']=="accepted")
        {
            echo "accepted";
            die();
        }
        if($getEstimateMeta['status']=="declined")
        {
            echo "declined";
            die();
        }
        $updateEstimateMeta=DB::table('estimate_meta')->where('key',$key)->update(['status' => 'accepted']);
        $updateEstimateStatus=DB::table('estimates')->where('id',$getEstimate['id'])->update(['status' => 'accepted']);
        $response=$this->convertEstimateToInvoice($getEstimate['contact_type'],$getEstimate['contact'],$getEstimateItems,$getEstimate['company_id'],$shipping_cost,$discount,$tax,$getEstimateMeta['discount_id'],$getEstimateMeta['tax_id'],$getEstimateMeta['adjustment'],$getEstimate['created_by'],$getEstimateMeta['part_pay'],$getEstimateMeta['part_pay_value'],$getEstimateMeta['terms_and_conditions'],$getEstimateItems[0]['quantity_type'],$getEstimate['currency'],$getEstimate['estimate_title']);    //CONVERT ESTIMATE TO INVOICE
        $sendInvoice=$this->sendInvoiceMail($getEstimate['contact'],$getEstimateItems,$total,$shipping_cost,$discount,$tax,$adjustment,$getEstimateItemTotal,$response['inv_no'],$response['due_date'],$getEstimateMeta['terms_and_conditions'],$response['link'],$response['estimate_title'],$response['logo'],'','unpaid');    //SEND INVOICE EMAIL
        $data=$this->sendEstimateMail($getEstimate['contact'],$getEstimateItems,$total,$shipping_cost,$discount,$tax,$adjustment,$getEstimateItemTotal,$key,true,$getEstimateMeta['terms_and_conditions'],$getEstimate['link'],$getEstimate['estimate_title'],$response['logo']);   //SHOW ESTIMATE ACCEPTED ON PAGE WITH PARAMETER SHOW ON PAGE TRUE
        return view('dashboard/estimate/accept-estimate', ['items_array'=>$data['items_array'],'currency_symbol' =>$this->getCurrencySymbol('en',$getEstimateItems[0]['currency']), 'inv_total'=>$data['inv_total'],'discount'=>$data['discount'],'tax'=>$data['tax'],'adjustment'=>$getEstimateMeta['adjustment'],'terms_and_conditions'=>$getEstimateMeta['terms_and_conditions'],'quantity_type'=>$getEstimateItems[0]['quantity_type'],'total'=>$data['total'],'contact_address'=>$data['contact_address'],'contact_country'=>$data['contact_country'],'contact_state'=>$data['contact_state'],'contact_post_code'=>$data['contact_post_code'],'contact_name'=>$data['contact_name'],'company_name'=>$data['company_name'],'company_address'=>$data['company_address'],'company_country'=>$data['company_country'],'company_state'=>$data['company_state'],'company_post_code'=>$data['company_post_code'],'accept_link'=>$data['accept_link'],'decline_link'=>$data['decline_link']]);
    }

    public function declineEstimate($key)
    {
        $getEstimateMeta=DB::table('estimate_meta')->select('*')->where('key',$key)->first();   //CHECK IF KEY EXISTS IN ESTIMATE META TABLE
        $getEstimateMeta=json_decode(json_encode($getEstimateMeta), true);
        $getEstimate=DB::table('estimates')->select('*')->where('id',$getEstimateMeta['estimate_id'])->first(); //GET ESTIMATE FROM ESTIMATE META ESTIMATE ID
        $getEstimate=json_decode(json_encode($getEstimate), true);
        $getEstimateItems=DB::table('estimate_items')->select('*')->where('estimate_id',$getEstimateMeta['estimate_id'])->get();    //GET ESTIMATE ITEM FROM ESTIMATE META ESTIMATE ID
        $getEstimateItems=json_decode(json_encode($getEstimateItems), true);
        $getEstimateItemTotal=DB::table('estimate_items')->where('estimate_id',$getEstimateMeta['estimate_id'])->sum('total');
        $getEstimateItemTotal=json_decode(json_encode($getEstimateItemTotal), true);
        $total=$getEstimateMeta['inv_total'];
        $shipping_cost=$getEstimateMeta['shipping_cost'];
        $discount=$getEstimateMeta['discount'];
        $tax=$getEstimateMeta['tax'];
        $adjustment=$getEstimateMeta['adjustment'];
        if($getEstimateMeta['status']=="accepted")
        {
            echo "accepted";
            die();
        }
        if($getEstimateMeta['status']=="declined")
        {
            echo "declined";
            die();
        }
        $updateEstimateMeta=DB::table('estimate_meta')->where('key',$key)->update(['status' => 'declined']);
        $updateEstimateStatus=DB::table('estimates')->where('id',$getEstimate['id'])->update(['status' => 'declined']);

        $data=$this->sendEstimateMail($getEstimate['contact'],$getEstimateItems,$total,$shipping_cost,$discount,$tax,$adjustment,$getEstimateItemTotal,$key,true,$getEstimateMeta['terms_and_conditions'],$getEstimate['link'],$getEstimate['estimate_title'],'');   //SHOW ESTIMATE ACCEPTED ON PAGE WITH PARAMETER SHOW ON PAGE TRUE
        return view('dashboard/estimate/decline-estimate', ['items_array'=>$data['items_array'],'currency_symbol'=>$this->getCurrencySymbol('en',$getEstimateItems[0]['currency']),'inv_total'=>$data['inv_total'],'discount'=>$data['discount'],'tax'=>$data['tax'],'adjustment'=>$getEstimateMeta['adjustment'],'terms_and_conditions'=>$getEstimateMeta['terms_and_conditions'],'quantity_type'=>$getEstimateItems[0]['quantity_type'],'total'=>$data['total'],'contact_address'=>$data['contact_address'],'contact_country'=>$data['contact_country'],'contact_state'=>$data['contact_state'],'contact_post_code'=>$data['contact_post_code'],'contact_name'=>$data['contact_name'],'company_name'=>$data['company_name'],'company_address'=>$data['company_address'],'company_country'=>$data['company_country'],'company_state'=>$data['company_state'],'company_post_code'=>$data['company_post_code'],'accept_link'=>$data['accept_link'],'decline_link'=>$data['decline_link']]);
    }

    public function showPdf($key)
    {
        $data=$this->getInvoiceData($key);
        $pdf = App::make('dompdf.wrapper');
        if($data['payment_status']['payment_status']=="unpaid")
        {
            $pdf = PDF::loadView('templates.pulse-mailer.invoice-pdf-template',$data);
        }
        else
        {
            $pdf = PDF::loadView('templates.pulse-mailer.invoice-pdf-payment-done-template',$data);
        }
        return $pdf->stream();
    }

    public function showEstimatePdf($key)
    {
        $data=$this->getEstimateData($key);
        $pdf = App::make('dompdf.wrapper');
        $pdf = PDF::loadView('templates.pulse-mailer.estimate-pdf-template',$data);
        return $pdf->stream();
    }

    public function checkUserStatus()
    {
        $checkUserStatus=DB::table('users')->select('*')->where('email',Input::get('email'))->first();
        $checkUserStatus=json_decode(json_encode($checkUserStatus), true);
        return response()->json(['response' => $checkUserStatus['status']]);
    }

    public function getDealNameAndNumber()
    {
        $getDeals=DB::table('deals')->select('*')->whereMonth('created_at', '=', date('m'))->get();
        $getDeals=json_decode(json_encode($getDeals), true);
        $getDealStage=DB::table('stages')->select('*')->get();
        $getDealStage=json_decode(json_encode($getDealStage), true);
        $dealStage=array();
        $dealNameAndNumber=[];
        if(!empty($getDeals))
        {
            foreach($getDealStage as $stage_index=>$stage_value)
            {
                $count=0;
                foreach($getDeals as $deal_index=>$deal_value)
                {
                    if($stage_value['id']==$deal_value['stage'])
                    {
                        $count++;
                    }
                }
                //$dealNameAndNumber[$stage_value['slug']]=$count;  //BAR CHART
                array_push($dealNameAndNumber,$count);
                array_push($dealStage,$stage_value['slug']);
            }
            return response()->json(['response' => 'success' , 'dealNameAndNumber' => $dealNameAndNumber , 'dealStage' => $dealStage]);
        }
        else
        {
            return response()->json(['response' => 'failed']);
        }
    }

    public function getShowTutorial()
    {
        $checkShowTutorial=$this->checkShowTutorial();
        return response()->json(['response' => $checkShowTutorial]);
    }

    public function skipTutorial()
    {
        $skipTutorial=$this->changeTutorialStatus(Input::get('purpose'));
        return response()->json(['response' => $skipTutorial]);
    }

    public function viewEstimateOnBrowser($key)
    {
        $data=$this->getEstimateData($key);
        $accept_link=$this->url->to('/').'/accept-estimate/'.$data['key'];  //ESTIMATE ACCEPT LINK
        $decline_link=$this->url->to('/').'/decline-estimate/'.$data['key'];    //ESTIMATE DECLIE LINK
        $pdf_link=$this->url->to('/').'/show-estimate-pdf/'.$key;
        $data['accept_link']=$accept_link;
        $data['decline_link']=$decline_link;
        $data['pdf_link']=$pdf_link;
        return view('templates/pulse-mailer/view-estimate-template',$data);
    }

    public function viewInvoiceOnBrowser($key)
    {
        $data=$this->getInvoiceData($key);
        $payment_link=$this->url->to('/').'/invoice/payment/'.$key;
        $pdf_link=$this->url->to('/').'/show-invoice-pdf/'.$key;
        $getAdminCompanyDetails=DB::table('admin_company')->select('*')->first();   //GET ADMIN COMPANY
        $getAdminCompanyDetails=json_decode(json_encode($getAdminCompanyDetails), true);
        $company_id=$getAdminCompanyDetails['id'];
        $getDefaultPaymentMethod=$this->get_meta('default_payment_gateway',$company_id);
        $getDefaultPaymentMethod=json_decode(json_encode($getDefaultPaymentMethod), true);
        if($getDefaultPaymentMethod['meta_value']!="")
        {
            $getPaymentGatewayCredentials=DB::table('payment_gateways')->select('*')->where('company_id',$company_id)->where('payment_gateway',$getDefaultPaymentMethod['meta_value'])->first();
            $getPaymentGatewayCredentials=json_decode(json_encode($getPaymentGatewayCredentials), true);
            if($getPaymentGatewayCredentials['credentials']!="")
            {
                $payment="on";
            }
            else
            {
                $payment="off";
            }
        }
        else
        {
            $payment="off";
        }
        $data['payment_link']=$payment_link;
        $data['pdf_link']=$pdf_link;
        $data['payment']=$payment;
        return view('templates/pulse-mailer/view-invoice-template',$data);
    }

    public function getEstimateData($key)
    {
        $getEstimate=DB::table('estimates')->select('*')->where('link',$key)->first();
        $getEstimate=json_decode(json_encode($getEstimate), true);
        $getEstimateItems=DB::table('estimate_items')->select('*')->where('estimate_id',$getEstimate['id'])->get();
        $getEstimateItems=json_decode(json_encode($getEstimateItems), true);
        $getEstimateMeta=DB::table('estimate_meta')->select('*')->where('estimate_id',$getEstimate['id'])->first();
        $getEstimateMeta=json_decode(json_encode($getEstimateMeta), true);
        $getCompanyDetails=DB::table('admin_company')->select('*')->where('id',$getEstimate['company_id'])->first();
        $getCompanyDetails=json_decode(json_encode($getCompanyDetails), true);
        $getContact=DB::table('contacts')->select('*')->where('id',$getEstimate['contact'])->first();
        $getContact=json_decode(json_encode($getContact), true);
        $getContactDetails=Db::table('contacts')->join('contacts_address', 'contacts_address.contact_id', '=', 'contacts.id')->select('*')->where('contacts.id', '=', $getEstimate['contact'])->first();
        $getContactDetails=json_decode(json_encode($getContactDetails), true);
        $getEstimateItemTotal=DB::table('estimate_items')->where('estimate_id',$getEstimate['id'])->sum('total');
        $getEstimateItemTotal=json_decode(json_encode($getEstimateItemTotal), true);
        $quantity_type=$getEstimateItems[0]['quantity_type'];
        if($quantity_type=="quantity")
        {
            $quantity_type="Quantity";
        }
        else if($quantity_type=="hr")
        {
            $quantity_type="hr";
        }
        else if($quantity_type=="item")
        {
            $quantity_type="Item";
        }
        else
        {
            $quantity_type="hr + Item";
        }
        $data=array(
            'id'=>$getEstimate['est_no'],
            'company_name'=>$getCompanyDetails['company_name'],
            'company_address'=>$getCompanyDetails['address'],
            'company_country'=>$getCompanyDetails['country'],
            'company_state'=>$getCompanyDetails['state'],
            'company_post_code'=>$getCompanyDetails['post_code'],
            'contact_name'=>$getContact['first_name']." ".$getContact['last_name'],
            'contact_address'=>$getContactDetails['street_address'],
            'contact_country'=>$getContactDetails['country'],
            'contact_state'=>$getContactDetails['state'],
            'contact_post_code'=>$getContactDetails['post_code'],
            'items_array'=>$getEstimateItems,
            'total'=>$getEstimateItemTotal,
            'discount'=>$getEstimateMeta['discount'],
            'tax'=>$getEstimateMeta['tax'],
            'adjustment'=>$getEstimateMeta['adjustment'],
            'inv_total'=>$getEstimateMeta['inv_total'],
            'quantity_type'=>$quantity_type,
            'key' => $getEstimateMeta['key'],
            'currency_symbol' => $this->getCurrencySymbol('en',$getEstimateItems[0]['currency']),
            'terms_and_conditions'=>$getEstimateMeta['terms_and_conditions']
            );
        return $data;
    }

    public function getInvoiceData($key)
    {
        $getInvoice=DB::table('invoices')->select('*')->where('link',$key)->first();    //CHECK IF LINK EXISTS IN INVOICE TABLE
        $getInvoice=json_decode(json_encode($getInvoice), true);
        $getInvoiceItems=DB::table('invoice_items')->select('*')->where('inv_id',$getInvoice['id'])->get();
        $getInvoiceItems=json_decode(json_encode($getInvoiceItems), true);
        $getInvoiceMeta=DB::table('invoice_meta')->select('*')->where('inv_id',$getInvoice['id'])->first();
        $getInvoiceMeta=json_decode(json_encode($getInvoiceMeta), true);
        $getCompanyDetails=DB::table('admin_company')->select('*')->where('id',$getInvoice['company_id'])->first();
        $getCompanyDetails=json_decode(json_encode($getCompanyDetails), true);
        $getContact=DB::table('contacts')->select('*')->where('id',$getInvoice['contact'])->first();
        $getContact=json_decode(json_encode($getContact), true);
        $getContactDetails=Db::table('contacts')->join('contacts_address', 'contacts_address.contact_id', '=', 'contacts.id')->select('*')->where('contacts.id', '=', $getInvoice['contact'])->first();
        $getContactDetails=json_decode(json_encode($getContactDetails), true);
        $getInvoiceItemTotal=DB::table('invoice_items')->where('inv_id',$getInvoice['id'])->sum('total');
        $getInvoiceItemTotal=json_decode(json_encode($getInvoiceItemTotal), true);
        $getInvoiceCompanyDetails=DB::table('admin_company')->select('*')->where('id',$getInvoice['company_id'])->first();
        $getInvoiceCompanyDetails=json_decode(json_encode($getInvoiceCompanyDetails), true);
        if($getInvoiceCompanyDetails['logo']!="")
        {
            $company_logo=$getInvoiceCompanyDetails['logo'];
        }
        else
        {
            $company_logo=$this->url->to('/').'/assets/img/logo.png';
        }
        $quantity_type=$getInvoiceItems[0]['quantity_type'];
        if($quantity_type=="quantity")
        {
            $quantity_type="Quantity";
        }
        else if($quantity_type=="hr")
        {
            $quantity_type="hr";
        }
        else if($quantity_type=="item")
        {
            $quantity_type="Item";
        }
        else
        {
            $quantity_type="hr + Item";
        }
        $data=array(
            'id'=>$getInvoice['inv_no'],
            'due_date'=>$getInvoice['due_date'],
            'company_name'=>$getCompanyDetails['company_name'],
            'company_address'=>$getCompanyDetails['address'],
            'company_country'=>$getCompanyDetails['country'],
            'company_state'=>$getCompanyDetails['state'],
            'company_post_code'=>$getCompanyDetails['post_code'],
            'contact_name'=>$getContact['first_name']." ".$getContact['last_name'],
            'contact_address'=>$getContactDetails['street_address'],
            'contact_country'=>$getContactDetails['country'],
            'contact_state'=>$getContactDetails['state'],
            'contact_post_code'=>$getContactDetails['post_code'],
            'items_array'=>$getInvoiceItems,
            'total'=>$getInvoiceItemTotal,
            'discount'=>$getInvoiceMeta['discount'],
            'tax'=>$getInvoiceMeta['tax'],
            'adjustment'=>$getInvoiceMeta['adjustment'],
            'inv_total'=>$getInvoiceMeta['inv_total'],
            'terms_and_conditions'=>$getInvoiceMeta['terms_and_conditions'],
            'quantity_type'=>$quantity_type,
            'payment_status'=>$getInvoice,
            'currency_symbol' => $this->getCurrencySymbol('en',$getInvoiceItems[0]['currency']),
            'logo'=>$company_logo
            );
        return $data;
    }

    //testing purpose
    public function getPaypentToBeProceed()
    {
        $link='q88TVYMQFn';
        $getInvoice=DB::table('invoices')->select('*')->where('link',$link)->first();
        $getInvoice=json_decode(json_encode($getInvoice), true);
        $getInvoiceMeta=DB::table('invoice_meta')->select('*')->where('inv_id',$getInvoice['id'])->first();
        $getInvoiceMeta=json_decode(json_encode($getInvoiceMeta), true);
        $part_pay=$getInvoiceMeta['part_pay'];
        $part_pay_value=$getInvoiceMeta['part_pay_value'];
        $inv_total=$getInvoiceMeta['inv_total'];
        if($getInvoice['payment_status']=="unpaid")
        {
            if($part_pay=="fixed")
            {
                $payment_to_be_proceed=number_format((double)$part_pay_value, 2, '.', '');
            }
            else if($part_pay=="percentile")
            {
                $payment_to_be_proceed=$inv_total-(($inv_total*$part_pay_value)/100);
                $payment_to_be_proceed=number_format((double)$payment_to_be_proceed, 2, '.', '');
            }
            else
            {
                $payment_to_be_proceed=number_format((double)$inv_total, 2, '.', '');
            }
        }
        if($getInvoice['payment_status']=="partly_paid")
        {
            $payment_to_be_proceed=$inv_total-$getInvoice['paid'];
            $payment_to_be_proceed=number_format((double)$payment_to_be_proceed, 2, '.', '');
        }
        // return $payment_to_be_proceed;
        echo $payment_to_be_proceed;
        echo "</br>";
        echo $payment_to_be_proceed*100;
    }

    public function test()
    {
        // $getRecords=DB::table('deals')->select('*')->where( DB::raw('YEAR(created_at)'), '=', '2017' )->get();
        // $getRecords=json_decode(json_encode($getRecords), true);
        // echo "<pre>";
        // print_r($getRecords);
        // echo "</pre>";
        // $getWonStage=DB::table('status')->select('*')->where('slug','won')->first();
        // $date_array=array();
        // $deal_count_array=array();
        // if(!empty($getRecords))
        // {
        //     foreach($getRecords as $key=>$value)
        //     {
        //         if(empty($date_array) || !in_array($value['created_date'],$date_array))
        //         {
        //             array_push($date_array,$value['created_date']);
        //             $getCount=$this->getCount($getRecords,$value['created_date']);
        //             array_push($deal_count_array,$getCount);
        //         }
        //     }
        //     $deal_value_array=$this->getDealValue($getRecords,$getWonStage->id);
        //     // return response()->json(['status' => 'success' , 'date_array' => $date_array , 'deal_array' => $deal_count_array , 'deal_value_array' => $deal_value_array , 'dd' => DB::raw('YEAR(created_at)')]);
        // }
        // else
        // {
        //     // return response()->json(['status' => 'failed']);
        // }
        // echo "<pre>";
        // print_r($deal_count_array);
        // echo "</pre>";
        // echo "<pre>";
        // print_r($deal_value_array);
        // echo "</pre>";
        // return view('paywithstripe');
        // $cc=DB::table('deals')->where('stage','=','1')->where('currency','=','USD')->sum('value');
        // $cc=json_decode(json_encode($cc), true);
        // $symbol=$this->getCurrencySymbol('en','USD');
        // $cc=$symbol.$cc;
        // $result=array('currency' => 'USD' , 'value' => $cc);
        // echo $cc;
        // echo "<pre>";
        // print_r($result);
        // echo "</pre>";
        // $opop=DB::table('deals')->where('stage','=','2')->where('currency','=','EUR')->where('contact','=','3')->sum('value');
        // $opop=json_decode(json_encode($opop), true);
        // echo $opop;

        // $main_array=array();
        
        // Excel::create('New file', function($excel) {

        //     $excel->sheet('New sheet', function($sheet) {
        //         $temp_array=Input::get('data');
        //         $sheet->loadView('folder',array('html' => $temp_array));

        //     });

        // })->export('xls');
        // echo "dsfsd";
        // echo "</br>";
        // echo "<pre>";
        // print_r(Input::get('data'));
        // echo "</pre>";
        // $getSuperDeluxe1Booking = DB::table('deals')->where('stage','4')->where('deal_start','<=','2018-01-25')->where('deal_end','>=','2018-01-24')->whereRaw("FIND_IN_SET('4','related_product') <> 0")->count();
        // $getSuperDeluxe1Booking = json_decode(json_encode($getSuperDeluxe1Booking), true);
        // $getSuperDeluxe2Booking = DB::table('deals')->where('stage','4')->where('deal_start','<=','2018-01-25')->where('deal_end','>=','2018-01-24')->whereRaw("FIND_IN_SET('3','related_product') <> 0")->count();
        // $getSuperDeluxe2Booking = json_decode(json_encode($getSuperDeluxe2Booking), true);
        // // $tempQuery = DB::table('deals')->where('stage',$getCompleteStageId['id'])->whereBetween('deal')
        // echo "<pre>";
        // print_r($getSuperDeluxe1Booking);
        // echo "</pre>";
        // echo "<pre>";
        // print_r($getSuperDeluxe2Booking);
        // echo "</pre>";
        // die();
        // $room_array = [,'4','5'];
        // $getProduct = DB::table('products')->select('*')->get();
        // $getProduct = json_decode(json_encode($getProduct), true);
        // echo "<pre>";
        // print_r($getProduct);
        // echo "</pre>";
        // die();
        // $price = 0;
        // $price_array = array();
        // $type_array = ['budget','add-on'];
        // $phone_array = array('home' => array() , 'work' => array(Input::get('phone_no')));
        $inem = "ayanc@webinnova.net";
        $email_array = array('home' => array() , 'work' => array($inem));
        // $phone = serialize($phone_array);
        $email = serialize($email_array);
        echo "email serialized <br>".$email."</br>".'a:2:{s:4:"home";a:0:{}s:4:"work";a:1:{i:0;s:19:"ayanc@webinnova.net";}}'."</br>";
        $result = DB::table('contacts')->whereRaw("email REGEXP '".$email."'")->first();
        $result = json_decode(json_encode($result), true);
        echo "<pre>";
        print_r($result);
        echo "</pre>";
        if(!empty($result))
        {
            echo "not empty";
        }
        else
        {
            echo "empty";
        }
        die();
        $card = DB::select("SELECT created_at FROM `call_activity_logs` GROUP BY created_at");
        // $card = DB::select("SELECT t1.* FROM `call_activity_logs` AS t1 GROUP BY created_at JOIN `call_activity_logs` t2 ON t1.`created_at` = t2.`created_at`");
        $card = json_decode(json_encode($card), true);
        echo "<pre>";
        print_r($card);
        echo "</pre>";
        echo sizeof($card);
        die();
        // foreach($room_array as $room_key=>$room_value)
        // {
        //     $type = $type_array[$room_key];
        //     $getProductById = $this->productTable->getProductById($room_value);
        //     echo "<pre>";
        //     print_r($getProductById);
        //     echo "</pre>";
        //     if($type != "add-on")
        //     {
        //         $product_meta_array = unserialize($getProductById['product_meta']);
        //         echo "<pre>";
        //         print_r($product_meta_array);
        //         echo "</pre>";
        //         $price = $price + ($product_meta_array[$type]['room_fare'] * 2) + ($product_meta_array[$type]['per_person_price'] * 8);
        //         echo "price = ".$price."</br>";
                
        //     }
        //     else
        //     {
        //         if($getProductById['price_type'] == "fixed")
        //         {
        //             $price = $price + $getProductById['price'];
        //         }
        //         else
        //         {
        //             $percentage = ($price * $getProductById['price']) / 100;
        //             $price = $price + $percentage;
        //         }
        //         echo "price = ".$price."</br>";
        //         // array_push($price_array,$price);
        //     }
        //     array_push($price_array,$price);
        // }
        $room_array = ['1'];
        $add_on_array = ['4'];
        $add_on_title = ['Food'];
        $total_array = 0;
        foreach($room_array as $room_key=>$room_value)
        {
            $price = 0;
            $type = $type_array[$room_key];
            $getProductById = $this->productTable->getProductById($room_value);
            echo "<pre>";
            print_r($getProductById);
            echo "</pre>";
            // if($type != "add-on")
            // {
                $product_meta_array = unserialize($getProductById['product_meta']);
                echo "<pre>";
                print_r($product_meta_array);
                echo "</pre>";
                $price = $price + ($product_meta_array[$type]['room_fare'] * 2) + ($product_meta_array[$type]['per_person_price'] * 4);
                echo "price = ".$price."</br>";
                array_push($price_array,$price);
                
            // }
                foreach($add_on_array as $add_on_array_key=>$add_on_array_value)
                {
                    $getProductById = $this->productTable->getProductById($add_on_array_value);
                    echo "<pre>";
                    print_r($getProductById);
                    echo "</pre>";
                    if($getProductById['price_type'] == "fixed")
                    {
                        echo strtolower($add_on_title[$add_on_array_key])."</br>";
                        if(strpos(strtolower($add_on_title[$add_on_array_key]), 'food') !== false)
                        {
                            $food_price = ($getProductById['price'] * 2) * 4;
                            echo "food_price = ".$food_price;
                            $price = $price + $food_price;
                        }
                        else
                        {
                            $price = $price + $getProductById['price'];
                        }
                    }
                    else
                    {
                        $percentage = ($price * $getProductById['price']) / 100;
                        $price = $price + $percentage;
                    }
                    echo "price = ".$price."</br>";
                    array_push($price_array,$price);
                }
                $total_array = $total_array + $price;
            // else
            // {
            //     if($getProductById['price_type'] == "fixed")
            //     {
            //         $price = $price + $getProductById['price'];
            //     }
            //     else
            //     {
            //         $percentage = ($price * $getProductById['price']) / 100;
            //         $price = $price + $percentage;
            //     }
            //     echo "price = ".$price."</br>";
            //     // array_push($price_array,$price);
            // }
            // array_push($price_array,$price);
        }
        // echo "<pre>";
        // print_r($total_array);
        // echo "</pre>";
        echo $total_array;
        die();
        $se = 'a:2:{s:4:"home";a:0:{}s:4:"work";a:1:{i:0;s:10:"1234567890";}}';
        $un_se = unserialize($se);
        echo "<pre>";
        print_r($un_se);
        echo "</pre>";
        die();
        session(['exportData' => Input::get('exportFilterDataArray')]);
        return response()->json(['response' => Input::get('exportFilterDataArray')]);

        // $filter=["", "", "", "send", "draft"];
        // $table=['deals.source','deals.contact','deals.owner','invoices','estimates'];
        // $query=DB::table('deals')->select('deals.*');
        // // if($filter[3]!="")
        // // {
        // //     $query->join('invoices', 'invoices.deal_id', '=', 'deals.id');
        // //     echo $filter[3];
        // //     if($filter[3]=="draft" || $filter[3]=="send")
        // //     {
        // //         $query->where('invoices.status',$filter[3]);
        // //     }
        // //     else
        // //     {
        // //         $query->where('invoices.payment_status',$filter[3]);
        // //     }
        // //     foreach($filter as $key=>$value)
        // //     {
        // //         if($key<3 && $value!="")
        // //         {
        // //             $query->where($table[$key],$value);
        // //         }
        // //     }
        // //     $getDealInvoiceReport=$query->get();
        // //     $getDealInvoiceReport=json_decode(json_encode($getDealInvoiceReport), true);
        // //     echo "<pre>";
        // //     print_r($getDealInvoiceReport);
        // //     echo "</pre>";
        // // }
        // // if($filter[4]!="")
        // // {
        // //     echo $filter[4];
        // //     $query=DB::table('deals')->select('deals.*');
        // //     $query->join('estimates', 'estimates.deal_id', '=', 'deals.id');
        // //     $query->where('estimates.status',$filter[4]);
        // //     foreach($filter as $key=>$value)
        // //     {
        // //         if($key<3 && $value!="")
        // //         {
        // //             $query->where($table[$key],$value);
        // //         }
        // //     }
        // //     $getDealEstimateReport=$query->get();
        // //     $getDealEstimateReport=json_decode(json_encode($getDealEstimateReport), true);
        // //     echo "<pre>";
        // //     print_r($getDealEstimateReport);
        // //     echo "</pre>";
        // // }
        // // else
        // // {
        // //     $query=DB::table('deals')->select('deals.*');
        // //     foreach($filter as $key=>$value)
        // //     {
        // //         if($key<3 && $value!="")
        // //         {
        // //             $query->where($table[$key],$value);
        // //         }
        // //     }
        // //     $getDealReport=$query->get();
        // //     $getDealReport=json_decode(json_encode($getDealReport), true);
        // //     echo "<pre>";
        // //     print_r($getDealReport);
        // //     echo "</pre>";
        // // }
        // $query=DB::table('deals')->select('deals.*');
        // $query->join('invoices', 'invoices.deal_id', '=', 'deals.id');
        
        // $query->where('invoices.send_date','>=','2017-09-01')->where('invoices.send_date','<=','2017-09-08');
        // $getReport=$query->get();
        // $getReport=json_decode(json_encode($getReport), true);
        // // if(!empty($getDealInvoiceReport))
        // // {

        // // }
        
        // // $getReport=$query->get();
        // // $getReport=json_decode(json_encode($getReport), true);
        // echo "<pre>";
        // print_r($getReport);
        // echo "</pre>";

        // $userdupe=array();
        // $dealArray=array();
        // foreach ($getReport as $index=>$t) {
        //     if (isset($userdupe[$t["id"]])) {
        //         unset($getReport[$index]);
        //         continue;
        //     }
        //     $userdupe[$t["id"]]=$index;
        //     $dealArray[]=$getReport[$index];
        // }

        // echo "<pre>";
        // print_r($userdupe);
        // echo "</pre>";
        // echo "<pre>";
        // print_r($dealArray);
        // echo "</pre>";
    }

    public function test4()
    {
        // echo "<pre>";
        // print_r(session('exportData'));
        // echo "</pre>";
        Excel::create('New file', function($excel) {

            $excel->sheet('New sheet', function($sheet) {
                $temp_array=session('exportData');
                $sheet->loadView('folder',array('html' => $temp_array));

            });

        })->export('xls');
    }

    public function getCount($getRecords,$created_date)
    {
        $count=0;
        foreach($getRecords as $key=>$value)
        {
            if($value['created_date']==$created_date)
            {
                $count++;
            }
        }
        return $count;
    }

    public function getDealValue($getRecords,$wonStage)
    {
        $yearArray=array('01','02','03','04','05','06','07','08','09','10','11','12');
        $month=date('m', strtotime('-1 month'))+1;
        if($month<10)
        {
            $month="0".$month;
        }
        $dealValueArray=array();
        foreach($yearArray as $yearKey=>$yearValue)
        {
            if($yearValue<$month)
            {
                $dealValueCount=0;
                foreach($getRecords as $getRecordsKey=>$getRecordsValue)
                {
                    if(explode("-",$getRecordsValue['created_date'])[1]==$yearValue && $wonStage==$getRecordsValue['status'])
                    {
                        $dealValueCount=$dealValueCount+$getRecordsValue['value'];
                    }
                }
                array_push($dealValueArray,$dealValueCount);
            }
        }
        return $dealValueArray;
    }

    public function pagination()
    {
        echo config('app.name');
    }

    // public function report()
    // {
    //     $getDeals=DB::table()
    // }
}
