<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    //
    public function getProductList()
    {
    	$getProduct = DB::table('products')->select('*')->get();
    	$getProduct = json_decode(json_encode($getProduct), true);
    	return $getProduct;
    }

    public function saveProduct($data)
    {
    	$saveProduct = DB::table('products')->insertGetId($data);
    	return $saveProduct;
    }

    public function getProductById($id)
    {
    	$getProduct = DB::table('products')->select('*')->where('id',$id)->first();
    	$getProduct = json_decode(json_encode($getProduct), true);
    	return $getProduct;
    }

    public function updateProduct($data)
    {
    	$updateProduct = DB::table('products')->where('id',$data['id'])->update(['name' => $data['name'] , 'description' => $data['description'] , 'price_type' => $data['price_type'] , 'price' => $data['price'] , 'per_person_price' => $data['per_person_price'] , 'product_meta' => $data['product_meta']]);
    	return $updateProduct;
    }

    public function getProductRelatedDeal($deal_id,$result)
    {
        $getProductIdArray = array();
        $getProductRelatedDeal = array();
        $getDeal = DB::table('deals')->select('related_product')->where('id',$deal_id)->first();
        $getDeal = json_decode(json_encode($getDeal), true);
        if($getDeal['related_product'] != "")
        {
            $getProductIdArray = explode(",",$getDeal['related_product']);
            if($result == 'productId')
            {
                return $getProductIdArray;
            }
            foreach($getProductIdArray as $key=>$value)
            {
                $getProductById = $this->getProductById($value);
                array_push($getProductRelatedDeal,$getProductById);
            }
        }
        return $getProductRelatedDeal;
    }
}
