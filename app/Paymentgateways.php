<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Paymentgateways extends Model
{
    //
    protected $table = 'payment_gateways';

    public static function getPaypalClientId()
    {
    	$getPaypalCredentials=DB::table('payment_gateways')->select('*')->where('company_id','1')->first();
    	$getPaypalCredentials=json_decode(json_encode($getPaypalCredentials), true);
    	$unserializePaypalCredentialsArray=unserialize($getPaypalCredentials['credentials']);
    	return $unserializePaypalCredentialsArray['client_id'];
    }
}
