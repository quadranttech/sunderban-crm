<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Notification extends Model
{
    //
    public function createNotification($user_1,$user_2,$notification_title)
    {
    	$createNotification=DB::table('notifications')->insertGetId(['user_1' => $user_1 , 'user_2' => $user_2 , 'notification' => $notification_title , 'created_at' => date('Y-m-d h:i:s a'), 'updated_at' => date('Y-m-d h:i:s a')]);
    	return $createNotification;
    }

    public function getNotification($user)
    {
        $valuesArray=array('unread','partialy_read');
    	$getNotification=DB::table('notifications')->select('*')->where('user_2',$user)->whereIn('marked_as',$valuesArray)->get();
    	$getNotification=json_decode(json_encode($getNotification), true);
    	return $getNotification;
    }
}
