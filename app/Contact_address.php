<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contact_address extends Model
{
    //
    protected $fillable = [];
    protected $table = 'contacts_address';

    public static function getAddressByContactId($id)
    {
    	$sql="SELECT * FROM `contacts_address` WHERE `contact_id` = '".$id."'";
        $result = DB::select($sql);
    	// $result=array(
    	// 	"ss" => "dd"
    	// 	);
        $result=json_decode(json_encode($result), true);
        return $result;
    }
}
