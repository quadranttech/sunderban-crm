<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Product;
use Illuminate\Support\Facades\Input;

Route::get('/', function () {
    $path = base_path('.env');
    if (file_exists($path)) {
        if(env("DB_HOST", "")=="" && env("DB_DATABASE", "")=="" && env("DB_USERNAME", "")=="")
        {
            return redirect('/install');
        }
        else
        {
            return redirect('welcome');
        }
    }
});

Route::get('/welcome', function () {
    $path = base_path('.env');
    if (file_exists($path)) {
        if(env("DB_HOST", "")=="" && env("DB_DATABASE", "")=="" && env("DB_USERNAME", "")=="")
        {
            return redirect('/install');
        }
        else
        {
            return redirect()->route('log-in');
        }
    }
}); 

Route::get('/welcome', [ 'as' => 'log-in', 'uses' => 'Log@bull']);

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/dashboard','HomeController@dashboard');

Route::get('/log','Log@bull');

Route::get('/log', [ 'as' => 'log', 'uses' => 'Log@bull']);

Route::get('/sign-up', [ 'as' => 'sign-up', 'uses' => 'Log@bull']);

Route::get('/newlogin','Log@newlogin');

Route::post('/session/login','Log@authenticate');

//GOOGLE LOGIN
Route::get('/auth/google', 'GoogleController@redirectToProvider')->name('google.login');
Route::get('/auth/google/callback', 'GoogleController@handleProviderCallback');

//LINKEDIN LOGIN
Route::get('/auth/linkedin', 'LinkedinController@redirectToProvider')->name('linkedin.login');
Route::get('/auth/linkedin/callback', 'LinkedinController@handleProviderCallback');

Route::get('/contacts','ContactController@index');
Route::get('/contact/new','ContactController@company');
Route::get('/contact/create-separation','ContactController@createSeparation');
Route::post('/add-contacts','ContactController@create');

Route::get('/ajax/checkEmailExists','Ajaxcontroller@checkEmailExists');

Route::get('/ajax/resetPasswordEmail','Ajaxcontroller@sendEmail');

Route::get('/ajax/getContactByName','Ajaxcontroller@getContactByName');

Route::get('/edit-contact/{id}','ContactController@editContact');

Route::post('/edit-contacts','ContactController@edit');

Route::get('/tasks','TaskController@index');

Route::get('/create-task','TaskController@create');

Route::post('/save-new-task','TaskController@saveNewTask');

Route::post('/edit-contact/save-new-task','TaskController@saveNewTask');

Route::get('/ajax/markedCompleteTask','TaskController@markedCompleteTask');

Route::get('/ajax/getTaskTemplate','TaskController@getTaskTemplate');

Route::get('/ajax/getUser','Ajaxcontroller@getUser');

Route::get('/ajax/changeAssignedTo','TaskController@changeAssignedTo');

Route::get('/ajax/addNewTagToTask','TaskController@addNewTagToTask');

Route::get('/ajax/getTaskTagById','TaskController@getTaskTagById');

Route::get('/ajax/changeTaskTitle','TaskController@changeTaskTitle');

Route::get('/ajax/changeTaskDescription','TaskController@changeTaskDescription');

Route::get('/ajax/getTask','TaskController@getTask');

Route::get('/removeTagFromTask','TaskController@removeTagFromTask');

Route::post('/removeTagFromTask', 'TaskController@removeTagFromTask');

//DEALS
Route::get('/deals','DealController@index');

Route::post('/create-new-deal','DealController@create');

Route::get('/ajax/create-new-deal','DealController@create');

Route::get('/ajax/changeDealStage/','DealController@changeDealStage');

Route::get('/ajax/removeTask','TaskController@removeTask');

//DOCUMENT UPLOAD
Route::post('/ajax/document_upload','DocumentController@index');

Route::get('/ajax/getDocumentByContactId','DocumentController@getDocumentByContactId');

Route::post('/edit-contact/create-note','NoteController@createNote');

Route::post('/ajax/create-note','NoteController@createNote');

Route::post('/ajax/edit-note','NoteController@editNote');

Route::post('/ajax/attachment_upload','NoteController@noteAttachment');

Route::post('/edit-contact/show-note','NoteController@showNote');

Route::post('/ajax/getNoteById','NoteController@getNoteById');

Route::post('/edit-contact/delete-document','DocumentController@deleteDocument');

Route::post('/delete-document','DocumentController@deleteDocument');

Route::post('/setTask','TaskController@index');

Route::post('/edit-contact/setTask','TaskController@index');

Route::post('/edit-contact/get-contexual-task','TaskController@getContexualTask');

Route::post('/edit-contact/get-deal','DealController@index');

Route::post('/edit-contact/show-feed','FeedController@index');

Route::get('/settings','SettingsController@index');

Route::post('/upload-company-logo','SettingsController@uploadCompanyLogo');

Route::post('/save-company-details','SettingsController@saveCompanyDetails');

Route::post('/get-company-email','SettingsController@getCompanyEmail');

Route::post('/create-user','SettingsController@createUser');

Route::get('/accept_invitation/link/{link}','Ajaxcontroller@acceptInvitation');

Route::post('/delete-user','SettingsController@deleteUser');

Route::get('/edit-user','SettingsController@editUser');

Route::get('/manage-account','ManageaccountController@index');

Route::post('/save-account-details','ManageaccountController@saveAccountDetails');

Route::post('/upload-user-image','ManageaccountController@uploadUserImage');

Route::post('/user-search','SettingsController@userSearch');

Route::get('/deal-contexual','DealController@dealContexual');

Route::post('/update-deal','DealController@updateDeal');

Route::get('/contact/individual','ContactController@individual');

Route::post('/edit-contact/set-due-date','TaskController@setDueDate');

Route::post('/set-due-date','TaskController@setDueDate');

Route::post('/edit-contact/delete-note','NoteController@deleteNote');

Route::post('/delete-note','NoteController@deleteNote');

Route::post('/get-deal-by-contact-id','DealController@getDealByContactId');

Route::post('/edit-deal-contexual','DealController@editDeal');

Route::post('/show-deal-task','TaskController@showDealTask');

Route::post('/show-deal-document','DocumentController@showDealDocument');

Route::post('/show-deal-note','NoteController@showDealNote');

Route::get('/test','HomeController@test');

Route::get('/get-all-deals','HomeController@getAllDeals');

Route::post('/edit-contact/get-all-deals','HomeController@getAllDeals');

Route::post('/show-deal-feed','FeedController@showDealFeed');

Route::get('/change-password','Ajaxcontroller@changePassword');

Route::post('/reset-password','Ajaxcontroller@resetPassword');

Route::get('/logout','Ajaxcontroller@logout');

Route::get('/invoice','InvoiceController@createInvoice');

Route::post('/save-invoice','InvoiceController@saveInvoice');

Route::post('/invoice-attachment-upload','InvoiceController@invoiceAttachment');

Route::post('/save-settings-proposal','SettingsController@saveSettingsProposal');

Route::get('/install','InstallerController@install');

Route::post('/create-database','InstallerController@checkDatabaseConnectivity');

Route::post('/import-db','InstallerController@importDb');

Route::post('/ajax/create-user','InstallerController@createUser');

Route::post('/save-smtp-details','SettingsController@saveSmtpDetails');

Route::get('create-company','ContactController@createCompany');

Route::get('/invoices','InvoiceController@showInvoices');

Route::get('/pdf','InvoiceController@pdf');

Route::get('/edit-invoice','InvoiceController@createInvoice');

Route::post('/send-test-mail','SettingsController@sendTestMail');

Route::get('/send-in','InvoiceController@sendInvoiceMail');

Route::get('/agent','SettingsController@agentD');

Route::post('/edit-user','SettingsController@saveEditUser');

Route::post('/send-invoice','InvoiceController@sendInvoice');

Route::get('/estimates','EstimateController@showEstimate');

Route::post('/save-estimate','EstimateController@saveEstimate');

Route::get('/read-all-notification','TaskController@readAllNotification');

Route::post('/image-crop','SettingsController@imageCrop');

Route::get('/edit-estimate','EstimateController@createEstimate');

Route::get('/estimate','EstimateController@createEstimate');

Route::post('/upload-company-logo-signin','Ajaxcontroller@uploadCompanyLogo');

Route::get('/accept-estimate/{key}','Ajaxcontroller@acceptEstimate');

Route::get('/show-invoice-pdf/{key}','Ajaxcontroller@showPdf');

Route::get('/show-estimate-pdf/{key}','Ajaxcontroller@showEstimatePdf');

Route::post('/user-edit','SettingsController@userEdit');

Route::get('/get-time','Ajaxcontroller@getTime');

Route::post('/send-estimate','EstimateController@sendEstimate');

Route::get('/decline-estimate/{key}','Ajaxcontroller@declineEstimate');

Route::get('payPremium', ['as'=>'payPremium','uses'=>'PaypalController@payPremium']);
Route::get('getCheckout/{pay}/{link}', ['as'=>'getCheckout','uses'=>'CheckoutController@getCheckout']);
Route::get('getDone/{pay}/{link}', ['as'=>'getDone','uses'=>'CheckoutController@getDone']);
Route::get('getCancel', ['as'=>'getCancel','uses'=>'PaypalController@getCancel']);
Route::get('paypal-checkout/{pay}/{link}','CheckoutController@getCheckout');

Route::get('/invoice/payment/{id}','CheckoutController@checkout');

Route::post('/save-payment-credential','PaymentController@savePaymentCredentials');

Route::post('/default-payment-gateway','PaymentController@defaultPaymentCredentials');

Route::get('StripeController/stripe/{link}', array('as' => 'addmoney.paywithstripe','uses' => 'StripeController@payWithStripe'));
Route::post('StripeController/postPaymentWithStripe', array('as' => 'addmoney.stripe','uses' => 'StripeController@postPaymentWithStripe'));

Route::post('/add-deal-source','SettingsController@addDealSource');

Route::post('/remove-deal-source','SettingsController@removeDealSource');

Route::post('/crop','CropController@getAvatar');

Route::get('/asd','PaymentController@aa');

Route::get('/check-user-status','Ajaxcontroller@checkUserStatus');

Route::post('/add-currency','SettingsController@addCurrency');

Route::post('/remove-currency','SettingsController@removeCurrency');

Route::get('/get-deal-name-number','Ajaxcontroller@getDealNameAndNumber');

Route::get('/get-show-tutorial','Ajaxcontroller@getShowTutorial');

Route::get('/skip-tutorial','Ajaxcontroller@skipTutorial');

Route::post('/change-deal-visiblity','SettingsController@changeDealVisiblity');

Route::post('/add-deal-stage','SettingsController@addDealStage');

Route::post('/check-invoice','InvoiceController@checkInvoiceNo');

Route::post('/get-contact','ContactController@getContact');

Route::get('/get-ajax-notification','HomeController@getAjaxNotification');

Route::get('/view-estimate-on-browser/{key}','Ajaxcontroller@viewEstimateOnBrowser');

Route::get('/view-invoice-on-browser/{key}','Ajaxcontroller@viewInvoiceOnBrowser');

Route::post('/create-report','ReportController@createReport');
Route::post('/export-report/xls','ReportController@exportReportXls');
Route::post('/export-report/pdf','ReportController@exportReportPdf');

Route::get('/reports','ReportController@report');

Route::post('/generate-reports','ReportController@generateReport');

Route::post('/import/contact','ContactController@importContact');

//PRODUCT ROUTE
Route::get('/products','ProductController@productList');

Route::get('/create-product','ProductController@createProduct');

Route::post('/save-product','ProductController@saveProduct');

Route::get('/edit-product/{id}','ProductController@editProduct');

Route::post('/edit-product/{id}','ProductController@editProduct');

Route::get('/get-product-related-deal',function(){
    $productTable = new Product();
    $getProductRelatedDeal = $productTable->getProductRelatedDeal(Input::get('deal_id') , 'productData');   //WITH ALL PRODUCT DATA
    return response()->json(['response' => $getProductRelatedDeal]);
});

Route::get('/delete-product-from-deal',function(){
    // $getProductRelatedDeal = DB::table('deals')->select('related_product')->where('id',Input::get('deal_id'))->first();
    // $getProductRelatedDeal = json_decode(json_encode($getProductRelatedDeal), true);
    // if($getProductRelatedDeal['related_product'] != "")
    // {
    //     $getProductIdArray = unserialize($getDeal['related_product']);
    // }
    $product_id = "";
    $productTable = new Product();
    $getProductRelatedDeal = $productTable->getProductRelatedDeal(Input::get('deal_id') , 'productId');   //WITH ALL PRODUCT ID
    $removeProductPosition = array_search(Input::get('product_id'), $getProductRelatedDeal);
    unset($getProductRelatedDeal[$removeProductPosition]);
    if(!empty($getProductRelatedDeal))
    {
        $product_id = serialize($getProductRelatedDeal);
    }
    $updateProductRelatedDeal = DB::table('deals')->where('id',Input::get('deal_id'))->update(['related_product' => $product_id]);
    return response()->json(['response' => $getProductRelatedDeal]);
});

Route::get('/update-product-deal',function(){
    $productTable = new Product();
    $new_product_array = array();   //NEW ADDED PRODUCT ARRAY
    $getProductRelatedDeal = $productTable->getProductRelatedDeal(Input::get('deal_id') , 'productId');   //WITH ALL PRODUCT ID
    foreach(Input::get('product_id_to_be_added') as $key=>$value)
    {
        array_push($getProductRelatedDeal,$value);
        $getProductById = $productTable->getProductById($value);
        array_push($new_product_array,$getProductById);
    }
    $getProductRelatedDeal = serialize($getProductRelatedDeal);
    $updateProductRelatedDeal = DB::table('deals')->where('id',Input::get('deal_id'))->update(['related_product' => $getProductRelatedDeal]);
    return response()->json(['response' => $updateProductRelatedDeal , 'new_product_array' => $new_product_array]);
});

//API
Route::get('/get-api','ApiController@checkBooking');

Route::get('/get-v','ApiController@getV');

Route::get('/request-booking','ApiController@requestBooking');

Route::get('/phoneContact','ApiController@phoneContact');

Route::get('/call','ContactController@call');

Route::get('/getCallActivityLog',function(){
    $getCallActivityLog = DB::table('call_activity_logs')->select('*')->where('ref_phone_contact',Input::get('phone_id'))->get();
    $getCallActivityLog = json_decode(json_encode($getCallActivityLog), true);
    $getPhoneContact = DB::table('phone_contacts')->select('*')->where('id',Input::get('phone_id'))->first();
    return response()->json(['response' => $getCallActivityLog , 'phone_contact' => $getPhoneContact]);
});

Route::get('/send-quote','ApiController@sendQuote');

Route::get('/send-quote-v2','ApiController@sendQuoteV2');

Route::get('/test','Ajaxcontroller@test');

Route::paginate('pagination', 'Ajaxcontroller@pagination');

//ERROR
Route::get('/error',function(){
    View::make('errors/404')->with('data','');
});
